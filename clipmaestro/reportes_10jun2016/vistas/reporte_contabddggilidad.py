#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import connection
from django.core.paginator import *
from django import forms, template
from django.utils.html import *
from  contabilidad.models import *
from librerias.funciones.excel_response_mod import *
import xlwt
from django.db.models import Sum
import operator
from reportes.formularios.reportes_form import *
from django.forms.formsets import formset_factory
from reportes.models import *
from funciones import *
from librerias.funciones.funciones_vistas import *
import calendar
from django.forms.util import ErrorList
from contabilidad.funciones.contabilidad_func import *
from librerias.funciones.paginacion import *
from librerias.funciones.permisos import *
import requests
from django.db import connection


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def libro_diario(request):
    buscador = FormBuscadoresAsientoContable(request.GET)
    cabeceras = busqueda_contabilidad(buscador, 0)
    paginacion = Paginator(cabeceras, get_num_filas_template())
    numero_pagina = request.GET.get("page")

    try:
        cabeceras = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        cabeceras = paginacion.page(1)
    except EmptyPage:
        cabeceras = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = cabeceras.number
    lista_pag = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('asiento_contable/libro_diario.html', {"buscador": buscador, "objetos": cabeceras,
                                "total_paginas": total_paginas, "numero": numero,
                                "mostrar_paginacion": muestra_paginacion_template(paginacion.count),
                                "arreglo_paginado": lista_pag}, context_instance=RequestContext(request))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def libro_diario_pdf(request, id):
    total_debe = 0.00
    total_haber = 0.00
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    try:
        cabecera = Cabecera_Comp_Contable.objects.get(id=id)

        if cabecera.status == 1:
            detalles_debe = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera.id, dbcr="D").order_by("plan_cuenta__codigo")
            detalles_haber = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera.id, dbcr="H").order_by("plan_cuenta__codigo")
            for obj in detalles_debe:
                total_debe += float(obj.valor)
            for obj in detalles_haber:
                total_haber += float(obj.valor)
        else:
            detalles_debe = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera.id, dbcr="D").order_by("plan_cuenta__codigo")
            detalles_haber = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera.id, dbcr="H").order_by("plan_cuenta__codigo")
            for obj in detalles_debe:
                total_debe += float(obj.valor)
            for obj in detalles_haber:
                total_haber += float(obj.valor)

        html = render_to_string('asiento_contable/detalle_asiento_contable_pdf.html',
                                {'pagesize': 'A4',
                                 "empresa":empresa,
                                 "usuario": usuario,
                                 "fecha_actual": datetime.datetime.now(),
                                 "request": request,
                                 "total_debe": total_debe,
                                 "total_haber": total_haber,
                                 "objeto_cabecera": cabecera,
                                 "objeto_detalle_debe": detalles_debe,
                                 "objeto_detalle_haber": detalles_haber
                                 }, context_instance=RequestContext(request))

        return generar_pdf_get(html)

    except Cabecera_Comp_Contable.DoesNotExist:
        return Http404


class Reporte_103():
    def __init__(self):
        self.codigo = Codigo_SRI()
        self.lista = []
        self.total_base = 0.0
        self.total_ret = 0.0


def generar_reporte_103(lista, fecha_ini, fecha_fin):
    """
    Funcion donde se ejecutan las consultas a la base y las sumatorias
    para el reporte 103
    :param lista:
    :param fecha_ini:
    :param fecha_fin:
    :return:
    """
    obj_vigencia_retencion = VigenciaRetencionSRI()
    total_todas_compras = 0
    total_todas_ret = 0
    #obj_vigencia_retencion = VigenciaRetencionSRI.objects.filter(status= 1, fecha_inicio__lte__fecha_ini, fecha_final__gte_fecha_fin )
    #print len(obj_vigencia_retencion)
    query = Codigo_SRI.objects.values('id').distinct().order_by("codigo")#saca todos los codigo de sri ordenados por coigo
    print 'la longitud es',len(query), query[0]
    for obj in query:
        print obj["id"]
        datos_reporte = Reporte_103()
        datos_reporte.codigo = Codigo_SRI.objects.filter(id=obj["id"])[0]#el primero que encuenrtede hacer el query
        #print datos_reporte.codigo
        q_lista = CompraDetalle.objects.filter(status=1,
                                               compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                               codigo_ret_fte__id=obj["id"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")

        datos_reporte.lista = q_lista  # Lista del detalle de compra que tiene ese código de retención

        q_tcompra = CompraDetalle.objects.filter(status=1,
                                                 compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                 codigo_ret_fte__id=obj["id"])\
            .exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor',
                                                                                      field="valor"))["valor__sum"]
        total_compras = cero_if_none(q_tcompra)  # 'q_tcompra' sumariza el total de las compras ret de ese codigo

        q_tcompras_nc = CompraDetalle.objects.filter(status=1,
                                                     compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                     codigo_ret_fte__id=obj["id"],
                                                     compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('valor', field="valor"))["valor__sum"]

        total_compras_n_c = cero_if_none(q_tcompras_nc)  # 'q_tcompras_nc' sumariza el total de las compras
                                                         # con notas de credito o débito en esa fecha

        q_tret = CompraDetalle.objects.filter(status=1,
                                              compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                              codigo_ret_fte__id=obj["id"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('monto_ret_fte',
                           field="monto_ret_fte"))["monto_ret_fte__sum"]

        total_ret = cero_if_none(q_tret)  # 'q_tret' sumariza el total de las retenciones a la fte de ese codigo
                                          # en esa fecha

        datos_reporte.total_base = total_compras - total_compras_n_c
        datos_reporte.total_ret = total_ret

        if len(datos_reporte.lista) > 0:
            lista.append(datos_reporte)
            total_todas_compras += datos_reporte.total_base
            total_todas_ret += total_ret
    print 'la longitud salida es',len(query), query[0]
    return total_todas_compras, total_todas_ret




def generar_reporte_103_hasta_21_julio(lista, fecha_ini, fecha_fin):
    """
    Funcion donde se ejecutan las consultas a la base y las sumatorias
    para el reporte 103
    :param lista:
    :param fecha_ini:
    :param fecha_fin:
    :return:
    """
    total_todas_compras = 0
    total_todas_ret = 0

    query = Codigo_SRI.objects.values('codigo').distinct().order_by("codigo")#saca todos los codigo de sri ordenados por coigo
    for obj in query:
        datos_reporte = Reporte_103()
        datos_reporte.codigo = Codigo_SRI.objects.filter(codigo=obj["codigo"])[0]#el primero que encuenrtede hacer el query

        q_lista = CompraDetalle.objects.filter(status=1,
                                               compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                               codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")

        datos_reporte.lista = q_lista  # Lista del detalle de compra que tiene ese código de retención

        q_tcompra = CompraDetalle.objects.filter(status=1,
                                                 compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                 codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor',
                                                                                      field="valor"))["valor__sum"]
        total_compras = cero_if_none(q_tcompra)  # 'q_tcompra' sumariza el total de las compras ret de ese codigo

        q_tcompras_nc = CompraDetalle.objects.filter(status=1,
                                                     compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                     codigo_ret_fte__codigo=obj["codigo"],
                                                     compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('valor', field="valor"))["valor__sum"]

        total_compras_n_c = cero_if_none(q_tcompras_nc)  # 'q_tcompras_nc' sumariza el total de las compras
                                                         # con notas de credito o débito en esa fecha

        q_tret = CompraDetalle.objects.filter(status=1,
                                              compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                              codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('monto_ret_fte',
                           field="monto_ret_fte"))["monto_ret_fte__sum"]

        total_ret = cero_if_none(q_tret)  # 'q_tret' sumariza el total de las retenciones a la fte de ese codigo
                                          # en esa fecha

        datos_reporte.total_base = total_compras - total_compras_n_c
        datos_reporte.total_ret = total_ret

        if len(datos_reporte.lista) > 0:
            lista.append(datos_reporte)
            total_todas_compras += datos_reporte.total_base
            total_todas_ret += total_ret

    return total_todas_compras, total_todas_ret

def generar_reporte_103_anio_mes(lista, anio, mes):
    """
    Funcion donde se ejecutan las consultas a la base y las sumatorias
    para el reporte 103
    :param lista:
    :param fecha_ini:
    :param fecha_fin:
    :return:
    """
    total_todas_compras = 0
    total_todas_ret = 0
    fecha_ini = 0
    fecha_fin = 0
    primerdia = str(anio),'-',str(mes),'-',str(01)
    print('el primer dia es')
    print primerdia
    ultimodia = 31

    query = Codigo_SRI.objects.values('codigo').distinct().order_by("codigo")#saca todos los codigo de sri ordenados por coigo
    for obj in query:
        datos_reporte = Reporte_103()
        datos_reporte.codigo = Codigo_SRI.objects.filter(codigo=obj["codigo"])[0]#el primero que encuenrtede hacer el query

        q_lista = CompraDetalle.objects.filter(status=1,
                                               compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                               codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")

        datos_reporte.lista = q_lista  # Lista del detalle de compra que tiene ese código de retención

        q_tcompra = CompraDetalle.objects.filter(status=1,
                                                 compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                 codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor',
                                                                                      field="valor"))["valor__sum"]
        total_compras = cero_if_none(q_tcompra)  # 'q_tcompra' sumariza el total de las compras ret de ese codigo

        q_tcompras_nc = CompraDetalle.objects.filter(status=1,
                                                     compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                     codigo_ret_fte__codigo=obj["codigo"],
                                                     compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('valor', field="valor"))["valor__sum"]

        total_compras_n_c = cero_if_none(q_tcompras_nc)  # 'q_tcompras_nc' sumariza el total de las compras
                                                         # con notas de credito o débito en esa fecha

        q_tret = CompraDetalle.objects.filter(status=1,
                                              compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                              codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('monto_ret_fte',
                           field="monto_ret_fte"))["monto_ret_fte__sum"]

        total_ret = cero_if_none(q_tret)  # 'q_tret' sumariza el total de las retenciones a la fte de ese codigo
                                          # en esa fecha

        datos_reporte.total_base = total_compras - total_compras_n_c
        datos_reporte.total_ret = total_ret

        if len(datos_reporte.lista) > 0:
            lista.append(datos_reporte)
            total_todas_compras += datos_reporte.total_base
            total_todas_ret += total_ret

    return total_todas_compras, total_todas_ret




@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103(request):
    """
    Función que me devuelve un html con el reporte
    103
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={'fecha_ini': get_fecha_rango_ant(now)[0].strftime("%Y-%m-%d"),
                                         'fecha_final': get_fecha_rango_ant(now)[1].strftime("%Y-%m-%d")})
    lista = []
    total_todas_compras = 0.0
    total_todas_ret = 0.0
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_fin):
            total_todas_compras, total_todas_ret = generar_reporte_103(lista, fecha_ini, fecha_fin)
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    else:
        lista = []
    return render_to_response('reporte_contabilidad/reporte_103.html',
                              {"buscador": buscador,
                               "total_todas_compras": total_todas_compras,
                               "total_todas_ret": total_todas_ret,
                               "lista": lista
                               }, context_instance=RequestContext(request))

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_desde_22junio_revisar(request):
    """
    Función que me devuelve un html con el reporte
    103
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={'fecha_ini': get_fecha_rango_ant(now)[0].strftime("%Y-%m-%d"),
                                         'fecha_final': get_fecha_rango_ant(now)[1].strftime("%Y-%m-%d"),
                                         'anio': datetime.datetime.now().year})
    lista = []
    total_todas_compras = 0.0
    total_todas_ret = 0.0
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        mes = buscador.getMes()
        try:
            anio = buscador.getAnio()
        except:
            anio = datetime.datetime.now().year

        print('messsss')
        print str(mes)
        print 'anioooo'
        print str(anio)
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        if len(str(mes)) <2:
            mes= '0'+str(mes)
        try:
            primerdia = str(anio)+'-'+str(mes)+'-'+str('01')
        except:
            primerdia = str(datetime.datetime.now().year)+'-'+str(mes)+'-'+str('01')
        #print str(fecha_ini)
        print('el primer dia es')
        print primerdia, 'el primer dia de mes'
        try:
        #print calendar.monthrange(anio,int(mes))[1]
            ultimodigitodelmes = calendar.monthrange(anio,int(mes))[1]
        except:
            ultimodigitodelmes = 30
        try:
            ultimodia = str(anio)+'-'+str(mes)+'-'+str(ultimodigitodelmes)
        except:
            ultimodia = str(datetime.datetime.now().year)+'-'+str(mes)+'-'+str(ultimodigitodelmes)
        print ultimodia, 'el ultimo dia de mes'
        #if None not in (fecha_ini, fecha_fin):
        if str(anio) is not str('0') and str(mes) != '00' and str(mes) != '0' and anio is not None and str(anio) is not str('00'):
            #total_todas_compras, total_todas_ret = generar_reporte_103(lista, fecha_ini, fecha_fin)
            total_todas_compras, total_todas_ret = generar_reporte_103(lista, primerdia, ultimodia)
        else:
            '''
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            '''
            if str(mes) is str('0') or str(mes) is str('00'):
                print 'el mes es incorrecto :o '
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"El valor del mes es requerido")
            if anio is int('0') or anio is int('00') or anio is None:
                print('anio incorrecto :o ')
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"El valor del año es requerido")

            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerida")
    else:
        lista = []
    return render_to_response('reporte_contabilidad/reporte_103.html',
                              {"buscador": buscador,
                               "total_todas_compras": total_todas_compras,
                               "total_todas_ret": total_todas_ret,
                               "lista": lista
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_pdf(request):
    """
    Función que me devuelve un html con el reporte
    103
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    lista = []
    total_todas_compras = 0.0
    total_todas_ret = 0.0
    buscador = BuscadorReportes()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_fin):
            total_todas_compras, total_todas_ret = generar_reporte_103(lista, fecha_ini, fecha_fin)
            html = render_to_string('reporte_contabilidad/reporte_103_pdf.html',
                            {'pagesize': 'A4',
                             "lista": lista,
                             "total_todas_compras": total_todas_compras,
                             "total_todas_ret": total_todas_ret,
                             "empresa":empresa,
                             "request": request
                             }, context_instance=RequestContext(request))

            nombre = ( 'Reporte 103 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))#cambios
            return generar_pdf_nombre(html, nombre)
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 103
    return render_to_response('reporte_contabilidad/reporte_103.html',
                      {"buscador": buscador,
                       "total_todas_compras": total_todas_compras,
                       "total_todas_ret": total_todas_ret,
                       "lista": lista,
                       }, context_instance=RequestContext(request))







@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_pdf_offline_ok(request):
    """
    Función que me devuelve un html con el reporte
    103
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    lista = []
    total_todas_compras = 0.0
    total_todas_ret = 0.0
    buscador = BuscadorReportes()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_fin):
            total_todas_compras, total_todas_ret = generar_reporte_103(lista, fecha_ini, fecha_fin)
            html = render_to_string('reporte_contabilidad/reporte_103_pdf.html',
                            {'pagesize': 'A4',
                             "lista": lista,
                             "total_todas_compras": total_todas_compras,
                             "total_todas_ret": total_todas_ret,
                             "empresa":empresa,
                             "request": request
                             }, context_instance=RequestContext(request))

            nombre = ( 'Reporte 103 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))#cambios
            return generar_pdf_nombre(html, nombre)
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 103
    return render_to_response('reporte_contabilidad/reporte_103.html',
                      {"buscador": buscador,
                       "total_todas_compras": total_todas_compras,
                       "total_todas_ret": total_todas_ret,
                       "lista": lista,
                       }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_pdf_desde22_julio(request):
    """
    Función que me devuelve un html con el reporte
    103
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    lista = []
    total_todas_compras = 0.0
    total_todas_ret = 0.0
    #buscador = BuscadorReportes()
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={'fecha_ini': get_fecha_rango_ant(now)[0].strftime("%Y-%m-%d"),
                                         'fecha_final': get_fecha_rango_ant(now)[1].strftime("%Y-%m-%d"),
                                         'anio': datetime.datetime.now().year})

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        mes = buscador.getMes()
        try:
            anio = buscador.getAnio()
        except:
            anio = datetime.datetime.now().year

        print('messsss')
        print str(mes)
        print 'anioooo'
        print str(anio)
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        if len(str(mes)) <2:
            mes= '0'+str(mes)
        try:
            primerdia = str(anio)+'-'+str(mes)+'-'+str('01')
        except:
            primerdia = str(datetime.datetime.now().year)+'-'+str(mes)+'-'+str('01')
        #print str(fecha_ini)
        print('el primer dia es')
        print primerdia, 'el primer dia de mes'
        try:
        #print calendar.monthrange(anio,int(mes))[1]
            ultimodigitodelmes = calendar.monthrange(anio,int(mes))[1]
        except:
            ultimodigitodelmes = 30
        try:
            ultimodia = str(anio)+'-'+str(mes)+'-'+str(ultimodigitodelmes)
        except:
            ultimodia = str(datetime.datetime.now().year)+'-'+str(mes)+'-'+str(ultimodigitodelmes)
        print ultimodia, 'el ultimo dia de mes'
        #if None not in (fecha_ini, fecha_fin):
        if str(anio) is not str('0') and str(mes) != '00' and str(mes) != '0' and anio is not None and str(anio) is not str('00'):
            print 'el pdf del 103'
            total_todas_compras, total_todas_ret = generar_reporte_103(lista, primerdia, ultimodia)
            #total_todas_compras, total_todas_ret = generar_reporte_103(lista, fecha_ini, fecha_fin)
            html = render_to_string('reporte_contabilidad/reporte_103_pdf.html',
                            {'pagesize': 'A4',
                             "lista": lista,
                             "total_todas_compras": total_todas_compras,
                             "total_todas_ret": total_todas_ret,
                             "empresa":empresa,
                             "request": request
                             }, context_instance=RequestContext(request))

            nombre = ( 'Reporte 103 pepin ')#cambios
            return generar_pdf_nombre(html, nombre)
        else:
            if str(mes) is str('0') or str(mes) is str('00'):
                print 'el mes es incorrecto :o '
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"El valor del mes es requerido")
            if anio is int('0') or anio is int('00') or anio is None:
                print('anio incorrecto :o ')
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"El valor del año es requerido")
    # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 103
    else:
        print('el pdf no trabaja')
    return render_to_response('reporte_contabilidad/reporte_103.html',
                      {"buscador": buscador,
                       "total_todas_compras": total_todas_compras,
                       "total_todas_ret": total_todas_ret,
                       "lista": lista,
                       }, context_instance=RequestContext(request))

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_excel(request):
    """
    Función que me devuelve el reporte
    103 en excel
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    lista_clase = []
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        lista = [[u"", u"", u"", u"REPORTE 103"+TITULO]]

        if None not in (fecha_ini, fecha_fin):
            total_todas_compras, total_todas_ret = generar_reporte_103(lista_clase, fecha_ini, fecha_fin)
            ###############  Cabecera del EXCEL  ###############
            lista.append([u""])
            lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini.strftime("%Y-%m-%d")])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_fin.strftime("%Y-%m-%d")])
            lista.append([u""])
            ####################################################
            for obj in lista_clase:
                lista.append([u"Código:"+SUBTITULO, obj.codigo.codigo])
                lista.append([u"Descripción:"+SUBTITULO, obj.codigo.descripcion])
                lista.append([u""])
                lista.append([u"Código"+COLUMNA, u"Fecha"+COLUMNA, u"Tipo Doc."+COLUMNA, u"# Documento"+COLUMNA,
                              u"Concepto"+COLUMNA, u"Base"+COLUMNA, u" % "+COLUMNA, u"Valor Retenido"+COLUMNA])
                for det in obj.lista:
                    obj_list = [det.codigo_ret_fte.codigo,
                                det.compra_cabecera.fecha_reg.strftime("%Y-%m-%d"),
                                det.compra_cabecera.documento.descripcion_documento,
                                det.compra_cabecera.num_doc,
                                det.compra_cabecera.concepto,
                                det.valor,
                                obj.codigo.porcentaje,
                                #det.porc_ret_fte,# 20 julio 2015 el mismo porcentaje que el del jefe del ciclo
                                det.monto_ret_fte]
                    lista.append(obj_list)
                lista.append([u"Totales"+SUBTITULO, u"", u"", u"", u"", "$ " + str(obj.total_base)+G_TOTAL, u"",
                              "$ " + str(obj.total_ret)+G_TOTAL])
                lista.append([u""])
            lista.append([u"",u"",u"",u"",u"",u"TOTAL BASE"+SUBTITULO,U"TOTAL VALOR RETENIDO"+SUBTITULO])
            lista.append([u"",u"",u"",u"",u"","$ " + str(total_todas_compras)+G_TOTAL, "$ " + str(total_todas_ret)+G_TOTAL])
            return ExcelResponse(lista, 'Reporte 103 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))

        else:  # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 103
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/reporte_103.html',
                              {"buscador": buscador,
                               "lista": lista_clase,
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_excel(request):
    """
    Función que me devuelve el reporte
    103 en excel
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    lista_clase = []
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        lista = [[u"", u"", u"", u"REPORTE 103"+TITULO]]
