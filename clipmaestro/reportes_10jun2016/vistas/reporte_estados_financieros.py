#! /usr/bin/python
# -*- coding: UTF-8-*-
import requests

__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import json
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import connection
from django.core.paginator import *
from django import template, forms

from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.db.models import Sum
import operator
from reportes.formularios.reportes_form import *
from django.forms.formsets import formset_factory
from librerias.funciones.funciones_vistas import *
from librerias.funciones.permisos import *
from reportes.models import *
from librerias.funciones.excel_response_mod import *
# Para las comas en los miles

def print_titulo_html(titulo):
    return format_html('<h2 class="titulo-balance-g">' + titulo + "</h2>")


def print_subtitulo_html(subtitulo):
    return format_html('<h4 class="titulo-balance">' + subtitulo + "</h4>")


def print_linea_total_coln(n):
    return format_html('<p data-index=' + str(n) + ' class="totales">&nbsp;</p>')


def print_saldo_coln(a, obj, descripcion, saldo, coln, n, tipo):
    a.extend(["" for x in range(0, 2)])
    a.insert(0, descripcion)
    if tipo == 1:  #HTML
        a.insert(n, '<p class="text-right">' + float_con_comas(saldo) + '</p>')
    else:
        a.insert(n, saldo)

    if obj.operacion == "+":
        coln += saldo
    else:
        coln -= saldo

    return coln


def print_total_cuenta(obj, tipo, a, col, n):
    """
    :param obj: la instancia de la clase ( Registro en la base de datos )
    :param tipo: Si es HTML o EXCEL
    :param a: array donde se alojan los datos
    :param col: el valor del saldo de la cuenta
    :param n: el numero de la columna en que se va a imprimir (1, 2)
    :return:
    """

    a.extend(["" for x in range(0, 2)])
    if obj.operacion == "+":
        if tipo == 1:  # HTML
            a.insert(n, '<p class="text-right">' + float_con_comas(col) + '</p>')
        else:  # Excel
            a.insert(n, str(col)+G_TOTAL)

    elif obj.operacion == "-":
        if tipo == 1:  # HTML
            a.insert(n, '<p class="text-right">' + float_con_comas((col*-1)) + '</p>')
        else:  # Excel
            a.insert(n, str(col*-1)+G_TOTAL)


def sumarizar_columna(obj, col_agre, col_set):
    if obj.operacion == "@":
        col_agre += col_set
    elif obj.operacion == "#":
        col_agre -= col_set
    else:
        pass
    return col_agre


def print_linea_total(a, tipo, obj):
    a.append("")
    if obj.columna == "1":
        if tipo == 1:  # HTML
            print_linea_total_coln(1)
        else:  # EXCEL
            a.append("")
        a.extend("" for i in range(2))
    elif obj.columna == "2":
        a.append("")
        if tipo == 1:  # HTML
            a.append(print_linea_total_coln(2))
        else:
            a.append("")
        a.append("")
    else:
        pass


def format_balance_general(instance, balance, anio, mes, col1, col2, col3, col4, col5, tipo, es_acum, es_result=False):

    cursor2 = connection.cursor()

    for obj in instance.__class__.objects.all().order_by("linea", "id"):
        a = []
        try:
            codigo = " (" + obj.plan_cuenta.codigo + ")"
        except:
            codigo = None

        if obj.tipo == "T":  # Solo es título
            if obj.operacion == "E":
                if tipo == 1:  # HTML
                    a.append(print_titulo_html(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo)))
                else:  # EXCEL
                    a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo) + TITULO)
                a.extend("" for i in range(3))
            elif obj.operacion == "S":
                if tipo == 1:  # HTML:
                    a.append(print_subtitulo_html(convert_vacio_none(obj.descripcion)+convert_vacio_none(codigo)))
                else:  # EXCEL
                    a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo) + SUBTITULO)
                a.extend("" for i in range(3))
            else:
                pass

        elif obj.tipo == "L":  # Linea para totales
            print_linea_total(a, tipo, obj)

        elif obj.tipo == "D":  # Operaciones entre columnas
            if es_acum:
                if obj.caracteristica == "O":
                    try:
                        cursor2.execute('SELECT get_resultado_ejercicio_a_fecha(%s, %s, %s);', [mes, anio, 1])
                        saldo = convert_cero_none(cursor2.fetchone()[0])
                    except:
                        saldo = 0
                    finally:
                        cursor2.close()
                else:
                    if es_result:
                        try:
                            saldo_cta = SaldoCuenta.objects.get(anio=anio, plan_cuenta_id=obj.plan_cuenta_id)
                            saldo_inicial = convert_cero_none(saldo_cta.saldo_inicial)
                        except:
                            saldo_inicial = 0
                    else:
                        saldo_inicial = 0

                    saldo = redondeo(get_saldo_cuenta_acum(obj.plan_cuenta_id, anio, mes), 2) - saldo_inicial
            else:
                if obj.caracteristica == "O":
                    try:
                        cursor2.execute('SELECT get_resultado_ejercicio_a_fecha(%s, %s, %s);', [mes, anio, 0])
                        saldo = convert_cero_none(cursor2.fetchone()[0])
                    except:
                        saldo = 0
                    finally:
                        cursor2.close()
                else:
                    saldo = redondeo(get_saldo_cuenta(obj.plan_cuenta_id, anio, mes), 2)

            if obj.columna == "1":  # Presenta el saldo en la columna 1
                col1 = print_saldo_coln(a, obj, convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo), saldo, col1, 1, tipo)

            elif obj.columna == "2":  # Presenta el saldo en la columna 2
                col2 = print_saldo_coln(a, obj, convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo), saldo, col2, 2, tipo)

            elif obj.columna == "3":
                if obj.operacion == "@":
                    col3 += saldo
                elif obj.operacion == "#":
                    col3 -= saldo
                else:
                    pass

            elif obj.columna == "4":
                if obj.operacion == "@":
                    col4 += saldo
                elif obj.operacion == "#":
                    col4 -= saldo
                else:
                    pass

            elif obj.columna == "5":
                if obj.operacion == "@":
                    col5 += saldo
                elif obj.operacion == "#":
                    col5 -= saldo
                else:
                    pass
            else:
                pass

        elif obj.tipo == "1":  # Sumarizar la columna 1
            if tipo == 1:  # HTML
                if obj.caracteristica == "E":
                    a.append(print_titulo_html(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo)))

                elif obj.caracteristica == "S":
                    a.append(print_subtitulo_html(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo)))
                else:
                    a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo))
            else:  # EXCEL
                a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo) + SUBTITULO)

            if obj.columna == "1":  # Presentar en la columna 1
                print_total_cuenta(obj, tipo, a, col1, 1)
                col1 = 0
            elif obj.columna == "2":  # Presentar en la columna 2
                print_total_cuenta(obj, tipo, a, col1, 2)
                if obj.operacion == "+":
                    col2 += col1
                elif obj.operacion == "-":
                    col2 -= col1
                else:
                    pass
                col1 = 0

            if obj.columna == "3":  # Sumarizar en la columna 3
                if obj.operacion == "@":
                    col3 += col1

                elif obj.operacion == "#":
                    col3 -= col1

                else:
                    pass
            if obj.columna == "4":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col4 += col1

                elif obj.operacion == "#":
                    col4 -= col1

                else:
                    pass
            if obj.columna == "5":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col5 += col1

                elif obj.operacion == "#":
                    col5 -= col1

                else:
                    pass
            else:
                pass
        elif obj.tipo == "2":  # Sumarizar la columna 2
            if tipo == 1:  # HTML
                if obj.caracteristica == "E":
                    a.append(print_titulo_html(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo)))

                elif obj.caracteristica == "S":
                    a.append(print_subtitulo_html(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo)))
                else:
                    a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo))

            else:  #EXCEL
                a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo) + SUBTITULO)

            if obj.columna == "1":  # Presentar en la columna 1
                print_total_cuenta(obj, tipo, a, col2, 1)
                if obj.operacion == "+":
                    col1 += 2
                elif obj.operacion == "-":
                    col1 -= col2
                else:
                    pass
                col2 = 0

            elif obj.columna == "2":  # Presentar en la columna 2
                print_total_cuenta(obj, tipo, a, col2, 2)
                col2 = 0

            if obj.columna == "3":  # Sumarizar en la columna 3
                if obj.operacion == "@":
                    col3 += col2

                elif obj.operacion == "#":
                    col3 -= col2

                else:
                    pass
            if obj.columna == "4":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col4 += col2

                elif obj.operacion == "#":
                    col4 -= col2

                else:
                    pass
            if obj.columna == "5":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col5 += col2

                elif obj.operacion == "#":
                    col5 -= col2

                else:
                    pass
            else:
                pass
        elif obj.tipo == "3":  # Sumarizar la columna 3
            if tipo == 1:  # HTML
                if obj.caracteristica == "E":
                    a.append(print_titulo_html(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo)))

                elif obj.caracteristica == "S":
                    a.append(print_subtitulo_html(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo)))
                else:
                    a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo))

            else:  #EXCEL
                a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo) + SUBTITULO)

            if obj.columna == "1":  # Presentar en la columna 1
                print_total_cuenta(obj, tipo, a, col3, 1)
                col3 = 0

            elif obj.columna == "2":  # Presentar en la columna 2
                print_total_cuenta(obj, tipo, a, col3, 2)
                col3 = 0

            else:
                pass

        elif obj.tipo == "4":  # Sumarizar la columna 4
            a.append(convert_vacio_none(obj.descripcion) + convert_vacio_none(codigo))
            if obj.columna == "1":  # Presentar en la columna 1
                print_total_cuenta(obj, tipo, a, col4, 1)
                col4 = 0

            elif obj.columna == "2":  # Presentar en la columna 2
                print_total_cuenta(obj, tipo, a, col4, 2)
                col4 = 0
            else:
                pass

        elif obj.tipo == "5":  # Sumarizar la columna 5
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                print_total_cuenta(obj, tipo, a, col5, 1)
                col5 = 0

            elif obj.columna == "2":  # Presentar en la columna 2
                print_total_cuenta(obj, tipo, a, col5, 2)
                col5 = 0
            else:
                pass
        else:  # Opciones q no se consideran
            a.append(convert_vacio_none(obj.descripcion))
            a.extend("" for i in range(3))

        if len(a) > 0:
            balance.append(a)


def format_balance_general_pdf(instance, balance, anio, mes, col1, col2, col3, col4, col5,
                                         tipo, es_acum, es_result=False):
    cursor2 = connection.cursor()
    for obj in instance.__class__.objects.all().order_by("linea", "id"):
        a = []

        cuenta_detalle = []
        if obj.tipo == "T":  # Solo es título
            if obj.operacion == "E":
                if obj.descripcion is not None:
                    a.append(format_html('<p style="'+styles['titulo-balance-g']+'">' + obj.descripcion +
                                         "("+convert_vacio_none(obj.plan_cuenta.codigo)+")"+"</p>"))
                else:
                    a.append("")
                a.extend("" for i in range(1))

            elif obj.operacion == "S":
                if obj.descripcion is not None:
                    a.append(format_html('<p style="'+styles['titulo-balance']+'">' + obj.descripcion +
                                         "(" + convert_vacio_none(obj.plan_cuenta.codigo) + ")" + "</p>"))
                else:
                    a.append("")
                a.extend("" for i in range(1))

            else:
                pass
        elif obj.tipo == "L":  # Linea para totales
            a.append("")
            if obj.columna == "1":
                a.append("")
                a.append(format_html('<p style="'+styles['totales']+'">&nbsp;</p>'))
            elif obj.columna == "2":
                a.append("")
                a.append(format_html('<p style="'+styles['totales']+'">&nbsp;</p>'))
            else:
                pass
        elif obj.tipo == "D":  # Operaciones entre columnas
            if es_acum:
                if obj.caracteristica == "O":
                    try:
                        cursor2.execute('SELECT get_resultado_ejercicio_a_fecha(%s, %s, %s);', [mes, anio, 1])
                        saldo = convert_cero_none(cursor2.fetchone()[0])
                    except:
                        saldo = 0
                    finally:
                        cursor2.close()
                else:
                    if es_result:
                        try:
                            saldo_cta = SaldoCuenta.objects.get(anio=anio, plan_cuenta_id=obj.plan_cuenta_id)
                            saldo_inicial = convert_cero_none(saldo_cta.saldo_inicial)
                        except:
                            saldo_inicial = 0
                    else:
                        saldo_inicial = 0

                    saldo = redondeo(get_saldo_cuenta_acum(obj.plan_cuenta_id, anio, mes), 2) - saldo_inicial
            else:
                if obj.caracteristica == "O":
                    try:
                        cursor2.execute('SELECT get_resultado_ejercicio_a_fecha(%s, %s, %s);', [mes, anio, 0])
                        saldo = convert_cero_none(cursor2.fetchone()[0])
                    except:
                        saldo = 0
                    finally:
                        cursor2.close()
                else:
                    saldo = redondeo(get_saldo_cuenta(obj.plan_cuenta_id, anio, mes), 2)

            if obj.columna == "1":  # Presenta el saldo en la columna 1
                if obj.descripcion is not None:
                    try:
                        descripcion = convert_vacio_none(obj.descripcion) + \
                                      "(" + convert_vacio_none(obj.plan_cuenta.codigo) + ")"
                    except:
                        descripcion = convert_vacio_none(obj.descripcion)

                    a.append(format_html('<p style="'+styles['descripcion_cuenta']+'">'+ descripcion + "</p>"))

                a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(convert_cero_none(saldo))+"</p>"))
                a.extend("" for i in range(1))

                if obj.operacion == "+":
                    col1 += saldo
                else:
                    col1 -= saldo

            elif obj.columna == "2":  # Presenta el saldo en la columna 2
                try:
                    descripcion = convert_vacio_none(obj.descripcion) + \
                                  "(" + convert_vacio_none(obj.plan_cuenta.codigo) + ")"
                except:
                    descripcion = convert_vacio_none(obj.descripcion)

                a.append(format_html('<p style="'+styles['descripcion_cuenta']+'">' + descripcion + "</p>"))
                a.append("")
                a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(convert_cero_none(saldo))+"</p>"))

                if obj.operacion == "+":
                    col2 += saldo
                else:
                    col2 -= saldo

            elif obj.columna == "3":
                if obj.operacion == "@":
                    col3 += saldo
                elif obj.operacion == "#":
                    col3 -= saldo
                else:
                    pass
            elif obj.columna == "4":
                if obj.operacion == "@":
                    col4 += saldo
                elif obj.operacion == "#":
                    col4 -= saldo
                else:
                    pass
            elif obj.columna == "5":
                if obj.operacion == "@":
                    col5 += saldo
                elif obj.operacion == "#":
                    col5 -= saldo
                else:
                    pass
            else:
                pass
        elif obj.tipo == "1":  # Sumarizar la columna 1
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.caracteristica == "E":
                    a.append(format_html('<h2 style="'+styles['titulo-balance-g']+'">'+convert_vacio_none(obj.descripcion)+'</h2>'))
                elif obj.caracteristica == "S":
                    a.append(format_html('<h4 style="'+styles['titulo-balance']+'">'+convert_vacio_none(obj.descripcion)+'</h4>'))
                else:
                    a.append(convert_vacio_none(obj.descripcion))

                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(col1))+'</p>'))
                    a.append("")

                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(col1)*-1)+'</p>'))
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                if obj.caracteristica == "E":
                    a.append(format_html('<h2 style="'+styles['titulo-balance-g']+'">'+convert_vacio_none(obj.descripcion)+'</h2>'))
                elif obj.caracteristica == "S":
                    a.append(format_html('<h4 style="'+styles['titulo-balance']+'">'+convert_vacio_none(obj.descripcion)+'</h4>'))
                else:
                    a.append(convert_vacio_none(obj.descripcion))
                a.append("")

                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(col1))+'</p>'))
                    col2 += col1
                    col1 = 0
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(col1)*-1)+'</p>'))
                    col2 -= col1
                    col1 = 0
                else:
                    pass
            if obj.columna == "3":  # Sumarizar en la columna 3
                if obj.operacion == "@":
                    col3 += col1

                elif obj.operacion == "#":
                    col3 -= col1

                else:
                    pass
            if obj.columna == "4":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col4 += col1

                elif obj.operacion == "#":
                    col4 -= col1

                else:
                    pass
            if obj.columna == "5":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col5 += col1

                elif obj.operacion == "#":
                    col5 -= col1

                else:
                    pass
            else:
                pass
        elif obj.tipo == "2":  # Sumarizar la columna 2

            if obj.columna == "1":  # Presentar en la columna 1
                if obj.caracteristica == "E":
                    a.append(format_html('<h2 style="'+styles['titulo-balance-g']+'">'+convert_vacio_none(obj.descripcion)+'</h2>'))
                elif obj.caracteristica == "S":
                    a.append(format_html('<h4 style="'+styles['titulo-balance']+'">'+convert_vacio_none(obj.descripcion)+'</h4>'))
                else:
                    a.append(convert_vacio_none(obj.descripcion))

                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(col2))+'</p>'))
                    a.append("")
                    col1 += col2
                    col2 = 0
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(col2)*-1)+'</p>'))
                    a.append("")
                    col1 -= col2
                    col2 = 0
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                if obj.caracteristica == "E":
                    a.append(format_html('<h2 style="'+styles['titulo-balance-g']+'">'+convert_vacio_none(obj.descripcion)+'</h2>'))
                elif obj.caracteristica == "S":
                    a.append(format_html('<h4 style="'+styles['titulo-balance']+'">'+convert_vacio_none(obj.descripcion)+'</h4>'))
                else:
                    a.append(convert_vacio_none(obj.descripcion))
                a.append("")

                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(col2))+'</p>'))

                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(col2*-1))+'</p>'))
                else:
                    pass
                col2 = 0

            if obj.columna == "3":  # Sumarizar en la columna 3
                if obj.operacion == "@":
                    col3 += col2

                elif obj.operacion == "#":
                    col3 -= col2

                else:
                    pass
            if obj.columna == "4":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col4 += col2

                elif obj.operacion == "#":
                    col4 -= col2

                else:
                    pass
            if obj.columna == "5":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col5 += col2

                elif obj.operacion == "#":
                    col5 -= col2

                else:
                    pass
            else:
                pass
        elif obj.tipo == "3":  # Sumarizar la columna 3
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col3)+'</p>'))
                    col3 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col3*-1)+'</p>'))
                    col3 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col3)+'</p>'))
                    col3 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col3*-1)+'</p>'))
                    col3 = 0
                    a.append("")
                else:
                    pass
            else:
                pass
        elif obj.tipo == "4":  # Sumarizar la columna 4
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col4)+'</p>'))
                    col4 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col4*-1)+'</p>'))
                    col4 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col4)+'</p>'))
                    col4 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col4*-1)+'</p>'))
                    col4 = 0
                    a.append("")
                else:
                    pass
            else:
                pass
        elif obj.tipo == "5":  # Sumarizar la columna 5
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col5)+'</p>'))
                    col5 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col5*-1)+'</p>'))
                    col5 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col5)+'</p>'))
                    col5 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(col5*-1)+'</p>'))
                    col5 = 0
                    a.append("")
                else:
                    pass
            else:
                pass
        else:  # Opciones q no se consideran
            a.append(convert_vacio_none(obj.descripcion))
            a.extend("" for i in range(3))
        if len(a) > 0:
            balance.append(a)
            for obj in cuenta_detalle:
                balance.append(obj)


def saldos_cta_hijas(plan_cuenta, es_acum, a, anio, mes, tipo, num_class, col,
                     cuenta_detalle, es_result=False, id_centro_costo=None):
    """
    Llena la lista de las cuentas Hijas en el
    reporte de Balance General
    :param obj:
    :return:
    """
    b = []
    if es_acum:
        if es_result:
            try:
                if id_centro_costo is not None:
                    saldo_cta = SaldoCuentaCentroCosto.objects.get(anio=anio,
                                                                   plan_cuenta=plan_cuenta.id,
                                                                   centro_costo_id=id_centro_costo)
                else:
                    saldo_cta = SaldoCuenta.objects.get(anio=anio, plan_cuenta=plan_cuenta.id)

                saldo_inicial = convert_cero_none(saldo_cta.saldo_inicial)

            except (SaldoCuenta.DoesNotExist, SaldoCuentaCentroCosto.DoesNotExist):
                saldo_inicial = 0
        else:
            saldo_inicial = 0

        if id_centro_costo is not None:
            saldo = redondeo(get_saldo_cuenta_acum_centro_costo(plan_cuenta.id,
                                                                anio, mes, centro_costo_id=id_centro_costo), 2) - saldo_inicial
        else:
            saldo = redondeo(get_saldo_cuenta_acum(plan_cuenta.id, anio, mes), 2) - saldo_inicial

    else:

        if id_centro_costo is not None:
            saldo = redondeo(get_saldo_cuenta_centro_costo(plan_cuenta.id, anio, mes, centro_costo_id=id_centro_costo), 2)
        else:
            saldo = redondeo(get_saldo_cuenta(plan_cuenta.id, anio, mes), 2)

    if saldo != 0:
        if tipo == 1:  # HTML
            if id_centro_costo:
                c_costo = Centro_Costo.objects.get(id=id_centro_costo)
                b.append(format_html('<p data-index=1 class="sub_cuenta'
                                     + str(num_class)+'">'
                                     + convert_vacio_none(plan_cuenta.descripcion) +
                                     "("+convert_vacio_none(plan_cuenta.codigo) + ") " + c_costo.descripcion + '</p>'))
            else:
                b.append(format_html('<p data-index=1 class="sub_cuenta'
                                     + str(num_class)+'">'
                                     + convert_vacio_none(plan_cuenta.descripcion) +
                                     "("+convert_vacio_none(plan_cuenta.codigo) + ") " + '</p>'))
            if col == 1:
                b.append(float_con_comas(saldo))
                b.append("")
                b.append("")
            else:
                b.append("")
                b.append(float_con_comas(saldo))
                b.append("")

        elif tipo == 2:  # EXCEL
            if id_centro_costo:
                c_costo = Centro_Costo.objects.get(id=id_centro_costo)
                b.append(convert_vacio_none(plan_cuenta.descripcion) +
                         "("+convert_vacio_none(plan_cuenta.codigo) + ") " + c_costo.descripcion)
            else:
                b.append(convert_vacio_none(plan_cuenta.descripcion)+"(" + convert_vacio_none(plan_cuenta.codigo) + ")")

            if col == 1:
                b.append(saldo)
                b.append("")
                b.append("")
            else:
                b.append("")
                b.append(saldo)
                b.append("")
        cuenta_detalle.append(b)

    else:  # No se presenta la cuenta que no tenga movimientos
        pass

    if len(plan_cuenta.getHijos()) > 0:
        for obj_hijos in plan_cuenta.getHijos():
            saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, num_class+1,
                             col, cuenta_detalle, es_result,
                             id_centro_costo=id_centro_costo)


def calcular_saldo(centro_costos_formset, centro_costo, obj, anio, mes, es_acum):
    cont = 0
    id_centro_costo = []

    if centro_costo:
        if es_acum:
            cont = redondeo(get_saldo_cuenta_acum(obj.plan_cuenta_id, anio, mes), 2)
        else:
            cont = redondeo(get_saldo_cuenta(obj.plan_cuenta_id, anio, mes), 2)

    elif centro_costos_formset is not None:
        centro_costos_formset.is_valid()
        for obj_cc in centro_costos_formset:
            info = obj_cc.cleaned_data
            try:
                c_costo = Centro_Costo.objects.get(id=info.get("centro_costo"))
                id_centro_costo.append(c_costo.id)
            except (Centro_Costo.DoesNotExist, ValueError):
                pass
        if len(id_centro_costo) > 0:
            for obj_list in id_centro_costo:
                if es_acum:
                    cont += redondeo(get_saldo_cuenta_acum_centro_costo(obj.plan_cuenta_id, anio, mes, centro_costo_id=obj_list), 2)
                else:
                    cont += redondeo(get_saldo_cuenta_centro_costo(obj.plan_cuenta_id, anio, mes, centro_costo_id=obj_list), 2)
        else:
            if es_acum:
                cont = redondeo(get_saldo_cuenta_acum(obj.plan_cuenta_id, anio, mes), 2)
            else:
                cont = redondeo(get_saldo_cuenta(obj.plan_cuenta_id, anio, mes), 2)
    else:
        if es_acum:
            cont = redondeo(get_saldo_cuenta_acum(obj.plan_cuenta_id, anio, mes), 2)
        else:
            cont = redondeo(get_saldo_cuenta(obj.plan_cuenta_id, anio, mes), 2)

    return cont


def format_balance_general_detallado(instance, balance, anio, mes, col1,
                                     col2, col3, col4, col5, tipo, es_acum,
                                     es_result=False, centro_costo=False,
                                     perdida_ganancia=False, centro_costos_formset=None):

    cursor2 = connection.cursor()
    id_c_costo = 0

    if centro_costos_formset is not None:
        centro_costos_formset.is_valid()
        for obj_cc in centro_costos_formset:
            info = obj_cc.cleaned_data
            try:
                if info.get("centro_costo") is not None and info.get("centro_costo") != "":
                    if len(Centro_Costo.objects.all()) > 0:
                        c_costo = Centro_Costo.objects.get(id=info.get("centro_costo"))
                        id_c_costo = c_costo.id
            except (Centro_Costo.DoesNotExist, ValueError):
                pass

    for obj in instance.__class__.objects.all().order_by("linea", "id"):
        a = []
        cuenta_detalle = []

        if obj.tipo == "T":  # Solo es título
            if obj.operacion == "E":
                if tipo == 1:  # HTML
                    if obj.plan_cuenta_id is not None and obj.descripcion is not None:
                        a.append(print_titulo_html(obj.descripcion + "("+convert_vacio_none(obj.plan_cuenta.codigo) + ")"))
                    else:
                        a.append("")
                    a.extend("" for i in range(3))
                else:  # EXCEL
                    if obj.plan_cuenta_id is not None and obj.descripcion is not None:
                        a.append(convert_vacio_none(obj.descripcion)+"("+convert_vacio_none(obj.plan_cuenta.codigo) + ")"+TITULO)
                    else:
                        a.append("")
                    a.extend("" for i in range(3))

            elif obj.operacion == "S":
                if tipo == 1:  # HTML:
                    if obj.plan_cuenta_id is not None and obj.descripcion is not None:
                        a.append(print_subtitulo_html(obj.descripcion+"("+convert_vacio_none(obj.plan_cuenta.codigo)+")"))
                    else:
                        a.append("")
                    a.extend("" for i in range(3))
                else:  # EXCEL
                    if obj.plan_cuenta_id is not None and obj.descripcion is not None:
                        a.append(convert_vacio_none(obj.descripcion)+"("+convert_vacio_none(obj.plan_cuenta.codigo)+")"+SUBTITULO)
                    else:
                        a.append("")
                    a.extend("" for i in range(3))

            else:
                pass

        elif obj.tipo == "L":  # Linea para totales
            print_linea_total(a, tipo, obj)

        elif obj.tipo == "D":  # Operaciones entre columnas
            if es_acum:
                if obj.caracteristica == "O":
                    # Saldo del resultado del ejercicio cuando es por centro de costo
                    if id_c_costo == 0:
                        try:
                            cursor2.execute('SELECT get_resultado_ejercicio_a_fecha(%s, %s, %s);', [mes, anio, 1])
                            saldo = convert_cero_none(cursor2.fetchone()[0])
                        except:
                            saldo = 0
                        finally:
                            cursor2.close()
                    else:
                        try:
                            cursor2.execute('SELECT get_resultado_ejercicio_centro_costo_a_fecha(%s, %s, %s, %s);', [mes, anio, 1, id_c_costo])
                            saldo = convert_cero_none(cursor2.fetchone()[0])
                        except:
                            saldo = 0
                        finally:
                            cursor2.close()

                else:
                    if es_result:
                        try:
                            saldo_cta = SaldoCuenta.objects.get(anio=anio, plan_cuenta=obj.plan_cuenta_id)
                            saldo_inicial = convert_cero_none(saldo_cta.saldo_inicial)
                        except:
                            saldo_inicial = 0
                    else:
                        saldo_inicial = 0

                    #saldo = redondeo(get_saldo_cuenta_acum(obj.plan_cuenta_id, anio, mes), 2) - saldo_inicial
                    saldo = calcular_saldo(centro_costos_formset, centro_costo, obj, anio, mes, es_acum) - saldo_inicial
            else:
                if obj.caracteristica == "O":
                    if id_c_costo == 0:
                        try:
                            cursor2.execute('SELECT get_resultado_ejercicio_a_fecha(%s, %s, %s);', [mes, anio, 0])
                            saldo = convert_cero_none(cursor2.fetchone()[0])
                        except:
                            saldo = 0
                        finally:
                            cursor2.close()
                    else:
                        try:
                            cursor2.execute('SELECT get_resultado_ejercicio_centro_costo_a_fecha(%s, %s, %s, %s);', [mes, anio, 0, id_c_costo])
                            saldo = convert_cero_none(cursor2.fetchone()[0])
                        except:
                            saldo = 0
                        finally:
                            cursor2.close()

                else:
                    #saldo = redondeo(get_saldo_cuenta(obj.plan_cuenta_id, anio, mes), 2)
                    saldo = calcular_saldo(centro_costos_formset, centro_costo, obj, anio, mes, es_acum)

            if obj.columna == "1":  # Presenta el saldo en la columna 1
                try:
                    descripcion = convert_vacio_none(obj.descripcion) + \
                                  "(" + convert_vacio_none(obj.plan_cuenta.codigo) + ")"
                except:
                    descripcion = convert_vacio_none(obj.descripcion)

                if tipo == 1:  # HTML
                    if obj.descripcion is not None:
                        a.append(print_titulo_html(descripcion))

                    a.append(format_html('<h2 style="font-weight: bold">'+float_con_comas(saldo)+"</h2>"))
                    a.extend("" for i in range(2))

                else:  # EXCEL
                    a.append(descripcion+SUBTITULO)
                    a.append(saldo)
                    a.extend("" for i in range(2))
                try:
                    if obj.plan_cuenta_id is not None:
                        if len(obj.plan_cuenta.getHijos()) > 0:
                            for obj_hijos in obj.plan_cuenta.getHijos():
                                if perdida_ganancia:
                                    if centro_costo:
                                        for obj_centro_costo in Centro_Costo.objects.filter(status=1):
                                            id_cc = obj_centro_costo.id
                                            saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, 2, 1,
                                                             cuenta_detalle, es_result=es_result,
                                                             id_centro_costo=id_cc)
                                        saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, 2, 1,
                                                             cuenta_detalle, es_result=es_result, id_centro_costo=0
                                                             )
                                    else:
                                        if len(centro_costos_formset) > 0:  # Los centro de costos que hayan elegido
                                            if centro_costos_formset.is_valid():
                                                for obj_centro_costo_formset in centro_costos_formset:
                                                    info = obj_centro_costo_formset.cleaned_data
                                                    saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                                     cuenta_detalle, es_result=es_result,
                                                                     id_centro_costo=info.get("centro_costo", None),
                                                                     )
                                else:
                                    saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, 2, 1,
                                                     cuenta_detalle, es_result=es_result)
                except:
                    pass

                if obj.operacion == "+":
                    col1 += saldo
                else:
                    col1 -= saldo


            elif obj.columna == "2":  # Presenta el saldo en la columna 2
                try:
                    descripcion = convert_vacio_none(obj.descripcion) + \
                                  "(" + convert_vacio_none(obj.plan_cuenta.codigo) + ")"
                except:
                    descripcion = convert_vacio_none(obj.descripcion)

                if tipo == 1:  # HTML

                    a.append(print_titulo_html(descripcion))
                    a.append("")
                    a.append(print_subtitulo_html(float_con_comas(saldo)))
                    a.append("")
                else:  # EXCEL
                    a.append(descripcion+SUBTITULO)
                    a.append("")
                    a.append(saldo)
                    a.append("")
                try:
                    if obj.plan_cuenta_id is not None:
                        if len(obj.plan_cuenta.getHijos()) > 0:
                            for obj_hijos in obj.plan_cuenta.getHijos():
                                if perdida_ganancia:
                                    if centro_costo:  # Todos los centros de costo
                                        for obj_centro_costo in Centro_Costo.objects.filter(status=1):
                                            saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                         cuenta_detalle, es_result=es_result, id_centro_costo=obj_centro_costo.id)
                                        saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                         cuenta_detalle, es_result=es_result, id_centro_costo=0)
                                    else:
                                        if centro_costos_formset.is_valid():
                                            for obj_centro_costo_formset in centro_costos_formset:
                                                info = obj_centro_costo_formset.cleaned_data
                                                saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                                 cuenta_detalle, es_result=es_result,
                                                                 id_centro_costo=info.get("centro_costo", None))
                                else:
                                    saldos_cta_hijas(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                     cuenta_detalle, es_result=es_result)
                except:
                    pass

                if obj.operacion == "+":
                    col2 += saldo
                else:
                    col2 -= saldo

            elif obj.columna == "3":
                if obj.operacion == "@":
                    col3 += saldo
                elif obj.operacion == "#":
                    col3 -= saldo
                else:
                    pass
            elif obj.columna == "4":
                if obj.operacion == "@":
                    col4 += saldo
                elif obj.operacion == "#":
                    col4 -= saldo
                else:
                    pass
            elif obj.columna == "5":
                if obj.operacion == "@":
                    col5 += saldo
                elif obj.operacion == "#":
                    col5 -= saldo
                else:
                    pass
            else:
                pass
        elif obj.tipo == "1":  # Sumarizar la columna 1
            if obj.columna == "1":  # Presentar en la columna 1
                if tipo == 1:  # HTML
                    if obj.caracteristica == "E":
                        a.append(print_titulo_html(convert_vacio_none(obj.descripcion)))
                    elif obj.caracteristica == "S":
                        a.append(print_subtitulo_html(convert_vacio_none(obj.descripcion)))
                    else:
                        a.append(convert_vacio_none(obj.descripcion))
                else:  # EXCEL
                    a.append(convert_vacio_none(obj.descripcion)+SUBTITULO)

                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col1))
                        a.append("")
                        a.append("")
                    else:  # EXCEL
                        a.append(str(col1)+G_TOTAL)
                        a.append("")
                        a.append("")

                elif obj.operacion == "-":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col1*-1))
                        a.append("")
                        a.append("")
                    else:  # EXCEL
                        a.append(str(col1*-1)+G_TOTAL)
                        a.append("")
                        a.append("")
                else:
                    pass
                col1 = 0

            elif obj.columna == "2":  # Presentar en la columna 2
                if tipo == 1:  # HTML
                    if obj.caracteristica == "E":
                        a.append(print_titulo_html(convert_vacio_none(obj.descripcion)))
                    elif obj.caracteristica == "S":
                        a.append(print_subtitulo_html(convert_vacio_none(obj.descripcion)))
                    else:
                        a.append(convert_vacio_none(obj.descripcion))
                    a.append("")

                else:  # EXCEL
                    a.append(convert_vacio_none(obj.descripcion)+SUBTITULO)
                    a.append("")

                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col1))
                        a.append("")
                    else:  # EXCEL
                        a.append(str(col1)+G_TOTAL)
                        a.append("")

                    col2 += col1

                elif obj.operacion == "-":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col1*-1))
                        a.append("")
                    else:  # EXCEL
                        a.append(str(col1*-1)+G_TOTAL)
                        a.append("")

                    col2 -= col1

                else:
                    pass

                col1 = 0
            if obj.columna == "3":  # Sumarizar en la columna 3
                if obj.operacion == "@":
                    col3 += col1

                elif obj.operacion == "#":
                    col3 -= col1

                else:
                    pass
            if obj.columna == "4":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col4 += col1

                elif obj.operacion == "#":
                    col4 -= col1

                else:
                    pass
            if obj.columna == "5":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col5 += col1

                elif obj.operacion == "#":
                    col5 -= col1

                else:
                    pass
            else:
                pass
        elif obj.tipo == "2":  # Sumarizar la columna 2

            if obj.columna == "1":  # Presentar en la columna 1
                if tipo == 1:  # HTML
                    if obj.caracteristica == "E":
                        a.append(print_titulo_html(convert_vacio_none(obj.descripcion)))
                    elif obj.caracteristica == "S":
                        a.append(print_subtitulo_html(convert_vacio_none(obj.descripcion)))
                    else:
                        a.append(convert_vacio_none(obj.descripcion))
                else:  # EXCEL
                    a.append(convert_vacio_none(obj.descripcion)+SUBTITULO)

                if obj.operacion == "+":
                    if tipo == 1:  # Html
                        a.append(float_con_comas(col2))
                        a.append("")
                    else:  # EXCEL
                        a.append(str(col2)+G_TOTAL)
                        a.append("")

                    col1 += col2
                    col2 = 0
                elif obj.operacion == "-":
                    if tipo == 1:  # Html
                        a.append(float_con_comas(col2*-1))
                        a.append("")
                    else:  # EXCEL
                        a.append(str(col2*-1)+G_TOTAL)
                        a.append("")

                    col1 -= col2
                    col2 = 0
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                if tipo == 1:  # HTML
                    if obj.caracteristica == "E":
                        a.append(print_titulo_html(convert_vacio_none(obj.descripcion)))
                    elif obj.caracteristica == "S":
                        a.append(print_subtitulo_html(convert_vacio_none(obj.descripcion)))
                    else:
                        a.append(convert_vacio_none(obj.descripcion))
                    a.append("")
                else:  # EXCEL
                    a.append(convert_vacio_none(obj.descripcion)+SUBTITULO)
                    a.append("")

                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col2))
                        a.append("")
                    else:  # EXCEL
                        a.append(str(col2)+G_TOTAL)
                        a.append("")

                elif obj.operacion == "-":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col2*-1))
                        a.append("")
                    else:  # EXCEL
                        a.append(str(col2*-1)+G_TOTAL)
                        a.append("")
                else:
                    pass
                col2 = 0
            if obj.columna == "3":  # Sumarizar en la columna 3
                if obj.operacion == "@":
                    col3 += col2

                elif obj.operacion == "#":
                    col3 -= col2

                else:
                    pass
            if obj.columna == "4":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col4 += col2

                elif obj.operacion == "#":
                    col4 -= col2

                else:
                    pass
            if obj.columna == "5":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col5 += col2

                elif obj.operacion == "#":
                    col5 -= col2

                else:
                    pass
            else:
                pass
        elif obj.tipo == "3":  # Sumarizar la columna 3
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col4))
                    else:  # EXCEL
                        a.append(col4)
                    col3 = 0
                    a.append("")
                elif obj.operacion == "-":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col3*-1))
                    else:  # EXCEL
                        a.append(col3*-1)
                    col3 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col3))
                    else:
                        a.append(col3)
                    col3 = 0
                    a.append("")
                elif obj.operacion == "-":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col3*-1))
                    else:  # EXCEL
                        a.append(col3*-1)
                    col3 = 0
                    a.append("")
                else:
                    pass
            else:
                pass
        elif obj.tipo == "4":  # Sumarizar la columna 4
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col4))
                    else:  # EXCEL
                        a.append(col4)
                    col4 = 0
                    a.append("")
                elif obj.operacion == "-":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col4*-1))
                    else:  # EXCEL
                        a.append(col4*-1)
                    col4 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col4))
                    else:  # ELSE
                        a.append(col4)
                    col4 = 0
                    a.append("")
                elif obj.operacion == "-":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col4*-1))
                    else:  # ELSE
                        a.append(col4*-1)
                    col4 = 0
                    a.append("")
                else:
                    pass
            else:
                pass
        elif obj.tipo == "5":  # Sumarizar la columna 5
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col5))
                    else:  # ELSE
                        a.append(col5)
                    col5 = 0
                    a.append("")
                elif obj.operacion == "-":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col5*-1))
                    else:  # EXCEL
                        a.append(col5*-1)
                    col5 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    if tipo == 1:  # HTML
                        a.append(float_con_comas(col5))
                    else:  # EXCEL
                        a.append(col5)
                    col5 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(float_con_comas(col5*-1))
                    col5 = 0
                    a.append("")
                else:
                    pass
            else:
                pass

        else:  # Opciones q no se consideran
            a.append(convert_vacio_none(obj.descripcion))
            a.extend("" for i in range(3))
        if len(a) > 0:
            balance.append(a)
            for obj in cuenta_detalle:
                balance.append(obj)

styles = {'titulo-balance': "font-weight: bold; padding-left: 20px;font-size: 11px; width:100%",
          'titulo-balance-d': "font-weight: bold; padding-left: 40px; font-size: 11px; width:100%",
          'titulo-balance-g': "font-size: 13px; font-weight: bold; padding-top: 20px; width:100%",
          'descripcion_cuenta': "font-size: 12px; width:100%",
          'totales': 'border-top: 1px solid',
          'sub_cuenta2': 'padding-left: 40px; width:100%',
          'sub_cuenta3': 'padding-left: 60px; width:100%',
          'sub_cuenta4': 'padding-left: 800px; width:100%',
          'sub_cuenta5': 'padding-left: 100px; width:100%',
          'numerico': 'padding-left: 40px; text-align: right; width:100%',
          'numerico-bold': 'padding-left: 40px; text-align: right; font-weight: bold; width:100%',
          }


def saldos_cta_hijas_pdf(plan_cuenta, es_acum, a, anio, mes, tipo, num_class, col,
                         cuenta_detalle, es_result=False, id_centro_costo=None):
    """
    Llena la lista de las cuentas Hijas en el
    reporte de Balance General
    :param obj:
    :return:
    """
    b = []
    if es_acum:
        if es_result:
            try:
                if id_centro_costo is not None:
                    saldo_cta = SaldoCuentaCentroCosto.objects.get(anio=anio,
                                                                   plan_cuenta=plan_cuenta.id,
                                                                   centro_costo_id=id_centro_costo)
                else:
                    saldo_cta = SaldoCuenta.objects.get(anio=anio, plan_cuenta=plan_cuenta.id)

                saldo_inicial = convert_cero_none(saldo_cta.saldo_inicial)

            except (SaldoCuenta.DoesNotExist, SaldoCuentaCentroCosto.DoesNotExist):
                saldo_inicial = 0
        else:
            saldo_inicial = 0

        if id_centro_costo is not None:
            saldo = redondeo(get_saldo_cuenta_acum_centro_costo(plan_cuenta.id,
                                                                anio, mes, centro_costo_id=id_centro_costo), 2) - saldo_inicial
        else:
            saldo = redondeo(get_saldo_cuenta_acum(plan_cuenta.id, anio, mes), 2) - saldo_inicial

    else:

        if id_centro_costo is not None:
            saldo = redondeo(get_saldo_cuenta_centro_costo(plan_cuenta.id, anio, mes, centro_costo_id=id_centro_costo), 2)
        else:
            saldo = redondeo(get_saldo_cuenta(plan_cuenta.id, anio, mes), 2)

    if saldo != 0:
        if id_centro_costo:
            c_costo = Centro_Costo.objects.get(id=id_centro_costo)
            b.append(format_html('<p style="'+styles['sub_cuenta'+str(num_class)]+'">'
                             + convert_vacio_none(plan_cuenta.descripcion) +
                             "(" + convert_vacio_none(plan_cuenta.codigo) + ") " + c_costo.descripcion + '</p>'))
        else:
            b.append(format_html('<p style="'+styles['sub_cuenta'+str(num_class)]+'">'
                             + convert_vacio_none(plan_cuenta.descripcion) +
                             "(" + convert_vacio_none(plan_cuenta.codigo) + ") " + '</p>'))

        if col == 1:
            b.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(saldo)+'</p>'))
            b.append("")
        else:
            b.append("")
            b.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(saldo)+'</p>'))

        cuenta_detalle.append(b)

    else:  # No se presenta la cuenta que no tenga movimientos
        pass

    if len(plan_cuenta.getHijos()) > 0:
        for obj_hijos in plan_cuenta.getHijos():
            saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, num_class+1,
                             col, cuenta_detalle, es_result, id_centro_costo=id_centro_costo)


def format_balance_general_detallado_pdf(instance, balance, anio, mes, col1, col2, col3, col4, col5,
                                         tipo, es_acum, es_result=False, centro_costo=False,
                                         perdida_ganancia=False, centro_costos_formset=None):
    cursor2 = connection.cursor()
    id_c_costo = 0
    if centro_costos_formset is not None:
        centro_costos_formset.is_valid()
        for obj_cc in centro_costos_formset:
            info = obj_cc.cleaned_data
            try:
                c_costo = Centro_Costo.objects.get(id=info.get("centro_costo"))
                id_c_costo = c_costo.id
            except (Centro_Costo.DoesNotExist, ValueError):
                pass

    for obj in instance.__class__.objects.all().order_by("linea", "id"):
        a = []
        cuenta_detalle = []
        if obj.tipo == "T":  # Solo es título
            if obj.operacion == "E":
                if obj.descripcion is not None:
                    a.append(format_html('<p style="'+styles['titulo-balance-g']+'">' + obj.descripcion +
                                         "("+convert_vacio_none(obj.plan_cuenta.codigo)+")"+"</p>"))
                else:
                    a.append("")
                a.extend("" for i in range(1))

            elif obj.operacion == "S":
                if obj.descripcion is not None:
                    a.append(format_html('<p style="'+styles['titulo-balance']+'">' + obj.descripcion +
                                         "(" + convert_vacio_none(obj.plan_cuenta.codigo) + ")" + "</p>"))
                else:
                    a.append("")
                a.extend("" for i in range(1))

            else:
                pass
        elif obj.tipo == "L":  # Linea para totales
            a.append("")
            if obj.columna == "1":
                a.append("")
                a.append(format_html('<p style="'+styles['totales']+'">&nbsp;</p>'))
            elif obj.columna == "2":
                a.append("")
                a.append(format_html('<p style="'+styles['totales']+'">&nbsp;</p>'))
            else:
                pass
        elif obj.tipo == "D":  # Operaciones entre columnas
            if es_acum:
                if obj.caracteristica == "O":
                    # Saldo del resultado del ejercicio cuando es por centro de costo
                    if id_c_costo == 0:
                        try:
                            cursor2.execute('SELECT get_resultado_ejercicio_a_fecha(%s, %s, %s);', [mes, anio, 1])
                            saldo = convert_cero_none(cursor2.fetchone()[0])
                        except:
                            saldo = 0
                        finally:
                            cursor2.close()
                    else:
                        try:
                            cursor2.execute('SELECT get_resultado_ejercicio_centro_costo_a_fecha(%s, %s, %s, %s);', [mes, anio, 1, id_c_costo])
                            saldo = convert_cero_none(cursor2.fetchone()[0])
                        except:
                            saldo = 0
                        finally:
                            cursor2.close()

                else:
                    if es_result:
                        try:
                            saldo_cta = SaldoCuenta.objects.get(anio=anio, plan_cuenta=obj.plan_cuenta_id)
                            saldo_inicial = convert_cero_none(saldo_cta.saldo_inicial)
                        except:
                            saldo_inicial = 0
                    else:
                        saldo_inicial = 0

                    #saldo = redondeo(get_saldo_cuenta_acum(obj.plan_cuenta_id, anio, mes), 2) - saldo_inicial
                    saldo = calcular_saldo(centro_costos_formset, centro_costo, obj, anio, mes, es_acum) - saldo_inicial
            else:
                if obj.caracteristica == "O":
                    if id_c_costo == 0:
                        try:
                            cursor2.execute('SELECT get_resultado_ejercicio_a_fecha(%s, %s, %s);', [mes, anio, 0])
                            saldo = convert_cero_none(cursor2.fetchone()[0])
                        except:
                            saldo = 0
                        finally:
                            cursor2.close()
                    else:
                        try:
                            cursor2.execute('SELECT get_resultado_ejercicio_centro_costo_a_fecha(%s, %s, %s, %s);', [mes, anio, 0, id_c_costo])
                            saldo = convert_cero_none(cursor2.fetchone()[0])
                        except:
                            saldo = 0
                        finally:
                            cursor2.close()

                else:
                    #saldo = redondeo(get_saldo_cuenta(obj.plan_cuenta_id, anio, mes), 2)
                    saldo = calcular_saldo(centro_costos_formset, centro_costo, obj, anio, mes, es_acum)

            if obj.columna == "1":  # Presenta el saldo en la columna 1
                if obj.descripcion is not None:
                    try:
                        descripcion = convert_vacio_none(obj.descripcion) + \
                                      "(" + convert_vacio_none(obj.plan_cuenta.codigo) + ")"
                    except:
                        descripcion = convert_vacio_none(obj.descripcion)

                    a.append(format_html('<h2 style="'+styles['titulo-balance-d']+'">'+ descripcion + "</h2>"))

                a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(saldo))+"</p>"))
                a.extend("" for i in range(1))
                try:
                    if obj.plan_cuenta_id is not None:
                        if len(obj.plan_cuenta.getHijos()) > 0:
                            for obj_hijos in obj.plan_cuenta.getHijos():
                                if perdida_ganancia:
                                    if centro_costo:
                                        for obj_centro_costo in Centro_Costo.objects.filter(status=1):
                                            id_cc = obj_centro_costo.id
                                            saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, 2, 1,
                                                             cuenta_detalle, es_result=es_result,
                                                             id_centro_costo=id_cc)
                                        saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, 2, 1,
                                                             cuenta_detalle, es_result=es_result, id_centro_costo=0
                                                             )
                                    else:
                                        if len(centro_costos_formset) > 0:  # Los centro de costos que hayan elegido
                                            if centro_costos_formset.is_valid():
                                                for obj_centro_costo_formset in centro_costos_formset:
                                                    info = obj_centro_costo_formset.cleaned_data
                                                    saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                                     cuenta_detalle, es_result=es_result,
                                                                     id_centro_costo=info.get("centro_costo", None),
                                                                     )
                                else:
                                    saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, 2, 1,
                                                     cuenta_detalle, es_result=es_result)
                except:
                    pass

                if obj.operacion == "+":
                    col1 += saldo
                else:
                    col1 -= saldo

            elif obj.columna == "2":  # Presenta el saldo en la columna 2
                try:
                    descripcion = convert_vacio_none(obj.descripcion) + \
                                  "(" + convert_vacio_none(obj.plan_cuenta.codigo) + ")"
                except:
                    descripcion = convert_vacio_none(obj.descripcion)

                a.append(format_html('<h2 style="'+styles['titulo-balance-d']+'">' + descripcion + "</h2>"))
                a.append("")
                a.append(format_html('<p style="'+styles['numerico-bold']+'">'+float_con_comas(convert_cero_none(saldo))+"</p>"))

                try:
                    if obj.plan_cuenta_id is not None:
                        if len(obj.plan_cuenta.getHijos()) > 0:
                            for obj_hijos in obj.plan_cuenta.getHijos():
                                if perdida_ganancia:
                                    if centro_costo:  # Todos los centros de costo
                                        for obj_centro_costo in Centro_Costo.objects.filter(status=1):
                                            saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                         cuenta_detalle, es_result=es_result, id_centro_costo=obj_centro_costo.id)
                                        saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                         cuenta_detalle, es_result=es_result, id_centro_costo=0)
                                    else:
                                        if centro_costos_formset.is_valid():
                                            for obj_centro_costo_formset in centro_costos_formset:
                                                info = obj_centro_costo_formset.cleaned_data
                                                saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                                 cuenta_detalle, es_result=es_result,
                                                                 id_centro_costo=info.get("centro_costo", None))
                                else:
                                    saldos_cta_hijas_pdf(obj_hijos, es_acum, a, anio, mes, tipo, 2, 2,
                                                     cuenta_detalle, es_result=es_result)
                except:
                    pass

                if obj.operacion == "+":
                    col2 += saldo
                else:
                    col2 -= saldo

            elif obj.columna == "3":
                if obj.operacion == "@":
                    col3 += saldo
                elif obj.operacion == "#":
                    col3 -= saldo
                else:
                    pass
            elif obj.columna == "4":
                if obj.operacion == "@":
                    col4 += saldo
                elif obj.operacion == "#":
                    col4 -= saldo
                else:
                    pass
            elif obj.columna == "5":
                if obj.operacion == "@":
                    col5 += saldo
                elif obj.operacion == "#":
                    col5 -= saldo
                else:
                    pass
            else:
                pass
        elif obj.tipo == "1":  # Sumarizar la columna 1
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.caracteristica == "E":
                    a.append(format_html('<h2 style="'+styles['titulo-balance-g']+'">'+convert_vacio_none(obj.descripcion)+'</h2>'))
                elif obj.caracteristica == "S":
                    a.append(format_html('<h4 style="'+styles['titulo-balance']+'">'+convert_vacio_none(obj.descripcion)+'</h4>'))
                else:
                        a.append(convert_vacio_none(obj.descripcion))

                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(convert_cero_none(col1))+'</p>'))
                    a.append("")

                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(convert_cero_none(col1)*-1)+'</p>'))
                    a.append("")

                else:
                    pass
                col1 = 0
            elif obj.columna == "2":  # Presentar en la columna 2
                if obj.caracteristica == "E":
                    a.append(format_html('<h2 style="'+styles['titulo-balance-g']+'">'+convert_vacio_none(obj.descripcion)+'</h2>'))
                elif obj.caracteristica == "S":
                    a.append(format_html('<h4 style="'+styles['titulo-balance']+'">'+convert_vacio_none(obj.descripcion)+'</h4>'))
                else:
                    a.append(convert_vacio_none(obj.descripcion))
                a.append("")

                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+str(col1)+'</p>'))
                    col2 += col1

                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(convert_cero_none(col1)*-1)+'</p>'))
                    col2 -= col1

                else:
                    pass
                col1 = 0
            if obj.columna == "3":  # Sumarizar en la columna 3
                if obj.operacion == "@":
                    col3 += col1

                elif obj.operacion == "#":
                    col3 -= col1

                else:
                    pass
            if obj.columna == "4":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col4 += col1

                elif obj.operacion == "#":
                    col4 -= col1

                else:
                    pass
            if obj.columna == "5":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col5 += col1

                elif obj.operacion == "#":
                    col5 -= col1

                else:
                    pass
            else:
                pass
        elif obj.tipo == "2":  # Sumarizar la columna 2

            if obj.columna == "1":  # Presentar en la columna 1
                if obj.caracteristica == "E":
                    a.append(format_html('<h2 style="'+styles['titulo-balance-g']+'">'+convert_vacio_none(obj.descripcion)+'</h2>'))
                elif obj.caracteristica == "S":
                    a.append(format_html('<h4 style="'+styles['titulo-balance']+'">'+convert_vacio_none(obj.descripcion)+'</h4>'))
                else:
                    a.append(convert_vacio_none(obj.descripcion))

                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(convert_cero_none(col2))+'</p>'))
                    a.append("")
                    col1 += col2

                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(convert_cero_none(col2)*-1)+'</p>'))
                    a.append("")
                    col1 -= col2

                else:
                    pass
                col2 = 0
            elif obj.columna == "2":  # Presentar en la columna 2
                if obj.caracteristica == "E":
                    a.append(format_html('<h2 style="'+styles['titulo-balance-g']+'">'+convert_vacio_none(obj.descripcion)+'</h2>'))
                elif obj.caracteristica == "S":
                    a.append(format_html('<h4 style="'+styles['titulo-balance']+'">'+convert_vacio_none(obj.descripcion)+'</h4>'))
                else:
                    a.append(convert_vacio_none(obj.descripcion))
                a.append("")

                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col2)+'</p>'))

                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col2*-1)+'</p>'))

                else:
                    pass
                col2 = 0
            if obj.columna == "3":  # Sumarizar en la columna 3
                if obj.operacion == "@":
                    col3 += col2

                elif obj.operacion == "#":
                    col3 -= col2

                else:
                    pass
            if obj.columna == "4":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col4 += col2

                elif obj.operacion == "#":
                    col4 -= col2

                else:
                    pass
            if obj.columna == "5":  # Sumarizar en la columna 4
                if obj.operacion == "@":
                    col5 += col2

                elif obj.operacion == "#":
                    col5 -= col2

                else:
                    pass
            else:
                pass
        elif obj.tipo == "3":  # Sumarizar la columna 3
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col3)+'</p>'))
                    col1 += col3
                    col3 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col3*-1)+'</p>'))
                    col1 -= col3
                    col3 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col3)+'</p>'))
                    col3 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col3*-1)+'</p>'))
                    col3 = 0
                    a.append("")
                else:
                    pass
            else:
                pass
        elif obj.tipo == "4":  # Sumarizar la columna 4
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col4)+'</p>'))
                    col1 += col4
                    col4 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col4*-1)+'</p>'))
                    col1 -= col4
                    col4 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col4)+'</p>'))
                    col4 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col4*-1)+'</p>'))
                    col4 = 0
                    a.append("")
                else:
                    pass
            else:
                pass
        elif obj.tipo == "5":  # Sumarizar la columna 5
            a.append(convert_vacio_none(obj.descripcion))
            if obj.columna == "1":  # Presentar en la columna 1
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col5)+'</p>'))
                    col1 += col5
                    col5 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col5*-1)+'</p>'))
                    col1 -= col5
                    col5 = 0
                    a.append("")
                else:
                    pass
            elif obj.columna == "2":  # Presentar en la columna 2
                a.append("")
                if obj.operacion == "+":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col5)+'</p>'))
                    col5 = 0
                    a.append("")
                elif obj.operacion == "-":
                    a.append(format_html('<p style="'+styles['numerico']+'">'+float_con_comas(col5*-1)+'</p>'))
                    col5 = 0
                    a.append("")
                else:
                    pass
            else:
                pass
        else:  # Opciones q no se consideran
            a.append(convert_vacio_none(obj.descripcion))
            a.extend("" for i in range(3))
        if len(a) > 0:
            balance.append(a)
            for obj in cuenta_detalle:
                balance.append(obj)


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def balance_general(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            if buscador.getSelAll():  # Acumulado
                format_balance_general(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 1, True)
            else:
                format_balance_general(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 1, False)
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

    return render_to_response('reporte_estado_financiero/balance_general.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def balance_general_pdf(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0

    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        acumulado = buscador.getSelAll()
        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            format_balance_general_pdf(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 1, acumulado)

            html = render_to_string('reporte_estado_financiero/balance_general_pdf.html',
                                    {'pagesize': 'A4',
                                     "balance": balance,
                                     "mes": mes,
                                     "anio": anio,
                                     "acumulado": acumulado,
                                     "empresa": empresa,
                                     "usuario": usuario,
                                     "request": request}, context_instance=RequestContext(request))
            nombre = ( u'Balance General ' + str(get_mes(mes))+'_'+str(anio))
            return generar_pdf_nombre(html, nombre)
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

    return render_to_response('reporte_estado_financiero/balance_general.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def balance_general_excel(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0
    buscador = BuscadorReportes()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            balance.append([u"", u"", u"BALANCE GENERAL%/%1"])
            balance.append([u""])
            balance.append([u"", u"MES:%/%2", get_mes(mes), u"AÑO:%/%2", anio])
            if buscador.getSelAll():
                balance.append([u"", u"ACUMULADO:%/%2", u"SI"])
                balance.append([u""])
                format_balance_general(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 2, True)
            else:
                balance.append([u"", u"ACUMULADO:%/%2", u"NO"])
                balance.append([u""])
                format_balance_general(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 2, False)

            return ExcelResponse(balance, u'Rpt_bal_gen_'+str(get_mes(mes))+'_'+str(anio))

    return render_to_response('reporte_estado_financiero/balance_general.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))



@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def balance_general_detallado(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            if buscador.getSelAll():
                format_balance_general_detallado(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 1, True)
            else:
                format_balance_general_detallado(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 1, False)
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

    return render_to_response('reporte_estado_financiero/balance_general_detallado.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))

@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def balance_general_detallado_pdf(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0
    acumulado = False
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            # DEBIDO A UN PROBLEMA QUE EL PDF NO PRESENTABA LOS MISMOS VALORES
            # QUE LA PANTALLA CAMBIE EN LA FUNCIÓN DE PDF Y EL BOOLEANO DE LA ÜLTIMA COLUMNA LO HICE COMO UN SWITCH
            # 1 ACUMULADO y 0 NO ACUMULADO MANTENIENDO LA MISMA LÖGICA SÓLO QUE AL PARECER COMO SE TENIA INICIALIZADO
            # EN LA FUNCIÓN 'es_result=True' y desde acá no tomaba en cuenta el valor mandado directo en la función
            # como estaba True en acumulado y False en el otro caso.
            if buscador.getSelAll():
                format_balance_general_detallado_pdf(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4,
                                                     col5, 3, True)
                acumulado = True
            else:
                format_balance_general_detallado_pdf(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4,
                                                     col5, 3, False)
                acumulado = False
        else:

            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

        html = render_to_string('reporte_estado_financiero/balance_general_detallado_pdf.html',
                            {'pagesize': 'A4',
                             "buscador": buscador,
                             "acumulado": acumulado,
                             "balance": balance,
                             "mes": mes,
                             "anio": anio,
                             "empresa": empresa,
                             "usuario": usuario,
                             "request": request}, context_instance=RequestContext(request))
        nombre = ( u'Rpt_bal_gen_detall_'+str(get_mes(mes))+'_'+str(anio))
        return generar_pdf_nombre(html, nombre)

    return render_to_response('reporte_estado_financiero/balance_general_detallado.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def balance_general_detallado_excel(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            balance.append([u"", u"", u"BALANCE GENERAL"+TITULO])
            balance.append([u""])
            balance.append([u"", u"MES:"+SUBTITULO, get_mes(mes)])
            if buscador.getSelAll():
                balance.append([u"", u"ACUMULADO:"+SUBTITULO, u"SI"])
                balance.append([u""])
                format_balance_general_detallado(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 2, True)
            else:
                balance.append([u"", u"ACUMULADO:"+SUBTITULO, u"NO"])
                balance.append([u""])
                format_balance_general_detallado(BalanceGeneral(), balance, anio, mes, col1, col2, col3, col4, col5, 2, False)
            return ExcelResponse(balance, u'Rpt_bal_gen_detall_'+str(get_mes(mes))+'_'+str(anio))

        else:

            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")

            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

    return render_to_response('reporte_estado_financiero/balance_general_detallado.html',
                              {"buscador": buscador,
                               "balance": balance},
                           context_instance=RequestContext(request))

@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_perdidas_ganacias(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()

        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            if buscador.getSelAll():
                format_balance_general(PerdidasGanancias(), balance, anio, mes, col1, col2, col3, col4, col5, 1, True, True)
            else:
                format_balance_general(PerdidasGanancias(), balance, anio, mes, col1, col2, col3, col4, col5, 1, False, True)
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

    return render_to_response('reporte_estado_financiero/perdidas_ganancias.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_perdidas_ganacias_pdf(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0

    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            if buscador.getSelAll():  # Acumulado
                format_balance_general_pdf(PerdidasGanancias(), balance, anio, mes, col1, col2, col3, col4, col5, 1, True)
                acumulado = True
            else:
                format_balance_general_pdf(PerdidasGanancias(), balance, anio, mes, col1, col2, col3, col4, col5, 1, False)
                acumulado = False

            html = render_to_string('reporte_estado_financiero/perdidas_ganancias_pdf.html',
                                    {'pagesize': 'A4',
                                     "buscador": buscador,
                                     "balance": balance,
                                     "mes": mes,
                                     "anio": anio,
                                     "acumulado": acumulado,
                                     "empresa": empresa,
                                     "usuario": usuario,
                                     "request": request}, context_instance=RequestContext(request))
            nombre = ( u'Balance Perdidas y Ganancias' + str(get_mes(mes)) + '_' + str(anio))
            return generar_pdf_nombre(html, nombre)
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

    return render_to_response('reporte_estado_financiero/perdidas_ganancias.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_perdidas_ganancias_excel(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()

        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            balance.append([u"", u"", u"PÉRDIDAS Y GANANCIAS%/%1"])
            balance.append([u""])
            balance.append([u"", u"MES:%/%2", get_mes(mes)])
            balance.append([u"", u"AÑO:%/%2", anio])
            #
            if buscador.getSelAll():
                balance.append([u"", u"ACUMULADO:%/%2", u"SI"])
                balance.append([u""])
                format_balance_general(PerdidasGanancias(), balance, anio, mes, col1, col2, col3, col4, col5, 2, True, es_result=True)
            else:
                balance.append([u"", u"ACUMULADO:%/%2", u"NO"])
                balance.append([u""])
                format_balance_general(PerdidasGanancias(), balance, anio, mes, col1, col2, col3, col4, col5, 2, False, es_result=True)
            return ExcelResponse(balance, u'Rpt_bal_Perd_Gan_'+str(get_mes(mes))+'_'+str(anio))
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")


    return render_to_response('reporte_estado_financiero/perdidas_ganancias.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))

@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_perdidas_ganacias_detallado_9mar16(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    ccosto_formset = formset_factory(ReporteSaldosForm)
    centro_costos = ccosto_formset(prefix='centro_costos')
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0
    cursor = connection.cursor()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        centro_costos = ccosto_formset(request.POST, prefix="centro_costos")
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        por_centro_costo = buscador.por_centro_costo()

        if mes > 0 and anio > 1980:
            try:
                #  Actualiza la tabla de saldo de cuentas
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()

                cursor.execute('SELECT populate_saldo_cuenta_centro_costo(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            if buscador.getSelAll():
                format_balance_general_detallado(PerdidasGanancias(), balance, anio, mes,
                                                 col1, col2, col3, col4, col5, 1, True,
                                                 es_result=True, centro_costo=por_centro_costo,
                                                 perdida_ganancia=True, centro_costos_formset=centro_costos)
            else:
                format_balance_general_detallado(PerdidasGanancias(), balance, anio, mes,
                                                 col1, col2, col3, col4, col5, 1, False,
                                                 es_result=True, centro_costo=por_centro_costo,
                                                 perdida_ganancia=True, centro_costos_formset=centro_costos)
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

    return render_to_response('reporte_estado_financiero/perdidas_ganancias_detallado.html',
                              {"buscador": buscador,
                               "balance": balance,
                               "centro_costos": centro_costos},
                              context_instance=RequestContext(request))

@csrf_exempt
@login_required(login_url="/")
def reporte_perdidas_ganacias_detallado(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})
    #buscador = BuscadorReportes()
    ccosto_formset = formset_factory(ReporteSaldosForm)
    centro_costos = ccosto_formset(prefix='centro_costos')
    micentrocosto = '0'
    mimes = 0
    mianio = 0
    misaldo = ""
    mitipo_rpt = ""
    palabrabanderatodos = ''
    proveed_formset = formset_factory(ReporteSaldosForm)

    r = None




    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        ccosto_formset = formset_factory(ReporteSaldosForm)
        centro_costos = ccosto_formset(request.POST, prefix="centro_costos")
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        centro_costos.is_valid()
        for obj_cc in centro_costos:
            info = obj_cc.cleaned_data
            try:
                c_costo = info.get("centro_costo")#getCentroCosto().id#Centro_Costo.objects.get(id=obj_cc.getCentroCosto().id)
                id_c_costo = c_costo
                if c_costo is not None:
                    pass
                else:
                    id_c_costo = "0"
                    c_costo = "0"
                    mitipo_rpt = "0"
                print str(id_c_costo)+'del centro'+str(c_costo)
                micentrocosto = id_c_costo
                mitipo_rpt = id_c_costo
            except (Centro_Costo.DoesNotExist, ValueError):
                print ('no hay cebytro costps')
                mitipo_rpt = "0"
                pass
        if buscador.getSelAll():
            palabrabanderatodos= "19"

        else:
            palabrabanderatodos = "09"
        misaldo = palabrabanderatodos
        print "elmes"+str(mes)+"elanio"+str(anio)+"elccosto"+mitipo_rpt+"estanchecadostodos"+misaldo



        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        #if None not in(mes, anio):
        if mes > 0 and anio > 1980:
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('FINANCIERO_PYG_DETALLADO') + '.html'
            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)
            '''

            '''
            # Params
            print str(mes)
            print str(anio)
            print misaldo
            print mitipo_rpt
            params = {'mes': mes,
                      'anio': anio,
                      'saldo': misaldo,
                      'tiporpt': int(mitipo_rpt)
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:19000]
            if r.status_code != 200:
                r = None

        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"El mes es requerido")
            if anio <= 0:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"El año es requerido")
    #print (r.content)
    return render_to_response('reporte_estado_financiero/perdidas_ganancias_detallado.html',
                              {"buscador": buscador,
                               "balance": [],
                               "lista": r,
                               "centro_costos": centro_costos},
                              context_instance=RequestContext(request))
#lo mismo pero a xls

@csrf_exempt
@login_required(login_url="/")
def reporte_perdidas_ganacias_detallado_excel(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})
    #buscador = BuscadorReportes()
    ccosto_formset = formset_factory(ReporteSaldosForm)
    centro_costos = ccosto_formset(prefix='centro_costos')
    micentrocosto = '0'
    mimes = 0
    mianio = 0
    misaldo = ""
    mitipo_rpt = ""
    palabrabanderatodos = ''
    proveed_formset = formset_factory(ReporteSaldosForm)

    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        ccosto_formset = formset_factory(ReporteSaldosForm)
        centro_costos = ccosto_formset(request.POST, prefix="centro_costos")
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        centro_costos.is_valid()
        for obj_cc in centro_costos:
            info = obj_cc.cleaned_data
            try:
                c_costo = info.get("centro_costo")#getCentroCosto().id#Centro_Costo.objects.get(id=obj_cc.getCentroCosto().id)
                id_c_costo = c_costo
                if c_costo is not None:
                    pass
                else:
                    id_c_costo = "0"
                    c_costo = "0"
                    mitipo_rpt = "0"
                print str(id_c_costo)+'del centro'+str(c_costo)
                micentrocosto = id_c_costo
                mitipo_rpt = id_c_costo
            except (Centro_Costo.DoesNotExist, ValueError):
                print ('no hay cebytro costps')
                pass
        if buscador.getSelAll():
            palabrabanderatodos= "19"

        else:
            palabrabanderatodos = "09"
        misaldo = palabrabanderatodos
        print "elmes"+str(mes)+"elanio"+str(anio)+"elccosto"+mitipo_rpt+"estanchecadostodos"+misaldo



        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        #if None not in(mes, anio):
        if mes > 0 and anio > 1980:
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('FINANCIERO_PYG_DETALLADO') + '.xls'
            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)
            '''

            '''
            # Params
            print str(mes)
            print str(anio)
            print misaldo
            print mitipo_rpt
            params = {'mes': mes,
                      'anio': anio,
                      'saldo': misaldo,
                      'tiporpt': int(mitipo_rpt)
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1900]
            if r.status_code != 200:
                r = None

        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"El mes es requerido")
            if anio <= 0:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"El año es requerido")
    #print (r.content)

    response = HttpResponse(r.content, content_type='application/excel')
    response['Content-Disposition'] = 'attachment;filename="Per_gan_detall.xls"'
    return response
#lo mismo pero a pdf

@csrf_exempt
@login_required(login_url="/")
def reporte_perdidas_ganacias_detallado_pdf(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})
    #buscador = BuscadorReportes()
    ccosto_formset = formset_factory(ReporteSaldosForm)
    centro_costos = ccosto_formset(prefix='centro_costos')
    micentrocosto = '0'
    mimes = 0
    mianio = 0
    misaldo = ""
    mitipo_rpt = ""
    palabrabanderatodos = ''
    proveed_formset = formset_factory(ReporteSaldosForm)

    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        ccosto_formset = formset_factory(ReporteSaldosForm)
        centro_costos = ccosto_formset(request.POST, prefix="centro_costos")
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        centro_costos.is_valid()
        for obj_cc in centro_costos:
            info = obj_cc.cleaned_data
            try:
                c_costo = info.get("centro_costo")#getCentroCosto().id#Centro_Costo.objects.get(id=obj_cc.getCentroCosto().id)
                id_c_costo = c_costo
                if c_costo is not None:
                    pass
                else:
                    id_c_costo = "0"
                    c_costo = "0"
                    mitipo_rpt = "0"
                print str(id_c_costo)+'del centro'+str(c_costo)
                micentrocosto = id_c_costo
                mitipo_rpt = id_c_costo
            except (Centro_Costo.DoesNotExist, ValueError):
                print ('no hay cebytro costps')
                pass
        if buscador.getSelAll():
            palabrabanderatodos= "19"

        else:
            palabrabanderatodos = "09"
        misaldo = palabrabanderatodos
        print "elmes"+str(mes)+"elanio"+str(anio)+"elccosto"+mitipo_rpt+"estanchecadostodos"+misaldo



        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        #if None not in(mes, anio):
        if mes > 0 and anio > 1980:
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('FINANCIERO_PYG_DETALLADO') + '.pdf'
            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)
            '''

            '''
            # Params
            print str(mes)
            print str(anio)
            print misaldo
            print mitipo_rpt
            params = {'mes': mes,
                      'anio': anio,
                      'saldo': misaldo,
                      'tiporpt': int(mitipo_rpt)
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:190]
            if r.status_code != 200:
                r = None

        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"El mes es requerido")
            if anio <= 0:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"El año es requerido")
    #print (r.content)

    response = HttpResponse(r.content, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Rpt_per_gan_detallado.pdf"'
    return response



@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_perdidas_ganacias_detallado_pdf_9mar16(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes()
    ccosto_formset = formset_factory(ReporteSaldosForm)
    centro_costos = ccosto_formset(prefix='centro_costos')

    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0
    cursor = connection.cursor()
    acumulado = False
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        centro_costos = ccosto_formset(request.POST, prefix="centro_costos")
        mes = buscador.getMes()
        anio = buscador.getAnio()
        por_centro_costo = buscador.por_centro_costo()

        if mes > 0 and anio > 1980:

            try:
                #  Actualiza la tabla de saldo de cuentas
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()

                cursor.execute('SELECT populate_saldo_cuenta_centro_costo(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            if buscador.getSelAll():
                format_balance_general_detallado_pdf(PerdidasGanancias(), balance, anio, mes,
                                                     col1, col2,col3, col4, col5, 1, True, es_result=True,
                                                     centro_costo=por_centro_costo, perdida_ganancia=True,
                                                     centro_costos_formset=centro_costos)
                acumulado = True
            else:
                format_balance_general_detallado_pdf(PerdidasGanancias(), balance, anio, mes,
                                                     col1, col2,col3, col4, col5, 1, False,  es_result=True,
                                                     centro_costo=por_centro_costo, perdida_ganancia=True,
                                                     centro_costos_formset=centro_costos)
                acumulado = False
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

        html = render_to_string('reporte_estado_financiero/perdidas_ganancias_detallado_pdf.html',
                                {'pagesize': 'A4',
                                 "buscador": buscador,
                                 "balance": balance,
                                 "acumulado": acumulado,
                                 "mes": mes,
                                 "anio": anio,
                                 "empresa": empresa,
                                 "usuario": usuario,
                                 "request": request}, context_instance=RequestContext(request))

        nombre = ( u'Rpt_perd_gan_Detall_'+str(get_mes(mes))+'_'+str(anio))
        return generar_pdf_nombre(html, nombre)

    return render_to_response('reporte_estado_financiero/perdidas_ganancias_detallado.html',
                              {"buscador": buscador,
                               "balance": balance,
                               "centro_costos": centro_costos
                               },
                              context_instance=RequestContext(request))

@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_perdidas_ganacias_detallado_excel_9mar16(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    ccosto_formset = formset_factory(ReporteSaldosForm)
    centro_costos = ccosto_formset(prefix='centro_costos')

    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0
    cursor = connection.cursor()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        por_centro_costo = buscador.por_centro_costo()
        centro_costos = ccosto_formset(request.POST, prefix="centro_costos")

        if mes > 0 and anio > 1980:

            try:
                #  Actualiza la tabla de saldo de cuentas
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()

                cursor.execute('SELECT populate_saldo_cuenta_centro_costo(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            balance.append([u"", u"", u"PÉRDIDAS Y GANANCIAS DETALLADO"+TITULO])
            balance.append([u""])
            balance.append([u"", u"MES:"+SUBTITULO, get_mes(mes)])
            balance.append([u"", u"AÑO:"+SUBTITULO, anio])
            if buscador.getSelAll():
                balance.append([u"", u"ACUMULADO:"+SUBTITULO, u"SI"])
                balance.append([u""])
                format_balance_general_detallado(PerdidasGanancias(),
                                                 balance, anio, mes, col1, col2, col3,
                                                 col4, col5, 2, True, es_result=True,
                                                 centro_costo=por_centro_costo, perdida_ganancia=True,
                                                 centro_costos_formset=centro_costos)
            else:
                balance.append([u"", u"ACUMULADO:"+SUBTITULO, u"NO"])
                balance.append([u""])
                format_balance_general_detallado(PerdidasGanancias(),
                                                 balance, anio, mes, col1, col2, col3,
                                                 col4, col5, 2, False, es_result=True,
                                                 centro_costo=por_centro_costo, perdida_ganancia=True,
                                                 centro_costos_formset=centro_costos)

            return ExcelResponse(balance, u'Rpt_perd_gan_Detall_'+str(get_mes(mes))+'_'+str(anio))
        else:
            if mes <= 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Campo Requerido")
            if anio <= 1980:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Campo Requerido")

    return render_to_response('reporte_estado_financiero/perdidas_ganancias_detallado.html',
                              {"buscador": buscador,
                               "balance": balance,
                               "centro_costos": centro_costos},
                              context_instance=RequestContext(request))


# cosas traidas del ultimo clipmaestro activo en la nube

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def balance_general_o_PIG(request):
    print('general o PIG')
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes()
    r = None
    post = False  # flag template message
    variableselecciontodos = 0
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        pigonopig = buscador.getTipoReportePIG()

        if pigonopig==1:
            print('es de tipo 1 no es PIG')
        else:
            print('es PIG')
        if buscador.getSelAll():
            print 'la cosa es mensual'
            variableselecciontodos = 1
        else:
            variableselecciontodos= 0
        print('la variable de seleccion mensual es')+str(variableselecciontodos)

        #buscador = BuscadorReportes(request.POST)
        post = True
        tiporpt = 0
        urljasper = get_url_jasper()
        if buscador.is_valid():
            print 'elresumidoodetallado---'
            print 'elresumidoodetallado'+str(buscador.getTipoReporte())
            tiporpt= buscador.getTipoReporte()
            print '***elresumidoodetallado'
            report = ''
            if anio is not None and mes is not None:
                print 'los parametros son no noneeeee'
                if pigonopig == 2:
                    print 'resumido vencido'

                    print (mes)
                    print (anio)
                    print('es de tipo 2 si es PIG perdidaganancias')
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    if tiporpt == 1:
                        print('en tiporeporte es resumido y uso el codigo de 3 en el params')
                        report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO') + '.html'
                    if tiporpt == 2:
                        print('en tiporeporte es detallado y uso el codigo de 4 en el params')
                        report = get_nombre_reporte('FINANCIERO_PYG_DETALLADO') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'mes': mes,
                              'anio': anio,
                              'saldo': variableselecciontodos,
                              'tiporpt': tiporpt
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                    #r = s.get(url=url+report, auth=auth, params=params)

                if pigonopig == 1:
                    #estos 2 deben ser los detallados, no los resumidos
                    print('es de tipo 1 no es PIG es balance general')
                    print('debe ser resumido o detallado depende del combo 1  es resumido')
                    print (mes)
                    print (anio)

                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    if tiporpt == 1:
                        print('en tiporeporte es resumido y uso el codigo de 3 en el params')
                        report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO_BG') + '.html'
                    if tiporpt == 2:
                        print('en tiporeporte es detallado y uso el codigo de 4 en el params')
                        report = get_nombre_reporte('FINANCIERO_BG_DETALLADO') + '.html'
                    # report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO_BG') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params de detallados usan 4 cosas
                    params = {'mes': mes,
                              'anio': anio,
                              'saldo': variableselecciontodos,
                              'tiporpt': tiporpt
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                r = s.get(url=url+report, auth=auth, params=params)
                print('el reoprte es de ')+str(report)
        else:
            print 'el buscador esta mal'
            errors = buscador._errors.setdefault("estado_reporte", ErrorList())
            errors.append(u"La fecha inicial es requerida")
    else:
        print 'algo pasa y no es post'
    return render_to_response('reporte_estado_financiero/balance_general_o_PIG.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))



@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def balance_general_o_PIG_excel(request):
    print('general o PIG')
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes()
    r = None
    tiporpt = 0
    nombrereporte = ''
    report = ''
    post = False  # flag template message
    variableselecciontodos = 0
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        pigonopig = buscador.getTipoReportePIG()

        if pigonopig==1:
            print('es de tipo 1 no es PIG')
        else:
            print('es PIG')
        if buscador.getSelAll():
            print 'la cosa es mensual'
            variableselecciontodos = 1
        else:
            variableselecciontodos= 0
        print('la variable de seleccion mensual es')+str(variableselecciontodos)

        #buscador = BuscadorReportes(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            print 'elresumidoodetallado---'
            print 'elresumidoodetallado'+str(buscador.getTipoReporte())
            tiporpt= buscador.getTipoReporte()
            print '***elresumidoodetallado'
            print '***elresumidoodetallado'
            if anio is not None and mes is not None:
                print 'los parametros son no noneeeee'
                # print 'elresumidoodetallado'+str(buscador.getTipoReporte())
                print 'elresumidoodetallado1565455454'
                if pigonopig == 2:
                    print 'resumido vencido'

                    print (mes)
                    print (anio)
                    print('es de tipo 2 si es PIG')
                    nombrereporte = 'Perdidas_ganancias'
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    if tiporpt == 1:
                        print('en tiporeporte es resumido y uso el codigo de 3 en el params')
                        report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO') + '.html'
                    if tiporpt == 2:
                        print('en tiporeporte es detallado y uso el codigo de 4 en el params')
                        report = get_nombre_reporte('FINANCIERO_PYG_DETALLADO') + '.html'
                    # report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'mes': mes,
                              'anio': anio,
                              'saldo': variableselecciontodos,
                              'tiporpt': tiporpt
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                    #r = s.get(url=url+report, auth=auth, params=params)

                if pigonopig == 1:
                    print 'resumido por vencer'#estos 2 deben ser los detallados, no los resumidos
                    print('es de tipo 1 no es PIGddd')
                    print (mes)
                    print (anio)

                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    if tiporpt == 1:
                        print('en tiporeporte es resumido y uso el codigo de 3 en el params')
                        report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO_BG') + '.html'
                        nombrereporte = 'Balance_General'
                    if tiporpt == 2:
                        print('en tiporeporte es detallado y uso el codigo de 4 en el params')
                        report = get_nombre_reporte('FINANCIERO_BG_DETALLADO') + '.html'
                        nombrereporte = 'Balance_General_detallado'
                    # report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO_BG') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'

                    # Params
                    params = {'mes': mes,
                              'anio': anio,
                              'saldo': variableselecciontodos,
                              'tiporpt': tiporpt
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                r = s.get(url=url+report, auth=auth, params=params)
                print('el reoprte es de ')+str(report)
        else:
            print 'el buscador esta mal'
            errors = buscador._errors.setdefault("estado_reporte", ErrorList())
            errors.append(u"La fecha inicial es requerida")
    else:
        print 'algo pasa y no es post'
    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
    nombre = nombrereporte+str(anio)+'_'+str(mes)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    return response




@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def balance_general_o_PIG_pdf(request):
    print('general o PIG')
    ahora = datetime.datetime.now()
    hora = ahora.hour
    minuto = ahora.minute
    seg = ahora.second
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes()
    r = None
    tiporpt = 0
    nombrereporte = ''
    report = ''
    post = False  # flag template message
    variableselecciontodos = 0
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        pigonopig = buscador.getTipoReportePIG()

        if pigonopig==1:
            print('es de tipo 1 no es PIG')
        else:
            print('es PIG')
        if buscador.getSelAll():
            print 'la cosa es mensual'
            variableselecciontodos = 1
        else:
            variableselecciontodos= 0
        print('la variable de seleccion mensual es')+str(variableselecciontodos)

        #buscador = BuscadorReportes(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            print 'elresumidoodetallado---'
            print 'elresumidoodetallado'+str(buscador.getTipoReporte())
            tiporpt= buscador.getTipoReporte()
            print '***elresumidoodetallado'
            print '***elresumidoodetallado'
            if anio is not None and mes is not None:
                print 'los parametros son no noneeeee'
                # print 'elresumidoodetallado'+str(buscador.getTipoReporte())
                print 'elresumidoodetallado1565455454'
                if pigonopig == 2:
                    print 'resumido vencido'

                    print (mes)
                    print (anio)
                    print('es de tipo 2 si es PIG')
                    nombrereporte = 'Perdidas_ganancias'
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    if tiporpt == 1:
                        print('en tiporeporte es resumido y uso el codigo de 3 en el params')
                        report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO') + '.pdf'
                    if tiporpt == 2:
                        print('en tiporeporte es detallado y uso el codigo de 4 en el params')
                        report = get_nombre_reporte('FINANCIERO_PYG_DETALLADO') + '.pdf'
                    # report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'mes': mes,
                              'anio': anio,
                              'saldo': variableselecciontodos,
                              'tiporpt': tiporpt
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                    #r = s.get(url=url+report, auth=auth, params=params)

                if pigonopig == 1:
                    print 'resumido por vencer'#estos 2 deben ser los detallados, no los resumidos
                    print('es de tipo 1 no es PIGddd')
                    print (mes)
                    print (anio)

                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    if tiporpt == 1:
                        print('en tiporeporte es resumido y uso el codigo de 3 en el params')
                        report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO_BG') + '.pdf'
                        nombrereporte = 'Balance_General'
                    if tiporpt == 2:
                        print('en tiporeporte es detallado y uso el codigo de 4 en el params')
                        report = get_nombre_reporte('FINANCIERO_BG_DETALLADO') + '.pdf'
                        nombrereporte = 'Balance_General_detallado'
                    # report = get_nombre_reporte('FINANCIERO_ESTADO_FINANCIERO_BG') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'

                    # Params
                    params = {'mes': mes,
                              'anio': anio,
                              'saldo': variableselecciontodos,
                              'tiporpt': tiporpt
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                r = s.get(url=url+report, auth=auth, params=params)
                print('el reoprte es de ')+str(report)
        else:
            print 'el buscador esta mal'
            errors = buscador._errors.setdefault("estado_reporte", ErrorList())
            errors.append(u"La fecha inicial es requerida")
    else:
        print 'algo pasa y no es post'
    response = HttpResponse(r.content, content_type='application/pdf')
    nombre = nombrereporte+str(anio)+'_'+str(mes)+'_'+str(get_horadescarga())
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".pdf"'
    return response



@csrf_exempt
@login_required(login_url="/")
#@permiso_accion(mensaje=mensaje_permiso)
def comparativo_estado_resultado(request):
    """
    reporte financiero
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    balance = []
    col1 = 0.0
    col2 = 0.0
    col3 = 0.0
    col4 = 0.0
    col5 = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        if mes > 0 and anio > 1980:

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            if buscador.getSelAll():  # Acumulado
                format_balance_general(ComparativoEstadoResultado(), balance, anio, mes, col1, col2, col3, col4, col5, 1, True)
            else:
                format_balance_general(ComparativoEstadoResultado(), balance, anio, mes, col1, col2, col3, col4, col5, 1, False)

    return render_to_response('reporte_estado_financiero/comparativo_estado_resultado.html',
                              {"buscador": buscador,
                               "balance": balance},
                              context_instance=RequestContext(request))
