#! /usr/bin/python
# -*- coding: UTF-8 -*-
import clipmaestro
__author__ = 'Clip Maestro'
from django import template
from contabilidad.models import *
from django.core.exceptions import PermissionDenied
from administracion.models import *
from django.http import HttpResponse
from django.core.urlresolvers import reverse
from django.http import *
from django.contrib.auth.decorators import permission_required
from django.contrib import messages
from django.core.urlresolvers import resolve
import json


mensaje_permiso = u"Usted no tiene permisos para realizar la acción requerida, " \
                     u"por favor si tiene alguna, comuníquese con el administrador"

def tiene_permiso(request):
    """
    Función que revisa si el usuario tiene o no permiso en
    una aplicación determinada, identificada con "perm" el
    cual es el parámetro con que se identifica la aplicación
    :param request:
    :param perm:
    :return:
    """

    if request.user.is_active and request.user.is_superuser:
        return True
    else:
        persona = Persona.objects.get(id=request.user.id)
        # Si es administrador de sistemas de la compañía
        if persona.es_admin:
            return True

        grupo = persona.grupo.all()
        try:
            match = resolve(request.path)
            permiso = Permisos.objects.get(etiqueta=str(match.url_name).replace(" ", ""))
        except Permisos.DoesNotExist:
            return False

        if Persona.objects.filter(id=persona.id, permisos=permiso).exists():
            return True
        else:
            for obj in grupo:
                if Grupo.objects.filter(id=obj.id, permisos=permiso).exists():
                    return True

        return False  # Retorna False si no encontró permisos al usuario


def tiene_permiso_tag(request, urlname):
    """
    Función que revisa si el usuario tiene o no permiso en
    el template para mostrar o ocultar opciones
    :param request:
    :param perm:
    :return:
    """

    if request.user.is_active and request.user.is_superuser:
        return True
    else:
        persona = Persona.objects.get(id=request.user.id)
        grupo = persona.grupo.all()
        try:
            permiso = Permisos.objects.get(etiqueta=urlname)
        except Permisos.DoesNotExist:
            return False

        if Persona.objects.filter(id=persona.id, permisos=permiso).exists():
            return True
        else:
            for obj in grupo:
                if Grupo.objects.filter(id=obj.id, permisos=permiso).exists():
                    return True

        return False  # Retorna False si no encontró permisos al usuario


def tiene_permiso_tag_menu(request, codigo_app):
    """
    Función que revisa si el usuario tiene o no permiso en
    el template para mostrar o ocultar opciones
    :param request:
    :param perm:
    :return:
    """

    if request.user.is_active and request.user.is_superuser:
        return True
    else:
        persona = Persona.objects.get(id=request.user.id)
        grupo = persona.grupo.all()
        try:
            aplicacion_query = Aplicacion.objects.filter(codigo=codigo_app)
            permiso = Permisos.objects.filter(aplicacion__in=aplicacion_query)
        except Permisos.DoesNotExist:
            return False

        if Persona.objects.filter(id=persona.id, permisos__in=permiso).exists():
            return True
        else:
            for obj in grupo:
                if Grupo.objects.filter(id=obj.id, permisos__in=permiso).exists():
                    return True

        return False  # Retorna False si no encontró permisos al usuario


def tiene_permiso_tag_submenu(request, urlname):
    """
    Función que revisa si el usuario tiene o no permiso en
    el template para mostrar o ocultar opciones
    :param request:
    :param perm:
    :return:
    """

    if request.user.is_active and request.user.is_superuser:
        return True
    else:
        persona = Persona.objects.get(id=request.user.id)
        grupo = persona.grupo.all()
        try:
            cualquier_permiso = Permisos.objects.get(etiqueta=urlname)
            aplicacion = Aplicacion.objects.get(id=cualquier_permiso.aplicacion_id)
        except (Permisos.DoesNotExist, Aplicacion.DoesNotExist):
            return False

        if Persona.objects.filter(id=persona.id, permisos__in=aplicacion.get_permisos()).exists():
            return True
        else:
            for obj in grupo:
                if Grupo.objects.filter(id=obj.id, permisos__in=aplicacion.get_permisos()).exists():
                    return True

        return False  # Retorna False si no encontró permisos al usuario


def permiso_accion(mensaje=None, function=None, home_url=None, es_ajax=False, es_admin=False):
    """
    Revisa si el usuario tiene o no permiso en el sistema
    :param mensaje: "mensaje que le saldrá al usuario cuando no tenga permiso"
    :param function:
    :param home_url:
    :param es_ajax:
    :param es_admin:
    :return:
    """
    if home_url is None:
        home_url = "administracion"

    def _dec(view_func):
        def _view(request, *args, **kwargs):
            if request.user.is_authenticated():
                if not es_admin:
                    if tiene_permiso(request):
                        return view_func(request, *args, **kwargs)
                    else:
                        if mensaje is not None:
                            messages.error(request, mensaje)

                        if not es_ajax:
                            return HttpResponseRedirect(reverse(home_url))
                        else:
                            respuesta = {"status": 5, "mensaje": mensaje}
                            resultado = json.dumps(respuesta)
                            return HttpResponse(resultado, mimetype='application/json')
                else:
                    if request.user.persona.es_admin:
                        return view_func(request, *args, **kwargs)
                    else:
                        if mensaje is not None:
                            messages.error(request, mensaje)
                        return HttpResponseRedirect(reverse(home_url))
            else:
                return HttpResponseRedirect(reverse(home_url))

        _view.__name__ = view_func.__name__
        _view.__dict__ = view_func.__dict__
        _view.__doc__ = view_func.__doc__

        return _view

    if function is None:
        return _dec
    else:
        return _dec(function)
