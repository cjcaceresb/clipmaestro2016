# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
import math
import copy

def convert_cero_none(obj):
    """
    Retorna cero si el elemento que le envian es None
    caso contrario retorna el mismo elemento
    :param obj:
    :return:
    """
    if obj is None:
        return 0.00
    else:
        return obj


def redondeo(numero, precision=2):
    """
    Funcion de redondeo que reemplaza a la de python por problemas
    al redondear
    :param numero:
    :param precision:
    :return:
    """
    multiplicador = math.pow(10, precision + 1)
    num_multi = math.floor(numero * multiplicador)
    num_redondeado = round(num_multi/10) * 10 / multiplicador
    if num_redondeado == 0:
        return 0
    else:
        return num_redondeado


def convert_vacio_none(obj):
    """
    Retorna " " si el elemento que le envian es None
    caso contrario retorna el mismo elemento
    :param obj:
    :return:
    """
    if obj is None:
        return ""
    else:
        return obj


def get_next_id(instance):
    """
    Compara el último id de la tabla con la secuencia actual, y
    si la secuencia es menor, lo actualiza
    :param instance:
    :return:
    """
    from django.db.models import Max
    from django.db import connection

    if instance.__class__.objects.all().aggregate(Max("id"))['id__max'] is None:
        id_max = int(convert_cero_none(instance.__class__.objects.all().aggregate(Max("id"))['id__max'])+1)
    else:
        id_max = int(instance.__class__.objects.all().aggregate(Max("id"))['id__max'])

    cursor = connection.cursor()
    query = "SELECT nextval('{0}_id_seq'::regclass)".format(instance._meta.db_table)
    cursor.execute(query)
    row = cursor.fetchone()
    id = int(row[0])

    if id_max > id:
        query = "SELECT setval('{0}_id_seq', {1}, true);".format(instance._meta.db_table, id_max + 1)
        cursor.execute(query)
        instance.id = id_max + 1
    else:
        instance.id = id

    cursor.close()


def copiar_modelo(class_model):
    """
    Función para copiar modelo
    se le pone id = None para que coja otro id automaticamente
    :param class_model:
    :return: class_model
    """
    new_model = copy.copy(class_model)
    new_model.id = None
    return new_model
