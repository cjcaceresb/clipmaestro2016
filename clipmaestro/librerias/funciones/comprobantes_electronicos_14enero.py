#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from xml.etree import ElementTree
import xml.etree.cElementTree as ET
from xml.dom import minidom
from xml.etree.ElementTree import Element, SubElement
from librerias.funciones.funciones_vistas import get_apikey
from administracion.models import Empresa
from librerias.funciones.funciones_modelos import *
from librerias.funciones.funciones_vistas import *
from contabilidad.models import *
import requests
import json
from django.contrib import messages
import time
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
import ssl
from requests.adapters import HTTPAdapter
from urllib3.poolmanager import PoolManager
from django.conf import settings
from contabilidad.views import get_ride_factura_pdf, get_ride_retencion_electronica, get_ride_nota_credito_pdf
from contabilidad.formularios.ComprasForm import *
from contabilidad.formularios.VentaForm import FormEnvioCorreoFactura
from contabilidad.formularios.NotaCreditoForm import FormEnvioCorreoNotaCredito
from django.utils import datastructures

#DELIMITADOR = "!=.$"

class TlSv1HttpAdapter(HTTPAdapter):
    """"Transport adapter" that allows us to use SSLv3."""

    def init_poolmanager(self, connections, maxsize, block=False):
        self.poolmanager = PoolManager(num_pools=connections,
                                       maxsize=maxsize,
                                       block=block,
                                       ssl_version=ssl.PROTOCOL_TLSv1)
def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

"""
Tipo de emision:
1: Emisión Normal
2: Emisión por indisponibilidad del sistema
"""
"""
Codigo de Impuestos:
2: IVA
3: ICE

Tarifa de Iva:
0%: 0
12%: 2
No objeto: 6
"""
def facturacion_electronica(venta_cabecera, empresa, tipo_emision):
    """
    Funcion que me crea el xml para la factura electrónica
    :param venta_cabecera:
    :param empresa:
    :param tipo_emision:
    :return:
    """
    top = Element('factura', {"id": "comprobante", "version": "1.0.0"})
    ##############################
    #   INFORMACIÓN TRIBUTARIA   #
    ##############################
    info_tributaria = SubElement(top, 'infoTributaria')

    ambiente = SubElement(info_tributaria, 'ambiente')
    ambiente.text = '1'  # Prueba
    tipo_emi = SubElement(info_tributaria, 'tipoEmision')
    tipo_emi.text = "1"
    razon_social = SubElement(info_tributaria, 'razonSocial')
    razon_social.text = unicode(empresa.razon_social)
    nombre_comercial = SubElement(info_tributaria, 'nombreComercial')
    nombre_comercial.text = unicode(empresa.nombre_comercial)
    ruc = SubElement(info_tributaria, 'ruc')
    ruc.text = empresa.rucci
    clave_acceso = SubElement(info_tributaria, 'claveAcceso')
    clave_acceso.text = "1214354213854531"  # Clave de acceso
    cod_doc = SubElement(info_tributaria, 'codDoc')
    cod_doc.text = venta_cabecera.documento.codigo_documento
    estab = SubElement(info_tributaria, 'estab')
    estab.text = str(venta_cabecera.vigencia_doc_empresa.serie).split("-")[0]
    pto_emi = SubElement(info_tributaria, 'ptoEmi')
    pto_emi.text = str(venta_cabecera.vigencia_doc_empresa.serie).split("-")[1]
    secuencia = SubElement(info_tributaria, 'secuencia')
    secuencia.text = str(venta_cabecera.num_documento)
    dir_matriz = SubElement(info_tributaria, 'dirMatriz')
    dir_matriz.text = unicode(empresa.direccion)

    ################################
    #  INFORMACIÓN DE LA FACTURA   #
    ################################
    info_factura = SubElement(top, 'infoFactura')

    fecha_emision = SubElement(info_tributaria, 'fechaEmision')
    fecha_emision.text = venta_cabecera.fecha.strftime("%d/%m/%Y")
    dir_estableci = SubElement(info_tributaria, 'dirEstablecimiento')
    dir_estableci.text = "NN"
    contri_esp = SubElement(info_factura, 'contribuyenteEspecial')
    contri_esp.text = '001'
    oblig_cont = SubElement(info_factura, 'obligadoContabilidad')
    oblig_cont.text = "SI"
    tipo_ide_comp = SubElement(info_factura, 'tipoIdentificacionComprador')
    tipo_ide_comp.text = venta_cabecera.cliente.tipo_identificacion.descripcion
    guia_remi = SubElement(info_factura, 'guiaRemision')
    guia_remi.text = ""
    razon_soc_compr = SubElement(info_factura, 'razonSocialComprador')
    razon_soc_compr.text = unicode(venta_cabecera.cliente.razon_social)
    ident_compr = SubElement(info_factura, 'identificacionComprador')
    ident_compr.text = venta_cabecera.cliente.ruc
    total_sin_imp = SubElement(info_factura, 'totalSinImpuesto')
    total_sin_imp.text = "%.2f" % venta_cabecera.subtotal_tarifa0
    total_desc = SubElement(info_factura, 'totalDescuento')
    total_desc.text = "%.2f" % venta_cabecera.getDescuentoTotal()

    ############################
    #    TOTAL CON IMPUESTO    #
    ############################
    total_con_impuesto = SubElement(info_factura, 'totalConImpuesto')

    ####### ICE #########
    total_impuesto = SubElement(total_con_impuesto, 'totalImpuesto')
    codigo = SubElement(total_impuesto, 'codigo')
    codigo.text = "3"
    codigo_porc = SubElement(total_impuesto, 'codigoPorcentaje')
    codigo_porc.text = "0.00"
    base_imp = SubElement(total_impuesto, 'baseImponible')
    base_imp.text = "0.00"
    valor = SubElement(total_impuesto, 'valor')
    valor.text = "0.00"

    ####### IVA #######
    total_impuesto = SubElement(total_con_impuesto, 'totalImpuesto')
    codigo = SubElement(total_impuesto, 'codigo')
    codigo.text = "2"
    codigo_porc = SubElement(total_impuesto, 'codigoPorcentaje')
    codigo_porc.text = "0.00"
    base_imp = SubElement(total_impuesto, 'baseImponible')
    base_imp.text = "0.00"
    valor = SubElement(total_impuesto, 'valor')
    valor.text = "0.00"

    ########## DATOS ADICIONALES ##########
    propina = SubElement(info_factura, 'propina')
    propina.text = "0.00"
    importe_total = SubElement(info_factura, 'importeTotal')
    importe_total.text = "0.00"
    moneda = SubElement(info_factura, 'moneda')
    moneda.text = "DOLAR"

    ############### DETALLE DE LA VENTA ##############
    detalles = SubElement(top, 'detalles')
    #for obj in venta_cabecera.getDetalleVenta():
    for obj in Venta_Detalle.objects.filter(venta_cabecera=venta_cabecera):
        detalle = SubElement(detalles, 'detalle')
        codigo_princ = SubElement(detalle, 'codigoPrincipal')
        codigo_princ.text = obj.item.codigo
        codigo_aux = SubElement(detalle, 'codigoAuxiliar')
        codigo_aux.text = obj.item.codigo
        descripcion = SubElement(detalle, "descripcion")
        descripcion.text = unicode(obj.item.nombre)
        cantidad = SubElement(detalle, "cantidad")
        cantidad.text = "%.2f" % obj.cantidad
        p_unitario = SubElement(detalle, "precioUnitario")
        p_unitario.text = "%.2f" % obj.precio_unitario
        descuento = SubElement(detalle, "descuento")
        descuento.text = obj.descuento_valor
        prec_tot_sin_impuest = SubElement(detalle, "precioTotalSinImpuesto")
        prec_tot_sin_impuest.text = "%.2f" % ((obj.precio_unitario * obj.cantidad) - obj.descuento_valor)

        ########## DETALLES ADICIONALES ###########
        detalles_adicionales = SubElement(detalle, "detallesAdicionales")
        detalles_adicionales.text = ""

        ######### IMPUESTOS AL ITEM ##########
        impuestos = SubElement(detalle, "impuestos")
        impuesto = SubElement(impuestos, "impuesto")

        ################# ICE ################
        codigo = SubElement(impuesto, "codigo")
        codigo.text = "3"
        codigo_porc = SubElement(impuesto, "codigoPorcentaje")
        codigo_porc.text = "1245"
        tarifa = SubElement(impuesto, "tarifa")
        tarifa.text = "5"  # Porcentje del impuesto
        base_imp = SubElement(impuesto, "baseImponible")
        base_imp.text = "%.2f" % ((obj.precio_unitario * obj.cantidad) - obj.descuento_valor)
        valor = SubElement(impuesto, "valor")
        valor.text = "%.2f" % (((obj.precio_unitario * obj.cantidad) - obj.descuento_valor) * 0.05)  # porc
        ################ IVA #################
        ################# ICE ################
        codigo = SubElement(impuesto, "codigo")
        codigo.text = "2"
        codigo_porc = SubElement(impuesto, "codigoPorcentaje")
        codigo_porc.text = str(obj.item.iva)
        tarifa = SubElement(impuesto, "tarifa")
        tarifa.text = "5"  # Porcentje del impuesto
        base_imp = SubElement(impuesto, "baseImponible")
        base_imp.text = "%.2f" % ((obj.precio_unitario * obj.cantidad) - obj.descuento_valor)
        valor = SubElement(impuesto, "valor")
        valor.text = "%.2f" % (((obj.precio_unitario * obj.cantidad) - obj.descuento_valor) * 0.05)  # porc

    print prettify(top)
    tree = ET.ElementTree(top)
    tree.write("mixml.xml")
    #return HttpResponse(open('mixml.xml').read())
    #return HttpResponse(open('mixml.xml').read(), content_type='text/xml')

def datos_json_retencion(retencion, apikey):
    items = []
    tipo_doc = retencion.compra_cabecera.documento.codigo_documento

    for obj in RetencionCompraDetalle.objects.filter(retencion_cabecera_compra=retencion, status=1):
        if obj.codigo_sri.porcentaje == 30:
            codigo = u'1'
        elif obj.codigo_sri.porcentaje == 70:
            codigo = u'2'
        elif obj.codigo_sri.porcentaje == 100:
            codigo = u'3'
        else:
            codigo = obj.codigo_sri.codigo

        if obj.tipo_iva:
            codigo_impuesto = u"2"
        else:
            codigo_impuesto = u"1"

        item = {'codigo': codigo, 'codigo_impuesto': codigo_impuesto,
                'baseimponible': obj.base, 'tipodocumento': tipo_doc,
                'numerodocumento': retencion.compra_cabecera.num_doc,
                'fechadocumento': retencion.compra_cabecera.fecha_emi.strftime("%d-%m-%Y"), }

        items.append(item)

    return {
            "apikey": apikey,
            "codigoestablecimiento": retencion.vigencia_doc_empresa.serie.split("-")[0],
            "codigopuntoventa": retencion.vigencia_doc_empresa.serie.split("-")[1],
            "secuencia": retencion.numero_ret,
            "mesfiscal": retencion.fecha_emi.month,
            "annofiscal": retencion.fecha_emi.year,
            "fecha": retencion.fecha_emi.strftime("%d-%m-%Y"),
            "sujeto": {"tipoidentificacion": tipo_identificacion_datilmedia(retencion.compra_cabecera.proveedor.tipo_identificacion),
                        "identificacion": retencion.compra_cabecera.proveedor.ruc,
                        "razonsocial": retencion.compra_cabecera.proveedor.razon_social,
                        "correo": retencion.compra_cabecera.proveedor.email,
                        "direccion": convert_vacio_none(retencion.compra_cabecera.proveedor.direccion),
                        "telefono": convert_vacio_none(retencion.compra_cabecera.proveedor.telefono)},
            "items": items,
            "adicionales": [""]
        }


def enviar_doc_electronico_api(data_json, url, put=False):
    """
    Envia los datos de los documentos electrónicos a datilmedia
    para posteriormente firmarlo y enviarlos al SRI
    :param data_json: diccionario que tiene los datos a enviar
    :param url: direccion "url" a la cual va a ser enviado el documento,
                varia dependiendo del documento,
                ej: para retención = "http://factora.ec/api/retencion"
    :return:
    """
    b = requests.Session()
    b.mount('https://app.datil.co', TlSv1HttpAdapter())

    try:
        print "----------------url-------------------------"
        print url
        print "----------------data=json.dumps(data_json)-------------------------"
        print json.dumps(data_json)
        if put:
            a = requests.put(url, data=json.dumps(data_json))
            print 'existe put'
        else:
            a = requests.post(url, data=json.dumps(data_json))
            print 'existe post'

        print "-------------aaaaaaaaaaaaaaaa primer cambio xde destados-----------------"
        print str(a.status_code)
        print "-------------aaaaaaaaaaaaaaaa-----------------"

        if int(a.status_code) == 200:
            variables = a.json()
            weburl = variables.get('weburl', '')
            result = variables.get('result')
            error = variables.get('error', '')
        else:
            weburl = ''
            result = 'bad'
            error = u'Error de conexión en el proceso de guardar el documento, por favor intente nuevamente'

        return result, weburl, error

    except requests.ConnectionError:
        weburl = ""
        result = "bad"
        error = u"Error Fatal: Error de conexión intente más tarde"
        return result, weburl, error
    except ValueError:
        weburl = ""
        result = "bad"
        error = u"Error Fatal: Error de respuesta del SRI"
        return result, weburl, error


def firmar_doc_electronico_api(weburl, url):
    """
    Función que sirve para firmar el documento con codigo weburl, enviado anteriormente
    a datilmedia.
    :param weburl:  Código del documento guardado en datilmedia
    :param url:  dirección "url" del recurso
    :return:
    """
    #  Firmar documento
    #  Password API NRMASTER : Rmaster1090

    dict_firma = {"apikey": get_apikey(), "weburl": weburl, "password": get_passwordapi(),}

    try:
        a = requests.post(url, data=json.dumps(dict_firma), verify=False)
        if int(a.status_code) == 200:
            variables = a.json()
            weburl = variables.get('weburl', '')
            result = variables.get('result')
            error = variables.get('error', '')
        else:
            weburl = ''
            result = 'bad'
            error = u'Error de conexión en el proceso de firma del documento, por favor intente nuevamente'
        return result, weburl, error

    except requests.ConnectionError:
        weburl = ""
        result = "bad"
        error = u"Error Fatal: Error de conexión intente más tarde"
        return result, weburl, error

    except ValueError:
        weburl = ""
        result = "bad"
        error = u"Error Fatal: Error de respuesta del SRI"
        return result, weburl, error


def envio_sri_api(weburl):
    """
    Función para enviar el documento electrónico ya firmado al SRI
    :param weburl: código del documento guardado en datilmedia
    :return:
    """
    url_envio_sri = 'https://app.datil.co/api/enviosri'

    dict_envio = {"apikey": get_apikey(),  "weburl": weburl, }
    print(str(dict_envio))
    print('esto era el dictenvio en el novel2')
    b = requests.Session()
    b.mount('https://app.datil.co', TlSv1HttpAdapter())
    try:
        a = requests.post(url_envio_sri, data=json.dumps(dict_envio), verify=False)
        print str(a)
        print('esto es lo que se crea en el sri')
        print str(a.status_code)
        print 'esto fue el statuscode de a alm irse al sri'
        if int(a.status_code) == 200:
            variables = a.json()
            weburl = variables.get('weburl', '')
            result = variables.get('result')
            error = variables.get('error', '')
        else:
            weburl = ''
            result = 'bad'
            error = u'Error de conexión en el proceso de envio al SRI, por favor intente nuevamente'

        return result, weburl, error
    except requests.ConnectionError:
        print 'del cambio de estado se cae en except cuando era dos y esta en el sri :o'
        weburl = ""
        result = "bad"
        print 'se cae en el except cuando trata de cambiar el status en el sri'
        error = u"Error Fatal: Error de conexión intente más tarde"
        return result, weburl, error
    except ValueError:
        weburl = ""
        result = "bad"
        error = u"Error Fatal: Error de respuesta del SRI"
        return result, weburl, error


def autorizacion_sri_api(weburl):
    """
    Función para pedir autorización al SRI mediante el API de datilmedia
    :param weburl:
    :return:
    """
    url_autorizacion = 'https://app.datil.co/api/autorizacionsri'
    dict_auto = {"apikey": get_apikey(), "weburl": weburl, }
    b = requests.Session()
    b.mount('https://app.datil.co', TlSv1HttpAdapter())

    try:
        a = requests.post(url_autorizacion, data=json.dumps(dict_auto), verify=False)
        if int(a.status_code) == 200:
            variables = a.json()
            autorizado = variables.get('autorizado', '')
            estado = variables.get('estado', '')
            result = variables.get('result', '')
            error = variables.get('mensajes', '')
            autorizacion = variables.get('autorizacion', '')
        else:
            autorizado = False
            error = "Error General"
            result = "bad"
            estado = ""
            autorizacion = ""

        return autorizado, estado, result, error, autorizacion

    except requests.ConnectionError:
        autorizado = False
        error = "Error General"
        result = "bad"
        estado = ""
        autorizacion = ""
        return autorizado, estado, result, error, autorizacion

    except ValueError:
        autorizado = False
        error = u"Error Fatal: Error de respuesta del SRI"
        result = "bad"
        estado = ""
        autorizacion = ""
        return autorizado, estado, result, error, autorizacion


def enviar_correo_doc_electronico(weburl):
    """
     Envía un mail al cliente/proveedor con el documento electrónico
     adjunto
    :param weburl:
    :return:
    """
    url_correo = 'https://app.datil.co/api/enviocorreo'
    dict_auto = {"apikey": get_apikey(), "weburl": weburl, }

    b = requests.Session()
    b.mount('https://app.datil.co', TlSv1HttpAdapter())

    a = requests.post(url_correo, data=json.dumps(dict_auto), verify=False)
    variables = a.json()
    result = variables.get('result')
    error = variables.get('error', '')
    return result, error


def envio_retencion_segun_estado(retencion, data_json=None):
    """
    Utiliza diferentes funciones dependiendo de la columna estado_firma en la
    tabla retencion_compra_cabecera
    1: Guardada en el API
    2: Fimada
    3: Enviada al SRI
    4: Autorizada por el SRI
    :param retencion:
    :param weburl:
    :param data_json:
    :return:
    """
    if retencion.estado_firma == 0:
        ##################################################################################
        #        Valida si la retención que esta utilizando es editable o no para        #
        #        luego firmarla opción que viene de anular y generar cuando ya ha        #
        #                       sido guardada en el api de datil                         #
        ##################################################################################
        if retencion.web_url is not None and not retencion.web_url.isspace() and retencion.web_url != "":
            print(str(retencion.web_url))+'la weburl'
            result, weburl, error = enviar_doc_electronico_api(data_json,
                                                               'https://app.datil.co/api/retencion/'+retencion.web_url,
                                                               put=True)
        else:
            print(str(retencion.web_url)+'asasasas')
            result, weburl, error = enviar_doc_electronico_api(data_json, 'https://app.datil.co/api/retencion')

        if result == "ok":
            retencion.web_url = weburl
            retencion.estado_firma = 1
            retencion.save()
            return result, ""
        else:
            print('la cosa de la retencion no se va electronicamente')
            return result, u"Error al guardar la retención en API: " + error

    elif retencion.estado_firma == 1:
        result, weburl, error = firmar_doc_electronico_api(retencion.web_url, 'https://app.datil.co/api/firmar')
        if result == "ok":
            retencion.web_url = weburl
            retencion.estado_firma = 2
            retencion.save()
            return result, ""
        else:
            return result, u"Error al firmar la retención: " + error

    elif retencion.estado_firma == 2:
        result, weburl, error = envio_sri_api(retencion.web_url)
        if result == "ok":
            retencion.estado_firma = 3
            retencion.save()
            return result, ""
        else:
            return result, u"Error al enviar la retención al SRI: " + error
    elif retencion.estado_firma == 3:
        # Según el SRI se debe de esperar al menos 3 segundos después para poder recibir la autorización
        time.sleep(3)
        autorizado, estado, result, error, autorizacion = autorizacion_sri_api(retencion.web_url)

        if autorizado:
            retencion.estado_firma = 4
            retencion.autorizacion = autorizacion
            retencion.save()
            return result, ""
        else:
            return "Error", u"Error al autorizar: " + error

def envio_retencion(cabecera_compra):
    """
    Envía la retencion para que sea autorizada por el SRI
    :param cabecera_compra:
    :return:
    """
    data_json = datos_json_retencion(cabecera_compra.getRetencion(), get_apikey())

    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    print data_json
    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXX"

    retencion = cabecera_compra.getRetencion()
    cont = 0
    error = ""

    while retencion.estado_firma < 4:
        result, error = envio_retencion_segun_estado(retencion, data_json=data_json)
        if result != "ok":
            cont += 1
            break

    if cont > 0:
        return [0, error]
    else:
        return [1, ""]


def enviar_correo_doc_electronico_retencion(request, compra):
    """
     Envía un mail al cliente/proveedor con el documento electrónico
     adjunto
    :param weburl:
    :return:
    """
    proveedor = Proveedores.objects.get(id=compra.proveedor_id)
    retencion = compra.getRetencion()
    empresa = Empresa.objects.all()[0].get_empresa_general()
    form_correo = FormEnvioCorreo(request.POST)
    cadenanombre = ''
    cc = form_correo.get_cc()

    if retencion.web_url is not None:
        if proveedor.email:
            url_download_xml = 'https://app.datil.co/ver/'+retencion.web_url+'/xml'
            mensaje = render_to_string("mails/envio_documento_electronico.html",
                                       {"proveedor": proveedor,
                                        "url_download_xml": url_download_xml,
                                        "retencion": retencion
                                        }, context_instance=RequestContext(request))

            subject = u'Ha recibido un documento electrónico de ' + empresa.razon_social[0:20]
            if empresa.correo is not None and not empresa.correo.isspace():
                correo = empresa.correo
            else:
                correo = ""

            from_email = empresa.razon_social[0:20].replace(",", " ").replace("(", " ").replace(")", " ") + '<' + correo + '>'
            to = proveedor.email

            msg = EmailMessage(subject, mensaje, from_email, [to], cc=convert_cc_to_tuple(cc))
            msg.content_subtype = "html"  # Main content is now text/html

            url = 'https://app.datil.co/ver/' + retencion.web_url + '/xml'
            a = requests.get(url, verify=False)
            xmldoc = minidom.parseString(a.text.encode('UTF-8'))

            ride_pdf = get_ride_retencion_electronica(request, retencion.web_url).getvalue()
            print 'el num de ret'
            print str(retencion.numero_ret)
            print 'la serte'
            print str(retencion.vigencia_doc_empresa.serie)
            print 'eeeeeeeeee'
            cadenanombre = str(retencion.vigencia_doc_empresa.serie)+'-'+str(retencion.numero_ret)
            msg.attach("RideRetencion"+cadenanombre+".pdf", ride_pdf, "application/pdf")
            msg.attach("RetencionXML"+cadenanombre+".xml", xmldoc.toxml(encoding="utf-8"), "application/xml")

            # file.chunks convierte al tipo content para ser enviado por correo
            try:
                file = request.FILES['file']
                for chunk in file.chunks():
                    ext = file.name.split(".")[1]
                    msg.attach(file.name, chunk, "application/"+ext)
            except datastructures.MultiValueDictKeyError:
                pass

            msg.send()
            messages.success(request, u"Se ha enviado el mail con éxito")
            retencion.estado_firma = 5
            retencion.usuario_actualizacion = request.user.username
            retencion.fecha_actualizacion = datetime.datetime.now()
            retencion.save()
            return True
        else:
            messages.error(request, u"Error el proveedor no tiene un correo electrónico ingresado en el sistema")
            return False
    else:
        messages.error(request, u"Error no se pudo enviar el mail, recurso no encontrado")
        return False


#################################################
#           Facturas - Ventas                   #
#################################################
def obtener_conceptos_adicionales(nombre, concepto):
    """
    Función que obtiene las líneas del concepto adicional
    y las agrega por item
    :param concepto:
    :return :
    """
    lista = []
    if concepto is not None:
        concepto = concepto.split(DELIMITADOR)
    else:
        concepto = []
    print 'iniciolenconcepto'
    print concepto
    print 'fin el concepto'
    string = ""
    print len(concepto)
    print 'lo que mide el conceptos'
    if len(concepto) > 2:
        for i in range(2, len(concepto)):
            string = string + " " + concepto[i]
        detalle = {"nombre": nombre, "descripcion": string}
        lista.append(detalle)
    #esto es para simplemente llenar y no vaya vacio
    else:
        string = "."
        detalle = {'nombre': nombre,"descripcion": string }
        lista.append(detalle)

    return lista


def datos_json_facturas(venta_cabecera, apikey):
    """
    Función que me devuelve un Json con los datos
    requeridos por el API
    :param apikey:
    :return:
    """
    # Cliente falta parámetros requeridos por el
    # API como correo, teléfono
    items = []
    codigo_doc = venta_cabecera.vigencia_doc_empresa.serie.split("-")
    lista_ventas = Venta_Detalle.objects.filter(status=1, venta_cabecera=venta_cabecera)

    for obj in lista_ventas:
        detalles_adicion = obtener_conceptos_adicionales(obj.item.nombre, obj.concepto)
        if obj.concepto is not None:
            concepto = obj.concepto.split(DELIMITADOR)
            if len(concepto) >= 2:
                linea = concepto[0]+" "+concepto[1]
            else:
                if len(concepto) > 0:
                    linea = concepto[0]
                else:
                    linea = ""
        else:
            linea = ""

        if obj.monto_iva >0 :
            iva_detalle = '2'
        else:
            iva_detalle = '0'

        if len(detalles_adicion) > 0:
            item = {'cantidad': obj.cantidad,
                    'codigoprincipal': obj.item.id,
                    'codigoauxiliar': '',
                    'nombre': obj.item.nombre+" "+linea,
                    'precio': obj.precio_unitario,
                    'descuento': obj.descuento_valor,
                    'codigoiva': iva_detalle,
                    'codigoice': '',
                    'detalle_adicional': detalles_adicion

                    }
        else:
            item = {'cantidad': obj.cantidad,
                    'codigoprincipal': obj.item.id,
                    'codigoauxiliar': '',
                    'nombre': obj.item.nombre+' '+linea,
                    'precio': obj.precio_unitario,
                    'descuento': obj.descuento_valor,
                    'codigoiva': iva_detalle,
                    'codigoice': ''}

        items.append(item)

    if venta_cabecera.cliente.email is not None:
        email = venta_cabecera.cliente.email
    else:
        email = ''

    if venta_cabecera.concepto is not None and venta_cabecera.concepto != '':

        dic = {'apikey': apikey, 'codigoestablecimiento': codigo_doc[0],
            'codigopuntoventa': codigo_doc[1],
            'secuencia': venta_cabecera.num_documento,
            'guiaremision': '',
            'cliente': {'tipoidentificacion': tipo_identificacion_datilmedia(venta_cabecera.cliente.tipo_identificacion),
                        'identificacion': venta_cabecera.cliente.ruc,
                        'razonsocial': venta_cabecera.cliente.razon_social,
                        'correo': email,
                        'direccion': 'direcciones',
                        'telefono': '2449894'
                        },
            'items': items,
            'propina': '0.00',
            'informacion_adicional': {'concepto': unicode(venta_cabecera.concepto)}
        }

    else:
        dic = {'apikey': apikey, 'codigoestablecimiento': codigo_doc[0],
                'codigopuntoventa': codigo_doc[1],
                'secuencia': venta_cabecera.num_documento,
                'guiaremision': '',
                'cliente': {'tipoidentificacion': tipo_identificacion_datilmedia(venta_cabecera.cliente.tipo_identificacion),
                            'identificacion': venta_cabecera.cliente.ruc,
                            'razonsocial': venta_cabecera.cliente.razon_social,
                            'correo': email,
                            'direccion': 'direcciones',
                            'telefono': '2449894'
                            },
                'items': items,
                'propina': '0.00',
                'informacion_adicional': {'concepto': 'Venta del mes'}
                }

    return dic

def envio_factura_segun_estado(venta, data_json=None):
    """
    Utiliza diferentes funciones dependiendo de la columna estado_firma en la
    tabla venta_cabecera
    1: Guardada en el API
    2: Fimada
    3: Enviada al SRI
    4: Autorizada por el SRI
    :param venta:
    :param weburl:
    :param data_json:
    :return:
    """

    if venta.estado_firma == 0:
        result, weburl, error = enviar_doc_electronico_api(data_json, 'https://app.datil.co/api/facturar')

        if result == "ok":
            venta.web_url = weburl
            venta.estado_firma = 1
            venta.save()
            return result, ""
        else:
            return result, u"Error al guardar la venta en el API: " + error

    elif venta.estado_firma == 1:
        result, weburl, error = firmar_doc_electronico_api(venta.web_url, 'https://app.datil.co/api/firmar')

        if result == "ok":
            venta.web_url = weburl
            venta.estado_firma = 2
            venta.save()
            return result, ""
        else:
            return result, u"Error al firmar la venta: " + error

    elif venta.estado_firma == 2:
        print 'el estado de la firma es dos'
        result, weburl, error = envio_sri_api(venta.web_url)

        if result == "ok":
            print 'ha cambiado del 2 al 3 en l estado de la firma'
            venta.estado_firma = 3
            venta.save()
            return result, ""
        else:
            print('esta cosa se cayo en el estado sdos de la firma')
            return result, u"Error al enviar la venta al SRI: " + error

    elif venta.estado_firma == 3:
        autorizado, estado, result, error, autorizacion = autorizacion_sri_api(venta.web_url)

        if autorizado:
            venta.estado_firma = 4
            venta.autorizacion = autorizacion
            venta.save()
            return result, ""
        else:
            return "Error", u"Error al autorizar al SRI: " + error

def envio_factura(venta):
    """
    Envía la factura de la venta para que sea
    autorizada por el SRI
    :param venta:
    :return:
    """
    data_json = datos_json_facturas(venta, get_apikey())
    cont = 0
    print 'esto es el datajson'
    print data_json
    print 'zxzxzxzxzxz'
    error = ""

    while venta.estado_firma < 4:
        result, error = envio_factura_segun_estado(venta, data_json=data_json)

        if result != "ok":
            cont += 1
            print 'el estado no es de ok en envio_facturaaaaa :o'
            break

    if cont > 0:
        return [0, error]
    else:
        return [1, ""]

def enviar_correo_doc_electronico_factura(request, venta):
    """
     Envía un mail al cliente/proveedor con el documento electrónico
     adjunto
    :param weburl:
    :return:
    """
    miventa = Venta_Cabecera()
    miventa = venta
    prefijo = ''
    cliente = Cliente.objects.get(id=venta.cliente_id)
    empresa = Empresa.objects.all()[0].get_empresa_general()
    form_correo = FormEnvioCorreoFactura(request.POST)
    tipo = 1    # FACTURA

    form_correo.is_valid()
    cc = form_correo.get_cc()

    if venta.web_url is not None:
        if cliente.email:
            url_download_xml = 'https://app.datil.co/ver/'+venta.web_url+'/xml'
            mensaje = render_to_string("mails/envio_documento_electronico.html",
                                       {"cliente": cliente,
                                        "tipo": tipo,
                                        "url_download_xml": url_download_xml,
                                        "venta": venta,
                                        }, context_instance=RequestContext(request))

            subject = u'Ha recibido un documento electrónico de ' + empresa.razon_social[0:20]
            if empresa.correo is not None and not empresa.correo.isspace():
                correo = empresa.correo
            else:
                correo = ""

            from_email = empresa.razon_social[0:20].replace(",", " ").replace("(", " ").replace(")", " ") + '<' + correo + '>'
            to = cliente.email

            msg = EmailMessage(subject, mensaje, from_email, [to], cc=convert_cc_to_tuple(cc))
            msg.content_subtype = "html"  # Main content is now text/html

            url = 'https://app.datil.co/ver/' + venta.web_url + '/xml'
            a = requests.get(url, verify=False)
            xmldoc = minidom.parseString(a.text.encode('UTF-8'))
            print 'esrto ocurre en una ventaaaaaa'
            print str(miventa.num_documento)
            print 'la serieee****'
            print str(miventa.vigencia_doc_empresa.serie)
            print 'asasasa****'
            prefijo = str(miventa.vigencia_doc_empresa.serie)+'-'+str(miventa.num_documento)
            file_to_be_sent = get_ride_factura_pdf(request, venta).getvalue()
            msg.attach("RideFactura"+prefijo+".pdf", file_to_be_sent, "application/pdf")
            msg.attach("FacturaXML"+prefijo+".xml", xmldoc.toxml(encoding="utf-8"), "application/xml")
            
            # file.chunks convierte al tipo content para ser enviado por correo


            try:
                file = request.FILES['file']
                for chunk in file.chunks():
                    ext = file.name.split(".")[1]
                    msg.attach(file.name, chunk, "application/"+ext)
            except datastructures.MultiValueDictKeyError:
                pass
            
            msg.send()
            messages.success(request, u"Se ha enviado el mail con éxito")
            venta.estado_firma = 5
            venta.save()
            return True
        else:
            messages.error(request, u"Error el cliente no tiene un correo electrónico ingresado en el sistema")
            return False
    else:
        messages.error(request, u"Error no se pudo enviar el mail, recurso no encontrado")
        return False




#################################################
#             Nota de Crédito                   #
#################################################
def data_json_nota_credito(ajuste_cabecera, apikey):
    '''
    Función que
    :param ajuste_cabecera:
    :param apikey:
    :return:
    '''
    print 'creando el json de la nota de credito'
    items = []
    codigo_doc = ajuste_cabecera.vigencia_doc_empresa.serie.split("-")
    lista = AjusteVentaDetalle.objects.filter(status=1, ajuste_venta_cabecera=ajuste_cabecera)

    for obj in lista:
        # MOMENTANEAMENTE PERO ESTO DEBE DE CAMBIARSE PARA QUE SEA DINÁMICO
        if obj.cantidad_devolver is not None:
            cantidad = obj.cantidad_devolver
        else:
            cantidad = 1
        #18 noviembre cambiado
        if obj.monto_iva >0 :
            iva_detalle = '2'
        else:
            iva_detalle = '0'

        item = {'cantidad': cantidad,
                'codigoprincipal': str(obj.item.id),
                'codigoauxiliar': "",
                'nombre': smart_str(obj.item.nombre),
                'precio': obj.valor,            # PRECIO POR UNIDAD BIEN/SERVICIO
                "descuento": obj.descuento_valor,
                "codigoiva": iva_detalle,               # Quemado estaba 2
                "codigoice": ""}
        items.append(item)

    if ajuste_cabecera.cliente.email is not None:
        email = ajuste_cabecera.cliente.email
    else:
        email = ""

    dic = {"apikey": apikey,
           "codigoestablecimiento": codigo_doc[0],
            "codigopuntoventa": codigo_doc[1],
            "secuencia": str(int(ajuste_cabecera.num_doc)),
            "fecha": str(ajuste_cabecera.fecha_emi.strftime('%d-%m-%Y')),
            "fechadocumento": str(ajuste_cabecera.fecha_doc_modifica.strftime('%d-%m-%Y')),
            "numerodocumento": ajuste_cabecera.num_doc_modifica,
            "motivodocumento": ajuste_cabecera.razon_modifica,
            "tipodocumento": "01",  # Revisar el código del documento a modificar (Factura)
            "cliente": {"tipoidentificacion": tipo_identificacion_datilmedia(ajuste_cabecera.cliente.tipo_identificacion),
                        "identificacion": ajuste_cabecera.cliente.ruc,
                        "razonsocial": ajuste_cabecera.cliente.razon_social,
                        "correo": email,
                        "direccion": "direcciones",
                        "telefono": ""
                        },
            "items": items,
            "adicionales": [ajuste_cabecera.concepto]}

    return dic


def envio_nota_credito_segun_estado(ajuste_cabecera, data_json=None):
    """
    :param ajuste_cabecera:
    :param data_json:
    :return:
    """

    if ajuste_cabecera.estado_firma == 0:
        result, weburl, error = enviar_doc_electronico_api(data_json, 'https://app.datil.co/api/notacredito')
        if result == "ok":
            ajuste_cabecera.web_url = weburl
            ajuste_cabecera.estado_firma = 1
            ajuste_cabecera.save()
            return result, ""
        else:
            return result, u"Error al guardar la nota de crédito en el API: " + error

    elif ajuste_cabecera.estado_firma == 1:
        result, weburl, error = firmar_doc_electronico_api(ajuste_cabecera.web_url, 'https://app.datil.co/api/firmar')

        if result == "ok":
            ajuste_cabecera.web_url = weburl
            ajuste_cabecera.estado_firma = 2
            ajuste_cabecera.save()
            return result, ""
        else:
            return result, u"Error al firmar la nota de crédito: " + error

    elif ajuste_cabecera.estado_firma == 2:
        print 'la funcion trata deirse cuando el estado es dos :o '
        print('la ruta por la que se quiere ir')
        print(str(ajuste_cabecera.web_url))
        print('vvvvvvvvvvvvv')
        result, weburl, error = envio_sri_api(ajuste_cabecera.web_url)

        if result == "ok":
            ajuste_cabecera.estado_firma = 3
            ajuste_cabecera.save()
            return result, ""
        else:
            return result, u"Error al enviar la nota de crédito al SRI: " + error

    elif ajuste_cabecera.estado_firma == 3:
        autorizado, estado, result, error, autorizacion = autorizacion_sri_api(ajuste_cabecera.web_url)

        if autorizado:
            ajuste_cabecera.estado_firma = 4
            ajuste_cabecera.autorizacion = autorizacion
            ajuste_cabecera.save()
            return result, ""
        else:
            return "Error", u"Error al autorizar al SRI: " + error

def envio_nota_credito(ajuste_cabecera):
    """
    Envía la nota de crédito para que sea
    autorizada por el SRI
    :param venta:
    :return:
    """
    data_json = data_json_nota_credito(ajuste_cabecera, get_apikey())
    cont = 0
    error = ""
    print('esrto es el data_json de la nota de credito en la funcion del envio')
    print(str(data_json))
    print('xzxzxzxzxzxz')
    while ajuste_cabecera.estado_firma < 4:
        result, error = envio_nota_credito_segun_estado(ajuste_cabecera, data_json=data_json)

        if result != "ok":
            cont += 1
            break

    if cont > 0:
        return [0, error]
    else:
        return [1, ""]

def enviar_correo_nota_credito_electronico(request, ajuste_venta):
    '''
    Envía un mail al cliente/proveedor con el documento electrónico adjunto
    :param request:
    :param ajuste_venta:
    :return:
    '''
    cliente = Cliente.objects.get(id=ajuste_venta.cliente_id)
    empresa = Empresa.objects.all()[0].get_empresa_general()
    tipo = 2    # NOTA DE CRÉDITO
    form_correo = FormEnvioCorreoNotaCredito(request.POST)
    form_correo.is_valid()
    cc = form_correo.get_cc()

    if ajuste_venta.web_url is not None:
        if cliente.email:
            url_download_xml = 'https://app.datil.co/ver/'+ajuste_venta.web_url+'/xml'
            mensaje = render_to_string("mails/envio_documento_electronico.html",
                                       {"cliente": cliente,
                                        "tipo": tipo,
                                        "url_download_xml": url_download_xml,
                                        "nota_credito": ajuste_venta,
                                        }, context_instance=RequestContext(request))

            subject = u'Ha recibido un documento electrónico de ' + empresa.razon_social[0:20]
            if empresa.correo is not None and not empresa.correo.isspace():
                correo = empresa.correo
            else:
                correo = ""

            from_email = empresa.razon_social[0:20].replace(",", " ").replace("(", " ").replace(")", " ") + '<' + correo + '>'
            to = cliente.email

            msg = EmailMessage(subject, mensaje, from_email, [to], cc=convert_cc_to_tuple(cc))
            msg.content_subtype = "html"  # Main content is now text/html

            url = 'https://app.datil.co/ver/' + ajuste_venta.web_url + '/xml'
            a = requests.get(url, verify=False)
            xmldoc = minidom.parseString(a.text.encode('UTF-8'))

            # RIDE NOTA CREDITO REVISAR
            file_to_be_sent = get_ride_nota_credito_pdf(request, ajuste_venta.web_url).getvalue()
            msg.attach("RideNotaCredito.pdf", file_to_be_sent, "application/pdf")
            msg.attach("NotaCreditoXML.xml", xmldoc.toxml(encoding="utf-8"), "application/xml")

            # file.chunks convierte al tipo content para ser enviado por correo
            try:
                file = request.FILES['file']
                for chunk in file.chunks():
                    ext = file.name.split(".")[1]
                    msg.attach(file.name, chunk, "application/"+ext)
            except datastructures.MultiValueDictKeyError:
                pass

            msg.send()
            messages.success(request, u"Se ha enviado el mail con éxito")
            ajuste_venta.estado_firma = 5
            ajuste_venta.save()
            return True
        else:
            messages.error(request, u"Error el cliente no tiene un correo electrónico ingresado en el sistema")
            return False
    else:
        messages.error(request, u"Error no se pudo enviar el mail, recurso no encontrado")
        return False