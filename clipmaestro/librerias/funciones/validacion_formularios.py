#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.exceptions import ValidationError
import datetime
import time

__author__ = 'Roberto'

def validador_espacios(value):
    if unicode(value).isspace():
       raise ValidationError(u"Por favor corrija el campo no puede tener solamente espacios en blanco")

def validar_valor(value):
    try:
        if (float(value)) <= 0:
            raise ValidationError(u"Ingrese un valor superior a cero")
        else:
            return True
    except:
        raise ValidationError(u"Ingrese un valor superior a cero")


def isMayorEdad(value):
    '''
    Función que recibe como parámetro una fecha
    que represente a la fecha de nacimiento de una persona.
    retorna un True o un False si es o no mayor de 18 años
    :param value:
    :return:
    '''
    anio_nac = int(str(value).split("-")[0])
    anio_actual = datetime.datetime.now().date().year

    if anio_actual - anio_nac >= 18:
        return True
    else:
        return False


def isFechaActual(value):
    '''
    Función que recibe como parámetro una fecha
    que represente a la fecha que será validada
    para que no permitir fechas futuras retorna
    un True o un False
    :param value:
    :return:
    '''
    anio_nac = int(str(value).split("-")[0])
    anio_actual = datetime.datetime.now().date().year

    if anio_nac <= anio_actual:
        return True
    else:
        return False