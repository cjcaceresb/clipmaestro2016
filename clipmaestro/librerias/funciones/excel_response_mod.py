import datetime
from django.db.models.query import QuerySet, ValuesQuerySet
from django.http import HttpResponse
__author__ = 'Clip Maestro'

delimitador = u"%/%"
titulo = 1
subtit = 2
columna = 3
g_total = 4

TITULO = delimitador+str(titulo)
SUBTITULO = delimitador+str(subtit)
COLUMNA = delimitador+str(columna)
G_TOTAL = delimitador+str(g_total)


class ExcelResponse(HttpResponse):

    def __init__(self, data, output_name='excel_data', headers=None,
                 force_csv=False, encoding='utf8'):

        # Make sure we've got the right type of data to work with
        valid_data = False
        if isinstance(data, ValuesQuerySet):
            data = list(data)
        elif isinstance(data, QuerySet):
            data = list(data.values())
        if hasattr(data, '__getitem__'):
            if isinstance(data[0], dict):
                if headers is None:
                    headers = data[0].keys()
                data = [[row[col] for col in headers] for row in data]
                data.insert(0, headers)
            if hasattr(data[0], '__getitem__'):
                valid_data = True
        assert valid_data is True, "ExcelResponse requires a sequence of sequences"

        import StringIO
        output = StringIO.StringIO()
        # Excel has a limit on number of rows; if we have more than that, make a csv
        use_xls = False
        if len(data) <= 65536 and force_csv is not True:
            try:
                import xlwt
            except ImportError:
                # xlwt doesn't exist; fall back to csv
                pass
            else:
                use_xls = True
        if use_xls:
            book = xlwt.Workbook(encoding=encoding)
            sheet = book.add_sheet('Sheet 1')
            styles = {'datetime': xlwt.easyxf(num_format_str='yyyy-mm-dd hh:mm:ss'),
                      'date': xlwt.easyxf(num_format_str='yyyy-mm-dd'),
                      'time': xlwt.easyxf(num_format_str='hh:mm:ss'),
                      'float': xlwt.easyxf("align: horiz right", num_format_str='0.00'),
                      'titulo': xlwt.easyxf('font: height 160, name Arial Black, bold on; align: wrap on, vert centre, horiz center; borders: bottom double'),
                      'subtitulo': xlwt.easyxf('font: height 160, name Arial Black; align: wrap on, vert centre, horiz left;'),
                      'columna': xlwt.easyxf('font: height 160, name Arial Black; align: wrap on, vert centre, horiz center;'),
                      'titulo-totales': xlwt.easyxf('font: height 140, name Arial, colour_index brown, bold on, italic on; align: wrap on, vert centre, horiz left;'),
                      'subtitulo_der_style': xlwt.easyxf('font: height 120, name Arial, colour_index brown, bold on, italic on; align: wrap on, vert centre, horiz left;'),
                      'subtitulo_top_and_bottom_style': xlwt.easyxf('font: height 120, name Arial, colour_index black, bold off, italic on; align: wrap on, vert centre, horiz left;'),
                      'guion_total': xlwt.easyxf("borders: top MEDIUM; align: horiz right", num_format_str='0.00'),
                      'default': xlwt.Style.default_style}
            #xlwt.Borders().
            for rowx, row in enumerate(data):
                i = 0  # Contador de columna, para darle ancho estatico
                for colx, value in enumerate(row):
                    if isinstance(value, datetime.datetime):
                        cell_style = styles['datetime']
                    elif isinstance(value, datetime.date):
                        cell_style = styles['date']
                    elif isinstance(value, datetime.time):
                        cell_style = styles['time']
                    elif isinstance(value, float):
                        cell_style = styles['float']
                    elif isinstance(value, basestring):
                        try:
                            tipo_formato = int(unicode(value).split(delimitador)[1])
                            value = unicode(value).split(delimitador)[0]
                            if tipo_formato == titulo:
                                cell_style = styles['titulo']

                            elif tipo_formato == subtit:
                                cell_style = styles['subtitulo']

                            elif tipo_formato == columna:
                                cell_style = styles['columna']

                            elif tipo_formato == g_total:
                                cell_style = styles['guion_total']
                                try:
                                    value = float(value)
                                except ValueError:
                                    pass
                            elif tipo_formato == "4":
                                cell_style = styles['titulo-totales']

                            else:
                                cell_style = styles['default']
                        except:
                            cell_style = styles['default']
                    else:
                        cell_style = styles['default']

                    sheet.write(rowx, colx, value, style=cell_style)
                    sheet.col(i).width = 5000
                    i += 1
            book.save(output)
            mimetype = 'application/vnd.ms-excel'
            file_ext = 'xls'
        else:
            for row in data:
                out_row = []
                for value in row:
                    if not isinstance(value, basestring):
                        value = unicode(value)
                    value = value.encode(encoding)
                    out_row.append(value.replace('"', '""'))
                output.write('"%s"\n' %
                             '","'.join(out_row))
            mimetype = 'text/csv'
            file_ext = 'csv'
        output.seek(0)
        super(ExcelResponse, self).__init__(content=output.getvalue(),
                                            mimetype=mimetype)
        self['Content-Disposition'] = 'attachment;filename="%s.%s"' % \
            (output_name.replace('"', '\"'), file_ext)
