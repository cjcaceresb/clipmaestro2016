#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template

def isCedula(value):
    cont = 0
    try:

        if value[2] < "6" and len(value) == 10 and (int(value[0]+value[1]) < 25):
            valores = [int(value[x]) * (2 - x % 2) for x in range(9)]
            suma = sum(map(lambda x: x > 9 and x - 9 or x, valores))
            num = int(str(suma)[-1:])
            if num == 0:
                dig_veri = 0
                if int(value[9]) == dig_veri or int(value[9]) == 0:
                    cont += 1
                else:
                    cont = 0
            else:
                dig_veri = 10-num

                if int(value[9]) == dig_veri or int(value[9]) == 0:
                    cont += 1
                else:
                    cont = 0
        else:
            cont = 0

        if value == "9999999999":
            cont += 1

    except:
        pass
        cont = 0

    if cont == 0:
        return False
    else:
        return True

def isPersonaNatural(value):

    contador = 0
    try:
        if value[2] < "6" and (int(value[10]) == 0 and int(value[11]) == 0 and int(value[12]) == 1) and (int(value[0]+value[1])<25):
            valores = [int(value[x]) * (2 - x % 2) for x in range(9)]
            suma = sum(map(lambda x: x > 9 and x - 9 or x, valores))
            num = int(str(suma)[-1:])
            if num == 0:
                dig_veri = 0
                if int(value[9]) == dig_veri or int(value[9]) == 0:
                    contador += 1
                else:
                    contador = 0
            else:
                dig_veri = 10-num

                if int(value[9]) == dig_veri or int(value[9]) == 0:
                    contador += 1
                else:
                    contador = 0
        else:
            contador = 0

        if value == "9999999999999":
            contador += 1

    except:
        pass
        contador = 0

    if contador == 0:
        return False
    else:
        return True

def isPersonaJuridica(value):
    cont = 0
    try:

        if value[2] == "9" and (int(value[10]) == 0 and int(value[11]) == 0 and int(value[12]) == 1) and (int(value[0]+value[1]) < 25):
            valores_juridica = [int(value[x]) for x in range(9)]
            m = [4, 3, 2, 7, 6, 5, 4, 3, 2]
            suma_total = 0

            for i in range(len(valores_juridica)):
                suma_total = suma_total + (valores_juridica[i]* m[i])

            residuo = suma_total%11
            respuesta = int(11-residuo)

            if int(value[9]) == respuesta or residuo == 0:
                cont += 1
            else:
                cont = 0
        else:
            cont = 0

        if value == "9999999999999":
            cont += 1

    except:
        pass
        cont = 0

    if cont == 0:
        return False
    else:
        return True

def isEmpresaPublica(value):
    contador_result = 0
    try:
        if value[2] == "6" and (int(value[9]) == 0 and int(value[10]) == 0 and int(value[11]) == 0 and int(value[12]) == 1) and (int(value[0]+value[1]) < 25):
            valores_juridica = [int(value[x]) for x in range(8)]
            m = [3, 2, 7, 6, 5, 4, 3, 2]
            suma_total = 0

            for i in range(len(valores_juridica)):
                suma_total = suma_total + (valores_juridica[i]* m[i])

            residuo = suma_total%11
            respuesta= int(11-residuo)

            if int(value[8]) == respuesta or residuo == 0:
                contador_result += 1
            else:
                contador_result = 0
        else:
            contador_result = 0
    except:
        pass
        contador_result = 0

    if contador_result == 0:
        return False
    else:
        return True
