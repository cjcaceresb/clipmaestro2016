#! /usr/bin/python
# -*- coding: UTF-8-*-



from contabilidad.formularios.CuentasxPagarForm import *
from cgi import escape
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
import json
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import transaction
from administracion.models import *
import cStringIO as StringIO

import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from contabilidad.models import PlanCuenta
import math
import calendar
from django.template.loader import render_to_string
import operator
from django.conf import settings
import os
from administracion.models import *
import math
import urllib2
from clipmaestro.settings import *
__author__ = 'Clip Maestro'

anios = (("", ""), (2014, "2014"), (2013, "2013"), (2012, "2012"), (2011, "2011"), (2010, "2010"), (2009, "2009"), (2008, "2008"), (2007, "2007"),
                   (2006, "2006"), (2005, "2005"), (2004, "2004"), (2003, "2003"), (2002, "2002"), (2001, "2001"), (2000, "2000"), (1999, "1999"),
                   (1998, "1998"), (1997, "1997"), (1996, "1996"), (1995, "1995"), (1994, "1994"), (1993, "1993"), (1992, "1992"), (1991, "1991"),
                   (1990, "1990"), (1989, "1989"), (1988, "1988"), (1987, "1987"), (1986, "1986"), (1985, "1985"), (1984, "1984"), (1983, "1983"),
                   (1982, "1982"), (1981, "1981"), (1980, "1980"), (1979, "1979"), (1978, "1977"), (1976, "1976"), (1975, "1975"), (1974, "1974"),
                   (1973, "1973"), (1972, "1972"), (1971, "1971"), (1970, "1970"))


DELIMITADOR = "!=.$"


def cont_mov(obj, id):
    """
    #############################################
    # Funcion para determinar si una cuenta     #
    # que se desee eliminar o modificar         #
    # puede eliminarse o midificarse            #
    # parm: obj = "Cuenta del plan de cuenta"   #
    # id: "id" de la base de datos              #
    #############################################
    :param obj:
    :param id:
    :return:
    """
    cont = 0
    if obj.nivel == 5:
        if obj.id == id:
            return 1
    else:
        nodo = obj.getHijos()
        if nodo is not None:
            for objchild in nodo:
                cont += cont_mov(objchild, id)
    return cont


def tiene_mov(obj, id):
    """
    Función que retorna True or False dependiendo si
    la cuenta tiene movimientos contables
    :param obj:
    :param id:
    :return:
    """
    if cont_mov(obj, id) > 0:
        return True
    else:
        return False


def cero_if_none(obj):
    """
    Funcion que retorna cero si el obj ingresado es None o si es vacio
    :param obj:
    :return:
    """
    try:
        return float(obj)
    except TypeError:
        return 0.0


def buscarElemento(lista, elemento):
    for i in range(0, len(lista)):
        if(lista[i] == elemento):
            return i

def imprimirLista(lista, nombre):
    for i in range(0, len(lista)):
        print nombre + "[" + str(i) + "] = " + str(lista[i].id_cuenta) + " - " + str(lista[i].costo)


def generar_pdf(html):
    """
    Función interna para generar pdf
    :param template_src:
    :param context_dict:
    :return:
    """
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources)
    if not pdf.err:
        response = HttpResponse(result.getvalue(), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="Reporte.pdf"'
        result.close()
        pisa.log.removeHandler(pisa.log.handlers)
        return response

    return HttpResponse('Tenemos algunos errores<pre>%s</pre>' % escape(html))

def generar_pdf_nombre(html,nombre):
    """
    Función interna para generar pdf
    :param template_src:
    :param context_dict:
    :return:
    """
    result = StringIO.StringIO()
    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources)
    if not pdf.err:
        response = HttpResponse(result.getvalue(), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="'+nombre+'.pdf"'
        result.close()
        pisa.log.removeHandler(pisa.log.handlers)
        return response

    return HttpResponse('Tenemos algunos errores<pre>%s</pre>' % escape(html))

def generar_pdf_get(html):
    """
    Función interna para generar pdf por método get
    :param template_src:
    :param context_dict:
    :return:
    """
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources)
    if not pdf.err:
        response = HttpResponse(result.getvalue(), content_type='application/pdf')
        result.close()
        pisa.log.removeHandler(pisa.log.handlers)
        return response

    return HttpResponse('Tenemos algunos errores<pre>%s</pre>' % escape(html))


def fetch_resources(uri, rel):
    path = os.path.join(settings.MEDIA_ROOT, uri.replace(settings.MEDIA_URL, ""))
    return path


def generar_pdf_adjunto_mail(html):
    """
    Función interna para generar pdf por método get
    :param template_src:
    :param context_dict:
    :return:
    """
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=result, link_callback=fetch_resources)
    if not pdf.err:
        return result
    else:
        return None


def first_day_of_month(d):
    """
    Retorna el primer dia del mes
    en formato fecha
    :param d:
    :return:
    """
    return date(d.year, d.month, 1)


def is_first_day_month(d1):
    date_tmp = first_day_of_month(d1)

    if d1 == date_tmp:
        return True
    else:
        return False

def less_one_day_of_month(d):
    """
    Retorna el primer dia del mes
    en formato fecha
    :param d:
    :return:
    """
    if d.day == 1:
        return date(d.year, d.month, 1)
    else:
        return date(d.year, d.month, d.day - 1)


def last_day_of_month(d):
    '''
    Retorna el último dia del mes
    en formato fecha
    :param d:
    :return:
    '''
    return date(d.year, d.month, calendar.monthrange(d.year, d.month)[1])


def get_num_comp(id_tipo, fecha_reg, validar_fecha, request):
    """
    Retorna el numero_comp
    """
    tipo_comp = TipoComprobante.objects.get(id=id_tipo)
    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
    except SecuenciaTipoComprobante.DoesNotExist:
        secuencia_tipo = SecuenciaTipoComprobante()
        secuencia_tipo.tipo_comprobante = tipo_comp
        secuencia_tipo.fecha_creacion = datetime.datetime.now()
        secuencia_tipo.usuario_creacion = request.user.username
        secuencia_tipo.secuencia = 0
        secuencia_tipo.status = 1

    num_comp = tipo_comp.codigo + str(int(secuencia_tipo.secuencia) + 1).zfill(6)
    secuencia_tipo.secuencia += 1

    if validar_fecha:
        if secuencia_tipo.fecha_actual is not None:
            if fecha_reg >= secuencia_tipo.fecha_actual:
                secuencia_tipo.fecha_actual = fecha_reg
                secuencia_tipo.usuario_actualizacion = request.user.username
                secuencia_tipo.fecha_actualizacion = datetime.datetime.now()
                secuencia_tipo.save()
                return num_comp
            else:
                return -1
        else:
            secuencia_tipo.fecha_actual = fecha_reg
            secuencia_tipo.save()
            return num_comp
    else:
        secuencia_tipo.fecha_actual = fecha_reg
        secuencia_tipo.usuario_actualizacion = request.user.username
        secuencia_tipo.fecha_actualizacion = datetime.datetime.now()
        secuencia_tipo.save()

        return num_comp


def get_fecha_tipo_comp(id_tipo):
    """
    Retorna la última fecha del movimiento del tipo de comprobante contable
    """
    tipo_comp = TipoComprobante.objects.get(id=id_tipo)
    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
        fecha = secuencia_tipo.fecha_actual.strftime("%Y-%m-%d")
    except SecuenciaTipoComprobante.DoesNotExist:
        fecha = ""
    return fecha


def get_num_contrato(id_tipo, fecha_reg, validar_fecha, request):
    """
    Retorna el numero_contrato
    """
    tipo_comp = TipoComprobante.objects.get(id=id_tipo)
    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
    except SecuenciaTipoComprobante.DoesNotExist:
        secuencia_tipo = SecuenciaTipoComprobante()
        secuencia_tipo.tipo_comprobante = tipo_comp
        secuencia_tipo.fecha_creacion = datetime.datetime.now()
        secuencia_tipo.usuario_creacion = request.user.username
        secuencia_tipo.status = 1
        secuencia_tipo.secuencia = 0

    anio = fecha_reg.year

    num_comp = tipo_comp.codigo + str(anio)+"-"+str(int(secuencia_tipo.secuencia) + 1).zfill(6)
    secuencia_tipo.secuencia += 1

    if validar_fecha and secuencia_tipo.fecha_actual is not None:
        if fecha_reg >= secuencia_tipo.fecha_actual:
            secuencia_tipo.fecha_actual = fecha_reg
            secuencia_tipo.usuario_actualizacion = request.user.username
            secuencia_tipo.fecha_actualizacion = datetime.datetime.now()
            secuencia_tipo.save()

            return num_comp
        else:
            return -1

    else:
        secuencia_tipo.fecha_actual = fecha_reg
        secuencia_tipo.usuario_actualizacion = request.user.username
        secuencia_tipo.fecha_actualizacion = datetime.datetime.now()
        secuencia_tipo.save()

        return num_comp


def get_num_comp_contable(tipo_c, fecha_reg, request):
    """
    Retorna el numero_comp_contable (Asiento - Caja Chica)
    """
    anio = fecha_reg.year
    mes = fecha_reg.strftime('%m')
    try:
        secuencia_contable = Seq_Comp_Contable.objects.get(anio=anio, mes=mes, tipo_comp=tipo_c)  # Secuencia_Tipo_Comp.
        num_comp = tipo_c.codigo + str(anio) + str(mes) + str(int(secuencia_contable.secuencia) + 1).zfill(6)
        secuencia_contable.secuencia += 1
        secuencia_contable.fecha_actual = fecha_reg
        secuencia_contable.fecha_creacion = datetime.datetime.now()
        secuencia_contable.usuario_creacion = request.user.username
        secuencia_contable.usuario_actualizacion = request.user.username
        secuencia_contable.fecha_actualizacion = datetime.datetime.now()
        secuencia_contable.save()
        return num_comp

    except Seq_Comp_Contable.DoesNotExist:
        secuencia_contable = Seq_Comp_Contable()
        secuencia_contable.tipo_comp = tipo_c
        secuencia_contable.mes = mes
        secuencia_contable.anio = anio
        secuencia_contable.fecha_actual = fecha_reg
        secuencia_contable.fecha_creacion = datetime.datetime.now()
        secuencia_contable.fecha_actualizacion = datetime.datetime.now()
        secuencia_contable.usuario_creacion = request.user.username
        secuencia_contable.usuario_actualizacion = request.user.username
        secuencia_contable.secuencia = 0
        num_comp = tipo_c.codigo + str(anio) + str(mes) + str(int(secuencia_contable.secuencia) + 1).zfill(6)
        secuencia_contable.secuencia += 1
        secuencia_contable.save()
        return num_comp


def get_num_doc(vigencia_doc, fecha_reg, request):
    """
    Retorna el numero_doc
    """

    num_doc = str(int(vigencia_doc.sec_actual) + 1).zfill(9)
    vigencia_doc.sec_actual = int(vigencia_doc.sec_actual) + 1

    if vigencia_doc.sec_actual > vigencia_doc.sec_final:
        return -1

    else:
        vigencia_doc.fecha_actual = fecha_reg
        vigencia_doc.fecha_actualizacion = datetime.datetime.now()
        vigencia_doc.usuario_actualizacion = request.user.username
        vigencia_doc.save()

        return num_doc


def get_next_cheque(cta_banco, request):
    """
    Retorna el sigiente número de cheque
    :param cta_banco:
    :param request:
    :return:
    """
    if cta_banco.secuencia_cheque is None:
        cta_banco.secuencia_cheque = 0.0

    cta_banco.secuencia_cheque = int(cta_banco.secuencia_cheque) + 1
    cta_banco.fecha_actualizacion = datetime.datetime.now()
    cta_banco.usuario_actualizacion = request.user.username
    cta_banco.save()
    return int(cta_banco.secuencia_cheque)


def convert_cero_none(obj):
    """
    Retorna cero si el elemento que le envian es None
    caso contrario retorna el mismo elemento
    :param obj:
    :return:
    """
    if obj is None:
        return 0.00
    else:
        return obj


def convert_vacio_none(obj):
    """
    Retorna " " si el elemento que le envian es None
    caso contrario retorna el mismo elemento
    :param obj:
    :return:
    """
    if obj is None:
        return ""
    else:
        return obj


def get_mes(number):
    """
    Funcion que me retorna el nombre de un mes, dependiendo del
    número que envie. Ej: 1 return Enero
    :param number:
    :return:
    """
    mes = {1: "ENERO", 2: "FEBRERO", 3: "MARZO", 4: "ABRIL",
       5: "MAYO", 6: "JUNIO", 7: "JULIO", 8: "AGOSTO",
       9: "SEPTIEMBRE", 10: "OCTUBRE", 11: "NOVIEMBRE",
       12: "DICIEMBRE"}

    if type(number) is int:
        if number in mes:
            return mes[number]
        else:
            return ""
    else:
        return ""


def get_saldo_cuenta_acum(plan_cuenta_id, anio, mes):
    """
    Función que retorna el saldo acumulado de la tabla saldo de
    cuenta
    :param anio:
    :param mes:
    :return:
    """
    try:
        plan_cuenta = PlanCuenta.objects.get(id=plan_cuenta_id)
        saldo_cta = SaldoCuenta.objects.get(anio=anio, plan_cuenta=plan_cuenta)

        if plan_cuenta.naturaleza == "D":
            saldo_deudor_acum = saldo_cta.get_saldo_debe_acum(mes)
            #saldo_deudor = saldo_cta.get_saldo_debe_mes(mes)
            saldo_acreedor_acum = saldo_cta.get_saldo_haber_acum(mes)
            #saldo_acreedor = saldo_cta.get_saldo_haber_mes(mes)

            #return redondeo(convert_cero_none(saldo_cta.saldo_inicial) + saldo_deudor_acum + saldo_deudor
            #                - saldo_acreedor - saldo_acreedor_acum)
            return redondeo(convert_cero_none(saldo_cta.saldo_inicial) + saldo_deudor_acum - saldo_acreedor_acum)
        else:
            saldo_deudor_acum = saldo_cta.get_saldo_debe_acum(mes)
            #saldo_deudor = saldo_cta.get_saldo_debe_mes(mes)
            saldo_acreedor_acum = saldo_cta.get_saldo_haber_acum(mes)
            #saldo_acreedor = saldo_cta.get_saldo_haber_mes(mes)

            #return redondeo(convert_cero_none(saldo_cta.saldo_inicial) + saldo_acreedor + saldo_acreedor_acum
            #                - saldo_deudor - saldo_deudor_acum)
            return redondeo(convert_cero_none(saldo_cta.saldo_inicial) + saldo_acreedor_acum - saldo_deudor_acum)
    except:
        return 0.0


def get_saldo_cuenta_acum_centro_costo(plan_cuenta_id, anio, mes, centro_costo_id=None):
    """
    Función que retorna el saldo acumulado de la tabla saldo de
    cuenta
    :param anio:
    :param mes:
    :return:
    """
    try:
        plan_cuenta = PlanCuenta.objects.get(id=plan_cuenta_id)
        if centro_costo_id:
            saldo_cta = SaldoCuentaCentroCosto.objects.get(anio=anio, plan_cuenta=plan_cuenta, centro_costo_id=centro_costo_id)
        else:
            saldo_cta = SaldoCuentaCentroCosto.objects.get(anio=anio, plan_cuenta=plan_cuenta, centro_costo_id=None)

        if plan_cuenta.naturaleza == "D":
            saldo_deudor_acum = saldo_cta.get_saldo_debe_acum(mes)
            saldo_acreedor_acum = saldo_cta.get_saldo_haber_acum(mes)

            return redondeo(convert_cero_none(saldo_cta.saldo_inicial) + saldo_deudor_acum - saldo_acreedor_acum)
        else:
            saldo_deudor_acum = saldo_cta.get_saldo_debe_acum(mes)
            saldo_acreedor_acum = saldo_cta.get_saldo_haber_acum(mes)

            return redondeo(convert_cero_none(saldo_cta.saldo_inicial) + saldo_acreedor_acum - saldo_deudor_acum)
    except:
        return 0.0


def get_saldo_cuenta(plan_cuenta_id, anio, mes):
    """
    Función que retorna em saldo acumulado de la tabla saldo de
    cuenta
    :param anio:
    :param mes:
    :return:
    """
    try:
        plan_cuenta = PlanCuenta.objects.get(id=plan_cuenta_id)
        saldo_cta = SaldoCuenta.objects.get(anio=anio, plan_cuenta=plan_cuenta)

        if plan_cuenta.naturaleza == "D":
            saldo_deudor = saldo_cta.get_saldo_debe_mes(mes)
            saldo_acreedor = saldo_cta.get_saldo_haber_mes(mes)
            return saldo_deudor - saldo_acreedor
        else:
            saldo_deudor = saldo_cta.get_saldo_debe_mes(mes)
            saldo_acreedor = saldo_cta.get_saldo_haber_mes(mes)
            return saldo_acreedor - saldo_deudor
    except SaldoCuenta.DoesNotExist:
        return 0.0


def get_saldo_cuenta_centro_costo(plan_cuenta_id, anio, mes, centro_costo_id=None):
    """
    Función que retorna em saldo acumulado de la tabla saldo de
    cuenta
    :param anio:
    :param mes:
    :return:
    """
    try:
        plan_cuenta = PlanCuenta.objects.get(id=plan_cuenta_id)
        if centro_costo_id:
            saldo_cta = SaldoCuentaCentroCosto.objects.get(anio=anio, plan_cuenta=plan_cuenta, centro_costo_id=centro_costo_id)
        else:
            saldo_cta = SaldoCuentaCentroCosto.objects.get(anio=anio, plan_cuenta=plan_cuenta, centro_costo_id=None)

        if plan_cuenta.naturaleza == "D":
            saldo_deudor = saldo_cta.get_saldo_debe_mes(mes)
            saldo_acreedor = saldo_cta.get_saldo_haber_mes(mes)
            return saldo_deudor - saldo_acreedor
        else:
            saldo_deudor = saldo_cta.get_saldo_debe_mes(mes)
            saldo_acreedor = saldo_cta.get_saldo_haber_mes(mes)
            return saldo_acreedor - saldo_deudor
    except:
        return 0.0


def get_fecha_rango_ant(date):
    """
    Obtiene el rango de fecha inicio final como tupla.
    Ex: 05-09-2014 returna (2014-08-01-, 2014-08-31)
    :param fecha type date:
    :return:
    """
    month = date.month
    year = date.year
    if month == 1:
        print calendar.monthrange(year-1, 11)
        dweek, lday = calendar.monthrange(year-1, 12)
        return datetime.datetime(year-1, 12, 1), datetime.datetime(year-1, 12, lday)
    else:
        dweek, lday = calendar.monthrange(year, month - 1)
        return datetime.datetime(year, month - 1, 1), datetime.datetime(year, month - 1, lday)


def redondeo(numero, precision=2):
    """
    Funcion de redondeo que reemplaza a la de python por problemas
    al redondear
    :param numero:
    :param precision:
    :return:
    """
    try:
        multiplicador = math.pow(10, precision + 1)
        num_multi = math.floor(numero * multiplicador)
        return round(num_multi/10) * 10 / multiplicador
    except:
        return numero


def busqueda_clientes_proveedores(buscador, tipo):
    '''
    Función que realiza las búsquedas de los Clientes_Proveedores
    según los parámetros y seleccionando la núsqueda por la variable tipo.
    tipo = 1 -> Cliente
    tipo = 2 -> Proveedor
    :param buscador:
    :return:
    '''
    predicates = []
    if buscador.getTipoIdentificacion() is not None:
        predicates.append(('tipo_identificacion__id', buscador.getTipoIdentificacion().id))

    if buscador.getNombreComercial() != "":
        predicates.append(('nombre__icontains', buscador.getNombreComercial()))

    predicates.append(('razon_social__icontains', buscador.getRazonSocial()))

    if buscador.getNumID() != "":
        predicates.append(('ruc__icontains', buscador.getNumID()))

    q_list = [Q(x) for x in predicates]

    if tipo == 1:           # CLientes
        entries = Cliente.objects.exclude(status=0).filter(reduce(operator.and_, q_list))
    else:                   # Proveedores
        entries = Proveedores.objects.exclude(status=0).filter(reduce(operator.and_, q_list))

    return entries

def emite_docs_electronicos():
    """
    Función que retorna el True si son  empresas que emiten
    documentos electrónicos
    """
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    if empresa.doc_electronico:
        return True
    else:
        return False

def get_datos_empresa():
    '''
    Función que retorna el Objeto de Empresa General
    :return:
    '''
    try:
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = EmpresaGeneral.objects.get(id=id_empresa)
        return empresa
    except EmpresaGeneral.DoesNotExist:
        return None

def get_parametros_empresa():
    '''
    Función que retorna el Obeto Empresa Parametro
    :param empresa:
    :return:
    '''
    try:
        return Empresa_Parametro.objects.using("base_central").get(id=get_datos_empresa().id)
    except Empresa_Parametro.DoesNotExist:
        return None

def controla_stock():
    '''
    Función que retorna true si son
    ventas de modo sólo productos
    :return:
    '''
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa_parametro = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    if empresa_parametro.stock == True:
        return True
    else:
        return False

def modifica_precio_venta():
    '''
    Función que me indica si el vendedor puede o no realizar un cambio en el precio de venta
    al realizar el proceso de venta esto se valida en el formset del detalle de venta para que el
    campo sea un input normal o de sólo lectura
    :return:
    '''
    empresa_parametro = get_parametros_empresa()

    if empresa_parametro.modifica_precio_venta:
        return True
    else:
        return False



def get_modo_venta():
    '''
    Función que va a determinar que forma o modelo de venta
    se va a realizar por empresa
    Ej:
    1.- Venta Productos (Controlando Stock - Sin controlar Stock)
    2.- Venta Servicios
    3.- Venta Productos y Servicios (Controlando Stock - Sin controlar Stock)
    :return:
    '''

    modo_venta = 0
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa_parametro = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    if empresa_parametro.tipo_venta == 1:   # Productos

        if emite_docs_electronicos():
            if empresa_parametro.stock:         # Si Controla Stock
                print "funcion de la venta que valida el stock (empresa_parametro.stock)"
            else:                               # No controla Stock Productos Cantidad por entregar
                print "función de la venta que no valida el stock - cantidad por entregar"
        else:
            print "no emite docs electrónicos"


    elif empresa_parametro.tipo_venta == 2:  # Servicios

        if emite_docs_electronicos():
            pass

    elif empresa_parametro.tipo_venta == 3:  # Producto y Servicios

        if emite_docs_electronicos():
            if empresa_parametro.stock:          # Si Controla Stock
                print "funcion de la venta que valida el stock si es producto"
            else:
                print "función de la venta que no valida el stock (cantidad por entregar)"
        else:
            pass


def get_apikey():
    """
    Función que retorna el apikey de la empresa para los
    documentos electrónicos
    """
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)
    if empresa.doc_electronico:
        return empresa.apikey
    else:
        return ""


def get_passwordapi():
    """
    Función que retorna el apikey de la empresa para los
    documentos electrónicos
    """
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)
    if empresa.clave_api:
        return empresa.clave_api
    else:
        return ""


def tipo_identificacion_datilmedia(tipo_identificacion):
    """
    Función que parametriza el tipo de identificación
    según los parametros requeridos por el API de
    datilmedia
    :param tipo_identificacion:
    :return:
    """
    if tipo_identificacion.codigo == "R":
        return "RUC"
    elif tipo_identificacion.codigo == "C":
        return "CEDULA"
    elif tipo_identificacion.codigo == "P":
        return "PASAPORTE"
    else:
        return "CONSUMIDORFINAL"


def codigo_identificacion_sri(tipo_transaccion, tipo_identificacion):
    """
    Función que consulta según el tipo de transacción sea este una compra o una venta
    para extender se debe definir bien los conceptos de las otras transacciones
    y también recibe como parametro el tipo de identificación con la finalidad de
    obtener el código asignado por el sri para el ATS.
    Ej:
    TIPO_TRANSACCIÓN: VENTA
    RUC => 04,
    Cédula => 05,
    Pasaporte => 06,
    Consumidor Final => 07
    :param tipo_transaccion:
    :param tipo_identificacion:
    :return:
    """

    # Compra
    if tipo_transaccion.id == 1:
        if tipo_identificacion.id == 1:         # Ruc
            return "01"
        elif tipo_identificacion.id == 2:       # Cédula
            return "02"
        elif tipo_identificacion.id == 3:       # Pasaporte
            return "03"
    # Venta
    elif tipo_transaccion.id == 2:
        if tipo_identificacion.id == 1:         # Ruc
            return "04"
        elif tipo_identificacion.id == 2:       # Cédula
            return "05"
        elif tipo_identificacion.id == 3:       # Pasaporte
            return "06"
        elif tipo_identificacion.id == 4:       # Consumidor Final
            return "07"


def get_nombre_doc_sri_by_cod(cod):
    """
    Retorna el nombre del documento dependiendo del codigo
    """
    try:
        documento = Documento.objects.get(codigo_documento=cod)
        return documento.descripcion_documento
    except Documento.DoesNotExist:
        return ""


def convert_cc_to_tuple(cc):
    """
    Convierte un string separado por ";" a un arreglo con sus elementos en él
    :param cc:
    :return:
    """
    result = []
    cc.replace(" ", "")
    if len(cc.split(";")) > 0:
        for obj in cc.split(";"):
            result.append(obj.lower())
    else:
        if cc != "":
            result.append(cc.lower())
    return result


def son_iguales(a, b, e=2):
    """
    Compara dos valores de tipo punto flotante si son iguales o no con
    un valor e que significa a cuantas cifras significativas se va a
    comparar
    Ej:
    Si e = 2 => se compara a 0.001 osea toma como dos cifras significativas
     a comparar
    :param a:
    :param b:
    :param e:
    :return:
    """
    result = abs(a - b)
    epsilon = (1.0 / (10.0 ** e+1))
    return result < epsilon


def get_num_filas_template():
    """
    funcion que retorna el número de filas que se muestran en la lista de los templates,
    ej: lista de compras, lista de ventas
    :return:
    """
    empresa = Empresa.objects.all()[0]
    empresa_general = empresa.get_empresa_general()
    empresa_parametro = Empresa_Parametro.objects.using("base_central").get(id=empresa_general.id)
    try:
        if empresa_parametro.num_filas_template is not None:
            return empresa_parametro.num_filas_template
        else:
            return 50
    except:
        return 50


def esta_periodo_contable(fecha):
    """
    Valida si se pueden hacer transacciones contables en el sistema
    :param fecha:
    :return:
    """
    parametro_empresa = get_parametros_empresa()
    if parametro_empresa is not None:
        mes = parametro_empresa.mes_cierre
        anio = parametro_empresa.anio_cierre
        if None not in (mes, anio):
            mes_asiento = fecha.month
            anio_asiento = fecha.year

            if anio_asiento > anio:
                return True
            elif anio_asiento == anio:
                if mes_asiento > mes:
                    return True
                else:
                    return False
            else:
                return False
        return True  # Todavia no tienen una fecha de cierre contable
    else:
        raise Exception(u"Parámetro de empresa no configurado")


def get_fila_t_detalle_compra(form):
    """
    Funcion que devuelve la fila de la tabla de detalle de compras
    :param form:
    :return:
    """
    html = render_to_string('tags/forms/compras/fila_tabla_det_compra.html', {'formularios': form})
    return html


def int_con_comas(x):
    """
    retorna la representación de un número entero en formato 'coma/miles'
    Ej: x=12563   int_con_comas(x)  ->  12,563
    :param x: valor entero
    :return: string
    """
    if type(x) not in [type(0), type(0L)]:
        raise TypeError("Parameter must be an integer.")
    if x < 0:
        return '-' + int_con_comas(-x)
    result = ''
    while x >= 1000:
        x, r = divmod(x, 1000)
        result = ",%03d%s" % (r, result)
    return "%d%s" % (x, result)


def separar_decimales(x, n=2):
    """
    retorna el valor entero de los decimales que contenga un número cualquiera sea entero o flotante
    :param x: numero que se quiere obtener los decimales
    :param n: cantidad de decimales que se quiere obtener
    :return:
    """
    num_ent = int(x)
    cant_dec = 10**n
    dec = ("0"*n) + str(int(redondeo(redondeo(abs(x - num_ent)) * cant_dec)))  # Se agregan ceros a la izquierda
    return dec[-n:]


def float_con_comas(x):
    """
    retorna la representación de un numero entero o flotante en formato (coma/miles)
    ej: x=1245.36  float_con_comas(x)  ->  1,245.36
    :param x: numero entero o flotante
    :return: string de la representación
    """
    if type(x) not in [type(0), type(0L), type(1.0)]:
        raise TypeError("Parameter must be an integer or float.")

    decimales = separar_decimales(x)

    ent_comas = int_con_comas(int(x))
    return ent_comas + "." + decimales

def validar_url_avatar(url):
    """
    retorna la ruta del avatar del usuario en el caso de que exista, de lo contrario devuelve el avatar por defecto
    :param x: ruta
    :return: string de la ruta
    """
    try:
        if not(url is None or url == "" or url == "''"):

            ruta= str(URL_MEDIA)+str(url)
            test = urllib2.Request(ruta)
            test.get_method = lambda : 'HEAD'
            try:
                response = urllib2.urlopen(test)
                return ruta
            except:
                ruta = MEDIA_URL +"avatar_defecto/usuario_avatar.jpg"
                return ruta
        else:
            ruta = MEDIA_URL +"avatar_defecto/usuario_avatar.jpg"
            return ruta
    except:
        ruta = MEDIA_URL +"avatar_defecto/usuario_avatar.jpg"
        return ruta
def validar_url_logotipo(url):
    """
    retorna la ruta del avatar del usuario en el caso de que exista, de lo contrario devuelve el avatar por defecto
    :param x: ruta
    :return: string de la ruta
    """
    try:
        if not(url is None or url == "" or url == "''"):
            ruta= str(URL_MEDIA)+str(url)
            test = urllib2.Request(ruta)
            test.get_method = lambda : 'HEAD'
            try:
                response = urllib2.urlopen(test)
                return ruta
            except:
                ruta = MEDIA_URL +"avatar_defecto/empresa_avatar.jpg"
                return ruta
    except:
        ruta = MEDIA_URL +"avatar_defecto/empresa_avatar.jpg"
        return ruta


def es_reembolso_gasto(id_documento):
    """
    Retorna verdadero si el documento que ingresó es un reembolso de gasto
    :param id_documento:
    :return:
    """
    try:
        doc = Documento.objects.get(id=id_documento)
        return doc.codigo_documento == '41'
    except (Documento.DoesNotExist, ValueError):
        return False


def es_nota_credito(id_documento):
    """
    Retorna verdadero si el documento que ingresó es un reembolso de gasto
    :param id_documento:
    :return:
    """
    try:
        doc = Documento.objects.get(id=id_documento)
        return doc.codigo_documento == '04'
    except (Documento.DoesNotExist, ValueError):
        return False


def es_nota_debito(id_documento):
    """
    Retorna verdadero si el documento que ingresó es un nota de debito
    :param id_documento:
    :return:
    """
    try:
        doc = Documento.objects.get(id=id_documento)
        return doc.codigo_documento == '05'
    except (Documento.DoesNotExist, ValueError):
        return False


def tiene_doc_modifica(id_documento):
    """
    Retorna verdadero si el documento que ingresó es un nota de debito o una de debito
    :param id_documento:
    :return:
    """
    return es_nota_credito(id_documento) or es_nota_debito(id_documento)

def actualizar_fecha_actual_tipo_comprobante(tipo_comp_id):
    """
    Actuliza la fecha de la secuencia del tipo de comprobante
    :param tipo_comprobante_id: id del tipo de comprobante
    :return: nada
    """

    try:
        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=tipo_comp_id,status=1)
        cabecera_contable = Cabecera_Comp_Contable.objects.filter(status=1,tipo_comprobante_id=tipo_comp_id).order_by('-id','-fecha')

        if len(cabecera_contable) > 0:
            fecha_ultima = cabecera_contable[0].fecha
            secuencia.fecha_actual= fecha_ultima
            secuencia.save()
        else:
            empresa_Parametro = get_parametros_empresa()
            fecha_inicial = empresa_Parametro.fecha_inicio_sistema
            secuencia.fecha_actual= fecha_inicial
            secuencia.save()
    except SecuenciaTipoComprobante.DoesNotExist:
        pass


def get_fila_formulario_formset(request, form):
    """
    Función que obtiene la fila de un formulario cualquiera tipo
    formset_factory
    :param request:
    :param form:
    :return:
    """
    return render_to_string('tags/forms/fila_tabla_formularios.html', {'form': form}, context_instance=RequestContext(request))

def get_url_jasper():
    try:
        urljasper = Config_Jasper.objects.using("base_central").all()[0].url
        return urljasper
    except IndexError:
        return ''

def get_nombre_reporte(codigo):
    try:
        reporte = Reportes_Jasper.objects.using("base_central").get(codigo=codigo)
        return reporte.nombre
    except Reportes_Jasper.DoesNotExist:
        return ''


def get_horadescarga():
    try:
        ahora = datetime.datetime.now()
        print(str(ahora))
        hora = ahora.hour
        minuto = ahora.minute
        seg = ahora.second
        cadena = str(hora)+'_'+str(minuto)+'_'+str(seg)
        return cadena
    except:
        return "00-00-00"