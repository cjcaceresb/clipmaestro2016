import json
from django.utils.encoding import smart_str


class ResponseAngular():
    response = ""

    def __init__(self, body):
        self.response = json.loads(smart_str(body))

    def get(self, dictionary, value=None):
        k = (self.response[dictionary] if self.response.has_key(dictionary) else value)
        return k