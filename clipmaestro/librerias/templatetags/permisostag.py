#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from librerias import views
from librerias.funciones.permisos import tiene_permiso_tag, tiene_permiso_tag_menu, tiene_permiso_tag_submenu
from librerias.funciones.funciones_vistas import *
from contabilidad.models import *

register = template.Library()
condition_tag = views.condition_tag

@register.tag
@condition_tag
def if_tiene_permiso(request, urlname):
    if tiene_permiso_tag(request, urlname):
        return True
    else:
        return False


@register.tag
@condition_tag
def if_tiene_permiso_menu(request, codigo_app):
    if tiene_permiso_tag_menu(request, codigo_app):
        return True
    else:
        return False


@register.tag
@condition_tag
def if_tiene_permiso_submenu(request, url_name):
    """
    Esta funcion sirve para bloquear o permitir en el submenu cada opción
    y recibe un url_name, con este se encuentra el permiso, luego la aplicacion
    y de ahi se valida si existe algun permiso relacionada a ella
    :param request:
    :param url_name:
    :return:
    """
    if tiene_permiso_tag_submenu(request, url_name):
        return True
    else:
        return False


@register.tag
@condition_tag
def if_emite_docs_electronicos():
    """
    Esta función sirve para permitir/bloquear las opciones de menú cuando una empresa
    emite o no documentos electrónicos
    :return:
    """
    return emite_docs_electronicos()

@register.tag
@condition_tag
def if_controla_stock():
    """
    Esta función sirve para indicar si utiliza el formset de la venta
    con cant.ent o sin esa columna y para en la lógica indicar que
    formato de venta realizar si controla el stock o no
    :return:
    """
    return controla_stock()

@register.tag
@condition_tag
def if_estado_asiento_inicial():
    '''
    Snnipet que me va ayudar a bloquear la opción de asiento inicial
    cuando se haya realizado este proceso
    Tipo Comprobante de Asiento de Apertura id=26
    :return:
    '''
    anio_actual = datetime.datetime.now().year

    if len(Cabecera_Comp_Contable.objects.filter(status=1, fecha__year=anio_actual, tipo_comprobante=TipoComprobante.objects.get(id=26))) > 0:
        return True
    else:
        return False

