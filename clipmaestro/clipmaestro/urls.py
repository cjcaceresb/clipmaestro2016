# -*- coding: UTF-8 -*-
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from administracion import urls_administracion
from django.conf import settings
from reportes import url_reportes
from administracion import views
admin.autodiscover()
from librerias.funciones.funciones_vistas import emite_docs_electronicos

#from django.conf.urls.defaults import *
#from wkhtmltopdf.views import PDFTemplateView



handler404 = 'administracion.vistas.page_errors.handler404'
handler500 = 'administracion.vistas.page_errors.handler500'

urlpatterns = patterns('',
    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^', include(urls_administracion)),
    url(r'^', include(url_reportes)),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    )

#Administración Clip Maestro
urlpatterns += patterns('administracion.vistas.administraciones',
url('^administracion/$', 'administracion', name="administracion"),
url('^contabilidad_menu/$', 'contabilidad_menu', name="contabilidad_menu"),
url('^ventas_menu/$', 'ventas_menu', name="ventas_menu"),
url('^compras_menu/$', 'compras_menu', name="compras_menu"),
url('^cm_login/$', 'cm_login', name="cm_login"),
)


# Funciones para django admin
urlpatterns += patterns('administracion.funciones_admin',
url('^administracion/contabilidad/venta_cabecera/obtener_direccion/$',
    'get_cliente_direccion', name="get_cliente_direccion"),
)

#Login
urlpatterns += patterns('administracion.vistas.login',
url(r'^$', 'login_view', name="login"),
url('^logout/$', 'salir', name="salir"),
)


#contabilidad Clientes
urlpatterns += patterns('contabilidad.vistas.cliente',
url('^clientes/registrar/$', 'registrar_cliente', name="registrar_cliente"),
url('^clientes/editar/(?P<id>\d+)/(?P<numero>\d+)/$', 'modificar_cliente', name="modificar_cliente"),
url('^clientes/cambiar/estado/(?P<id>\d+)/$', 'cambiar_estado_cliente', name="cambiar_estado_cliente"),
url('^clientes/eliminar/(?P<id>\d+)/$', 'eliminar_cliente', name="eliminar_cliente"),
url('^clientes/detalle/(?P<id>\d+)/(?P<numero>\d+)/$', 'detalle_cliente', name="detalle_cliente"),
url('^clientes/$', 'clientes', name="clientes"),
)

#Proveedores
urlpatterns += patterns('contabilidad.vistas.proveedores',
        url('^proveedores/registrar/$', 'registrar_proveedor', name="registrar_proveedor"),
        url('^proveedores/modificar/(?P<id>\d+)/(?P<numero>\d+)/$', 'modificar_proveedor', name="modificar_proveedor"),
        url('^proveedores/cambiar/estado/(?P<id>\d+)/$', 'cambiar_estado_proveedor', name="cambiar_estado_proveedor"),
        url('^proveedores/eliminar/(?P<id>\d+)/$', 'eliminar_proveedor', name="eliminar_proveedor"),
        url('^proveedores/detalle/(?P<id>\d+)/(?P<numero>\d+)/$', 'detalle_proveedor', name="detalle_proveedor"),
        url('^proveedores/$', 'proveedores', name="proveedores"),
)

#Banco
urlpatterns += patterns('contabilidad.vistas.banco',
url('^bancos/agregar/$', 'registrar_banco', name="registrar_banco"),
url('^bancos/editar/(?P<id>\d+)/(?P<numero>\d+)/$', 'modificar_banco', name="modificar_banco"),
url('^bancos/cambio_estado/(?P<id>\d+)/$', 'cambiar_estado_cuenta_banco', name="cambiar_estado_cuenta_banco"),
url('^bancos/eliminar/(?P<id>\d+)/$', 'eliminar_banco', name="eliminar_banco"),
url('^bancos/$', 'lista_bancos', name="lista_bancos"),
url('^bancos/detalle/(?P<id>\d+)/(?P<numero>\d+)/$', 'detalle_banco', name="detalle_banco"),

################################### TRANSACCIONES BANCARIAS  ###############################################
url('^bancos/transacciones/$', 'lista_transaccion_banco', name="lista_transaccion_banco"),
url('^bancos/transacciones/detalle/(?P<id>\d+)$', 'detalle_transaccion_banco', name="detalle_transaccion_banco"),
url('^bancos/transacciones/detalle_cheque/(?P<id>\d+)$', 'detalle_cheque', name="detalle_cheque"),
url('^bancos/cheque/detalle/reporte_pdf/(?P<id>\d+)/$', 'detalle_cheque_reportepdf', name="detalle_cheque_reportepdf"),

url('^bancos/transacciones/detalle/reporte_pdf/(?P<id>\d+)/$', 'detalle_transaccion_banco_reportepdf', name="detalle_transaccion_banco_reportepdf"),
url('^bancos/transacciones/agregar/$', 'transacciones_bancarias', name="transacciones_bancarias"),
url('^bancos/transacciones/anular/(?P<id>\d+)$', 'anular_transaccion_bancaria', name="anular_transaccion_bancaria"),
url('^bancos/transacciones/anular/generar/(?P<id>\d+)$', 'anular_generar_transaccion_bancaria', name="anular_generar_transaccion_bancaria"),
url('^bancos/transacciones/copiar/(?P<id>\d+)$', 'copiar_transaccion_bancaria', name="copiar_transaccion_bancaria"),
url('^bancos/conciliacion_bancaria/$', 'conciliacion_bancaria', name="conciliacion_bancaria"),

# Ajax
url('^bancos/transacciones/anular_cheque/$', 'anular_cheque', name="anular_cheque"),
url('^bancos/transacciones/get_ultimo_comprobante_egreso_usado/$', 'get_ultimo_comprobante_egreso_usado', name="get_ultimo_comprobante_egreso_usado"),
url('^bancos/transacciones/get_ultimo_cheque_usado/$', 'get_ultimo_cheque_usado', name="get_ultimo_cheque_usado"),
url('^bancos/concilacion_bancaria/get_transacciones_conciliar/$', 'get_transacciones_conciliar', name="get_transacciones_conciliar"),
url('^bancos/get_secuencia_cheque_transaccion/$', 'get_secuencia_cheque_transaccion', name="get_secuencia_cheque_transaccion"),
url('^bancos/get_fecha_transaccion/$', 'get_fecha_transaccion', name="get_fecha_transaccion"),
)

#Plan de Cuenta
urlpatterns += patterns('contabilidad.vistas.plan_cuenta',
        url('^plancuenta/$', 'lista_plan_cuenta', name="lista_plan_cuenta"),
        url('^plancuenta/agregar/$', 'agregar_plan', name="agregar_plan"),
        url('^plancuenta/editar/$', 'editar_plan', name="editar_plan"),
        url('^plancuenta/eliminar/$', 'eliminar_cuenta', name="eliminar_cuenta"),
        url('^plancuenta/obtenerdatos/$', 'obtener_datos', name="obtener_datos"),
        url('^plancuenta/obtener_tipo_cta/$', 'get_tipo_cuenta', name="get_tipo_cuenta")
)

#Centro de Costos
urlpatterns += patterns('contabilidad.vistas.centro_costos',
    url('^centro_costos/agregar/$', 'registrar_centro_costo', name="registrar_centro_costo"),
    url('^centro_costos/editar/(?P<id>\d+)/$', 'modificar_centro_costo', name="modificar_centro_costo"),
    url('^centro_costos/cambiar/estado/(?P<id>\d+)/$', 'cambiar_estado_centro_costo', name="cambiar_estado_centro_costo"),
    url('^centro_costos/eliminar/(?P<id>\d+)/$', 'eliminar_centro_costo', name="eliminar_centro_costo"),
    url('^centro_costos/$', 'lista_centro_costos', name="lista_centro_costos"),
    url('^centro_costos/reasignar/$', 'rectificar_centro_costo', name="rectificar_centro_costo"),

    url('^centro_costos/get_transacciones_reasignar/$', 'get_transacciones_centro_costo_reasignar', name="get_transacciones_centro_costo_reasignar"),
)

#Vigencia Documento Empresa
urlpatterns += patterns('contabilidad.vistas.vigencia_documento_empresa',
    url('^vigencia_documento/$', 'lista_vigencia_documento_empresa', name="lista_vigencia_documento_empresa"),
    url('^vigencia_documento/detalle/(?P<id>\d+)/$', 'detalle_vigencia_documento_empresa', name="detalle_vigencia_documento_empresa"),
    url('^vigencia_documento/agregar/$', 'agregar_vigencia_documento_empresa', name="agregar_vigencia_documento_empresa"),
    url('^vigencia_documento/editar/(?P<id>\d+)/$', 'editar_vigencia_retencion_empresa', name="editar_vigencia_retencion_empresa"),
    url('^vigencia_documento/eliminar/(?P<id>\d+)/$', 'eliminar_vigencia_documento_empresa', name="eliminar_vigencia_documento_empresa"),
    url('^vigencia_doc_empresa/get_vigencia_doc/$', 'get_vigencia_doc', name='get_vigencia_doc'),
    url('^vigencia_doc_empresa/registrar_vigencia_doc/modulo/$', 'registrar_vigencia_doc', name='registrar_vigencia_doc')
)

#Usuarios
urlpatterns += patterns('administracion.vistas.personas',
    url('^usuarios/cambiar/estado/(?P<id>\d+)/$', 'cambiar_estado_usuarios', name="cambiar_estado_usuarios"),
    url('^usuarios/editar/(?P<id>\d+)/$', 'modificar_persona', name="modificar_persona"),
    url('^usuarios/eliminar/(?P<id>\d+)/$', 'eliminar_persona', name="eliminar_persona"),
    url('^usuarios/detalle/(?P<id>\d+)/$', 'detalle_persona', name="detalle_persona"),
    url('^usuarios/$', 'lista_persona', name="lista_persona"),
    url('^usuarios/agregar/$', 'agregar_persona', name="agregar_persona"),
    url('^usuarios/cambio/contrasenia/$', 'cambiar_contrasenia', name="cambiar_contrasenia"),
    url('^administracion/perfil/(?P<id>\d+)/$', 'perfil', name="perfil"),
)

#Grupos
urlpatterns += patterns('administracion.vistas.grupos',
    url('^administracion/grupos/agregar/$', 'agregar_grupo', name="agregar_grupo"),
    url('^administracion/grupos/editar/(?P<id>\d+)/$', 'editar_grupo', name="editar_grupo"),
    url('^administracion/grupos/$', 'lista_grupos', name="lista_grupos"),
    url('^administracion/grupos/detalle/(?P<id>\d+)/$', 'detalle_grupo', name="detalle_grupo"),
    url('^administracion/grupos/eliminar/(?P<id>\d+)/$', 'eliminar_grupo', name="eliminar_grupo"),

)

#Compras
urlpatterns += patterns('contabilidad.vistas.compras',
    url('^compras/agregar/$', 'agregarcompras', name="agregarcompras"),
    url('^compras/migrar/$', 'migrar_compras', name="migrar_compras"),
    url('^compras/$', 'lista_compras', name="lista_compras"),
    url('^compras/anulargenerar/(?P<id>\d+)/$', 'anular_generar_compra', name="anular_generar_compra"),
    url('^compras/cambiarretencion/$', 'cambiar_retencion', name="cambiar_retencion"),
    url('^compras/tiene_ret/$', 'tiene_retencion_compra', name="tiene_retencion_compra"),
    url('^compras/anularcompra/(?P<id>\d+)/$', 'anular_compra', name="anular_compra"),
    url('^compras/copiarcompra/(?P<id>\d+)/$', 'copiar_compra', name="copiar_compra"),
    url('^compras/usoprovisioncompra/(?P<id>\d+)/$', 'uso_provision_compra', name="uso_provision_compra"),
    url('^compras/detallecompras/(?P<id>\d+)/$', 'detalle_compra', name="detalle_compra"),
    url('^compras/detallecompras_pdf/(?P<id>\d+)/$', 'detalle_compra_pdf', name="detalle_compra_pdf"),
    url('^compras/buscarcuenta/$', 'buscar_cuenta', name="buscar_cuenta"),
    url('^compras/buscarcuentaretenciones/$', 'buscar_cuenta_retenciones', name="buscar_cuenta_retenciones"),
    url('^compras/buscarcuentacodigo/$', 'buscar_cuenta_codigo', name="buscar_cuenta_codigo"),
    url('^compras/buscarcuentacodigoexacto/$', 'buscar_cuenta_codigo_exacto', name="buscar_cuenta_codigo_exacto"),
    url('^compras/buscarproveedor/$', 'buscar_proveedor', name="buscar_proveedor"),
    url('^compras/cuenta_retenciones/$', 'cuentas_retenciones', name="cuentas_retenciones"),
    url('^compras/obtener_datos_retencion/$', 'obtener_datos_retencion', name="obtener_datos_retencion"),
    url('^compras/buscar_retencion/$', 'buscar_retencion', name="buscar_retencion"),
    url('^compras/anular_retencion/$', 'anular_retencion', name="anular_retencion"),
    url('^compras/ultima_ret_usada/$', 'get_ultima_ret_usada', name="get_ultima_ret_usada"),
    url('^compras/buscar_autorizacion/$', 'buscar_autorizacion', name="buscar_autorizacion"),
    url('^compras/getcomboiva/$', 'get_combo_iva', name="get_combo_iva"),
    url('^compras/get_combo_ret_fte/$', 'get_combo_ret_fte', name="get_combo_ret_fte"),
    url('^compras/get_combo_ret_iva/$', 'get_combo_ret_iva', name="get_combo_ret_iva"),
    url('^compras/get_datos_proveedor/$', 'get_datos_proveedor', name="get_datos_proveedor"),
    url('^compras/get_datos_retencion_serie/$', 'get_combo_ret_serie', name="get_combo_ret_serie"),
    url('^compras/get_proveedores/$', 'get_proveedores', name="get_proveedores"),
    url('^compras/get_correo_proveedores/$', 'get_correo_proveedor', name="get_correo_proveedor"),
    url('^compras/verificar_fecha_emi_retencion/$', 'verificar_fecha_vencimiento_retencion', name="verificar_fecha_vencimiento_retencion"),
    url('^compras/buscar_sustento_tributario/$', 'buscar_sustento_tributario', name="buscar_sustento_tributario"),
    url('^compras/buscar_codigo_sri_fte/$', 'buscar_codigo_sri_fte', name="buscar_codigo_sri_fte"),
    url('^compras/buscar_codigo_sri_iva/$', 'buscar_codigo_sri_iva', name="buscar_codigo_sri_iva"),
    url('^compras/buscar_centro_costo/$', 'buscar_centro_costo', name="buscar_centro_costo"),
    url('^compras/buscar_codigo_iva/$', 'buscar_codigo_iva', name="buscar_codigo_iva"),

)

urlpatterns += patterns('contabilidad.views',
        url('^jasper/$', 'get_pdf_compras', name="get_pdf_compras"),  # Prueba jasper
        url('^jasper2/$', 'get_pdf_ventas2', name="get_pdf_ventas2"),  # Prueba jasper
        #url('^reprocesar_detalle_contrato/$', 'reprocesar_detalle_contrato', name="reprocesar_detalle_contrato"),
        url('^excel/read/$', 'leer_excel', name="leer_excel"),  # Prueba lectura excel
    )

# Cuando emite documentos  electrónicos
#if emite_docs_electronicos():
urlpatterns += patterns('contabilidad.vistas.compras',
    url('^compras/enviar_retencion_sri/(?P<id>\d+)/$', 'enviar_retencion_electronica_sri', name="enviar_retencion_electronica_sri"),
    url('^compras/enviar_correo_retencion/$', 'enviar_correo_retencion_electronica_view', name="enviar_correo_retencion_electronica_view"),
)

urlpatterns += patterns('contabilidad.vistas.ventas',
    url('^ventas/enviar_factura_electronica_sri/(?P<id>\d+)/$', 'enviar_factura_electronica_sri', name="enviar_factura_electronica_sri"),
    url('^ventas/enviar_correo_factura/$', 'enviar_correo_factura', name="enviar_correo_factura"),
)

urlpatterns += patterns('contabilidad.views',
    url('^ventas/ridefactura/(?P<id>\d+)/$', 'ride_factura_electronica', name="ride_factura_electronica"),
    url('^compras/rideretencion/(?P<web_url>\w+)/$', 'ride_retencion_electronica', name="ride_retencion_electronica"),
    url('^nota_credito/ridenotacredito/(?P<web_url>\w+)/$', 'ride_nota_credito_electronica', name="ride_nota_credito_electronica"),
)

urlpatterns += patterns('contabilidad.vistas.nota_credito',
    url('^ventas/enviar_nota_credito_electronica_sri/(?P<id>\d+)/$', 'enviar_nota_credito_electronica_sri', name="enviar_nota_credito_electronica_sri"),
    url('^ventas/enviar_correo_nota_credito/$', 'enviar_correo_nota_credito', name="enviar_correo_nota_credito"),
)


#Contabilidad
urlpatterns += patterns('contabilidad.vistas.asiento_contable',
    url('^contabilidad/$', 'lista_asientos', name="lista_asientos"),
    url('^contabilidad/agregar/$', 'registrar_asiento_contable', name="registrar_asiento_contable"),
    url('^contabilidad/agregar/asiento_inicial/$', 'agregar_asiento_inicial', name="agregar_asiento_inicial"),
    url('^contabilidad/copiar/(?P<id>\d+)/$', 'copia_asiento', name="copia_asiento"),
    url('^contabilidad/editar/(?P<id>\d+)/$', 'modificar_asiento', name="modificar_asiento"),
    url('^contabilidad/anular/generar/(?P<id>\d+)$', 'anular_generar_asiento_contable', name="anular_generar_asiento_contable"),
    url('^contabilidad/detalle/(?P<id>\d+)/$', 'detalle_asiento_contable', name="detalle_asiento_contable"),
    url('^contabilidad/anular/(?P<id>\d+)/$', 'anular_asiento_contable', name="anular_asiento_contable"),
    url('^contabilidad/generar_asiento_pdf/(?P<id>\d+)/$', 'generar_asiento_pdf', name="generar_asiento_pdf"),
    url('^contabilidad/buscarcuenta/$', 'buscar_cuenta', name="buscar_cuenta"),
    url('^contabilidad/busquedacuentacodigoajax/$', 'busqueda_cuenta_codigo_exacto', name="busqueda_cuenta_codigo_exacto"),
    url('^contabilidad/get_fecha_secuencia_contabilidad/$', 'get_fecha_secuencia_contabilidad', name="get_fecha_secuencia_contabilidad"),
    url('^contabilidad/cierre_contable/anual/$', 'cierre_anio_contable', name="cierre_anio_contable"),
    url('^contabilidad/cierre_contable/mensual/$', 'cierre_mes_contable', name="cierre_mes_contable"),


)

#Caja Chica
urlpatterns += patterns('contabilidad.vistas.caja_chica',
url('^cajachica/detalle/(?P<id>\d+)/$', 'detalle_compra_caja_chica', name="detalle_compra_caja_chica"),
url('^cajachica/generarpdf/$', 'generar_pdf', name="generar_pdf"),
url('^cajachica/detalle/reportepdf/(?P<id>\d+)/$', 'detalle_compra_caja_chica_pdf', name="detalle_compra_caja_chica_pdf"),
url('^cajachica/anular/(?P<id>\d+)/$', 'anular_compra_caja_chica', name="anular_compra_caja_chica"),
url('^cajachica/anulargenerar/(?P<id>\d+)/$', 'anular_generar_compra_caja_chica', name="anular_generar_compra_caja_chica"),
url('^cajachica/agregar/$', 'registrar_caja_chica', name="vista_caja_chica"),
url('^cajachica/copiar/(?P<id>\d+)/$', 'copiar_caja_chica', name="copiar_caja_chica"),
url('^cajachica/buscar_cuenta/$', 'buscar_cuenta', name="buscar_cuenta"),
url('^cajachica/$', 'lista_compras_caja_chica', name="lista_compras_caja_chica"),
)

#Ventas
urlpatterns += patterns('contabilidad.vistas.ventas',
    url('^ventas/$', 'lista_ventas', name="lista_ventas"),
    #url('^ventas/migrar/$', 'migrar_ret_ventas', name="migrar_ret_ventas"),


    url('^ventas/agregar/$', 'agregar_ventas', name="agregar_ventas"),
    url('^ventas/copiar/(?P<id>\d+)/$', 'copia_ventas', name="copia_ventas"),
    url('^ventas/anular_generar/(?P<id>\d+)/$', 'anular_generar', name="anular_generar"),
    url('^ventas/anular/(?P<id>\d+)/$', 'anular_venta', name="anular_venta"),
    url('^ventas/agregar/venta_provisionada/(?P<id>\d+)/$', 'provision_venta', name="provision_venta"),
    url('^ventas/detalle/(?P<id>\d+)/$', 'detalle_venta', name="detalle_venta"),
    url('^ventas/detalle/reportepdf/(?P<id>\d+)/$', 'detalle_venta_reportepdf', name="detalle_venta_reportepdf"),
    url('^ventas/guia_remision/lista/(?P<id>\d+)/$', 'lista_guias_remision', name="lista_guias_remision"),


    url('^ventas/buscardirecciones/$', 'ajax_seleccionar_direccion', name='ajax_seleccionar_direccion'),
    url('^ventas/seleccionar/documento/$', 'ajax_seleccionar', name='ajax_seleccionar'),
    url('^ventas/busqueda/documentos/serie/$', 'ajax_seleccionar_documentos', name='ajax_seleccionar_documentos'),
    url('^ventas/buscar_item/$', 'buscar_item_venta', name='buscar_item_venta'),
    url('^ventas/buscar_cliente_item/$', 'buscar_cliente_item_venta', name='buscar_cliente_item_venta'),
    url('^ventas/get_cantidades_bodega/$', 'get_cantidades_bodega', name='get_cantidades_bodega'),
    url('^ventas/get_clientes/$', 'get_clientes', name='get_clientes'),
    url('^ventas/agregar_retencion/$', 'agregar_retencion', name='agregar_retencion'),
    url('^ventas/anular_retencion/(?P<id>\d+)/$', 'anular_retencion_venta', name='anular_retencion_venta'),
    url('^ventas/obtener_retencion/$', 'get_retencion_venta', name='get_retencion_venta'),
    url('^ventas/tiene_retencion/$', 'tiene_retencion_venta', name='tiene_retencion_venta'),
    url('^ventas/cantidad_disponible_bodega_stock/$', 'cantidad_disponible_bodega_stock', name='cantidad_disponible_bodega_stock'),
    url('^ventas/agregar_guia_rem_venta/(?P<id>\d+)/$', 'agregar_guia_rem_venta', name='agregar_guia_rem_venta'),
    url('^ventas/anular_guia_rem_venta/(?P<id>\d+)/$', 'anular_guia_rem_venta', name='anular_guia_rem_venta'),
    url('^ventas/tiene_guia_rem/$', 'tiene_guia_rem', name='tiene_guia_rem'),
    url('^ventas/existe_item_bodega/$', 'get_existe_item_bodega', name='get_existe_item_bodega'),
    url('^ventas/seleccion_contratos_cliente/$', 'ajax_seleccion_contratos_cliente', name='ajax_seleccion_contratos_cliente'),
    url('^ventas/rectificar_venta/$', 'rectificar_venta', name='rectificar_venta'),
    url('^ventas/get_ultima_factura_usada/$', 'get_ultima_factura_usada', name='get_ultima_factura_usada'),
    url('^ventas/get_ultima_comprobante_venta_usado/$', 'get_ultima_comprobante_venta_usado', name='get_ultima_comprobante_venta_usado'),
    url('^ventas/get_facturas_vigentes/$', 'valida_documentos_vigentes', name='valida_documentos_vigentes'),
    url('^ventas/guias_remision/existe_guia_rem_venta/$', 'existe_guia_rem_venta', name="existe_guia_rem_venta"),
    url('^ventas/get_correo_clientes/$', 'get_correo_cliente', name="get_correo_cliente"),

    url('^ventas/mantenimiento/$', 'mantenimiento_ventas_inventario', name="mantenimiento_ventas_inventario"),

    ############################################ FACTURA REPORTLAB #########################################################
    url('^ventas/factura_etiqueta/(?P<id>\d+)/$', 'funcion_formato_factura', name='funcion_formato_factura'),
    url('^ventas/factura/(?P<id>\d+)/$', 'factura', name='factura'),

)

# Nota de Crédito -> Devolución Venta
urlpatterns += patterns('contabilidad.vistas.nota_credito',
    url('^ventas/notas_credito/$', 'lista_nota_credito', name="lista_nota_credito"),
    url('^nota_credito/agregar/$', 'agregar_nota_credito', name="agregar_nota_credito"),
    url('^nota_credito/anular_nota_credito/(?P<id>\d+)/$', 'anular_nota_credito', name="anular_nota_credito"),
    url('^nota_credito/detalle/(?P<id>\d+)/$', 'detalle_nota_credito', name="detalle_nota_credito"),
    url('^nota_credito/detalle/pdf/(?P<id>\d+)/$', 'detalle_nota_credito_pdf', name="detalle_nota_credito_pdf"),

    # AJAX
    url('^ajax_documentos_vigentes_nota_credito/$', 'ajax_documentos_vigentes_nota_credito', name='ajax_documentos_vigentes_nota_credito'),
    url('^ajax_seleccionar_nota_credito/$', 'ajax_seleccionar_nota_credito', name='ajax_seleccionar_nota_credito'),
    url('^ajax_seleccion_facturas/$', 'ajax_seleccion_facturas', name='ajax_seleccion_facturas'),
    url('^get_datos_venta/$', 'get_datos_venta', name='get_datos_venta'),
    url('^get_disponible_cantidad_devolver/$', 'get_disponible_cantidad_devolver', name='get_disponible_cantidad_devolver'),
    url('^get_correo_clientes_nc/$', 'get_correo_cliente_nota_credito', name="get_correo_cliente_nota_credito"),
)

urlpatterns += patterns('contabilidad.vistas.provisionar_docs',
    # PROVISIÓN EN VENTAS
    url('^ventas/provision_venta/$', 'provision_doc_venta', name="provision_doc_venta"),
    url('^ventas/seleccionar_ultima_secuencia/$', 'ajax_seleccionar_ultima_secuencia', name="ajax_seleccionar_ultima_secuencia"),
    url('^ventas/seleccionar_documento_prov_venta/$', 'seleccionar_documento_prov_venta', name="seleccionar_documento_prov_venta"),

    url('^ventas/get_documentos_provision/$', 'get_documentos_provision', name="get_documentos_provision"),
    url('^ventas/ajax_seleccionar_ultima_secuencia_compras/$', 'ajax_seleccionar_ultima_secuencia_compras', name="ajax_seleccionar_ultima_secuencia_compras"),

    # PROVISIÓN EN COMPRAS
    url('^compras/provision_compra/$', 'provision_compra', name="provision_compra"),
    url('^compras/provision_compra_retencion/$', 'get_documentos_provision_retencion', name="get_documentos_provision_retencion"),

    # PROVISIÓN CHEQUES
    url('^banco/provision_cheque/$', 'provision_cheque', name="provision_cheque"),
)


#Guia Remision
urlpatterns += patterns('contabilidad.vistas.guia_remision_view',
    url('^guias_remision/$', 'listado_todas_guias_remision', name="listado_todas_guias_remision"),
    url('^guias_remision/anular_guia_rem/(?P<id>\d+)/$', 'anular_guia_rem', name="anular_guia_rem"),

    url('^guias_remision/detalle/(?P<id>\d+)/$', 'detalle_guia_remision', name="detalle_guia_remision"),
    url('^guias_remision/detalle/pdf/(?P<id>\d+)/$', 'detalle_guia_remision_pdf', name="detalle_guia_remision_pdf"),
    url('^guias_remision/vigencia_doc_guia_remision/$', 'valida_documentos_guia_remision_vigentes', name='valida_documentos_guia_remision_vigentes'),
)

#Cuentas por pagar
urlpatterns += patterns('contabilidad.vistas.cuentas_por_pagar',
    url('^cuentas_pagar/pagos$', 'lista_pago_proveedores', name="lista_pago_proveedores"),
    url('^cuentas_pagar/lista_proveedores_pagar/lista_pagos/detalle_pago_proveedor/(?P<id>\d+)/$', 'detalle_pago_proveedores', name="detalle_pago_proveedores"),
    url('^cuentas_pagar/lista_proveedores_pagar/lista_pagos/detalle_pago_proveedor/anularpago/(?P<id>\d+)/$', 'anular_pago', name="anular_pago"),
    url('^cuentas_pagar/lista_proveedores_pagar/lista_pagos/detalle_pago_proveedor_pdf/(?P<id>\d+)/$', 'detalle_pago_proveedores_pdf', name="detalle_pago_proveedores_pdf"),
    url('^cuentas_pagar/pago_proveedores/agregar_pago$', 'pago_proveedores', name="pago_proveedores"),
    url('^cuentas_pagar/pago_proveedores/detalle_cheque_c_x_p/(?P<id>\d+)$', 'detalle_cheque_c_x_p', name="detalle_cheque_c_x_p"),
    url('^cuentas_pagar/lista_proveedores_pagar/cruce_documentos/$', 'cruce_documentos_cuentas', name="cruce_documentos_cuentas"),
    url('^cuentas_pagar/lista_proveedores_pagar/cruce_documentos/buscar_cta/$', 'buscar_cuenta', name="buscar_cuenta"),
    url('^cuentas_pagar/pago_proveedores/agregar_pago/buscar_cuentas_pagar$', 'cargar_cuentas_pagar', name="cargar_cuentas_pagar"),
    url('^cuentas_pagar/pago_proveedores/agregar_pago/cruce_cta_doc/buscar_cruce_doc$', 'cargar_cruce_doc', name="cargar_cruce_doc"),
    url('^cuentas_pagar/pago_proveedores/agregar_pago/cruce_cta_doc/buscar_cruce_cta$', 'cargar_cruce_cta', name="cargar_cruce_cta"),
    url('^cuentas_pagar/pago_proveedores/agregar_pago/get_ultimo_cheque$', 'get_ultimo_cheque', name="get_ultimo_cheque"),
    url('^cuentas_pagar/pago_proveedores/agregar_pago/get_fecha_actual_comprobante$', 'get_fecha_actual_comprobante', name="get_fecha_actual_comprobante"),
    url('^cuentas_pagar/pago_proveedores/agregar_pago/get_cheque_provisionado$', 'get_cheques_provisionados', name="get_cheques_provisionados"),
    url('^cuentas_pagar/cuentas_x_pagar_inicial$', 'lista_cuentas_x_pagar_iniciales', name="lista_cuentas_x_pagar_iniciales"),
    url('^cuentas_pagar/cuentas_x_pagar_inicial/agregar$', 'cuentas_x_pagar_iniciales', name="cuentas_x_pagar_iniciales"),
    url('^cuentas_pagar/cuentas_x_pagar_inicial/eliminar/(?P<id>\d+)/$', 'eliminar_cuentas_x_pagar_iniciales', name="eliminar_cuentas_x_pagar_iniciales"),
    url('^cuentas_cobrar/reversar_pago/(?P<id>\d+)/$', 'reversar_pago', name="reversar_pago"),
)

#Categoria_Item
urlpatterns += patterns('contabilidad.vistas.categoria_item',
    url('^categoria_item/agregar/$', 'registrar_categoria_item', name="registrar_categoria_item"),
    url('^categoria_item/eliminar/(?P<id>\d+)/$', 'eliminar_categoria_item', name="eliminar_categoria_item"),
    url('^categoria_item/editar/(?P<id>\d+)/$', 'editar_categoria_item', name="editar_categoria_item"),
    url('^categoria_item/cambiar/estado/(?P<id>\d+)/$', 'cambiar_estado_categoria_item', name="cambiar_estado_categoria_item"),
    url('^categoria_item/buscar_cuenta/$', 'buscar_cuenta', name="buscar_cuenta"),
    url('^categoria_item/buscar_cuenta/inventario/$', 'buscar_cuenta_inventario_compra', name="buscar_cuenta_inventario_compra"),
    url('^categoria_item/buscar_cuenta/venta/$', 'buscar_cuenta_venta', name="buscar_cuenta_venta"),
    url('^categoria_item/buscar_cuenta/costo_venta/$', 'buscar_cuenta_costo_venta', name="buscar_cuenta_costo_venta"),
    url('^categoria_item/$', 'lista_categoria', name="lista_categoria"),
    url('^categoria_item/detalle/(?P<id>\d+)/$', 'detalle_categoria_item', name="detalle_categoria_item"),
)


#Item
urlpatterns += patterns('contabilidad.vistas.items',
    url('^items/agregar/$', 'registrar_items', name="registrar_items"),
    url('^items/eliminar/(?P<id>\d+)/$', 'eliminar_items', name="eliminar_items"),
    url('^items/cambiar/estado/(?P<id>\d+)/$', 'cambiar_estado_item', name="cambiar_estado_item"),
    url('^items/editar/(?P<id>\d+)/$', 'editar_items', name="editar_items"),
    url('^items/$', 'lista_items', name="lista_items"),
    url('^items/detalle/(?P<id>\d+)/$', 'detalle_item', name="detalle_item"),
)


#Cuentas por Cobrar
urlpatterns += patterns('contabilidad.vistas.cuentas_por_cobrar',
    url('^cuentas_cobrar/$', 'lista_cobro', name="lista_cobro"),
    url('^cuentas_cobrar/agregar_cobro/$', 'cobro_clientes', name="cobro_clientes"),
    url('^cuentas_cobrar/agregar_cobro/cargar_doc_cobrar/$', 'cargar_documentos_cobrar', name="cargar_documentos_cobrar"),
    url('^cuentas_cobrar/cruce_documentos/$', 'cruce_documentos_cobros', name="cruce_documentos_cobros"),
    url('^cuentas_cobrar/cruce_documentos/cargar_cruce_doc$', 'cargar_cruce_documentos_cobros', name="cargar_cruce_documentos_cobros"),
    url('^cuentas_cobrar/cruce_documentos/cargar_cruce_cuentas$', 'cargar_cruce_cuentas_cobros', name="cargar_cruce_cuentas_cobros"),
    url('^cuentas_cobrar/detalle_cobro/(?P<id>\d+)/$', 'detalle_cobro', name="detalle_cobro"),
    url('^cuentas_cobrar/anular_cobro/(?P<id>\d+)/$', 'anular_cobro', name="anular_cobro"),
    url('^cuentas_cobrar/reversar_cobro/(?P<id>\d+)/$', 'reversar_cobro', name="reversar_cobro"),
    url('^cuentas_cobrar/detalle_cobro_pdf/(?P<id>\d+)/$', 'detalle_cobro_pdf', name="detalle_cobro_pdf"),
    url('^cuentas_cobrar/cuentas_x_cobrar_inicial$', 'lista_cuentas_x_cobrar_iniciales', name="lista_cuentas_x_cobrar_iniciales"),
    url('^cuentas_cobrar/cuentas_x_cobrar_inicial/agregar$', 'cuentas_x_cobrar_iniciales', name="cuentas_x_cobrar_iniciales"),
    url('^cuentas_cobrar/cuentas_x_cobrar_inicial/eliminar/(?P<id>\d+)/$', 'eliminar_cuentas_x_cobrar_iniciales', name="eliminar_cuentas_x_cobrar_iniciales")
)



#Inventario
urlpatterns += patterns('contabilidad.vistas.inventario',
    url('^inventario/$', 'lista_inventario', name="lista_inventario"),
    url('^inventario/detalle/(?P<id>\d+)/$', 'detalle_inventario', name="detalle_inventario"),
    url('^inventario/detalle_pdf/(?P<id>\d+)/$', 'detalle_inventario_pdf', name="detalle_inventario_pdf"),
    url('^inventario/registrar/$', 'registrar_inventario', name="registrar_inventario"),
    url('^inventario/guia_compra/$', 'convertir_guia_compra', name="convertir_guia_compra"),
    url('^inventario/guia_compra/cargar_guia_compra/$', 'cargar_guia_compra', name="cargar_guia_compra"),
    url('^inventario/buscar_item/$', 'buscar_item', name="buscar_item"),
    url('^inventario/buscar_cuenta/$', 'buscar_cuenta', name="buscar_cuenta"),
    url('^inventario/get_comp_inv/$', 'get_compras_inventario', name="get_compras_inventario"),
    url('^inventario/registrar/validar_fecha_inv_ie/$', 'validar_fecha_inventario_ie', name="validar_fecha_inventario_ie"),
    url('^inventario/modificar_inventario/$', 'modificar_inventario', name="modificar_inventario"),
    url('^inventario/recambio_inventario/excel/$', 'recambio_inventario', name="recambio_inventario"),
    #################################NUEVA SECCION -  CONVERSION DE ITEMS###############################################
    url('^inventario/conversion_items/$', 'conversion_items', name="conversion_items"),
    url('^buscar_item_conversion/$', 'buscar_item_conversion', name="buscar_item_conversion"),
    url('^get_datos_conversion/$', 'get_datos_conversion', name="get_datos_conversion"),
    url('^get_factor_conversion/$', 'get_factor_conversion', name="get_factor_conversion"),
    url('^validar_cabecera/$', 'validar_cabecera', name='validar_cabecera'),
    url('^validar_detalle/$', 'validar_detalle', name='validar_detalle'),
    url('^validate_stock_item/$', 'actualizar_stock_item', name="actualizar_stock_item"),
    url('^grabar_conversion/$', 'grabar_conversion', name="grabar_conversion"),

    ###################################### AJAX CONTRATO ###############################################################
    url('^inventario/buscar_item_contrato/$', 'buscar_item_contrato', name="buscar_item_contrato"),
    url('^inventario/get_cantidades_bodega_det_inventario/$', 'get_cantidades_bodega_det_inventario', name="get_cantidades_bodega_det_inventario"),
    url('^inventario/get_existe_item_bodega_contrato/$', 'get_existe_item_bodega_contrato', name="get_existe_item_bodega_contrato"),
    url('^inventario/get_select_cliente_contrato/$', 'get_select_cliente_contrato', name="get_select_cliente_contrato"),
    ####################################################################################################################
    ###############################CIERRE ANUAL############
    url('^inventario/cierre_anual/$', 'cierre_anual_inventario', name="cierre_anual_inventario"),
    url('^inventario/repaso_proceso_inventario/$', 'repaso_reproceso_inventario', name="repaso_reproceso_inventario"),

)

#Contrato
urlpatterns += patterns('contabilidad.vistas.contrato',
    url('^contrato/$', 'lista_contrato', name="lista_contrato"),
    url('^contrato/agregar/$', 'agregar_contrato', name="agregar_contrato"),
    url('^contrato/detalle/(?P<id>\d+)/$', 'detalle_contrato', name="detalle_contrato"),
    url('^contrato/editar/(?P<id>\d+)/$', 'editar_contrato', name="editar_contrato"),
    url('^contrato/anular/(?P<id>\d+)/$', 'anular_contrato', name="anular_contrato"),
    url('^contrato/costos_adicionales/$', 'costo_contrato', name="costo_contrato"),
    ######################################## AJAX ######################################################################
    url('^contrato/centro_costos/get_facturas/$', 'get_facturas_costo_contrato', name="get_facturas_costo_contrato"),
    url('^contrato/centro_costos/get_valor_costo_contrato/$', 'get_valor_costo_contrato', name="get_valor_costo_contrato"),


)

#Vendedores
urlpatterns += patterns('contabilidad.vistas.colaboradores',
    url('^vendedores/$', 'lista_vendedores', name="lista_vendedores"),
    url('^vendedores/agregar/$', 'agregar_vendedores', name="agregar_vendedores"),
    url('^vendedores/editar/(?P<id>\d+)/$', 'editar_vendedores', name="editar_vendedores"),
    url('^vendedores/ver/(?P<id>\d+)/$', 'ver_vendedores', name="ver_vendedores"),
    url('^vendedores/eliminar/(?P<id>\d+)/$', 'eliminar_vendedores', name="eliminar_vendedores"),
    url('^vendedores/cambiar_estado/(?P<id>\d+)/$', 'cambiar_estado_vendedores', name="cambiar_estado_vendedores"),
)

#Empleados
urlpatterns += patterns('contabilidad.vistas.empleados',
    url('^empleados/$', 'lista_empleados', name="lista_empleados"),
    url('^empleados/agregar/$', 'agregar_empleado', name="agregar_empleado"),
    url('^empleados/editar/(?P<id>\d+)/$', 'editar_empleado', name="editar_empleado"),
    url('^empleados/cambiar_estado_empleado/(?P<id>\d+)/$', 'cambiar_estado_empleado', name="cambiar_estado_empleado"),
    url('^empleados/ver/(?P<id>\d+)/$', 'detalle_empleado', name="detalle_empleado"),
    url('^empleados/eliminar/(?P<id>\d+)/$', 'eliminar_empleado', name="eliminar_empleado"),
    url('^empleados/sueldo_mensual/$', 'generar_sueldo_empleado', name="generar_sueldo_empleado"),
    url('^empleados/get_detalle_empleados_sueldo/$', 'get_detalle_empleados_sueldo', name="get_detalle_empleados_sueldo"),
)

#SRI
urlpatterns += patterns('contabilidad.vistas.sri',
    url('^sri/ats/$', 'vista_ATS', name="vista_ATS"),
    url('^sri/ats/editar/$', 'editar_ats', name="editar_ats"),
    url('^sri/101/$', 'vista_101', name="vista_101"),
    url('^sri/103/$', 'vista_103', name="vista_103"),
    url('^sri/104/$', 'vista_104', name="vista_104"),
)


urlpatterns += patterns('contabilidad.vistas.busquedas_ajax',
    url('^busquedas/documentos/$', 'buscar_tipo_documento', name="buscar_tipo_documento"),
    url('^busquedas/es_n_c/$', 'es_nota_credito_view', name="es_nota_credito"),
    url('^busquedas/es_n_d/$', 'es_nota_debito_view', name="es_nota_debito"),
    url('^busquedas/es_reembolso/$', 'es_reembolso_gasto_view', name="es_reembolso_gasto"),
    url('^busquedas/tiene_doc_modifica/$', 'tiene_doc_modifica_view', name="tiene_doc_modifica"),
    url('^busquedas/origen_pago_sri/$', 'buscar_origen_pago_sri', name="buscar_origen_pago_sri"),
    url('^busquedas/forma_pago_sri/$', 'buscar_forma_pago_sri', name="buscar_forma_pago_sri"),
    url('^busquedas/contratos/$', 'buscar_contratos', name="buscar_contratos"),
    url('^busquedas/es_pago_credito/$', 'es_pago_credito', name="es_pago_credito"),
    url('^busquedas/buscar_paises/$', 'buscar_paises', name="buscar_paises"),
    url('^busquedas/buscar_block_retenciones/$', 'buscar_block_retenciones', name="buscar_block_retenciones"),
    url('^busquedas/buscar_cuentas_retencion/$', 'buscar_cuentas_retencion', name="buscar_cuentas_retencion"),
    url('^busquedas/buscar_items/$', 'buscar_items', name="buscar_items"),
    url('^busquedas/buscar_clientes/$', 'buscar_clientes', name="buscar_clientes"),
    url('^busquedas/buscar_ciudad/$', 'buscar_ciudad', name="buscar_ciudad"),

    url('^busquedas/get_fila_form_compra_inventario/$', 'get_fila_form_compra_inventario', name="get_fila_form_compra_inventario"),
    url('^busquedas/get_fila_form_ajuste_cantidad_inventario/$', 'get_fila_form_ajuste_cantidad_inventario', name="get_fila_form_ajuste_cantidad_inventario"),
    url('^busquedas/get_fila_form_ajuste_costo_inventario/$', 'get_fila_form_ajuste_costo_inventario', name="get_fila_form_ajuste_costo_inventario"),
    url('^busquedas/get_fila_form_guia_rem_inventario/$', 'get_fila_form_guia_rem_inventario', name="get_fila_form_guia_rem_inventario"),

    url('^busquedas/get_fila_form_compra_inicial_inventario/$', 'get_fila_form_compra_inicial_inventario', name="get_fila_form_compra_inicial_inventario"),
    url('^busquedas/get_fila_form_produccion_inventario/$', 'get_fila_form_produccion_inventario', name="get_fila_form_produccion_inventario"),
    url('^busquedas/get_fila_form_consumo_interno_inventario/$', 'get_fila_form_consumo_interno_inventario', name="get_fila_form_consumo_interno_inventario"),

    url('^busquedas/es_compra_exterior/$', 'es_compra_exterior', name="es_compra_exterior"),
    url('^busquedas/calcular_totales_detalle_compra/$', 'calcular_totales_detalle_compra', name="calcular_totales_detalle_compra"),
    url('^busquedas/calcular_totales_detalle_retencion/$', 'calcular_totales_detalle_retencion', name="calcular_totales_detalle_retencion"),
    url('^busquedas/agregar_fila_det_compra/$', 'agregar_fila_det_compra', name="agregar_fila_det_compra"),

    url('^busquedas/calcular_totales_compra_inventario/$', 'calcular_totales_compra_inventario', name="calcular_totales_compra_inventario"),

    url('^busquedas/get_datos_codigo_sri/$', 'get_datos_codigo_sri', name="get_datos_codigo_sri"),
    url('^busquedas/get_unidad_medida_item/$', 'get_unidad_medida_item', name="get_unidad_medida_item"),
    url('^busquedas/get_costo_item/$', 'get_costo_item', name="get_costo_item"),

    url('^busquedas/cuentas_x_cobrar/esta_reversado/$', 'esta_reversado', name="esta_reversado"),
    url('^busquedas/cuentas_x_cobrar/get_fecha_reversar/$', 'get_fecha_reversar', name="get_fecha_reversar"),

    url('^busquedas/cuentas_x_cobrar/esta_reversado_pago/$', 'esta_reversado_pago', name="esta_reversado_pago"),
    url('^busquedas/cuentas_x_cobrar/get_fecha_reversar_pago/$', 'get_fecha_reversar_pago', name="get_fecha_reversar_pago"),

)

import gc
gc.collect()
