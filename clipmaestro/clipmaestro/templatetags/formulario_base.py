__author__ = 'Clip Maestro'
#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *
from django.template.loader import render_to_string
from django.template import RequestContext
from librerias.funciones.funciones_vistas import get_fila_formulario_formset
register = template.Library()


@register.simple_tag(name='formulario')
def formulario(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/formulario_base.html')(formulario)


@register.simple_tag(name='formulario_doble')
def formulario_doble(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/formulario_doble_columna.html')(formulario_doble)


@register.simple_tag(name='formulario_popup')
def formulario_popup(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/formulario_popup.html')(formulario_popup)


@register.simple_tag(name='formulario_cab_rol')
def formulario_cab_rol(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/formulario_cabecera_rol.html')(formulario_cab_rol)


@register.simple_tag(name='tabla_formset')
def tabla_formset(request, form):
    return render_to_string('tags/forms/tabla_formularios.html', {'formset': form}, context_instance=RequestContext(request))


@register.simple_tag(name='fila_tabla_formset')
def fila_tabla_formset(request, form):
    return get_fila_formulario_formset(request, form)
