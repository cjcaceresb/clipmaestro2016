
#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from contabilidad.models import *
from librerias.funciones.funciones_vistas import *

register = template.Library()
__author__ = 'Clip Maestro'

@register.inclusion_tag('Compras/snnippet_comp_ret.html')
def retencion_snippet(id, tipo):
    try:
        retencion = RetencionCompraCabecera.objects.get(compra_cabecera=CompraCabecera.objects.get(id=id))
    except:
        retencion = None
        tipo = None
    return {'retencion': retencion, 'tipo': tipo}

@register.inclusion_tag('Compras/snnippet_comp_sust_trib.html')
def sust_tribut_snippet(codigo):
    if codigo<10:
        sustento = Sustento_Tributario.objects.get(codigo="0"+str(codigo))
    else:
        sustento = Sustento_Tributario.objects.get(codigo=str(codigo))
    return {'sustento': sustento}

@register.inclusion_tag('tags/operaciones.html')
def multiplicacion_snippet(a, b):
    try:
        multiplicacion = a * b
        return {'obj': multiplicacion}
    except:
        return {'obj': 0.0}

@register.inclusion_tag('tags/operaciones.html')
def resta_snippet(a, b):
    resta = a-b
    return {'obj': resta}


@register.inclusion_tag('tags/operaciones.html')
def suma_2_snippet(a, b):
    suma = a+b
    return {'obj': suma}

@register.inclusion_tag('tags/operaciones.html')
def get_pagos_proveedor_doc(id, fecha_ini, fecha_fin):
    """
    Funcion que me permite obtener la cantidad de pago que se hizo en ese
    lapso de tiempo con ese proveedor

    #### if cuentas_x_pagar.naturaleza == 1: ####
    Se pregunta por la naturaleza ya que cuando es
    un anticipo se crea automaticamente un registro en cxp_pago, justo ese registro es el que
    se tiene que ignorar ya que es el registro donde se registro el anticipo, al contrario en
    compras cuando se registra una factura, no se crea un registro en cxp_pago
    """
    total_pago = 0.0
    cuentas_x_pagar = Cuentas_por_Pagar.objects.get(id=id)
    if cuentas_x_pagar.naturaleza == 1:  # Anticipo
        # reg_ant: es el registro cuando se hizo el anticipo, ese no se debe de contar en el total de pago
        reg_ant = CXP_pago.objects.filter(cuentas_x_pagar=cuentas_x_pagar,
                                           pago__fecha_reg__range=(fecha_ini,
                                                                   fecha_fin)).order_by("id").reverse()[0]

        for obj in CXP_pago.objects.filter(cuentas_x_pagar=cuentas_x_pagar,
                                           pago__fecha_reg__range=(fecha_ini, fecha_fin),
                                           status=1).order_by("id"):
            total_pago += obj.monto
        total_pago -= reg_ant.monto
    else:
        for obj in CXP_pago.objects.filter(cuentas_x_pagar=cuentas_x_pagar,
                                           pago__fecha_reg__range=(fecha_ini, fecha_fin),
                                           status=1):
            total_pago += obj.monto
    return {'obj': total_pago}

@register.inclusion_tag('tags/operaciones.html')
def suma_n(*args):
    suma = 0.0
    try:
        for obj in args:
            suma += float(obj)
    except:
        pass
    return {'obj': suma}

@register.inclusion_tag('tags/operaciones.html')
def descuento_snippet(a, b, c):
    subtotal = a*b
    porc_des = c/100
    total_desc = subtotal*porc_des
    return {'obj': total_desc}

@register.inclusion_tag('tags/operaciones.html')
def subtotal_venta_snippet(a, b, c):
    subtotal = a*b
    porc_des = c/100
    total_desc = subtotal - subtotal*porc_des
    return {'obj': total_desc}

@register.inclusion_tag('tags/operaciones.html')
def subtotal_venta_resta_snippet(a, b, c):
    subtotal = a*b
    total_desc = subtotal - c
    return {'obj': total_desc}

@register.inclusion_tag('tags/operaciones.html')
def descuento_venta_porcentaje_snippet(a, b, c):
    total_desc = (a*b)
    porc = (c/100)
    resultado = total_desc*porc
    return {'obj': resultado}

@register.inclusion_tag('tags/operaciones.html')
def descuento_venta_resta_porcentaje_snippet(a, b, c):
    total_desc = (a*b)
    porc = (c/100)
    print "porc ", porc
    resultado = total_desc*porc
    final = total_desc - resultado
    return {'obj': final}

@register.inclusion_tag('tags/operaciones.html')
def divicion_snippet(a, b, c=2):
    try:
        divicion = a/b
    except ZeroDivisionError:
        divicion = 0

    return {'obj': redondeo(divicion, c)}


@register.inclusion_tag('tags/tiene_retencion.html')
def tiene_ret(obj):
    try:
        retencion = RetencionCompraCabecera.objects.get(compra_cabecera=obj, status=1)
        flag = True
        return{'flag': flag, "obj": obj}
    except:
        flag = False
        return{'flag': flag}

@register.inclusion_tag('tags/operaciones.html')
def subtract(*args):
    total = float(args[0])
    if len(args) > 1:
        for i in range(1, len(args)):
            total = total - float(args[i])
    else:
        total = 0
    return {'obj': total}

class MakeListNode(template.Node):
    def __init__(self, items, varname):
        self.items = map(template.Variable, items)
        self.varname = varname

    def render(self, context):
        context[self.varname] = [ i.resolve(context) for i in self.items ]
        return ""

@register.tag
def make_list(parser, token):
    """
    Funcion que recibe n parametros y los devuelve como una lista
    """
    bits = list(token.split_contents())
    if len(bits) >= 4 and bits[-2] == "as":
        varname = bits[-1]
        items = bits[1:-2]
        return MakeListNode(items, varname)
    else:
        raise template.TemplateSyntaxError("%r expected format is 'item [item ...] as varname'" % bits[0])


@register.simple_tag
def SumaResta(a, b):
    t_suma = 0.0
    t_resta = 0.0
    for obj in a:
        t_suma += cero_if_none(obj)
    for obj in b:
        t_resta += cero_if_none(obj)
    return str("%.2f" % round((t_suma - t_resta), 2))

@register.simple_tag
def TpagarCompReten(a, b, c, d, e):
    try:
        return '%.2f' % float(a+b+c-d-e)
    except:
        return "0"

#Verificacion Retencion a la fuente Base*%codigo y base = retfut + retiva
@register.inclusion_tag('tags/operaciones.html')
def verificacion_retencion_snippet(a, b):
    verificacion = a*(b/100)
    return {'obj': verificacion}


#Diferencia Retencion a la fuente Retencion - Verificacion
@register.inclusion_tag('tags/operaciones.html')
def diferencia_retencion_snippet(a, b, c):
    diferencia = a - ((b * c/100))
    return {'obj': diferencia}


@register.inclusion_tag('tags/operaciones.html')
def calcular_iva(obj_compra_cabecera):
    total_iva = 0.0
    for obj in CompraDetalle.objects.filter(compra_cabecera=obj_compra_cabecera):
        total_iva += obj.monto_iva
    return {'obj': total_iva}

@register.inclusion_tag('tags/operaciones.html')
def calcular_subtotal_cero(obj_compra_cabecera):
    total_base0 = 0.0
    for obj in CompraDetalle.objects.filter(compra_cabecera=obj_compra_cabecera):
        if obj.monto_iva == 0:
            total_base0 += obj.valor
    return {'obj': total_base0}

@register.inclusion_tag('tags/operaciones.html')
def calcular_subtotal_iva(obj_compra_cabecera):
    total_base12 = 0.0
    for obj in CompraDetalle.objects.filter(compra_cabecera=obj_compra_cabecera):
        if obj.monto_iva != 0:
            total_base12 += obj.valor
    return {'obj': total_base12}

@register.inclusion_tag('tags/operaciones.html')
def calcular_total_retenciones(obj_compra_cabecera):
    total_retenciones = 0.0
    try:
        retencion = RetencionCompraCabecera.objects.get(compra_cabecera=obj_compra_cabecera, status=1)
        for obj in RetencionCompraDetalle.objects.filter(retencion_cabecera_compra=retencion, status=1):
            total_retenciones += obj.valor_retenido
        return {'obj': total_retenciones}
    except:
        return {'obj': total_retenciones}

#### Cuentas por pagar  #####
@register.simple_tag
def saldo_anterior(id):
    cxp_pago = CXP_pago.objects.get(id=id)
    cta_pagar = Cuentas_por_Pagar.objects.get(id=cxp_pago.cuentas_x_pagar_id)
    pago = Pago.objects.get(id=cxp_pago.pago_id)

    if cta_pagar.documento.codigo_documento == "AN":  # Cuando es un Anticipo del cliente ahy que omitir el primer registro de pago de anticipo
        pagos_ant = Pago.objects.filter(cxp_pago__cuentas_x_pagar__id=cta_pagar.id, fecha_reg__lt=pago.fecha_reg, status=1)
        if len(pagos_ant) > 2:
            pagos_ant = pagos_ant[1:]
        else:
            pagos_ant = []
    else:  # Otros documentos
        pagos_ant = Pago.objects.filter(cxp_pago__cuentas_x_pagar__id=cta_pagar.id, fecha_reg__lt=pago.fecha_reg, status=1)


    total_pag_ant = 0.0
    print "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    for obj in pagos_ant:
        total_pag_ant += obj.monto
    print "total_pagar_ant", total_pag_ant
    print "monto: ", cta_pagar.monto
    saldo_ant = cta_pagar.monto - total_pag_ant
    print "saldo: ", saldo_ant

    return saldo_ant

### Split dentro del template #####
@register.simple_tag
def splits(string, splitter, i):
    numero = ''
    try:
        if len(str(string).split(splitter)[i]) < 2:
            numero = (str(string).split(splitter)[i])+"0"
        else:
            numero = (str(string).split(splitter)[i])
        return numero #str(string).split(splitter)[i]
    except:
        return ""

@register.simple_tag
def doc_selected(valor):
    if float(valor) > 0:
        return "checked = true"
    else:
        return ""


### Saldo de Reporte Banco ###
@register.simple_tag
def get_saldo_banco_tag(banco, saldo_ini):
    total_credito = 0.0

    if banco.naturaleza == 1:
        print "debito"
        return banco.valor
    else:
        print "credito"
        saldo_ini = saldo_ini - banco.valor
        total_credito += saldo_ini
        return total_credito


@register.inclusion_tag('tags/transacciones/contrato_detalle.html')
def detalle_contrato(id):
    try:
        detalle = ContratoDetalle.objects.get(status=1,id=id)

        if detalle.tipo_comprobante_id == 15 or detalle.tipo_comprobante_id == 14:
            inventario = Inventario_Cabecera.objects.get(status=1,id=detalle.modulo_id)
            return {'inventario': inventario,'tipo':1}

        elif detalle.tipo_comprobante_id == 2:
            venta = Venta_Cabecera.objects.get(status=1,id=detalle.modulo_id)
            return {'venta': venta,'tipo':2}


        elif detalle.tipo_comprobante_id == 1:
            compra = CompraCabecera.objects.get(status=1,id=detalle.modulo_id)
            #compra.num_comp
            return {'compra': compra,'tipo':3}

        else:
            pass
    except:
        pass


@register.inclusion_tag('tags/transacciones/contrato_detalle_pdf.html')
def detalle_contrato_pdf(id):
    try:
        detalle = ContratoDetalle.objects.get(status=1,id=id)

        if detalle.tipo_comprobante_id == 15 or detalle.tipo_comprobante_id == 14:
            inventario = Inventario_Cabecera.objects.get(status=1,id=detalle.modulo_id)
            return {'inventario': inventario,'tipo':1}

        elif detalle.tipo_comprobante_id == 2:
            venta = Venta_Cabecera.objects.get(status=1,id=detalle.modulo_id)
            return {'venta': venta,'tipo':2}


        elif detalle.tipo_comprobante_id == 1:
            compra = CompraCabecera.objects.get(status=1,id=detalle.modulo_id)
            #compra.num_comp
            return {'compra': compra,'tipo':3}

        else:
            pass
    except:
        pass