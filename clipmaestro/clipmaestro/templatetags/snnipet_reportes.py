#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from contabilidad.models import *
from administracion.models import *
from django.contrib import messages
from reportes.models import *
from clipmaestro.templatetags.formulario_base import *

register = template.Library()

@register.simple_tag(name='get_total_debito')
def get_total_debito(saldo_cuenta, mes, es_acum):
    if es_acum:  # Total acumulado
        total = saldo_cuenta.saldo_inicial
        if mes == 1:  # Enero
            pass
        elif mes == 2:  # Febrero
            total += saldo_cuenta.debe1
        elif mes == 3:  # Marzo
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2
        elif mes == 4:  # Abril
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + saldo_cuenta.debe3
        elif mes == 5:  # Mayo
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + saldo_cuenta.debe3 + saldo_cuenta.debe4
        elif mes == 6:  # Junio
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + \
                            saldo_cuenta.debe3 + saldo_cuenta.debe4 + saldo_cuenta.debe5 + saldo_cuenta.debe6
        elif mes == 7:  # Julio
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + \
                            saldo_cuenta.debe3 + saldo_cuenta.debe4 + \
                            saldo_cuenta.debe5 + saldo_cuenta.debe6 + saldo_cuenta.debe7
        elif mes == 8:  # Agosto
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + \
                            saldo_cuenta.debe3 + saldo_cuenta.debe4 + \
                            saldo_cuenta.debe5 + saldo_cuenta.debe6 + \
                            saldo_cuenta.debe7 + saldo_cuenta.debe8
        elif mes == 9:  # Septiembre
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + \
                            saldo_cuenta.debe3 + saldo_cuenta.debe4 + \
                            saldo_cuenta.debe5 + saldo_cuenta.debe6 + \
                            saldo_cuenta.debe7 + saldo_cuenta.debe8 + saldo_cuenta.debe9
        elif mes == 10:  # Octubre
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + \
                            saldo_cuenta.debe3 + saldo_cuenta.debe4 + \
                            saldo_cuenta.debe5 + saldo_cuenta.debe6 + \
                            saldo_cuenta.debe7 + saldo_cuenta.debe8 + \
                            saldo_cuenta.debe9 + saldo_cuenta.debe10
        elif mes == 11:  # Noviembre
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + \
                            saldo_cuenta.debe3 + saldo_cuenta.debe4 + \
                            saldo_cuenta.debe5 + saldo_cuenta.debe6 + \
                            saldo_cuenta.debe7 + saldo_cuenta.debe8 + \
                            saldo_cuenta.debe9 + saldo_cuenta.debe10 + saldo_cuenta.debe11
        elif mes == 12:  # Diciembre
            total += saldo_cuenta.debe1 + saldo_cuenta.debe2 + \
                            saldo_cuenta.debe3 + saldo_cuenta.debe4 + \
                            saldo_cuenta.debe5 + saldo_cuenta.debe6 + \
                            saldo_cuenta.debe7 + saldo_cuenta.debe8 + \
                            saldo_cuenta.debe9 + saldo_cuenta.debe10 + \
                            saldo_cuenta.debe11 + saldo_cuenta.debe12
        else:  # Sin mes
            return 0.00
        return total
    else:
        if mes == 1:  # Enero
            return saldo_cuenta.debe1
        elif mes == 2:  # Febrero
            return saldo_cuenta.debe2
        elif mes == 3:  # Marzo
            return saldo_cuenta.debe3
        elif mes == 4:  # Abril
            return saldo_cuenta.debe4
        elif mes == 5:  # Mayo
            return saldo_cuenta.debe5
        elif mes == 6:  # Junio
            return saldo_cuenta.debe6
        elif mes == 7:  # Julio
            return saldo_cuenta.debe7
        elif mes == 8:  # Agosto
            return saldo_cuenta.debe8
        elif mes == 9:  # Septiembre
            return saldo_cuenta.debe9
        elif mes == 10:  # Octubre
            return saldo_cuenta.debe10
        elif mes == 11:  # Noviembre
            return saldo_cuenta.debe11
        elif mes == 12:  # Diciembre
            return saldo_cuenta.debe12
        else:  # Sin mes
            return 0.00

@register.simple_tag(name='get_total_credito')
def get_total_credito(saldo_cuenta, mes, es_acum):
    if es_acum:  # Total acumulado
        total = saldo_cuenta.saldo_inicial
        if mes == 1:  # Enero
            pass
        elif mes == 2:  # Febrero
            total += saldo_cuenta.haber1
        elif mes == 3:  # Marzo
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2
        elif mes == 4:  # Abril
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + saldo_cuenta.haber3
        elif mes == 5:  # Mayo
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + saldo_cuenta.haber3 + saldo_cuenta.haber4
        elif mes == 6:  # Junio
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + \
                            saldo_cuenta.haber3 + saldo_cuenta.haber4 + saldo_cuenta.haber5 + saldo_cuenta.haber6
        elif mes == 7:  # Julio
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + \
                            saldo_cuenta.haber3 + saldo_cuenta.haber4 + \
                            saldo_cuenta.haber5 + saldo_cuenta.haber6 + saldo_cuenta.haber7
        elif mes == 8:  # Agosto
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + \
                            saldo_cuenta.haber3 + saldo_cuenta.haber4 + \
                            saldo_cuenta.haber5 + saldo_cuenta.haber6 + \
                            saldo_cuenta.haber7 + saldo_cuenta.haber8
        elif mes == 9:  # Septiembre
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + \
                            saldo_cuenta.haber3 + saldo_cuenta.haber4 + \
                            saldo_cuenta.haber5 + saldo_cuenta.haber6 + \
                            saldo_cuenta.haber7 + saldo_cuenta.haber8 + saldo_cuenta.haber9
        elif mes == 10:  # Octubre
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + \
                            saldo_cuenta.haber3 + saldo_cuenta.haber4 + \
                            saldo_cuenta.haber5 + saldo_cuenta.haber6 + \
                            saldo_cuenta.haber7 + saldo_cuenta.haber8 + \
                            saldo_cuenta.haber9 + saldo_cuenta.haber10
        elif mes == 11:  # Noviembre
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + \
                            saldo_cuenta.haber3 + saldo_cuenta.haber4 + \
                            saldo_cuenta.haber5 + saldo_cuenta.haber6 + \
                            saldo_cuenta.haber7 + saldo_cuenta.haber8 + \
                            saldo_cuenta.haber9 + saldo_cuenta.haber10 + saldo_cuenta.haber11
        elif mes == 12:  # Diciembre
            total += saldo_cuenta.haber1 + saldo_cuenta.haber2 + \
                            saldo_cuenta.haber3 + saldo_cuenta.haber4 + \
                            saldo_cuenta.haber5 + saldo_cuenta.haber6 + \
                            saldo_cuenta.haber7 + saldo_cuenta.haber8 + \
                            saldo_cuenta.haber9 + saldo_cuenta.haber10 + \
                            saldo_cuenta.haber11 + saldo_cuenta.haber12
        else:  # Sin mes
            return 0.00
        return total
    else:
        if mes == 1:  # Enero
            return saldo_cuenta.haber1
        elif mes == 2:  # Febrero
            return saldo_cuenta.haber2
        elif mes == 3:  # Marzo
            return saldo_cuenta.haber3
        elif mes == 4:  # Abril
            return saldo_cuenta.haber4
        elif mes == 5:  # Mayo
            return saldo_cuenta.haber5
        elif mes == 6:  # Junio
            return saldo_cuenta.haber6
        elif mes == 7:  # Julio
            return saldo_cuenta.haber7
        elif mes == 8:  # Agosto
            return saldo_cuenta.haber8
        elif mes == 9:  # Septiembre
            return saldo_cuenta.haber9
        elif mes == 10:  # Octubre
            return saldo_cuenta.haber10
        elif mes == 11:  # Noviembre
            return saldo_cuenta.haber11
        elif mes == 12:  # Diciembre
            return saldo_cuenta.haber12
        else:  # Sin mes
            return 0.00