__author__ = 'Clip Maestro'
#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()

@register.simple_tag(name='formulario_datos_generales')
def formulario_datos_generales(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/Empresa/datos_generales.html')(formulario_datos_generales)

@register.simple_tag(name='formulario_datos_contador')
def formulario_datos_contador(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/Empresa/datos_contador.html')(formulario_datos_contador)
