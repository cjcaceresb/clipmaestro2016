__author__ = 'Clip Maestro'
#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from contabilidad.models import *
from librerias.funciones.funciones_vistas import get_fila_t_detalle_compra


register = template.Library()

@register.simple_tag(name='formulario_cabecera_comp_cont')
def formulario_cabecera_comp_cont(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/compras/formulario_cabecera_comprobante_contable.html')(formulario_cabecera_comp_cont)


@register.simple_tag(name='agregar_filas_tabla_detalle_compra')
def agregar_filas_tabla_detalle_compra(form):
    return get_fila_t_detalle_compra(form)

@register.simple_tag(name='formulario_detalle_comp_com')
def formulario_detalle_comp_com(form, id):
    forma_pago = Forma_Pago_SRI.objects.filter(status=1).order_by("codigo")
    # Para poner la forma de pago en el select
    try:
        compra = CompraCabecera.objects.get(id=id)
    except:
        compra = None
    try:
        cod = compra.ats.forma_pago_sri.codigo
    except:
        cod = None
    return {"forms": form, "forma_pago": forma_pago, "codigo": cod, "compra": compra}
register.inclusion_tag('tags/forms/formulario_detalle_comprobante_compra.html')(formulario_detalle_comp_com)

def formulario_reembolso(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/compras/formulario_reembolso_gastos.html')(formulario_reembolso)
