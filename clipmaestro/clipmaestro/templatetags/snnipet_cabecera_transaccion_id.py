#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from contabilidad.models import *
from administracion.models import *
from django.contrib import messages
from clipmaestro.templatetags.formulario_base import *

register = template.Library()

@register.simple_tag(name='getTransaccionId')
def getTransaccionId(id_comp):
    url = get_url_detalle_comp(id_comp)
    return {"url": url}
register.inclusion_tag('tags/transacciones/snnipet_transacciones.html')(getTransaccionId)


@register.simple_tag(name='get_url_detalle_comp')
def get_url_detalle_comp(id_comp):
    url = "#"

    try:
        comp_cont = Cabecera_Comp_Contable.objects.get(id=id_comp)
        tipo = comp_cont.tipo_comprobante_id

        if tipo is not None:
            if tipo == 1:
                try:
                    cabecera_compra = CompraCabecera.objects.get(num_comp=comp_cont.numero_comprobante).id
                    url = reverse("detalle_compra_pdf", args={cabecera_compra})
                except:
                    messages.error("Existen problemas al consultar el comprobante, por favor intentelo nuevamente.")

            elif tipo == 2:
                try:
                    if Venta_Cabecera.objects.filter(numero_comprobante=comp_cont.numero_comprobante).exists():
                        cabecera = Venta_Cabecera.objects.get(numero_comprobante=comp_cont.numero_comprobante).id
                        url = reverse("detalle_venta_reportepdf", args={cabecera})
                    else:
                        cabecera = Cabecera_Comp_Contable.objects.get(numero_comprobante=comp_cont.numero_comprobante).id
                        url = reverse("generar_asiento_pdf", args={cabecera})
                except:
                    messages.error("Existen problemas al consultar el comprobante, por favor intentelo nuevamente.")

            elif tipo == 3:
                cabecera = Banco.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_transaccion_banco_reportepdf", args={cabecera})

            elif tipo == 4:
                cabecera = Banco.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_transaccion_banco_reportepdf", args={cabecera})

            elif tipo == 6:
                cabecera = Cabecera_Comp_Contable.objects.get(numero_comprobante=comp_cont.numero_comprobante).id
                url = reverse("generar_asiento_pdf", args={cabecera})

            elif tipo == 8:
                try:
                    cabecera = Cabecera_Caja_Chica.objects.get(numero_comprobante=comp_cont.numero_comprobante).id
                    url = reverse("detalle_compra_caja_chica_pdf", args={cabecera})
                except:
                    pass

            elif tipo == 9:
                cabecera = Pago.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_pago_proveedores_pdf", args={cabecera})

            elif tipo == 10:
                cabecera = Cobro.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_cobro_pdf", args={cabecera})

            elif tipo == 11:
                cabecera = Pago.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_pago_proveedores_pdf", args={cabecera})

            elif tipo == 12:
                cabecera = Pago.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_pago_proveedores_pdf", args={cabecera})

            elif tipo == 13:
                print "cin"

            elif tipo == 14:
                cabecera = Inventario_Cabecera.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_inventario_pdf", args={cabecera})

            elif tipo == 15:
                cabecera = Inventario_Cabecera.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_inventario_pdf", args={cabecera})

            elif tipo == 16:
                cabecera = Cobro.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_cobro_pdf", args={cabecera})

            elif tipo == 17:
                cabecera = Cobro.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_cobro_pdf", args={cabecera})

            elif tipo == 18:
                cabecera = Cobro.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_cobro_pdf", args={cabecera})

            elif tipo == 19:  # Contrato
                pass

            elif tipo == 20:  # Roles Nomina
                pass

            elif tipo == 21:  # Costo Contrato
                pass

            elif tipo == 22:  # Nota de Crédito
                cabecera = AjusteVentaCabecera.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_nota_credito_pdf", args={cabecera})

            elif tipo == 23:   # Transacciones Bancarias
                cabecera = Banco.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_transaccion_banco_reportepdf", args={cabecera})

            elif tipo == 24:    # N/C Bancos
                cabecera = Banco.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_transaccion_banco_reportepdf", args={cabecera})

            elif tipo == 25:    # N/D Bancos
                cabecera = Banco.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_transaccion_banco_reportepdf", args={cabecera})

            elif tipo == 26:    # Asiento Contable Apertura
                cabecera = Cabecera_Comp_Contable.objects.get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("generar_asiento_pdf", args={cabecera})
    except:
        pass

    return url



@register.simple_tag(name='get_url_detalle_comp_num_comp')
def get_url_detalle_comp_num_comp(num_comp):
    """
    Función provisional, ya que el parámetro de la funcion debe de ser el id del comprobante
    contable pero en algunas tablas no se guarda la relación sino solamente el numero de
    comprobante
    :param num_comp:
    :return:
    """
    url = ""
    try:
        comp_cont = Cabecera_Comp_Contable.objects.get(numero_comprobante=num_comp)
        tipo = comp_cont.tipo_comprobante_id

        if tipo is not None:
            if tipo == 1:
                try:
                    cabecera_compra = CompraCabecera.objects.exclude(status=0).get(num_comp=comp_cont.numero_comprobante).id
                    url = reverse("detalle_compra_pdf", args={cabecera_compra})
                except:
                    pass

            elif tipo == 2:
                try:
                    cabecera = Venta_Cabecera.objects.exclude(status=0).get(numero_comprobante=comp_cont.numero_comprobante).id
                    url = reverse("detalle_venta_reportepdf", args={cabecera})
                except:
                    pass

            elif tipo == 3:
                cabecera = Banco.objects.exclude(status=0).get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_transaccion_banco_reportepdf", args={cabecera})

            elif tipo == 4:
                cabecera = Banco.objects.exclude(status=0).get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_transaccion_banco_reportepdf", args={cabecera})


            elif tipo == 8:
                try:
                    cabecera = Cabecera_Caja_Chica.objects.exclude(status=0).get(numero_comprobante=comp_cont.numero_comprobante).id
                    url = reverse("detalle_compra_caja_chica_pdf", args={cabecera})
                except:
                    pass

            elif tipo == 9:
                cabecera = Pago.objects.exclude(status=0).get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_pago_proveedores_pdf", args={cabecera})

            elif tipo == 10:
                print "cob"
                cabecera = Cobro.objects.exclude(status=0).get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_cobro_pdf", args={cabecera})

            elif tipo == 11:
                cabecera = Pago.objects.exclude(status=0).get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_pago_proveedores_pdf", args={cabecera})

            elif tipo == 12:
                cabecera = Pago.objects.exclude(status=0).get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_pago_proveedores_pdf", args={cabecera})

            elif tipo == 13:
                print "cin"

            elif tipo == 14:
                print "iin"

            elif tipo == 15:
                print "ein"

            elif tipo == 16:
                cabecera = Cobro.objects.exclude(status=0).get(num_comp=comp_cont.numero_comprobante).id
                url = reverse("detalle_cobro_pdf", args={cabecera})

            elif tipo == 17:
                cabecera = Cobro.objects.exclude(status=0).get(num_comp=num_comp).id
                url = reverse("detalle_cobro_pdf", args={cabecera})

            elif tipo == 18:
                cabecera = Cobro.objects.exclude(status=0).get(num_comp=num_comp).id
                url = reverse("detalle_cobro_pdf", args={cabecera})
    except:
        pass
    return url

@register.simple_tag(name='tiene_comprobante')
def tiene_comprobante(num_comp):
    if get_url_detalle_comp_num_comp(num_comp) == "":
        return True
    else:
        return False