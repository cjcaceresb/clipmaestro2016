#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from clipmaestro import *

register = template.Library()
__author__ = 'Roberto'

@register.simple_tag(name='formulario_costo_contrato_detalle')
def formulario_costo_contrato_detalle(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/contratos/formulario_costo_contrato_detalle.html')(formulario_costo_contrato_detalle)



@register.simple_tag(name='formulario_costo_contrato_detalle_proveedor')
def formulario_costo_contrato_detalle_proveedor(form):
    return {"forms": form}
register.inclusion_tag('tags/forms/contratos/formulario_costo_contrato_detalle_proveedor.html')(formulario_costo_contrato_detalle_proveedor)