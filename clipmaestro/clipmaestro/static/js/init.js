/**
 * Created by Clip Maestro on 28/04/2015.
 */
jQuery(document).ready(function($)
{
    /******************************************************
     * Inicializa los inputs que poseen una máscara
     * mediante el atributo data-mask
     */
    $('input[data-mask]').each(function(){
        try{
            $(this).mask($(this).attr("data-mask"))
        }
        catch (e){
            alert(e)
        }
    });

    /*****************************
     * Limpia el formulario
     */
    $('.limpiar_form').click(function(){
        var form = $("form");
        limpiar_form(form);
    });

    /*************************
     * Para paginación
     */
    $(".evento_page").each(function() {
        $(this).click(function () {
            var page = $(this).attr("data-page");
            $("#id_page").val(page);
            $("#buscar").click();
        })
    });

    $(".datepicker").datepicker().on("changeDate", function(){
        $(this).datepicker("hide");
    });

    if($(".selectpicker").length){
        $(".selectpicker").selectpicker();
    }


});


