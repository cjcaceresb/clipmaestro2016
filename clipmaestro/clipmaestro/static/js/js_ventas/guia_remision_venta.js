/**
 * Created by Roberto on 23/06/14.
 */
var id_cliente = getIdCliente();
var URLexistenciaBodega = getCantidadBodega();

/*********************************** Funciones Guía de Remisión - VEnta ***********************************************/

/******************
* Función Ajax para seleccionar la Dirección del cliente en base a a la venta
* @param event
* @constructor event
*/
function seleccionarDirecciones(){
    var combo_direccion_guia = $("#id_direccion_guia");
    combo_direccion_guia.empty();

    $.ajax(
    {
        url: "/ventas/buscardirecciones/",
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id': id_cliente
        },
        error: function(data){
            return true;
        },
        success: function(data){
            $("#id_direccion_guia").val(data[0].id);

            var flag = true;
            if(data[0].id == 0)
            {
                flag=false;
            }
            if(flag){
                 for(var j=0; j<data.length; j++)
                {
                    combo_direccion_guia.append('<option value="' + data[j].id + '">' + data[j].descripcion +" - "+ data[j].direccion1+ '</option>');
                }
            }
        }
    });
    combo_direccion_guia.selectpicker("refresh");
}
/******************
 *
 * @param event
 * @constructor event
 */
function Numerico(event){
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39) || (event.keyCode == 110))
    {
         return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
/****************
 * function NumeroEntero(event)
 * Sirve para que solo ingresen números enteros, sin punto flotante
 * para casos como el numde doc y el num de retención
 * @param event
 * @constructor
 */
function NumeroEntero(event){
    // Allow: backspace, delete, tab, escape, enter and.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39))
        {
            return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
/***********************************
 * Agrega puntos decimales a los
 * inputs
 * @param e
 */
function agregar_punto_guia(e){
    var numero = $(e).val();
    var tr = $(e).parent().parent();
    var subtotal_t = $(tr).find(".subtotal");

    if(numero != ""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));
}
/***********************************
 * Valida la Fecha de Registro de la Guia de Remisión
 * inputs
 * @param e
 */
function ValidarFechaGuia(){
    try
    {
        var dia_inicio = Date.parse($("#id_fecha_inicio").val());
        var date_ini = new Date(dia_inicio);

        var dia_final = Date.parse($("#id_fecha_final").val());
        var date_final = new Date(dia_final);

        if(date_ini > date_final)
        {
            $("#tit-alert-ok").html("La fecha final o de entrega de la guía debe de ser mayor igual " +
                "a la fecha de inicio");
            $("#body-alert-ok").html("");
            $("#alert-ok").modal();
            $("#id_fecha_final").val("");
        }
    }
    catch (err)
    {
        alert(err);
    }
}
/***
 * Función que Valida la vigencia del documento
 * guía remisión
 * @param url
 */
function valida_vigencia_guia_remision(url){
    var combo_serie = $("#id_serie");
    combo_serie.empty();
    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id': $("#id_tipo_documento").val(),
            'fecha': $("#id_fecha_reg").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data){
            $("#id_serie").val(data[0].id);

            if(data[0].status == 0){
                $("#add_doc").attr("disabled", false);
                $("#tit-alert-ok").html("No tiene block de documentos vigentes registrados para esta fecha de emisión. Para continuar, " +
                    "dele click en Aceptar e ingrese un block de documento vigente");
                $("#body-alert-ok").html("");
                $("#alert-ok").modal();
            }

            if(data[0].status == 1){
                $("#add_doc").attr("disabled", true);
            }

            if(data[0].status == -1){
                $("#tit-alert-ok").html("Erro del Servidor " +
                    "por favor comuníquese con el administrador");
                $("#body-alert-ok").html("");
                $("#alert-ok").modal();
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }

    });
    combo_serie.selectpicker("refresh");
}

/**************************************************
 *  Función para agregar un Documento Vigente para la guia remisión
 **************************************************/

function AgregarDocGuiaRemision(e){
    var datastring = $("#form_vigencia_doc").serialize();
    var url = $("#form_vigencia_doc").attr("action");

    alert(url);

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: datastring,

        error: function(){
            return true;
        },
        success: function(data){
            var cont = 0;
            var mensajes = new Array();

            for(var i=0; i<data.length; i++)
            {

                if(data[i].status != 1)
                {
                    mensajes.push(data[i].mensaje);
                    cont++;
                }
            }
            if(cont != 0)
            {
                var stringhtml = "";
                for(i=0; i<mensajes.length; i++)
                {
                    stringhtml = (stringhtml+"<p>"+mensajes[i]+"</p>")
                }

                $("#error").fadeIn("slow");
                $("#error-doc-vigente").html(stringhtml)
            }

            if(cont == 0)
            {

                var combo_serie = $("#id_serie_guia");
                combo_serie.empty();

                var url_2 = $("#form_vigencia_doc").attr("data-search");
                $.ajax({
                    url:url_2,
                    type: 'POST',
                    async: false,
                    cache: false,
                    timeout: 300,
                    beforeSend: function(msj){
                    $('#loading').show();
                    $('body').css('display', '0.5');
                    },
                    error: function(){
                        return true;
                    },
                    success: function(data)
                    {
                        for(var i = 0; i < data.length; i++)
                        {
                            $(combo_serie).append('<option value="' + data[i].id +'" >' + data[i].serie +'</option>');
                        }
                    }
                });

                $('#div-doc-guia').modal('hide');

                $("#id_serie_guia").selectpicker("refresh");
                $('#loading').fadeOut();
                $('body').css('opacity', '1');
            }
        }
    });
}


/************************************ Función para validar cantidad bodega de Item Inventario *************************/
function cantidadBodegaVenta(obj, url){
    var tr = $(obj).parent().parent();
    var id_item = $(tr).find(".id_item");
    var item = $(tr).find(".item");
    var cantidad = $(tr).find(".cantidad");
    var cantidad_entregada = $(tr).find(".cant_entregada");
    var tipo_item = $(tr).find(".tipo_item");

    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data : {
            'id_item': $(id_item).val(),
            'cantidad_venta': $(cantidad).val(),
            'cantidad_entregada': $(cantidad_entregada).val()
        },
        error: function(data){
          return true;
        },
        success: function(data){
            var estado = data[0].status;
            var cantidad_actual = data[0].cantidad_bodega;

            if($(tipo_item).val() != 2){
                switch (estado){
                case 0:
                    $("#tit-alert-ok").html("El item no se encuentra registrado en bodega.");
                    $("#body-alert-ok").html("");
                    $("#alert-ok").modal();
                    $(cantidad).val("");
                    $(cantidad_entregada).val("");
                    $(item).val("");

                break;

                case 1:
                    $(cantidad_entregada).val($(cantidad).val());
                break;

                case 2:
                    //alert("ok cant_entrega menor a cant_act_bodega");
                break;

                case 3:
                    //alert("cant_entrega mayor a cant_act_bodega ");
                    $("#tit-alert-ok").html("El Item seleccionado no está disponible en bodega.");
                    $("#body-alert-ok").html("");
                    $("#alert-ok").modal();
                    $(cantidad_entregada).val(cantidad_actual);
                break;
                    case 4:
                    //alert("case4: cantidad de venta mayor a la cantidad de bodega");
                    //$(cantidad).val(cantidad_actual);
                break;

                default :
                break;
                }
            }
        }
    });
}
$(document).ready(function(e){
    $('.selectpicker').selectpicker();

    // Inhabilita el boton de submit evitando que se haga más de una petición
    $("input[type='submit']").click(function(){
        $(this).attr("disabled", false);
        $("form").submit(function(){
          $("input[type='submit']").attr("disabled", true).val("Por favor, espere...");
          return true;
        });
    });

    $('#id_fecha_reg').mask("9999-99-99").datepicker({
         todayBtn: true,
         autoclose: true

    }).on('changeDate', function (ev) {
        valida_vigencia_guia_remision(url_valida_guia_remision);
    });

    $("#id_serie").mask("999-999");
    $('#id_fecha_emision').mask("9999-99-99").datepicker({
         todayBtn: true,
         autoclose: true

    });
    $('#id_fecha_vencimiento').mask("9999-99-99").datepicker({
         todayBtn: true,
         autoclose: true

    });

    $("#add_doc").click(function(){
        $("#error").hide();

        $("#error-doc-vigente").html("");
        $("#div-doc-guia").modal();

    });

    $("#send_form_doc_guia").click(function(e){ AgregarDocGuiaRemision(e) });


    valida_vigencia_guia_remision(url_valida_guia_remision);

    seleccionarDirecciones();

    $("#id_fecha_inicio" ).mask("9999-99-99").datepicker({
             todayBtn: true,
             autoclose: true

    }).on('changeDate', function(ev){
        $(this).datepicker("hide");
        ValidarFechaGuia();
        //validar_fecha_actual(this);
    });

    $("#id_fecha_final").mask("9999-99-99").datepicker({
         todayBtn: true,
         autoclose: true

    }).on('changeDate', function(ev){
        $(this).datepicker("hide");
        ValidarFechaGuia();
    });

    $(".cant_entregada").change(function(){
        var tr = $(this).parent().parent();
        var cantidad_t = $(tr).find(".cantidad");
        var cantidad_entregada1 = $(tr).find(".cant_entregada1");
        var saldo = 0.0;

        if($(this).val()!=""){
            agregar_punto_guia(this);
            saldo = parseFloat($(cantidad_t).val()) - parseFloat($(cantidad_entregada1).val());

            if(parseFloat($(this).val()) > saldo || parseFloat($(this).val()) > parseFloat($(cantidad_t).val()) ){
                $("#tit-alert-ok").html("No puede ingresar una cantidad por entregar mayor que la cantidad de venta.");
                $("#body-alert-ok").html("");
                $("#alert-ok").modal();
                $(this).val("");

            }else{
                cantidadBodegaVenta(this, URLexistenciaBodega);
            }
        }

    });

    $(".numerico").each(function(){
       $(this).keydown(function(event){
            Numerico(event);
        });
    });

    $(".numerico2").each(function(){
       $(this).keydown(function(event){
            NumeroEntero(event);
        });
    });

});