/***
 * Función que presenta y oculta la información requerida
 * de Venta ya sea Anular Generar
 * @param tipo
 */
function presentar_ocultar_anular_generar(tipo){
    if (tipo == 2){
        $(".ocultar_anular_generar").hide();
        $(".presenta_anular_generar").show();
    }else{
        $(".ocultar_anular_generar").show();
        $(".presenta_anular_generar").hide();
    }
}
/***
 * Función que según el parametro va indicar
 * si presenta el div con el formset venta o el de
 * formset_stock
 * @param es_stock
 */
function presentar_formset_venta(es_stock){

    if(es_stock==1){
        $("#div_formset_stock").show();
        $("#div_formset_venta_test").hide();
    }else{
        $("#div_formset_venta_test").show();
        $("#div_formset_stock").hide();
    }
}

/***
 * Función que llama a la vista ajax para realizar
 * la anulación directamente la factura
 */
function anular_factura_venta(obj){

    $(obj).attr("disabled", true);
    var texto = $(obj).text();
    var datastring = $("#form_anular_factura").serialize();
    $.ajax(
    {
        url: url_anular_factura,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: datastring,
        beforeSend: function(){
            $(obj).text("Por favor, espere...");
            $("#cancel-factura").hide();
        },
        error: function(){
            $(".mensaje-error-retencion").text("Error en la conexión por favor vuelva a intentarlo más tarde");
            $(".error-modal").fadeIn("slow");
            $(obj).attr("disabled", false).text(texto);
            $("#cancel-factura").show();
        },
        success: function(data)
        {
            if(data.status == 1)
            {
                window.location.reload(true);
            }
            else
            {
                $(".mensaje-error-retencion").text(data.mensaje);
                $(".error-modal").fadeIn("slow");
                $(obj).attr("disabled", false).text(texto);

            }
        }
    });
}

/***
 * Función que llama a la url Ajax para presentar el
 * último numero de factura utilizado
 * @returns {string}
 */
function get_ultima_factura(){
    var id = $("#id_documento").val();
    var num_doc = "";

    $.ajax(
    {
        url: url_ultima_factura,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {"id_vigencia": id},
        error: function(){
            num_doc = "Error de Conexión"
        },
        success: function(data)
        {
            if(data.status == 1)
            {
                num_doc = data.num_doc;
            }
            else
            {
                num_doc = "Escoja una block de documento válida"
            }
        }
    });
    return num_doc;
}

/***
 * Función que llama a la url Ajax para presentar la
 * última secuencia de comprobante
 * @returns {string}
 */
function get_ultimo_comprobante_venta(){
    var num_comp = "";
    var fecha_reg = $("#id_fecha_reg");

     $.ajax(
     {
        url: url_ultimo_comp_venta,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        error: function(){
            num_comp = "Error de Conexión"
        },
        success: function(data)
        {
            if(data.status == 1)
            {
                num_comp = data.num_comp;
                $(fecha_reg).val(data.fecha_comp);

            }
        }
    });

    return num_comp;
}

/***
 * Función que llama a la url Ajax para validar la vigencia del
 * block de la factura
 * @param url
 */
function valida_factura_vigente(url){
    var combo_serie = $("#id_documento");
    combo_serie.empty();

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'fecha': $("#id_fecha_reg").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data){
            $("#id_serie").val(data[0].id);

            if(data[0].status == 0)
            {
                $("#tit-alert-ok").html("No tiene block de documentos vigentes registrados para esta fecha de emisión. " +
                    "Por Favor antes de realizar la venta, ingrese un block de documento nuevo vigente.");
                $("#body-alert-ok").html("");
                $("#alert-ok").modal();

                $("#enviar").on("click",function(){
                    var url = $(this).attr("data-href");
                        window.location.href = url;
                });
            }
            else{

                for(var i=0; i<data.length; i++){
                    combo_serie.append('<option value="' + data[i].id + '">' + data[i].serie + '</option>');
                }
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }

    });
    combo_serie.selectpicker("refresh");
}


/***********************
 *  Autocompletado del Item
 * @param obj
 * @param url
 * @constructor busqueda cuenta
 */
function BusquedaItem(obj, url){
    $(obj).autocomplete({
        source: function( request, response )
        {
            $.ajax({
                url: url,
                dataType: "json",
                data: {
                cliente: $("#id_cliente").val(),
                featureClass: "P",
                style: "full",
                maxRows: 12,
                name: request.term
                },
                success: function( data )
                {
                    response( $.map( data, function( item )
                    {
                    return {
                        label: item.name,
                        value: item.name,
                        id_item: item.id,
                        id_cuenta: item.id_cuenta,
                        cuenta: item.cuenta,
                        iva: item.iva,
                        ice: item.ice,
                        tipo : item.tipo,
                        precio: item.precio
                    }
                    }));
                }
            });
        },
        minLength: 2,
        select: function( event, ui ) {
            var tr = $(obj).parent().parent();
            var id_item = $(tr).find(".id_item");
            var iva_class = $(tr).find(".iva");
            var precio_class = $(tr).find('.precio_u');
            var ice_class= $(tr).find(".ice");
            var tipo_item = $(tr).find(".tipo_item");

            var id_cuenta = ui.item.id_cuenta;
            var cuenta = ui.item.cuenta;

            id_item.attr("data-cuenta", cuenta);
            id_item.attr("data-id_cuenta", id_cuenta);
            id_item.val(ui.item.id_item);
            tipo_item.val(ui.item.tipo);

            log( ui.item ?
            "Selected: " + ui.item.label :
            "Nothing selected, input was " + obj.value);

            $(iva_class).val(ui.item.iva);
            $(ice_class).val(ui.item.iva);
            $(precio_class).val(ui.item.precio);
        },
        open: function() {
            $( obj ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
        },
        close: function() {
            $( obj ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
        }
    });
}

/************************************************************
* Validación número documento en venta
* (Inactivado en el módulo actualmente)
*************************************************************/
$("#id_numero_documento").focusout(function(){
    var valor = $(this).val();
    var titulo = "El número de documento es incorrecto";
    $(this).popover('destroy');

    if( valor == 0 ){
        $(this).popover({title: titulo,placement: 'bottom'}).popover('show');
        $(this).val("");

    }else{
        $(this).popover('destroy');
         var a= ("00000000"+valor);
        $(this).val(a.slice(-9));
    }
});

/************************************************************
* Validación número documento en guía remisión (Inactivado en el módulo actualmente)
*************************************************************/
$("#id_num").focusout(function(){
    var valor = $(this).val();
    var titulo = "El número de documento es incorrecto";
    $(this).popover('destroy');
    if( valor == 0 ){
        $(this).popover({title: titulo,placement: 'bottom'}).popover('show');
        $(this).val("");

    }else{
        $(this).popover('destroy');
         var a= ("00000000"+valor);
        $(this).val(a.slice(-9));
    }
});


/***************************************************************************************
 * Función que valida los documentos y seri vigentes según los documentos de la empresa
 ***************************************************************************************/
function ValidaDocumentoSerie(url){
    var combo_serie = $("#id_serie");
    combo_serie.empty();

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            //'id': $("#id_tipo_documento").val(),
            'fecha': $("#id_fecha").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data){
            $("#id_serie").val(data[0].id);

            if(data[0].status == 0){
                $("#tit-alert-ok").html("No tiene block de documentos vigentes registrados para " +
                    "esta fecha de emisión. Para continuar, " +
                    "dele click en Aceptar e ingrese un block de documento vigente");

                $("#body-alert-ok").html("");
                $("#alert-ok").modal();

                $("#enviar").on("click",function(){
                    var url = $(this).attr("data-href");
                        window.location.href = url;
                });
            }

            if(data[0].status == 1){
                for(var i=0; i<data.length; i++){
                    combo_serie.append('<option value="' + data[i].id + '">' + data[i].serie + '</option>');
                }

            }

            if(data[0].status == -1){
                $("#tit-alert-ok").html("Error del Servidor " +
                    "por favor comuníquese con el administrador");
                $("#body-alert-ok").html("");
                $("#alert-ok").modal();

                $("#enviar").on("click",function(){
                    var url = $(this).attr("data-href");
                        window.location.href = url;
                });
            }


            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }

    });
    combo_serie.selectpicker("refresh");
}

 /***********************************************************************************************
 * Función que selecciona el numero de autorizacion y fecha de vencimiento del documento vigente
 ***********************************************************************************************/
function SelectSerieNum(){
    $.post(URLseleccionDocumento, {
            'id': $("#id_serie").val()
        }, function(data){
            var autorizacion = data.autorizacion;
            var fecha_vencimiento = data.vencimiento;
            $("#autorizacion").text(autorizacion);
            $("#fecha_vencimiento").text(fecha_vencimiento);
            $("#doc_anterior").text(data.secuencia);
            }
    );
}

/***************************************************************************************
 * Función que seleccion los contratos según el cliente seleccionado
 ***************************************************************************************/
function getSelectContratos(url){
    var combo_contrato = $("#id_contrato");

    if (flag_post_venta == 0){
        combo_contrato.empty();
    }

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id': $("#id_cliente").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(data){
            return true;
        },
        success: function(data){
            var flag = true;

            if(data[0].status == 0 || data[0].status == -1)
            {
                flag=false;
            }

            if(flag){
                combo_contrato.append('<option value=""></option>'); // Para llenar en blanco la primera posición del select
                for(var i=0; i<data.length; i++){
                    combo_contrato.append('<option value="' + data[i].id + '">' + data[i].num_cont +" - "+ data[i].cliente+ '</option>');
                }
            }
            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
    combo_contrato.selectpicker("refresh");
}

/***************************************************************************************
 * Función que seleccion una dirección según el cliente seleccionado
 ***************************************************************************************/
function seleccionarDirecciones(){
    var combo_direccion = $("#id_direccion");
    var combo_direccion_guia = $("#id_direccion_guia");
    combo_direccion.empty();
    combo_direccion_guia.empty();
    $.ajax(
    {
        url: "/ventas/buscardirecciones/",
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id': $("#id_cliente").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(data){
            return true;
        },
        success: function(data){
            $("#id_direccion").val(data[0].id);
            $("#id_direccion_guia").val(data[0].id);
            var flag = true;

            if(data[0].status == 0 || data[0].status == -1)
            {
                flag=false;
            }

            if(flag){
                for(var i=0; i<data.length; i++)
                {
                    combo_direccion.append('<option value="' + data[i].id + '">' + data[i].descripcion +" - "+ data[i].direccion1+ '</option>');
                }
                 for(var j=0; j<data.length; j++)
                {
                    combo_direccion_guia.append('<option value="' + data[j].id + '">' + data[j].descripcion +" - "+ data[j].direccion1+ '</option>');
                }
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
    combo_direccion.selectpicker("refresh");
    combo_direccion_guia.selectpicker("refresh");
}

/***********************************************************
* Funcion para recalcular los ids
* @param tableid
************************************************************/
function recalcular_ids_Venta(tableid){
    var nombre_form = ""
    $(tableid+" tbody tr").each(function(index)
    {
        var cont = index;
        $(this).find(":input:not(:button, .input-block-level)").each(function()
        {
            var name_campo = $(this).attr("name").split("-");
            nombre_form = name_campo[0];
            var nombre_campo = name_campo[0]+"-" + cont + "-"+name_campo[2];
            $(this).attr("name", nombre_campo);
            $(this).attr("id", "id_"+nombre_campo);
        });
    });
    $('#id_'+nombre_form+'-TOTAL_FORMS').val($(tableid+" tbody").children().length);
}
/***********************************************************
* Funcion para eliminar fila de la tabla de retenciones
************************************************************/
function EliminarFilaRetencion(e, fila, tableid){
    e.preventDefault();
    if($(tableid+" tbody").children().length>1){
        $(fila).parent().parent().remove();
        recalcular_ids_Venta(tableid);
    }
    else{
        $("#error-pop_up").html('<p>No se puede eliminar la fila</p>');
        $("#error").fadeIn("slow");
    }
}
/***********************************************************
* Funcion para reliminar filas
************************************************************/
function validarEliminar(num2){
    var num = parseInt(num2);
    if (num == 1){
        $("#tit-alert-ok").html("Alerta");
        $("#body-alert-ok").html('<p>No puede eliminar la fila</p>');
        $("#alert-ok").modal();
    }
}
/***********************************************************
 * Función Ajax para validar si el Item existe en Bodega
 * @param obj
 * @param url
 ***********************************************************/
function ExisteItemBodega(obj, url){
    var tr = $(obj).parent().parent();
    var id_item = $(tr).find(".id_item");
    var item = $(tr).find(".item");
    var iva = $(tr).find(".iva");
    var ice= $(tr).find(".ice");
    var precio_class = $(tr).find('.precio_u');

     $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data : {
            'id_item': $(item).val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
         },
        error: function(data){
          return true;
        },
        success: function(data){
            var estado = data[0].status;

            switch (estado){
            case 0:
                $("#tit-alert-ok").html("ALERTA");
                $("#body-alert-ok").html("El item no se encuentra registrado en bodega.");
                $("#alert-ok").modal();
                $(item).val("");
                $(item).removeAttr("data-original-title");
                $(iva).val("");
                $(precio_class).val("");
                $(ice).val("");

            break;

            case 1:

                default:
                    break;
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
}

/***************************************************************************
 * Función Ajax para validar si el Item posee cantidad disponible en Bodega
 * @param obj
 * @param url
 ***************************************************************************/

function cantidadBodegaVenta(obj, url){
    var tr = $(obj).parent().parent();
    var item = $(tr).find(".item");
    var cantidad = $(tr).find(".cantidad");
    var cantidad_entregada = $(tr).find(".cant_entregada");
    var subtotal = $(tr).find(".subtotal");
    var tipo_item = $(tr).find(".tipo_item");

    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data : {
            'id_item': $(item).val(),
            'cantidad_venta': $(cantidad).val(),
            'cantidad_entregada': $(cantidad_entregada).val()
        },
        error: function(data){
          return true;
        },
        success: function(data){
            var estado = data[0].status;
            var cantidad_actual = data[0].cantidad_bodega;

            if($(tipo_item).val() != 2){
                switch (estado){
                case 0:
                    $("#tit-alert-ok").html("El item no se encuentra registrado en bodega.");
                    $("#body-alert-ok").html("");
                    $("#alert-ok").modal();
                    $(cantidad).val("");
                    $(cantidad_entregada).val("");
                    $(item).val("");
                break;

                case 1:
                    $(cantidad_entregada).val($(cantidad).val());
                break;

                case 2:
                    // Ok la cantidad a entregar es menor a cantidad actual en bodega");
                break;
                /*
                case 3:
                    // Cantidad a Entregar mayor a la cantidad en bodega
                    $("#tit-alert-ok").html("ALERTA");
                    $("#body-alert-ok").html('<p>La cantidad disponible en bodega es: '+ cantidad_actual +'</p>');
                    $("#alert-ok").modal();
                    $(cantidad_entregada).val(cantidad_actual);
                break;
                    case 4:
                    // La Cantidad de venta es mayor a la cantidad actual en  bodega
                    $("#tit-alert-ok").html("ALERTA");
                    $("#body-alert-ok").html('<p>La cantidad disponible en bodega es: '+ cantidad_actual+'</p>');
                    $("#alert-ok").modal();
                    $(cantidad).val("");
                    $(subtotal).val("");
                break;
                */
                default :
                break;
                }
            }
        }
    });
}
/***********************
 *  Selección del Item
 * @param obj
 * @param url
 **********************/
function SelectItem(obj, url){
    var tr = $(obj).parent().parent();
    var id_item = $(tr).find(".id_item");
    var item = $(tr).find(".item");
    var iva_class = $(tr).find(".iva");
    var precio_class = $(tr).find('.precio_u');
    var ice_class= $(tr).find(".ice");
    var tipo_item = $(tr).find(".tipo_item");
    var cant_entregada = $(tr).find(".cant_entregada");
    //var cliente = $("#id_cliente").val();
    /*
    if (cliente == ""){
        $("#tit-alert-ok").html("Alerta");
        $("#body-alert-ok").html("Ingrese un cliente, para continuar con la venta.");
        $("#alert-ok").modal();
        $(item).val("");
        $(item).removeAttr("selected");
        $(item).removeAttr("data-original-title");
    }

    else{*/

    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data : {
            'id_item': $(item).val()
            //'cliente': cliente
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
         },
        error: function(data){
          return true;
        },

        success: function(data){

            if(data[0].tipo==2){
                $(id_item).val(data[0].id);
                $(tipo_item).val(data[0].tipo);
                $(iva_class).val(data[0].iva);
                $(precio_class).val(data[0].precio);
                $(ice_class).val(data[0].ice);
                $(cant_entregada).val("");
            }else{
                $(id_item).val(data[0].id);
                $(tipo_item).val(data[0].tipo);
                $(iva_class).val(data[0].iva);
                $(precio_class).val(data[0].precio);
                $(ice_class).val(data[0].ice);
                ExisteItemBodega(obj, URLExisteItemBodega);

            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }

    });
    //}
}

/***********************
 *  Selección del Item - Cliente
 * @param obj
 * @param url
 **********************/
function SelectClienteItem(obj, url){
    var cliente = $(obj).val();
    if(cliente != ""){
        if(ExisteItems()){
            //realizar la busquedas para setear valores y colocar nuevos según corresponda
            $(ID_TABLA_VENTA+" tbody tr").each(function(index){
                var item = $(this).find(".item");
                var id_item = $(this).find(".id_item");
                var tipo = $(this).find(".tipo_item");
                var iva = $(this).find(".iva");
                var ice = $(this).find(".ice");
                var precio = $(this).find(".precio_u");

                $.ajax({
                    url: url,
                    type: 'POST',
                    async: false,
                    cache: false,
                    timeout: 300,
                    data : {
                        'id_item': $(item).val(),
                        'cliente': cliente
                    },
                      beforeSend: function(msj){
                        $('#loading').show();
                        $('body').css('display', '0.5');
                    },
                    error: function(data){
                      return true;
                    },

                    success: function(data){
                        $(id_item).val(data[0].id);
                        $(tipo).val(data[0].tipo);
                        $(iva).val(data[0].iva);
                        $(precio).val(data[0].precio);
                        $(ice).val(data[0].ice);

                        $('#loading').fadeOut();
                        $('body').css('opacity', '1');
                    }
                });
            });
        }
    }
}

/***********************************
 * Agrega puntos decimales a los
 * inputs
 * @param e
 **********************************/
function agregar_punto_VENTA(e){
    var numero = $(e).val();
    var tr = $(e).parent().parent();
    var subtotal_t = $(tr).find(".subtotal");

    if(numero != ""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));

        else

            $(e).val((0).toFixed(2));
            $(subtotal_t).val("");
            iteraccionCombosDescuento();
            calcularSubtotalesPepa();
            DescuetosPorVenta();
    }
    else
        $(e).val((0).toFixed(2));
}

/************************************************************
 * Agregar el autocompletado del item
 * @param message
 ************************************************************/
function log( message ) {
    $( "<div>" ).text( message ).prependTo( "#log" );
    $( "#log" ).scrollTop( 0 );
}

/************************************************************
* Funcion para mostar la fecha de vencimiento del documento
*************************************************************/
function MostrarFechaVenci(e){
    if($("#id_forma_pago").val()==2){
        if($(e).val()!="")
        {
            $(e).removeClass("campo_requerido");
            var Fecha = new Date();
            // Ponemos la fecha
            var sFecha = $("#id_fecha").val();
            var aFecha = sFecha.split("-");
            Fecha.setDate(aFecha[2]);
            Fecha.setMonth(aFecha[1]-1);
            Fecha.setFullYear(aFecha[0]);
            // Sacamos los milisegundos, le sumamos los dias
            // y lo pones de nuevo en la fecha
            var fFecha=Fecha.getTime();
            var dias = parseInt($(e).val());
            fFecha=fFecha+(1000*60*60*24*dias);
            Fecha.setTime(fFecha);
            if(Fecha.getMonth()<9)
                $("#dia_vencimiento").html("<b>"+Fecha.getFullYear() + "-" + "0" +(Fecha.getMonth()+1) + "-" + Fecha.getDate()+"</b>");
            else
                $("#dia_vencimiento").html("<b>"+Fecha.getFullYear() + "-" + (Fecha.getMonth()+1) + "-" + Fecha.getDate()+"</b>");

            $("#dia_vencimiento").attr('style', 'top: 8px; position: relative;');
        }
        else
        {
            $("#dia_vencimiento").html("");
            $(e).addClass("campo_requerido");
        }
    }
}

/************************
 * Función Porcentaje Descuento para iteraccion de Descuento
 * ********************/
function iteraccionCombosDescuento(tabid){
    var porc_desce = $("#id_descuento_porc").val();
    var tipo_desce = $("#id_tipo_descuento").val();

    if(porc_desce == "" && tipo_desce == ""){
        $("#hide_input").attr("style","display:none");
        $("#id_monto_descuento").attr("style","display:none");
        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");
        $("#hide_input2").attr("style","display:none");
        $("#hide_input_doce").attr("style","display:none");
    }

    if(porc_desce == "" && tipo_desce != ""){
        $("#id_monto_descuento").attr("disabled",false);
        $("#hide_input").attr("style","display:none");
        $("#id_monto_descuento").attr("style","display:none");
        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");
        $("#camuflado").html("");
        ///////////////////////////////////////////
        $("#hide_input_doce").attr("style","display:none");
        $("#hide_input2").attr("style","display:none");
        $("#id_monto_descuento_cero").attr("disabled",false);
        $("#id_monto_descuento_doce").attr("disabled",false);
        $("#camuflado2").html("");
        $("#camuflado_3").html("");
    }
    if(porc_desce != "" && tipo_desce == ""){
        $("button[data-id='id_tipo_descuento']").addClass("campo_requerido");
        ///////////////////////////////////////////
        $("#hide_input").attr("style","display:none");
        $("#id_monto_descuento").attr("style", "display: none");
        $("#hide_input_doce").attr("style","display:none");
        $("#hide_input2").attr("style","display:none");
        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");
    }

    if(porc_desce == "1" && tipo_desce == 1){
        $("#hide_tr_2").attr("style","display: none");
        $("#hide_tr_3").attr("style","display:none");

        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");

        $("#hide_tr_1").attr("style", "display: show");
        $("button[data-id='id_tipo_descuento']").removeClass("campo_requerido");
        $("#hide_input").attr("style","display:block");
        $("#id_monto_descuento").attr("style","display:block; width: 45px; margin-left: 72px; margin-right: -20px; ");

        $("#simbolo_descuento").html("");
        $("#id_monto_descuento").addClass("campo_requerido");
        $("#id_monto_descuento").attr("disabled",false);
        $("#simbolo_descuento").append('<b>% </b>');
        /////////////////////////////////////////////
        $("#tarifa12").html("");
        $("#tarifa0").html("");
        //////////////////////////////////////////////
         $(tabid).find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
         });
    }

    if(porc_desce == "2" && tipo_desce == 1){
        $("#id_monto_descuento").attr("style","display: none");
        $("#hide_tr_1").attr("style", "display: none");
        var flag12 = false;
        var flag0 = false;

        $(tabid+" tbody tr").each(function(index)
        {
            var iva = $(this).find(".iva");

            if($(iva).val()!=""){
                if (parseFloat($(iva).val()) != 0.0){
                    flag12 = true;
                }
                if (parseFloat($(iva).val()) == 0.0){
                    flag0 = true;
                }
                if (flag12==true || flag0==true ){
                    $("#tarifa12").html("12%");
                    $("#hide_tr_2").attr("style", "display: show");
                    $("#id_monto_descuento_doce").attr("style","display: block; width: 45px; margin-left: 82px; margin-right: -20px; ");
                    $("#tarifa0").html("0%");
                    $("#hide_tr_3").attr("style", "display: show");
                    $("#id_monto_descuento_cero").attr("style","display: block; width: 45px; margin-left: 82px; margin-right: -20px;");
                }
                if (flag12==true && flag0==false){
                    $("#hide_tr_3").attr("style", "display: none");
                    $("#id_monto_descuento_cero").attr("style","display:none");

                    $("#tarifa12").html("12%");
                    $("#hide_tr_2").attr("style", "display: show");
                    $("#id_monto_descuento_doce").attr("style","display: block; width: 45px; margin-left: 82px; margin-right: -20px; ");
                }
                if (flag0==true && flag12==false){
                    $("#hide_tr_2").attr("style", "display: none");
                    $("#id_monto_descuento_doce").attr("style","display:none");

                    $("#tarifa0").html("0%");
                    $("#hide_tr_3").attr("style", "display: show");
                    $("#id_monto_descuento_cero").attr("style","display: block; width: 45px; margin-left: 82px; margin-right: -20px;");
                }
            }
        });

        $("#simbolo_descuento").html("");
        $("#simbolo_descuento").append('<b>$ </b>');
        $("#id_monto_descuento_cero").addClass("campo_requerido");
        $("#id_monto_descuento_cero").val("");
        $("#id_monto_descuento_cero").attr("disabled",false);
        $("#id_monto_descuento_doce").addClass("campo_requerido");
        $("#id_monto_descuento_doce").val("");
        $("#id_monto_descuento_doce").attr("disabled",false);
        /////////////////////////////////////////////////////////////////
         $(tabid).find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });
    }

     if(porc_desce == "2" && tipo_desce == ""){
        $("#simbolo_descuento").html("");
        $("button[data-id='id_tipo_descuento']").addClass("campo_requerido");
    }

     if(porc_desce == "1" && tipo_desce == 2){
        $("#simbolo_descuento").html("");
        $("#simbolo_descuento").append('<b>% </b>');

        $("#hide_tr_1").attr("style", "display: none");
        $("#hide_tr_2").attr("style", "display: none");
        $("#hide_tr_3").attr("style", "display: none");

        $("#id_monto_descuento").attr("style","display: none");
        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");
        //////////////////////////////////////////////////////////////////
        $(tabid).find(".descuento").each(function(){
            $(this).parent().attr("style","display: show");
        });
    }

    if(porc_desce == "2" && tipo_desce == 2){
        $("#simbolo_descuento").html("");
        $("#simbolo_descuento").append('<b>$ </b>');

        $("#hide_tr_1").attr("style", "display: none");
        $("#hide_tr_2").attr("style", "display: none");
        $("#hide_tr_3").attr("style", "display: none");

        $("#id_monto_descuento").attr("style","display: none");
        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");
        //////////////////////////////////////////////////////////////////
        $(tabid).find(".descuento").each(function(){
            $(this).parent().attr("style","display: show");
        });
    }

}

/************************
 * Función para ocultar la Columna de Descuento según el escenario
 * ********************/
function columnahide(tbid){
    if ($("#id_descuento_porc").val()=="" && $("#id_tipo_descuento").val()==""){
        $(tbid).find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });
        $("#hide_tr_1").attr("style","display: none");
        $("#hide_tr_2").attr("style","display: none");
        $("#hide_tr_3").attr("style","display: none");

        $("#id_monto_descuento").attr("style","display: none");
        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");
    }


    if ($("#id_descuento_porc").val()=="" && $("#id_tipo_descuento").val()!=""){
        $(tbid).find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });
        $("#hide_tr_1").attr("style","display: none");
        $("#hide_tr_2").attr("style","display: none");
        $("#hide_tr_3").attr("style","display: none");

        $("#id_monto_descuento").attr("style","display: none");
        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");
    }
    if ($("#id_descuento_porc").val()!="" && $("#id_tipo_descuento").val()==""){
        $(tbid).find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });
        $("#hide_tr_1").attr("style","display: none");
        $("#hide_tr_2").attr("style","display: none");
        $("#hide_tr_3").attr("style","display: none");

        $("#id_monto_descuento").attr("style","display: none");
        $("#id_monto_descuento_cero").attr("style","display:none");
        $("#id_monto_descuento_doce").attr("style","display:none");
    }
}

/**************************
* Función para mostrar el descuento según el check
**************************/
function MostrarDescuento(tabid){
    var desc = $("#id_select_desc");
    $("#show-descuento").hide();
    if ( $(desc).is(':checked')){
        $("#show-descuento").show();
    }
    $(desc).click( function(){
        if($(this).is(':checked')){
            $("#show-descuento").show();
        }
        else{
            $("#show-descuento").hide();
            $("#id_descuento_porc").prop('selectedIndex', 0).selectpicker("refresh");
            $("#id_tipo_descuento").prop('selectedIndex', 0).selectpicker("refresh");
            $(tabid).find(".descuento").each(function(){
                $(this).parent().attr("style","display: none");
                $(this).val(0.0);
            });

            $("#hide_tr_2").hide();
            $("#hide_tr_1").hide();
            $("#hide_tr_3").hide();

            $("#id_monto_descuento").hide();
            $("#id_monto_descuento_doce").hide();
            $("#id_monto_descuento_cero").hide();

            $("#id_monto_descuento").val(0.0);
            $("#id_monto_descuento_doce").val(0.0);
            $("#id_monto_descuento_cero").val(0.0);
            calcularSubtotalesPepa();
            DescuetosPorVenta();
            calcularSubtotalesStock();
            DescuetosPorVentaStock();
        }
    });
}

/**************************
* Función para calcular el Subtotal sin Desc. y por Desc. producto
**************************/
function calcularSubtotalesPepa(){
    var descuento_combo = $("#id_descuento_porc").val();
    var tipo_combo_descuento = $("#id_tipo_descuento").val();
    /******************** Sin Descuento ************************/
    var cantidad_porcentaje = 0.0;
    var precio_u_porcentaje = 0.0;
    var proc_iva = 0.0;
    var proc_ice = 0.0;
    var subtotales_iva_porcentaje = 0.0;
    var iva_total_procentaje = 0.0;
    var ice_total_porcentaje = 0.0;
    var x = 0.0;
    var subtotales_iva_porcentaje_cero = 0.0;
    var cantidad_porcentaje_cero = 0.0;
    var precio_u_porcentaje_cero = 0.0;
    var yz = 0.0;
    /**********************************************************/
    /*********************** Descuento % Producto *************/
    var cantidad_valores = 0.0;
    var precio_valores =0.0;
    var proc_iva_prod = 0.0;
    var proc_desc_prod = 0.0;
    var proc_ice_prod = 0.0;
    var subtotales_descuento_producto_pro = 0.0;
    var totales_desc = 0.0;
    var totales_ice = 0.0;
    var iva_desc = 0.0;
    var subtotales_descuento_producto_cero = 0.0;
    var cantidad_valores_cero = 0.0;
    var precio_valores_cero = 0.0;
    var proc_iva_prod_cero = 0.0;
    var proc_desc_prod_cero = 0.0;
    var totales_desc_cero = 0.0;
    var zdesc = 0.0;
    /**********************************************************/
    var bandera=0;
    var tmp = 0.0;
    var tmp_cero = 0.0;
    var cont = 0;
    var flag=false;
    /**********************************************************/
    var subtotal12 = 0.0;
    var subtotal0 = 0.0;
    var subtotal12_monto = 0.0;
    var subtotal0_monto = 0.0;
    /**************************************************************/
    if (descuento_combo==1 && tipo_combo_descuento==2){
        bandera=1;
    }
    if (descuento_combo==2 && tipo_combo_descuento==2){
        bandera=2;
    }

    $("#tabla_formset_venta_test tbody tr").each(function(index)
    {
        var cantidad = $(this).find(".cantidad");
        var precio = $(this).find(".precio_u");
        var iva = $(this).find(".iva");
        var ice = $(this).find(".ice");
        var descuento = $(this).find(".descuento"); // Descuento del detalle de la venta, interviene en el cálculo en el caso de Descuento por producto
        var subtotal = $(this).find(".subtotal");
        // Sin Descuento

        if ( descuento_combo == "" && tipo_combo_descuento == "" ){
            if($(cantidad).val() !="" && $(precio).val() !="" && $(iva).val() !="")
            {
                if ($(iva).val()!=0 ){
                    cantidad_porcentaje = parseFloat($(cantidad).val());
                    precio_u_porcentaje = parseFloat($(precio).val());
                    proc_iva = parseFloat($(iva).val());

                    if ($(ice).val()==""){
                        proc_ice = 0.0;
                    }else{
                        proc_ice = parseFloat($(ice).val());
                    }
                    subtotales_iva_porcentaje= subtotales_iva_porcentaje + parseFloat(cantidad_porcentaje*precio_u_porcentaje);
                    iva_total_procentaje =  (subtotales_iva_porcentaje)*(proc_iva/100);
                    ice_total_porcentaje = ice_total_porcentaje + parseFloat((cantidad_porcentaje*precio_u_porcentaje)*(proc_ice/100));
                    $(subtotal).val(parseFloat(cantidad_porcentaje*precio_u_porcentaje));
                    $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                    $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                    $("#descuento").text("$"+0.0);
                    $("#iva").text("$"+iva_total_procentaje.toFixed(2));
                    x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));

                }else{
                     cantidad_porcentaje_cero = parseFloat($(cantidad).val());
                     precio_u_porcentaje_cero = parseFloat($(precio).val());
                     subtotales_iva_porcentaje_cero = subtotales_iva_porcentaje_cero + parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero);

                     $(subtotal).val(parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero));
                     $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                     $("#descuento").text("$"+0.0);
                     x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                     $("#total_pagar").text("$"+x.toFixed(2));
                }
            yz = subtotales_iva_porcentaje+ subtotales_iva_porcentaje_cero;
            $("#total_subtotal").text("$"+yz.toFixed(2));
            }
        }

        else{

            // Descuento por Producto
            if ($(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && tipo_combo_descuento == 2){
                cantidad_valores = parseFloat($(cantidad).val());
                precio_valores = parseFloat($(precio).val());
                proc_iva_prod = parseFloat($(iva).val());
                proc_desc_prod = parseFloat($(descuento).val());

                if(descuento_combo == 1){ // Porcentaje de Descuento por producto
                    if(proc_iva_prod != 0){ // Tiene IVA

                        if ($(ice).val()==""){
                            proc_ice_prod = 0.0;
                        }else{
                            proc_ice_prod = parseFloat($(ice).val());
                        }

                        if (bandera==1 && $(descuento).val()>100){
                            $(descuento).val(0.0);
                            tmp = parseFloat($(descuento).val());
                        }
                        else{
                            tmp = parseFloat($(descuento).val());
                        }

                        if(tmp <= 100 && tmp >= 0){
                            subtotal12 = subtotal12 +  parseFloat((cantidad_valores*precio_valores));

                            subtotales_descuento_producto_pro = subtotales_descuento_producto_pro
                            + parseFloat(cantidad_valores*precio_valores) - parseFloat((cantidad_valores*precio_valores)*tmp/100);
                            totales_desc = totales_desc + parseFloat((cantidad_valores*precio_valores)*(tmp/100));
                            iva_desc = parseFloat(subtotales_descuento_producto_pro*proc_iva_prod/100);
                            totales_ice = totales_ice + parseFloat((cantidad_valores*precio_valores)*(proc_ice_prod/100))
                            $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - parseFloat((cantidad_valores*precio_valores)*tmp/100));
                            $("#subtotal").text("$"+subtotal12.toFixed(2));
                            $("subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                            $("#total_subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                            $("#ice").text("$"+totales_ice.toFixed(2));
                            $("#descuento").text("$"+totales_desc.toFixed(2));
                            $("#iva").text("$"+iva_desc.toFixed(2));
                            x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                            $("#total_pagar").text("$"+x.toFixed(2));
                        }
                        else{
                            $("#tit-alert-ok").html("Alerta");
                            $("#body-alert-ok").html('<p>Por Favor ingrese un porcentaje de descuento válido.</p>');
                            $("#alert-ok").modal();
                            $(descuento).val("");
                            $(descuento).val(0.0);
                            totales_desc = 0.0;
                        }

                    }else{ // IVA = 0.0
                            cantidad_valores_cero = parseFloat($(cantidad).val());
                            precio_valores_cero = parseFloat($(precio).val());
                            proc_iva_prod_cero = parseFloat($(iva).val());
                            proc_desc_prod_cero = parseFloat($(descuento).val());

                            if (bandera==1 && $(descuento).val()>100){
                                $(descuento).val(0.0);
                                tmp_cero = parseFloat($(descuento).val());
                            }
                            else{
                                tmp_cero = parseFloat($(descuento).val());
                            }

                            if(tmp_cero < 100 && tmp_cero >= 0){
                                subtotal0 = subtotal0 + parseFloat(cantidad_valores_cero*precio_valores_cero);
                                subtotales_descuento_producto_cero = subtotales_descuento_producto_cero + parseFloat(cantidad_valores_cero*precio_valores_cero)- parseFloat(cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100);
                                totales_desc_cero = totales_desc_cero + parseFloat((cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100));
                                $(subtotal).val(parseFloat(cantidad_valores_cero*precio_valores_cero) - parseFloat(cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100));
                                $("#subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                                $("#subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                                $("#total_pagar").text("$"+x.toFixed(2));

                            }else{
                                $("#tit-alert-ok").html("Alerta");
                                $("#body-alert-ok").html('<p>Por Favor ingrese un porcentaje de descuento válido.</p>');
                                $("#alert-ok").modal();
                                totales_desc = 0.0;
                                $(descuento).val("");
                                $(descuento).val(0.0);
                                $("#descuento").text("$"+totales_desc.toFixed(2));
                            }
                    }

                    $("#subtotal").text("$"+subtotal12.toFixed(2));
                    $("#subtotal_cero").text("$"+subtotal0.toFixed(2));
                    $("#total_subtotal").text("$"+parseFloat((subtotales_descuento_producto_pro) + (subtotales_descuento_producto_cero)).toFixed(2));
                    $("#ice").text("$"+totales_ice.toFixed(2));
                    zdesc = totales_desc + totales_desc_cero;
                    $("#descuento").text("$"+zdesc.toFixed(2));
                    $("#iva").text("$"+iva_desc.toFixed(2));
                    x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));
                    cont++;
                }
                else
                {
                    // POR MONTO PRODUCTO
                    if(descuento_combo == 2){
                        if(proc_iva_prod != 0){
                            if ($(ice).val()==""){
                                proc_ice_prod = 0.0;
                            }else{
                                proc_ice_prod = parseFloat($(ice).val());
                            }

                            if(parseFloat(cantidad_valores*precio_valores)>$(descuento).val()){
                                subtotal12_monto = subtotal12_monto + parseFloat(cantidad_valores*precio_valores);
                                 subtotales_descuento_producto_pro = subtotales_descuento_producto_pro
                                + parseFloat(cantidad_valores*precio_valores) - parseFloat($(descuento).val());
                                totales_desc = totales_desc + parseFloat($(descuento).val());
                                iva_desc = parseFloat(subtotales_descuento_producto_pro*proc_iva_prod/100);
                                totales_ice = totales_ice + parseFloat((cantidad_valores*precio_valores)*(proc_ice_prod/100))

                                $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - parseFloat($(descuento).val()));
                                $("#subtotal").text("$"+subtotal12_monto.toFixed(2));
                                $("subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                                $("#total_subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                $("#descuento").text("$"+totales_desc.toFixed(2));
                                $("#iva").text("$"+iva_desc.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                                $("#total_pagar").text("$"+x.toFixed(2));
                            }
                            else{

                                 $("#tit-alert-ok").html("Alerta");
                                 $("#body-alert-ok").html('<p>Por Favor Ingrese un monto de descuento válido, acorde al subtotal.</p>');
                                 $("#alert-ok").modal();
                                 $(descuento).val("");
                                 $(descuento).val(0.0);
                            }
                        }// IVA == 0
                        else{

                            if(parseFloat(cantidad_valores*precio_valores)>$(descuento).val()){
                                subtotal0_monto = subtotal0_monto + parseFloat(cantidad_valores*precio_valores);
                                subtotales_descuento_producto_cero = subtotales_descuento_producto_cero
                                    + parseFloat(cantidad_valores*precio_valores)
                                    - $(descuento).val();
                                totales_desc_cero = totales_desc_cero + parseFloat($(descuento).val());
                                $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - $(descuento).val() );
                                $("#subtotal").text("$"+subtotal12_monto.toFixed(2));
                                $("#subtotal_cero").text("$"+subtotal0_monto.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero; // falta valores
                                $("#total_pagar").text("$"+x.toFixed(2));

                            }else{

                                 $("#tit-alert-ok").html("Alerta");
                                 $("#body-alert-ok").html('<p>Por Favor Ingrese un monto de descuento válido, acorde al subtotal.</p>');
                                 $("#alert-ok").modal();
                                 $(descuento).val("");
                                 $(descuento).val(0.0);
                            }
                        }
                        $("#subtotal").text("$"+subtotal12_monto.toFixed(2));
                        $("#subtotal_cero").text("$"+subtotal0_monto.toFixed(2));
                        $("#total_subtotal").text("$"+parseFloat((subtotal12_monto) + (subtotal0_monto)).toFixed(2));
                        $("#ice").text("$"+totales_ice.toFixed(2));
                        zdesc = totales_desc + totales_desc_cero;
                        $("#descuento").text("$"+zdesc.toFixed(2));
                        $("#iva").text("$"+iva_desc.toFixed(2));
                        x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                        $("#total_pagar").text("$"+x.toFixed(2));

                    }
                }
            }
        }
    });
}

/**************************
* Función para calcular el Subtotal  Desc. por venta
**************************/
function DescuetosPorVenta(){
   var cantidad_porcentaje = 0.0;
   var precio_u_porcentaje = 0.0;
   var proc_iva = 0.0;
   var proc_ice = 0.0;
   var descuento_combo = $("#id_descuento_porc").val();
   var tipo_combo_descuento = $("#id_tipo_descuento").val();

   var monto_descuento = $("#id_monto_descuento").val();

   var monto_descuento_doce = $("#id_monto_descuento_doce").val();
   var monto_descuento_cero = $("#id_monto_descuento_cero").val();

   var subtotales_iva_porcentaje = 0.0;
   var iva_total_procentaje = 0.0;
   var ice_total_porcentaje = 0.0;
   var x = 0.0;
   var yz = 0.0;
   var subtotales_iva_porcentaje_cero = 0.0;
   var cantidad_porcentaje_cero = 0.0;
   var precio_u_porcentaje_cero = 0.0;
   var result_descuento_porc = 0.0;
   var result_iva_porc = 0.0;
   var tmp = 0.0;
   var z = 0.0;
   var result_descuento_porc_iva_cero = 0.0;
   var tmp_iva_cero = 0.0;
   var zdesc = 0.0;
   var bandera1=0;
   var cont=0;
   /***************************************************************/
   var tarifa12_monto = 0.0;
   var tarifa0_monto = 0.0;
   var tarifa12_monto_iva = 0.0;
   var descuento12_monto = 0.0;
   var descuento0_monto = 0.0;
   var total_descuento_monto_final = 0.0;
   var cont1 = 0;
   var tam = 0;
   var result_total_pagar = 0.0;
   var bandera_result=0;
   var bandera_result2=0;
   tam = parseInt($("#tabla_formset_venta_test tbody tr").length);

    $("#tabla_formset_venta_test tbody tr").each(function(index)
    {
        var cantidad = $(this).find(".cantidad");
        var precio = $(this).find(".precio_u");
        var iva = $(this).find(".iva");
        var ice = $(this).find(".ice");
        var subtotal = $(this).find(".subtotal");
        cont1++;

        if(tipo_combo_descuento==1 && $(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && descuento_combo !=""){
            if ($(iva).val()!=0 ){
                cantidad_porcentaje = parseFloat($(cantidad).val());
                precio_u_porcentaje = parseFloat($(precio).val());
                proc_iva = parseFloat($(iva).val());
                if ($(ice).val()==""){
                     proc_ice = 0.0;
                }else{
                     proc_ice = parseFloat($(ice).val());
                }
                subtotales_iva_porcentaje= subtotales_iva_porcentaje + parseFloat(cantidad_porcentaje*precio_u_porcentaje);
                iva_total_procentaje =  (subtotales_iva_porcentaje)*(proc_iva/100);
                ice_total_porcentaje = ice_total_porcentaje + parseFloat((cantidad_porcentaje*precio_u_porcentaje)*(proc_ice/100));
                $(subtotal).val(parseFloat(cantidad_porcentaje*precio_u_porcentaje));
                $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                $("#descuento").text("$"+0.0);
                $("#iva").text("$"+iva_total_procentaje.toFixed(2));
                x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                $("#total_pagar").text("$"+x.toFixed(2));

            }else{
                 cantidad_porcentaje_cero = parseFloat($(cantidad).val());
                 precio_u_porcentaje_cero = parseFloat($(precio).val());
                 subtotales_iva_porcentaje_cero = subtotales_iva_porcentaje_cero + parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero);
                 $(subtotal).val(parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero));
                 $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                 $("#descuento").text("$"+0.0);
                 x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                 $("#total_pagar").text("$"+x.toFixed(2));
                }
            yz = subtotales_iva_porcentaje+ subtotales_iva_porcentaje_cero;
            $("#total_subtotal").text("$"+yz.toFixed(2));
        }

        if(tipo_combo_descuento==1 && $(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && descuento_combo !="")
        {

            if(descuento_combo==1){//Porcentaje
                if ( $("#id_monto_descuento").val()==""){
                    $("#id_monto_descuento").val(0.0);
                }

                if($(iva).val()!=0){
                    result_descuento_porc = (subtotales_iva_porcentaje)*(monto_descuento/100);
                    tmp = subtotales_iva_porcentaje - result_descuento_porc;
                    result_iva_porc = (subtotales_iva_porcentaje - result_descuento_porc)*(proc_iva/100);
                    $("#descuento").text("$"+result_descuento_porc.toFixed(2));
                    $("#iva").text("$"+result_iva_porc.toFixed(2));
                    x = tmp  + ice_total_porcentaje + result_iva_porc + result_descuento_porc_iva_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));

                }else{
                    //iva=0
                    result_descuento_porc_iva_cero = (subtotales_iva_porcentaje_cero)*(monto_descuento/100);
                    tmp_iva_cero = subtotales_iva_porcentaje_cero - result_descuento_porc_iva_cero;
                    $("#descuento").text("$"+result_descuento_porc_iva_cero.toFixed(2));
                    x = tmp_iva_cero  + ice_total_porcentaje + result_iva_porc + result_descuento_porc_iva_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));
                }


                $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                $("#total_subtotal").text("$"+parseFloat((subtotales_iva_porcentaje) + (subtotales_iva_porcentaje_cero)).toFixed(2));
                $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                zdesc = result_descuento_porc + result_descuento_porc_iva_cero;

                $("#descuento").text("$"+zdesc.toFixed(2));
                $("#iva").text("$"+result_iva_porc.toFixed(2));
                z = tmp + tmp_iva_cero;
                x = z + ice_total_porcentaje + result_iva_porc ;
                $("#total_pagar").text("$"+x.toFixed(2));
            }

            if(descuento_combo==2){//Monto
                if (monto_descuento_doce == ""  && monto_descuento_cero==""){
                    $("#id_monto_descuento_doce").val(0.0);
                    $("#id_monto_descuento_cero").val(0.0);
                }

                if ($(iva).val()!=0){
                    tarifa12_monto = tarifa12_monto + parseFloat($(cantidad).val()*$(precio).val());
                    descuento12_monto = tarifa12_monto - $("#id_monto_descuento_doce").val();
                    tarifa12_monto_iva = (descuento12_monto)*($(iva).val()/100);
                }else{
                     tarifa0_monto = tarifa0_monto + parseFloat($(cantidad).val()*$(precio).val());
                     descuento0_monto = tarifa0_monto - $("#id_monto_descuento_cero").val();
                }

                 if(tarifa12_monto < $("#id_monto_descuento_doce").val() && cont1==tam){
                    $("#tit-alert-ok").html("Alerta");
                    $("#body-alert-ok").html('<p>Por Favor ingrese un monto de descuento válido según el subtotal_12%.</p>');
                    $("#alert-ok").modal();

                    $("#id_monto_descuento_doce").val("");
                    $("#id_monto_descuento_doce").addClass("campo_requerido");
                    $("#id_monto_descuento_doce").val(0.0);
                    bandera_result++;
                 }
                 if(tarifa0_monto < $("#id_monto_descuento_cero").val()&& cont1==tam){
                    $("#tit-alert-ok").html("Alerta");
                    $("#body-alert-ok").html('<p>Por Favor ingrese un monto de descuento válido según el subtotal_0%.</p>');
                    $("#alert-ok").modal();

                     $("#id_monto_descuento_cero").val("");
                     $("#id_monto_descuento_cero").addClass("campo_requerido");
                     $("#id_monto_descuento_cero").val(0.0);
                     bandera_result2++;
                 }
                 if(bandera_result==0 && bandera_result2==0){
                     $("#subtotal").text("$"+tarifa12_monto.toFixed(2));
                     $("#subtotal_cero").text("$"+tarifa0_monto.toFixed(2));
                     $("#total_subtotal").text("$"+parseFloat((tarifa12_monto) + (tarifa0_monto)).toFixed(2));
                     total_descuento_monto_final = parseFloat($("#id_monto_descuento_cero").val()) + parseFloat($("#id_monto_descuento_doce").val());
                     $("#descuento").text("$"+total_descuento_monto_final.toFixed(2));
                     $("#iva").text("$"+tarifa12_monto_iva.toFixed(2));
                     $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                     result_total_pagar = ice_total_porcentaje + descuento12_monto + tarifa12_monto_iva + descuento0_monto;
                     $("#total_pagar").text("$"+result_total_pagar.toFixed(2));
                 }
            }
        }

    });
}

/*********************************************************************
* Función para agregar el Título(tooltip).
**********************************************************************/
function AgregarTitulo(e){
    var padre = $(e).parent().get(0);
    var boton_combo = $(padre).children()[1];
    var titulo = "";
    var div_texto = boton_combo.firstChild.firstChild;
    var codigo = "";
    $(e).find("option").each(function(){
      if(this.selected)
      {
          titulo = $(this).text();
      }
    });
    $(boton_combo).tooltip('hide')
      .attr('data-original-title', titulo)
      .tooltip('fixTitle')
      //.tooltip('show');
      codigo = titulo.split("-")[0];

    $(div_texto).change(function(){
        $(this).text(codigo);
    });
}

/***********************
 * Función Existe Items
 **********************/
function ExisteItems(){
    var existe_item = false;
    $(ID_TABLA_VENTA+" tbody tr").each(function(index){
        var item = $(this).find(".item");
        if ($(item).val()!=""){
            existe_item=true;
        }
    });

    return existe_item;
}

/**********************************************************************
 * Función para validar el ingreso de Items repetidos
 **********************************************************************/
function ValidateItemsRepetidos(obj, tab_id){
    var array = [];
    var acum = 0;
    var cont = 0;
    var tr = $(obj).parent().parent();
    var item = $(tr).find(".item");
    var id_item = $(tr).find(".id_item");
    var iva_class = $(tr).find(".iva");
    var precio_class = $(tr).find('.precio_u');
    var ice_class= $(tr).find(".ice");
    var tipo_item = $(tr).find(".tipo_item");
    var val_tipo_item = $(tr).find("option:selected").attr("data-tipo");

    $(tab_id+" tbody").find("tr").each(function(){
        var item = $(this).find(".item").val(); // item
        array.push(item);
        cont++;
    });

    if(array.length>1){
        for (var i=0; i<cont; i++){
            for(var j=i+1; j<cont; j++){
                if(array[i] != "" && array[j] != "" && array[i] == array[j]){
                    acum++;
                    array.pop();
                }
            }
        }
    }

    if(acum>0){
        $("#tit-alert-ok").html("NO puede ingresar un item repetido");
        $("#body-alert-ok").html("");
        $("#alert-ok").modal();
        $(id_item).val("");
        $(item).val("");
        $(iva_class).val("");
        $(ice_class).val("");
        $(precio_class).val("");
        $(tipo_item).val("");
    }

}

/**********************************************************************
 *  Función para presentar el formulario de Guía Remisión
 **********************************************************************/
function presentaForm(){
    var guia_rem = $("#id_guia_remision");

    $("#guia_remision").attr("style", "display: none");

    if ( $(guia_rem).is(':checked')){
        $("#guia_remision").attr("style", "display: show");
    }
    $(guia_rem).click( function(){
        if($(this).is(':checked')){
            $("#guia_remision").attr("style", "display: show");
        }
        else{
            $("#guia_remision").attr("style", "display: none");
        }
    });
}

/**********************************************************************
 *  Función para validar la cantidad entregada de la Guía de Remisión
 *********************************************************************/
function cantidadEntregada(){
    var checked = $("#id_guia_remision");
    var total_cantidad_entregada = 0.0;

    $(ID_TABLA_VENTA+" tbody tr").each(function(index){
        var cantidad_entregada = $(this).find(".cant_entregada");
        var cantidad_venta = $(this).find(".cantidad");
        var tipo_item = $(this).find($(".tipo_item"));

        if ($(cantidad_entregada).val() != ""){
            total_cantidad_entregada = total_cantidad_entregada + parseFloat($(cantidad_entregada).val());
        }

        if ($(tipo_item).val() != ""){

            if ($(tipo_item).val() == 2){
                $(cantidad_entregada).val("");
                $(cantidad_entregada).attr("disabled", true);

            }else{
                $(cantidad_entregada).attr("disabled", false);
            }
        }

        if (parseFloat(total_cantidad_entregada) == 0.0){
            $(checked).removeAttr("checked");
            presentaForm();
            $(checked).attr("disabled", true);

        }else{

             if (parseFloat($(cantidad_entregada).val()) > parseFloat($(cantidad_venta).val())){
                $("#alert-ok").attr("data-id", $(this).attr("id"));
                $("#alert-ok").attr("data-target",1);
                $("#tit-alert").html("Alerta");
                $("#body-alert-ok").html('<p><strong>Por Favor ingrese una cantidad de entrega' +
                    ' correcta.</span></strong></p>');
                $("#alert-ok").modal();
                $(cantidad_entregada).val(0.0);
                total_cantidad_entregada = 0.0;

                if ($(checked).is(":checked")){
                    $(checked).prop("checked", "");
                    presentaForm();
                }

                if (total_cantidad_entregada==0.0){
                    $(checked).attr("disabled", true);
                }

             }else{
                 $(checked).attr("disabled", false);
             }
        }
    });
}

/********************************************************
 *  Función para validar la Fecha de la Guía Remisión
 ********************************************************/
function ValidarFechaGuia(){
    try
    {
        var dia_inicio = Date.parse($("#id_fecha_inicio").val());
        var date_ini = new Date(dia_inicio);

        var dia_final = Date.parse($("#id_fecha_final").val());
        var date_final = new Date(dia_final);
        if(date_ini > date_final)
        {
            $("#tit-alert-ok").html("La fecha final o de entrega de la guía debe de ser mayor igual " +
                "a la fecha de inicio");
            $("#body-alert-ok").html("");
            $("#alert-ok").modal();
            $("#id_fecha_final").val("");
        }
    }
    catch (err)
    {
        alert(err);
    }
}

/****************************************
 *  Función para validar la Fecha Acutal
 ****************************************/
function validar_fecha_actual(obj){
    try
    {
        var str_fecha = fecha_actual.split("-");
        var date = new Date(parseInt(str_fecha[0]),parseInt(str_fecha[1]),parseInt(str_fecha[2]));
        var str_fecha_reg = $(obj).val().split("-");
        var fecha_reg = new Date(parseInt(str_fecha_reg[0]),parseInt(str_fecha_reg[1]),parseInt(str_fecha_reg[2]));

        if(fecha_reg > date){
            $("#tit-alert-ok").html("NO puede ingresar una fecha superior a la fecha actual");
            $("#body-alert-ok").html("");
            $("#alert-ok").modal();
            $(obj).val(fecha_actual);
        }
    }
    catch(err){
        $(obj).val("");
        $("#tit-alert-ok").html("NO puede ingresar una fecha superior a la fecha actual");
        $("#body-alert-ok").html("");
        $("#alert-ok").modal();
    }
}

/*******************************************************
 *  Función para validar el tipo de id del form cliente
 *******************************************************/
function formCliente(tipo_id){
    if(tipo_id == "P"){
            $("#select_tipo_cliente").show();
    }else{
        $("#select_tipo_cliente").hide();
    }
}
/**************************************************
 *  Función para agregar un cliente desde la venta
 **************************************************/
function AgregarCliente(e){
    var id_clien = $("#id_Num_Identificacion");
    var tipo_id = $("#id_tipo_identificacion").val();

    if (validar_ruc_venta(id_clien, tipo_id)){
        var datastring = $("#form_cliente").serialize();
        var url = $("#form_cliente").attr("action");

        $.ajax(
        {
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: datastring,

            error: function(){
                return true;
            },
            success: function(data){
                var cont = 0;
                var mensajes = new Array();
                for(var i=0; i<data.length; i++)
                {
                    if(data[i].status != 1)
                    {
                        mensajes.push(data[i].mensaje)
                        cont++;
                    }
                }
                if(cont != 0)
                {
                    var stringhtml = "";
                    for(i=0; i<mensajes.length; i++)
                    {
                        stringhtml = (stringhtml+"<p>"+mensajes[i]+"</p>")
                    }

                    $("#error").fadeIn("slow");
                    $("#error-cliente").html(stringhtml)
                }
                if(cont == 0)
                {
                    var combo_clientes = $("#id_cliente");
                    var id_cliente = data[0].id;
                    var url = $("#form_cliente").attr("data-search");
                    combo_clientes.empty();

                    $.ajax({
                        url:url,
                        type: 'POST',
                        async: false,
                        cache: false,
                        timeout: 300,
                        beforeSend: function(msj){
                        $('#loading').show();
                        $('body').css('display', '0.5');
                        },
                        error: function(){
                            return true;
                        },
                        success: function(data)
                        {
                            for(var i = 0; i < data.length; i++)
                            {
                                if(id_cliente == data[i].id){
                                    $(combo_clientes).append('<option value="' + data[i].id +'" selected="selected">' +
                                        '' + data[i].ruc + " - "+ data[i].razon_social + '</option>');

                                    seleccionarDirecciones();

                                }else{
                                    $(combo_clientes).append('<option value="' + data[i].id +'" >' +
                                    '' + data[i].ruc + " - "+ data[i].razon_social +'</option>');
                                }

                            }
                        }
                    });

                    $('#div-cliente').modal('hide');
                    $("#id_cliente").selectpicker("refresh");

                    $('#loading').fadeOut();
                    $('body').css('opacity', '1');
                }
            }
        });

    }
    else
    {
        e.preventDefault();
        alert("Número de identificación no válido, por favor verifique");
    }
}

/******************************************************
 * Valida el ruc de los Clientes ingresados
 * @param obj
 * @param tipo_id
 ******************************************************/
function validar_ruc_venta(obj, tipo_id){
    var ruc = $(obj);
    if ($(ruc).val() != "" && tipo_id !="")
    {
        if ( tipo_id == "R" )
        {
            if (isRUC($(ruc)) && $(ruc).val().length==13)
            {
                $(obj).removeClass("campo_requerido");
                return true;
            }
            else{
                $(obj).addClass("campo_requerido");
                return false;
            }
        }
        if( tipo_id == "C" )
        {
            if(isCedula(ruc) && $(ruc).val().length==10){
               $(obj).removeClass("campo_requerido");
                return true;
            }else{
                $(obj).addClass("campo_requerido");
                return false;
            }
        }
        if( tipo_id == "P" ){
            return true;
        }
    }
    else{
        return false;
    }
}

/********************************************************
 * Función para bloquear la cantidad_entregada a los items,
 * tipo servicio que cargan al momento de copiar la venta
 ********************************************************/
function DisabledServicioCantEntrega(){
     $(ID_TABLA_VENTA+" tbody tr").each(function(index){
        var item = $(this).find(".item");
        var cant_ent = $(this).find(".cant_entregada");
        var tipo_item = $(this).find(".tipo_item");

        if ($(item).val() != "" && $(tipo_item).val() == 2){
            $(cant_ent).attr("disabled", "disabled" );
        }
    });
}


/*****   STOCK - FORMSET2 ****/

/***************************************************************************************
 * Función para sleccionar los parametros del item, como: iva, ice, precio
 ***************************************************************************************/
function SelectITemParametros(obj){
    var tr = $(obj).parent().parent();
    var iva = $(tr).find(".iva_stock");
    var item = $(tr).find(".item_stock");
    var id_item = $(tr).find(".id_item_stock");
    var ice = $(tr).find(".ice_stock");
    var precio = $(tr).find(".precio_u_stock");
    var unidad = $(tr).find(".unidad_stock");

    var val_iva = $(tr).find("option:selected").attr("data-iva");
    var val_ice = $(tr).find("option:selected").attr("data-ice");
    var val_precio = $(tr).find("option:selected").attr("data-precio");
    var val_unidad = $(tr).find("option:selected").attr("data-unidad");

    $(id_item).val($(item).val());
    $(item).val($(item).val());
    $(iva).val(val_iva);
    $(ice).val(val_ice);
    $(precio).val(val_precio);
    $(unidad).val(val_unidad);
}


/***************************************************************************
 * Función Ajax para validar si el Item posee cantidad disponible en Bodega
 * @param obj
 * @param url
 ***************************************************************************/
function cantidadBodegaVentaStock(obj, url){
    var tr = $(obj).parent().parent();
    var item = $(tr).find(".item_stock");
    var cantidad = $(tr).find(".cantidad_stock");
    var subtotal = $(tr).find(".subtotal_stock");
    var precio_u = $(tr).find(".precio_u_stock");
    var ice_stock = $(tr).find(".ice_stock");
    var iva_stock = $(tr).find(".iva_stock");
    var unidad_stock = $(tr).find(".unidad_stock");

    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data : {
            'id_item': $(item).val(),
            'cantidad_venta': $(cantidad).val()
        },
        error: function(data){
          return true;
        },
        success: function(data){
            var estado = data[0].status;
            var cantidad_actual = data[0].cantidad_bodega;
            var item_nombre = data[0].item;

            switch (estado){
                case 0:
                    $("#tit-alert-ok").html("El item no se encuentra registrado en bodega.");
                    $("#body-alert-ok").html("");
                    $("#alert-ok").modal();
                    $(cantidad).val("");
                    $(item).val("");
                break;

                case 1: // OK

                break;

                case 2: // Cantidad en Bodega es 0.0
                    $("#tit-alert-ok").html("ALERTA");
                    $("#body-alert-ok").html('<p>El item: '+ item_nombre + ' no posee cantidad disponible en bodega'+'</p>');
                    $("#alert-ok").modal();
                    $(item).val("");
                    $(cantidad).val("");
                    $(subtotal).val("");
                    $(precio_u).val("");
                    $(iva_stock).val("");
                    $(ice_stock).val("");
                    $(unidad_stock).val("");
                break;

                case 3: // Cantidad Venta es mayor a la cantidad en Bodega
                    $("#tit-alert-ok").html("ALERTA");
                    $("#body-alert-ok").html('<p>La cantidad disponible en bodega del item '+ item_nombre + ' es: '+  cantidad_actual +'</p>');
                    $("#alert-ok").modal();
                    $(item).val("");
                    $(cantidad).val("");
                    $(subtotal).val("");
                    $(precio_u).val("");
                    $(iva_stock).val("");
                    $(ice_stock).val("");
                    $(unidad_stock).val("");
                break;

                case 4: // Cantidad vacía

                break;

                case 5: // Items Servicio

                break;

                default :
                break;
            }
        }
    });
}

/***********************************************************
 * Función Ajax para validar si el Item existe en Bodega
 * @param obj
 * @param url
 ***********************************************************/
function ExisteItemBodegaStock(obj, url){
    var tr = $(obj).parent().parent();
    var id_item = $(tr).find(".id_item");
    var item = $(tr).find(".item_stock");
    var iva = $(tr).find(".iva_stock");
    var ice= $(tr).find(".ice_stock");
    var precio_class = $(tr).find('.precio_u_stock');

     $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data : {
            'id_item': $(item).val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
         },
        error: function(data){
          return true;
        },
        success: function(data){
            var estado = data[0].status;
            var nombre_item = data[0].item;

            switch (estado){
            case 0:
                $("#tit-alert-ok").html("ALERTA");
                $("#body-alert-ok").html("El item: "+ nombre_item +",   no se encuentra registrado en bodega.");
                $("#alert-ok").modal();
                $(item).val("");
                $(item).removeAttr("data-original-title");
                $(iva).val("");
                $(precio_class).val("");
                $(ice).val("");

            break;

            case 1:

                default:
                break;
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
}

/**********************************************************************
 * Función para validar el ingreso de Items repetidos
 **********************************************************************/
function ValidateItemsRepetidosStock(obj, tab_id){
    var array = [];
    var acum = 0;
    var cont = 0;
    var tr = $(obj).parent().parent();
    var item = $(tr).find(".item_stock");
    var id_item = $(tr).find(".id_item");
    var iva_class = $(tr).find(".iva_stock");
    var precio_class = $(tr).find('.precio_u_stock');
    var ice_class= $(tr).find(".ice_stock");
    var tipo_item = $(tr).find(".tipo_item_stock");
    var unidad = $(tr).find(".unidad_stock");


    $(tab_id+" tbody").find("tr").each(function(){
        var item = $(this).find(".item_stock").val(); // item
        array.push(item);
        cont++;
    });

    if(array.length>1){
        for (var i=0; i<cont; i++){
            for(var j=i+1; j<cont; j++){
                /* Si el item  es igual*/
                if(array[i] != "" && array[j] != "" && array[i] == array[j]){
                    acum++;
                    array.pop();
                }
            }
        }
    }

    if(acum>0){
        $("#tit-alert-ok").html("NO puede ingresar un item repetido");
        $("#body-alert-ok").html("");
        $("#alert-ok").modal();
        $(id_item).val("");
        $(item).val("");
        $(iva_class).val("");
        $(ice_class).val("");
        $(precio_class).val("");
        $(tipo_item).val("");
        $(unidad).val("");
    }
}


/**************************
* Función para calcular el Subtotal sin Desc. y por Desc. producto
**************************/
function calcularSubtotalesStock(){
    var descuento_combo = $("#id_descuento_porc").val();
    var tipo_combo_descuento = $("#id_tipo_descuento").val();
    /******************** Sin Descuento ************************/
    var cantidad_porcentaje = 0.0;
    var precio_u_porcentaje = 0.0;
    var proc_iva = 0.0;
    var proc_ice = 0.0;
    var subtotales_iva_porcentaje = 0.0;
    var iva_total_procentaje = 0.0;
    var ice_total_porcentaje = 0.0;
    var x = 0.0;
    var subtotales_iva_porcentaje_cero = 0.0;
    var cantidad_porcentaje_cero = 0.0;
    var precio_u_porcentaje_cero = 0.0;
    var yz = 0.0;

    /*********************** Descuento % Producto *************/
    var cantidad_valores = 0.0;
    var precio_valores =0.0;
    var proc_iva_prod = 0.0;
    var proc_desc_prod = 0.0;
    var proc_ice_prod = 0.0;
    var subtotales_descuento_producto_pro = 0.0;
    var totales_desc = 0.0;
    var totales_ice = 0.0;
    var iva_desc = 0.0;
    var subtotales_descuento_producto_cero = 0.0;
    var cantidad_valores_cero = 0.0;
    var precio_valores_cero = 0.0;
    var proc_iva_prod_cero = 0.0;
    var proc_desc_prod_cero = 0.0;
    var totales_desc_cero = 0.0;
    var zdesc = 0.0;
    /**********************************************************/
    var bandera=0;
    var tmp = 0.0;
    var tmp_cero = 0.0;
    var cont = 0;
    var flag=false;
    /**********************************************************/
    var subtotal12 = 0.0;
    var subtotal0 = 0.0;
    var subtotal12_monto = 0.0;
    var subtotal0_monto = 0.0;
    /**************************************************************/
    if (descuento_combo==1 && tipo_combo_descuento==2){
        bandera=1;
    }
    if (descuento_combo==2 && tipo_combo_descuento==2){
        bandera=2;
    }

    $(ID_TABLA_VENTA_STOCK+" tbody tr").each(function(index)
    {
        var cantidad = $(this).find(".cantidad_stock");
        var precio = $(this).find(".precio_u_stock");
        var iva = $(this).find(".iva_stock");
        var ice = $(this).find(".ice_stock");
        var descuento = $(this).find(".descuento");
        var subtotal = $(this).find(".subtotal_stock");
        // Sin Descuento

        if ( descuento_combo == "" && tipo_combo_descuento == "" ){
            if($(cantidad).val() !="" && $(precio).val() !="" && $(iva).val() !="")
            {
                if ($(iva).val()!=0 ){
                    cantidad_porcentaje = parseFloat($(cantidad).val());
                    precio_u_porcentaje = parseFloat($(precio).val());
                    proc_iva = parseFloat($(iva).val());

                    if ($(ice).val()==""){
                        proc_ice = 0.0;
                    }else{
                        proc_ice = parseFloat($(ice).val());
                    }
                    subtotales_iva_porcentaje= subtotales_iva_porcentaje + parseFloat(cantidad_porcentaje*precio_u_porcentaje);
                    iva_total_procentaje =  (subtotales_iva_porcentaje)*(proc_iva/100);
                    ice_total_porcentaje = ice_total_porcentaje + parseFloat((cantidad_porcentaje*precio_u_porcentaje)*(proc_ice/100));
                    $(subtotal).val(parseFloat(cantidad_porcentaje*precio_u_porcentaje));
                    $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                    $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                    $("#descuento").text("$"+0.0);
                    $("#iva").text("$"+iva_total_procentaje.toFixed(2));
                    x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));

                }else{
                     cantidad_porcentaje_cero = parseFloat($(cantidad).val());
                     precio_u_porcentaje_cero = parseFloat($(precio).val());
                     subtotales_iva_porcentaje_cero = subtotales_iva_porcentaje_cero + parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero);

                     $(subtotal).val(parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero));
                     $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                     $("#descuento").text("$"+0.0);
                     x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                     $("#total_pagar").text("$"+x.toFixed(2));
                }
            yz = subtotales_iva_porcentaje+ subtotales_iva_porcentaje_cero;
            $("#total_subtotal").text("$"+yz.toFixed(2));
            }
        }

        else{

            // Descuento por Producto
            if ($(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && tipo_combo_descuento == 2){
                cantidad_valores = parseFloat($(cantidad).val());
                precio_valores = parseFloat($(precio).val());
                proc_iva_prod = parseFloat($(iva).val());
                proc_desc_prod = parseFloat($(descuento).val());

                if(descuento_combo == 1){ // Porcentaje de Descuento por producto
                    if(proc_iva_prod != 0){ // Tiene IVA

                        if ($(ice).val()==""){
                            proc_ice_prod = 0.0;
                        }else{
                            proc_ice_prod = parseFloat($(ice).val());
                        }

                        if (bandera==1 && $(descuento).val()>100){
                            $(descuento).val(0.0);
                            tmp = parseFloat($(descuento).val());
                        }
                        else{
                            tmp = parseFloat($(descuento).val());
                        }

                        if(tmp <= 100 && tmp >= 0){
                            subtotal12 = subtotal12 +  parseFloat((cantidad_valores*precio_valores));
                            subtotales_descuento_producto_pro = subtotales_descuento_producto_pro
                            + parseFloat(cantidad_valores*precio_valores) - parseFloat((cantidad_valores*precio_valores)*tmp/100);
                            totales_desc = totales_desc + parseFloat((cantidad_valores*precio_valores)*(tmp/100));
                            iva_desc = parseFloat(subtotales_descuento_producto_pro*proc_iva_prod/100);
                            totales_ice = totales_ice + parseFloat((cantidad_valores*precio_valores)*(proc_ice_prod/100));
                            $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - parseFloat((cantidad_valores*precio_valores)*tmp/100));
                            $("#subtotal").text("$"+subtotal12.toFixed(2));
                            $("subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                            $("#total_subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                            $("#ice").text("$"+totales_ice.toFixed(2));
                            $("#descuento").text("$"+totales_desc.toFixed(2));
                            $("#iva").text("$"+iva_desc.toFixed(2));
                            x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                            $("#total_pagar").text("$"+x.toFixed(2));
                        }
                        else{
                            $("#tit-alert-ok").html("Alerta");
                            $("#body-alert-ok").html('<p>Por Favor ingrese un porcentaje de descuento válido.</p>');
                            $("#alert-ok").modal();
                            $(descuento).val("");
                            $(descuento).val(0.0);
                            totales_desc = 0.0;
                        }

                    }else{ // IVA = 0.0
                            cantidad_valores_cero = parseFloat($(cantidad).val());
                            precio_valores_cero = parseFloat($(precio).val());
                            proc_iva_prod_cero = parseFloat($(iva).val());
                            proc_desc_prod_cero = parseFloat($(descuento).val());

                            if (bandera==1 && $(descuento).val()>100){
                                $(descuento).val(0.0);
                                tmp_cero = parseFloat($(descuento).val());
                            }
                            else{
                                tmp_cero = parseFloat($(descuento).val());
                            }

                            if(tmp_cero < 100 && tmp_cero >= 0){
                                subtotal0 = subtotal0 + parseFloat(cantidad_valores_cero*precio_valores_cero);
                                subtotales_descuento_producto_cero = subtotales_descuento_producto_cero + parseFloat(cantidad_valores_cero*precio_valores_cero)- parseFloat(cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100);
                                totales_desc_cero = totales_desc_cero + parseFloat((cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100));
                                $(subtotal).val(parseFloat(cantidad_valores_cero*precio_valores_cero) - parseFloat(cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100));
                                $("#subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                                $("#subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                                $("#total_pagar").text("$"+x.toFixed(2));

                            }else{
                                $("#tit-alert-ok").html("Alerta");
                                $("#body-alert-ok").html('<p>Por Favor ingrese un porcentaje de descuento válido.</p>');
                                $("#alert-ok").modal();
                                totales_desc = 0.0;
                                $(descuento).val("");
                                $(descuento).val(0.0);
                                $("#descuento").text("$"+totales_desc.toFixed(2));
                            }
                    }

                    $("#subtotal").text("$"+subtotal12.toFixed(2));
                    $("#subtotal_cero").text("$"+subtotal0.toFixed(2));
                    $("#total_subtotal").text("$"+parseFloat((subtotales_descuento_producto_pro) + (subtotales_descuento_producto_cero)).toFixed(2));
                    $("#ice").text("$"+totales_ice.toFixed(2));
                    zdesc = totales_desc + totales_desc_cero;
                    $("#descuento").text("$"+zdesc.toFixed(2));
                    $("#iva").text("$"+iva_desc.toFixed(2));
                    x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));
                    cont++;
                }
                else
                {
                    // POR MONTO PRODUCTO
                    if(descuento_combo == 2){
                        if(proc_iva_prod != 0){
                            if ($(ice).val()==""){
                                proc_ice_prod = 0.0;
                            }else{
                                proc_ice_prod = parseFloat($(ice).val());
                            }

                            if(parseFloat(cantidad_valores*precio_valores)>$(descuento).val()){
                                subtotal12_monto = subtotal12_monto + parseFloat(cantidad_valores*precio_valores);
                                 subtotales_descuento_producto_pro = subtotales_descuento_producto_pro
                                + parseFloat(cantidad_valores*precio_valores) - parseFloat($(descuento).val());
                                totales_desc = totales_desc + parseFloat($(descuento).val());
                                iva_desc = parseFloat(subtotales_descuento_producto_pro*proc_iva_prod/100);
                                totales_ice = totales_ice + parseFloat((cantidad_valores*precio_valores)*(proc_ice_prod/100))

                                $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - parseFloat($(descuento).val()));
                                $("#subtotal").text("$"+subtotal12_monto.toFixed(2));
                                $("subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                                $("#total_subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                $("#descuento").text("$"+totales_desc.toFixed(2));
                                $("#iva").text("$"+iva_desc.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                                $("#total_pagar").text("$"+x.toFixed(2));
                            }
                            else{

                                 $("#tit-alert-ok").html("Alerta");
                                 $("#body-alert-ok").html('<p>Por Favor Ingrese un monto de descuento válido, acorde al subtotal.</p>');
                                 $("#alert-ok").modal();
                                 $(descuento).val("");
                                 $(descuento).val(0.0);
                            }
                        }// IVA == 0
                        else{

                            if(parseFloat(cantidad_valores*precio_valores)>$(descuento).val()){
                                subtotal0_monto = subtotal0_monto + parseFloat(cantidad_valores*precio_valores);
                                subtotales_descuento_producto_cero = subtotales_descuento_producto_cero
                                    + parseFloat(cantidad_valores*precio_valores)
                                    - $(descuento).val();
                                totales_desc_cero = totales_desc_cero + parseFloat($(descuento).val());
                                $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - $(descuento).val() );
                                $("#subtotal").text("$"+subtotal12_monto.toFixed(2));
                                $("#subtotal_cero").text("$"+subtotal0_monto.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero; // falta valores
                                $("#total_pagar").text("$"+x.toFixed(2));

                            }else{

                                 $("#tit-alert-ok").html("Alerta");
                                 $("#body-alert-ok").html('<p>Por Favor Ingrese un monto de descuento válido, acorde al subtotal.</p>');
                                 $("#alert-ok").modal();
                                 $(descuento).val("");
                                 $(descuento).val(0.0);
                            }
                        }
                        $("#subtotal").text("$"+subtotal12_monto.toFixed(2));
                        $("#subtotal_cero").text("$"+subtotal0_monto.toFixed(2));
                        $("#total_subtotal").text("$"+parseFloat((subtotal12_monto) + (subtotal0_monto)).toFixed(2));
                        $("#ice").text("$"+totales_ice.toFixed(2));
                        zdesc = totales_desc + totales_desc_cero;
                        $("#descuento").text("$"+zdesc.toFixed(2));
                        $("#iva").text("$"+iva_desc.toFixed(2));
                        x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                        $("#total_pagar").text("$"+x.toFixed(2));

                    }
                }
            }
        }
    });
}

/**************************
* Función para calcular el Subtotal  Desc. por venta
**************************/
function DescuetosPorVentaStock(){
   var cantidad_porcentaje = 0.0;
   var precio_u_porcentaje = 0.0;
   var proc_iva = 0.0;
   var proc_ice = 0.0;
   var descuento_combo = $("#id_descuento_porc").val();
   var tipo_combo_descuento = $("#id_tipo_descuento").val();
   var monto_descuento = $("#id_monto_descuento").val();
   var monto_descuento_doce = $("#id_monto_descuento_doce").val();
   var monto_descuento_cero = $("#id_monto_descuento_cero").val();
   var subtotales_iva_porcentaje = 0.0;
   var iva_total_procentaje = 0.0;
   var ice_total_porcentaje = 0.0;
   var x = 0.0;
   var yz = 0.0;
   var subtotales_iva_porcentaje_cero = 0.0;
   var cantidad_porcentaje_cero = 0.0;
   var precio_u_porcentaje_cero = 0.0;
   var result_descuento_porc = 0.0;
   var result_iva_porc = 0.0;
   var tmp = 0.0;
   var z = 0.0;
   var result_descuento_porc_iva_cero = 0.0;
   var tmp_iva_cero = 0.0;
   var zdesc = 0.0;
   /***************************************************************/
   var tarifa12_monto = 0.0;
   var tarifa0_monto = 0.0;
   var tarifa12_monto_iva = 0.0;
   var descuento12_monto = 0.0;
   var descuento0_monto = 0.0;
   var total_descuento_monto_final = 0.0;
   var cont1 = 0;
   var tam = 0;
   var result_total_pagar = 0.0;
   var bandera_result=0;
   var bandera_result2=0;
   tam = parseInt($(ID_TABLA_VENTA_STOCK+" tbody tr").length);

    $(ID_TABLA_VENTA_STOCK+" tbody tr").each(function(index)
    {
        var cantidad = $(this).find(".cantidad_stock");
        var precio = $(this).find(".precio_u_stock");
        var iva = $(this).find(".iva_stock");
        var ice = $(this).find(".ice_stock");
        var subtotal = $(this).find(".subtotal_stock");
        cont1++;

        if(tipo_combo_descuento==1 && $(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && descuento_combo !=""){
            if ($(iva).val()!=0 ){
                cantidad_porcentaje = parseFloat($(cantidad).val());
                precio_u_porcentaje = parseFloat($(precio).val());
                proc_iva = parseFloat($(iva).val());
                if ($(ice).val()==""){
                     proc_ice = 0.0;
                }else{
                     proc_ice = parseFloat($(ice).val());
                }
                subtotales_iva_porcentaje= subtotales_iva_porcentaje + parseFloat(cantidad_porcentaje*precio_u_porcentaje);
                iva_total_procentaje =  (subtotales_iva_porcentaje)*(proc_iva/100);
                ice_total_porcentaje = ice_total_porcentaje + parseFloat((cantidad_porcentaje*precio_u_porcentaje)*(proc_ice/100));
                $(subtotal).val(parseFloat(cantidad_porcentaje*precio_u_porcentaje));
                $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                $("#descuento").text("$"+0.0);
                $("#iva").text("$"+iva_total_procentaje.toFixed(2));
                x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                $("#total_pagar").text("$"+x.toFixed(2));

            }else{
                 cantidad_porcentaje_cero = parseFloat($(cantidad).val());
                 precio_u_porcentaje_cero = parseFloat($(precio).val());
                 subtotales_iva_porcentaje_cero = subtotales_iva_porcentaje_cero + parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero);
                 $(subtotal).val(parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero));
                 $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                 $("#descuento").text("$"+0.0);
                 x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                 $("#total_pagar").text("$"+x.toFixed(2));
                }
            yz = subtotales_iva_porcentaje+ subtotales_iva_porcentaje_cero;
            $("#total_subtotal").text("$"+yz.toFixed(2));
        }

        if(tipo_combo_descuento==1 && $(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && descuento_combo !="")
        {

            if(descuento_combo==1){//Porcentaje
                if ( $("#id_monto_descuento").val()==""){
                    $("#id_monto_descuento").val(0.0);
                }

                if($(iva).val()!=0){
                    result_descuento_porc = (subtotales_iva_porcentaje)*(monto_descuento/100);
                    tmp = subtotales_iva_porcentaje - result_descuento_porc;
                    result_iva_porc = (subtotales_iva_porcentaje - result_descuento_porc)*(proc_iva/100);
                    $("#descuento").text("$"+result_descuento_porc.toFixed(2));
                    $("#iva").text("$"+result_iva_porc.toFixed(2));
                    x = tmp  + ice_total_porcentaje + result_iva_porc + result_descuento_porc_iva_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));

                }else{
                    //iva=0
                    result_descuento_porc_iva_cero = (subtotales_iva_porcentaje_cero)*(monto_descuento/100);
                    tmp_iva_cero = subtotales_iva_porcentaje_cero - result_descuento_porc_iva_cero;
                    $("#descuento").text("$"+result_descuento_porc_iva_cero.toFixed(2));
                    x = tmp_iva_cero  + ice_total_porcentaje + result_iva_porc + result_descuento_porc_iva_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));
                }


                $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                $("#total_subtotal").text("$"+parseFloat((subtotales_iva_porcentaje) + (subtotales_iva_porcentaje_cero)).toFixed(2));
                $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                zdesc = result_descuento_porc + result_descuento_porc_iva_cero;

                $("#descuento").text("$"+zdesc.toFixed(2));
                $("#iva").text("$"+result_iva_porc.toFixed(2));
                z = tmp + tmp_iva_cero;
                x = z + ice_total_porcentaje + result_iva_porc ;
                $("#total_pagar").text("$"+x.toFixed(2));
            }

            if(descuento_combo==2){//Monto
                if (monto_descuento_doce == ""  && monto_descuento_cero==""){
                    $("#id_monto_descuento_doce").val(0.0);
                    $("#id_monto_descuento_cero").val(0.0);
                }

                if ($(iva).val()!=0){
                    tarifa12_monto = tarifa12_monto + parseFloat($(cantidad).val()*$(precio).val());
                    descuento12_monto = tarifa12_monto - $("#id_monto_descuento_doce").val();
                    tarifa12_monto_iva = (descuento12_monto)*($(iva).val()/100);
                }else{
                     tarifa0_monto = tarifa0_monto + parseFloat($(cantidad).val()*$(precio).val());
                     descuento0_monto = tarifa0_monto - $("#id_monto_descuento_cero").val();
                }

                 if(tarifa12_monto < $("#id_monto_descuento_doce").val() && cont1==tam){
                    $("#tit-alert-ok").html("Alerta");
                    $("#body-alert-ok").html('<p>Por Favor ingrese un monto de descuento válido según el subtotal_12%.</p>');
                    $("#alert-ok").modal();

                    $("#id_monto_descuento_doce").val("");
                    $("#id_monto_descuento_doce").addClass("campo_requerido");
                    $("#id_monto_descuento_doce").val(0.0);
                    bandera_result++;
                 }
                 if(tarifa0_monto < $("#id_monto_descuento_cero").val()&& cont1==tam){
                    $("#tit-alert-ok").html("Alerta");
                    $("#body-alert-ok").html('<p>Por Favor ingrese un monto de descuento válido según el subtotal_0%.</p>');
                    $("#alert-ok").modal();

                     $("#id_monto_descuento_cero").val("");
                     $("#id_monto_descuento_cero").addClass("campo_requerido");
                     $("#id_monto_descuento_cero").val(0.0);
                     bandera_result2++;
                 }
                 if(bandera_result==0 && bandera_result2==0){
                     $("#subtotal").text("$"+tarifa12_monto.toFixed(2));
                     $("#subtotal_cero").text("$"+tarifa0_monto.toFixed(2));
                     $("#total_subtotal").text("$"+parseFloat((tarifa12_monto) + (tarifa0_monto)).toFixed(2));
                     total_descuento_monto_final = parseFloat($("#id_monto_descuento_cero").val()) + parseFloat($("#id_monto_descuento_doce").val());
                     $("#descuento").text("$"+total_descuento_monto_final.toFixed(2));
                     $("#iva").text("$"+tarifa12_monto_iva.toFixed(2));
                     $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                     result_total_pagar = ice_total_porcentaje + descuento12_monto + tarifa12_monto_iva + descuento0_monto;
                     $("#total_pagar").text("$"+result_total_pagar.toFixed(2));
                 }
            }
        }

    });
}

function AgregarDetalleVentaStock(e){
    e.preventDefault();
    var clonar_tr = $(ID_TABLA_VENTA_STOCK+" tbody tr:first").clone();
    clonar_tr.find(".bootstrap-select").remove();

    clonar_tr.find(".eliminar_stock").click(function(e){
        EliminarFila(e,this,ID_TABLA_VENTA_STOCK);
    });

    $(clonar_tr.find(".cantidad_stock")).addClass("numerico").val("");
    $(clonar_tr.find(".precio_u_stock")).val("");
    $(clonar_tr.find(".unidad_stock")).val("");

    $(clonar_tr.find(".item_stock")).attr("disabled", false);

    $(clonar_tr).find("select[data-name='combo_centro_costo']").prop('selectedIndex', 0);
    $(clonar_tr.find(".selectpicker")).change(function(){
        AgregarTitulo(this);
    });

    $(clonar_tr.find(".iva_stock")).val("");
    $(clonar_tr.find(".ice_stock")).val("");
    $(clonar_tr.find(".subtotal_stock")).val("");
    $(clonar_tr.find(".descuento")).val("");
    $(clonar_tr.find(".item_stock")).val("");


    $(clonar_tr.find(".item_stock")).change(function(e){
       SelectITemParametros(this);
       ExisteItemBodegaStock(this, URLExisteItemBodega);
       ValidateItemsRepetidosStock(this, ID_TABLA_VENTA_STOCK);



    });

    $(clonar_tr.find(".selectpicker")).change(function(){
        AgregarTitulo(this);
    });
    $(clonar_tr.find(".numerico")).each(function(){
        $(this).keydown(function(event){
            Numerico(event);
        }).change(function(event){
                redondear(this);
        });
     });

    $(clonar_tr.find(".cantidad_stock")).change(function(e){
         var tr = $(this).parent().parent();
         var item = $(tr).find(".item_stock");
         var subtotal = $(tr).find(".subtotal_stock");

        $(this).removeClass("campo_requerido");
        $(subtotal).removeClass("campo_requerido");

        $(ID_TABLA_VENTA_STOCK).find(".descuento").each(function(){
             if ($(this).val()==""){
                 $(this).val(0.0);
             }
        });

        if($(item).val() != ""){
            agregar_punto_VENTA(this);
            cantidadBodegaVentaStock(this, url_get_cantidad_disponible_stock);
        }
        else{
              $("#tit-alert-ok").html("Alerta");
              $("#body-alert-ok").html("Seleccione un item, para realizar la venta.");
              $(this).val("");
              $("#alert-ok").modal();
        }

        if (parseFloat($(this).val()) == 0.0){
              $("#tit-alert-ok").html("Alerta");
              $("#body-alert-ok").html("Ingrese una cantidad correcta, para realizar la venta.");
              $(this).val("");
              $("#alert-ok").modal();
        }

        calcularSubtotalesStock();
        iteraccionCombosDescuento(ID_TABLA_VENTA_STOCK);
        DescuetosPorVentaStock();
    });

    $(clonar_tr.find(".iva_stock")).focusout(function(e){
        calcularSubtotalesStock();
        iteraccionCombosDescuento(ID_TABLA_VENTA_STOCK);
        DescuetosPorVentaStock();
    });

    $(clonar_tr.find(".precio_u_stock")).change(function(e){
         calcularSubtotalesStock();
         iteraccionCombosDescuento(ID_TABLA_VENTA_STOCK);
         DescuetosPorVentaStock();
         agregar_punto_VENTA(this);
    });

    $(clonar_tr.find(".descuento")).change(function(e){
        if ($(this).val()==""){
            $(this).val(0.0);
        }
          calcularSubtotalesStock();
          DescuetosPorVentaStock();
    });

    // Refresca los combos selectpicker
    $(clonar_tr).find(".selectpicker").selectpicker("refresh");

    $(ID_TABLA_VENTA_STOCK+" tbody").append(clonar_tr);
    recalcular_ids_Venta(ID_TABLA_VENTA_STOCK);

    $(clonar_tr).find(':input').keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
    });

}

