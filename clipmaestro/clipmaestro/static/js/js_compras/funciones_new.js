/**
 * Created by Clip Maestro on 05/06/2015.
 */
//Variables globales
var border, outline;
var FECHA_ASIENTO = "#id_fecha_asiento";
var FECHA_EMI = "#id_fecha";
var FECHA_RET = "#id_fecha_emision_retencion";
var SERIE_RET = "#id_serie_ret";
var AUT_RET = "#id_autorizacion_retencion";
var T_REEMBOLSO = "#total_reemb_b";
var TR_BASE0 = "#trbase0";
var TR_BASE12 = "#trbase12";
var S_FORMA_PAGO = "#id_forma_pago";
var S_PAIS = "#id_pais";
var NUM_DOC = "#id_serie";
var NUM_DOC_MODIF = "#id_serie_modif";
var NUM_RET = "#id_numero_retencion";
var S_SUST_TRIB = "#id_sust_tribut";
var S_TIPO_DOC = "#id_tipo_de_doc";
var T_DETALLE_COMP = "#tabla_formset";
var T_DETALLE_REEM = "#tabla_formset_reembolso";
var S_COD_RET_FTE = "select[data-name='combo_cod_ret_fte']";
var S_COD_RET_IVA = "select[data-name='combo_cod_ret_iva']";
var S_COMBO_CUENTA = "select[data-name='combo_cuenta']";
var NUM_RET_ANT = "#num_ret_ant"; // label que contiene el numero anterior de la retención

var MENSAJE_COD_SRI = "Se cambiarán la lista de los CÓDIGOS del SRI, por favor si ya los tenía llenos " +
                      "vuelva a llenarlos, ya que si al cambiar la fecha del asiento se encuentra en la fecha " +
                      "de otra nómina de CÓDIGOS, el sistema los cambiará automáticamente";


function toFixed ( number, precision ) {
    var multiplier = Math.pow(10, precision + 1),
        wholeNumber = Math.floor(number * multiplier);
    return Math.round(wholeNumber / 10) * 10 / multiplier;
}

/********************
 * Funcion para cargar los cambos de iva, ret_fte, ret_iva
 */

function getComboInpuestos(sfecha_asiento, select_combo_iva, url, tipo_ret)
{
    var contador = 0;  // Para saber si se muestra el mensaje de alerta de los nuevos códigos sri

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'fecha_asiento': sfecha_asiento,
            'fecha_anterior': fecha_asiento_ant
        },
        error: function(){
            return true;
        },
        success: function(datajson){
            if(datajson.status == 1)
            {
                switch (tipo_ret)
                {
                    case 0:
                    {
                        var data = datajson.lista;
                        contador ++;
                        $(select_combo_iva).empty();
                        for(var i=0; i<data.length; i++)
                        {
                            $(select_combo_iva).append('<option value="' + data[i].id + '" data-codigo="' + data[i].codigo +'" data-porcentaje="' + data[i].porcentaje + '" >' + data[i].descripcion + '</option>');
                        }
                        $(select_combo_iva).selectpicker("refresh");
                        break;
                    }
                    case 1:  // Retencion Fte.
                    {
                        $(".ret_fte").val(datajson.respuesta.name);
                        $(".ret_fte").attr("value", datajson.respuesta.id);
                        $('.ret_fte[type="hidden"]').val(datajson.respuesta.id);
                        $(".porc_ret_fte").val(datajson.respuesta.porcentaje);

                        contador ++;
                        break;
                    }
                    case 2:  // Retención Iva
                    {
                        $(".ret_iva").val("");
                        $(".porc_ret_iva").val("");
                        contador ++;
                        break;
                    }
                }
                CalcularRetencion();
            }
        }
    });
    return contador;
}
function getComboSerieRet(){
    // Cargar el bloc de las retenciones
    var select_serie = $("#id_serie_ret");
    var cont = 0;
    var id_block_sel = select_serie.val();
    if($("#id_fecha_emision_retencion").val() != "")
    {
        $.ajax({
            url: "/compras/get_datos_retencion_serie/",
            type: 'POST',
            async: false,
            cache: false,
            timeout: 3000,
            data: {
                'fecha_emi_ret': $("#id_fecha_emision_retencion").val()
            },
            error: function(){
                return false;
            },
            success: function(data){
                if(data[0].id == 0)
                {
                    cont ++;
                }
                else
                {
                    select_serie.empty();

                    if (data.length > 1)
                    {
                        select_serie.append('<option value=""></option>');
                    }
                    for(var i=0; i<data.length; i++)
                    {
                        select_serie.append('<option value="' + data[i].id + '">' + data[i].descripcion + '</option>');
                    }
                }
            }
        });
        if (id_block_sel)
        {
            select_serie.val(id_block_sel);
        }
        select_serie.selectpicker("refresh");
    }
    return cont == 0;
}
function getDatosProveedor(id_prov, tipo_doc)
{
    $.ajax(
    {
        url: "/compras/get_datos_proveedor/",
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'id_prov': id_prov,
            'id_tipo_doc': tipo_doc
        },
        error: function(){
            return true;
        },
        success: function(data){
            if (data.status == 1)
            {
                var autorizacion = $("#id_autorizacion");
                var vencimiento = $("#id_vence");

                if (autorizacion.val()=="")
                    autorizacion.val(data.autorizacion);
                if (vencimiento.val())
                    vencimiento.val(data.vencimiento);
            }
        }
    });
}

function CambioFechaAsiento()
{
    var dia_fecha_asiento = new Date(parse_date($(FECHA_ASIENTO).val()));
    var dia_fecha_emision = new Date(parse_date($(FECHA_EMI).val()));
    var dia_fecha_retencion = new Date(parse_date($(FECHA_RET).val()));
    var contador = 0;

    $(T_DETALLE_REEM+" tbody").find("tr").each(function(){
        var dia_fecha_reembolso = new Date(parse_date($(this).find(".fecha_emision").val()));
        if(dia_fecha_asiento < dia_fecha_reembolso){
            $(this).find(".fecha_emision").val($(FECHA_ASIENTO).val());
        }
    });

    /*************************
     * Codigo para cambiar combo retencion, codigoiva y retencion iva
     */

    var select_combo_iva = $(T_DETALLE_COMP).find("select[data-name='combo_iva']");
    contador += getComboInpuestos($(FECHA_ASIENTO).val(), select_combo_iva, "/compras/getcomboiva/", 0);
    contador += getComboInpuestos($(FECHA_ASIENTO).val(), select_combo_iva, "/compras/get_combo_ret_fte/", 1);
    contador += getComboInpuestos($(FECHA_ASIENTO).val(), select_combo_iva, "/compras/get_combo_ret_iva/", 2);

    if( contador > 0){
        mensaje_alerta(MENSAJE_COD_SRI);  // Se muestra el mensaje de codigos de SRI
    }

    if(dia_fecha_asiento < dia_fecha_emision){
        $(FECHA_EMI).val($(FECHA_ASIENTO).val());
    }
    if(dia_fecha_asiento< dia_fecha_retencion){
        $(FECHA_RET).val($(FECHA_ASIENTO).val());
    }
}

/*******************************
 * función para cargar la autorización
 * de la retención
 * @constructor
 */
function CargarAutorizacion()
{
    var serie = $(SERIE_RET).attr("value");
    $.post('/compras/buscar_autorizacion/',
        {
            'serie': serie
        },
        function(data)
        {
            if (data.status == 1)
            {
                $(AUT_RET).val(data.autorizacion);
                $(NUM_RET_ANT).text(data.num_ret);
            }
    });
}


/**************************************
 * Funcion para validar que la fecha de emision
 *    no supere a la fecha del asiento
 *     contable
 * @param obj
 * @returns {null}
 */
function validar_fecha_em(obj)
{
    var dia_fecha_asiento = new Date(parse_date($(FECHA_ASIENTO).val()));
    var fecha_emi = new Date(parse_date($(obj).val()));
    var fecha_emi_ret = new Date(parse_date($(FECHA_RET).val()));
    var fecha_5_dias = new Date(fecha_emi.getTime());

    if(dia_fecha_asiento< fecha_emi){
        $(obj).val($(FECHA_ASIENTO).val());
        mensaje_alerta('No puede ingresar una fecha superior a la fecha del asiento');
    }
    sumar_dias(fecha_5_dias, 5);

    if (fecha_emi_ret > fecha_5_dias)
        $(FECHA_RET).val($(obj).val());
    return null;
}


/************************
 * Valida la fecha del documento con
 * la actual en la retención
 * @param obj
 * @returns {null}
 */
function validar_fecha_ret(obj)
{
    var dia_fecha_retencion = new Date(parse_date($(FECHA_RET).val()));
    var dia_fecha = new Date(parseInt(parse_date($(obj).val())));

    if(dia_fecha_retencion < dia_fecha){
        $(FECHA_RET).val($(obj).val())
    }
    return null;
}


/**************************************
 * Funcion para validar que la fecha
 *    no supere a la fecha del asiento
 *     contable
 * @param obj
 * @returns {null}
 */
function validar_fecha_em_ret(obj)
{
    var dia_fecha_emision = new Date(parse_date($(FECHA_EMI).val()));
    var dia_fecha_asiento = new Date(parse_date($(FECHA_ASIENTO).val()));
    var dia_fecha = new Date(parse_date($(obj).val()));
    var fecha_5_dias = new Date(dia_fecha_emision.getTime());

    if(dia_fecha_emision > dia_fecha){
        $(obj).val($(FECHA_EMI).val());
        mensaje_alerta('No puede ingresar una fecha inferior a la fecha de la emisión del documento ' +
            'la cual es: '+$(FECHA_EMI).val());
    }

    if(dia_fecha_asiento < dia_fecha){
        $(obj).val($(FECHA_EMI).val());
        mensaje_alerta('No puede ingresar una fecha superior a la fecha del ' +
            'asiento la cual es: '+$(FECHA_ASIENTO).val());
    }

    // Validacion que no puede ingresar una retencion mayor a 5 dias del documento
    sumar_dias(fecha_5_dias, 5);
    if(dia_fecha > fecha_5_dias){
        $(obj).val($(FECHA_EMI).val());
        mensaje_alerta('No puede ingresar una fecha superior a 5 dias de fecha de la emisión del documento ' +
            'la cual es: '+$(FECHA_EMI).val());
    }

    return null;
}


/*************************************
 * Habilita o deshabilita el porcentaje
 * de retención del iva dependiendo si es
 * bien o servicio
 * @param combo_bs
 * @returns {null}
 */
function cambio_ret_iva(combo_bs)
{
    var opt = $(combo_bs).val();
    var tr = $(combo_bs).parent().parent().get(0);
    var select_ret_iva = $(tr).find(S_COD_RET_IVA);

    if(opt == 1)
    {
        $(select_ret_iva).find("option").each(function(){
            if(parseFloat($(this).attr("data-porcentaje")) == 70)
                $(this).attr("disabled", true);
            if(parseFloat($(this).attr("data-porcentaje")) == 30)
                $(this).attr("disabled",false);
        });
    }
    else
    {
        $(select_ret_iva).find("option").each(function(){
            if(parseFloat($(this).attr("data-porcentaje")) == 30)
                $(this).attr("disabled", true);
            if(parseFloat($(this).attr("data-porcentaje")) == 70)
                $(this).attr("disabled",false);
        });
    }
    $(select_ret_iva).selectpicker("refresh");
    return null;
}


/*********************************
 * Función para calcular el total
 * de los reembolsos
 */
function calcular_totales_reembolso()
{
    var tbase0 = 0.0;
    var tbase12 = 0.0;
    var tbase0cta = 0.0;
    var tbase12cta = 0.0;
    var esta_cuadrado = false;
    $(T_DETALLE_REEM+" tbody input").each(function(){
        if($(this).hasClass("base_cero"))
        {
            if($(this).val()!=""){
                tbase0 = tbase0 + parseFloat($(this).val());
            }
        }
        if($(this).hasClass("base_iva"))
        {
            if($(this).val()!=""){
                tbase12 = tbase12 + parseFloat($(this).val());
            }
        }
    });
    $(T_DETALLE_COMP+" tbody tr").each(function(){
        var combo_iva = $(this).find("select[data-name='combo_iva']");
        var base = $(combo_iva).find("option:selected").attr("data-porcentaje");
        if(parseFloat(base) == 0)
        {
            tbase0cta = tbase0cta + parseFloat($(this).find(".valor").val());
        }
        else
        {
            tbase12cta = tbase12cta + parseFloat($(this).find(".valor").val());
        }
    });
    $(TR_BASE0).text(tbase0);
    $(TR_BASE12).text(tbase12);
    if((tbase0==tbase0cta && tbase12==tbase12cta) && (tbase0>0 || tbase12>0))
    {
        $(T_REEMBOLSO).removeClass("rembolso-error");
        $(T_REEMBOLSO).addClass("rembolso-ok");
        $(T_REEMBOLSO).find("span").removeClass("glyphicon glyphicon-remove");
        $(T_REEMBOLSO).find("span").addClass("glyphicon glyphicon-ok");
        esta_cuadrado = true;
    }
    else
    {
        $(T_REEMBOLSO).removeClass("rembolso-ok");
        $(T_REEMBOLSO).addClass("rembolso-error");
        $(T_REEMBOLSO).find("span").removeClass("glyphicon glyphicon-ok");
        $(T_REEMBOLSO).find("span").addClass("glyphicon glyphicon-remove");
        esta_cuadrado = false;
    }
    return esta_cuadrado;
}


/***************************************
 * Función para habilitar los divs para
 * que ingrese info del pago
 * @param obj
 * @constructor
 */
function PagoExterior(obj)
{
    var local_ext = parseInt($(obj).val());
    var boton_pais = $("button[data-id='id_pais']");
    var pais_select = $(S_PAIS);

    if(compra_exterior(url_compra_exterior, local_ext)){
        $(".pago_exterior").attr("style","display: table-row");
        if($(pais_select).val()=="")
        {
            $(boton_pais).addClass("campo_requerido");
            $(S_PAIS).attr("disabled", false);
            $(S_PAIS).selectpicker('refresh');
        }
    }
    else{
        $(".pago_exterior").attr("style","display: none");
        $(boton_pais).removeClass("campo_requerido");
        $(S_PAIS).prop('selectedIndex', 0);
        $(S_PAIS).selectpicker('refresh');
    }
}


/**************
 * function ContadoCredito(obj)
 * Sirve para habilitar la caja de
 * plazo en dias
 * @param obj
 * @constructor
 */
function ContadoCredito(obj)
{
    var selected = parseInt($(obj).val());
    var tr_plazo = $("#tr_plazo");
    if(es_pago_credito(url_es_pago_credito, selected)){
        tr_plazo.show();
    }
    else{
        tr_plazo.hide();
        $("#dia_vencimiento").html("");
    }
}

/***************************************
 * Funcion que habilita o deshabilita los campos de
 * retención del iva dependiendo de la "bandera" si es
 * true lo habilita caso contrario la deshabilita
 * @param bandera
 */
function habilitar_deshabilitar_campo_ret(bandera){
    var porc_ret_iva = $(".porc_ret_iva");
    var ret_iva = $(".ret_iva");
    if (bandera){
        $(porc_ret_iva).removeAttr("disabled");
        $(ret_iva).removeAttr("disabled");

    }
    else{
        $(porc_ret_iva).val("");
        $(porc_ret_iva).attr("disabled", true);
        $(porc_ret_iva).attr("value", "");

        $(ret_iva).val("");
        $(ret_iva).attr("disabled", true);
        $(ret_iva).attr("value", "");
    }
}

/*************************************************************
* Funcion para habilitar el tipo de Nota de venta o nota de debito o si es reembolso
* N/C cod 04
* N/D cod 05
* Reembolso 41
*/
function CambioDocumento(obj)
{
    if(es_reembolso_gasto(url_es_reebolso, $(obj).val()))
    {
        habilitar_deshabilitar_campo_ret(false);
        $("#detalle_reembolso").attr("style","display:block");
        $("#documento_modifica").attr("style","display: none");
    }
    else
    {
        if(tiene_doc_modifica(url_tiene_doc_mod, $(obj).val()))
        {
            $("#documento_modifica").attr("style","display: block");
            $("#detalle_reembolso").attr("style","display: none");
            habilitar_deshabilitar_campo_ret(false);
        }
        else
        {
            $("#documento_modifica").attr("style","display: none");
            $("#detalle_reembolso").attr("style","display: none");
            habilitar_deshabilitar_campo_ret(true);
        }
    }
    CalcularRetencion();
}


/*******************************
 * Habilita o deshabilita
 * el combo ret_iva si es que
 * es sujeto a iva esa compra
 * @param obj
 */
function habilitar_bs_iva(obj)
{
    var combo_iva_padre = $(obj).parent().parent().get(0);
    var combo_iva = $(combo_iva_padre).find("select[data-name='combo_iva']");
    var combo_ret_iva = $(combo_iva_padre).find(".ret_iva");
    var input_ret_iva = $(combo_iva_padre).find(".porc_ret_iva");
    var iva_porc = parseFloat($(combo_iva.find("option:selected")).attr("data-porcentaje"));

    if(iva_porc == 0.0)
    {
        $(combo_ret_iva).val("");
        $(combo_ret_iva).attr("disabled", true);
        $(input_ret_iva).val("");
        $(input_ret_iva).attr("disabled", true);
    }
    else
    {
        $(combo_ret_iva).removeAttr("disabled");
        $(input_ret_iva).removeAttr("disabled");
    }

}


/*******************************
 * Verifica si el total de la compra
 * es mayor o igual a $ 1000.00
 * para presentar la forma de pago
 * @constructor empty
 */
function VerificarFormaPago()
{
    var contador = 0.0;
    $(T_DETALLE_COMP).find(".valor").each(function(){
        var val = $(this).val();
        if(val != ""){
            contador = contador + parseFloat(val);
        }
    });
    if (contador >= 1000){
        $("#forma_pago").attr("style","display: block");
    }
    else{
        $("#forma_pago").attr("style","display: none");
    }
}

/********************************
 * obtiene los elementos inputs en formato { name: value }
 * de un elemento padre ej: un form o un simple div
 * @param parent
 * @returns {Array}
 */
function get_input_data_serialize(parent)
{
    var data = {};

    $(parent).find(":input").each(function(){
        var name = $(this).attr("name");
        data[name] = $(this).val();
    });

    return data;

}

/****************************
 * Función para calcular los totales
 * de la compra
 * @returns {boolean}
 * @constructor
 */

function CalcularTotales()
{

    var total_retencion = 0;
    var mostrar = 0;
    $.ajax(
    {
        url: url_calcular_totales,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: $('#formulario').serialize(),

        error: function(){
            return true;
        },
        success: function(data)
        {
            $("#total_iva_0").text(parseFloat(data.compra_0).toFixed(2));
            $("#total_iva_12").text(parseFloat(data.compra_iva).toFixed(2));
            $("#total_iva").text(parseFloat(data.total_iva).toFixed(2));
            $("#total_retencion_det").text(parseFloat(data.total_retenciones).toFixed(2));
            $("#total_pagar").text(parseFloat(data.total_pagar).toFixed(2));
            total_retencion = parseFloat(data.total_retenciones);
            mostrar = parseFloat(data.mostrar);
    }});
    return (mostrar > 0)
}


/************************
 * Agrega un tooltip a los
 * combos para que se pueda
 * observar all su contenido
 * @param e
 * @constructor e
 */
function AgregarTitulo(e)
{
    /*
    var padre = $(e).parent().get(0);
    var boton_combo = $(padre).children()[1];
    var titulo = "";
    var div_texto = boton_combo.firstChild.firstChild;
    var codigo = "";
    $(e).find("option").each(function(){
      if(this.selected)
      {
          titulo = $(this).text();
      }
    });
    $(boton_combo).tooltip('hide')
    .attr('data-original-title', titulo)
    .tooltip('fixTitle');*/
}


/*************************************
 * Funcion para recalcular los ids
 */
function recalcular_ids(tableid)
{
    var nombre_form = "";
    $(tableid+" tbody tr").each(function(index)
    {
        var cont = index;
        $(this).find(":input:not(:button, .input-block-level)").each(function()
        {
            var name_campo = $(this).attr("name").split("-");
            nombre_form = name_campo[0];
            var nombre_campo = name_campo[0]+"-" + cont + "-"+name_campo[2];
            $(this).attr("name", nombre_campo);
            $(this).attr("id", "id_"+nombre_campo);
        });
    });
    $('#id_'+nombre_form+'-TOTAL_FORMS').val($(tableid+" tbody").children().length);
}


/***************************************
 * Funcion para mostar la fecha de vencimiento
 * del documento
 */

function MostrarFechaVenci(e)
{
    var select_cont_credito = $("#id_forma_pago");
    if(es_pago_credito(url_es_pago_credito, select_cont_credito.val()))
    {
        if($(e).val()!="")
        {
            var aFecha = $("#id_fecha").val().split("-");
            var Fecha = new Date(parseInt(aFecha[0]), parseInt(aFecha[1]-1), parseInt(aFecha[2]));

            // Sacamos los milisegundos, le sumamos los dias
            // y lo pones de nuevo en la fecha
            var fFecha = Fecha.getTime();
            var dias = parseInt($(e).val());
            fFecha = fFecha+(1000*60*60*24*dias);

            Fecha.setTime(fFecha);

            $("#dia_vencimiento").text("El crédito vence el "+Fecha.getDate() + " de "+
                get_mes(Fecha.getMonth())+ " del " + Fecha.getFullYear());
        }
        else
        {
            $("#dia_vencimiento").html("&nbsp;");
        }
    }
}


/********************************************
 * Funcion para verificar que no digiten
 * documentos repetidos en el reembolso
 */
function verificar_docs_rep(){
    //preguntar si array.lenght >1 si no false
    var array = [];
    var prov = [];
    var serie = [];
    var tipo_doc = [];
    var acum = 0;
    var cont = 0;
    array.push(prov);
    array.push(serie);
    array.push(tipo_doc);

    $(T_DETALLE_REEM+" tbody").find("tr").each(function()
    {
        array[0][cont] = $(this).find(".proveedor").val();
        array[1][cont] = $(this).find(".serie").val();
        array[2][cont] = $(this).find(".tipo_documento").val();
        cont++;
    });
    if(array.length>1)
    {
        for (var i=0; i<cont; i++){
            for(var j=i+1; j<cont; j++){
                /* Si el numero de documento es igual y el proveedor es el mismo
                   y si es el mismo número de documento, entonces error esta ingresando dos veces el
                   mismo documento
                 */
                if(array[1][i] != "" && array[1][j] != "" && array[1][i] == array[1][j] && array[0][i] == array[0][j] && array[2][i] == array[2][j]){
                    acum++;
                }
            }
        }
        if(acum>0){
            return true;
        }
        else{
            return false;
        }
    }
    return false;
}


/*********************
 * Valida el ruc de los proveedores
 * ingresados en el reembolso de gastos
 * @params obj, tipo_id
 * @returns {boolean}
 */
function validar_ruc(obj, tipo_id){
    var ruc = $(obj);
    if ($(ruc).val() != "" && tipo_id !="")
    {
        if ( tipo_id == "R" )
        {
            if (isRUC($(ruc)) && $(ruc).val().length==13)
            {
                $(obj).removeClass("campo_requerido");
                return true;
            }
            else{
                $(obj).addClass("campo_requerido");
                return false;
            }
        }
        if( tipo_id == "C" )
        {
            if(isCedula(ruc) && $(ruc).val().length==10){
               $(obj).removeClass("campo_requerido");
                return true;
            }else{
                $(obj).addClass("campo_requerido");
                return false;
            }
        }
        if( tipo_id == "P" ){
            return true;
        }
    }
    else{
        return false;
    }
}


function AgregarProveedor(e){
    var id_prov = $("#id_num_identificacion");
    var tipo_id = $("#id_tipo_identificacion").val();

    if(validar_ruc(id_prov, tipo_id))
    {
        var datastring = $("#form_prov").serialize();
        var url = $("#form_prov").attr("action");
        $.ajax(
        {
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: datastring,
            error: function(){
                return true;
            },
            success: function(data){
                var mensajes = new Array();
                if (data.status == 1){
                    $('#div-proveedor').modal('hide');
                    mensaje_alerta("Se ha registrado exitosamente el proveedor");
                }
                else
                {
                    var stringhtml = "";
                    for(var i=0; i<data.mensaje.length; i++)
                    {
                        mensajes.push(data.mensaje[i]);
                    }
                    for(i=0; i<mensajes.length; i++)
                    {
                        stringhtml = (stringhtml+"<p>"+mensajes[i]+"</p>")
                    }
                    $("#error-proveedor").html(stringhtml);
                    $(".pop-error").fadeIn("slow");
                }
            }
        });
    }
    else
    {
        e.preventDefault();
        $("#error-proveedor").html("Número de Identificación no válido, por favor verrifique");
        $(".pop-error").fadeIn("slow");
    }
}


function EliminarFila(e, fila, tableid)
{
    e.preventDefault();
    if($(tableid+" tbody").children().length>1){
        $(fila).parent().parent().remove();
        recalcular_ids(tableid);
    }
    else{
        mensaje_alerta('No se puede eliminar');
    }
}


function AgregarFilasDetalleCompras(e)
{
    e.preventDefault();
    // var clonar_tr = $(T_DETALLE_COMP+" tbody tr:first").clone();
    var clonar_tr = undefined;
    $.ajax(
    {
        url: url_add_fila_det_comp,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: '',
        error: function(){
            return true;
        },
        success: function(data){
            clonar_tr = $(data);
        }
    });

    clonar_tr.find(".bootstrap-select").remove();
    clonar_tr.find(".check_sust_div").show();


    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, T_DETALLE_COMP);
        CalcularRetencion();
    }).show();

    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });

    $(clonar_tr).find(S_COMBO_CUENTA).prop('selectedIndex', 0);
    $(clonar_tr).find(S_COD_RET_FTE).prop('selectedIndex', 0);
    $(clonar_tr).find(S_COD_RET_IVA).prop('selectedIndex', 0);
    $(clonar_tr).find('select[data-name="bien_serv"]').prop('selectedIndex', 0);
    $(clonar_tr).find("select[data-name='combo_iva']").prop('selectedIndex', 0);
    $(clonar_tr.find(".valor")).change(function(){
        redondear(this);
        VerificarFormaPago();

        // Si es un reembolso de gastos calcula sus totales
        if (es_reembolso_gasto(url_es_reebolso, $("#id_tipo_de_doc").val()))
            calcular_totales_reembolso();
        else
            CalcularRetencion();
    });


    // Sustento tributario en detalle de compras
    var combo_sust_trib = $(clonar_tr).find("select[data-name='sust_tribut']");
    var check_sust_trib = $(clonar_tr).find(".check_sust");
    var sust_trib_general = $("#id_sust_tribut");
    cambio_sustento_tributario(check_sust_trib, combo_sust_trib, sust_trib_general);
    //AgregarTitulo(combo_sust_trib);

    // Habilita e inhabilita el sustento tributario
     $(clonar_tr).find(".check_sust").click(function(){
        var combo_sust_trib = $(clonar_tr).find("select[data-name='sust_tribut']");
        var sust_trib_general = $("#id_sust_tribut");
        cambio_sustento_tributario($(this), combo_sust_trib, sust_trib_general);
    });

    // Agregar titulo a check
     $(clonar_tr).find(".check_sust").tooltip('hide')
    .attr('data-original-title', $(this).attr("title"))
    .tooltip('fixTitle');


    /**************************************
    * Para validar el cambio de bien o servicio
    * con el porc de retencion
    */
    cambio_ret_iva($(clonar_tr).find('select[data-name="bien_serv"]'));
    $(clonar_tr).find('select[data-name="bien_serv"]').change(function(){cambio_ret_iva(this);});


    $(clonar_tr.find(".valor")).change(function(){
        CalcularRetencion();
    });
    $(clonar_tr.find(".iva")).change(function(){
       CalcularRetencion();
    });

    $($(clonar_tr).find("select[data-name='combo_iva']")).change(function(){habilitar_bs_iva(this)});

    $(clonar_tr.find("select:not(.detalle_cta, .centro_costo)")).change(function()
    {
        CalcularRetencion();
        //AgregarTitulo(this);
    });

    // Refresca los combos selectpicker
    $(clonar_tr).find(".selectpicker").selectpicker("refresh");
    clonar_tr.find('.ret_fte').ajaxComboBox(
        '',
        get_data_ajaxcb({"fecha_asiento": "#id_fecha_asiento"}, true)
    ).bind('change_ajaxcombo', function(){
                cambio_retenciones(this, ".porc_ret_fte");
            });

    clonar_tr.find('.ret_iva').ajaxComboBox(
        '',
        get_data_ajaxcb({"fecha_asiento": "#id_fecha_asiento"}, true)
    ).bind('change_ajaxcombo', function(){
                cambio_retenciones(this, ".porc_ret_iva");
            });
    clonar_tr.find(".porc_ret_fte").change(function(){
        CalcularRetencion();
    });
    clonar_tr.find(".porc_ret_iva").change(function(){
        CalcularRetencion();
    });
    clonar_tr.find('.ajaxcombobox').ajaxComboBox('',
        {
            lang: 'es',
            per_page: 10,
            db_table: "fff",
            button_img: dir_combo_vineta,
            init_record: "",
            bind_to: "change_ajaxcombo",
            scrollWindow: true
        }
    );
    //Agrega la fila clonada y recalcula los ids de las filas
    $(T_DETALLE_COMP+" tbody").append(clonar_tr);

    recalcular_ids(T_DETALLE_COMP);
    // Deshabilitar los combos de retenciones, ya que no existen en un reembolso ni en N/D y N/C
    CambioDocumento($(S_TIPO_DOC));
}


function AgregarFilasDetalleReembolso(e)
{
    e.preventDefault();
    var clonar_tr = $(T_DETALLE_REEM+" tbody tr:first").clone();
    clonar_tr.find(".bootstrap-select").remove();
    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });

    $(clonar_tr).find(".serie").mask('999-999-999999999');

    $(clonar_tr).find(".serie ").focusout(function(){
        AutocompletarCeros(this);
        if(verificar_docs_rep())
        {
            mensaje_alerta('No puede ingresar el mismo documento dos veces del mismo proveedor. Por favor verifique');
        }
    });

    // Elimina una fila de la table de reembolso
    $(clonar_tr.find(".eliminar")).click(function(e){
        EliminarFila(e, this, T_DETALLE_REEM);
        calcular_totales_reembolso();
    });

    //Cambia los combos a sus valores iniciales
    $(clonar_tr).find("select[data-name='combo_tipo_doc']").prop('selectedIndex', 0).selectpicker("refresh");
    $(clonar_tr).find("select[data-name='combo_tipo_id']").prop('selectedIndex', 0).selectpicker("refresh");

    // Agrega un punto flotante al valor ingresado
    $(clonar_tr.find(".base_cero")).focusout(function(){
       redondear(this);
       calcular_totales_reembolso();
    });
    // Agrega un punto flotante al valor ingresado
    $(clonar_tr.find(".base_iva")).focusout(function(){
       redondear(this);
        calcular_totales_reembolso();
    });

    // Agregar un tooltip a los combos
    $(clonar_tr).find("select").each(function(){
        //AgregarTitulo(this);
    });

    // Valida la fecha de emision del documento que no sea mayor a la del asieno contable
    $(clonar_tr).find(".fecha_emision").datepicker().on('changeDate', function (ev) {
        validar_fecha_em(this);
    });

    // Elimina el popover que se crea al clonar
    $($($(clonar_tr).find(".proveedor").parent().parent().get(0)).children()[2].children[1]).remove();

    // Valida que el número del docuemnto que ingresó sea válido
    $(clonar_tr).find(".proveedor").change(function(){
        var tr = $(this).parent().parent();
        var tipo_id = $(tr).find('select[data-name="combo_tipo_id"]').val();
        if($(this).val() != ""){
            if(validar_ruc(this, tipo_id))
            {
                mensaje_alerta('Número de documento inválido, revise el número de identificación y su tipo.');
            }
        }
    });

    // Valida que el número del docuemnto que ingresó sea válido al cambio del tipo de documento
    $(clonar_tr).find("select[data-name='combo_tipo_id']").change(function(){
        var tr = $(this).parent().parent();
        var ruc = $(tr).find(".proveedor");
        var tipo_id = $(this).val();

        if (ruc.val() != "")
        {
            if(validar_ruc(ruc, tipo_id))
            {
                mensaje_alerta('Número de documento inválido, revise el número de identificación y su tipo.');
            }
        }
    });

    // Agrega el elemento clonado a la tabla
    $(T_DETALLE_REEM+" tbody").append(clonar_tr);

    recalcular_ids(T_DETALLE_REEM);
}


function validar_fecha_asiento()
{
    var dia_asiento = new Date(parse_date($("#id_fecha_asiento").val()));
    if(dia_asiento > dia_actual_servidor)
    {
        mensaje_alerta('No puede ingresar una fecha superior a la fecha actual');
        $("#id_fecha_asiento").val(str_dia);
    }
}


function verificar_filas_encero()
{
    var array = [];
    var prov = [];
    var serie = [];
    var tipo_doc = [];
    var acum = 0;
    var cont = 0;
    array.push(prov);
    array.push(serie);
    array.push(tipo_doc);

    $(T_DETALLE_REEM+" tbody").find("tr").each(function()
    {
        array[0][cont] = $(this).find(".proveedor").val();
        array[1][cont] = $(this).find(".serie").val();
        array[2][cont] = $(this).find(".tipo_documento").val();
        cont++;
    });
    if(array.length>1)
    {
        for (var i=0; i<cont; i++){
            for(var j=i+1; j<cont; j++){
                /* Si el numero de documento es igual y el proveedor es el mismo
                   y si es el mismo número de documento, entonces error esta ingresando dos veces el
                   mismo documento
                */
                if(array[1][i] != "" && array[1][j] != "" && array[1][i] == array[1][j] && array[0][i] == array[0][j] && array[2][i] == array[2][j]){
                    acum++;
                }
            }
        }
        if(acum>0){
            return true;
        }
        else{
            return false;
        }
    }
    return false;
}


function verificar_fecha_vencimiento_retencion(url, fecha_emi, id_vigencia)
{
    $.ajax(
    {
        url:url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {'fecha_emi': fecha_emi,
               'id_vigencia': id_vigencia},
        error: function(){
            return true;
        },
        success: function(data)
        {
            if(data.status == 2){
                mensaje_alerta(data.mensaje);
            }
    }});
}


function es_reembolso_gasto(url, id)
{
    var resp = false;
    $.ajax(
    {
        url:url,
        type: 'GET',
        async: false,
        cache: false,
        timeout: 300,
        data: {"id": id},
        error: function(){
            return true;
        },
        success: function(data)
        {
            resp = data.status;
    }});
    return resp
}

function tiene_doc_modifica(url, id)
{
    var resp = false;
    $.ajax(
    {
        url:url,
        type: 'GET',
        async: false,
        cache: false,
        timeout: 300,
        data: {"id": id},
        error: function(){
            return true;
        },
        success: function(data)
        {
            resp = data.status;
    }});
    return resp
}

function compra_exterior(url, id)
{
    var resp = false;
    $.ajax(
    {
        url:url,
        type: 'GET',
        async: false,
        cache: false,
        timeout: 300,
        data: {"id": id},
        error: function(){
            return true;
        },
        success: function(data)
        {
            resp = data.status;
    }});
    return resp
}

function es_pago_credito(url, id)
{
    var resp = false;
    $.ajax(
    {
        url:url,
        type: 'GET',
        async: false,
        cache: false,
        timeout: 300,
        data: {"id": id},
        error: function(){
            return true;
        },
        success: function(data)
        {
            resp = data.status;
    }});
    return resp
}


function get_datos_cod_sri(url, id)
{
    var resp = {};

    $.ajax(
    {
        url:url,
        type: 'GET',
        async: false,
        cache: false,
        timeout: 300,
        data: {"id": id},
        error: function(){
            return true;
        },
        success: function(data)
        {
            resp = data

    }});
    return resp
}

/*******************************************
 * Función se activa cuando el usuario cambia el porcentaje
 * de retención y obtiene sus datos para ver si es de porcentaje
 * variable o no y coloca su porcentaje en una casilla de la tabla
 * @param obj
 * @param clase_porc
 */
function cambio_retenciones(obj, clase_porc){
    var datos = get_datos_cod_sri(url_get_datos_codigo_sri, $(obj).attr("value"));
    var tr = $(obj).parents("tr:first");
    var porc_ret_fte = tr.find(clase_porc);

    if(datos["status"] == 1)
    {
        var valor = datos["porcentaje"];
        var es_variable = datos["variable"];
        var porc_min = datos["porc_min"];
        var porc_max = datos["porc_max"];

        if (es_variable)
        {
            $(porc_ret_fte).removeAttr("readonly");
            $(porc_ret_fte).val(porc_min);
            mensaje_alerta("El código de retención elegido es de porcentaje variable " +
                    "el cual está entre el "+porc_min+" hasta "+porc_max+" porciento. Predeterminado " +
                    "esta el porcentaje minimo de ese codigo, pero usted lo puede cambiar.");
        }
        else{
            $(porc_ret_fte).attr("readonly", "readonly");
            $(porc_ret_fte).val(valor)
        }
    }
    else{
        $(porc_ret_fte).attr("readonly", "readonly");
        $(porc_ret_fte).val(valor)
    }
    CalcularRetencion();
}
/*****************************
 * Funcion que devuelve la data necesaria para el ajaxcombobox
 * @param extradata
 * @param subinfo
 * @returns {{lang: string, per_page: number, db_table: string, button_img: *, init_record: string, bind_to: string, scrollWindow: boolean, sub_info: boolean, primary_key: string, extradata: *}}
 */
function get_data_ajaxcb(extradata, subinfo){
    return {
            lang: 'es',
            per_page: 10,
            db_table: "fff",
            button_img: dir_combo_vineta,
            init_record: "",
            bind_to: "change_ajaxcombo",
            scrollWindow: true,
            sub_info: subinfo,
            primary_key: "id",
            extradata: extradata
        }
}