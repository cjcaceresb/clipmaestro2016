
// Variables Locales
var CampReq = "campo_requerido";

/***********************
 * function enviar_retencion: Valida los datos de la retención
 * y luego los envía al servidor para cambiar la retención
 * @returns {null}
 */
function enviar_retencion()
{
    var num_ret_cam = $("#id_numero_ret");
    var fecha_emi_cam = $("#id_fecha_emision");

    // Revisa si tiene la clase requerido y la elimina
    if(num_ret_cam.hasClass(CampReq))
        num_ret_cam.removeClass(CampReq);

    if(fecha_emi_cam.hasClass(CampReq))
        fecha_emi_cam.removeClass(CampReq);

    // Validar q ingrese algun dato
    if(num_ret_cam.val() == "" || fecha_emi_cam.val() == ""){
        if(num_ret_cam.val() == "")
            num_ret_cam.addClass(CampReq);
        if(fecha_emi_cam.val() == "")
            fecha_emi_cam.addClass(CampReq);
    }
    else
    {
        var id = $("#cambio_ret").attr("data-id");
        $("#id_id_ret").val(id);
        var datastring = $("#form-nueva-ret").serialize();
        $.ajax({
            url: url_camb_ret,
            type: 'POST',
            async: false,
            timeout: 3000,
            dataType: "json",
            data: datastring,
            error: function(){
                return true;
            },
            success: function(data){
                var cont = 0;
                var mensajes = new Array();

                for(var i=0; i<data.length; i++)
                {
                    if(data[i].status != 5)
                    {
                        mensajes.push(data[i].mensaje);
                        cont++;
                    }
                    else
                    {
                        if(data[i].status == 2)
                        {
                            mensajes.push(data[i].mensaje);
                            cont++;
                        }
                    }
                }
                if(cont != 0)
                {
                    var stringhtml = "";
                    for(i=0; i<mensajes.length; i++)
                    {
                        stringhtml = (stringhtml+"<p>"+mensajes[i]+"</p>")
                    }
                    $(".error-modal").fadeIn("slow");
                    $("#error-retencion").html(stringhtml)
                }
                if(cont == 0)
                    window.location.href = url_comp;
            }
        });
    }
    return null;
}

/************************
 * Función que te redirecciona al detalle de
 * la compra
 */
function ver_compra()
{
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0){
        window.location.href = input_check.attr("data-ver");
    }else{
        mensaje_alerta("Seleccione un item para ver el detalle de la compra");
    }
}

/************************
 * Función que te redirecciona a la
 * pantalla de copiar compra
 */
function copiar_compra()
{
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0)
    {
        var status = parseInt(input_check.attr("data-status"));
        switch (status)
        {
            case 1:
                window.location.href = input_check.attr("data-copiar");
                break;
            case 2:
                mensaje_alerta("La compra que quiere copiar se encuentra anulada.");
                break;
            case 3:
                mensaje_alerta("La compra que quiere copiar se encuentra en estado pendiente.");
                break;
        }
    }
    else{
        mensaje_alerta("Seleccione un item para copiar una compra");
    }
}

/*******************************
 * Función que te redirecciona la URL de anular compra
 */
function anular_compra()
{
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0)
    {
        var tr = input_check.parent().parent();
        var nombre = tr.find(".nombre").text();
        var num_ret = tr.find(".num_retencion").text();
        var url = input_check.attr("data-anular");
        var status = input_check.attr("data-status");
        var estado_firma = parseInt(input_check.attr("data-estado_firma"));
        var num_comp = tr.find(".num_comp").text();

        if(status == "2")
        {
            mensaje_alerta("La compra que quiere anular ya se encuentra anulada");
        }
        else{
            if (estado_firma >= 3)
            {
                mensaje_alerta_yn("La compra que seleccionó tiene asociada una retención que ya ha sido autorizada por el SRI, " +
                    "es su responsabilidad que esta retención ya haya sido anulada desde la página del SRI <br><br>" +
                    "Desea Continuar ?", "ANULAR COMPRA", "data-href", url)
            }
            else{
                if (num_ret!= "")
                {
                    mensaje_alerta_yn("Desea anular la compra: " + num_comp + " con documento #" + nombre + " y con la retención # " + num_ret + " ?", "ANULAR COMPRA", "data-href", url);
                }
                else
                {
                    mensaje_alerta_yn("Desea anular la compra " + num_comp + " con documento #" + nombre + " ?", "ANULAR COMPRA", "data-href", url);
                }
            }
        }
    }
    else{
        mensaje_alerta("Seleccione un item para anular una compra");
    }
}

/******************************
 * Función que te redirecciona a la pantalla de uso de
 * provisión de la compra
 */
function provision_compra()
{
    var input_check = $("#lista").find("input[type='radio']:checked");

    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var nombre = tr.find(".num_comp").text();

        var url = input_check.attr("data-provision");
        var status = input_check.attr("data-status");
        if(status == "3")
        {
            mensaje_alerta_yn("Desea usar esta compra provisionada " + nombre + " ?", "PROVISIÓN COMPRA", "data-href", url);
        }
        else
        {
            if(status == "2")
            {
                mensaje_alerta("La compra que quiere provisionar se encuentra anulada");
            }
            else
            {
                mensaje_alerta("La compra que seleccionó no se puede usar para la provisión");
            }
        }
    }
    else{
        mensaje_alerta("Seleccione un item para provisionar una compra");
    }
}

function cambiar_retencion()
{
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var nombre = tr.find(".nombre").text();
        var url = input_check.attr("data-anular_generar");
        var id = input_check.val();
        $("#id_venta").val(input_check.val());
        $(".error-modal").hide();
        $.ajax(
        {
            url: url_tien_ret,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: {
                'id': input_check.val()
            },
            error: function(){
                return true;
            },
            success: function(data)
            {
                if(data.status == 1)
                {
                    var cambio_ret_modal = $("#cambio_ret");
                    cambio_ret_modal.attr("data-id", id);
                    // Cargar datos de la retención antigua
                    $.ajax({
                        type: "POST",
                        url: url_dat_ret,
                        data: { id: id}
                        })
                        .done(function( msg ) {
                            $("#id_numero_ret").val(msg.numero);
                            $("#id_fecha_emision").val(msg.fecha);
                            $("#id_serie").val(msg.serie).selectpicker("refresh");
                    });
                    // Revisa si tiene la clase requerido y la elimina
                    $("#id_numero_ret").removeClass(CampReq);
                    cambio_ret_modal.modal();
                    $(".error-modal").hide();
                }
                else
                {
                    if (data.status == 0)
                    {
                        mensaje_alerta("La compra seleccionada no tiene registrada una retención");
                    }
                    else
                    {
                        mensaje_alerta("La compra seleccionada se encuentra anulada");
                    }
                }
            }
        });
    }
    else{
        mensaje_alerta("Seleccione un item para agregar una retención");
    }
}

/************************************
 * Función que te lleva a la pantalla de anular y
 * generar una compra
 */
function anular_generar_compra()
{
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var nombre = tr.find(".nombre").text();
        var url = input_check.attr("data-anular_generar");
        var status = parseInt(input_check.attr("data-status"));
        var estado_firma = parseInt(input_check.attr("data-estado_firma"));

        switch (status)
        {
            case 1:
                if(estado_firma >= 3)
                {
                    mensaje_alerta("La compra que seleccionó no se puede anular y generar ya que la retención " +
                                    "asociada a esa compra ya ha sido enviada al SRI, en el caso que desee anular " +
                                    "la retención, tiene que hacerlo desde la página del SRI, y en el sistema " +
                                    "usar la opción de anular la compra y una ves anulada luego copiarla")
                }
                else{
                    mensaje_alerta_yn("Desea anular la compra" + nombre + ' y generar otra ?',
                    "ANULAR Y GENERAR COMPRA", "data-href", url);
                }
                break;
            case 2:
                mensaje_alerta("La compra que quiere anular se encuentra anulada");
                break;
            case 3:
                mensaje_alerta("La compra que quiere anular se encuentra en estado pendiente");
                break;
        }

    }
    else{
        mensaje_alerta("Seleccione un item para anular y generar una compra");
    }
}

/**************************************
 * Función para anular la siguiente retención
 * del blog de retenciones
 * @param obj
 */
function anular_retencion(obj)
{
    $(obj).attr("disabled", true);
    var texto = $(obj).text();
    var datastring = $("#form_anular_ret").serialize();
    $.ajax(
    {
        url: url_anul_ret,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 6000,
        data: datastring,
        beforeSend: function(){
            $(obj).text("Por favor, espere...");
        },
        error: function(){
            $(".mensaje-error-retencion").text("Error en la conexión por favor vuelva a intentarlo más tarde");
            $(".error-modal").fadeIn("slow");
            $(obj).attr("disabled", false).text(texto);
        },
        success: function(data)
        {
            if(data.status == 1)
            {
                window.location.reload(true);
            }
            else
            {
                $(".mensaje-error-retencion").text(data.mensaje);
                $(".error-modal").fadeIn("slow");
                $(obj).attr("disabled", false).text(texto);
            }
        }
    });
}

/*****************************************
 * Función que obtine el número de la última retención
 * del block selecionado
 * @returns {string}
 */
function get_ultima_ret()
{
    var id = $("#id_documento").val();
    var num_ret = "";
    $.ajax(
    {
        url: url_get_ult_ret,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {"id_vigencia": id},
        beforeSend: function(){
            $("#icon-load").addClass("fa-spin");
        },
        error: function(){
            num_ret = "Error de Conexión";
            $("#icon-load").removeClass("fa-spin");
        },
        success: function(data)
        {
            if(data.status == 1)
            {
                num_ret = data.num_ret
            }
            else
            {
                num_ret = "Escoja una block de retención válida"
            }
            $("#icon-load").removeClass("fa-spin");
        }
    });
    return num_ret;
}

function enviar_retencion_electronica()
{
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var nombre = tr.find(".nombre").text();
        var num_retencion = tr.find(".num_retencion").text();
        var url = input_check.attr("data-enviar_ret_elect");
        var status = input_check.attr("data-status");
        var estado_firma = parseInt(input_check.attr("data-estado_firma"));
        if(status == "1")
        {
            if (estado_firma == -1)
            {
                mensaje_alerta("La compra no tiene retención por favor verifique")
            }
            else
            {
                if(estado_firma < 4)
                {
                    mensaje_alerta_yn("Desea autorizar la retención #"+num_retencion+" , de la compra #" + nombre + " ?",
                                      "AUTORIZAR RETENCIÓN SRI", "data-href", url);
                }
                else
                {
                    mensaje_alerta("La retención de la compra seleccionada ya se encuentra autorizada por el SRI")
                }
            }
        }
        else if(status == "2"){
            mensaje_alerta("La compra seleccionada se encuentra anulada");
        }
        else if(status == "3"){
            mensaje_alerta("La compra seelccionada se encuentra en estado pendiente");
        }
    }
    else{
        mensaje_alerta("Seleccione una compra con retención para enviarla al SRI");
    }
}

function enviar_mail_retencion_electronica()
{
    var input_check = $("#lista").find("input[type='radio']:checked");
    if (input_check.length != 0){
        var tr = input_check.parent().parent();
        var nombre = tr.find(".nombre").text();
        var num_retencion = tr.find(".num_retencion").text();
        var url = input_check.attr("data-enviar_mail_ret_elect");
        var status = input_check.attr("data-status");
        var modal_correo = $("#envio_correo");
        var estado_firma = parseInt(input_check.attr("data-estado_firma"));

        $(".error-modal").hide();

        if(status == "1")
        {
            if (estado_firma == -1)
            {
                mensaje_alerta("La compra no tiene retención por favor verifique")
            }
            else
            {
                if( estado_firma >= 4 )
                {
                    $.ajax(
                    {
                        url: url_get_correo,
                        type: 'POST',
                        async: false,
                        cache: false,
                        timeout: 150,
                        data: {
                            'id': input_check.attr("value")
                        },
                        beforeSend: function(){
                            $('#loading').show();
                            $('body').css('display', '0.7');
                        },
                        error: function(){
                            $("#correo_proveedor").text("Hubo un error de conexión, por favor cierre la ventana e intente nuevamente");
                            $("#ok_send_mail").hide();
                            $('#loading').fadeOut();
                            $('body').css('opacity', '1');
                        },
                        success: function(data)
                        {
                            if(data.status=1)
                            {
                                $("#correo_proveedor").text(data.correo);
                            }
                            else
                            {
                                $("#correo_proveedor").text(data.mensaje);
                            }
                            $('#loading').fadeOut();
                            $('body').css('opacity', '1');
                        }
                    });

                    $("#id_id_compra").val(input_check.attr("value"));
                    modal_correo.modal();
                }
                else{
                    mensaje_alerta("La compra que seleccionó no tiene retención autorizada por el SRI, por favor verifique")
                }
            }
        }
        else if(status == "3"){
            mensaje_alerta("La compra seleccionada se encuentra en estado pendiente");
        }
        else if(status == "2"){
            mensaje_alerta("La compra seleccionada se encuentra anulada");
        }
    }
    else{
        mensaje_alerta("Seleccione una compra con retención para enviarla por correo");
    }
}

jQuery(document).ready(function($){

    $(".evento_page").each(function(){
        $(this).click(function(){
            var page = $(this).attr("data-page");
            $("#id_page").val(page);
            $("#buscar").click();
        });
    });

    $(":file").filestyle({buttonText: "Buscar"});

    // Limpiar el formulario
    $("#limpiar").click(function(){
        var form = $("#formulario");
        limpiar_form(form);
    });

    /**************************
     * Enviar retencion electrónica por mail
     */
    $("#ok_send_mail").click(function(){
        $(this).text("Por favor Espere ...").attr("disabled", "disabled");
        $("#cancel_envio_correo").hide();
        $.ajax(
        {
            url: url_enviar_correo,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: new FormData($("#form_envio_correo")[0]),
            processData: false,
            contentType: false,
            beforeSend: function(){
            },
            error: function(){
                $("#ok_send_mail").text("Enviar al Correo").attr("disabled", false);
                $("#cancel_envio_correo").show();
                $("#error-envio").text("Existió un error en la conexión al servidor por favor intente más tarde");
                $(".error-modal").show()
            },
            success: function(data)
            {
                if(data.status == 1)
                    location.reload();
                else{
                    $("#cancel_envio_correo").show();
                    $("#error-envio").text(data.mensaje);
                    $(".error-modal").show();
                    $(this).text("Enviar al correo").attr("disabled", false);
                }
            }
        });
    });

    // Si existe algun error el botón de enviar correo se oculta, esta función lo muestra nuevamente
    $("#cancel_envio_correo").click(function(){
        $("#ok_send_mail").show();
    });

    // Cierra el div de error en el modal
    $(".close-error").click(function(){
       $(".error-modal").hide();
    });
    /******************************
     * Anular Retención
     */
        $("#ok-ret-anul").click(function(){
            anular_retencion(this);
        });

        $("#anular_ret").click(function(){
            $("#ultima_ret").text(get_ultima_ret()).fadeIn("slow");
            $(".error-modal").hide();
            $("#anular_ret_modal").modal();
        });

        $("#id_documento").change(function(){
            var ult_retencion = $("#ultima_ret");
            ult_retencion.hide();
            ult_retencion.text(get_ultima_ret());
            ult_retencion.fadeIn("slow");
        });

        $(".cargar_ret").click(function(){
            var ult_retencion = $("#ultima_ret");
            ult_retencion.hide();
            ult_retencion.text(get_ultima_ret()).fadeIn("slow");
            ult_retencion.fadeIn("slow");
        });
    /***********************************
     */

    $(".cerrar-error").click(function(e){
        e.preventDefault();
        $(this).parent().fadeOut("slow");
    });

    $(".numerico").keydown(function(event) {Numerico(event)});
    $(".numerico_doc").keydown(function(event) {NumeroEntero(event)});
    $(".selectpicker").selectpicker();

    $("#id_fecha").mask('9999-99-99');
    $("#id_fecha_final").mask('9999-99-99');
    $("#id_fecha_retencion").mask('9999-99-99');

    $("#cerrar").click(function(){
       $("#error").fadeOut("slow");
    });

    $("#ok-ret").on("click",function(){
        enviar_retencion()
    });

    $("#cancel-ret").on("click", function(){
        $("#num_ret_camb").val("");
        $("#aut_ret_camb").val("");
        $("#id_fecha_retencion").val("");
    });

    $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000).removeClass("container-buscador-hide");
        $(".opcion-buscar").addClass("opcion-buscar-hide");
    });

    $(".buscador-cerrar").click(function(){
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
    });

    $("#ver").click(function(){ ver_compra() });
    $("#copiar").click(function(){ copiar_compra() });
    $("#anular").click(function(){ anular_compra() });
    $("#cambiar_ret").click(function(){ cambiar_retencion() });
    $("#anular_generar").click(function(){ anular_generar_compra() });
    $("#provision").click(function(){provision_compra()});
    $("#enviar_reten_electronica").click(function(){enviar_retencion_electronica()});
    $("#enviar_mail_reten_electronica").click(function(){enviar_mail_retencion_electronica()});

    $("#ok-alert").on("click",function(){
        window.location.href = $("#alert-yn").attr("data-href");
    });
});
