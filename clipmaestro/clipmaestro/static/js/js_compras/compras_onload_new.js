/**
 * Created by Clip Maestro on 05/06/2015.
 */

function cambio_sustento_tributario(check_sust_trib, combo_sust_trib, sust_trib_general){

    if (check_sust_trib.is(":checked")){
        combo_sust_trib.attr("disabled", false);
        combo_sust_trib.selectpicker("refresh");
    }
    else
    {
        combo_sust_trib.attr("disabled", true);
        combo_sust_trib.val(sust_trib_general.val()).trigger("change");
        combo_sust_trib.selectpicker("refresh");
    }
}

// Funciones que necesiten ejecutarse cuando ya se haya cargado el documento
$( window ).load(function() {

    $('#loading').fadeOut();
    $('body').css('opacity', '1');

    // Inhabilita el boton submit
    $(".btn_grabar").click(function()
    {
        $('#loading').show();
        $('body').css('display', '0.5');
        $(".btn_grabar").hide();
        $(".btn_cancelar").attr("href", "#").text("Por favor, espere...");
        $(".sust_tribut").attr("disabled", false);

        $("form").submit(function(){
        });
    });

    if(!provision)
    {
        $( "#id_fecha_asiento" ).datepicker().on('changeDate', function (ev) {
        $(this).datepicker("hide");
        CambioFechaAsiento();
        validar_fecha_asiento();
        cambio_ret_iva($('select[data-name="bien_serv"]'));
        }).focus(function(){
            fecha_asiento_ant = $(this).val();
        });
    }


    $( "#id_fecha_emision_retencion" ).datepicker().on('changeDate', function (ev){
        var id_vigencia = 0;
        var id_vigencia_anular_generar = $("#id_vigencia_ret_anular_generar").text();

        validar_fecha_em_ret(this);
        getComboSerieRet();

        if (id_vigencia_anular_generar)
        {
            verificar_fecha_vencimiento_retencion(url_verificar_fecha_ret, $(this).val(), id_vigencia_anular_generar)
        }
        else
        {
            id_vigencia = $(SERIE_RET).val();
            verificar_fecha_vencimiento_retencion(url_verificar_fecha_ret, $(this).val(), id_vigencia)
        }
        $(this).datepicker("hide");
    });


    $( "#id_fecha" ).datepicker().on('changeDate', function (ev) {
        validar_fecha_em(this);
        validar_fecha_ret(this);
        $(this).datepicker("hide");
    });


    $("#add_prov").click(function(){
        $(".pop-error").hide();
        $("#div-proveedor").modal();
    });
    // Cierra el div de mensajes de error al agregar proveedor en compras
    $("#cerrar").click(function(){
        $(this).parent().hide();
    });

    // Presenta el tipo de proveedor cuando la identificación del proveedor sea PASAPORTE
    $("#id_tipo_identificacion").change(function(){
        if($(this).val() == "P"){
            $(".tipo_identificacion_prov").fadeIn("slow");
        }
        else{
            $(".tipo_identificacion_prov").fadeOut("slow");
        }
    });

    $("#send_form_prov").click(function(e){ AgregarProveedor(e) });


    // Validar que el número de identificación en el reembolso de gastos es correcto
    $(".proveedor").change(function(){
        var tr = $(this).parent().parent();
        var tipo_id = $(tr).find('select[data-name="combo_tipo_id"]').val();
        if ($(this).val != "")
        {
            if(!validar_ruc(this, tipo_id)){
                mensaje_alerta('Número de documento inválido, revise el número de identificación y su tipo.');
            }
        }
    });

    // Validar que el número de identificación en el reembolso de gastos es correcto
    $("select[data-name='combo_tipo_id']").change(function(){
        var tr = $(this).parent().parent();
        var ruc = $(tr).find(".proveedor");
        var tipo_id = $(this).val();

        if (ruc.val() != "")
        {
            if(!validar_ruc(ruc, tipo_id))
            {
                mensaje_alerta('Número de documento inválido, revise el número de identificación y su tipo.');
            }
        }
    });

    //Inhabilitar el "enter" en los inputs para no enviar el formulario con un "ENTER"
    $('form :input').keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
    });
    // Agrega una nueva fila a la tabla de detalle de compras
    $("#add").click(function(e){ AgregarFilasDetalleCompras(e) });

    // Agrega una fila en la tabla de detalle de remmbolso
    $("#add_reembolso").click(function(e){ AgregarFilasDetalleReembolso(e) });

    // Para eliminar las filas del detalle de compras y de reembolso
    $(T_DETALLE_COMP).find(".eliminar").click(function(e){ EliminarFila(e, this, T_DETALLE_COMP);} );
    $(T_DETALLE_REEM).find(".eliminar").click(function(e){ EliminarFila(e, this, T_DETALLE_REEM); });

    // Autocompleta los ceros que tiene un documento ej: "001-001-000000001"
    // Se debe usar el evento focusout no funciona con change
    $(NUM_DOC).focusout(function(){
        AutocompletarCeros(this);
        getDatosProveedor( $("#id_proveedor").attr("value"), $("#id_tipo_de_doc").val());
    });
    $(NUM_DOC_MODIF).focusout(function(){ AutocompletarCeros(this);});

    $(NUM_RET).focusout(function(){ AutocompletarCeros(this); });

    $(T_DETALLE_REEM).find(".serie ").focusout(function(){
        AutocompletarCeros(this);
        if (verificar_docs_rep()){
            mensaje_alerta("No puede ingresar el mismo documento dos veces del mismo proveedor. Por favor verifique.");
        }
    });

    // Valida que la fecha de los documentos del reembolso sea mayor a la fecha del asiento contable
    $(T_DETALLE_REEM).find(".fecha_emision").focusout(function(){validar_fecha_em(this)});

    $(T_DETALLE_REEM).find("select").change(function(){ AgregarTitulo(this) });

    $(T_DETALLE_REEM).find(".fecha_emision").datepicker().on('changeDate', function (ev){ validar_fecha_em(this) });

    //Para que agrege automaticamente punto
    $(T_DETALLE_REEM).find(".base_cero").change(function(){
        redondear(this);
        calcular_totales_reembolso();
    });
    $(T_DETALLE_REEM).find(".base_iva").change(function(){
        redondear(this);
        calcular_totales_reembolso();
    });

    $(".eliminar").click(function(){
        CalcularRetencion();
    });

    $(".porc_ret_fte").change(function () {
        CalcularRetencion();
    });
    $(".porc_ret_iva").change(function () {
        CalcularRetencion();
    });

    // Agrega los punto decimal a los valores ingresados y calcula totales
    $(".valor").change(function()
    {
        redondear(this);
        VerificarFormaPago();

        // Si es un reembolso de gastos calcula sus totales
        if (es_reembolso_gasto(url_es_reebolso, $("#id_tipo_de_doc").val()))
            calcular_totales_reembolso();
        else
            CalcularRetencion(this);
    });

    //  Funcion para deshabilitar el combo Retencion Iva
    //  si es que esta o no sujeta a IVA
    var select_combo_iva = $(T_DETALLE_COMP).find("select[data-name='combo_iva']");
    $(select_combo_iva).change(function(){ habilitar_bs_iva(this) });





    // Codigo para el sustento tributario en detalle de compras
    $(T_DETALLE_COMP).find("tbody tr").each(function(){
        var combo_sust_trib = $(this).find("select[data-name='sust_tribut']");
        var check_sust_trib = $(this).find(".check_sust");
        var sust_trib_general = $("#id_sust_tribut");
        cambio_sustento_tributario(check_sust_trib, combo_sust_trib, sust_trib_general);
    }).change(function(){
        AgregarTitulo(this);
    });

    // Habilita e inhabilita el sustento tributario
    $(".check_sust").click(function(){
        var tr = $(this).parent().parent();
        var combo_sust_trib = $(tr).find("select[data-name='sust_tribut']");
        var sust_trib_general = $("#id_sust_tribut");
        cambio_sustento_tributario($(this), combo_sust_trib, sust_trib_general);
    });

    // Agregar titulo a check
    $(".check_sust").tooltip('hide')
    .attr('data-original-title', $(this).attr("title"))
    .tooltip('fixTitle');

    $("#id_sust_tribut").change(function(){
        $(T_DETALLE_COMP).find("tbody tr").each(function(){
            var combo_sust_trib = $(this).find("select[data-name='sust_tribut']");
            var check_sust_trib = $(this).find(".check_sust");
            var sust_trib_general = $("#id_sust_tribut");
            cambio_sustento_tributario(check_sust_trib, combo_sust_trib, sust_trib_general);
        })
    });


    // Función que me permite ingresar valores numéricos
    // incluidos el punto flotante
    $(".numerico").keydown(function(event) {
        Numerico(event)
    });
    // Función que me permite inngresar sólo números enteros
    $(".numero_natural").keydown(function(event){
        NumeroEntero(event)
    });

    // Aparecer div cuando este seleccionado
    // el reembolso de gasto y calcula sus totales
    if(es_reembolso_gasto(url_es_reebolso, $("#id_tipo_de_doc").attr("value")))
    {
        calcular_totales_reembolso();
        $("#detalle_reembolso").attr("style","display:block");
        $(T_DETALLE_COMP).find(S_COD_RET_FTE).each(function(){ $(this).attr("disabled", true); });
        $(T_DETALLE_COMP).find(S_COD_RET_IVA).each(function(){ $(this).attr("disabled", true); });
    }

    // Para mostrar la fecha de vencimiento al iniciar si es que existe un plazo ya definido "enviado desde el servidor"
    MostrarFechaVenci($("#id_plazo"));

    $("#id_plazo").change(function(){MostrarFechaVenci(this)});


     // Funcion para Credito o Contado activa la celda de plazo y la pinta como requerida
     // Credito cod 2
     // Contado cod 1
    ContadoCredito($("#id_forma_pago"));
    $(S_FORMA_PAGO).change(function(){
       ContadoCredito($("#id_forma_pago"))
    });

    //  Cambia el color del borde para que el usuario vea que tiene que
    //  seleccionar un pais
    $("#id_pago").change(function(){
        PagoExterior($("#id_pago"));
    });

    /**************************
     * Revisar N/C y N/D los cambios en el documento que ya se ha borrado
     */
    // Función para los cambios de documentos y realiza los cambios necesarios para cada uno
    $("#id_tipo_de_doc").bind('change', function(){
        CambioDocumento(this);
    });

    $(S_SUST_TRIB).change(function(){
        $("#doc_mofi_hide").attr("style","display: none");
        $("#doc_modifica_title").attr("style","display: none");
    });
    // Función para validar la fecha de vencimiento de la factura
    $( "#id_vence" ).datepicker().on('changeDate', function(ev){
        var dia_fecha_emision = new Date(parse_date($("#id_fecha").val()));
        var dia_fecha = new Date(parse_date($(this).val()));

        if(dia_fecha_emision > dia_fecha){
            $(this).val("");
            mensaje_alerta("No puede escojer una fecha de vencimiento inferior a la fecha de emisión del documento.");
            $(this).datepicker("hide");
        }
        $(this).datepicker("hide");
    });
    // Carga los datos de la retención
    CargarAutorizacion();
});

