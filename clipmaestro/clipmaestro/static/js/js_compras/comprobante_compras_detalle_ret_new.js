/**
 * Created by Clip Maestro on 05/06/2015.
 */

var cuentas_ret = [];

function toFixed ( number, precision ) {
    var multiplier = Math.pow( 10, precision + 1 ),
        wholeNumber = Math.floor( number * multiplier );
    return Math.round( wholeNumber / 10 ) * 10 / multiplier;
}

function LlenarArrayRetenciones(cod_retenciones, cod_ret_selec, i, valor, porc_ret, id_cod_sri, tipo)
{
    if(i==0){ // La primera vez que ingresa una RETENCION (FUENTE o IVA)
        cod_retenciones[i] = new Array(3);
        cod_retenciones[i][0] = cod_ret_selec;
        cod_retenciones[i][1] = valor;
        cod_retenciones[i][2] = porc_ret;
        cod_retenciones[i][3] = tipo;
        cod_retenciones[i][4] = id_cod_sri;
        i++;
    }
    else
    {
        var cont =0; // variable para contabilizar las concurrencias entre el codigo de las retenciones
        for (var j=0; j< cod_retenciones.length; j++){
            if(cod_retenciones[j][0] == cod_ret_selec)
            {
                cod_retenciones[j][1]= parseFloat(cod_retenciones[j][1])+parseFloat(valor);
                cont++;
            }
        }
        if(cont == 0){ // Si no existe ningun codigo de retención repetido se crea otra instancia
            cod_retenciones[i] = new Array(3);
            cod_retenciones[i][0] = cod_ret_selec;
            cod_retenciones[i][1] = valor;
            cod_retenciones[i][2] = porc_ret;
            cod_retenciones[i][3] = tipo;
            cod_retenciones[i][4] = id_cod_sri;
            i++;
        }
    }
    return i;
}

function LlenarFilasRetencion(cod_retenciones, cuentas_ret, url_ajax, tipo, x, cont_forms)
{
    /**********************************************
    *   Creacion del  variables de tags
    */
    var tabla_det_ret = $("#detalle_retenciones");
    var clonar = $("#detalle_retenciones tbody tr:first").clone();
    var tipo_input = clonar.find(".tipo");
    var cod_sri = clonar.find(".cod_sri");
    var codigo_ret = clonar.find(".codigo_ret");
    var cod_cta = clonar.find('select[data-name="cod_cta"]');
    var base = clonar.find(".base");
    var porcentaje = clonar.find(".porcentaje");
    var total = clonar.find(".total");
    tipo_input.val(tipo);
    tipo_input.attr("value", tipo);

    if(cod_retenciones[x][3] == 1)
        codigo_ret.val("Ret IVA. "+ cod_retenciones[x][0]);
    else
        codigo_ret.val("Ret Fte. "+ cod_retenciones[x][0]);

    cod_sri.val(cod_retenciones[x][4]);

    cod_sri.attr("value", cod_retenciones[x][4]);

    base.val(parseFloat(cod_retenciones[x][1]));
    porcentaje.val(cod_retenciones[x][2]);

    total.val(toFixed(parseFloat(parseFloat(cod_retenciones[x][1])*parseFloat(cod_retenciones[x][2])/100), 2));
    tabla_det_ret.append(clonar);

    cod_cta.empty();

    $.ajax(
    {
        url: url_ajax,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'porc_ret': porcentaje.val(),
            'tipo': tipo
        },
        error: function(){
            return true;
        },
        success: function(data){
            for(var i=0; i<data.length; i++)
            {
                if( i==0 )
                    cod_cta.append('<option value="' + data[i].id + '" selected="selected" >' + data[i].descripcion + '</option>');
                else
                    cod_cta.append('<option value="' + data[i].id + '" >' + data[i].descripcion + '</option>');
            }
        }
    });

    cont_forms++;
    return cont_forms;
}

function llenarRetenciones()
{
    $("#retenciones").empty();
    var cod_retenciones = [];
    var cod_retenciones_iva_b = [];
    var cod_retenciones_iva_s = [];
    var i=0; // contador para el numero de ret_fte
    var p=0; // contador para el numero de ret_iva_b
    var k=0; // contador para el numero de ret_iva_s
    var cont_forms = 0; // Cuenta la cantidad de form


    $("#tabla_formset tbody tr").each(function(){
        var iva_valor_comb = $(this).find('select[data-name="combo_iva"]');
        var iva_valor;
        var retencion = $(this).find('select[data-name="combo_cod_ret_fte"]');
        var cod_ret_selec;
        var porc_ret_fte;
        var iva = $(this).find('select[data-name="combo_cod_ret_iva"]');
        var cod_ret_iva_selec;
        var por_ret_iva_selec;
        var bien_servicio = $(this).find('select[data-name="bien_serv"]');
        var bien_servicio_selec;
        var valor_input = $(this).find('.valor');
        var valor = 0.0;
        if($(valor_input).val()!=""){
            valor = parseFloat($(valor_input).val());
        }
        iva_valor = $(iva_valor_comb).find("option:selected").attr("data-porcentaje");

        cod_ret_selec = $(retencion).find("option:selected").attr("data-codigo");
        porc_ret_fte = $(retencion).find("option:selected").attr("data-porcentaje");

        cod_ret_iva_selec = $(iva).find("option:selected").attr("data-codigo");
        por_ret_iva_selec = $(iva).find("option:selected").attr("data-porcentaje");

        bien_servicio_selec = $(bien_servicio).find("option:selected").attr("value");

        if(cod_ret_iva_selec != ""){
            if(bien_servicio_selec == 1){
                p = LlenarArrayRetenciones(cod_retenciones_iva_b, cod_ret_iva_selec, p, (valor*iva_valor/100), por_ret_iva_selec, iva.val(), 1)
            }
            else{
                k = LlenarArrayRetenciones(cod_retenciones_iva_s, cod_ret_iva_selec, k, (valor*iva_valor/100), por_ret_iva_selec, iva.val(), 1)
            }
        }
        if(cod_ret_selec!="")
        {
            i = LlenarArrayRetenciones(cod_retenciones, cod_ret_selec, i, valor, porc_ret_fte, retencion.val(), 2)
        }
    });
    for(var x=0; x<cod_retenciones.length; x++){
        if(cod_retenciones[x][2]!=0)
        {
            cont_forms = LlenarFilasRetencion(cod_retenciones, cuentas_ret, "/compras/buscar_retencion/", "r", x, cont_forms)
        }
    }
    for(var j=0; j<cod_retenciones_iva_b.length; j++){
        cont_forms = LlenarFilasRetencion(cod_retenciones_iva_b, cuentas_ret, "/compras/buscar_retencion/", 1, j, cont_forms)
    }
    for(var t=0; t<cod_retenciones_iva_s.length; t++){
        cont_forms = LlenarFilasRetencion(cod_retenciones_iva_s, cuentas_ret, "/compras/buscar_retencion/", 2, t, cont_forms)
    }

    if($("#detalle_retenciones tbody tr").length >= 2)
        $("#detalle_retenciones tbody tr:first").remove();
    recalcular_ids("#detalle_retenciones");

}

function CalcularRetencion()
{
    var bandera = CalcularTotales();
    var tabla_det_ret = $("#detalle_retenciones");
    tabla_det_ret.find("tbody tr:not(tr:first)").remove();

    if (bandera)
    {
        $.ajax(
        {
            url: url_tabla_retenciones,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: $('#formulario').serialize(),
            error: function(){
                return true;
            },
            success: function(data){
                $("#detalle_retenciones").empty();
                $("#detalle_retenciones").html(data);
                $("#detalle_retenciones").find('.ajaxcombobox').ajaxComboBox(
                    '',
                    {
                        lang: 'es',
                        per_page: 10,
                        db_table: "fff",
                        button_img: dir_combo_vineta,
                        init_record: "",
                        bind_to: "change_ajaxcombo",
                        scrollWindow: true
                    }
                );
            }
        });
        $("#div_retencion").show();
    }
    else{
        $("#div_retencion").hide();
        $("#id_fecha_emision_retencion").val($("#id_fecha_asiento").val());
    }
    tabla_det_ret.find('.bootstrap-select').remove();
    $('#detalle_retenciones select[data-name="cod_cta"]').selectpicker();

}

