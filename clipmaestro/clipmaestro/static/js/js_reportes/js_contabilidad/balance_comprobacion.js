
function EliminarFila(e, fila, tableid)
{
    e.preventDefault();
    if($(tableid+" tbody").children().length>1){
        $(fila).parent().parent().remove();
        recalcular_ids(tableid);
    }
    else{
        $("#tit-alert-ok").html("Alerta");
        $("#body-alert-ok").html("<strong>No se puede eliminar</strong>");
        $("#alert-ok").modal();
    }
}

function recalcular_ids(tableid)
{
    var nombre_form = "";
    $(tableid+" tbody tr").each(function(index)
    {
        var cont = index;
        $(this).find(":input:not(:button, .input-block-level)").each(function()
        {
            var name_campo = $(this).attr("name").split("-");
            nombre_form = name_campo[0];
            var nombre_campo = name_campo[0]+"-" + cont + "-"+name_campo[2];
            $(this).attr("name", nombre_campo);
            $(this).attr("id", "id_"+nombre_campo);
        });
    });
    $('#id_'+nombre_form+'-TOTAL_FORMS').val($(tableid+" tbody").children().length);
}

function AgregarProveedor(e)
{
    e.preventDefault();
    var clonar_tr = $("#proveedores tbody tr:first").clone();
    clonar_tr.find(".bootstrap-select").remove();
    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, "#cuenta");
    });
    clonar_tr.find('select').each(function(){
        $(this).find("option").each(function(){
            $(this).removeAttr("selected");
        });
        $(this).find("option:first").attr("selected", "selected");
    });
    clonar_tr.find(".selectpicker").selectpicker('refresh');
    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });

    $("#proveedores tbody").append(clonar_tr);
    recalcular_ids("#proveedores");
}

function saldo_cuenta()
{
    $(".tabla_saldos").each(function(){
        var total_deudor =  0.0;
        var total_acreedor = 0.0;
        var t_saldo_inicial_deudor = 0.0;
        var t_saldo_inicial_acreedor = 0.0;
        var total_saldo_final = 0.0;
        $(this).find(".saldo").each(function()
        {
            var tr = $(this).parent();
            var saldo_acreedor = parseFloat($(tr).find(".saldo_acreedor").text());
            var saldo_deudor = parseFloat($(tr).find(".saldo_deudor").text());
            var saldo_ini = parseFloat($(tr).find(".saldo_inicial").text());
            var saldo_act = $(tr).find(".saldo");
            var naturaleza =  parseInt($(tr).find(".saldo_inicial").attr("data-naturaleza"));

            if(naturaleza == 1)
            {
                saldo_act.text((saldo_ini + saldo_deudor - saldo_acreedor).toFixed(2));
                total_deudor += saldo_deudor;
                total_acreedor += saldo_acreedor;
                t_saldo_inicial_deudor += saldo_ini;
                total_saldo_final += (saldo_ini + saldo_deudor - saldo_acreedor);
            }
            else
            {
                saldo_act.text((saldo_ini + saldo_acreedor - saldo_deudor).toFixed(2));
                total_deudor += saldo_deudor;
                total_acreedor += saldo_acreedor;
                t_saldo_inicial_acreedor += saldo_ini;
                total_saldo_final -= (saldo_ini + saldo_acreedor - saldo_deudor)
            }
        });
        $(this).find(".total_saldo_deudor").text(total_deudor.toFixed(2));
        $(this).find(".total_saldo_acreedor").text(total_acreedor.toFixed(2));
        $(this).find(".total_saldo_inicial").text((t_saldo_inicial_deudor - t_saldo_inicial_acreedor).toFixed(2));
        $(this).find(".total_saldo").text(total_saldo_final.toFixed(2));
    });
}
function show_hide_prov(obj)
{
    if($(obj).is(':checked'))
        $("#tabla_hide").hide();
    else
        $("#tabla_hide").show();
}
