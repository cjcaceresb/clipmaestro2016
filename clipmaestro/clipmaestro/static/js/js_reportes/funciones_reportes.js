/**
 * Created by Clip Maestro on 28/04/2015.
 */

function descargar_reporte(e, formulario, obj)
{
    e.preventDefault();
    formulario.attr('action', formulario.attr("data-action"));

    var href = $(obj).attr("href");

    if($(formulario).attr("action") != href)
    {

        formulario.attr('method', "GET");
        formulario.attr("target", "_blank");
        formulario.attr('action', href).submit();
    }
    else
    {
        formulario.attr('method', "POST");
        formulario.submit();
    }
}