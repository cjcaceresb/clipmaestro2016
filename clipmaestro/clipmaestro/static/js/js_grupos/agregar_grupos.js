function marcar_si_llena_opciones()
{
    $(".div-cont-modulo").each(function()
    {
        var cont = 0;
        var sel_all_mod = $(this).find(".sel_all_modulo");
        $(this).find(".div-cont-aplicacion").each(function(){
            var cont_app = 0;
            var sel_app = $(this).find(".sel_all_aplicacion");
            $(this).find(".checks").each(function(){
                if(!$(this).is(":checked"))
                {
                    cont ++;
                    cont_app ++;
                }
            });
            if(cont_app == 0){
                $(sel_app).attr("checked", true);
            }
        });

        if(cont == 0)
        {
            $(sel_all_mod).attr("checked", true);
        }
    });
}
$(document).ready(function()
{
    marcar_si_llena_opciones();

    $("#sel_all").change(function()
    {
        if($(this).is(":checked"))
        {
            $(':input').each(function()
            {
                var type = this.type || this.tagName.toLowerCase();
                if(type=="checkbox")
                    $(this).prop('checked', true);
            });
        }
        else
        {
            $(':input').each(function()
            {
                var type = this.type || this.tagName.toLowerCase();
                if(type=="checkbox")
                    $(this).attr("checked",false);
            });
        }
    });
    $(".sel_all_modulo").change(function()
    {
        var padre = $(this).closest('.div-cont-modulo');
        if($(this).is(":checked"))
        {
            $(padre).find(".sel_all_aplicacion").each(function(){
                var type = this.type || this.tagName.toLowerCase();
                if(type=="checkbox")
                    $(this).prop('checked', true);
            });
            $(padre).find(".checks").each(function(){
                var type = this.type || this.tagName.toLowerCase();
                if(type=="checkbox")
                    $(this).prop('checked', true);
            });
        }
        else
        {
            $(padre).find(".sel_all_aplicacion").each(function(){
                var type = this.type || this.tagName.toLowerCase();
                if(type=="checkbox")
                    $(this).prop('checked', false);
            });
            $(padre).find(".checks").each(function(){
                var type = this.type || this.tagName.toLowerCase();
                if(type=="checkbox")
                    $(this).prop('checked', false);
            });
        }
    });

    $(".sel_all_aplicacion").change(function()
    {
        var padre = $(this).closest('.div-cont-aplicacion');
        if($(this).is(":checked"))
        {
            $(padre).find(".checks").each(function(){
                var type = this.type || this.tagName.toLowerCase();
                if(type=="checkbox")
                    $(this).prop('checked', true);
            });
        }
        else
        {
            $(padre).find(".checks").each(function(){
                var type = this.type || this.tagName.toLowerCase();
                if(type=="checkbox")
                    $(this).prop('checked', false);
            });
        }
    });

    $('.checks').change(function()
    {
        var sel_all = $("#sel_all");
        var padre = $(this).closest('.div-cont-modulo');
        var padre_app = $(this).closest('.div-cont-aplicacion');
        var check_app = $(padre_app).find('.sel_all_aplicacion');
        var check_modulo = $(padre).find('.sel_all_modulo');

        if(!$(this).is(":checked"))
            var flag = true;

        if(flag)
        {
            sel_all.attr("checked",false);
            $(check_app).attr("checked",false);
            $(check_modulo).attr("checked",false);
        }
    });
});