angular.module('appConversion',['ui.utils.masks','angucomplete-alt'])
    .controller('controllerConversion',function($scope,$http){
        //variables del formulario
        $scope.cabecera = {
            bodega: null,
            secuencia: null,
            fecha: null
        };
        $scope.fila = {
            disabled: true,
            inicio_id: null,
            cantidad_maxima: null,
            inicio_unidad: null,
            inicio_descripcion: null,
            inicio_cantidad: null,
            inicio_unidad_id:null,
            inicio_costo:null,
            factor_conversion:null,
            fin_id: null,
            fin_unidad: null,
            fin_descripcion: null,
            fin_cantidad: null,
            fin_unidad_id:null,
            fin_costo:null
        };
        $scope.validador_item = null;
        $scope.items = [$scope.fila];
        $scope.bodegas = [];
        $scope.mensaje = {
            tipo:"info",
            visible:false,
            texto:null,
            lista:[],
            cantidad:0
        };
        $scope.pass_validacion_cabecera = false;

        $scope.get_datos = function(){
            $http.post(get_datos_conversion, {})
            .success(function (data, status, header, config) {
                if(data.success){
                    $scope.bodegas = data.bodegas;
                    $scope.cabecera.secuencia = data.secuencia;
                    $scope.cabecera.fecha = data.fecha;
                    $('#id_fecha').datepicker('update', data.fecha);
                    $("#loading-conversion").fadeOut();
                }else{
                    $("#loading-conversion").fadeOut();
                    if(data.bodegas == null){
                        $('#body-alert-ok p').text("Ud. no dispone de bodegas. Contáctese con el administrador del sistema.");
                    }else{
                        $('#body-alert-ok p').text("No se puede inicializar el módulo. Contáctese con el administrador del sistema.");
                    }
                    $('#alert-ok').modal('show');
                    $('#alert-ok').on('hidden.bs.modal', function (e) {
                        $("#loading-conversion").fadeIn();
                        window.location=url_lista_inventario;
                    });
                }

            })
            .error(function (data, status, headers, config) {
                $("#loading-conversion").fadeOut();
                $('#body-alert-ok p').text("No tiene acceso a este módulo. Contáctese con el administrador del sistema.");
                $('#alert-ok').modal('show');
                $('#alert-ok').on('hidden.bs.modal', function (e) {
                    $("#loading-conversion").fadeInn();
                    window.location=url_lista_inventario;
                });
            });
        };

        //Metodos
        $scope.init = function(){$scope.get_datos();};

        // $scope.selecionada = null;
        //
        // $scope.fila_seleccionada = function(index){
        //     $scope.selecionada = index;
        // };
        $scope.item_seleccionado1 = function(selected){
            if(selected != undefined) {
                if($scope.cabecera.bodega != null){
                    console.log(selected);
                    $scope.validador_item = null;
                    var indice_fila = this.$parent.$index;
                    var inicio_unidad = selected.originalObject.unidad;
                    var inicio_unidad_id = selected.originalObject.unidad_id;
                    var costo = selected.originalObject.costo_item;
                    $scope.items[indice_fila]['inicio_unidad'] = inicio_unidad;
                    $scope.items[indice_fila]['inicio_descripcion'] = selected.title;
                    $scope.items[indice_fila]['inicio_unidad_id'] = inicio_unidad_id;
                    $scope.items[indice_fila]['inicio_id'] = selected.originalObject.id;
                    $scope.items[indice_fila]['inicio_costo'] = costo;
                    $scope.validador_item = selected.originalObject.id;
                    $scope.items[indice_fila]['disabled'] = false;
                    $('input.cantidad_inicial_'+indice_fila).focus();
                }else{
                    $('#body-alert-ok p').text("Debe de seleccionar una bodega para continuar con el proceso de conversión");
                    $('#alert-ok').modal('show');
                }
            }
        };


        $scope.item_seleccionado2 = function(selected){
            if(selected != undefined) {
                console.log(selected);
                var indice_fila = this.$parent.$index;
                if (selected.originalObject.id != $scope.validador_item) {
                    var fin_unidad = selected.originalObject.unidad;
                    var fin_unidad_id = selected.originalObject.unidad_id;
                    $http.post(get_factor_conversion, {
                        unidad_desde: $scope.items[indice_fila]['inicio_unidad_id'],
                        unidad_hasta: fin_unidad_id
                    })
                        .success(function (data, status, header, config) {
                            if (data.success) {
                                $scope.items[indice_fila]['factor_conversion'] = data.factor_conversion;
                                $scope.items[indice_fila]['fin_unidad'] = fin_unidad;
                                $scope.items[indice_fila]['fin_descripcion'] = selected.title;
                                $scope.items[indice_fila]['fin_unidad_id'] = fin_unidad_id;
                                $scope.items[indice_fila]['fin_id'] = selected.originalObject.id;
                                $scope.items[indice_fila]['fin_cantidad'] = $scope.items[indice_fila]['inicio_cantidad'] / $scope.items[indice_fila]['factor_conversion'];
                                $scope.items[indice_fila]['fin_costo'] = $scope.items[indice_fila]['inicio_costo'] / $scope.items[indice_fila]['factor_conversion'];

                            } else {
                                $('#body-alert-ok p').text("No existe factor de conversión. Contáctese con el administrador del sistema.");
                                $('#id_item').val(indice_fila);
                                $scope.items[indice_fila]['fin_unidad'] = null;
                                $scope.items[indice_fila]['fin_descripcion'] = null;
                                $scope.items[indice_fila]['fin_unidad_id'] = null;
                                $scope.items[indice_fila]['fin_id'] = null;
                                $('#alert-ok').modal('show');
                            }
                        })
                        .error(function (data, status, headers, config) {
                            $('#body-alert-ok p').text("No existe factor de conversión. Contáctese con el administrador del sistema.");
                            $('#id_item').val(indice_fila);
                            $scope.items[indice_fila]['fin_unidad'] = null;
                            $scope.items[indice_fila]['fin_unidad_id'] = null;
                            $scope.items[indice_fila]['fin_id'] = null;
                            $('#alert-ok').modal('show');
                        });


                } else {
                    $('#body-alert-ok p').text("No se puede convertir el mismo item: " + selected.originalObject.label);
                    $('#id_item').val(indice_fila);
                    $scope.items[indice_fila]['fin_unidad'] = null;
                    $scope.items[indice_fila]['fin_unidad_id'] = null;
                    $scope.items[indice_fila]['fin_id'] = null;
                    $('#alert-ok').modal('show');
                }
            }
        };

        $scope.agregar_fila = function(){
            $scope.fila_new = {
                disabled: true,
                cantidad_maxima: null,
                inicio_id: null,
                inicio_unidad: null,
                inicio_descripcion: null,
                inicio_cantidad: null,
                inicio_unidad_id:null,
                inicio_costo:null,
                factor_conversion:null,
                fin_id: null,
                fin_unidad: null,
                fin_descripcion: null,
                fin_cantidad: null,
                fin_unidad_id:null,
                fin_costo:null
            };
            $scope.items.push($scope.fila_new);
        };
        $scope.calcular_conversion = function(indice){
            if($scope.items[indice].fin_id != null){
                $scope.items[indice]['fin_cantidad'] = $scope.items[indice]['inicio_cantidad'] / $scope.items[indice]['factor_conversion'];
                $scope.items[indice]['fin_costo'] = $scope.items[indice]['inicio_costo'] / $scope.items[indice]['factor_conversion'];
            }
        };
        $scope.modificar_cantidad_entrada = function(index){
            if($scope.items[index].inicio_cantidad != null){
                $http.post(url_actualizar_stock_item, {
                    'id_bodega': $scope.cabecera.bodega,
                    'id_item': $scope.items[index].inicio_id,
                    'items': $scope.items,
                    'indice_actual': index
                })
                .success(function (data, status, header, config) {
                        if(data.success) {
                            $scope.items[index].cantidad_maxima = data.cantidad_actual_bodega;
                            if(parseFloat($scope.items[index].inicio_cantidad) <= 0 || $scope.items[index].inicio_cantidad == null){
                                $('#body-alert-ok p').text("Por favor ingrese un número válido");
                                $("#alert-ok").modal('show');
                                // $('#alert-ok').on('hidden.bs.modal', function (e) {
                                //   $('input.inp_cantidad_'+index).focus().select();
                                // });
                                $scope.items[index].inicio_cantidad = 1;
                                $scope.calcular_conversion(index);
                            }else if($scope.items[index].inicio_cantidad <= $scope.items[index].cantidad_maxima){
                                console.log($scope.items[index].cantidad);
                                console.log($scope.items[index].cantidad_maxima);
                                $scope.calcular_conversion(index);
                            }else{
                                $scope.items[index].inicio_cantidad = $scope.items[index].cantidad_maxima;
                                $scope.calcular_conversion(index);
                                $('#body-alert-ok p').text("Ha superado el stock del item. Actual stock: "+$scope.items[index].cantidad_maxima);
                                $("#alert-ok").modal('show');
                                $('#alert-ok').on('hidden.bs.modal', function (e) {
                                    $('input.cantidad_inicial_'+index).focus().select();
                                });
                            }
                        }
                        else{
                            if(data.codigo == 1){
                                $scope.items[index].inicio_cantidad = null;
                                $('#body-alert-ok p').text(data.mensaje);
                                $('#alert-ok').on('hidden.bs.modal', function (e) {
                                    $('input.item1_'+index).focus();
                                    $scope.items[index].disabled = false;
                                    $scope.items[index].cantidad_maxima = null;
                                    $scope.items[index].inicio_cantidad = null;
                                    $scope.items[index].inicio_costo = null;
                                    $scope.items[index].factor_conversion = null;
                                    $scope.items[index].fin_id = null;
                                    $scope.items[index].fin_unidad = null;
                                    $scope.items[index].fin_descripcion = null;
                                    $scope.items[index].fin_cantidad = null;
                                    $scope.items[index].fin_unidad_id = null;
                                    $scope.items[index].fin_costo = null;
                                });
                                $("#alert-ok").modal('show');
                            }else if(data.codigo == 2){
                                $scope.items[index].inicio_cantidad = null;
                                $('#body-alert-ok p').text(data.mensaje);
                                $('#alert-ok').on('hidden.bs.modal', function (e) {
                                    $('input.item1_'+index).focus();
                                });
                                $("#alert-ok").modal('show');
                            }

                        }
                })
                .error(function (data, status, headers, config) {
                    $('#body-alert-ok p').text("Error al seleccionar el item.");
                    $("#alert-ok").modal('show');
                });
            }else{
                // $('#body-alert-ok p').text("Por favor ingrese un número válido");
                // $('#alert-ok').on('hidden.bs.modal', function (e) {
                //     $('input.cantidad_inicial_'+index).focus();
                // });
                // $("#alert-ok").modal('show');
            }
        };
        $scope.eliminar_item = function(index){
            $scope.items.splice(index, 1);
        };
        $scope.validateCabecera = function(){
            $scope.cabecera.fecha = $("#id_fecha").val();
            $("#loading-ptoventa").fadeIn();
            if($scope.cabecera.bodega != null && $scope.cabecera.bodega != ''){
                $http.post(url_validar_cabecera, {'fecha': $scope.cabecera.fecha})
                .success(function (data, status, header, config) {
                    if(data[0].status==1) {
                        $scope.pass_validacion_cabecera = true;
                    }else {
                        $scope.pass_validacion_cabecera = false;
                        $("#id_fecha").val("");
                        $('#id_fecha').datepicker('update', '');
                        $('#body-alert-ok p').html(data[0].descripcion);
                        $("#alert-ok").modal('show');
                    }
                    $("#loading-ptoventa").fadeOut();
                })
                .error(function (data, status, headers, config) {
                    $scope.pass_validacion_cabecera = false;
                    $('#body-alert-ok p').html("Existen problemas de conexión, inténtelo en unos minutos.");
                    $("#alert-ok").modal('show');
                    $("#loading-ptoventa").fadeOut();
                });

            }else{
                $scope.pass_validacion_cabecera = false;
                $('#body-alert-ok p').html("Recuerde seleccionar la bodega");
                $("#alert-ok").modal('show');
                $("#loading-ptoventa").fadeOut();
            }
        };
        $scope.validateDetalle = function(){
            if($scope.items.length > 0){
                /*$("#loading-conversion").fadeIn();
                $http.post(url_validar_detalle, {'id_bodega': $scope.cabecera.bodega, 'items':$scope.items})
                .success(function (data, status, header, config) {
                    if(data.success){
                        $("#loading-conversion").fadeOut();
                    }else{
                        $scope.mensaje.tipo = "danger";
                        $scope.mensaje.visible = true;
                        $scope.mensaje.texto = null;
                        $scope.mensaje.lista = data.lista_mensajes;
                        $scope.mensaje.cantidad = data.lista_mensajes.length;
                        $scope.marcar_errores();
                        $("#loading-conversion").fadeOut();
                        $("#alert-ok #body-alert-ok").html("El detalle contiene errores");
                        $("#alert-ok").modal('show');
                    }
                })
                .error(function (data, status, headers, config) {
                    $scope.pass_validacion_cabecera = false;
                    $('#body-alert-ok p').html("Existen problemas de conexión, inténtelo en unos minutos.");
                    $("#alert-ok").modal('show');
                    $("#loading-conversion").fadeOut();
                });*/
                var item_validos = 0;
                for(var x=0; x<$scope.items.length; x++){
                    if(!$scope.items[x].disabled && ($scope.items[x].factor_conversion != null && $scope.items[x].factor_conversion != "")
                        && ($scope.items[x].fin_cantidad != null && $scope.items[x].fin_cantidad != "")){
                        item_validos += 1;
                    }
                }
                if(item_validos > 0) return true;
                else return false;
            }else{
                return false;
            }
        };
        $scope.validacion_guardar = function(){
            if($scope.cabecera.bodega == null || $scope.cabecera.bodega == ''){
                return 1;
            }else if($scope.cabecera.fecha == null || $scope.cabecera.fecha == '' ){
                return 2;
            }else if(!$scope.validateDetalle()){
                return 3;
            }else{
                return 0;
            }
        };
        $scope.grabar = function(){
            var cod_error = $scope.validacion_guardar();
            if(cod_error == 0){
                $("#loading-conversion").fadeIn();
                $http.post(url_grabar_conversion, {
                    cabecera: $scope.cabecera,
                    items: $scope.items
                })
                .success(function (data, status, header, config) {
                    if(data.success){
                        $("#loading-conversion").fadeOut();
                        $('#body-alert-ok p').text(data.mensaje);
                        $('#alert-ok').on('hidden.bs.modal', function (e) {
                            $("#loading-conversion").fadeIn();
                            window.location=url_lista_inventario;
                        });
                        $('#alert-ok').modal('show');
                    }else{
                        if(data.codigo == 1 ){
                            $('#body-alert-ok p').text(data.mensaje);
                            $("#alert-ok").modal('show');
                            $("#id_fecha").parent().css("border","1px solid red");
                            $("#loading-conversion").fadeOut();
                        }else{
                            $scope.mensaje.tipo = "danger";
                                $scope.mensaje.visible = true;
                                $scope.mensaje.texto = null;
                                $scope.mensaje.lista = data.lista_mensajes;
                                $scope.mensaje.cantidad = data.lista_mensajes.length;
                                $("#loading-conversion").fadeOut();
                                $scope.marcar_errores();
                                $("#alert-ok p").html("El detalle contiene errores");
                                $("#alert-ok").modal('show');
                        }
                    }
                })
                .error(function (data, status, headers, config) {
                });
            }else{
                if(cod_error == 1 ){
                    $("#alert-ok p").html("Debe seleccionar la bodega");
                    $("#alert-ok").modal('show');
                    $("#id_bodega").parent().css("border","1px solid red");
                }
                if(cod_error == 2 ){
                    $("#alert-ok p").html("Debe seleccionar la fecha de emisión");
                    $("#alert-ok").modal('show');
                    $("#id_fecha").parent().css("border","1px solid red");
                }
                if(cod_error == 3 ){
                    $("#alert-ok p").html("Debe de ingresar por lo menos un item válido en el detalle");
                    $("#alert-ok").modal('show');
                }
            }

        };
        $scope.marcar_errores = function(){
            for(var i=0;i<$scope.mensaje.lista.length;i++){
                if($scope.mensaje.lista[i].indice != -1){
                    $("#item_detalle_"+$scope.mensaje.lista[i].indice).addClass("error-tr-table");
                }
            }
        };

    });