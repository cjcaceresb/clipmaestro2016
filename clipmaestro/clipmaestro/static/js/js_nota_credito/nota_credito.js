/**
 * Created by Roberto on 23/12/2014.
 */
$(document).ready(function(e){
     $('.selectpicker').selectpicker();

      /*********************************************
     * Inhabilitar el "enter" en los inputs para no enviar
     * el formulario con un enter
     */
    $('form :input').keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
    });

     $("#id_num_documento_modifica").mask('999-999-999999999');
     $(".eliminar").click(function(e){
        EliminarFila(e,this,ID_TABLA_MANUAL);
     });  // Eliminar para formSet Devolución Manual

     $("input[type='submit']").click(function(){
        $(this).attr("disabled", false);
        $("form").submit(function(){
          $("input[type='submit']").attr("disabled", true).val("Por favor, espere...");
          return true;
        });
     });

     $('#id_fecha').mask("9999-99-99").datepicker({
             todayBtn: true,
             autoclose: true
        }).on('changeDate', function (ev) {
             validar_fecha_actual(this);
            ValidaDocumentoSerieNotaCredito(url_documentos_vigentes_nota_credito);
     });

     $('#id_fecha').focusout(function(){
        ValidaDocumentoSerieNotaCredito(url_documentos_vigentes_nota_credito);
     });

     $("#id_serie").change(function(e){
        SelectSerieNumDevolucion();
     });

     $(".numerico").each(function(){
           $(this).keydown(function(event){
                Numerico(event);
           }).change(function(event){
                redondear(this);
           });
      });

     HideOpciones();

     $("#id_cliente").change(function(){
         flag_post = 0;

         $("#div_detalle_dev_aut").hide();
         $("#div_detalle_dev_man").hide();
         $("#grabar").hide();
         $("#totales").hide();
         clearTotales();

         if($(this).val() != ""){
             SelecNumFacturas();
         }else{
             SelecNumFacturas();
             $("#div_detalle_dev_aut").hide();
             $("#div_detalle_dev_man").hide();
         }
     });


    $("#all_cantidad_man").click(function(){
        SelectAllCantidadCostoManual();
        CalcularSubtotal(ID_TABLA_MANUAL);
    });

    $("#all_costo_man").click(function(){
        SelectAllCantidadCostoManual();
        CalcularSubtotal(ID_TABLA_MANUAL);
    });


    $("#id_num_documentos_venta").change(function(){
        if ($(this).val() == "0"){
            SelectAllCantidadCostoManual();
            $("#div_detalle_dev_man").show();
            $("#tr_documento_modifica").show();
            $("#totales").show();
            $("#grabar").show();
        }

        if($(this).val() != "" && $(this).val() != "0"){
             cargar_detalle_devolucion_venta_a();
             $("#tr_documento_modifica").hide();

        }else{
             $("#div_detalle_dev_aut").hide();
        }
    });


     /*********************************
     *      Devolución Manual
     **********************************/

     $(".item").change(function(){
         ValidateItemsRepetidos(this, ID_TABLA_MANUAL);
         DisabledActInventarioManual(this);
     });

    $(".cant_vendida_m").change(function(){
        var tr = $(this).parent().parent();
        var cantidad_dev = $(tr).find(".cant_dev");

        if ($(cantidad_dev).val() != ""){
            validateCantidadesDevManual(this);
            CalcularTotalesDevolucion(ID_TABLA_MANUAL);
        }

    });

    $(".precio").change(function(){
        var tr = $(this).parent().parent();
        var cantidad_dev = $(tr).find(".cant_dev");

        if ($(cantidad_dev).val() != ""){
            CalcularTotalesDevolucion(ID_TABLA_MANUAL);
        }

    });

    $(".cant_dev_a").change(function(){
        CalcularSubtotal(ID_TABLA_AUTOMATICA);
        //validateCantidadesDevManual(this);
        //CalcularTotalesDevolucion(ID_TABLA_MANUAL);
        //CalcularSubtotal(this);
    });


    /******************************
    * Clona Tabla Detalle Manual
    *******************************/
    $("#add").click(function(e){
       AgregarFilasDevolucionManual(e);
    });

});

