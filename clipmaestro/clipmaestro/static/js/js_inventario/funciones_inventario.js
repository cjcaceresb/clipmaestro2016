/***************************************
 * Oculta todos los divs de formularios
 * @constructor null
 */
function Ocultar_Opciones(){
    $("#div_compra").hide();
    $("#tr_proveedor").hide();
    $("#div_venta").hide();
    $("#div_produccion").hide();
    $("#div_ajuste_cant").hide();
    $("#div_ajuste_costo").hide();
    $("#div_gr_compra").hide();
    $("#tr_num_guia_r").hide();
    $("#tr_num_doc").hide();
    $("#tr_tipo_doc").hide();
    $("#div_gr_venta").hide();
    $("#div_eg_compra").hide();
    $("#div_formset_con_interno").hide();
    $("#div_inventario_inicial").hide();
    /*********** Divs de Info Contrato *********/
        $("#tr_num_contrato").hide();
        $("#div_formset_contrato").hide();

    /********************************************/
}


/***************************************************************************
 * Función Ajax para validar si el Item posee cantidad disponible en Bodega
 * @param obj
 * @param url
 ***************************************************************************/
function cantidadBodegaInventarioC(obj, url) {
    var tr = $(obj).parent().parent();
    var id_item = $(tr).find(".id_item");
    var item = $(tr).find("select.item_contrato");
    var cantidad = $(tr).find(".cantidad_contrato");
    var costo = $(tr).find(".costo");

    if (!isNaN(parseInt($(item).val())))
    {
        $.ajax({
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: {
                'id_item': $(item).val(),
                'cantidad': $(cantidad).val()
            },
            error: function (data) {
                return true;
            },
            success: function (data) {
                var estado = data[0].status;
                switch (estado) {

                    case 0:
                        $("#tit-alert-ok").html("El item no se encuentra registrado en bodega.");
                        $("#body-alert-ok").html("");
                        $("#alert-ok").modal();
                        $(cantidad).val("");
                        $(item).val("");
                        $(costo).val("");
                        $(item).selectpicker("refresh");
                        break;

                    case 1:
                        break; // Status ok
                    case 2:
                        if (parseInt($("#id_tipo").val()) == 2) {
                            $("#tit-alert-ok").html("ALERTA");
                            $("#body-alert-ok").html("La cantidad de item ingresada, no se encuentra disponible en bodega. " +
                                "Existen actualmente: " + data[0].cantidad_bodega + " unidades");
                            $("#alert-ok").modal();
                            $(cantidad).val("");
                        }
                        break;

                    default :
                        break;
                }
            }
        });
    }

}


/**********************************************************************
 * Función para validar el ingreso de Items repetidos
 **********************************************************************/
function ValidateItemsRepetidosContrato(obj, tab_id){
    var array = [];
    var acum = 0;
    var cont = 0;
    var tr = $(obj).parent().parent();
    var item = $(tr).find(".item_contrato");
    var id_item = $(tr).find(".id_item");
    var costo= $(tr).find(".costo");
    var tipo_item = $(tr).find(".tipo_item");

    $(tab_id+" tbody").find("tr").each(function(){
        var item = $(this).find(".item_contrato").val(); // item
        array.push(item);
        cont++;
    });

    if(array.length>1){
        for (var i=0; i<cont; i++){
            for(var j=i+1; j<cont; j++){
                /* Si el item  es igual*/
                if(array[i] != "" && array[j] != "" && array[i] == array[j]){
                    acum++;
                    array.pop();
                }
            }
        }
    }

    if(acum>0){
        $("#tit-alert-ok").html("NO puede ingresar un item repetido");
        $("#body-alert-ok").html("");
        $("#alert-ok").modal();
        $(id_item).val("");
        $(item).val("");
        $(tipo_item).val("");
        $(costo).val("");
    }
}

/************************
 * funcion que te devuelve la unidad de medida de un item
 * @param url
 * @param id
 * @returns {string}
 */
function get_unidad_medida(url, id){
    var unidad = "";
    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: { "id": id },
        error: function (data) {
            return true;
        },
        success: function (data)
        {
            if (data.status == 1){
                unidad = data.unidad_medida;
            }
        }
    });

    return unidad
}

/************************
 * funcion que te devuelve la unidad de medida de un item
 * @param url
 * @param id
 * @returns {string}
 */
function get_costo_item(url, id){
    var costo = "";
    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: { "id": id },
        error: function (data) {
            return true;
        },
        success: function (data)
        {
            if (data.status == 1){
                costo = data.costo;
            }
        }
    });

    return costo
}
/**********************************
 * calcula los datos de los totales de la
 * compra
 * @param url
 * @param form_serialize
 * @returns {undefined}
 */
function get_calculo_totales(url, form_serialize){
    var datos = undefined;
    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: form_serialize,
        error: function (data) {
            return true;
        },
        success: function (data)
        {
            datos = data;
        }
    });

    return datos
}