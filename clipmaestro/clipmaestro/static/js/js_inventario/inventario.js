
var T_DETALLE_ITEM_GR = "#tabla_item_gr";
var T_DETALLE_AJUST_CANT = "#tabla_ajuste_cantidad";
var T_DETALLE_AJUST_COSTO = "#tabla_ajuste_costo";
var T_DETALLE_COMPRA = "#tabla_detalle_compra";
var T_DETALLE_INVENTARIO_INICIAL = "#tabla_detalle_inventario_inicial";
var T_CONSUMO_INTERNO = "#tabla_con_interno";

/*********************** VAR PARA REQUISICIÓN DE CONTRATO *****************/
var T_DETALLE_CONTRATO = "#tabla_item_contrato";

var URLCAntBodega = getURLCantBodegaInventario();

function getDatosProveedor(id_prov, tipo_doc){
    $.ajax(
    {
        url: "/compras/get_datos_proveedor/",
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'id_prov': id_prov,
            'id_tipo_doc': tipo_doc
        },
        error: function(){
            return true;
        },
        success: function(data){
            $("#id_autorizacion").val(data[0].autorizacion);
            $("#id_vence").val(data[0].vencimiento);
        }
    });
}

function AgregarFilasItemContrato(obj){
    var url = $(obj).attr("data-url");
    var clonar_tr = get_data_html(url);
    var id_tabla = $(obj).attr("data-id_tabla");
    var item = clonar_tr.find(".item");
    var costo = clonar_tr.find(".mostrar_costo");
    var costo_total = clonar_tr.find(".mostrar_costo_total");
    var cantidad = clonar_tr.find(".cantidad_contrato");
    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, "#"+id_tabla);
        calcular_totales_produccion();
    });

    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
        if($(this).hasClass("campo_requerido"))
        {
            $(this).removeClass("campo_requerido")
        }
    });
    clonar_tr.find("*").each(function(){
        if($(this).hasClass("campo_requerido"))
        {
            $(this).removeClass("campo_requerido")
        }
    });
    clonar_tr.find("select").each(function(){
       $(this).val($(this).find("option:first").val());
    });

    $(clonar_tr.find(".numerico")).keydown(function(event){
        Numerico(event);
    }).change(function(event){
        redondear(this);
    });

    $(clonar_tr.find(".cantidad_contrato")).change(function(e){
        cantidadBodegaInventarioC(this, URLCAntBodega);
        calcular_totales_produccion();
    });

    clonar_tr.find('.ajaxcombobox').ajaxComboBox(
        '',
        get_data_ajaxcb()
    );
    $(cantidad).change(function(){
        var total = redondeo((cantidad.val()*costo.val()), 2);
        costo_total.val(total.toFixed(2));
        calcular_totales_produccion();
    });
    $(item).on("change_ajaxcombo", function()
        {
            costo.val(get_costo_item(url_get_costo, $(this).attr("value")));
            var total = redondeo((cantidad.val()*costo.val()), 2);
            costo_total.val(total.toFixed(2));
            calcular_totales_produccion();
        });

    $("#"+id_tabla+" tbody").append(clonar_tr);
    recalcular_ids("#"+id_tabla);
}

function AgregarFilasAjustCant(obj)
{
    var url = $(obj).attr("data-url");
    var clonar_tr = get_data_html(url);
    var unidad_medida = clonar_tr.find(".unidad");
    var id_tabla = $(obj).attr("data-id_tabla");
    var item = clonar_tr.find(".item:first");

    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, T_DETALLE_AJUST_CANT);
    });

    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });
    $(clonar_tr.find(".numerico")).keydown(function(event){
        Numerico(event);
    }).change(function(){
        redondear(this);
    });

    clonar_tr.find('.ajaxcombobox').ajaxComboBox(
        '',
        get_data_ajaxcb()
    );

    $(item).ajaxComboBox(
        '',
        get_data_ajaxcb()
    ).on("change_ajaxcombo", function()
        {
            unidad_medida.val(get_unidad_medida(url_get_unidad_medida, $(this).attr("value")));
        });

    //Agrega la fila clonada y recalcula los ids de las filas
    $("#"+id_tabla+" tbody").append(clonar_tr);
    recalcular_ids("#"+id_tabla);
}


function AgregarFilasAjustCosto(obj)
{
    var url = $(obj).attr("data-url");
    if (url == undefined){
        alert("url no identificada");
    }
    else
    {
        var clonar_tr = get_data_html(url);
        var unidad_medida = clonar_tr.find(".unidad");
        var item = clonar_tr.find(".item:first");

        clonar_tr.find(".eliminar").click(function(e)
        {
            EliminarFila(e, this, T_DETALLE_AJUST_COSTO);
        });

        clonar_tr.find("input").each(function( index ) {
            $(this).val("");
            $(this).parent().find(".error").remove();
        });
        $(clonar_tr.find(".numerico")).keydown(function(event){
            Numerico(event);
        }).change(function(){
            redondear(this);
        });

        clonar_tr.find('.ajaxcombobox').ajaxComboBox(
            '',
            get_data_ajaxcb()
        );
        $(item).ajaxComboBox(
            '',
            get_data_ajaxcb()
        ).on("change_ajaxcombo", function()
            {
                unidad_medida.val(get_unidad_medida(url_get_unidad_medida, $(this).attr("value")));
            });

        //Agrega la fila clonada y recalcula los ids de las filas
        $(T_DETALLE_AJUST_COSTO+" tbody").append(clonar_tr);
        recalcular_ids(T_DETALLE_AJUST_COSTO);
    }
}


function AgregarFilasItemGR(obj)
{
    var url = $(obj).attr("data-url");
    var clonar_tr = get_data_html(url);

    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, T_DETALLE_ITEM_GR);
    });

    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });

    $(clonar_tr.find(".numerico")).keydown(function(event){
        Numerico(event);
    }).change(function(){
        redondear(this);
    });

    clonar_tr.find('.ajaxcombobox').ajaxComboBox(
        '',
        get_data_ajaxcb()
    );
    //Agrega la fila clonada y recalcula los ids de las filas
    $(T_DETALLE_ITEM_GR+" tbody").append(clonar_tr);
    recalcular_ids(T_DETALLE_ITEM_GR);
}

function AgregarFilasDetalleCompra(obj)
{
    var url = $(obj).attr("data-url");
    var clonar_tr = get_data_html(url);
    var id_tabla = $(obj).attr("data-id_tabla");

    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, "#"+id_tabla);
        calcular_totales_compra();
    });

    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });
    $(clonar_tr.find(".numerico")).keydown(function(event){
        Numerico(event);
    }).change(function(){
        redondear(this);
    });

    clonar_tr.find(".cantidad").change(function(){
        CalcularPunitario(this)
    });
    clonar_tr.find(".costo").change(function(){
        CalcularPunitario(this);
        calcular_totales_compra();
    });

    clonar_tr.find(".item").bind('change_ajaxcombo', function(){
        calcular_totales_compra();
    });

    clonar_tr.find('.ajaxcombobox').ajaxComboBox(
        '',
        get_data_ajaxcb()
    );

    //Agrega la fila clonada y recalcula los ids de las filas
    $(T_DETALLE_COMPRA+" tbody").append(clonar_tr);
    recalcular_ids(T_DETALLE_COMPRA);
}

function AgregarFilasDetalleInventarioInicial(obj)
{
    var url = $(obj).attr("data-url");
    var clonar_tr = get_data_html(url);
    var id_tabla = $(obj).attr("data-id_tabla");

    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, "#"+id_tabla);
    });
    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });
    $(clonar_tr.find(".numerico")).keydown(function(event){
        Numerico(event);
    }).change(function(){
        redondear(this)
    });

    clonar_tr.find(".cantidad").change(function(){
        CalcularPunitario(this);
        redondear(this);
    });
    clonar_tr.find(".costo").change(function(){
        CalcularPunitario(this);
        redondear(this);
    });

    /*************************************************************
     * Funciones para calcular los totales del costo y el iva
     *************************************************************/
    clonar_tr.find(".costo").change(function(){
        calcular_totales_compra();
    });
    clonar_tr.find(".item").change(function(){
        calcular_totales_compra();
    });
    clonar_tr.find('.ajaxcombobox').ajaxComboBox(
        '',
        get_data_ajaxcb()
    );

    $("#"+id_tabla+" tbody").append(clonar_tr);
    recalcular_ids("#"+id_tabla);
}

/******************************
 * Agrega nueva fila a la tabla de
 * consumo interno
 * @param obj
 * @constructor dd
 */
function AgregarFilasConInterno(obj)
{
    var url = $(obj).attr("data-url");
    var clonar_tr = get_data_html(url);
    var id_tabla = $(obj).attr("data-id_tabla");
    var item = clonar_tr.find(".item");
    var costo = clonar_tr.find(".mostrar_costo");
    var unidad = clonar_tr.find(".unidad");

    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, "#"+id_tabla);
    });
    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
        if($(this).hasClass("campo_requerido"))
            $(this).removeClass("campo_requerido")
    });
    $(clonar_tr.find(".numerico")).keydown(function(event){
        Numerico(event);
    }).change(function(){
        redondear(this);
    });
    clonar_tr.find(".cantidad").val("");
    clonar_tr.find(".costo").val("");

    $(item).on("change_ajaxcombo", function()
    {
        costo.val(get_costo_item(url_get_costo, $(this).attr("value")));
        unidad.val(get_unidad_medida(url_get_unidad_medida, $(this).attr("value")));
    });
    clonar_tr.find('.ajaxcombobox').ajaxComboBox(
        '',
        get_data_ajaxcb()
    );

    $("#"+id_tabla+" tbody").append(clonar_tr);
    recalcular_ids("#"+id_tabla);
}

/*************************************
 * Cambio el motivi de el inventario
 * @param obj aa
 * @constructor
 */
function CambioMotivo(){
    var tipo = $("#id_tipo");
    var opt = 0;
    var tipo_inv = parseInt(tipo.val());

    Ocultar_Opciones();
    $("#grabar").show();

    if (tipo_inv == 1)
        opt = parseInt($("#id_motivo").val());
    else
        opt = parseInt($("#id_motivo_egreso").val());

    $("#id_tipo_doc").val("1");
    switch (opt)
    {
        case 1:  // Compras
            $("#div_compra").fadeIn("slow");
            $("#tr_proveedor").fadeIn("slow");
            $("#tr_num_doc").fadeIn("slow");
            $("#tr_tipo_doc").fadeIn("slow");
            break;
        case 2:  // Ventas inhabilitada
            $("#div_venta").fadeIn("slow");
            break;
        case 3:  // Producción - Transformación
            $("#div_produccion").fadeIn("slow");
            $("#div_formset_contrato").fadeIn("slow");
            $("#tr_num_contrato").fadeIn("slow");
            break;
        case 4:  // Ajuste a la cantidad
            $("#div_ajuste_cant").fadeIn("slow");
            break;
        case 5:  // Ajuste al costo
            $("#div_ajuste_costo").fadeIn("slow");
            break;
        case 6:  // Guía de remisión compra
            $("#div_gr_compra").fadeIn("slow");
            $("#tr_proveedor").fadeIn("slow");
            $("#tr_num_guia_r").fadeIn("slow");
            break;
        case 7:  // Guía de remisión Venta
            $("#div_gr_venta").fadeIn("slow");
            $("#div_gr_compra").fadeIn("slow");
            break;
        case 8:  // Consumo Interno
            $("#div_formset_con_interno").fadeIn("slow");
            break;
        case 9:  // Mantenimiento
            $("#div_ajuste_cant").fadeIn("slow");
            break;
        case 10:  // Garantía
            $("#div_ajuste_cant").fadeIn("slow");
            break;
        case 11:  // Inventario Inicial
            $("#div_inventario_inicial").fadeIn("slow");
            break;
        case 12:
            $("#div_compra").fadeIn("slow");
            $("#tr_proveedor").fadeIn("slow");
            $("#tr_num_doc").fadeIn("slow");
            $("#tr_tipo_doc").fadeIn("slow");
            $("#id_tipo_doc").val("4");
            break;
        default :
            break;
    }

    $("#id_tipo_doc").selectpicker("refresh");
}

function CalcularPunitario(obj){
    var tr = $(obj).parent().parent();
    var cantidad = parseFloat($(tr).find(".cantidad").val());
    var subtotal = parseFloat($(tr).find(".costo").val());
    var p_unitario = $(tr).find(".p_unitario");
    var p_unit = redondeo(subtotal/cantidad, 9).toFixed(9);  // Redondea a 9 decimales

    if(!isNaN(p_unit))
        p_unitario.val(p_unit);
    else
        p_unitario.val("");
}

/*********************
 * Valida el ruc de los proveedores
 * ingresados en el reembolso de gastos
 * @param obj
 * @param tipo_id
 * @returns {boolean}
 */
function validar_ruc(obj, tipo_id){
    var ruc = $(obj);
    if ($(ruc).val() != "" && tipo_id !="")
    {
        if ( tipo_id == "R" )
        {
            if (isRUC($(ruc)) && $(ruc).val().length==13)
            {
                $(obj).removeClass("campo_requerido");
                return true;
            }
            else{
                $(obj).addClass("campo_requerido");
                return false;
            }
        }
        if( tipo_id == "C" )
        {
            if(isCedula(ruc) && $(ruc).val().length==10){
               $(obj).removeClass("campo_requerido");
                return true;
            }else{
                $(obj).addClass("campo_requerido");
                return false;
            }
        }
        if( tipo_id == "P" ){
            return true;
        }
    }
    else{
        return false;
    }
}

function AgregarProveedor(e){
    var id_prov = $("#id_ruc");
    var tipo_id = $("#id_tipo_identificacion").val();
    var form_prov = $("#form_prov");
    if(validar_ruc(id_prov, tipo_id))
    {
        var datastring = form_prov.serialize();
        var url = form_prov.attr("action");
        $.ajax(
        {
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: datastring,
            error: function(){
                return true;
            },
            success: function(data){
                var cont = 0;
                var mensajes = new Array();
                for(var i=0; i<data.length; i++)
                {
                    if(data[i].status != 1)
                    {
                        mensajes.push(data[i].mensaje);
                        cont++;
                    }
                }
                if(cont != 0)
                {
                    var stringhtml = "";
                    for(i=0; i<mensajes.length; i++)
                    {
                        stringhtml = (stringhtml+"<p>"+mensajes[i]+"</p>")
                    }
                    $("#error").fadeIn("slow");
                    $("#error-proveedor").html(stringhtml)
                }
                if(cont == 0)
                {
                    mensaje_alerta("El proveedor se ha creado exitosamente");
                    $('#div-proveedor').modal('hide');
                }
            }
        });
    }
    else
    {
        e.preventDefault();
        alert("Número de identificación no válido, por favor verifique");
    }
}

function seleccionarDirecciones(){
        var combo_direccion = $("#id_direccion");
        var combo_direccion_guia = $("#id_direccion_guia");
        combo_direccion.empty();
        combo_direccion_guia.empty();
        $.ajax(
        {
            url: "/ventas/buscardirecciones/",
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data:{
                'id': $("#id_cliente").val()
            },
            error: function(data){
                return true;
            },
            success: function(data){
                $("#id_direccion").val(data[0].id);
                $("#id_direccion_guia").val(data[0].id);

                var flag = true;
                if(data[0].id == 0)
                {
                    flag=false;
                }
                if(flag){
                    for(var i=0; i<data.length; i++)
                    {
                        combo_direccion.append('<option value="' + data[i].id + '">' + data[i].descripcion +" - "+ data[i].direccion1+ '</option>');
                    }
                     for(var j=0; j<data.length; j++)
                    {
                        combo_direccion_guia.append('<option value="' + data[j].id + '">' + data[j].descripcion +" - "+ data[j].direccion1+ '</option>');
                    }
                }
            }
        });
        combo_direccion.selectpicker("refresh");
        combo_direccion_guia.selectpicker("refresh");
    }
function cambiarIngresoEgreso(obj)
{
    if($(obj).val()=="1")
        {
            $(".tr_motivo_egreso").hide();
            $(".tr_motivo_ingreso").show();
        }
    else
    {
        $(".tr_motivo_egreso").show();
        $(".tr_motivo_ingreso").hide();
    }
    CambioMotivo();
}

/***************
 * Para validar que la fecha que ingresa el usuario no
 * sobrepase la fecha actual
 * @param obj
 */
function validar_fecha_actual(obj)
{
    var date_asiento = new Date(Date.parse($(obj).val()));
    var dia_act_date = new Date(Date.parse(fecha_actual));
    $(obj).val(alerta_validacion_fecha_actual(date_asiento, dia_act_date));
}

/****************************
 * Valida que los usuarios no ingresen una fecha menor a la última
 * fecha de registro de inventario
 *
 * @param obj
 * @returns {null}
 */
function validar_fecha_reg_inv(obj)
{
    var fecha_reg =  $(obj).val();
    var tipo = $("#id_tipo").val();
    $.ajax({
        url: url_validar_fecha_inventario,
        type: "POST",
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'fecha': fecha_reg,
            'tipo': tipo
        },
        success: function( data ){
            if(data.status == "0"){
                mensaje_alerta("La fecha ingresada es menor a la última fecha de " + data.tipo +
                    " de inventario." + "<p>La última fecha es: <b>" + data.fecha + "</b> </p>");
                $(obj).val(data.fecha);
            }
        }
    });

    return null;
}


$(function(){
    recalcular_ids(T_DETALLE_ITEM_GR);
    calcular_totales_compra();
    $("#id_numero_guia_r").mask("999-999-999999999");
    $("#id_num_doc").mask("999-999-999999999");
    $("#id_num").mask("999999999").focusout(function(){AutocompletarCeros(this);});
});

function calcular_totales_compra(){
    var div = $("#div_compra");
    var data_serialize = div.find("input, textarea, select").serialize();
    var datos = get_calculo_totales(url_calcular_totales_compra_inventario, data_serialize);

    var costo_total = datos.costo_total;
    var subtotal_iva = datos.subtotal_iva;
    var total = datos.total;

    $("#costo_total").text(parseFloat(costo_total).toFixed(2));
    $("#sub_total_12").text(parseFloat(subtotal_iva).toFixed(2));
    $("#total").text(parseFloat(parseFloat(total).toFixed(2)));
}

function calcular_totales_produccion(){
    var tabla = $("#tabla_item_contrato");
    var total_costo = 0.0;

    tabla.find("tbody tr").each(function(){
        var costo = $(this).find(".mostrar_costo_total");
        var val_costo = parseFloat($(costo).val());
        total_costo += redondeo(val_costo, 2);

    });
    $("#costo_total_produccion").text("$ "+redondeo(total_costo, 2).toFixed(2));
}

function ValidarFechaGuia(){
    try
    {
        var dia_inicio = Date.parse($("#id_fecha_inicio").val());
        var date_ini = new Date(dia_inicio);

        var dia_final = Date.parse($("#id_fecha_final").val());
        var date_final = new Date(dia_final);
        if(date_ini > date_final)
        {
            $("#tit-alert-ok").html("La fecha final o de entrega de la guía debe de ser mayor igual " +
                "a la fecha de inicio");
            $("#body-alert-ok").html("");
            $("#alert-ok").modal();
            $("#id_fecha_final").val("");
        }
    }
    catch (err)
    {
        alert(err);
    }
}

$(document).ready(function(e)
{
    // Inhabilita el boton submit
    $(".btn_grabar").click(function()
    {
        $('#loading').show();
        $('body').css('display', '0.5');
        $(".btn_grabar").hide();
        $(".btn_cancelar").attr("href", "#").text("Por favor, espere...");
    });

    $("#cerrar").click(function(){
       $("#error").fadeOut("slow");
    });
    $("#add_prov").click(function(){
        $("#error").hide();
        $("#div-proveedor").modal();
    });

    //Inhabilitar el "enter" en los inputs para no enviar el formulario con un "ENTER"
    $('form :input').keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
    });

    $("#send_form_prov").click(function(e){ AgregarProveedor(e) });

    $(".numerico").keydown(function(event) {
        Numerico(event);
    }).change(function(e)
    {
        redondear(this);
    });

    /*************************************************************
     * Funciones para calcular los totales del costo y el iva
     * de compra de inventario
     *************************************************************/

    $("#tabla_detalle_compra tbody tr").find(".item").each(function(){
        $(this).bind('change_ajaxcombo', function(){
            calcular_totales_compra();
        });
    });

    $("#tabla_detalle_compra tbody tr").find(".cantidad").change(function(){
        CalcularPunitario(this)
    });
    $("#tabla_detalle_compra tbody tr").find(".costo").change(function(){
        CalcularPunitario(this);
        calcular_totales_compra();
    });

    $("#id_tipo_identificacion").change(function(){
        if($(this).val() == "P"){
            $(".tipo_identificacion_prov").fadeIn("slow");
        }
        else{
            $(".tipo_identificacion_prov").fadeOut("slow");
        }
    });

    $(".item").on("change_ajaxcombo", function () {
        var tr = $(this).parents("tr:first");
        var unidad_medida = $(tr).find(".unidad");
        var costo = $(tr).find(".mostrar_costo");

        unidad_medida.val(get_unidad_medida(url_get_unidad_medida, $(this).attr("value")));
        costo.val(get_costo_item(url_get_costo, $(this).attr("value")));
    });

    $(T_DETALLE_INVENTARIO_INICIAL + " tbody tr").find(".cantidad").change(function(){
        CalcularPunitario(this)
    });
    $(T_DETALLE_INVENTARIO_INICIAL + " tbody tr").find(".costo").change(function(){
        CalcularPunitario(this);
    });

    /******************************************************
     * Funcion para calcular los totales en producción
     */
    calcular_totales_produccion();
    $("#tabla_item_contrato tbody tr").each(function()
    {
        var cantidad = $(this).find(".cantidad_contrato");
        var costo = $(this).find(".mostrar_costo");
        var costo_total = $(this).find(".mostrar_costo_total");
        var item = $(this).find(".item");

        $(cantidad).change(function(){
            var total = redondeo((cantidad.val()*costo.val()), 2);
            costo_total.val(total.toFixed(2));
            calcular_totales_produccion();
        });

        $(item).on("change_ajaxcombo", function()
        {
            costo.val(get_costo_item(url_get_costo, $(this).attr("value")));
            var total = redondeo((cantidad.val()*costo.val()), 2);
            costo_total.val(total.toFixed(2));
            calcular_totales_produccion();
        });
    });

    $(".cantidad_contrato").change(function(){
        cantidadBodegaInventarioC(this, URLCAntBodega);
    });


    /******************************
     * cambio de opciones de ingreso
     * o egreso
     *******************************/
    cambiarIngresoEgreso($("#id_tipo"));
    $("#id_tipo").change(function(){
        cambiarIngresoEgreso(this)
    });

    /*******************************
     * Dirección de los clientes
     ********************************/
    seleccionarDirecciones();
    $("#id_cliente").change(function(){
        seleccionarDirecciones()
    });


    $("#id_numero_guia_r").focusout(function(){
       AutocompletarCeros(this);
    });

    $("#id_num_doc").focusout(function(){
        AutocompletarCeros(this);
    }).keydown(function(event){
       NumeroEntero(event);
    });

    $( "#id_fecha_reg").keydown(function(event){
        NumeroEntero(event);
    }).mask("9999-99-99");

    /***********************************
     * cambia el motivo del inventario
     ***********************************/
    CambioMotivo();
    $("#id_motivo").change(function(){
        CambioMotivo();
    });

    $("#id_motivo_egreso").change(function(){
        CambioMotivo();
    });


    /************************************
     * Funciones para agregar las filas
     ************************************/
    $("#add_ajuste_cant").click(function(e){
       AgregarFilasAjustCant(this);
    });
    $("#add_ajuste_costo").click(function(e){
       AgregarFilasAjustCosto(this);
   });
    $("#add_item_gr").click(function(e){
       AgregarFilasItemGR(this);
   });
    $("#add_detalle_compra").click(function(e){
        AgregarFilasDetalleCompra(this);
    });
    $("#add_det_contrato").click(function(e){
        AgregarFilasItemContrato(this);
    });
    $("#add_con_interno").click(function(e){
        AgregarFilasConInterno(this);
    });
    $("#add_detalle_inventario_inicial").click(function(e){
        AgregarFilasDetalleInventarioInicial(this);
    });

    /*****************************************
     * Funciones para eliminar las filas
     *****************************************/
    $(T_DETALLE_CONTRATO+" tbody").find(".eliminar").each(function(){
        $(this).click(function(e){
            EliminarFila(e,this,T_DETALLE_CONTRATO);
            calcular_totales_produccion();
        });
    });

    $(T_DETALLE_AJUST_CANT+" tbody").find(".eliminar").each(function(){
        $(this).click(function(e){
            EliminarFila(e,this,T_DETALLE_AJUST_CANT);
        });
    });
    $(T_DETALLE_ITEM_GR+" tbody").find(".eliminar").each(function(){
        $(this).click(function(e){
            EliminarFila(e,this,T_DETALLE_ITEM_GR);
        });
    });
    $(T_DETALLE_COMPRA+" tbody").find(".eliminar").each(function(){
        $(this).click(function(e){
            EliminarFila(e,this,T_DETALLE_COMPRA);
        });
        calcular_totales_compra();
    });
    $(T_CONSUMO_INTERNO+" tbody").find(".eliminar").each(function(){
        $(this).click(function(e){
            EliminarFila(e,this,T_CONSUMO_INTERNO);
        });
    });
    $(T_DETALLE_INVENTARIO_INICIAL + " tbody").find(".eliminar").each(function(){
        $(this).click(function(e){
            EliminarFila(e,this,T_DETALLE_INVENTARIO_INICIAL);
        });
    });

    /*************************************
     * Fechas de los inputs
    **************************************/
    $( "#id_fecha_reg" ).datepicker().on('changeDate', function(ev){
        //validar_fecha_actual(this); //18 agosto, esta cosa me le retrocede un dia pero no entiendo como ni por que, si le comento esto, anda bien :o
        validar_fecha_reg_inv(this);
        $( this ).datepicker("hide")
    });
    $( "#id_fecha_emision" ).datepicker().on('changeDate', function(ev){
        validar_fecha_actual(this);
        $( this ).datepicker("hide")
    });
    $( "#id_fecha_inicio" ).datepicker().on('changeDate', function(ev){
        $( this ).datepicker("hide");
        ValidarFechaGuia();
        validar_fecha_actual(this);
    });
    $( "#id_fecha_final" ).datepicker().on('changeDate', function(ev){
        $( this ).datepicker("hide");
        ValidarFechaGuia();
    });
    $("#id_fecha").datepicker().on('changeDate', function (ev) {
        validar_fecha_actual(this);
    });

    $('.ajaxcombobox').ajaxComboBox(
        '',
        {
            lang: 'es',
            per_page: 10,
            db_table: "fff",
            button_img: dir_combo_vineta,
            init_record: "",
            bind_to: "change_ajaxcombo",
            scrollWindow: true
        }
    );

});

