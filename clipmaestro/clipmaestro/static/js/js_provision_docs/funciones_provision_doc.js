/**
 * Created by Roberto on 23/07/14.
 */

/***
 * Función para Presentar el form provision de compra
 * con o sin retención
 */
function presentarInformacionCompraRet(){
    var id_check_ret = $(".con_retencion");
    var id_check_sin_ret = $(".sin_ret");

    $(id_check_ret).attr("checked", "checked");

    if ($(id_check_ret).is(':checked')){
          $("#id_cantidad_prov").val("");
          $("#id_num_inicio_comp").val("");
          $("#id_num_fin_comp").val("");
          $("#id_num_inicio_doc").val("");
          $("#id_num_fin_doc").val("");

          $("#id_cantidad_prov").removeAttr("placeholder");
          $("#id_cantidad_prov").attr("placeholder", "Ingrese la cantidad a provisionar de Retención. Ej: 5");
          $("#tr_serie_ret").show();
          $("#tr_doc_ret").show();
          $("#label_cantidad_doc").show();
          $("#rango-doc-ret").show();
          $("#label-doc-ret").show();
          $("#label_cantidad_comp").hide();

    }else{

        if ($(id_check_sin_ret).is(':checked')){
            $("#id_cantidad_prov").val("");
            $("#id_num_inicio_comp").val("");
            $("#id_num_fin_comp").val("");
            $("#id_num_inicio_doc").val("");
            $("#id_num_fin_doc").val("");

            $("#id_cantidad_prov").removeAttr("placeholder");
            $("#id_cantidad_prov").attr("placeholder", "Ingrese la cantidad a provisionar de Comprobante. Ej: 5");
            $("#tr_serie_ret").hide();
            $("#tr_doc_ret").hide();
            $("#label_cantidad_doc").hide();
            $("#rango-doc-ret").hide();
            $("#label-doc-ret").hide();
            $("#label_cantidad_comp").show();

        }
    }
}

/***************************************************
* Función que SelectSecuencias
****************************************************/
function SelectSecuencias(url){
    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {"id": $("#id_serie").val()
        },
        error: function(){
        },
        success: function(data)
        {
            if(data.status == 1)
            {
                $("#id_utlimo_doc_emititdo").val(data.secuencia);
                $("#id_utlimo_comp_emititdo").val(data.secuencia_comp);
            }
        }
    });
}

/***************************************************
* Función que SelectSecuencias del Cheque
****************************************************/
function SelectSecuenciaCheque(url) {
    $.ajax(
        {
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: {"id_cta_banco": $("#id_cuenta_banco").val()
            },
            error: function () {
            },
            success: function (data) {
                if (data.status == 1) {
                    $("#id_utlimo_doc_emititdo").val(data.secuencia);
                    $("#id_utlimo_comp_emititdo").val(data.secuencia_comp);
                }
            }
        });

}

function CalcularSeqProvision(){
    var ultim_seq_doc = parseInt($("#id_utlimo_doc_emititdo").val());
    var ultim_seq_comp = parseInt($("#id_utlimo_comp_emititdo").val());
    var cant_doc_prov = $("#id_cantidad_prov");
    var hasta_doc = 0;
    var hasta_comp = 0;
    var desde_doc = ultim_seq_doc + 1;
    var desde_comp = ultim_seq_comp + 1;

    if($(cant_doc_prov).val() !="" && $(cant_doc_prov).val() > 0.0){


        $("#id_num_inicio_doc").val(desde_doc);
        $("#id_num_inicio_comp").val(desde_comp);

        if (parseInt($(cant_doc_prov).val()) != 0){
            hasta_doc = ultim_seq_doc + parseInt($(cant_doc_prov).val());
            hasta_comp = ultim_seq_comp + parseInt($(cant_doc_prov).val());

            $("#id_num_fin_doc").val(hasta_doc);
            $("#id_num_fin_comp").val(hasta_comp);
        }

    }else{

        $(cant_doc_prov).val("");
        $("#id_num_inicio_doc").val("");
        $("#id_num_inicio_comp").val("");
        $("#id_num_fin_doc").val("");
        $("#id_num_fin_comp").val("");
        $("#id_num_fin_doc").val("");
        $("#id_num_fin_comp").val("");
    }

}

/***************************************************************************************
 * Función que valida los documentos y seri vigentes según los documentos de la empresa
 ***************************************************************************************/
function ValidaDocumentoVigencia(url){
   var combo_serie = $("#id_serie");

   if (flag_post == 0){
       combo_serie.empty();
   }


    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'id': $("#id_tipo_documento").val(),
            'fecha': $("#id_fecha").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data){

            if(data[0].status == 0)
            {
                $("#tit-alert-ok").html("No tiene block de documentos vigentes registrados para esta fecha de emisión. Para continuar, " +
                    "dele click en Aceptar e ingrese un block de documento vigente");

                $("#body-alert-ok").html("");
                $("#alert-ok").modal();

                $("#enviar").on("click",function(){
                    var url = $(this).attr("data-href");
                        window.location.href = url;
                });
            }

            if(data[0].status == 1){

                for(var i=0; i<data.length; i++){
                    combo_serie.append('<option value="' + data[i].id + '">' + data[i].serie + '</option>');
                }
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }

    });
    combo_serie.selectpicker("refresh");
}

function ValidaDocumentoVigenciaRetencion(url){
    var combo_serie = $("#id_serie");
    combo_serie.empty();

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            'fecha': $("#id_fecha").val()
        },
         beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data){

            if(data[0].status == 0)
            {
                $("#tit-alert-ok").html("No tiene block de documentos vigentes registrados para esta fecha de emisión. Para continuar, " +
                    "dele click en Aceptar e ingrese un block de documento vigente");

                $("#body-alert-ok").html("");
                $("#alert-ok").modal();

                $("#enviar").on("click",function(){
                    var url = $(this).attr("data-href");
                        window.location.href = url;
                });
            }

            else{

                for(var i=0; i<data.length; i++){
                    combo_serie.append('<option value="' + data[i].id + '">' + data[i].serie + '</option>');
                }
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }

    });
    combo_serie.selectpicker("refresh");
}