//Constantes
var IVA = 0.12;
var numero_result = 0;
var resultado_test=0.0;
var valores=0.0;
var valor_caja_detalle=0;
var calculos=0.0;
var total_haber=0;
var total_debe=0;
var resultado_haber=0;
var resultado_debe=0;
function validar_fecha_em(obj){
    var sfecha_asiento =  $("#id_fecha").val();
    var fecha_comp_asiento = sfecha_asiento.split("-");
    var dia_fecha_asiento = new Date(parseInt(fecha_comp_asiento[0]),parseInt(fecha_comp_asiento[1]),parseInt(fecha_comp_asiento[2]));

    var sfecha =  $(obj).val();
    var fecha_comp = sfecha.split("-");
    var dia_fecha = new Date(parseInt(fecha_comp[0]),parseInt(fecha_comp[1]),parseInt(fecha_comp[2]));

    if(dia_fecha_asiento< dia_fecha){
        $(obj).val("");
        $("#tit-alert").html("Alerta");
        $("#body-alert").html("<strong>NO</strong> puede ingresar una fecha superior a la <strong>fecha del asiento</strong>");
        $("#alert-yn").modal();
    }
    return null;
}
function alerts_eliminar(){
$('<div></div>').appendTo('body')
    .html('<div><h6>No se puede eliminar</h6></div>')
    .dialog({
        modal: true, title: 'Error', zIndex: 10000, autoOpen: true,
        width: 'auto', resizable: false,
        buttons: {
            "ok": function ()
            {
                $(this).dialog("close");
            }
            },
        close: function (event, ui)
        {
            $(this).remove();
        }
    });
}
function AgregarTitulo(e){
    var padre = $(e).parent().get(0);
    var boton_combo = $(padre).children()[1];
    var titulo = "";
    var div_texto = boton_combo.firstChild.firstChild;
    var codigo = "";
    $(e).find("option").each(function(){
      if(this.selected)
      {
          titulo = $(this).text();
      }
    });
    $(boton_combo).tooltip('hide')
      .attr('data-original-title', titulo)
      .tooltip('fixTitle')
      //.tooltip('show');
      codigo = titulo.split("-")[0];

    $(div_texto).change(function(){
        $(this).text(codigo);
    });
}
function isCedula(obj){
    var number = $(obj).val();
    var residuo=0;
    var respuesta=0;
    var suma_cedula=0;
    var result=0;
    var divi= number.substring(0,9);
    var porcion1 = number.substring(2,3);
    var rdivi3 = number.substring(9,10);//Digito a verificar cedula
    var contador=0;

        if(porcion1<6 && number.length == 10 && number.substring(0,2)<=24)
        {
            var tmp=0;
            var test=0;
                 for(var y=0; y<divi.length; y++){
                     if(y%2==0){
                        tmp=divi[y]*2;

                         if(tmp<=9){
                            suma_cedula= suma_cedula+tmp;
                         }
                         else{
                                result = tmp-9;
                                suma_cedula = suma_cedula+result;
                         }
                     }
                     else{
                            test= divi[y]*1;
                            suma_cedula=suma_cedula+test;
                     }
                 }

                 residuo = suma_cedula % 10;
                 if(residuo==0){
                     respuesta=0;
                     if(respuesta == rdivi3 || respuesta == 0){
                         contador++;
                     }
                 }
                 else{
                      respuesta = parseInt(10-residuo);
                      if(respuesta == rdivi3 || respuesta == 0){
                          contador++;
                      }
                 }
        }

        if(contador==0){
            return false;
        }
        else{
            return true;
        }
}
function isPersonaJuridica(obj){
  var number = obj;
  var mruc = [4,3,2,7,6,5,4,3,2];
  var suma_total = 0;
  var residuo=0;
  var respuesta=0;
  var divi= number.substring(0,9);
  var porcion1 = number.substring(2,3);
  var rdivi2 = number.substring(9,10);// Digito a verificar ruc
  var contador=0;

    if(porcion1==9 && number.substring(10,13)==001  && number.substring(0,2)<=24){

        for(var k=0; k<divi.length; k++){
           suma_total = suma_total + (mruc[k]*parseInt(divi[k]));
           residuo = suma_total % 11;
           respuesta = parseInt(11-residuo);
        }
            if (rdivi2 == respuesta || residuo==0){
                contador++;
            }
    }
    if(contador==0)
        return false;
    else
        return true;

}
function isIntitucionPublica(obj){
  var number = obj;
  var nruc_empresa = [3,2,7,6,5,4,3,2 ];
  var suma_total = 0;
  var residuo=0;
  var respuesta=0;
  var digiempresa = number.substring(0,8);
  var porcion1 = number.substring(2,3);

  var rdivi1 = number.substring(8,9);// Digito a verificar empresa pública
  var contador=0;

      if(porcion1==6 && number.substring(9,13)==0001 && number.substring(0,2)<=24)
      {
          for(var k=0; k<digiempresa.length; k++)
          {
               suma_total = suma_total + (nruc_empresa[k]*parseInt(digiempresa[k]));
               residuo = suma_total % 11;
               respuesta = parseInt(11-residuo);
          }
            if (rdivi1 == respuesta || residuo==0){
                contador++;
            }
      }

      if(contador==0)

        return false;
      else
        return true;

}
function isPersonaNatural(obj){
    var number = obj;
    var residuo=0;
    var respuesta=0;
    var suma_cedula=0;
    var result=0;
    var divi= number.substring(0,9);
    var rdivi3 = number.substring(9,10);//Digito a verificar cedula
    var porcion1 = number.substring(2,3);
    var contador=0;
        if(porcion1<6 && number.substring(10,13)==001 && number.substring(0,2)<=24)
        {
            var tmp=0;
            var test=0;
                 for(var y=0; y<divi.length; y++){
                     if(y%2==0){
                        tmp=divi[y]*2;

                         if(tmp<=9){
                            suma_cedula= suma_cedula+tmp;
                         }
                         else{
                                result = tmp-9;
                                suma_cedula = suma_cedula+result;
                         }
                     }
                     else{
                            test= divi[y]*1;
                            suma_cedula=suma_cedula+test;
                     }
                 }

                 residuo = suma_cedula % 10;
                 if(residuo==0){
                     respuesta=0;
                     if(respuesta == rdivi3 || respuesta == 0){
                         contador++;
                     }
                 }
                 else{
                      respuesta = parseInt(10-residuo);
                      if(respuesta == rdivi3 || respuesta == 0){
                          contador++;
                      }
                 }
        }

        if(contador==0){
            return false;
        }
        else{
            return true;
        }
}
function isRUC(obj){
    var number = $(obj).val();
    var cont=0;
    if (isPersonaJuridica(number) ){
        cont++;
    }
    if(isIntitucionPublica(number)){
        cont++;
    }
     if(isPersonaNatural(number)){
        cont++;
    }
    if(cont==0)
        return false;
    else
        return true;
}
// Funcion para validar ruc
function validar_ruc(tipo_ident, ruc){
    var titulo2 = "El Núm. de identificación es incorrecto";
    $(ruc).popover('destroy');
    if( $(ruc).val()!=""){
        if( $(tipo_ident.firstChild).val()=="")
        {
            var titulo3 = "Escoja un tipo de Identificación";
            $(ruc).popover({title: titulo3,placement: 'bottom'}).popover('show');
        }
        if($(tipo_ident.firstChild).val()=="R")
        {
            if($(ruc).val().length==13)
            {
                if(isRUC(ruc)){
                $(ruc).popover('destroy');
                }
                else{
                    $(ruc).popover({title: titulo2,placement: 'bottom'}).popover('show');
                }
            }
            else{
                $(ruc).popover({title: titulo2,placement: 'bottom'}).popover('show');
            }
        }
        if($(tipo_ident.firstChild).val()=="C")
        {
            if($(ruc).val().length==10)
            {
                if(isCedula(ruc)){
                    $(ruc).popover('destroy');
                }
                else{
                    $(ruc).popover({title: titulo2,placement: 'bottom'}).popover('show');
                }
            }
            else{
                $(ruc).popover({title: titulo2,placement: 'bottom'}).popover('show');
            }
        }
    }
}
//Calcular los Valores
function calcularValores(){
    var total_tarifa_doce = 0.0;
    var total_tarifa_cero = 0.0;
    var total_pagar = 0.0;
    var total_iva = 0.0;
    var valor = 0.0;

    $("#tabla_formset2 tbody tr").each(function(){
        var valor = $(this).children()[12].firstChild;
        var iva_select = $(this).find('select[data-name="combo_porc_iva"]');
        var iva = 0.0;
        iva = parseFloat($(iva_select).val().split("-")[1]);

        if($($(this).find(".valor")).val()!=""){
            valor = parseFloat($($(this).find(".valor")).val());
            if(iva==0){
                total_tarifa_cero = total_tarifa_cero + valor;
            }
            else{
                if (iva==12){
                total_tarifa_doce = total_tarifa_doce + valor;
                }
            }
        }

    });

    total_iva = total_tarifa_doce * IVA;
    total_pagar = (total_tarifa_cero + total_tarifa_doce + total_iva).toFixed(2);

    $("#total_iva_0").text(total_tarifa_cero.toFixed(2));
    $("#total_iva_12").text(total_tarifa_doce.toFixed(2));
    $("#total_iva").text(total_iva.toFixed(2));
    $("#total_pagar").text(total_pagar);
}
// Funcion para obtener los values de las Fechas al cargar la pàgina
function fechasDefault(){

    $("#tabla_formset2 tr ").each(function(){
        var fecha_emision = $(this).find(".fecha_emision");
        var fecha_vencimiento = $(this).find(".fecha_vencimiento");

        $(fecha_emision).val($(fecha_emision).attr("value"));
        $(fecha_vencimiento).val($(fecha_vencimiento).attr("value"));
    })

}
//Función para habilitar y deshabilitar cto_gasto y valor
function HabilitaCostoGasto(){
    $("#tabla_formset2 tr ").each(function(){
        var cod_iva = $(this).find(".cod_porc_iva").val();
        var cto_gto = $(this).find(".cto_gto");
        var valor_base = $(this).find(".valor");

       if($(cto_gto).val() == 1){

       if (parseFloat(cod_iva.split("-")[1]) == 12){
         $(cto_gto).attr("disabled", false );
         $(valor_base).attr("disabled",false);
       }

       else{

            $(cto_gto).attr("disabled", true);
            $(valor_base).attr("disabled",true);
            $("#alert-yn").attr("data-id", $(this).attr("id"));
            $("#alert-yn").attr("data-target",2);
            $("#tit-alert").html("Alerta");
            $("#body-alert").html('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Por Favor ingrese un porcentaje de iva acorde al Costo.</span></strong></p>');
            $("#alert-yn").modal();
       }

      }

    });
}
/********************************************
 * Funcion para verificar que no digiten
 * documentos repetidos
 */
function verificar_seri_rep(){
    //preguntar si array.lenght >1 si no false
    var array = [];
    var prov = [];
    var serie = [];
    var tipo_doc = [];
    var acum = 0;
    var cont = 0;
    array.push(prov);
    array.push(serie);
    array.push(tipo_doc);

    $("#tabla_formset2 tbody").find("tr").each(function()
    {
        array[0][cont] = $(this).find(".proveedor").val();
        array[1][cont] = $(this).find(".serie").val();
        array[2][cont] = $(this).find(".tipo_documento").val();
        cont++;
    });
    if(array.length>1)
    {
        for (var i=0; i<cont; i++){
            for(var j=i+1; j<cont; j++){
                /* Si el numero de documento es igual y el proveedor es el mismo
                   y si es el mismo número de documento, entonces error esta ingresando dos veces el
                   mismo documento
                 */
                if(array[1][i] != "" && array[1][j] != "" && array[1][i] == array[1][j] && array[0][i] == array[0][j] && array[2][i] == array[2][j]){
                    acum++;
                }
            }
        }
        if(acum>0){
            $("#tit-alert").html("Alerta");
            $("#body-alert").html("<strong>No</strong>puede ingresar el mismo documento <strong>dos veces</strong> del mismo proveedor. Por favor verifique");
            $("#alert-yn").modal();
        }
        else{

        }
    }
}
/*
function pagoLocalExterior(){

    $("#tabla_formset2 tr ").each(function(){
        var local_ext = $(this).find(".pago");
        var pais = $(this).find(".pais");
        var conv_db_trib = $(this).find(".centrar-cdt");
        var suje_ret = $(this).find(".centrar-sr");

        $(pais).attr("disabled",true);

        if($(local_ext).val()!=""){
            if ($(local_ext).val()==1){
                $(conv_db_trib).attr("disabled",true);
                $(suje_ret).attr("disabled",true);
            }
            if ($(local_ext).val()==2){
                $(pais).attr("disabled",false);
                $(conv_db_trib).attr("disabled",false);
                $(suje_ret).attr("disabled",false);
            }
        }
    })
}
*/
/*
function habilitacionFormaPago(){

  $("#tabla_formset2 tbody tr").each(function(){
    var valor = $(this).children()[12].firstChild;
    var forma_pago = $(this).children()[13].firstChild;
    calculos = parseFloat($(valor).val()) * 0.12 ;

    if ($(valor).val()!=""){
        valor_caja_detalle= parseFloat($(valor).val()) + calculos ;
        resultado_test = parseFloat(valor_caja_detalle.toFixed(2));
        valores= resultado_test;
    }
    if (resultado_test > 1000){
        $(forma_pago).attr("disabled",false);
    }
    else{
        $(forma_pago).attr("disabled",true);
    }
  });
    $(".id_info").selectpicker("refresh");
}
*/

//LLamado de validar RUC proveedor y la validación de paìs Local-Exterior - tooltip numero autorizacion
$(document).ready(function(){
    $('.cuenta').focusout(function(){
         var info= $(this).val();
        $(this).tooltip({title:info}).tooltip('show');
    });
    $('.serie').mask('999-999-999999999');
    $('.selectpicker').selectpicker();
    $(".proveedor").focusout(function(){
        var td = $($(this).parent().parent().get(0)).children()[2];
        validar_ruc(td, this);
    });
    $("select[data-name='combo_tipo_id']").change(function(){
        var td = $($(this).parent().parent().get(0)).children()[2];
        var ruc = $($(this).parent().parent().get(0)).children()[3].firstChild;
        validar_ruc(td, ruc);
    });
    $(".cod_porc_iva").change(function(){
        calcularValores();
        HabilitaCostoGasto();
    });
    $(".cto_gto").change(function(){
        calcularValores();
        HabilitaCostoGasto();
    });
    $(".num_autorizacion ").focusout(function(){
        var info= "Núm: "+ $(this).val();
        $(this).tooltip({title:info}).tooltip('show');
    });
    /*********************************************
     * Inhabilitar el "enter" en los inputs para no enviar
     * el formulario con un enter
     */
        $('form :input').keypress(function(e){
            if ( e.which == 13 ) e.preventDefault();
        });
    /************************************************************/
    $(".numerico").keydown(function(event) {
        if ( $.inArray(event.keyCode,[46,8,9,27,13,110,190]) !== -1 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });
     function log( message, e ) {
    };

    /***** Autocompletado ******/
    $( ".cuenta" ).autocomplete({
        source: "/cajachica/buscar_cuenta/",
        data:{tag:$(this).val()},
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            var padres = jQuery(event.target).parent().parent();
            var padre = padres[0];
            var padre_detalle_cta = padre.children[1];
            var cuenta = $(padre_detalle_cta.firstElementChild);
            var cuenta_string = ui.item.value.split("-")[1]
            $(this).val(cuenta_string);
            cuenta.val(ui.item.key);
            }
    });

    $(".serie").focusout(function(){
        var valor = $(this).val();
        var titulo = "numero de serie es inválido";
        var valor_final = valor.replace(/_/g , "");
        var separador = valor_final.split("-");
        $(this).popover('destroy');
        if(parseInt(valor_final.split("-")[0])==0 || parseInt(valor_final.split("-")[1])==0){
            $(this).popover({title: titulo,placement: 'bottom'}).popover('show');
            $(this).val("");
        }else{
             $(this).popover('destroy');
            var a= ("00000000"+separador[2]);
            $(this).val(separador[0]+"-"+separador[1]+"-"+a.slice(-9));
        }
        //verificar_seri_rep();
    });
     $(".valor").focusout(function(){
        calcularValores();
     });

 });
//Efectos de Combo
$(function(){
    $("#tabla_formset2").find(".selectpicker").change(function(){AgregarTitulo(this)});

    $("#tabla_formset2").find("select[data-name='combo_tipo_id']").find("option").each(function(){
        $(this).attr("title",$(this).text().split("-")[0]);
    });

    $("#tabla_formset2").find("select[data-name='combo_porc_iva']").find("option").each(function(){
        $(this).attr("title",$(this).text().split("-")[0]);
    });

    $("#tabla_formset2").find("select[data-name='combo_pago']").find("option").each(function(){
        $(this).attr("title",$(this).attr("value").split("-")[0]);
    });

    $("#tabla_formset2").find("select[data-name='combo_tipo_doc']").find("option").each(function(){
        $(this).attr("title",$(this).text().split("-")[0]);
    });

    $("#tabla_formset2").find("select[data-name='combo_sust_trib']").find("option").each(function(){
        $(this).attr("title",$(this).text().split("-")[0]);
    });

     $("#tabla_formset").find("select").change(function(){
        //$('#collapseOne').collapse("hide");
         AgregarTitulo(this)});

     $("#tabla_formset").find(".serie ").focusout(function(){
            var valor = $(this).val();
            var valor_final = valor.replace(/_/g , "");
            var separador = valor_final.split("-");
            var a= ("00000000"+separador[2]);
            $(this).val(separador[0]+"-"+separador[1]+"-"+a.slice(-9));
            verificar_seri_rep();
     });
});

/***********************************************************************************************************************/

//Clonar Detalle compra
$(document).ready(function(){
    function recalcular_ids2(){
        $("#tabla_formset2 tbody tr").each(function(index)
        {
            var cont = index;
            cont_ultimo = $("#tabla_formset2 tbody tr").length;

            $(this).find("td").each(function( index )
            {
                var hijo = $(this).children()[0];
                if (hijo.tagName == "INPUT" || hijo.tagName == "SELECT")
                {
                    var name_campo = $(hijo).attr("name").split("-");
                    name_campo[1] = cont;
                    var nombre_campo = name_campo[0]+"-"+name_campo[1]+"-"+name_campo[2];
                    $(hijo).attr("name", nombre_campo);
                    $(hijo).attr("id", "id_"+nombre_campo);
                    if (hijo.tagName == "SELECT")
                    {
                        $(hijo).selectpicker('refresh');
                        if($(this).children()[2]){
                            $(this).children()[2].remove();
                        };
                    }
                }
            });
        });
    };
    $("#add2").click(function(e)
    {
        e.preventDefault();
        var clonar_tr2 = $("#tabla_formset2 tbody tr:first").clone();
        clonar_tr2.find(".eliminar").click(function(e){
            e.preventDefault();
            if($("#tabla_formset2 tbody").children().length>1){
                $(this).parent().parent().remove();
                recalcular_ids2();
                calcularValores();
                $("#id_detalle_compra_caja-TOTAL_FORMS").val($("#tabla_formset2 tbody").children().length);
            }
            else{
                alerts_eliminar();
            }
        });
        $(clonar_tr2.find('.cod_porc_iva')).attr("disabled", false);
        $(clonar_tr2.find('.valor')).attr("disabled", false);
        $(clonar_tr2.find('.cto_gto')).attr("disabled", false);
        $(clonar_tr2.find(".sustento_tributario")).change(function(){
            var i = parseInt($(this).prop('selectedIndex'));
            var select = $(clonar_tr2.find($('select[data-name="combo_tipo_doc"]')));
            var tipo_id =  $(clonar_tr2.find(".tipo_identificacion")).val();
            var result=0;

                if(tipo_id=="R"){
                result=0;
                }
                if(tipo_id=="C" || $(tipo_id).val()=="P" ){
                result=1;
                }
               // cambiar_combo(select, i,result);

        });
        $(clonar_tr2.find('.cuenta')).focusout(function(){
                 var info= $(this).val();
                $(this).tooltip({title:info}).tooltip('show');
        });
        /***** Autocompletado ******/
        $(clonar_tr2.find( ".cuenta" )).autocomplete({
                source: "/cajachica/buscar_cuenta/",
                data:{tag:$(this).val()},
                minLength: 2,
                select: function( event, ui ) {
                    event.preventDefault();
                    var padres = jQuery(event.target).parent().parent();
                    var padre = padres[0];
                    var padre_detalle_cta = padre.children[1];
                    var cuenta = $(padre_detalle_cta.firstElementChild);
                    var cuenta_string = ui.item.value.split("-")[1]
                    $(this).val(cuenta_string);
                    cuenta.val(ui.item.key);
                    }
            });
        $(clonar_tr2.find(".tipo_identificacion")).val("");
        clonar_tr2.find(".serie").mask('999-999-999999999');
        clonar_tr2.find(".serie").focusout(function(){
            var valor = $(this).val();
            var titulo = "El número de serie es inválido";
            var valor_final = valor.replace(/_/g , "");
            var separador = valor_final.split("-");
            $(this).popover('destroy');
            if(parseInt(valor_final.split("-")[0])==0 || parseInt(valor_final.split("-")[1])==0){
                $(this).popover({title: titulo,placement: 'bottom'}).popover('show');
                $(this).val("");
            }else{
                 $(this).popover('destroy');
                var a= ("00000000"+separador[2]);
                $(this).val(separador[0]+"-"+separador[1]+"-"+a.slice(-9));
            }
            verificar_seri_rep();
        });
        clonar_tr2.find("input").each(function( index ) {
            $(this).val("");
            $(this).parent().find(".error").remove();
        });
        $(clonar_tr2.find(".selectpicker")).change(function(){
            AgregarTitulo(this);
        });

        $(clonar_tr2.find(".hasDatepicker")).each(function(){
            $(this).focusin(function(){
                $(this).removeClass('hasDatepicker'); // remove hasDatepicker class
                $(this).datepicker();
                $(this).datepicker("option", "dateFormat","yy-mm-dd");
            });
         });

        $(clonar_tr2.find(".cod_porc_iva")).change(function(){
            calcularValores();
            HabilitaCostoGasto();

        });

        $(clonar_tr2.find(".cto_gto")).change(function(){
            calcularValores();
            HabilitaCostoGasto();
        });

        //validación de fechas de emisión en clonación
        $(clonar_tr2.find(".fecha_emision")).datepicker().on('changeDate', function(ev){
            validar_fecha_em(this);
        });
        //validación de fechas de vencimiento en clonación
        $(clonar_tr2.find(".fecha_vencimiento")).datepicker().on('changeDate', function(ev){
            var stringfecha_emision =  $(clonar_tr2.find(".fecha_emision")).val();
            var fecha_comp_emision = stringfecha_emision.split("-");
            var dia_fecha_emision = new Date(parseInt(fecha_comp_emision[0]),parseInt(fecha_comp_emision[1]),parseInt(fecha_comp_emision[2]));

            var stringfecha_vencimiento =  $(this).val();
            var fecha_comp = stringfecha_vencimiento.split("-");
            var dia_fecha_vencimiento = new Date(parseInt(fecha_comp[0]),parseInt(fecha_comp[1]),parseInt(fecha_comp[2]));

            if(dia_fecha_vencimiento < dia_fecha_emision){
                $("#tit-alert").html("Alerta");
                $("#body-alert").html("<strong>NO</strong> puede ingresar una fecha inferior a la <strong>fecha de emisión</strong>");
                $("#alert-yn").modal();
                $(this).val("");
            }
        });

        //LLAmado de funcion para validar proveedor RUC - CEDULA
        $($($(clonar_tr2).find(".proveedor").parent().parent().get(0)).children()[2].children[1]).remove(); // Elimina el popover que se crea al clonar

        $(clonar_tr2).find(".proveedor").focusout(function(){
            var td = $($(this).parent().parent().get(0)).children()[2];
            validar_ruc(td, this);
        });
        $(clonar_tr2).find("select[data-name='combo_tipo_id']").change(function(){
            var td = $($(this).parent().parent().get(0)).children()[2];
            var ruc = $($(this).parent().parent().get(0)).children()[3].firstChild;
            validar_ruc(td, ruc);

        });
        $(clonar_tr2.find(".num_autorizacion")).focusout(function(){
            var info= "No. "+$(this).val();
            $(this).tooltip({title:info}).tooltip('show');

        });

        $(clonar_tr2.find(".valor")).focusout(function(){
            calcularValores();
        });

        $("#tabla_formset2 tbody").append(clonar_tr2);
        recalcular_ids2();
        $("#id_detalle_compra_caja-TOTAL_FORMS").val($("#tabla_formset2 tbody").children().length);
    });
    $(".eliminar").click(function(e){
        e.preventDefault();
        if($("#tabla_formset2 tbody").children().length>1){
            $(this).parent().parent().remove();
            recalcular_ids2();
            calcularValores();
            $("id_detalle_compra_caja-TOTAL_FORMS").val($("#tabla_formset tbody").children().length);
        }
        else{
           alerts_eliminar();
        }
    });
    $("#tabla_formset2").find("select").each(function(){AgregarTitulo(this)});
});