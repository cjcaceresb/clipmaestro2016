
/**********************************************************
 * Funcion para validar el Ruc o Cedula del Proveedor
 * @param obj , id
 * @constructor
 */


/************************************"**********************/
function isCedula(obj){
    var number = $(obj).val();
    var residuo = 0;
    var respuesta = 0;
    var suma_cedula = 0;
    var result = 0;
    var divi = number.substring(0,9);
    var porcion1 = parseInt(number.substring(2,3));
    var rdivi3 = parseInt(number.substring(9,10));//Digito a verificar cedula
    var contador = 0;

    if(porcion1 < 6 && number.length == 10 && parseInt(number.substring(0,2)) <= 24)
    {
        var tmp=0;
        var test=0;
             for(var y=0; y < divi.length; y++){
                 if( y % 2 == 0){
                    tmp = parseInt(divi[y])*2;

                     if(tmp <= 9){
                        suma_cedula = suma_cedula+tmp;
                     }
                     else{
                            result = tmp-9;
                            suma_cedula = suma_cedula+result;
                     }
                 }
                 else{
                        test = parseInt(divi[y]);
                        suma_cedula = suma_cedula+test;
                 }
             }

             residuo = suma_cedula % 10;
             if(residuo == 0){
                 respuesta = 0;
                 if(respuesta == rdivi3 || respuesta == 0){
                     contador++;
                 }
             }
             else{
                  respuesta = parseInt( 10 - residuo );
                  if(respuesta == rdivi3 || respuesta == 0){
                      contador++;
                  }
             }
    }
    if(contador==0){
        return false;
    }
    else{
        return true;
    }
}
/******************************
 * Valida si es persona juridica
 * @param obj
 * @returns {boolean}
 */
function isPersonaJuridica(obj){
  var number = obj.val();
  var mruc = [4,3,2,7,6,5,4,3,2];
  var suma_total = 0;
  var residuo = 0;
  var respuesta = 0;
  var divi = number.substring(0,9);
  var porcion1 = parseInt(number.substring(2,3));
  var rdivi2 = parseInt(number.substring(9,10));// Digito a verificar ruc
  var contador = 0;

    if(porcion1==9 && number.substring(10,13) == "001"  && parseInt(number.substring(0,2)) <= 24){
        for(var k=0; k<divi.length; k++){
           suma_total = suma_total + (mruc[k]*parseInt(divi[k]));
           residuo = suma_total % 11;
           respuesta = parseInt(11-residuo);
        }
            if (rdivi2 == respuesta || residuo==0){
                contador++;
            }
    }
    if(contador==0)
        return false;
    else
        return true;

}
/**************************************
 * Verifica si es Institución Publica
 * @param obj
 * @returns {boolean}
 */
function isIntitucionPublica(obj){
  var number = obj.val();
  var nruc_empresa = [3,2,7,6,5,4,3,2 ];
  var suma_total = 0;
  var residuo=0;
  var respuesta=0;
  var digiempresa = number.substring(0,8);
  var porcion1 = parseInt(number.substring(2,3));
  var rdivi1 = parseInt(number.substring(8,9));// Digito a verificar empresa pública
  var contador = 0;

      if(porcion1 == 6 && number.substring(9,13) == "0001" && parseInt(number.substring(0,2)) <= 24)
      {
          for(var k=0; k<digiempresa.length; k++)
          {
               suma_total = suma_total + (nruc_empresa[k]*parseInt(digiempresa[k]));
               residuo = suma_total % 11;
               respuesta = parseInt( 11 - residuo );
          }
            if ( rdivi1 == respuesta || residuo == 0 ){
                contador++;
            }
      }

      if(contador==0)
        return false;
      else
        return true;

}
/********************************
 * Verifica si es Persona Natural
 * @param obj
 * @returns {boolean}
 */
function isPersonaNatural(obj){
    var number = obj.val();
    var residuo=0;
    var respuesta=0;
    var suma_cedula=0;
    var result=0;
    var divi= number.substring(0,9);
    var rdivi3 = parseInt(number.substring(9,10));//Digito a verificar cedula
    var porcion1 = parseInt(number.substring(2,3));
    var contador = 0;

        if(porcion1 < 6 && number.substring(10,13) == "001" && parseInt(number.substring(0,2)) <= 24)
        {
            var tmp=0;
            var test=0;
                 for(var y=0; y < divi.length; y++){
                     if( y%2 == 0 ){
                        tmp = divi[y] * 2;
                         if(tmp<=9){
                            suma_cedula = suma_cedula + tmp;
                         }
                         else{
                                result = tmp - 9;
                                suma_cedula = suma_cedula + result;
                         }
                     }
                     else{
                            test= parseInt(divi[y]);
                            suma_cedula = suma_cedula + test;
                     }
                 }

                 residuo = suma_cedula % 10;
                 if( residuo == 0 ){
                     respuesta = 0;
                     if(respuesta == rdivi3){
                         contador++;
                     }
                 }
                 else{
                      respuesta = 10 - residuo;
                      if(respuesta == rdivi3 || respuesta == 0){
                          contador++;
                      }
                 }
        }
        if(contador == 0){
            return false;
        }
        else{
            return true;
        }
}
/**************************
 * Verifica si es Ruc
 * @param obj
 * @returns {boolean}
 */
function isRUC(obj){
    var cont=0;
    if (isPersonaJuridica(obj) ){
        cont++;
    }

    if(isIntitucionPublica(obj)){
        cont++;
    }

     if(isPersonaNatural(obj)){
        cont++;
    }

    if(cont==0)
        return false;
    else
        return true;
}