
$(function(){
    var combo_cliente = $("#id_cliente");
    var lista_doc = $("#lista_documentos");
    var forma_pago = $("#id_forma_pago");

    if(flag)
    {
        if(!isNaN(parseInt(combo_cliente.val())))
        {
            ajax_get_doc(combo_cliente.val())
        }
        else
        {
            $("#grabar").hide();
            lista_doc.hide();
            $("#total_observacion").hide();
        }
    }
    else
    {
        if (forma_pago.val() == "1"){
            AgregarFunciones();

        }
        else{
            AgregarFuncionesCC();


        }
    }
    combo_cliente.change(function(){
        if(!isNaN(parseInt($(this).val())))
        {
            ajax_get_doc(combo_cliente.val());

        }
        else
        {
            $("#grabar").hide();
            $("#mensajes").empty();
            $("#lista_documentos").hide();
            $("#total_observacion").hide();
        }


    });
    forma_pago.change(function(){
        if(!isNaN(parseInt(combo_cliente.val())))
            ajax_get_doc(combo_cliente.val())
    })

});

$(document).ready(function(e){
    // Totales de Cruce de documentos
    totales_cruce_doc();

    // Inhabilita el boton de submit evitando que se haga más de una petición
    $(".btn_grabar").click(function()
    {
        $('#loading').show();
        $('body').css('display', '0.5');
        $(".btn_grabar").hide();
        $(".btn_cancelar").attr("href", "#").text("Por favor, espere...");
    });

    // Para que solo se ingresen números
    $(".numerico").keydown(function(event) {
        solo_numero(event, this);
    });


    $("#id_fecha").datepicker().on('changeDate', function(ev){
        validar_fecha_actual(this);
        $( this ).datepicker("hide")
    });

    $(".selectpicker").selectpicker();
});


