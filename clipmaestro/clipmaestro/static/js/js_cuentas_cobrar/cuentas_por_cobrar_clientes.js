/*******************************
 * Suma los totales a pagar
 */
function totales()
{
    var totales = 0.0;
    $("#tabla_formset_detalle_doc").find(".valor").each(function(){
        totales = totales + parseFloat($(this).val());
    });
    $("#total_pagar").text("$ " + totales.toFixed(2));
}

/********************************************
 * Para el cambio de las columnas de
 *         la forma de pago
 ******************************************/
function cambio_combo(e){
    var n = parseInt($(e).val());
    var tabla = $("#detalle_forma_pago");
    $(tabla).find('.th_forma_pago').hide();
    $(tabla).find('.th_bancos').hide();
    $(tabla).find('.th_referencia').hide();
    $(tabla).find('.th_caja').hide();
    $(tabla).find('.th_termino').hide();
    $(tabla).find('.th_plazo').hide();
    $(tabla).find('.th_interes').hide();
    $(tabla).find('.th_tarjetas').hide();
    switch (n)
    {
        case 1:     // Pago en Cheque
            $(tabla).find('.th_forma_pago').show();
            $(tabla).find('.th_bancos').show();
            $(tabla).find('.th_referencia').show();
            break;
        case 2:     // Pago en efectivo
            $(tabla).find('.th_forma_pago').show();
            $(tabla).find('.th_referencia').show();
            $(tabla).find('.th_caja').show();
            break;
        case 3:     // Pago en Tarjeta de Crédito
            $(tabla).find('.th_forma_pago').show();
            $(tabla).find('.th_tarjetas').show();
            $(tabla).find('.th_termino').show();
            $(tabla).find('.th_plazo').show();
            $(tabla).find('.th_interes').show();
            break;
        case 4:     // Transferencia Bancaria
            $(tabla).find('.th_forma_pago').show();
            $(tabla).find('.th_bancos').show();
            $(tabla).find('.th_referencia').show();
            break;
        default :
            break;
    }
}
function SendData(event)
{
    var tabla_doc_pagar = $("#tabla_formset_detalle_doc");
    var tabla_forma_pago = $("#detalle_forma_pago");
    var combo_forma_pago = $(tabla_forma_pago).find("#id_forma_pago");
    var total_pagar = 0.0;

    $(tabla_doc_pagar).find(".valor").each(function(){
        if($(this).val() != ""){
            total_pagar += parseFloat($(this).val());
        }
    });

    if(total_pagar == 0){
        mensaje_alerta("Debe ingresar el valor a cobrar");
        event.preventDefault();
    }
}

function validar_fecha_actual(obj){
    try
    {
        var date = new Date(parse_date(fecha_actual));
        var fecha_reg = new Date(parse_date($(obj).val()));
        if(fecha_reg > date){
            mensaje_alerta("La fecha ingresada es mayor a la fecha actual");
            $(obj).val(fecha_actual);
        }
    }
    catch(err){
        $(obj).val("");
    }

}
$(document).ready(function(e)
{
    var fecha = $("#id_fecha");

    $(".selectpicker").selectpicker();

    fecha.datepicker().on('changeDate', function (ev){
         validar_fecha_actual(this);
         $(this).datepicker('hide');
    }).mask("9999-99-99").
        change(function(){
            if($(this).val()!="" && $(this).hasClass("campo_requerido"))
            {
                $(this).removeClass("campo_requerido");
            }
        });

    $('#id_fecha_cheque').datepicker({
        todayBtn: true,
        autoclose: true
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });

    // Inhabilita el boton de submit evitando que se haga más de una petición

    $(".btn_grabar").click(function()
    {
        $('#loading').show();
        $('body').css('display', '0.5');
        $(".btn_grabar").hide();
        $(".btn_cancelar").attr("href", "#").text("Por favor, espere...");
    });

    cambio_combo($("#id_forma_pago"));

    // Para inhabilitar que el enter haga submit
    $("body").find("input").keypress(function(e){
         if ( e.which == 13 ) e.preventDefault();
    });

    $(".numerico").keydown(function(event) {
        solo_numero(event, this);
    });
    $(".numero_cheque").keydown(function(event) {
        NumeroNatural(event, this);
    });

    $("#id_forma_pago").change(function(){
        cambio_combo(this);
    });

    $("#id_cliente").change(function(){
        if($("#id_cliente").val() != "")
        {
            $.ajax(
            {
                url: url_cargar_documentos_cobrar,
                type: 'POST',
                async: false,
                cache: false,
                timeout: 300,
                data:{id:$(this).val()},
                beforeSend: function(msj){
                    $('#loading').show();
                    $('body').css('display', '0.5');
                },
                error: function(){
                    return true;
                },
                success: function(data)
                {
                    $("#documentos_cobrar").html(data);
                    AgregarFuncionesDoc();
                    $("#documentos_cobrar").show();
                    $("#grabar").show();
                    $("#detalle_forma_pago").show();
                    $('#loading').fadeOut();
                    $('body').css('opacity', '1');
                }
            });
        }
        else
        {
            $("#documentos_cobrar").hide();
            $("#detalle_forma_pago").hide();
            $("#grabar").hide();
        }
    });


    $(".numerico_valor").focusout(function(event) {
        redondear(this);
        totales();
        var tr = $(this).parent().parent();
        var saldo = parseFloat($(tr).find(".saldo").val());
        var check = $(tr).find(".checks");
        if(saldo)
        {
            if(parseFloat($(this).val()) > saldo){
                mensaje_alerta("El valor ingresado en superior al saldo del documento");
                event.preventDefault();
                $(this).val("0.00");
                $(this).focus();
            }
            else{
                if(parseFloat($(this).val())>0)
                {
                    if (!$(check).is(':checked'))
                        $(check).prop('checked', true);
                }
                else
                {
                    $(check).prop('checked', false);
                }
            }
        }
    });
    var tabla = $("#tabla_formset_detalle_doc");
    $(".checks").each(function(){
        $(this).click(function(){
            var tr = $(this).parent().parent();
            var tipo = $(tr).find(".tipo");
            var total_pagar = 0;
            var total = $(tr).find(".saldo").val();

            if($(tipo).val()=="-1"){
                if($(this).is(':checked'))
                {
                    $(tr).find(".valor").attr("readonly", false);
                    $(tr).find(".valor").focus();
                }
                else
                {
                    $(tr).find(".valor").attr("readonly", true);
                    $(tr).find(".valor").val("0.00");
                }
            }

            if($(this).is(':checked'))
            {
                $(tr).find(".valor").val(total);
                $($(tr).find(".valor")).focus();
            }
            else
            {
                $(tr).find(".valor").val("0.00");
            }
            $(tabla).find(".valor").each(function(){
                if($(this).val()!="")
                    total_pagar = parseFloat(total_pagar) + parseFloat($(this).val());
            });
            $("#total_pagar").text("$ "+parseFloat(total_pagar).toFixed(2));
        });
    });



    // Carga la tabla de documentos por pagar
    $("#cargar_pagos").click(function(){
        cargar_tabla_pagos($("#id_proveedor"));
    });

});

function AgregarFuncionesDoc()
{
    var div_doc_cob = $("#documentos_cobrar");
    div_doc_cob.find(".numerico").keydown(function(event) {
        solo_numero(event, this);
    });
    div_doc_cob.find(".numerico_valor").last().attr("readonly",true);
    div_doc_cob.find(".numerico_valor").focusout(function(event) {
        redondear(this);
        totales();
        var tr = $(this).parent().parent();
        var saldo = parseFloat($(tr).find(".saldo").val());
        var check = $(tr).find(".checks");
        if(saldo)
        {
            if(parseFloat($(this).val()) > saldo){
                mensaje_alerta("El valor ingresado en superior al saldo del documento");
                event.preventDefault();
                $(this).val("0.00");
                $(this).focus();
            }
            else{
                if(parseFloat($(this).val())>0)
                {
                    if (!$(check).is(':checked'))
                        $(check).prop('checked', true);
                }
                else
                {
                    $(check).prop('checked', false);
                }
            }
        }
    });
    var tabla = $("#tabla_formset_detalle_doc");
    div_doc_cob.find(".checks").each(function(){
        $(this).click(function(){
            var tr = $(this).parent().parent().get(0);
            var tipo = $(tr).find(".tipo");
            if($(tipo).val()=="-1"){
                if($(this).is(':checked'))
                {
                    $(tr).find(".valor").attr("readonly", false);
                    $(tr).find(".valor").focus();
                }
                else
                {
                    $(tr).find(".valor").attr("readonly", true);
                    $(tr).find(".valor").val("0.00");
                }
            }
            var total_pagar = 0;
            var total = $(tr).find(".saldo").val();
            if($(this).is(':checked'))
            {
                $(tr).find(".valor").val(total);
                $($(tr).find(".valor")).focus();
            }
            else
            {
                $(tr).find(".valor").val("0.00");
            }
            $(tabla).find(".valor").each(function(){
                if($(this).val()!="")
                    total_pagar = parseFloat(total_pagar) + parseFloat($(this).val());
            });
            $("#total_pagar").text("$ "+parseFloat(total_pagar).toFixed(2));
        });
    });
    validar_fecha_vencimiento_documento_tabla("#tabla_formset_detalle_doc");

}