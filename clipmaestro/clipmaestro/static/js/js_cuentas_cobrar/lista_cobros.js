/**
 * Created by Clip Maestro on 30/03/2015.
 */
function esta_reversado(url, id)
{
    var estado = true;

    $.ajax(
        {
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: {"id": id},
            error: function(){
                return true;
            },
            success: function(data){
                if (data.status == 1)
                    estado = data.esta_reversado;
            }
        });
    return estado;
}

function get_fecha_reversado()
{
    var fecha;

    $.ajax(
        {
            url: url_get_fecha_reversar,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            error: function(){
                return true;
            },
            success: function(data){
                if (data.status == 1)
                    fecha = data.fecha;
            }
        });
    return fecha;
}


jQuery(document).ready(function($) {

    $("#limpiar").click(function(){
        var form = $("#formulario");
        limpiar_form(form);
    });

    $(".numerico").keydown(function(event) {Numerico(event)});
    $(".numerico_doc").keydown(function(event) {NumeroEntero(event)});

    $("#id_fecha_ini").mask('9999-99-99');
    $("#id_fecha").mask('9999-99-99').datepicker().on('changeDate', function (ev){$(this).datepicker('hide')});
    $("#id_fecha_final").mask('9999-99-99');
    $("#id_numero_ret").mask('999999999').focusout(function(){AutocompletarCeros(this)});
    $("#id_num_doc").mask('999999999').focusout(function(){
        AutocompletarCeros(this)}).keydown(function(event) {NumeroEntero(event)});

    $(".opcion-buscar").bind('click', function(event) {
        var cont_busc = $(".container-buscador");
        cont_busc.slideToggle(1000);
        $(".opcion-buscar").addClass("opcion-buscar-hide");
        cont_busc.removeClass("container-buscador-hide");
    });

    $(".buscador-cerrar").click(function(){

        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
    });

    $(".selectpicker").selectpicker();

    $("#ver").click(function(){
        var lista = $("#lista input[type='radio']:checked");
        if (lista.length != 0){
            window.location.href = lista.attr("data-ver");
        }else{
            mensaje_alerta('Seleccione un item para ver el datalle del pago');
        }
    });

    $("#anular").click(function(){
        var lista = $("#lista input[type='radio']:checked");
        if (lista.length != 0){
            var tr = lista.parent().parent();
            var nombre = tr.find(".nombre").text();
            var url = lista.attr("data-anular");

            if(lista.attr("data-status") == "2")
            {
                mensaje_alerta('El cobro seleccionado ya se encuentra anulado');
            }
            else
            {
                var msn = '<p>Desea anular el cobro con # comprobante"<strong>'+nombre+'</strong>"?</p>';
                mensaje_alerta_yn(msn, "Anular Pago", "data-href", url);
            }
        }
        else{
            mensaje_alerta('Seleccione un item para anular un pago');
        }
    });

    $("#reversar").click(function(){
        var lista = $("#lista input[type='radio']:checked");
        if (lista.length != 0){
            var tr = lista.parent().parent();
            var nombre = tr.find(".nombre").text();
            var url = lista.attr("data-reversar");
            var id = $(lista).val();
            var num_comp = $(tr).find(".nombre").text();
            var cliente = $(tr).find(".cliente").text();
            var fecha_reg = $(tr).find(".fecha_reg").text();
            var fecha_rever = get_fecha_reversado();

            $("#id_fecha").val(fecha_rever);
            $("#form_reversar_cobro").attr("action", url);
            $("#num_comp_rev").text(num_comp);
            $("#cliente_reversar").text(cliente);
            $("#fecha_reg_reversar").text(fecha_reg);


            if(lista.attr("data-status") == "2")
            {
                mensaje_alerta('El cobro seleccionado se encuentra anulado');
            }
            else
            {
                if (esta_reversado(url_esta_reversado, id))
                {
                    mensaje_alerta("El cobro que seleccionó ya se encuentra reversado, por favor verifique")
                }
                else
                {
                    $("#reversar_cobro_modal").modal();
                }
            }
        }
        else{
            mensaje_alerta('Seleccione un item para reversar un pago');
        }
    });
    $("#aceptar_reversar").click(function(){
        var form = $("#form_reversar_cobro");
        var url = form.attr("action");
        var datos = form.serialize();
        var div_mensaje = $(".div_error");
        div_mensaje.empty();

        $.ajax(
        {
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data: datos,
            error: function(){
                return true;
            },
            success: function(data){
                if (data.status == 1){
                    $("#reversar_cobro_modal").modal('hide');
                    mensaje_alerta("Se ha reversado exitosamente el cobro");
                    setTimeout(function(){
                      location.reload();
                    }, 2000);
                }
                else
                {
                    mensaje_error(data.mensaje, div_mensaje);
                }
            }
        });
    });
    $("#ok-alert").on("click",function(){
        window.location.href = $("#alert-yn").attr("data-href");
    });

    $(".evento_page").each(function(){
        $(this).click(function(){
            var page = $(this).attr("data-page");
            $("#id_page").val(page);
            $("#buscar").click();
        });
    });
});