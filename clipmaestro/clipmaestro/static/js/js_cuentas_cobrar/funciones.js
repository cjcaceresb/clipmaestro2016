/**
 * Created by Clip Maestro on 05/06/2014.
 */
/*******************************
 * Suma los totales a pagar
 */
function totales()
{
    var totales = 0.0;
    $("#tabla_formset_detalle_doc").find(".valor").each(function(){
        totales = totales + parseFloat($(this).val());
    });
    $("#total_pagar").text("$ " + totales.toFixed(2));
}

function validar_fecha_actual(obj){
    try
    {
        var date = new Date(parse_date(fecha_actual));
        var fecha_reg = new Date(parse_date($(obj).val()));
        if(fecha_reg > date){
            mensaje_alerta("La fecha ingresada es mayor a la fecha actual");
            $(obj).val(fecha_actual);
        }
    }
    catch(err){
        $(obj).val("");
    }
}

function AgregarFuncionesDoc()
{
    var div_doc_cobrar = $("#documentos_cobrar");
    div_doc_cobrar.find(".numerico").keydown(function(event) {
        solo_numero(event, this);
    });

    div_doc_cobrar.find(".numerico_valor").last().attr("disabled",true);
    div_doc_cobrar.find(".numerico_valor").focusout(function(event) {
        agregar_punto(this);
        totales();
        var tr = $(this).parent().parent();
        var saldo = parseFloat($(tr).find(".saldo").val());
        var check = $(tr).find(".checks");
        if(saldo)
        {
            if(parseFloat($(this).val()) > saldo){
                mensaje_alerta("El valor ingresado en superior al saldo del documento");
                event.preventDefault();
                $(this).val("0.00");
                $(this).focus();
            }
            else{
                if(parseFloat($(this).val())>0)
                {
                    if (!$(check).is(':checked'))
                        $(check).prop('checked', true);
                }
                else
                {
                    $(check).prop('checked', false);
                }
            }
        }
    });
    var tabla = $("#tabla_formset_detalle_doc");
    div_doc_cobrar.find(".checks").each(function(){
        $(this).click(function(){
            var tr = $(this).parent().parent().get(0);
            var tipo = $(tr).find(".tipo");
            if($(tipo).val()=="-1"){
                if($(this).is(':checked'))
                {
                    $(tr).find(".valor").attr("disabled", false);
                    $(tr).find(".valor").focus();
                }
                else
                {
                    $(tr).find(".valor").attr("disabled", true);
                    $(tr).find(".valor").val("0.00");
                }
            }
            var total_pagar = 0;
            var total = $(tr).find(".saldo").val();
            if($(this).is(':checked'))
            {
                $(tr).find(".valor").val(total);
                $($(tr).find(".valor")).focus();
            }
            else
            {
                $(tr).find(".valor").val("0.0");
            }
            $(tabla).find(".valor").each(function(){
                if($(this).val()!="")
                    total_pagar = parseFloat(total_pagar) + parseFloat($(this).val());
            });
            $("#total_pagar").text("$ "+parseFloat(total_pagar).toFixed(2));
        });
    });
}

function validar_total_pagar(valor)
{
    var val = parseFloat(parseFloat($(valor).val()).toFixed(2));
    var tr = $(valor).parent().parent().get(0);
    var saldo = parseFloat(parseFloat($(tr).find(".saldo").val()).toFixed(2));
    var saldo_act = $(tr).find(".saldo_actual");
    var check = $(tr).find(".checks");
    var tabla_cru_doc = $("#tabla_cruce_documentos");
    var tabla_doc_pagar = $("#tabla_formset_detalle_doc");
    var total_fact = 0.0;
    var total_doc = 0.0;

    if(val > saldo)
    {
        mensaje_alerta("El valor excede el saldo del Documento a cruzar el cual es: "+ saldo.toFixed(2));
        $(valor).val("0.00");
        if ($(check).is(':checked'))
            $(check).prop('checked', false);
    }
    else
    {
        $(tabla_cru_doc).find(".valor").each(function(){
            total_doc += parseFloat($(this).val());
        });
        $(tabla_doc_pagar).find(".valor").each(function(){
            total_fact += parseFloat($(this).val());
        });
        if(parseFloat(total_doc.toFixed(2)) > parseFloat(total_fact.toFixed(2)))
        {
            mensaje_alerta("El valor excede el total de pago de la factura o no ha seleccionado una para el cobro");
            $(valor).val("0.00");
        }
        else
        {
            if(val>0){
                if (!$(check).is(':checked'))
                    $(check).prop('checked', true);
                $(saldo_act).val((saldo-val).toFixed(2));
            }
        }
    }
}

/*******************************
 * Suma los totales a pagar
 * y los presenta en la pantalla
 */
function totales_cruce_doc()
{
    var total_pagar = 0.0;
    var total_pago = $("#total_pago");
    var saldo_actual = $("#saldo_actual");
    var saldo_anterior = 0.0;
    var s_actual = 0.0;
    var total_cruce = 0.0;

    $("#tabla_cruce_documentos").find(".valor").each(function(){
        total_cruce += parseFloat($(this).val());
    });

    var factura_selec = $("#tabla_formset_detalle_doc").find("input[name=doc_select]:checked");
    var tr = $(factura_selec).parent().parent();

    if(factura_selec.length > 0)
    {
        total_pagar = parseFloat($(tr).find(".valor").val());
        saldo_anterior = parseFloat($(tr).find(".saldo").val());
    }

    $("#total_pagar").text("$ " + total_pagar.toFixed(2));
    total_pago.text("$ " + total_cruce.toFixed(2));
    $("#saldo_anterior").text("$ " + saldo_anterior.toFixed(2));

    if(saldo_anterior > 0)
    {
        s_actual = parseFloat(saldo_anterior.toFixed(2)) - parseFloat(total_cruce.toFixed(2));
        $(saldo_actual).text("$ " + s_actual.toFixed(2));
    }
    else
    {
        $(saldo_actual).text("$ 0.00");
    }
}

/******************************
 * Función para copiar el valor
 * de un input de class1 a uno
 * de class2, el tr es para saber en
 * que fila me encuentro
 * @param tr
 * @param class1
 * @param class2
 * @constructor
 */
function CopiarValor(tr, class1, class2){
    var input_class2 = $(tr).find("."+class2);
    var input_class1 = $(tr).find("."+class1);
    $(input_class2).val(parseFloat($(input_class1).val()).toFixed(2));
}

function SendData(event)
{
    var total_pago = parseFloat($("#total_pago").text().replace("$", ""));
    var saldo_anterior = parseFloat($("#saldo_anterior").text().replace("$", ""));

    if(total_pago <= 0)
    {
        mensaje_alerta("Ingrese un valor para hacer el cruce");
        event.preventDefault();
    }

}

function ClickCheckCruceDoc(e, obj)
{
    var valor_factura = 0.0;
    var tr = $(obj).parent().parent();
    var saldo_doc_cruzar = parseFloat($(tr).find(".saldo").val());
    var saldo_act_doc_cruzar = parseFloat($(tr).find(".saldo_actual").val());
    var tabla = $("#tabla_formset_detalle_doc");

    $(tabla).find(".checks").each(function(){
        var tr = $(this).parent().parent().get(0);
        if($(this).is(':checked'))
        {
            valor_factura = parseFloat($(tr).find(".valor").val());
        }
    });
    if(valor_factura>0)
    {
        if($(obj).is(':checked'))
        {
            var saldo_actual = parseFloat($("#saldo_actual").text().replace("$", ""));

            if (saldo_actual > saldo_act_doc_cruzar)
            {
                CopiarValor(tr,"saldo_actual","valor");
                $(tr).find(".saldo_actual").val("0.00");
            }
            else
            {
                $(tr).find(".valor").val(saldo_actual.toFixed(2));
                $(tr).find(".saldo_actual").val((saldo_doc_cruzar - saldo_actual).toFixed(2));
            }
            totales_cruce_doc();
        }
        else
        {
            var val = parseFloat($(tr).find(".valor").val());
            $(tr).find(".saldo_actual").val((saldo_doc_cruzar).toFixed(2));
            $(tr).find(".valor").val("0.00");
            totales_cruce_doc();
        }
    }
    else
    {
        mensaje_alerta("Debe de seleccionar una documento a cobrar para realizar el cruce de documentos");
        e.preventDefault();
    }
}

function ClickCheckTablaPagos(obj)
{
    var tr = $(obj).parent().parent().get(0);

    // Si al inicio esta chequeado una factura
    if($(obj).is(':checked'))
    {
        CopiarValor(tr,"saldo","valor");
        $("#saldo_anterior").text("$ "+$(tr).find(".valor").val());
        totales_cruce_doc();
    }

    $("#total_pagar").text("$ " + $(tr).find(".valor").val());

    // Al hacer click
    $(obj).click(function(){
        var tr = $(this).parent().parent().get(0);
        // Encera los valores de la tabla de cruce de documentos
        $("#tabla_cruce_documentos").find(".checks").each(function(){
            var tr = $(this).parent().parent();
            var valor = parseFloat($(tr).find(".valor").val());
            if (valor > 0){
                var saldo = parseFloat($(tr).find(".saldo").val());
                $(tr).find(".saldo_actual").val((saldo).toFixed(2));
            }
            $(tr).find(".valor").val("0.00");
            $(this).prop('checked', false);
        });

        $("#tabla_formset_detalle_doc").find(".checks").each(function(){
            var tr = $(this).parent().parent();
            $(tr).find(".valor").val("0.00");
        });

        CopiarValor(tr,"saldo","valor");
        $("#saldo_anterior").text("$ "+$(tr).find(".valor").val());
        totales_cruce_doc();
        $("#total_pagar").text("$ " + $(tr).find(".valor").val());
    });
}
/******************************
 *
 * @param event
 * @param obj
 * @constructor ""
 */
function VerificarSaldo(event, obj)
{
    var tr = $(obj).closest("tr");
    var saldo = parseFloat($(tr).find(".saldo").val());
    var check = tr.find(".checks");
    if(parseFloat($(obj).val()) > saldo){
        mensaje_alerta("El valor ingresado es superior al saldo del documento");
        event.preventDefault();
        $(obj).val("0.00");
        $(obj).focus();
    }
    else
    {
        if (parseFloat($(obj).val())>0)
            $(check).prop('checked', true);
        else
            $(check).prop('checked', false);
    }
}

/***********************
 * Agrega funciones a los elementos
 * que son llamados mediante ajax
 * @constructor obj
 */
function AgregarFunciones()
{
    var totales_cruce_doc = $(".totales_cruce_doc");
     totales_cruce_doc.show();

    // Valida que solo ingrese numeros en los campos numericos
    $("#lista_documentos").find(".numerico").keydown(function(event) {
        solo_numero(event, this);
    }).focusout(function(event) {
        agregar_punto(this);
        totales_cruce_doc();
    });

    if ($("#tiene_docs").attr("data-error")=="1"){
        $("#grabar").hide();
    }

    // Agrega funcion de validar los totales en los campor valor dentro de la tabla de cruce de docs
    $("#tabla_cruce_documentos").find(".valor").each(function(){
        $(this).change(function(){
            validar_total_pagar(this);
            totales_cruce_doc();
        });
        $(this).focusout(function(){
            validar_total_pagar(this);
            totales_cruce_doc();
        });
    });

    $("#tabla_cruce_documentos").find(".checks").each(function(){
        $(this).click(function(e){
            ClickCheckCruceDoc(e, this)
        });
    });

    $("#tabla_formset_detalle_doc").find(".valor").each(function(){
        $(this).change(function(){
            validar_total_pagar(this);
            totales_cruce_doc();
        });
    }).attr("readonly", true);


    $("#tabla_formset_detalle_doc").find(".checks").each(function(){
        ClickCheckTablaPagos(this);
    });

    $(".numerico").change(function(event) {
        agregar_punto(this);
        VerificarSaldo(event, this);
    });

    $("body").find("input").keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
    });
    validar_fecha_vencimiento_documento_tabla("#tabla_formset_detalle_doc");
}

/***********************
 * Calcula los totales del cruce de cuentas
 * @constructor  obj
 */
function calcular_total_cruce_cuenta()
{
    var tabla_cc = $("#tabla_formset_detalle_cta");
    var total_cruce = 0.0;

    tabla_cc.find(".valor").each(function(){
        if (!isNaN($(this).val()))
        {
            total_cruce += parseFloat($(this).val());
        }
    });

    $("#total_cruce_cta").text("$ " + total_cruce.toFixed(2));
}
/**************************
 * Función colocar_saldos
 * Al hacer click sobre un checkbox de cruce de cuentas
 * se coloca el saldo del documento a la casilla de valor
 * @param obj
 */
function colocar_saldos(obj)
{
    var tr = $(obj).closest("tr");
    var saldo = parseFloat(tr.find(".saldo").val());
    var valor = tr.find(".valor");
    if($(obj).is(':checked'))
    {
        valor.val(saldo.toFixed(2))
    }
    else
    {
        valor.val("0.00")
    }
    valor.trigger("change");
}
/***********************
 * Agrega funciones a los elementos
 * que son llamados mediante ajax en Cruce de cuentas
 * @constructor obj
 */
function AgregarFuncionesCC()
{
    var btn_grabar = $("#grabar");
    var tabla_cc = $("#tabla_formset_detalle_cta");
    var totales_cruce_doc = $(".totales_cruce_doc");

    if ($("#tiene_docs").attr("data-error")=="1"){
        btn_grabar.hide();
    }

    totales_cruce_doc.hide();

    // Calcula los totales
    calcular_total_cruce_cuenta();

    // Valida que solo ingrese numeros en los campos numericos
    $("#lista_documentos").find(".numerico").keydown(function(event) {
        solo_numero(event, this);
    }).focusout(function(event) {
        agregar_punto(this);
    });

    // Agrega funcion de validar los totales en los campor valor dentro de la tabla de cruce de docs
    tabla_cc.find(".valor").each(function()
    {
        $(this).bind("change focusout", function(event){
            VerificarSaldo(event, this);
            calcular_total_cruce_cuenta()
        });
    });

    tabla_cc.find(".checks").each(function()
    {
        $(this).click(function(){
            colocar_saldos(this);
        })
    });

    $("body").find("input").keypress(function(e){
        if ( e.which == 13 ) e.preventDefault();
    });

    validar_fecha_vencimiento_documento_tabla("#tabla_formset_detalle_cta");
}

function ajax_get_doc(id_client)
{
    var lista_doc = $("#lista_documentos");
    var url = "";
    var forma_pago = $("#id_forma_pago");

    if (forma_pago.val() == "1"){
        url = url_cruce_doc;
    }
    else{
        url = url_cruce_cuenta;
    }

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{id:id_client},
        error: function(){
            return true;
        },
        beforeSend: function(msj){
            $('#loading').show();
            $('body').css('opacity', '0.5');
        },
        success: function(data)
        {
            var div = $($.parseHTML(data)).filter("#tiene_docs");
            lista_doc.html(data).hide();
            if(div.attr("data-error") == "1"){
                $("#mensajes").html(data);
            }
            else
            {
                $("#grabar").show();
                $("#mensajes").empty();
                $("#lista_documentos").show();
                $("#total_observacion").show();
            }
            lista_doc.find(".selectpicker").selectpicker();

            if (forma_pago.val() == "1"){
                AgregarFunciones()
            }
            else{
                AgregarFuncionesCC();
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
}