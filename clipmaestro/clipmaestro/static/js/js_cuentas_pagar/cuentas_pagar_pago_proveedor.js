function calcular_total_pagar()
{
    var tabla = $("#tabla_formset_detalle_doc");
    var total_pagar = 0;
    $(tabla).find(".valor").each(function(){
        if($(this).val()!="")
            total_pagar = parseFloat(total_pagar) + parseFloat($(this).val());
    });
    $("#total_pagar").text("$ "+parseFloat(total_pagar).toFixed(2));
}

function agregar_funciones_pago()
{
    var doc_pag = $("#documentos_pagar");
    validar_fecha_vencimiento_documento_tabla("#tabla_formset_detalle_doc");
    doc_pag.find(".numerico").keydown(function(event) {
        solo_numero(event, this);
    });

    if(doc_pag.find(".checks").last().is(':checked'))
    {
        doc_pag.find(".valor").last().attr("readonly",false);
    }
    else
    {
        doc_pag.find(".valor").last().attr("readonly",true);
    }
    doc_pag.find(".numerico").focusout(function(event) {
        redondear(this);
        totales();
        VerificarSaldo(this);
    });
    doc_pag.find(".checks").each(function(){
        $(this).click(function(){
            var tr = $(this).parent().parent().get(0);
            var tipo = $(tr).find(".tipo");
            if($(tipo).val()=="-1"){
                if($(this).is(':checked'))
                {
                    $(tr).find(".valor").attr("readonly", false);
                    $(tr).find(".valor").focus();
                }
                else
                {
                    $(tr).find(".valor").attr("readonly", true);
                    $(tr).find(".valor").val("0.00");
                }
            }

            var total = $(tr).find(".saldo").val();
            if($(this).is(':checked'))
            {
                $(tr).find(".valor").val(total);
                $($(tr).find(".valor")).focus();
            }
            else
            {
                $(tr).find(".valor").val("0.0");
            }
            calcular_total_pagar();
        });
    });
}
function cargar_cuentas_pagar()
{
    var proveedor = $("#id_proveedor");
    if(proveedor.val()  == "")
    {
        $("#detalle_forma_pago").hide();
        $('input[type="submit"]').hide();
    }
    $.ajax(
    {
        url: url_carg_cta_pag,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        dataType: "html",
        data:{id: proveedor.val()},
        beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data)
        {
            $("#documentos_pagar").html(data).hide();
            var div = $("#tiene_docs");
            if(div.attr("data-error") == "1"){
                $("#documentos_pagar").html(data).hide();
                $("#detalle_forma_pago").hide();
                $('input[type="submit"]').hide();
            }
            else
            {
                $("#documentos_pagar").html(data).show();
                $("#detalle_forma_pago").show();
                $('input[type="submit"]').show();
            }
            agregar_funciones_pago();
            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
}

function get_num_cheque()
{
    var banco = $("#id_bancos");
    var num_cheque = $("#id_cheque");
    num_cheque.val("");
    if (banco.val() != "")
    {
        if($("#id_forma_pago").val() == "1")
        {
            $.ajax(
            {
                url: url_get_ultimo_cheque,
                type: 'POST',
                async: false,
                cache: false,
                timeout: 300,
                data:{id_cta_banco: banco.val()},
                beforeSend: function(msj){
                    $('#loading').show();
                    $('body').css('display', '0.5');
                },
                error: function(){
                    return true;
                },
                success: function(data)
                {
                    if (data.status == "1")
                    {
                        if(data.es_cta_corriente)
                        {
                            $(num_cheque).val("Ult. Ch/: " + data.num_cheque);
                            $(num_cheque).attr("readonly", true);
                        }
                        else
                        {
                            mensaje_alerta("La cuenta que eligió no es una cuenta corriente, por favor corrija");
                            banco.prop("selectedIndex", 0);
                            banco.selectpicker("refresh");
                            $(num_cheque).val("");
                        }
                    }
                    $('#loading').fadeOut();
                    $('body').css('opacity', '1');
                }
            });
        }
    }
}

function get_fecha_comp()
{
    var forma_pago = $("#id_forma_pago");
    var fecha = $("#id_fecha");
    if(forma_pago.val() != "")
    {
        $.ajax(
        {
            url: url_get_fecha_actual_comprobante,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data:{id: forma_pago.val()},
            beforeSend: function(msj){
                $('#loading').show();
                $('body').css('display', '0.5');
            },
            error: function(){
                return true;
            },
            success: function(data)
            {
                if (data.status == "1")
                {

                        $(fecha).val(data.fecha);
                }
                $('#loading').fadeOut();
                $('body').css('opacity', '1');
            }
        });
    }

}

function get_cheques_provisionados()
{
    var select_cheques_prov = $("#id_lista_cheques_prov");
    var banco = $("#id_bancos").find("option:selected");
    select_cheques_prov.empty();
    if (banco.val() != "")
    {
        $.ajax(
        {
            url: url_get_cheque_provisionado,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 300,
            data:{
                id: banco.val()
            },
            beforeSend: function(msj){
                $('#loading').show();
                $('body').css('display', '0.5');
            },
            error: function(){
                return true;
            },
            success: function(data)
            {
                for(var i=0; i<data.length; i++)
                {
                    if( i==0 )
                        select_cheques_prov.append('<option value="' + data[i].id + '" data-fecha="'+ data[i].fecha +'"' +
                            ' selected="selected" >' +
                            data[i].descripcion + '</option>');
                    else
                        select_cheques_prov.append('<option value="' + data[i].id + '" data-fecha="'+ data[i].fecha +'"' +
                            ' >' + data[i].descripcion + '</option>');
                }

                $('#loading').fadeOut();
                $('body').css('opacity', '1');
            }
        });
    }
    select_cheques_prov.selectpicker("refresh");
}

function es_cheque_provisionado()
{
    var fecha_reg = $("#id_fecha");
    var es_prov = $("#id_cheque_provisionado");

    if($(es_prov).is(":checked"))
    {
        get_cheques_provisionados();
        $(".chq_prov").show();
        $(".chq_normal").hide();
        fecha_reg.attr("disabled", true);
        fecha_reg.addClass("input_deshabilitado");
        fecha_reg.val("");
    }
    else
    {
        get_num_cheque();
        $(".chq_prov").hide();
        $(".chq_normal").show();
        fecha_reg.attr("disabled", false);
        fecha_reg.removeClass("input_deshabilitado");
    }
}

$(document).ready(function(e)
{
    // Boton Guardar y Agregar
    $("#grabar_agregar").click(function(){
        var formulario = $("#formulario");
        var action = formulario.attr("action");
        formulario.attr("action", action+"?grabar_agregar=1");
        $("#grabar").click();
    });

    // Para inhabilitar el boton del sumbit
    $(".btn_grabar").click(function()
    {
        $('#loading').show();
        $('body').css('display', '0.5');
        $(".btn_grabar").hide();
        $(".btn_cancelar").attr("href", "#").text("Por favor, espere...");
    });

    if(flag)  // Si es true es porque se recibe del post
    {
        calcular_total_pagar();
        agregar_funciones_pago();
    }
    else
    {
        get_fecha_comp();
        cargar_cuentas_pagar();
    }
    $("#id_proveedor").change(function(){
        cargar_cuentas_pagar()
    });

    $("#id_bancos").change(function(){
        if ($("#id_cheque_provisionado").is(":checked"))
        {
            get_cheques_provisionados();
        }
        else{
            get_num_cheque()
        }
    });
    $("#actualizar_secuencia_cheque").click(function(){get_num_cheque()});
    $("#id_forma_pago").change(function(){
        get_num_cheque();
        get_fecha_comp();
    });

    $("body").find("input").keypress(function(e){
         if ( e.which == 13 ) e.preventDefault();
    });

    cambiar_forma_pago($("#id_forma_pago"));

    $(".selectpicker").selectpicker();

    $(".numerico").keydown(function(event){
        solo_numero(event, this)
    }).change(function(event){
        redondear(this);
        totales();
    });

    $(".numero_cheque").keydown(function(event) {
        NumeroNatural(event, this);
    });


    es_cheque_provisionado();

    $("#id_cheque_provisionado").click(function(){
        es_cheque_provisionado()
    });


    $("#id_lista_cheques_prov").change(function () {
        var opt_sel = $(this).find("option:selected");
        var fecha_reg = $("#id_fecha");
        if (opt_sel.attr("value") != "")
        {
            var fecha = opt_sel.attr("data-fecha");
            fecha_reg.val(fecha);
        }
        else
        {
            fecha_reg.val("")
        }
    });




    $("#id_forma_pago").change(function(){
        cambiar_forma_pago  (this);
    });

    $('#id_fecha_cheque').mask("9999-99-99").datepicker({
        todayBtn: true,
        autoclose: true
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
        validar_fecha_cheque();
    });

    $('#id_fecha_cheque').change(function(){
        validar_fecha_cheque();
    });

    $('#id_fecha').mask("9999-99-99").datepicker({
        todayBtn: true,
        autoclose: true
    }).on('changeDate', function (ev) {
        validar_fecha_actual(this);
        $(this).datepicker('hide');
        validar_fecha_cheque()
    });

    $("#id_fecha").change(function(){
        if($(this).val()!="" && $(this).hasClass("campo_requerido")){
            $(this).removeClass("campo_requerido");
        }
        validar_fecha_cheque()
    });
});
