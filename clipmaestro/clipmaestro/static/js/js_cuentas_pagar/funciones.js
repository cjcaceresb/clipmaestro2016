/**
 * Created by Clip Maestro on 09/06/2014.
 */

/** Funciones para el modulo pagos ...*/

function cambiar_forma_pago(obj)
{
    var opt = parseInt($(obj).val());
    $(".th_bancos").hide();
    $(".th_tarjetas").hide();
    $(".th_termino").hide();
    $(".th_plazo").hide();
    $(".th_interes").hide();
    //$(".th_cheque").hide();
    $(".th_fecha_cheque").hide();
    $(".th_caja").hide();
    $(".th_referencia").hide();
    switch (opt)
    {
        case 1: // Cheque
            $(".th_bancos").show();
            $(".th_cheque").show();
            $(".th_fecha_cheque").show();
            break;
        case 2: // Caja
            $(".th_caja").show();
            $(".th_cheque").hide();
            $(".th_referencia").show();
            break;
        case 3:
            break;
        case 4: // Transferencia
            $(".th_bancos").show();
            $(".th_cheque").hide();
            $(".th_referencia").show();
            break;
    }
}

/*******************************
 * Suma los totales a pagar
 */
function totales()
{
    var totales = 0.0;
    $("#tabla_formset_detalle_doc").find(".valor").each(function(){
        totales = totales + parseFloat($(this).val());
    });
    $("#total_pagar").text("$ " + totales.toFixed(2));
}

function validar_fecha_actual(obj){
    try
    {
        if(comparar_fechas($(obj).val(), fecha_actual, "mayor"))
        {
            mensaje_alerta("La fecha ingresada es mayor a la fecha actual");
            $(obj).val(fecha_actual);
        }

    }
    catch(err){
        alert(err);
        $(obj).val("");
    }
}

/*******************************
 * Suma los totales a pagar
 * en el cruce de documentos
 */
function totales_cruce_doc()
{
    var total_pagar = 0.0;
    var total_pago = $("#total_pago");
    var saldo_actual = $("#saldo_actual");
    var saldo_anterior = 0.0;
    var s_actual = 0.0;
    var total_cruce = 0.0;

    $("#tabla_cruce_documentos").find(".valor").each(function(){
        total_cruce += parseFloat($(this).val());
    });

    var factura_selec = $("#tabla_formset_detalle_doc").find("input[name=doc_select]:checked");
    var tr = $(factura_selec).parent().parent();

    if(factura_selec.length > 0)
    {
        total_pagar = parseFloat($(tr).find(".valor").val());
        saldo_anterior = parseFloat($(tr).find(".saldo").val());
    }

    $("#total_pagar").text("$ " + total_pagar.toFixed(2));
    total_pago.text("$ " + total_cruce.toFixed(2));
    $("#saldo_anterior").text("$ " + saldo_anterior.toFixed(2));

    if(saldo_anterior > 0)
    {
        s_actual = saldo_anterior - total_cruce;
        $(saldo_actual).text("$ " + s_actual.toFixed(2));
    }
    else
    {
        $(saldo_actual).text("$ 0.00");
    }
}

/****************************
 * Valida que el el total a pagar
 * este correcto con el valor de pago
 * @param valor
 */
function validar_total_pagar(valor)
{
    var val = parseFloat($(valor).val());
    var tabla_cru_doc = $("#tabla_cruce_documentos");
    var tabla_doc_pagar = $("#tabla_formset_detalle_doc");
    var total_fact = 0.0;
    var total_doc = 0.0;

    $(tabla_cru_doc).find(".valor").each(function(){
        total_doc += parseFloat($(this).val());
    });
    $(tabla_doc_pagar).find(".valor").each(function(){
        total_fact += parseFloat($(this).val());
    });
    if(total_doc > total_fact)
    {
        mensaje_alerta("El valor excede el total de pago de la Factura");
        $(valor).val("0.00");
    }
    else
    {
        if(val>0){
            var tr = $(valor).parent().parent();
            var check = $(tr).find(".checks");
            if (!$(check).is(':checked'))
                $(check).prop('checked', true);

        }
    }
}

/**************************************************************
 *  FUNCIONES PARA CRUCE DE DOCUMENTOS Y CUENTAS
 **************************************************************/

/************************************
 * Verifica que se haya seleccionado un documento por pagar
 * antes de seleccionar los documentos por cruzar
 * @param e
 * @param obj
 * @constructor
 */
function TieneDocPagar(e, obj)
{
    var saldo_anterior = 0.0;
    var tr = $(obj).parent().parent();
    var total_doc_cruzar = parseFloat($(tr).find(".saldo").val());
    var tabla = $("#tabla_formset_detalle_doc");

    $(tabla).find(".checks").each(function(){
        var tr = $(this).parent().parent().get(0);
        if($(this).is(':checked'))
        {
            saldo_anterior = parseFloat($(tr).find(".valor").val());
        }
    });

    if(saldo_anterior>0)
    {
        if($(obj).is(':checked'))
        {
            var saldo_actual = $("#saldo_actual").text().replace("$", "");
            if (saldo_actual > total_doc_cruzar)
            {
                CopiarValor(tr,"saldo","valor");
            }
            else
            {
                $(tr).find(".valor").val(saldo_actual);
            }
            totales_cruce_doc();
        }
        else
        {
            $(tr).find(".valor").val("0.00");
            totales_cruce_doc();
        }
    }
    else
    {
        mensaje_alerta("Debe seleccionar una documento para realizar el cruce de documentos, o bien si ya esta " +
            "seleccionada verifique que se le ha asigando un valor");

        e.preventDefault();
    }
}

/*******************************
 * Funcion que automaticamente pone el valor
 * a cruzar en la col "valor" de la tabla de
 * documentos por cruzar
 * @param obj
 * @constructor
 */
function PonerSaldoDoc(obj)
{
    var tr = $(obj).parent().parent();
    CopiarValor(tr,"saldo","valor");
    $("#saldo_anterior").text("$ "+$(tr).find(".valor").val());
    totales_cruce_doc();
    $("#total_pagar").text("$ " + $(tr).find(".valor").val());
}


/******************************
 * Función para copiar el valor
 * de un input de class1 a uno
 * de class2, el tr es para saber en
 * que fila me encuentro
 * @param tr
 * @param class1
 * @param class2
 * @constructor tt
 */
function CopiarValor(tr, class1, class2){
    var input_class2 = $(tr).find("."+class2);
    var input_class1 = $(tr).find("."+class1);
    $(input_class2).val(parseFloat($(input_class1).val()).toFixed(2));
}

/***********
 * Al hacer click en los checks, automaticamente se pone
 * el valor del saldo en el valor a pagar de la cuenta por pagar
 * @param obj
 * @constructor  obj
 */
function CopiarSaldoValor(obj)
{
    var tr = $(obj).parent().parent().get(0);
    var valor = parseFloat($(tr).find(".valor").val());

    if($(obj).is(':checked'))
    {
        if (valor <= 0)
            CopiarValor(tr,"saldo","valor");
    }
    else
    {
        $(tr).find(".valor").val("0.00")
    }
}

/**************************+++
 * Pone en cero a las facturas que no estan
 * seleccionadas en el cruce de documentos
 */
function encerar_facturas()
{
    $("#tabla_formset_detalle_doc tbody tr").each(function(){
        var check = $(this).find(".checks");
        if(!check.prop('checked'))
        {
            $(this).find(".valor").val("0.00");
        }
    });
}
/************************************
 * Verifica el valor del saldo del documento
 * para que no se pase de su monto
 * @param obj
 * @constructor
 */
function VerificarSaldo(obj)
{
    var valor = $(obj).val();
    var tr = $(obj).parent().parent().get(0);
    var check = $(tr).find(".checks");
    var saldo_anterior = parseFloat($(tr).find(".saldo").val());
    if(valor > 0){
        if(valor > saldo_anterior){
            mensaje_alerta("El valor ingresado es superior al saldo del documento");
            check.prop('checked', false);
            $(obj).val("0.00");
            $(obj).focus();
        }
        else
        {
            check.prop('checked', true)
        }
    }
    else
    {
        check.prop('checked', false)
    }
    totales();
}

function ActualizarTotalesCruceCta()
{
    var tabla = $("#tabla_cruce_cuenta");
    var t_saldo = 0.0;
    var t_pago = 0.0;
    tabla.find("tbody tr").each(function(){
        var saldo = parseFloat($(this).find(".saldo").val());
        var valor = parseFloat($(this).find(".valor").val());
        var check = $(this).find(".checks");
        if(check.prop('checked'))
        {
            t_saldo += saldo;
            t_pago += valor;
        }
    });
    $("#saldo_anterior").text("$ "+t_saldo.toFixed(2));
    $("#total_pago").text("$ "+t_pago.toFixed(2));
    $("#saldo_actual").text("$ "+(parseFloat(t_saldo - t_pago).toFixed(2)))
}

/***************************************************
 * Valida que la fecha del cheque no sea menor a la
 * fecha de registro
 */
function validar_fecha_cheque()
{
    var fecha_reg = $("#id_fecha");
    var fecha_cheque = $("#id_fecha_cheque");
    if (fecha_reg.val() != "" && fecha_cheque.val() != "")
    {
        if (comparar_fechas(fecha_reg.val(), fecha_cheque.val(), "mayor"))
        {
            mensaje_alerta("La fecha del cheque no puede ser menor a la del registro");
            fecha_cheque.val("");
        }
    }
}

/************************************
 * Agrega funciones a los elementos
 * que son llamados mediante ajax
 * @constructor
 */
function AgregarFunciones()
{
    var opt = $("#id_forma_pago_cabecera");
    var tabla_documentos = $("#lista_documentos");
    var tabla_cruce_doc = $("#tabla_cruce_documentos");
    var tabla_detalle_doc = $("#tabla_formset_detalle_doc");
    if (opt.val()=="1")
    {
        // Valida que solo ingrese numeros en los campos numericos
        tabla_documentos.find(".numerico").keydown(function(event) {
            solo_numero(event, this);
        });
        // Agrega valores de puntos decimales a los numeros y calcula los totales
        tabla_documentos.find(".numerico").change(function(event) {
            agregar_punto(this);
            totales_cruce_doc();
        });
        // Agrega funcion de validar los totales en los campor valor dentro de la tabla de cruce de docs
        tabla_cruce_doc.find(".valor").each(function(){
            $(this).change(function(){
                validar_total_pagar(this);
                totales_cruce_doc();
            });
            $(this).focusout(function(){
                validar_total_pagar(this);
                totales_cruce_doc();
            });
        });
        tabla_detalle_doc.find(".valor").each(function(){
            $(this).change(function(){
                validar_total_pagar(this);
                totales_cruce_doc();
            });
        });

        tabla_cruce_doc.find(".checks").each(function(){
            $(this).click(function(e){
                encerar_facturas();
                TieneDocPagar(e, this)
            });
        });

        tabla_detalle_doc.find(".checks").click(function(){
            encerar_facturas();
            PonerSaldoDoc(this);
        });

        tabla_detalle_doc.find(".valor").attr("readonly", true);

        $(".numerico").change(function(event) {
            agregar_punto(this);
            VerificarSaldo(event, this)
        });

        $("body").find("input").keypress(function(e){
            if ( e.which == 13 ) e.preventDefault();
        });
    }
    else // Funciones para cruce de Cuentas
    {
        // Valida que solo ingrese numeros en los campos numericos
        tabla_documentos.find(".numerico").keydown(function(event) {
            solo_numero(event, this);
        });
        // Agrega valores de puntos decimales a los numeros y calcula los totales
        tabla_documentos.find(".numerico").change(function(event) {
            redondear(this);
        });
        tabla_documentos.find(".checks").change(function(event) {
            CopiarSaldoValor(this);
            //VerificarSaldo(this);
            ActualizarTotalesCruceCta();
        });
        tabla_documentos.find(".valor").change(function(event) {
            VerificarSaldo(this);
            ActualizarTotalesCruceCta();
        });
        ActualizarTotalesCruceCta();
    }
}
