$(document).ready(function(e){
    // Totales de Cruce de documentos
    totales_cruce_doc();
    // Valida los campos antes de grabar
    $(".btn_grabar").click(function(event){
        $('#loading').show();
        $('body').css('display', '0.5');
        $(".btn_grabar").hide();
        $(".btn_cancelar").attr("href", "#").text("Por favor, espere...");
    });

    // Para que solo se ingresen números
    $(".numerico").keydown(function(event) {
        solo_numero(event, this);
    });

    $("#id_fecha_cheque").datepicker().on('changeDate', function(){
        $(this).datepicker("hide")});

    $("#id_fecha").datepicker().on('changeDate', function(){
        $(this).datepicker("hide");
        validar_fecha_actual(this);
    });

});

