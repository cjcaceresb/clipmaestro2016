/**
 * Created by Clip Maestro on 13/03/2015.
 */
var tipo_doc = ".tipo_doc";
var monto = ".monto";
var plazo = ".plazo";
var T_DETALLE_COMP = "#tabla_formset";

function validar_repetidos()
{
    var a = [];
    var b = [];
    $("#tabla_formset tbody tr").each(function(){
        var tipo_doc = $(this).find(".tipo_doc").val();
        var num_doc = $(this).find(".num_doc").val();
        if ( tipo_doc != "")
        {
            if ( $.inArray( tipo_doc, a ) != -1 )
            {
                if ($.inArray( num_doc, b ) != -1)
                {
                    if ($.inArray( tipo_doc, a ) == $.inArray( num_doc, b ))
                    {
                        mensaje_alerta("No puede ingresar dos documentos iguales del mismo proveedor, " +
                            "por favor verifique.");
                        $(this).find(".num_doc").val("");
                    }
                }
            }
            a.push(tipo_doc);
            b.push(num_doc);
        }
    });
}

function agregar_filas_saldoi_cxp(e)
{
    e.preventDefault();
    var clonar_tr = $(T_DETALLE_COMP+" tbody tr:first").clone();
    clonar_tr.find(".bootstrap-select").remove();
    clonar_tr.find(".eliminar").click(function(e)
    {
        EliminarFila(e, this, T_DETALLE_COMP);
    });

    clonar_tr.find("input").each(function( index ) {
        $(this).val("");
        $(this).parent().find(".error").remove();
    });

    $(clonar_tr).find(tipo_doc).prop('selectedIndex', 0);

    $(clonar_tr.find(".monto")).change(function(){
        redondear(this);
    });

    $(clonar_tr).find(".numerico").keydown(function(event) {
                Numerico(event)
    });

    $(clonar_tr).find(".numero_natural").keydown(function(event){
        NumeroEntero(event)
    });

    $(clonar_tr).find(".num_doc").mask("999-999-999999999").focusout(function(){
        AutocompletarCeros(this);
    });

    $(clonar_tr).find(".tipo_doc").change(function(){
        validar_repetidos();
    });

    $(clonar_tr).find(".num_doc").change(function() {
        validar_repetidos();
    });

    $(clonar_tr).find(".plazo").val("0");

    $(clonar_tr).find(".selectpicker").selectpicker("refresh");

    $(T_DETALLE_COMP+" tbody").append(clonar_tr);
    recalcular_ids(T_DETALLE_COMP);
}
