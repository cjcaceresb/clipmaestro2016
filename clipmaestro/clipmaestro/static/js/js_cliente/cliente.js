/**
 * Created by Roberto on 22/05/14.
 */
var ID_TABLA_DIRECCIONES = "#direcciones";
var ID_TABLA_CONTACTOS = "#contactos";
function isCedula(obj){
    var number = $(obj).val();
    var residuo=0;
    var respuesta=0;
    var suma_cedula=0;
    var result=0;
    var divi= number.substring(0,9);
    var porcion1 = number.substring(2,3);
    var rdivi3 = number.substring(9,10);//Digito a verificar cedula
    var contador=0;

        if(porcion1<6 && number.length == 10 && number.substring(0,2)<=24)
        {
            var tmp=0;
            var test=0;
                 for(var y=0; y<divi.length; y++){
                     if(y%2==0){
                        tmp=divi[y]*2;

                         if(tmp<=9){
                            suma_cedula= suma_cedula+tmp;
                         }
                         else{
                                result = tmp-9;
                                suma_cedula = suma_cedula+result;
                         }
                     }
                     else{
                            test= divi[y]*1;
                            suma_cedula=suma_cedula+test;
                     }
                 }

                 residuo = suma_cedula % 10;
                 if(residuo==0){
                     respuesta=0;
                     if(respuesta == rdivi3 || respuesta == 0){
                         contador++;
                     }
                 }
                 else{
                      respuesta = parseInt(10-residuo);
                      if(respuesta == rdivi3 || respuesta == 0){
                          contador++;
                      }
                 }
        }

        if(contador==0){
            return false;
        }
        else{
            return true;
        }
    }
function isPersonaJuridica(obj){
  var number = $(obj).val();
  var mruc = [4,3,2,7,6,5,4,3,2];
  var suma_total = 0;
  var residuo=0;
  var respuesta=0;
  var divi= number.substring(0,9);
  var porcion1 = number.substring(2,3);
  var rdivi2 = number.substring(9,10);// Digito a verificar ruc
  var contador=0;

    if(porcion1==9 && number.substring(10,13)==001  && number.substring(0,2)<=24){
        for(var k=0; k<divi.length; k++){
           suma_total = suma_total + (mruc[k]*parseInt(divi[k]));
           residuo = suma_total % 11;
           respuesta = parseInt(11-residuo);
        }
            if (rdivi2 == respuesta || residuo==0){
                contador++;
            }
    }
    if(contador==0)
        return false;
    else
        return true;

}
function isIntitucionPublica(obj){
  var number = $(obj).val();
  var nruc_empresa = [3,2,7,6,5,4,3,2 ];
  var suma_total = 0;
  var residuo=0;
  var respuesta=0;
  var digiempresa = number.substring(0,8);
  var porcion1 = number.substring(2,3);

  var rdivi1 = number.substring(8,9);// Digito a verificar empresa pública
  var contador=0;

      if(porcion1==6 && number.substring(9,13)==0001 && number.substring(0,2)<=24)
      {
          for(var k=0; k<digiempresa.length; k++)
          {
               suma_total = suma_total + (nruc_empresa[k]*parseInt(digiempresa[k]));
               residuo = suma_total % 11;
               respuesta = parseInt(11-residuo);
          }
            if (rdivi1 == respuesta || residuo==0){
                contador++;
            }
      }

      if(contador==0)

        return false;
      else
        return true;

}
function isPersonaNatural(obj){
    var number = $(obj).val();
    var residuo=0;
    var respuesta=0;
    var suma_cedula=0;
    var result=0;
    var divi= number.substring(0,9);
    var rdivi3 = number.substring(9,10);//Digito a verificar cedula
    var porcion1 = number.substring(2,3);
    var contador=0;
        if(porcion1<6 && number.substring(10,13)==001 && number.substring(0,2)<=24)
        {
            var tmp=0;
            var test=0;
                 for(var y=0; y<divi.length; y++){
                     if(y%2==0){
                        tmp=divi[y]*2;

                         if(tmp<=9){
                            suma_cedula= suma_cedula+tmp;
                         }
                         else{
                                result = tmp-9;
                                suma_cedula = suma_cedula+result;
                         }
                     }
                     else{
                            test= divi[y]*1;
                            suma_cedula=suma_cedula+test;
                     }
                 }

                 residuo = suma_cedula % 10;
                 if(residuo==0){
                     respuesta=0;
                     if(respuesta == rdivi3 || respuesta == 0){
                         contador++;
                     }
                 }
                 else{
                      respuesta = parseInt(10-residuo);
                      if(respuesta == rdivi3 || respuesta == 0){
                          contador++;
                      }
                 }
        }

        if(contador==0){
            return false;
        }
        else{
            return true;
        }
}
function isRUC(obj){
    var cont=0;
    if (isPersonaJuridica(obj) ){
        cont++;
    }
    if(isIntitucionPublica(obj)){
        cont++;
    }
     if(isPersonaNatural(obj)){
        cont++;
    }
    if(cont==0)
        return false;
    else
        return true;
}
function FormaPago(){
    var pago = $("#id_forma_pago").val();
    var plazo = $("#id_plazo");
    var monto = $("#id_monto_credito");

    if ($("#id_forma_pago").val()!=""){
        if (pago==1){
            $("label[for='Plazo']").attr("style", "display:none");
            $("#id_plazo").attr("style", "display:none; text-align: right;");
            $("label[for='Monto Crédito']").attr("style", "display:none");
            $("#id_monto_credito").attr("style", "display:none");
        }
        if (pago==2){
            $("label[for='Plazo']").attr("style", "display: show");
            $("#id_plazo").attr("style", "display: block; text-align: right;");
            $("label[for='Monto Crédito']").attr("style", "display: show");
            $("#id_monto_credito").attr("style", "display: block; text-align: right;");

            if ($(plazo).val()== "" || $(monto).val()== "" ){
                $("#id_monto_credito").val("");
                $("#id_plazo").val("");
            }

        }
    }else{
        $("label[for='Plazo']").attr("style", "display: none;");
        $("#id_plazo").attr("style", "display:none; text-align: right;");
        $("label[for='Monto Crédito']").attr("style", "display: none;");
        $("#id_monto_credito").attr("style", "display:none");
    }
}
/***********************************
 * Agrega puntos decimales a los
 * inputs
 * @param e
 */
function agregar_punto(e){
    var numero = $(e).val();

    if(numero!=""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));

}
function EliminarFila(e, fila, tableid){
    e.preventDefault();

    if($(tableid+" tbody > tr").not("td tr").length>1){
        $(fila).parent().parent().parent().parent().parent().parent().remove();
         recalcular_cliente_ids(tableid);

    }
    else{
        $("#tit-alert").html("Alerta");
        $("#body-alert").html("<strong>No se puede eliminar el Formulario.</strong>");
        $("#alert-yn").modal();
    }
}
function recalcular_cliente_ids(tableid){
    var nombre_form = "";
    $(tableid+" tbody > tr").not("td tr").each(function(index)
    {
        var cont = index;
        $(this).find(":input:not(:button, .input-block-level)").each(function()
        {
            var name_campo = $(this).attr("name").split("-");
            nombre_form = name_campo[0];
            var nombre_campo = name_campo[0]+"-" + cont + "-"+name_campo[2];
            $(this).attr("name", nombre_campo);
            $(this).attr("id", "id_"+nombre_campo);
        });
    });

     $('#id_'+nombre_form+'-TOTAL_FORMS').val($(tableid+" tbody > tr").not("td tr").length);
}

/*********************************** Clonar Dirección *************************************/
$(document).ready(function(){

    $("#id_margen_utilidad").keydown(function(event) {
        if ( $.inArray(event.keyCode,[46,8,9,27,13,110,190]) !== -1 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                 return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

    $(".eliminar").click(function(e){
        EliminarFila(e, this, ID_TABLA_DIRECCIONES);
    });

    $("#add_direccion").click(function(){
        var clonar = $(ID_TABLA_DIRECCIONES+" tbody tr:first").clone();
         clonar.find(".bootstrap-select").remove();

         $(clonar.find(".descripcion")).val("");
         $(clonar.find(".direccion1")).val("");
         $(clonar.find(".direccion2")).val("");
         $(clonar.find(".actividad")).val("");
         $(clonar.find(".telefono_direccion")).val("");
         $(clonar.find(".atencion")).val("");
         $(clonar.find(".ciudad")).prop('selectedIndex', 0).selectpicker("refresh");


        $(clonar.find(".numerico")).keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else
            {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });

        $(clonar.find(".eliminar")).click(function(e){
            EliminarFila(e, this, ID_TABLA_DIRECCIONES);
        });

        $("#direcciones").append(clonar);
        recalcular_cliente_ids(ID_TABLA_DIRECCIONES);
    });

    recalcular_cliente_ids(ID_TABLA_DIRECCIONES);


    $("#id_margen_utilidad").focusout(function(){
         if ($(this).val() > 100){
            $(this).val(0.0);
         }
         agregar_punto(this);
    });

    $("#id_monto_credito").focusout(function(){
        agregar_punto(this);
    });

});

/************************************************* Clonar Contactos ***************************/
$(document).ready(function(){

    $(".numerico").keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else
            {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
    });
    $(".eliminar_contacto").click(function(e){
        EliminarFila(e, this, ID_TABLA_CONTACTOS);
    });

     //$(".movil").mask("(99) 9999-9999");

    $("#add_contacto").click(function(e){
        e.preventDefault();
        var clonar_tr3 = $(ID_TABLA_CONTACTOS+" tbody tr:first").clone();
        clonar_tr3.find(".bootstrap-select").remove();

        $(clonar_tr3.find(".numerico")).keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else
            {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });

         $(clonar_tr3.find(".nombres")).val("");
         $(clonar_tr3.find(".apellidos")).val("");
         $(clonar_tr3.find(".email")).val("");
         $(clonar_tr3.find(".actividad")).val("");
         $(clonar_tr3.find(".telefono_contacto")).val("");
         $(clonar_tr3.find(".movil")).val("");
         $(clonar_tr3.find(".departamento")).prop('selectedIndex', 0).selectpicker("refresh");

        $(clonar_tr3.find(".movil")).mask("(99) 9999-9999");

         $(clonar_tr3.find(".eliminar_contacto")).click(function(e){
            EliminarFila(e, this, ID_TABLA_CONTACTOS);
         });

        /********************************  Númerico ************************************************************/
         $(clonar_tr3.find(".numerico")).keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,110,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });
         $(clonar_tr3.find(".numerico2")).keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });
         /*******************************************************************************************************/

         $(ID_TABLA_CONTACTOS).append(clonar_tr3);
         recalcular_cliente_ids(ID_TABLA_CONTACTOS);
    });
});