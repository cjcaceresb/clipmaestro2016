/**
 * Created by Roberto on 10/09/14.
 */
$(document).ready(function(e){

     $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").addClass("opcion-buscar-hide");
        $( ".container-buscador").removeClass("container-buscador-hide");
     });
     $(".buscador-cerrar").click(function(){
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
     });
     $('#limpiar').click(function(){
       limpiar_form($("#formulario"));
     });

     $("#eliminar").click(function(){

        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();

            var url = $("#lista input[type='radio']:checked").attr("data-eliminar");
            $("#tit-alert").html("Eliminar Cuenta Bancaria");
            $("#body-alert").html('<p>¿ Está seguro de eliminar la cuenta bancaria de :  "<strong>'+nombre+'</strong>"?</p>');
            $("#alert-yn").attr("data-tipo", 0);
            $("#alert-yn").attr("data-href", url);
            $("#alert-yn").modal();
        }

        else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para eliminar una cuenta bancaria.</p>');
            $("#alert-ok").modal();
        }

     });
     $("#ver").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();
            var url = $("#lista input[type='radio']:checked").attr("data-ver");
            window.location.href = url;
        }
        else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para ver una cuenta bancaria.</p>');
            $("#alert-ok").modal();
        }

     });
     $("#editar").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();
            var url = $("#lista input[type='radio']:checked").attr("data-editar");
            window.location.href = url;
        }
        else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para editar una cuenta bancaria.</p>');
            $("#alert-ok").modal();
        }
     });
     $("#estado").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var nombre = tr.find(".nombre").text();
            var prox_estado = tr.find(".estado").attr("data-pro_estado");
            var url = $("#lista input[type='radio']:checked").attr("data-estado");
            $("#tit-alert").html("Cambiar Estado");
            $("#body-alert").html('<p>¿ Desea '+prox_estado+' la cuenta bancaria:  "<strong>'+nombre+'</strong>"?</p>');
            $("#alert-yn").attr("data-href", url);
            $("#alert-yn").modal();
        }
        else
        {
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para cambiar de estado</p>');
            $("#alert-ok").modal();
        }
     });
     $("#ok-alert").on("click",function(){
        var url = $("#alert-yn").attr("data-href");
        var id = $("#alert-yn").attr("data-id");
            window.location.href = url;
     });




});