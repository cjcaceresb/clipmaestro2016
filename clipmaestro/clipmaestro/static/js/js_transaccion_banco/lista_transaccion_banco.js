/**
 * Created by Roberto on 10/09/14.
 */
function get_ultimo_cheque(){
    var num_cheque = "";
    $.ajax(
    {
        url: url_ultimo_cheque,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data : {
            'id_cta_banco': $(".cuenta_banco").val()
        },
        error: function(){
            num_cheque = "Error de Conexión"
        },
        success: function(data)
        {

            if(data.status == 1)
            {
                num_cheque = data.num_cheque;

            }
            else
            {
                num_cheque = "Escoja una cuenta válida"
            }
        }
    });
    return num_cheque;
}
function get_ultimo_comprobante(){
    var num_comp = "";
    var fecha_reg = $("#id_fecha_reg");
     $.ajax(
     {
        url: url_ultimo_comp,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        error: function(){
            num_comp = "Error de Conexión"
        },
        success: function(data)
        {
            if(data.status == 1)
            {
                num_comp = data.num_comp;
                $(fecha_reg).val(data.fecha_comp);
            }
        }
    });

    return num_comp;
}
function  anular_cheque(){
    var datastring = $("#form_anular_cheque").serialize();
    $.ajax(
    {
        url: url_anular_cheque,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: datastring,
        error: function(){
            alert("status error")
        },
        success: function(data)
        {
            if(data.status == 1)
            {
                window.location.reload(true);
            }
            else
            {
                $(".mensaje-error-retencion").text(data.mensaje);
                $(".error-modal").fadeIn("slow");

            }
        }
    });
 }

$(document).ready(function(e){
     $('.selectpicker').selectpicker();
     $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").addClass("opcion-buscar-hide");
        $( ".container-buscador").removeClass("container-buscador-hide");
     });
     $(".buscador-cerrar").click(function(){
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
     });
     $("#ver").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            window.location.href = $("#lista input[type='radio']:checked").attr("data-ver");
        }else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para ver el detalle de la transacción bancaria</p>');
            $("#alert-ok").modal();
        }
     });

    $("#ver-cheque").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            window.location.href = $("#lista input[type='radio']:checked").attr("data-ver-cheque");
        }else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para ver el detalle de la transacción bancaria</p>');
            $("#alert-ok").modal();
        }
     });



    $("#copiar").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var status = $("#lista input[type='radio']:checked").attr("data-status");
            var tipo_comp_id = $("#lista input[type='radio']:checked").attr("data-tipo-comp-id");

            if (status==1){
                if (tipo_comp_id != 26){
                    window.location.href = $("#lista input[type='radio']:checked").attr("data-copiar");
                }else{
                    $("#tit-alert-ok").html("ALERTA");
                    $("#body-alert-ok").html('<p>No puede copiar el comprobante, debido a que el mismo se realizó desde contabilidad.</p>');
                    $("#alert-ok").modal();
                }

            }else{
                $("#tit-alert-ok").html("ALERTA");
                $("#body-alert-ok").html('<p>No puede copiar la Transacción Bancaria, debido a que se encuentra anulada.</p>');
                $("#alert-ok").modal();
            }

        }else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para copiar la transacción bancaria</p>');
            $("#alert-ok").modal();
        }
     });

     $("#anular").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var num_comp = tr.find(".num_comp").text();
            var url = $("#lista input[type='radio']:checked").attr("data-anular");
            var status = $("#lista input[type='radio']:checked").attr("data-status");
            var tipo_comp_id = $("#lista input[type='radio']:checked").attr("data-tipo-comp-id");

            if (status == 1){
                if (tipo_comp_id != 26){
                    $("#tit-alert").html("Anular Transacción Bancaria");
                    $("#body-alert").html('<p>Desea anular la transacción con número de comprobante "<strong>'+num_comp+'</strong> "?</p>');
                    $("#alert-yn").attr("data-href", url);
                    $("#alert-yn").modal();

                }else{
                    $("#tit-alert-ok").html("ALERTA");
                    $("#body-alert-ok").html('<p>No puede anular el comprobante, debido a que el mismo ' +
                        'se realizó desde contabilidad.</p>');
                    $("#alert-ok").modal();
                }


            }else{
                $("#tit-alert-ok").html("ALERTA");
                $("#body-alert-ok").html('<p>La Transacción Bancaria que quiere anular ya se encuentra anulada</p>');
                $("#alert-ok").modal();
            }

        }
        else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para anular una transacción</p>');
            $("#alert-ok").modal();
        }
     });

    $("#anular_generar").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var num_comp = tr.find(".num_comp").text();
            var url = $("#lista input[type='radio']:checked").attr("data-anular-generar");
            var status = $("#lista input[type='radio']:checked").attr("data-status");
            var tipo_comp_id = $("#lista input[type='radio']:checked").attr("data-tipo-comp-id");

            if (status==1){

                if (tipo_comp_id != 26){
                    $("#tit-alert").html("Anular y Generar Transacción Bancaria");
                    $("#body-alert").html('<p>¿ Desea anular la transacción con número de comprobante:  "<strong>'+num_comp+'</strong>" y generar otra ?</p>');
                    $("#alert-yn").attr("data-href", url);
                    $("#alert-yn").modal();

                }else{
                    $("#tit-alert-ok").html("ALERTA");
                    $("#body-alert-ok").html('<p>No puede anular y generar el comprobante , ' +
                        'debido a que el mismo se realizó desde contabilidad.</p>');

                    $("#alert-ok").modal();
                }

            }else{
                $("#tit-alert-ok").html("ALERTA");
                $("#body-alert-ok").html('<p>La Transacción Bancaria que quiere anular y generar ya se encuentra anulada</p>');
                $("#alert-ok").modal();
            }
        }
        else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para anular y generar una transacción</p>');
            $("#alert-ok").modal();
        }
     });

    $('#limpiar').click(function(){
       limpiar_form($("#formulario"));
     });

     var now = new Date(2000,1,1, 0, 0, 0, 0);
     var checkin = $('#id_fecha_inicio').mask("9999-99-99").datepicker({
        onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
        }).on('changeDate', function(ev){

        if (ev.date.valueOf() > checkout.date.valueOf()){
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#id_fecha_fin')[0].focus();

        }).data('datepicker');
     var checkout = $('#id_fecha_fin').mask("9999-99-99").datepicker({
            onRender: function(date)
            {
                return date.valueOf() <= checkin.date.valueOf() ? '' : '';
            }
            }).on('changeDate', function(ev) {
                checkout.hide();
            }).data('datepicker');

     $(".evento_page").each(function(){
        $(this).click(function(){
            var page = $(this).attr("data-page");
            $("#id_page").val(page);
            $("#buscar").click();
        });
     });

     $("#id_fecha_reg").mask("9999-99-99").datepicker({
             todayBtn: true,
             autoclose: true

     }).on('changeDate', function(ev){
        $(this).datepicker("hide");

     });

    $("#anular-cheque").click(function(){
        $("#ultimo_cheque").text(get_ultimo_cheque());
        $("#ultimo_comprobante").text(get_ultimo_comprobante());
        $(".error-modal").hide();
        $("#anular_cheque_modal").modal();

    });

    $("#ok-cheque-anul").click(function(){
        anular_cheque();
    });

    $(".cuenta_banco").change(function(){
        $("#ultimo_cheque").text(get_ultimo_cheque());
    });

    $(".retencion-reload").click(function(){
        $("#ultimo_cheque").text(get_ultimo_cheque());
        $("#ultimo_comprobante").text(get_ultimo_comprobante());
    });

    /*****************************************/
    $("#ok-alert").on("click",function(){
        var url = $("#alert-yn").attr("data-href");
        window.location.href = url;
    });


});