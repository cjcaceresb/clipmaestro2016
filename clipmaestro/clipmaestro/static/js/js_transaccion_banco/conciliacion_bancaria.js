/**
 * Created by Roberto on 20/10/2014.
 */

/***
 * Function get_transacciones_bancarias(url)
 * Función Ajax que consulta las transacciones de banco a conciliar
 * @param url
 */
function get_transacciones_bancarias(url){
    var check_historial = $("#id_ver_historial");
    var fecha_corte = $("#id_fecha_cierre");
    var flag_check = 0.0;

    if ( $(check_historial).is(':checked')){
        flag_check=1;
    }else{
        flag_check = 0;
    }

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            id_cuenta_banco: $("#id_cuenta_banco").val(),
            check_historial: flag_check,
            fecha_corte: $(fecha_corte).val()
        },
        beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data)
        {

            $("#div_detalle_transacciones").html(data).hide();
            var div = $("#tiene_docs");

            if(div.attr("data-error") == "1"){
                $("#div_detalle_transacciones").html(data).show();
                $("#grabar").hide();

            }else{

                    $("#div_detalle_transacciones").html(data).show();
                    $("#grabar").show();

                    $(".numerico").each(function(){
                       $(this).keydown(function(event){
                            Numerico(event);
                       }).change(function(event){
                            redondear(this);
                       });
                    });

                    // Inhabilita el boton de submit evitando que se haga más de una petición
                    $("input[type='submit']").click(function(){
                        $(this).attr("disabled", false);
                        $("form").submit(function(){
                          $("input[type='submit']").attr("disabled", true).val("Por favor, espere...");
                          return true;
                        });
                    });
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
}

function validate_conciliar(){
    var conciliar = $("#conciliar");
    var cuenta_banco = $("#id_cuenta_banco");

    if ($(cuenta_banco).val() != ""){
        $(conciliar).show();
    }else{
        $(conciliar).hide();
    }

}

$(document).ready(function(){

    $('.selectpicker').selectpicker();
    $("#grabar").hide();

    validate_conciliar();

    $("#id_fecha_cierre" ).mask("9999-99-99").datepicker({
             todayBtn: true,
             autoclose: true
             ,date: {
                        format: 'YYYY-MM-DD',
                        message: 'The value is not a valid date'
            }
    }).on('changeDate', function(ev){

        if (validate_format_date($(this).val()) == false){
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Error: La fecha de corte es incorrecta por favor ingrese una fecha válida.</p>');
            $("#alert-ok").modal();
            $(this).val("");
            $(this).removeAttr("value");
        }


    });


    $("#id_fecha_cierre").focusout(function(){
        if (validate_format_date($(this).val()) == false){
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Error: La fecha de corte es incorrecta por favor ingrese una fecha válida.</p>');
            $("#alert-ok").modal();
            $(this).val("");
            $(this).removeAttr("value");
       }

    });

    $("#id_cuenta_banco").change(function(e){
        validate_conciliar();
    });

    $("#conciliar").click(function(e){
        get_transacciones_bancarias(url_get_transacciones_banco);
    });



});