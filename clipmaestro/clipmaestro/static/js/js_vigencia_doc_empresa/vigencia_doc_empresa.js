/**
 * Created by Clip Maestro on 10/06/2014.
 */
var fecha_emi = "#id_fecha_emision";
var fecha_ven = "#id_fecha_vencimiento";
var sec_ini = "#id_sec_ini";
var sec_fin = "#id_sec_final";
var sec_act ="#id_sec_actual";

function validar_fecha_actual(obj)
{
    try
    {
        var date_asiento = new Date(parse_date($(obj).val()));
        var dia_act_date = new Date(parse_date(fecha_actual));
        if(date_asiento > dia_act_date)
        {
            mensaje_alerta("No puede ingresar una fecha superior a la fecha actual");
            $(obj).val(fecha_actual);
        }
    }
    catch (err){
        mensaje_alerta("No puede ingresar una fecha superior a la fecha actual");
        $("#id_fecha_asiento").val(str_dia);
    }
}

function validar_fecha_emi_venc()
{
    try
    {
        var date_emi = new Date(parse_date($(fecha_emi).val()));
        var date_venc = new Date(parse_date($(fecha_ven).val()));
        if(date_emi >= date_venc)
        {
            mensaje_alerta("La fecha se emisión no puede ser mayor o igual a la fecha de " +
                "vencimiento, por favor corrija.");
            $(fecha_ven).val("");
        }
    }
    catch (err)
    {
        alert(err)
    }
}

function validar_secuencias()
{
    var sec_actual = parseInt($(sec_act).val());
    var sec_inicial = parseInt($(sec_ini).val());
    var sec_final = parseInt($(sec_fin).val());

    if( sec_actual + 1 < sec_inicial)
    {
        mensaje_alerta("El número de secuencia actual no puede ser menor " +
                       "a la secuencia inicial menos 1");
        $(sec_act).val("");
    }

    if( sec_actual >= sec_final)
    {
        mensaje_alerta("El número de secuencia actual no puede ser mayor igual " +
                       "a la secuencia final");
        $(sec_act).val("");
    }
    if( sec_final <= sec_inicial)
    {
        mensaje_alerta("El número de secuencia inicial no puede ser mayor igual" +
                       "a la secuencia final");
        $(sec_fin).val("");
    }
}
$(document).ready(function(){
    $(".numerico").keydown(function(event) {
        Numerico(event)
    });

    $('#id_serie').mask('999-999');

    $("#id_fecha_emision_elect").mask('9999-99-99').datepicker().on('changeDate', function(){
        $(this).datepicker("hide");
    });


    $(fecha_emi).mask('9999-99-99');
    $(fecha_emi).datepicker().on('changeDate', function(){
        $(this).datepicker("hide");
        validar_fecha_emi_venc()
    });
    $(fecha_ven).mask('9999-99-99');
    $(fecha_ven).datepicker().on('changeDate', function(){
        $(this).datepicker("hide");
        validar_fecha_emi_venc()
    });
    $(".selectpicker").selectpicker();

    var  template = '<div class="popover" role="tooltip" data-alert="alert">' +
        '           <div class="arrow">' +
        '               </div><h3 class="popover-title" data-alert="alert"></h3>' +
        '           <div class="popover-content" data-alert="alert"></div>' +
        '       </div>';
    $(sec_act).popover({trigger:'focus', template: template});
    $(sec_ini).popover({trigger:'focus', template: template});

    $(sec_act).change(function(){validar_secuencias()});
    $(sec_fin).change(function(){validar_secuencias()});
    $(sec_ini).change(function(){validar_secuencias()});
});
