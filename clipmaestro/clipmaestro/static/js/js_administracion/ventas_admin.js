/**
 * Created by Clip Maestro on 02/12/2014.
 */

function get_cliente_direccion()
{
    var combo_direccion = $("#id_cliente_direccion");
    var combo_cliente = $("#id_cliente");

    combo_direccion.empty();

    $.ajax(
    {
        url: "/administracion/contabilidad/venta_cabecera/obtener_direccion/",
        type: 'GET',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'cliente_id': combo_cliente.val()
        },
        error: function(){
            return true;
        },
        success: function(data){
            for(var i=0; i<data.length; i++)
            {
                if( i==0 )
                    combo_direccion.append('<option value="' + data[i].id + '" selected="selected" >' + data[i].value + '</option>');
                else
                    combo_direccion.append('<option value="' + data[i].id + '" >' + data[i].value + '</option>');
            }
        }
    });
}

$(document).ready(function(){
    get_cliente_direccion();
    var combo_cliente = $("#id_cliente");
    combo_cliente.change(function(){
        get_cliente_direccion();
    })
});