/**
 * Created by Clip Maestro on 27/06/2014.
 */
function cambio_tipo_pago(obj)
{
    var opt = parseInt($(obj).val());
    switch (opt){
        case 1:
            $(".plazo").hide();
            $(".vence").hide();
            $("#id_plazo").val("");
            break;
        case 2:
            $(".plazo").show();
            break;
        default :
            break;
    }
}
function pago_credito()
{
    var tipo_pago = $("#id_tipo_pago");
    if( tipo_pago.val() == 2 )
    {
        var dias = parseInt($("#id_plazo").val());
        var fecha_reg = new Date(parse_date($("#id_fecha_reg").val()));

        if( is_fecha(fecha_reg) && !isNaN(dias))
        {

            fecha_reg.setTime(fecha_reg.getTime() + (1000*60*60*24*dias));
            $("#vence").text(String(fecha_reg.getDate())+ " "
                    + meses[fecha_reg.getMonth()] + ", "
                    + String(fecha_reg.getFullYear()));

            $(".vence").show();
        }
        else
        {
            $(".vence").hide();
            $("#vence").text("");
        }
    }
    else
    {
        $("#vence").text("");
        $(".vence").hide();
    }
}
function calcular_totales()
{
    var costo_est = $("#id_costo_estimado").val();
    var valor = $("#id_valor").val();
    var impuesto = $("#id_iva").val();

    var tmp_impuesto = 0.0;

    if (impuesto == "01"){
        tmp_impuesto = 0.12;
    }else{
        tmp_impuesto = 0.0;
    }

    if( !(isNaN(costo_est) && isNaN(valor))){
         $("#id_monto_iva").val((parseFloat(valor)*parseFloat(tmp_impuesto)).toFixed(2));
         $("#id_total").val((parseFloat(valor) + (parseFloat(valor)*parseFloat(tmp_impuesto))).toFixed(2)); //calculoooo bien
         $("#id_utilidad").val(((parseFloat(valor) + (parseFloat(valor)*parseFloat(tmp_impuesto))) - parseFloat(costo_est)).toFixed(2));
    }

}





$(document).ready(function()
{
    var mens_fech = "La fecha de registro no puede ser mayor a la " +
                "fecha de entrega, por favor verifique";
    cambio_tipo_pago($("#id_tipo_pago"));
    pago_credito();
    calcular_totales();

    // Inhabilita el boton de submit evitando que se haga más de una petición
    $("input[type='submit']").click(function(){
        $(this).attr("disabled", false);
        $("form").submit(function(){
          $("input[type='submit']").attr("disabled", true).val("Por favor, espere...");
          return true;
        });
    });

    $(".datepicker").mask("9999-99-99");
    $(".selectpicker").selectpicker();
    $("#id_fecha_reg").on('changeDate change', function(){
        $(this).datepicker('hide');
        pago_credito();
        validar_fecha_actual(this, fecha_actual, "No puede ingresar una fecha superior a la fecha actual");
        validar_fecha_mayor(this, $("#id_fecha_ent"), mens_fech);
    });
    $("#id_fecha_ent").on('changeDate change', function(){
        $(this).datepicker('hide');
        validar_fecha_mayor( $("#id_fecha_reg"), this, mens_fech);
        pago_credito();
    });
    $("#id_tipo_pago").change(function(){
        cambio_tipo_pago(this);
    });
    $(".numerico").on("change focusout", function(event){
        redondear(this);
    }).keydown(function(event){
        Numerico(event);
    });
    $(".numero_n").on("keydown", function(event){
       NumeroNatural(event);
    });
    $("#id_plazo").change(function(e){
        pago_credito();
    });

    $(".iva").change(function(e){
       calcular_totales();
    });

    $("#id_costo_estimado").change(function(){calcular_totales()});
    $("#id_valor").change(function(){calcular_totales()});
    $("#id_iva").change(function(){calcular_totales()});
});