/**
 * Created by Clip Maestro on 27/06/2014.
 */
var meses =  ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre","Noviembre","Diciembre"];

/******************
 * Funcion que bloquea tipo de caracter que no sean numéricos
 * @param event
 * @constructor event
 */
function Numerico(event)
{
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39) || (event.keyCode == 110))
    {
         return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
/************************************
 * Función que evita que el usario ingrese un valor decimal
 * @param event
 * @param input
 */
function solo_numero(event, input)
{
    var contador = 0;
    // Allow: backspace, delete, tab, escape, enter and.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39) || (event.keyCode == 110))
        {
             // let it happen, don't do anything
            if(event.keyCode == 110){
                var texto = $(input).val();
                for(var i=0; i<texto.length; i++)
                {
                    if(texto[i]==".")
                        contador++;
                }
            }
            if(contador > 0)
                event.preventDefault();
            else
                return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}


/****************************
 * Función que presenta el alert personalizado de clipmaestro
 * @param msn
 */
function mensaje_alerta(msn)
{
    $("#tit-alert-ok").html("Alerta");
    $("#body-alert-ok").html("<p>" + msn + "</p>");
    $("#alert-ok").modal();
}

/**********************************************
 * Funcion que presenta un alert YES/NO personalizado de clipmaestro
 * @param msn
 * @param titulo
 * @param attr  -- atributo que se desee agregar al objeto
 * @param val  -- Valor del atributo
 */
function mensaje_alerta_yn(msn, titulo, attr, val)
{
    attr = attr || 0;
    val = val || 0;

    $("#tit-alert").html(titulo);
    $("#body-alert").html(msn);
    if (attr != 0)
        $("#alert-yn").attr(attr, val).modal();
    else
        $("#alert-yn").modal();
}

/****************
 * function NumeroEntero(event)
 * Sirve para que solo ingresen números enteros, sin punto flotante
 * para casos como el numde doc y el num de retención
 * @param event
 * @constructor  event
 */
function NumeroEntero(event){
    // Allow: backspace, delete, tab, escape, enter and.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39))
        {
            return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
/**********
 * FUNCION DE REDONDEO QUE REEMPLAZA A LA FUNCION Math.round()
 * de javascript por fallos al redondear
 * @param numero
 * @param presicion
 * @returns {number}
 */
function redondeo(numero, presicion){

    var multiplicador = Math.pow(10, presicion + 1);
    var num_multi = Math.floor(numero * multiplicador);
    var num_redondeado = (Math.round(num_multi/10)) * 10 / multiplicador;

    if (num_redondeado == 0)
        return 0;
    else
        if ( !isNaN(num_redondeado))
            return num_redondeado;
        else
            return 0;
}
/***********************************
 * función que redondea el valor que contiene
 * una caja de texto
 * inputs
 * @param obj
 */
function redondear(obj){
    var numero = $(obj).val();
    var presicion = 2;

    if (!isNaN(numero))
    {
        if (numero == 0)
            $(obj).val((0).toFixed(presicion));
        else
            $(obj).val(redondeo(numero, presicion).toFixed(2));
    }
}

/***********************************
 * Agrega puntos decimales a los
 * inputs
 * !!! Reemplazar esta función por redondear !!!
 * @param e
 */
function agregar_punto(e){
    var numero = $(e).val();
    if(numero!=""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));
}


function AutocompletarCeros(obj)
{
    var valor = $(obj).val();
    var valor_final = valor.replace(/_/g , "");
    var separador = valor_final.split("-");
    var a = "";
    if(separador.length > 1)
    {
        a = ("00000000"+separador[2]);
        $(obj).val(separador[0]+"-"+separador[1]+"-"+a.slice(-9));
    }
    // Para el numero de retencion
    else
    {
        a = ("00000000"+separador);
        $(obj).val(a.slice(-9));
    }
}

function NumeroNatural(event, input){
    var contador = 0;
    // Allow: backspace, delete, tab, escape, enter and.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39))
        {
            return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
/****************************
 * Funcion que devuelve en formato [anio. mes, dia]
 * para crear un objeto tipo "Date" en javascript
 * @param string
 * @returns {*}
 */
function parse_date(string)
{
    try{
        var str_date = string.split("-");
        return [parseInt(str_date[0]), parseInt(str_date[1]), parseInt(str_date[2])];
    }
    catch (err){
        return null;
    }
}


/***
 * Funcion que valida si el formato de fecha (yyy-mm-dd)
 * es Válido
 * @param fecha = str
 */
function validate_format_date(fecha){
    var ret = true;                             //RETURN VALUE
    var thisYear = new Date().getFullYear();    //YEAR NOW
    var minYear = 1960;                         //MIN YEAR

    // m[1] is year 'YYYY' * m[2] is month 'MM' * m[3] is day 'DD'
    var m = fecha.match(/(\d{4})-(\d{2})-(\d{2})/);

    // STRING FORMAT yyyy-mm-dd
    if(fecha == "" || fecha==null){
        return false;
    }

    // STR IS NOT FIT m IS NOT OBJECT
    if( m === null || typeof m !== 'object'){
        return false;
    }

    // CHECK m TYPE
    if (typeof m !== 'object' && m !== null && m.size!==3){
        return false;
    }

    // YEAR CHECK
    if( (m[1].length < 4) || m[1] < minYear || m[1] > thisYear){
        ret = false;
    }
    // MONTH CHECK
    if( (m[1].length < 2) || m[2] < 1 || m[2] > 12){
        ret = false;
    }
    // DAY CHECK
    if( (m[1].length < 2) || m[3] < 1 || m[3] > 31){
        ret = false;
    }

    return ret;
}

function is_fecha(date)
{
    if ( Object.prototype.toString.call(date) === "[object Date]")
    {
        if ( isNaN( date.getTime()) == false )
        {
            return (date.getTime() != 0); // returns true or false
        }
        else
            return false;
    }
    else
        return false;
}

function validar_fecha_actual(fecha, str_fecha_act, mensaje)
{
    try
    {
        var date_obj = new Date(parse_date($(fecha).val()));
        var dia_act = new Date(parse_date(str_fecha_act));
        if(date_obj > dia_act)
        {
            mensaje_alerta(mensaje);
            $(fecha).val(str_fecha_act);
        }
    }
    catch (err){
        mensaje_alerta(mensaje);
        $("#id_fecha_asiento").val(str_fecha);
    }
}

function validar_fecha_mayor(date_min, date_may, mensaje)
{
    try
    {   // verifica que las fechas sean válidas
        var dia_min = new Date(parse_date($(date_min).val()));
        var dia_may = new Date(parse_date($(date_may).val()));
        if(is_fecha(dia_min) && is_fecha(dia_may)) // verifica que sean fechas válidas
        {
            if(dia_min > dia_may)
            {
                mensaje_alerta(mensaje);
                $(date_may).val("");
            }
        }
    }
    catch (err){
        alert(err);
        mensaje_alerta(mensaje);
        $(date_may).val("");
    }
}

/****************************************
 * Compara dos fechas dependiendo del parámetro
 * comparador
 * @param fecha1
 * @param fecha2
 * @param comparador
 * @returns {boolean}
 */
function comparar_fechas(fecha1, fecha2, comparador)
{
    try
    {   // verifica que las fechas sean válidas
        var dia_min = new Date(parse_date(fecha1));
        var dia_may = new Date(parse_date(fecha2));

        if(is_fecha(dia_min) && is_fecha(dia_may)) // verifica que sean fechas válidas
        {
            switch (comparador)
            {
                case "mayor":
                {
                    return dia_min > dia_may;
                }
                case "menor":
                {
                    return dia_min < dia_may;
                }
                case "mayori":
                {
                    return dia_min >= dia_may;
                }
                case "menori":
                {
                    return dia_min < dia_may;
                }
                default :
                {
                    return false
                }
            }
        }
    }
    catch (err){
        return false
    }
}

/******************************
 * Cambia el atributo action con el atributo data-action y envia el formulario
 * esto sirve cuando se necesita utilizar un mismo formulario con diferentes url's
 * @param e
 * @param formulario
 * * @param obj
 */
function cambiar_action_form(e, formulario, obj){
    e.preventDefault();
    var url_action = formulario.attr("action");
    if(obj) // Si existe el objeto
    {
        if($(obj).attr("href") != url_action)
        {
            formulario.attr('action', $(obj).attr("href"));
        }
    }
    else  // Si es null se cambia el data-action con el action
    {
        url_action = formulario.attr("data-action");
        formulario.attr('action', url_action);
    }
}

/******************************
 * Limpiar los Formularios de Busqueda
 * @param form
 ******************************/
function limpiar_form(form){
  $(form).find('input[type=text]').each(function(){
      $(this).val("");
      $(this).attr("value", "");
  });
  $(form).find('input[type=checkbox]').each(function(){
      $(this).removeAttr('checked');
  });
  $(form).find('select').each(function(){
      $(this).find("option").each(function(){
          $(this).removeAttr("selected");
      });
      $(this).find("option:first").attr("selected", "selected");
  });
  $(form).find(".selectpicker").selectpicker("refresh");
}


/***********************
 * Obtiene el dominio de la web
 * @returns {string}
 */
function getDomain(){
    var url = window.location.href;
    var arr = url.split("/");
    return arr[0] + "//" + arr[2];
}


/********************************************
 * Obtiene el valor del parametro de la url
 * @param param
 * @returns {*}
 * @constructor
 ********************************************/
function obtener_parametros_url(param){
    var url_pag = window.location.search.substring(1);
    var url_variables = url_pag.split('&');
    for (var i=0; i < url_variables.length; i++){
        var nombre_param = url_variables[i].split('=');
        if (nombre_param[0] == param){
            return nombre_param[1];
        }
    }
}

/************************
 *  Para imprimir en pdf el comprobante
 *  url: es la dirección de la impresión del comprobante
 *  con un valor por default de 0
 *  ej: /compras/detalle_pdf/0
 * @returns {boolean}
 */
function imprimir_comprobante(url){
    var url_temp =  url;
    var url_fin = url_temp.substring(1, url_temp.length-2);
    var id = obtener_parametros_url("imp");
    if (id!=undefined){
        window.open(getDomain() + "/" + url_fin + id,'_blank');
        location.search = $.param([]);
    }
}

/************************
 *  Para imprimir en pdf la factura
 *  url: es la dirección de la impresión del documento
 *  con un valor por default de 0
 *  ej: /ventas/detalle_factura/0
 * @returns {boolean}
 */
function imprimir_factura(url)
{
    var url_temp =  url;
    var url_fin = url_temp.substring(1, url_temp.length-2);
    var id = obtener_parametros_url("fact");

    if (id != undefined){
        window.open(getDomain() + "/" + url_fin + id,'_blank');
        location.search = $.param([]);
    }


}

/*************************************
 * Funcion para recalcular los ids
 */
function recalcular_ids(tableid)
{
    var nombre_form = "";
    $(tableid+" tbody tr").each(function(index)
    {
        var cont = index;
        $(this).find(":input:not(:button, .input-block-level)").each(function()
        {
            var name_campo = $(this).attr("name").split("-");
            nombre_form = name_campo[0];
            var nombre_campo = name_campo[0]+"-" + cont + "-"+name_campo[2];
            $(this).attr("name", nombre_campo);
            $(this).attr("id", "id_"+nombre_campo);
        });
    });
    $('#id_'+nombre_form+'-TOTAL_FORMS').val($(tableid+" tbody").children().length);
}

/**
 * Función que elimina la fila de una
 * tabla utilizando el formset
 * @param e
 * @param fila
 * @param tableid
 * @constructor
 */
function EliminarFila(e, fila, tableid)
{
    e.preventDefault();
    if($(tableid+" tbody").children().length>1){
        $(fila).parent().parent().remove();
        recalcular_ids(tableid);
    }
    else{
        mensaje_alerta('No se puede eliminar la fila');
    }
}

/**
 * Función que valida fecha de vencimiento de documentos
 * tabla utilizando el formset
 * @param tableid
 * @constructor
 */

function validar_fecha_vencimiento_documento_tabla(tableid){

    $(tableid+' tbody tr td .fecha_vencimiento').each(function(){
        if ($(this).val()!=""){
            var fecha_ven = Date.parse($(this).val());
            var fecha_act = Date.parse(fecha_actual);
            if(fecha_ven < fecha_act){
                $(this).css('color','red');
            }
        }
    });
}

/************************+
 * Funcion que suma dias a un date object
 * @param date
 * @param dias
 */
function sumar_dias(date, dias)
{
    var miliseg = dias * 24 * 60 * 60 * 1000;
    date.setTime(date.getTime() + miliseg);
}

/***********************
 * Obtiene el mes del año
 * @param mes  entero que representa el mes
 * @returns {string}
 */
function get_mes(mes){
    try{
        return meses[mes]
    }catch (e){
        return "";
    }
}

/*****************************
 * Funcion que devuelve la data necesaria para el ajaxcombobox
 * @param extradata
 * @param subinfo
 * @returns {{lang: string, per_page: number, db_table: string, button_img: *, init_record: string, bind_to: string, scrollWindow: boolean, sub_info: boolean, primary_key: string, extradata: *}}
 */
function get_data_ajaxcb(extradata, subinfo){
    return {
            lang: 'es',
            per_page: 10,
            db_table: "fff",
            button_img: dir_combo_vineta,
            init_record: "",
            bind_to: "change_ajaxcombo",
            scrollWindow: true,
            sub_info: subinfo,
            primary_key: "id",
            extradata: extradata
        }
}
/**************************+
 * Esta función es para botener los datos por ajax
 * en funciones que no se necesite parametros de envio
 * como cuando se pide una porcion de html
 * @param url
 * @returns {undefined}
 */
function get_data_html(url)
{
    var clonar = undefined;
    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: '',
        error: function(){
            return true;
        },
        success: function(data){
            clonar = $(data);
        }
    });
    return clonar;
}

function alerta_validacion_fecha_actual(fecha, fecha_actual){
    try
    {
        if(fecha > fecha_actual)
        {
            mensaje_alerta("No puede ingresar una fecha superior a la fecha actual");
            return fecha_actual.strftime("%Y-%m-%d");
        }
        else
            return fecha.strftime("%Y-%m-%d");
    }
    catch (err){
        mensaje_alerta("No puede ingresar una fecha superior a la fecha actualeeee");
        return fecha_actual.strftime("%Y-%m-%d");
    }
}
/**********************************************
 * Carga la imagen de un input a un elemento img
 * @param input
 * @param element
 */
function cargar_imagen(input, element)
{
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(element).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function mensaje_error(msn, tag)
{
     $(tag).append('<div class="alert alert-danger alert-dismissable" style="margin-bottom:0px; margin-top: 20px">\
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>\
                  <div class="media">\
                       <figure class="pull-left alert--icon">\
                           <i class="pe-7s-close-circle"></i>\
                       </figure>\
                       <div class="media-body">\
                           <p id="error-pop_up" class="alert--text">'+msn+'</p>\
                       </div>\
                  </div>\
               </div>').hide().fadeIn("slow");

}