/**
 * Created by Roberto on 04/12/2014.
 */

/***
 * Función Ajax que enlista los sueldos de
 * los empleados
 * @param url
 */
function get_detalle_sueldos(url){
    var mes = $("#id_mes");
    var anio = $("#id_anio");

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            anio: $(anio).val(),
            mes: $(mes).val()
        },
        beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data)
        {

            $("#div_detalle_transacciones").html(data).hide();
            var div = $("#tiene_docs");

            if(div.attr("data-error") == "1"){
                $("#div_detalle_transacciones").html(data).show();
                $("#grabar").hide();

            }else{

                    $("#div_detalle_transacciones").html(data).show();
                    $("#grabar").show();

                    $(".numerico").each(function(){
                       $(this).keydown(function(event){
                            Numerico(event);
                       }).change(function(event){
                            redondear(this);
                       });
                    });

                    // Inhabilita el boton de submit evitando que se haga más de una petición
                    $("input[type='submit']").click(function(){
                        $(this).attr("disabled", false);
                        $("form").submit(function(){
                          $("input[type='submit']").attr("disabled", true).val("Por favor, espere...");
                          return true;
                        });
                    });
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });

}



