/**
 * Created by Roberto on 08/09/14.
 */
$(document).ready(function(){

     $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").addClass("opcion-buscar-hide");
        $( ".container-buscador").removeClass("container-buscador-hide");
     });
     $(".buscador-cerrar").click(function(){
            $( ".container-buscador" ).slideToggle(1000);
            $(".opcion-buscar").removeClass("opcion-buscar-hide");
     });
     $('.selectpicker').selectpicker();
     $('#limpiar').click(function(){
            limpiar_form($("#formulario"));
     });

      var now = new Date(2000,1,1, 0, 0, 0, 0);
      var checkin = $('#id_fecha_inicio').mask("9999-99-99").datepicker({
        onRender: function(date) {
        return date.valueOf() < now.valueOf() ? 'disabled' : '';
        }
        }).on('changeDate', function(ev){
        if (ev.date.valueOf() > checkout.date.valueOf()){
            var newDate = new Date(ev.date)
            newDate.setDate(newDate.getDate() + 1);
            checkout.setValue(newDate);
        }
        checkin.hide();
        $('#id_fecha_fin')[0].focus();
    }).data('datepicker');
      var checkout = $('#id_fecha_fin').mask("9999-99-99").datepicker({
        onRender: function(date)
        {
            return date.valueOf() <= checkin.date.valueOf() ? '' : '';
        }
        }).on('changeDate', function(ev) {
            checkout.hide();
        }).data('datepicker');
      $(".evento_page").each(function(){
            $(this).click(function(){
                var page = $(this).attr("data-page");
                $("#id_page").val(page);
                $("#buscar").click();
            });
      });
      $("#ver").click(function(){
        if ($("#lista input[type='radio']:checked").length != 0){
            window.location.href = $("#lista input[type='radio']:checked").attr("data-ver");
        }else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para ver el comprobante contable</p>');
            $("#alert-ok").modal();
        }
      });
      $("#copiar").click(function(){
            if ($("#lista input[type='radio']:checked").length != 0){
                var tr = $("#lista input[type='radio']:checked").parent().parent();
                var id_tipo = tr.find(".id_tipo").text();
                var status_asiento_modulo = $("#lista input[type='radio']:checked").attr("data-status-modulo");
                var url = $("#lista input[type='radio']:checked").attr("data-copiar");

                if (id_tipo == 6){
                    if (status_asiento_modulo == 1){
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>No puede copiar el asiento contable debido que el ' +
                                                    'mismo se generó con cuentas de módulo</p>');
                        $("#alert-ok").modal();

                    }else{
                        window.location.href = url;
                    }

                }
                else{
                    $("#tit-alert-ok").html("Alerta");
                    $("#body-alert-ok").html('<p>Sólo puede copiar comprobantes de asiento contable</p>');
                    $("#alert-ok").modal();
                }

            }else{
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Seleccione un item para copiar el comprobante contable</p>');
                $("#alert-ok").modal();
            }
      });
      $("#editar").click(function() {
        if ($("#lista input[type='radio']:checked").length != 0){
            var tr = $("#lista input[type='radio']:checked").parent().parent();
            var id_tipo = tr.find(".id_tipo").text();
            var status_asiento_modulo = $("#lista input[type='radio']:checked").attr("data-status-modulo");
            var url = $("#lista input[type='radio']:checked").attr("data-editar");

            if (id_tipo == 6 || id_tipo == 26){
                /*if (status_asiento_modulo == 1){
                    $("#tit-alert-ok").html("Alerta");
                    $("#body-alert-ok").html('<p>No puede editar el asiento contable debido que el ' +
                                                'mismo se generó con cuentas de módulo</p>');
                    $("#alert-ok").modal();
                }else{*/
                window.location.href = url;
                //}

            }else{
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Sólo puede editar los comprobantes de asiento contable</p>');
                $("#alert-ok").modal();
            }

        }else{
            $("#tit-alert-ok").html("Alerta");
            $("#body-alert-ok").html('<p>Seleccione un item para editar el comprobante contable</p>');
            $("#alert-ok").modal();
        }
      });
      $("#anular").click(function(){
            if ($("#lista input[type='radio']:checked").length != 0){
                var tr = $("#lista input[type='radio']:checked").parent().parent();
                var num_comp = tr.find(".nombre").text();
                var id_tipo = tr.find(".id_tipo").text();
                var estado = tr.find(".status").text();

                if (id_tipo==6 && estado==1 || id_tipo==26 && estado==1){
                    var url = $("#lista input[type='radio']:checked").attr("data-anular");
                    $("#tit-alert").html("Anular Comporbante Contable");
                    $("#body-alert").html('<p>¿ Desea anular el comprobante contable con número de comprobante ' +
                        '"<strong>'+num_comp+'</strong>" ?</p>');
                    $("#alert-yn").attr("data-href", url);
                    $("#alert-yn").modal();

                }else{

                    $("#tit-alert-ok").html("Alerta");
                    $("#body-alert-ok").html('<p>Sólo puede anular los comprobantes de asiento contable</p>');
                    $("#alert-ok").modal();
                }

            }else{
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Seleccione un item para anular el comprobante contable</p>');
                $("#alert-ok").modal();
            }
      });
      $("#anular_generar").click(function(){
            if ($("#lista input[type='radio']:checked").length != 0){
                var tr = $("#lista input[type='radio']:checked").parent().parent();
                var num_comp = tr.find(".nombre").text();
                var id_tipo = tr.find(".id_tipo").text();
                var estado = tr.find(".status").text();
                var status_asiento_modulo = $("#lista input[type='radio']:checked").attr("data-status-modulo");
                var url = $("#lista input[type='radio']:checked").attr("data-anular-generar");

                if(id_tipo==6 && estado==1){

                    if (status_asiento_modulo == 1){
                        $("#tit-alert-ok").html("Alerta");
                        $("#body-alert-ok").html('<p>No puede A/G el asiento contable debido que el ' +
                                                    'mismo se generó con cuentas de módulo</p>');
                        $("#alert-ok").modal();

                    }else{
                          $("#tit-alert").html("Anular y Generar Comprobante Contable");
                        $("#body-alert").html('<p>¿ Desea anular el comprobante contable con número de comprobante:  ' +
                            '"<strong>'+num_comp+'</strong>" y generar otro ?</p>');
                        $("#alert-yn").attr("data-href", url);
                        $("#alert-yn").modal();
                    }

                }else{

                    $("#tit-alert-ok").html("Alerta");
                    $("#body-alert-ok").html('<p>Sólo puede anular y generar los comprobantes de asiento contable</p>');
                    $("#alert-ok").modal();
                }

            }
            else{
                $("#tit-alert-ok").html("Alerta");
                $("#body-alert-ok").html('<p>Seleccione un item para anular y generar un comprobante contable nuevo.</p>');
                $("#alert-ok").modal();
            }
        });
      $("#ok-alert").on("click",function(){
            var url = $("#alert-yn").attr("data-href");
            window.location.href = url;
      });

});