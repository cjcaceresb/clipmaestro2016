    var modal_cta = "#modal-cuenta";
    var alert_modal = "#alert-yn";
    var url_add_cta = get_url_add_cta();
    var url_edit_cta = get_url_edit_cta();
    var url_del_cta = get_url_del_cta();
    var url_get_datos_cta = get_url_get_datos_cta();
    var url_plan_cta = get_url_plan_cta();
    var url_tipo_cuenta = get_url_tipo_cuenta();

    $(document).ready(function()
    {
        $('#arbol').treeview(
        {
            animated:"normal",
            persist: "cookie",
            control: "#treecontrol"
        });

        $(".numerico").keydown(function(event) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
                 // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39) || (event.keyCode == 110))
                {
                     // let it happen, don't do anything
                     return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });

    });
    function set_tipo_cuenta(id){
        var combo_tipo_cta = $("#id_tipo_cuenta");

        $.ajax(
        {
            url: url_tipo_cuenta,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 10000,
            data: {"id": id},
            beforeSend: function(msj){
                $('#loading').show();
                $('body').css('display', '0.7');
            },
            error: function(){
                $('#loading').fadeOut();
                $('body').css('opacity', '1');
                $("#error").fadeIn("slow");
                $("#error-cuenta").text("Error de Conexión, por favor intente más tarde.");
            },
            success: function(data)
            {
                if(data.status == 1)
                {
                    var id_tipo = data.tipo;
                    var cont = 0;
                    combo_tipo_cta.find("option").each(function(){
                        var value = parseInt($(this).attr("value"));
                        if(value == id_tipo){
                            $(this).attr("selected", "selected");
                            cont ++;
                        }
                    });

                    if (cont == 0){  // No match
                        combo_tipo_cta.val("");
                    }
                }
                else{
                    combo_tipo_cta.val("");
                }
                $('#loading').fadeOut();
                $('body').css('opacity', '1');
                $("#error").hide();
            }
        });
        //alert($("#id_tipo_cuenta").val(""));
        $(combo_tipo_cta).selectpicker("refresh");
    }

    function EnviarDatos(url, datastring)
    {
        var btn_acept = $("#send_form_cta");
        var texto = btn_acept.text();
        btn_acept.attr("disabled", true);
        $.ajax(
        {
            url: url,
            type: 'POST',
            async: false,
            cache: false,
            timeout: 10000,
            data: datastring,
            beforeSend: function(msj){
                $('#loading').show();
                $('body').css('display', '0.7');
                btn_acept.text("Por favor, espere...");
            },
            error: function(){
                btn_acept.text(texto);
                btn_acept.attr("disabled", false);
                $('#loading').fadeOut();
                $('body').css('opacity', '1');
                $("#error").fadeIn("slow");
                $("#error-cuenta").text("Error de Conexión, por favor intente más tarde.");
            },
            success: function(data)
            {
                if(data.status == 2)
                {
                    $("#error").fadeIn("slow");
                    $("#error-cuenta").text(data.mensaje);
                    $('#loading').fadeOut();
                    $('body').css('opacity', '1');
                    btn_acept.text(texto);
                    btn_acept.attr("disabled", false);
                }
                else
                {
                    window.location.href = url_plan_cta;
                }
            }
        });
    }

    $(function()
    {
        $('#id_descripcion').focusout(function(){
            if($(this).val()!=""){
                if($(this).hasClass("campo_requerido")){
                    $(this).removeClass("campo_requerido");
                }
            }
        });

        $("#cerrar").click(function(){
           $("#error").fadeOut("slow");
        });

        $('#alert-yn').on('hidden.bs.modal', function (e) {
            if($('#id_descripcion').hasClass("campo_requerido")){
                $('#id_descripcion').removeClass("campo_requerido");
            }
        });

        $(".selectpicker").selectpicker();

        $(".agregar").click(function(){
            $("#error").hide();
            if ($(this).attr("data-tipo") != "1")
            {
                var nivel = $(this).attr("data-nivel");
                $(modal_cta).attr("data-id", $(this).attr("name"));
                $(modal_cta).attr("data-action", 1);
                $("#tit-cuenta").html("Agregar una nueva cuenta contable");
                $('#id_descripcion').val("");
                $("#id_id_cta").val($(this).attr("name"));

                if(nivel=="4")
                {
                    $(".tipo_cuenta").show();
                    set_tipo_cuenta($(this).attr("name"));
                }
                else{
                    $(".tipo_cuenta").hide();
                }

                $(modal_cta).modal();
            }
        });

        $(".editar").click(function(){
            $("#error").hide();
            $(modal_cta).attr("data-id", $(this).attr("name"));
            $(modal_cta).attr("data-nivel", $(this).attr("data-nivel"));
            $(modal_cta).attr("data-action", 2);

            $("#id_id_cta").val($(this).attr("name"));

            $("#tit-cuenta").html("Editar cuenta contable");

            var id = $(this).attr("name");
            $.post(url_get_datos_cta,
            {
                'id': id
            },
            function(data){
                $("#id_descripcion").val(data.descripcion);
                $("#id_naturaleza").val(data.naturaleza);
                $("#id_naturaleza").selectpicker("refresh");

                if(data.tipo != 0)
                    $("#id_tipo_cuenta").val(data.tipo);
                else
                    $("#id_tipo_cuenta").val("");
                $("#id_tipo_cuenta").selectpicker("refresh");
            });

            $(modal_cta).modal();
        });

        $(".eliminar").click(function(){
            $("#error").hide();
            $(alert_modal).attr("data-id", $(this).attr("name"));
            $(alert_modal).attr("data-nivel", $(this).attr("data-nivel"));
            $(alert_modal).attr("data-action", 3);
            $(alert_modal).attr("data-target", $(this).attr("data-target"));
            $("#tit-alert").html("Eliminar cuenta contable");
            $("#body-alert").html("Desea eliminar la cuenta "+$(this).attr("data-name")+"?");
            $("#cuenta").html($(this).attr("data-name"));

            $("#body-ret").hide();

            $(alert_modal).modal();
        });

        $("#send_form_cta").click(function () {
            var opcion = parseInt($(modal_cta).attr("data-action")); // 1: agregar, 2: editar
            var tipo = 0;
            var datastring = $("#form_prov").serialize();

            switch (opcion){
                case 1: // Opción Agregar
                    if($(modal_cta).attr("data-tipo")!="")
                        tipo = parseInt($('#alert-yn').attr("data-tipo"));
                    EnviarDatos(url_add_cta, datastring);
                    $('#id_descripcion').val('');
                    break;
                case 2: // Opción Editar
                    var id = $(modal_cta).attr("data-id");
                    var nivel = parseInt($(modal_cta).attr("data-nivel"));
                    EnviarDatos(url_edit_cta, datastring);
                    break;
                default:
                    break;
            }
        });

        $('#ok-alert').click(function(){
            var id = $('#alert-yn').attr("data-id");
            var data = {"id": id};
            EnviarDatos(url_del_cta, data);
        });
    });

