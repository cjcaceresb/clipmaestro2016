/**
 * Created by Roberto on 08/09/14.
 */




$(document).ready(function(e){

    $('.selectpicker').selectpicker();

    $(".evento_page").each(function(){
        $(this).click(function(){
            var page = $(this).attr("data-page");
            $("#id_page").val(page);
            $("#buscar").click();
        });
    });

    $(".opcion-buscar").bind('click', function(event) {
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").addClass("opcion-buscar-hide");
        $( ".container-buscador").removeClass("container-buscador-hide");
        $( ".botones-administracion" ).slideToggle(1000);
    });
    $(".buscador-cerrar").click(function(){
        $( ".container-buscador" ).slideToggle(1000);
        $(".opcion-buscar").removeClass("opcion-buscar-hide");
        $( ".botones-administracion" ).slideToggle(1000);
    });

    $(".numerico").each(function(){
           $(this).keydown(function(event){
                Numerico(event);
           }).change(function(event){
                redondear(this);
           });
    });

     $('#limpiar').click(function(){
        limpiar_form($("#formulario"));
     });

    var now = new Date(2000,1,1, 0, 0, 0, 0);
        var checkin = $('#id_fecha_inicio').mask("9999-99-99").datepicker({
            onRender: function(date) {
            return date.valueOf() < now.valueOf() ? 'disabled' : '';
            }
        }).on('changeDate', function(ev)
        {
            if (ev.date.valueOf() > checkout.date.valueOf())
            {
                var newDate = new Date(ev.date)
                newDate.setDate(newDate.getDate());
                checkout.setValue(newDate);
            }
            checkin.hide();

            $('#id_fecha_fin')[0].focus();
        }).data('datepicker');

        var checkout = $('#id_fecha_fin').mask("9999-99-99").datepicker({
            onRender: function(date)
            {
                return date.valueOf() <= checkin.date.valueOf() ? '' : '';
            }
            }).on('changeDate', function(ev) {
                checkout.hide();
            }).data('datepicker');


        $(".opcion-anular").click(function(){
               $("#alert-yn").attr("data-id", $(this).attr("id"));
               $("#alert-yn").attr("data-target",1);
               $("#alert-yn").attr("data-name", $(this).attr("data-name"));
               $("#tit-alert").html("Anular");
               $("#body-alert").html('<p>Desea anular la transacción con número de comprobante: <strong>'+'  '+$(this).attr("name")+'</strong>' +'</p>');
               $("#alert-yn").modal();
        });

        $("#ok-alert").on("click",function(){
            var opt = $("#alert-yn").attr("data-target");
            var id = $("#alert-yn").attr("data-id");
            var flag = $("#alert-yn").attr("data-name");
            if (opt == 1){
                window.location.href = '/contabilidad/anular/'+id+'/'+flag;
            }
        });

});