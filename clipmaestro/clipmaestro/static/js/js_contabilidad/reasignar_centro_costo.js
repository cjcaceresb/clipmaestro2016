/**
 * Created by Roberto on 02/06/2015.
 */


function limp_rep(){
    $(".tabla-reasignar-centro-costo").each(function(){
        var i = 0, j= 0, k=0;
        var num_comp_ini = $(this).find(".num_comp:first").text();
        var fecha_reg_ini = $(this).find(".fecha_reg:first").text();
        var cliente_proveedor_ini = $(this).find(".cliente_proveedor:first").text();

        $(this).find(".num_comp").each(function(){
            if (i != 0){
                var num_comp = $(this).text();
                if(num_comp == num_comp_ini){
                    $(this).empty();


                }
                else{
                    num_comp_ini = num_comp;
                }
            }
            i++;
        });



    });
}

/***
 * Function get_transacciones_bancarias(url)
 * Función Ajax que consulta las transacciones de banco a conciliar
 * @param url
 */
function get_transacciones_reasignar_centro_costo(url){
    var check_centro_costo = $("#id_con_centro_costo");
    var flag_check = 0;

    if ($(check_centro_costo).is(':checked')){
        flag_check=1;
    }else{
        flag_check = 0;
    }

    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data:{
            fecha_ini: $("#id_fecha_ini").val(),
            fecha_fin: $("#id_fecha_final").val(),
            num_comp: $("#id_num_comp").val(),
            id_tipo_comp: $("#id_tipo_comp").val(),
            id_cuenta: $("#id_cuentas").val(),
            id_centro_costo: $("#id_centro_costo").val(),
            con_centro_costo: flag_check
        },
        beforeSend: function(msj){
            $('#loading').show();
            $('body').css('display', '0.5');
        },
        error: function(){
            return true;
        },
        success: function(data)
        {

            $("#div_detalle_transacciones").html(data).hide();
            var div = $("#tiene_docs");

            if(div.attr("data-error") == "1"){
                $("#div_detalle_transacciones").html(data).show();
                $('.selectpicker').selectpicker();
                $("#grabar").hide();

            }else{

                    $("#div_detalle_transacciones").html(data).show();
                    $('.selectpicker').selectpicker();
                    $("#grabar").show();
                    limp_rep();

                    $(".numerico").each(function(){
                       $(this).keydown(function(event){
                            Numerico(event);
                       }).change(function(event){
                            redondear(this);
                       });
                    });

                    // Inhabilita el boton de submit evitando que se haga más de una petición
                    $("input[type='submit']").click(function(){
                        $(this).attr("disabled", false);
                        $("form").submit(function(){
                          $("input[type='submit']").attr("disabled", true).val("Por favor, espere...");
                          return true;
                        });
                    });
            }

            $('#loading').fadeOut();
            $('body').css('opacity', '1');
        }
    });
}