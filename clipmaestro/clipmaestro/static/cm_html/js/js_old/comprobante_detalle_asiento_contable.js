var resultado_total_haber=0.0;
var resultado_total_debe=0.0;
resultado_haber=0;
resultado_debe=0;
function recalculardebehaber(){
    var total_debe = 0.0;
    var total_haber = 0.0;
    $("#tabla_formset tbody tr").each(function()
    {
       $(this).find(".debe").each(function(){
            if ($(this).val() != ""){
                 var debe = parseFloat($(this).val());
                 total_debe = total_debe + debe;
            }
       });

       $(this).find(".haber").each(function(){
           if ($(this).val()!=""){
               var haber = parseFloat($(this).val());
               total_haber = total_haber + haber;
           }
        });
    });

    $("#total_debe").html(parseFloat(total_debe).toFixed(2));
    $("#total_haber").html(parseFloat(total_haber).toFixed(2));

    resultado_total_debe=total_debe;
    resultado_total_haber=total_haber;

}

// Corregir Validar

function validar(){

    $("#tabla_formset tbody tr").each(function()
    {
        $(this).find("input").each(function(){
            var cuenta = $(this).find(".cod_cta");
            var debe = $(this).find(".debe");
            var haber = $(this).find(".haber");

            if ($(debe).val() == "" || $(haber).val() == "" || $(cuenta).val() == "" ){
                $(debe).attr('disabled',false);
                $(cuenta).attr('disabled',false);
                $(haber).attr('disabled',false);

            }
            else{

                if ($(debe).val() != "" && $(haber).val() == ""){
                    $(haber).attr('disabled',true);
                }
                if ($(haber).val() != "" && $(debe).val() == ""){
                   $(debe).attr('disabled',true);
                }

            }

        });
    });
}


function validarEliminar(num2){
    var num = parseInt(num2);
    if (num == 1){
        $("#alert-yn").attr("data-id", $(this).attr("id"));
        $("#alert-yn").attr("data-target",1);
        $("#tit-alert").html("Alerta");
        $("#body-alert").html('<p>No puede eliminar la fila</p>');
        $("#alert-yn").modal();
    }
}

function agregar_punto(e){
    var numero = $(e).val();
    var contador = 0.0;

    if(numero!=""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));

}

function alerts_modal(submit, event){
    var input_fecha = $("#id_fecha");
    var concepto = $("#id_concepto");
    var flag_debe_haber = false;
    var flag_detalle = false;
    var flag_valores = false;
    var flag_cuenta = false;


    if ($(input_fecha).val()=="" || $(concepto).val()==""){
         $(concepto).addClass("campo_requerido");
         $(concepto).focus();
    }
    else{
         $(concepto).removeClass("campo_requerido");
    }

    if ((resultado_total_debe != resultado_total_haber)){
       flag_debe_haber = true;
    }

    $("#tabla_formset tbody tr").each(function(index)
    {
        var codigo = $(this).find(".cod_cta");
        var detalle = $(this).find(".concepto");
        var debe_detalle = $(this).find(".debe");
        var haber_detalle = $(this).find(".haber");

        if ($(codigo).val()=="" && $(detalle).val()=="" && $(debe_detalle).val()=="" && $(haber_detalle).val()==""){
            flag_detalle = true;
        }
        else{

            if ($(codigo).val()==""){
                flag_cuenta = true;
            }
        }

        if ($(debe_detalle).val() == ""&& $(haber_detalle).val() == ""){
            flag_valores = true;
        }


    });

    if(flag_debe_haber || flag_detalle ){
        $("#alert-yn").attr("data-id", $(this).attr("id"));
        $("#alert-yn").attr("data-target",1);
        $("#tit-alert").html("Alerta");
        $("#body-alert").html('<p class="tick">Por Favor corrija lo siguiente:</p>');

        if(flag_debe_haber){
            $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Los valores del Debe y el Haber, ya que los mismos deben ser iguales.</strong></span></p>');
        }

        if(flag_detalle){
            $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Los valores del Detalle del Asiento Contable.</strong></span></p>');
        }

        if(flag_valores){
            $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Es requerido que ingrese valores del debe o  haber.</strong></span></p>');
        }

        if(flag_cuenta){
            $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Es requerido que ingrese la cuenta.</strong></span></p>');

        }

        $("#alert-yn").modal();
        event.preventDefault();
    }


}
function debeHaber(){
    var flag = false;

    $("#tabla_formset tbody tr").each(function(index)
    {
        var debe_detalle = $(this).find(".debe").val();
        var haber_detalle = $(this).find(".haber").val();

         if (debe_detalle == "" && haber_detalle == ""){
            flag = true;
         }
    });
    return flag;
}
$(document).ready(function(){
    $('.selectpicker').selectpicker();
    function log( message, e ) {
        };

    /**************** Autocompletado Cuenta  {cod - descripcion}  *************************/
    $(".cod_cta").autocomplete({
        source: "/contabilidad/buscarcuenta/",
        data:{tag:$(this).val()},
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            var padres = jQuery(event.target).parent().parent();
            var padre = padres[0];
            var padre_detalle_cta = padre.children[1];
            var cuenta = $(padre_detalle_cta.firstElementChild);
            $(this).val(ui.item.value);
            cuenta.val(ui.item.key);
        }
    });
    /*********************************************
     * Inhabilitar el "enter" en los inputs para no enviar
     * el formulario con un enter
     */
        $('form :input').keypress(function(e){
            if ( e.which == 13 ) e.preventDefault();
        });
    /************************************************************/

    /***************************************************************************************/

    function recalcular_ids(){
        $("#tabla_formset tbody tr").each(function(index)
        {
            var cont = index;
            cont_ultimo = $("#tabla_formset tbody tr").length;

            $(this).find("td").each(function( index ) {
                var hijo = $(this).children()[0];
                if (hijo.tagName == "INPUT" || hijo.tagName == "SELECT")
                {
                    var name_campo = $(hijo).attr("name").split("-"); //form-1-parada
                    name_campo[1] = cont;
                    var nombre_campo = name_campo[0]+"-"+name_campo[1]+"-"+name_campo[2];
                    $(hijo).attr("name", nombre_campo);
                    $(hijo).attr("id", "id_"+nombre_campo);

                    if (hijo.tagName == "SELECT"){
                        $(hijo).selectpicker('refresh');
                        if($(this).children()[2]){
                            $(this).children()[2].remove();
                        };
                    }
                }
            });
        });
    };

    $(".guardar").click(function(e){
        if(debeHaber()){
            e.preventDefault();

        }
             alerts_modal(this,e);
        });




    /******************************** Habilitar - Deshabilitar HABER ******************************/
    $(".debe").focusout(function(){
        padres = jQuery(this).parent().parent();
        padre = padres[0]; // tr
        padre_seleccion = padre.children[3];
        vdebe = $(padre_seleccion.firstElementChild);//debe
        padre_bs = padre.children[4];
        vhaber = $(padre_bs.firstElementChild);//haber
        resultado = 0;

        if($(this).val() != ""){
            vhaber.attr('disabled',true);
        }
        else
        {
            vhaber.attr('disabled',false);
        }

        if ($(this).val() == ""){
           // $("#total_debe").html(0.0);
            resultado_total_debe = 0.0;
        }
        else{

           $("#total_debe").html($(this).val());
           resultado_total_debe =  $(this).val();
        }
        recalculardebehaber();
       // agregar_punto(this);
    });
    /******************************************************************************************/

     /******************************** Habilitar - Deshabilitar DEBE *******************************/
    $(".haber").focusout(function(){
        padres = jQuery(this).parent().parent();
        padre = padres[0]; // tr
        padre_seleccion = padre.children[4];
        vdebe = $(padre_seleccion.firstElementChild);//debe
        padre_bs = padre.children[3];
        vhaber = $(padre_bs.firstElementChild);//haber
        resultado = 0;

        if($(this).val() != ""){
            vhaber.attr('disabled',true);
        }
        else
        {
            vhaber.attr('disabled',false);
        }

        if ($(this).val() == ""){
            //$("#total_haber").html(0.0);
            resultado_total_haber = 0.0;
        }
        else{
            $("#total_haber").html($(this).val());
            resultado_total_haber = $(this).val();
        }
        recalculardebehaber();
       // agregar_punto(this);
    });
    /********************************************************************************************/

    $("#add").click(function(e){
        e.preventDefault();
        var clonar_tr = $("#tabla_formset tbody tr:first").clone();
        clonar_tr.find(".eliminar").click(function(e){
            e.preventDefault();
            if($("#tabla_formset tbody").children().length>1){
                $(this).parent().parent().remove();
                recalcular_ids();
                recalculardebehaber();
                $("#id_form-TOTAL_FORMS").val($("#tabla_formset tbody").children().length);
            }
            else{
                validarEliminar(1);
            }
        });
        clonar_tr.find("input").each(function( index ) {
            $(this).val("");
            $(this).parent().find(".error").remove();
        });
        function log12( message, cuenta) {
            $( cuenta ).val( message );
        };

        /**************** Autocompletado Cuenta  {cod - descripcion}  *************************/
        $(clonar_tr.find( ".cod_cta" )).autocomplete({
            source: "/contabilidad/buscarcuenta/",
            data:{tag:$(this).val()},
            minLength: 2,
            select: function( event, ui ) {
                event.preventDefault();
                var padres = jQuery(event.target).parent().parent();
                var padre = padres[0];
                var padre_detalle_cta = padre.children[1];
                var cuenta = $(padre_detalle_cta.firstElementChild);
                $(this).val(ui.item.value);
                cuenta.val(ui.item.key);
            }
        });


        $(clonar_tr.find(".debe")).attr("disabled",false);
        $(clonar_tr.find(".haber")).attr("disabled",false);

        /***************** Habilitaciónn y Bloqueo de DEBE - HABER ****************************/

        $(clonar_tr.find(".debe")).focusout(function(){
            padres = jQuery(this).parent().parent();
            padre = padres[0]; // tr
            padre_seleccion = padre.children[3];
            vdebe = $(padre_seleccion.firstElementChild);//debe
            padre_bs = padre.children[4];
            vhaber = $(padre_bs.firstElementChild);//haber
            resultado = 0;

            if($(this).val() != ""){
                vhaber.attr('disabled',true);
            }
            else
            {
                vhaber.attr('disabled',false);
            };
            recalculardebehaber();
           // agregar_punto(this);
        });
        $(clonar_tr.find(".haber")).focusout(function(){
            padres = jQuery(this).parent().parent();
            padre = padres[0]; // tr
            padre_seleccion = padre.children[4];
            v_haber = $(padre_seleccion.firstElementChild);
            padre_bs = padre.children[3];
            v_debe = $(padre_bs.firstElementChild);
            resultado2 = 0;

            if($(this).val() != ""){
                v_debe.attr('disabled',true);
            }
            else
            {
                v_debe.attr('disabled',false);
            };
           recalculardebehaber();
          // agregar_punto(this);
        });
        /***************************************************************************************/


        $("#tabla_formset tbody").append(clonar_tr);
        recalcular_ids();
        $("#id_form-TOTAL_FORMS").val($("#tabla_formset tbody").children().length);
    });
    $(".eliminar").click(function(e){
        e.preventDefault();
        if($("#tabla_formset tbody").children().length>1){
            $(this).parent().parent().remove();
            recalcular_ids();
            recalculardebehaber();
            $("#id_form-TOTAL_FORMS").val($("#tabla_formset tbody").children().length);
        }
        else{
            validarEliminar(1);
        }
    });

});



