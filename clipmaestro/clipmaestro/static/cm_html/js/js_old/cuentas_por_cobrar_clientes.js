/*******************************
 * Suma los totales a pagar
 */
function totales()
{
    var totales = 0.0;
    $("#tabla_formset_detalle_doc").find(".valor").each(function(){
        totales = totales + parseFloat($(this).val());
    });
    $("#total_pagar").text("$ " + totales.toFixed(2));
}


/***********************************
 * Agrega puntos decimales a los
 * inputs
 * @param e
 */
function agregar_punto(e)
{
    var numero = $(e).val();
    var contador = 0.0;

    if(numero!=""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));

}
function solo_numero(event, input)
{
    var contador = 0;
    // Allow: backspace, delete, tab, escape, enter and.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39) || (event.keyCode == 110))
        {
             // let it happen, don't do anything
            if(event.keyCode == 110){
                var texto = $(input).val();
                for(var i=0; i<texto.length; i++)
                {
                    if(texto[i]==".")
                        contador++;
                }
            }
            if(contador > 0)
                event.preventDefault();
            else
                return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
function NumeroNatural(event, input)
{
    var contador = 0;
    // Allow: backspace, delete, tab, escape, enter and.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39))
        {
            return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
/********************************************
 * Para el cambio de las columnas de
 *         la forma de pago
 ******************************************/
function cambio_combo(e){
        var n = parseInt($(e).val());
        var tabla = $("#detalle_forma_pago");
        $(tabla).find('th').attr("style", "display: none");
        $(tabla).find('td').attr("style", "display: none");
        switch (n)
        {
            case 1:     // Pago en Cheque
                $(tabla).find('.th_forma_pago').css("display","table-cell");
                $(tabla).find('.th_bancos').css("display","table-cell");
                $(tabla).find('.th_cheque').css("display","table-cell");
                $(tabla).find('.th_fecha_cheque').css("display","table-cell");
                break;
            case 2:     // Pago en efectivo
                $(tabla).find('.th_forma_pago').css("display","table-cell");
                $(tabla).find('.th_referencia').css("display","table-cell");
                break;
            case 3:     // Pago en Tarjeta de Crédito
                $(tabla).find('.th_forma_pago').css("display","table-cell");
                $(tabla).find('.th_tarjetas').css("display","table-cell");
                $(tabla).find('.th_termino').css("display","table-cell");
                $(tabla).find('.th_plazo').css("display","table-cell");
                $(tabla).find('.th_interes').css("display","table-cell");
                break;
            case 4:     // Transferencia Bancaria
                $(tabla).find('.th_forma_pago').css("display","table-cell");
                $(tabla).find('.th_bancos').css("display","table-cell");
                $(tabla).find('.th_referencia').css("display","table-cell");
                break;
            default :
                break;
        }
}
function SendData(submit, event)
{
    var input_fecha = $("#id_fecha");
    var proveedor = $("#id_proveedor");
    var tabla_doc_pagar = $("#tabla_formset_detalle_doc");
    var tabla_forma_pago = $("#detalle_forma_pago");
    var combo_forma_pago = $(tabla_forma_pago).find("#id_forma_pago");
    var flag_fecha = false;
    var flag_cheque = false;
    var flag_fecha_cheque = false;
    var flag_referencia = false;
    var flag_interes = false;
    var total_pagar = 0.0;

    $(tabla_doc_pagar).find(".valor").each(function(){
        if($(this).val() != ""){
            total_pagar += parseFloat($(this).val());
        }
    });

    if(total_pagar == 0){
        $("#alert-yn").attr("data-id", $(this).attr("id"));
        $("#alert-yn").attr("data-target",1);
        $("#tit-alert").html("Alerta");
        $("#body-alert").html('<p>Debe de ingresar el valor a pagar a las facturas</p>');
        $("#alert-yn").modal();
        event.preventDefault();
    }
    else
    {
        switch (parseInt($(combo_forma_pago).val()))
        {
            case 1: // Banco
                if($("#id_cheque").val()==""){
                    flag_cheque = true;
                }
                if($("#id_fecha_cheque").val()==""){
                    flag_fecha_cheque = true;
                }
                break;
            case 2: // Efectivo "Caja Chica"
                if($("#id_referencia").val()==""){
                    flag_referencia = true;
                }
                break;
            case 3: // Tarjeta de Crédito
                if($("#id_interes").val()==""){
                    flag_interes = true;
                }
                break;
            case 4: // Transferecnia
                if($("#id_referencia").val()==""){
                    flag_referencia = true;
                }
                break;
        }
        if($(input_fecha).val()==""){
            flag_fecha = true;
        }
        if(flag_cheque || flag_fecha ||flag_fecha_cheque || flag_interes || flag_referencia){
            $("#alert-yn").attr("data-id", $(this).attr("id"));
            $("#alert-yn").attr("data-target",1);
            $("#tit-alert").html("Alerta");
            $("#body-alert").html('<p class="tick">Debe de ingresar los siguientes campos:</p>');
            if(flag_fecha)
            {
                $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Fecha</span></strong></p>');
                $(input_fecha).addClass("campo_requerido");
                $(input_fecha).focus();
            }
            if(flag_cheque)
                $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Número del Cheque</strong></span></p>');
            if(flag_fecha_cheque)
                $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Fecha del Cheque</strong></span></p>');
            if(flag_interes)
                $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Interes de la Tarjeta de Crédito</strong></p>');
            if(flag_referencia)
                $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Referencia</span></strong></p>');
            $("#alert-yn").modal();
            event.preventDefault();
        }
    }
}


$(document).ready(function(e){
    /****************
     * Para inhabilitar que el enter haga submit
     */
    $("body").find("input").keypress(function(e){
         if ( e.which == 13 ) e.preventDefault();
    });
    $(".selectpicker").selectpicker();

    $("#grabar").click(function(e){
        SendData(this,e);
    });

    $(".numerico").keydown(function(event) {
        solo_numero(event, this);
    });
    $(".numero_cheque").keydown(function(event) {
        NumeroNatural(event, this);
    });

    $("#id_forma_pago").change(function(){
        cambio_combo(this);
    });
    $("#id_fecha_cheque").datepicker();

    $(".numerico").focusout(function(event) {
        agregar_punto(this);
        totales();
        var tr = $(this).parent().parent();
        var saldo = parseFloat($(tr).find(".saldo").val());
        var check = $(tr).find(".checks");
        if(saldo)
        {
            if(parseFloat($(this).val()) > saldo){
                $("#alert-yn").attr("data-id", $(this).attr("id"));
                $("#alert-yn").attr("data-target",1);
                $("#tit-alert").html("Alerta");
                $("#body-alert").html('<p>El valor ingresado en superior al saldo del documento</p>');
                $("#alert-yn").modal();
                event.preventDefault();
                $(this).val("0.00");
                $(this).focus();
            }
            else{
                if(parseFloat($(this).val())>0)
                {
                    if (!$(check).is(':checked'))
                        $(check).prop('checked', true);
                }
                else
                {
                    $(check).prop('checked', false);
                }
            }
        }
    });
    var tabla = $("#tabla_formset_detalle_doc");
    $(".checks").each(function(){
        $(this).click(function(){
            var tr = $(this).parent().parent();
            var tipo = $(tr).find(".tipo")
            if($(tipo).val()=="-1"){
                if($(this).is(':checked'))
                {
                    $(tr).find(".valor").attr("disabled", false);
                    $(tr).find(".valor").focus();
                }
                else
                {
                    $(tr).find(".valor").attr("disabled", true);
                    $(tr).find(".valor").val("0.00");
                }
            }
            var total_pagar = 0;
            var tr = $(this).parent().parent().get(0);
            var total = $(tr).find(".saldo").val();
            if($(this).is(':checked'))
            {
                $(tr).find(".valor").val(total);
                $($(tr).find(".valor")).focus();
            }
            else
            {
                $(tr).find(".valor").val("0.0");
            }
            $(tabla).find(".valor").each(function(){
                if($(this).val()!="")
                    total_pagar = parseFloat(total_pagar) + parseFloat($(this).val());
            });
            $("#total_pagar").text("$ "+parseFloat(total_pagar).toFixed(2));
        });
    });


     $('#id_fecha').datepicker({
        todayBtn: true,
        autoclose: true
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });

    $('#id_fecha_cheque').datepicker({
        todayBtn: true,
        autoclose: true
    }).on('changeDate', function (ev) {
        $(this).datepicker('hide');
    });
    $("#id_fecha").change(function(){
        if($(this).val()!="" && $(this).hasClass("campo_requerido")){
            $(this).removeClass("campo_requerido");
        }
    });
    /*****************************************
     * Autocomplete de proveedores
     *****************************************/
    $("#id_proveedor").autocomplete({
    source: "/cuentas_pagar/pago_proveedores/buscarproveedores/",
    data:{tag:$(this).val()},
    minLength: 2,
    select: function( event, ui ) {
        event.preventDefault();
        var padres = jQuery(event.target).parent().parent();
        var padre = padres[0];
        var padre_detalle_cta = padre.children[1];
        var cuenta = $(padre_detalle_cta.firstElementChild);
        $(this).val(ui.item.value);
            cuenta.val(ui.item.key);
        }
    });
    // Carga la tabla de documentos por pagar
    $("#cargar_pagos").click(function(){
        cargar_tabla_pagos($("#id_proveedor"));
    });

});

