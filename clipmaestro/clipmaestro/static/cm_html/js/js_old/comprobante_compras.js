//Variables globales
var IVA = 0.12;
var border, outline;
var FECHA_ASIENTO = "#id_fecha_asiento";
var FECHA_EMI = "#id_fecha";
var FECHA_RET = "#id_fecha_emision_retencion";
var SERIE_RET = "#id_serie_ret";
var AUT_RET = "#id_autorizacion_retencion";
var T_REEMBOLSO = "#total_reemb_b";
var TR_BASE0 = "#trbase0";
var TR_BASE12 = "#trbase12";
var S_FORMA_PAGO = "#id_forma_pago";
var S_PAIS = "#id_pais";
var NUM_DOC = "#id_serie";
var NUM_DOC_MODIF = "#id_serie_modif";
var NUM_RET = "#id_numero_retencion";
var S_SUST_TRIB = "#id_sust_tribut";
var S_TIPO_DOC = "#id_tipo_de_doc";
var T_DETALLE_COMP = "#tabla_formset";
var T_DETALLE_REEM = "#tabla_formset_reembolso";
var S_COD_RET_FTE = "select[data-name='combo_cod_ret_fte']"
var S_COD_RET_IVA = "select[data-name='combo_cod_ret_iva']"

/********************
 * Funcion para cargar los cambos de iva, ret_fte, ret_iva
 */

function getComboInpuestos(sfecha_asiento, select_combo_iva, url)
{
    $(select_combo_iva).find("option").remove();
    $.ajax(
    {
        url: url,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'fecha_asiento': sfecha_asiento
        },
        error: function(){
            return true;
        },
        success: function(data){
            for(var j=0; j<select_combo_iva.length; j++)
            {
                for(var i=0; i<data.length; i++)
            {
                $(select_combo_iva[j]).append('<option value="' + data[i].id + '" data-codigo="' + data[i].codigo +'" data-porcentaje="' + data[i].porcentaje + '" >' + data[i].descripcion + '</option>');
            }
            $(select_combo_iva[j]).selectpicker("refresh");
            }
        }
    });
}

function getDatosProveedor(num_doc, id_prov, tipo_doc)
{
    $.ajax(
    {
        url: "/compras/get_datos_proveedor/",
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'fecha_asiento': sfecha_asiento
        },
        error: function(){
            return true;
        },
        success: function(data){
            for(var j=0; j<select_combo_iva.length; j++)
            {
                for(var i=0; i<data.length; i++)
            {
                $(select_combo_iva[j]).append('<option value="' + data[i].id + '" data-codigo="' + data[i].codigo +'" data-porcentaje="' + data[i].porcentaje + '" >' + data[i].descripcion + '</option>');
            }
            $(select_combo_iva[j]).selectpicker("refresh");
            }
        }
    });
}



function CambioFechaAsiento()
{
    var sfecha_asiento =  $(FECHA_ASIENTO).val();
    var fecha_comp_asiento = sfecha_asiento.split("-");
    var dia_fecha_asiento = new Date(parseInt(fecha_comp_asiento[0]),parseInt(fecha_comp_asiento[1]),parseInt(fecha_comp_asiento[2]));

    var sfecha_emi_doc = $(FECHA_EMI).val();
    var sub_fec_em_doc = sfecha_emi_doc.split("-");
    var dia_fecha_emision = new Date(parseInt(sub_fec_em_doc[0]),parseInt(sub_fec_em_doc[1]),parseInt(sub_fec_em_doc[2]));

    var sfecha_emi_ret = $(FECHA_RET).val();
    var sub_fec_em_ret = sfecha_emi_ret.split("-");
    var dia_fecha_retencion = new Date(parseInt(sub_fec_em_ret[0]),parseInt(sub_fec_em_ret[1]),parseInt(sub_fec_em_ret[2]));

    $(T_DETALLE_REEM+" tbody").find("tr").each(function(){
        var string_fecha_doc_reemb = $(this).find(".fecha_emision").val();
        var sub_fec_em_reem = string_fecha_doc_reemb.split("-");
        var dia_fecha_reembolso = new Date(parseInt(sub_fec_em_reem[0]),parseInt(sub_fec_em_reem[1]),parseInt(sub_fec_em_reem[2]));

        if(dia_fecha_asiento < dia_fecha_reembolso){
            $(this).find(".fecha_emision").val(stringfecha_asiento);
        }
    });
    /*************************
     * Codigo para cambiar combo retencion, codigoiva y retencion iva
     */

    var select_combo_iva = $(T_DETALLE_COMP).find("select[data-name='combo_iva']");
    for(var i=0; i<select_combo_iva.length; i++)
    {
        $(select_combo_iva[i]).find("option").remove();
    }
    getComboInpuestos(sfecha_asiento, select_combo_iva, "/compras/getcomboiva/");


    var select_combo_ret_fte = $(T_DETALLE_COMP).find(S_COD_RET_FTE);
    for(var i=0; i<select_combo_ret_fte.length; i++)
    {
        $(select_combo_ret_fte[i]).find("option").remove();
    }
    getComboInpuestos(sfecha_asiento, select_combo_ret_fte, "/compras/getcombo_ret_fte/");


    var select_combo_ret_iva = $(T_DETALLE_COMP).find(S_COD_RET_IVA);
    for(var i=0; i<select_combo_ret_iva.length; i++)
    {
        $(select_combo_ret_iva[i]).find("option").remove();
    }
    getComboInpuestos(sfecha_asiento, select_combo_ret_iva, "/compras/getcombo_ret_iva/");


    if(dia_fecha_asiento < dia_fecha_emision){
        $(FECHA_EMI).val(sfecha_asiento);
    }
    if(dia_fecha_asiento< dia_fecha_retencion){
        $(FECHA_RET).val(sfecha_asiento);
    }
}
/*********
$(function(){
    var sfecha_asiento =  $(FECHA_ASIENTO).val();
    var select_combo_iva = $(T_DETALLE_COMP).find("select[data-name='combo_iva']");
    for(var i=0; i<select_combo_iva.length; i++)
    {
        $(select_combo_iva[i]).find("option").remove();
    }
    getComboInpuestos(sfecha_asiento, select_combo_iva, "/compras/getcomboiva/");

    var select_combo_ret_fte = $(T_DETALLE_COMP).find(S_COD_RET_FTE);
    for(var i=0; i<select_combo_ret_fte.length; i++)
    {
        $(select_combo_ret_fte[i]).find("option").remove();
    }
    getComboInpuestos(sfecha_asiento, select_combo_ret_fte, "/compras/getcombo_ret_fte/");

    var select_combo_ret_iva = $(T_DETALLE_COMP).find(S_COD_RET_IVA);
    for(var i=0; i<select_combo_ret_iva.length; i++)
    {
        $(select_combo_ret_iva[i]).find("option").remove();
    }
    getComboInpuestos(sfecha_asiento, select_combo_ret_iva, "/compras/getcombo_ret_iva/");

});

/*******************************
 * función para cargar la autorización
 * de la retención
 * @constructor
 */
function CargarAutorizacion()
{
    var serie = $(SERIE_RET).val();
    $.post('/compras/buscar_autorizacion/',
        {
            'serie': serie
        },
        function(data)
        {
        $(AUT_RET).val(data.autorizacion);
    });
}

/**************************************
 * Funcion para validar que la fecha
 *    no supere a la fecha del asiento
 *     contable
 * @param obj
 * @returns {null}
 */
function validar_fecha_em(obj)
{
    var sfecha_asiento =  $(FECHA_ASIENTO).val();
    var fecha_comp_asiento = sfecha_asiento.split("-");
    var dia_fecha_asiento = new Date(parseInt(fecha_comp_asiento[0]),parseInt(fecha_comp_asiento[1]),parseInt(fecha_comp_asiento[2]));

    var sfecha =  $(obj).val();
    var fecha_comp = sfecha.split("-");
    var dia_fecha = new Date(parseInt(fecha_comp[0]),parseInt(fecha_comp[1]),parseInt(fecha_comp[2]));

    if(dia_fecha_asiento< dia_fecha){
        $(obj).val(sfecha_asiento);
        $("#tit-alert").html("Alerta");
        $("#body-alert").html("<strong>NO</strong> puede ingresar una fecha superior a la <strong>fecha del asiento</strong>");
        $("#alert-yn").modal();
    }
    return null;
}

/*************************************
 * Habilita o deshabilita el porcentaje
 * de retención del iva dependiendo si es
 * bien o servicio
 * @param combo_bs
 * @returns {null}
 */
function cambio_ret_iva(combo_bs)
{
    var opt = $(combo_bs).val();
    var tr = $(combo_bs).parent().parent().get(0);
    var select_ret_iva = $(tr).find(S_COD_RET_IVA);
    //$(select_ret_iva).prop('selectedIndex', 0).selectpicker("refresh");
    if(opt == 1)
    {
        $(select_ret_iva).find("option").each(function(){
            if(parseFloat($(this).attr("data-porcentaje")) == 70)
                $(this).attr("disabled", true);
            if(parseFloat($(this).attr("data-porcentaje")) == 30)
                $(this).attr("disabled",false);
        });
    }
    else
    {
        $(select_ret_iva).find("option").each(function(){
            if(parseFloat($(this).attr("data-porcentaje")) == 30)
                $(this).attr("disabled", true);
            if(parseFloat($(this).attr("data-porcentaje")) == 70)
                $(this).attr("disabled",false);
        });
    }
    $(select_ret_iva).selectpicker("refresh");
    return null;
}
/*********************************
 * Función para calcular el total
 * de los reembolsos
 */
function calcular_totales_reembolso()
{
    var tbase0 = 0.0;
    var tbase12 = 0.0;
    var tbase0cta = 0.0;
    var tbase12cta = 0.0;
    $(T_DETALLE_REEM+" tbody input").each(function(){
        if($(this).hasClass("base_cero"))
        {
            if($(this).val()!=""){
                tbase0 = tbase0 + parseFloat($(this).val());
            }
        }
        if($(this).hasClass("base_iva"))
        {
            if($(this).val()!=""){
                tbase12 = tbase12 + parseFloat($(this).val());
            }
        }
    });
    $(T_DETALLE_COMP+" tbody tr").each(function(){
        var base = $($(this).find("select[data-name='combo_iva']")).val();
        if(parseFloat(base.split("-")[1])==0.0)
        {
            tbase0cta = tbase0cta + parseFloat($(this).find(".valor").val());
        }
        else
        {
            tbase12cta = tbase12cta + parseFloat($(this).find(".valor").val());
        }
    });
    $(TR_BASE0).text(tbase0);
    $(TR_BASE12).text(tbase12);
    if(tbase0==tbase0cta && tbase12==tbase12cta)
    {
        $(T_REEMBOLSO).removeClass("btn btn-danger");
        $(T_REEMBOLSO).addClass("btn btn-success");
        $(T_REEMBOLSO).find("span").removeClass("glyphicon glyphicon-remove");
        $(T_REEMBOLSO).find("span").addClass("glyphicon glyphicon-ok");
    }
    else
    {
        $(T_REEMBOLSO).removeClass("btn btn-success");
        $(T_REEMBOLSO).addClass("btn btn-danger");
        $(T_REEMBOLSO).find("span").removeClass("glyphicon glyphicon-ok");
        $(T_REEMBOLSO).find("span").addClass("glyphicon glyphicon-remove");
    }
}

/***************************************
 * Función para habilitar los divs para
 * que ingrese info del pago
 * @param obj
 * @constructor
 */
function PagoExterior(obj)
{
    $(obj).find("option").each(function(){
        var boton_pais = $("button[data-id='id_pais']");
        var pais_select = $(S_PAIS);
        if(this.selected){
            if($(this).attr("value")==2){
                $("#pago_hide").attr("style","display: block");
                if($(pais_select).val()=="")
                {
                    $(boton_pais).addClass("campo_requerido");
                    $(S_PAIS).attr("disabled", false);
                    $(S_PAIS).selectpicker('refresh');
                }
            }
            else{
                $("#pago_hide").attr("style","display: none");
                $(boton_pais).removeClass("campo_requerido");
                $(S_PAIS).prop('selectedIndex', 0);
                $(S_PAIS).selectpicker('refresh');
            }
        }
    });
}
/**********************************************************
 * Funcion para validar el Ruc o Cedula del Proveedor
 * @param obj , id
 * @constructor
 */
function isCedula(obj){
    var number = $(obj).val();
    var residuo=0;
    var respuesta=0;
    var suma_cedula=0;
    var result=0;
    var divi= number.substring(0,9);
    var porcion1 = number.substring(2,3);
    var rdivi3 = number.substring(9,10);//Digito a verificar cedula
    var contador=0;

        if(porcion1<6 && number.length == 10 && number.substring(0,2)<=24)
        {
            var tmp=0;
            var test=0;
                 for(var y=0; y<divi.length; y++){
                     if(y%2==0){
                        tmp=divi[y]*2;

                         if(tmp<=9){
                            suma_cedula= suma_cedula+tmp;
                         }
                         else{
                                result = tmp-9;
                                suma_cedula = suma_cedula+result;
                         }
                     }
                     else{
                            test= divi[y]*1;
                            suma_cedula=suma_cedula+test;
                     }
                 }

                 residuo = suma_cedula % 10;
                 if(residuo==0){
                     respuesta=0;
                     if(respuesta == rdivi3 || respuesta == 0){
                         contador++;
                     }
                 }
                 else{
                      respuesta = parseInt(10-residuo);
                      if(respuesta == rdivi3 || respuesta == 0){
                          contador++;
                      }
                 }
        }

        if(contador==0){
            return false;
        }
        else{
            return true;
        }
}
/******************************
 * Valida si es persona juridica
 * @param obj
 * @returns {boolean}
 */
function isPersonaJuridica(obj){
  var number = obj;
  var mruc = [4,3,2,7,6,5,4,3,2];
  var suma_total = 0;
  var residuo=0;
  var respuesta=0;
  var divi= number.substring(0,9);
  var porcion1 = number.substring(2,3);
  var rdivi2 = number.substring(9,10);// Digito a verificar ruc
  var contador=0;

    if(porcion1==9 && number.substring(10,13)==001  && number.substring(0,2)<=24){
        for(var k=0; k<divi.length; k++){
           suma_total = suma_total + (mruc[k]*parseInt(divi[k]));
           residuo = suma_total % 11;
           respuesta = parseInt(11-residuo);
        }
            if (rdivi2 == respuesta || residuo==0){
                contador++;
            }
    }
    if(contador==0)
        return false;
    else
        return true;

}
/**************************************
 * Verifica si es Institución Publica
 * @param obj
 * @returns {boolean}
 */
function isIntitucionPublica(obj){
  var number = obj;
  var nruc_empresa = [3,2,7,6,5,4,3,2 ];
  var suma_total = 0;
  var residuo=0;
  var respuesta=0;
  var digiempresa = number.substring(0,8);
  var porcion1 = number.substring(2,3);

  var rdivi1 = number.substring(8,9);// Digito a verificar empresa pública
  var contador=0;

      if(porcion1==6 && number.substring(9,13)==0001 && number.substring(0,2)<=24)
      {
          for(var k=0; k<digiempresa.length; k++)
          {
               suma_total = suma_total + (nruc_empresa[k]*parseInt(digiempresa[k]));
               residuo = suma_total % 11;
               respuesta = parseInt(11-residuo);
          }
            if (rdivi1 == respuesta || residuo==0){
                contador++;
            }
      }

      if(contador==0)

        return false;
      else
        return true;

}
/********************************
 * Verifica si es Persona Natural
 * @param obj
 * @returns {boolean}
 */
function isPersonaNatural(obj){
    var number = obj;
    var residuo=0;
    var respuesta=0;
    var suma_cedula=0;
    var result=0;
    var divi= number.substring(0,9);
    var rdivi3 = number.substring(9,10);//Digito a verificar cedula
    var porcion1 = number.substring(2,3);
    var contador=0;

        if(porcion1<6 && number.substring(10,13)==001 && number.substring(0,2)<=24)
        {
            var tmp=0;
            var test=0;
                 for(var y=0; y<divi.length; y++){
                     if(y%2==0){
                        tmp=divi[y]*2;

                         if(tmp<=9){
                            suma_cedula= suma_cedula+tmp;
                         }
                         else{
                                result = tmp-9;
                                suma_cedula = suma_cedula+result;
                         }
                     }
                     else{
                            test= divi[y]*1;
                            suma_cedula=suma_cedula+test;
                     }
                 }

                 residuo = suma_cedula % 10;
                 if(residuo==0){
                     respuesta=0;
                     if(respuesta == rdivi3 || respuesta == 0){
                         contador++;
                     }
                 }
                 else{
                      respuesta = parseInt(10-residuo);
                      if(respuesta == rdivi3 || respuesta == 0){
                          contador++;
                      }
                 }
        }

        if(contador==0){
            return false;
        }
        else{
            return true;
        }
}
/**************************
 * Verifica si es Ruc
 * @param obj
 * @returns {boolean}
 */
function isRUC(obj){
    var number = $(obj).val();
    var cont=0;
    if (isPersonaJuridica(number) ){
        cont++;
    }
    if(isIntitucionPublica(number)){
        cont++;
    }
     if(isPersonaNatural(number)){
        cont++;
    }
    if(cont==0)
        return false;
    else
        return true;
}
/******************************
 * Verifica si el nnumero de
 * documento que ingresó
 * es válido
 * @param tipo_ident
 * @param ruc
 */
function validar_ruc(tipo_ident, ruc){
    var titulo2 = "El Núm. de identificación es incorrecto";
    $(ruc).popover('destroy');
    if( $(ruc).val()!=""){
        if( $(tipo_ident.firstChild).val()=="")
        {
            var titulo3 = "Escoja un tipo de Identificación";
            $(ruc).popover({title: titulo3,placement: 'bottom'}).popover('show');
        }
        if($(tipo_ident.firstChild).val()=="R")
        {
            if($(ruc).val().length==13)
            {
                if(isRUC(ruc)){
                $(ruc).popover('destroy');
                }
                else{
                    $(ruc).popover({title: titulo2,placement: 'bottom'}).popover('show');
                }
            }
            else{
                $(ruc).popover({title: titulo2,placement: 'bottom'}).popover('show');
            }
        }
        if($(tipo_ident.firstChild).val()=="C")
        {
            if($(ruc).val().length==10)
            {
                if(isCedula(ruc)){
                    $(ruc).popover('destroy');
                }
                else{
                    $(ruc).popover({title: titulo2,placement: 'bottom'}).popover('show');
                }
            }
            else{
                $(ruc).popover({title: titulo2,placement: 'bottom'}).popover('show');
            }
        }
    }
}


/*************************************************************
* Funcion para habilitar el tipo de Nota de venta o nota de debito o si es reembolso
* N/C cod 04
* N/D cod 05
* Reembolso 41
*/
function CambioDocumento(obj)
{
    $(obj).find("option").each(function(){
        if(this.selected){
            if(parseInt($(this).attr("value"))==41)
            {
                $(T_DETALLE_COMP).find(S_COD_RET_FTE).each(function()
                {
                    $(this).prop('selectedIndex', 0);
                    $(this).attr("disabled",true);
                    $(this).selectpicker("refresh");
                    AgregarTitulo(this);
                });
                $(T_DETALLE_COMP).find(S_COD_RET_IVA).each(function()
                {
                    $(this).prop('selectedIndex', 0);
                    $(this).attr("disabled",true);
                    $(this).selectpicker("refresh");
                    AgregarTitulo(this);
                });
                CalcularRetencion();
                $("#detalle_reembolso").attr("style","display:block");
                $("#doc_mofi_hide").attr("style","display: none");
                $("#doc_modifica_title").attr("style","display: none");
            }
            else
            {
                if(parseInt($(this).attr("value")) == 4 || parseInt($(this).attr("value")) == 5)
                {
                    $("#doc_mofi_hide").attr("style","display: block");
                    $("#doc_modifica_title").attr("style","display: block");
                    $("#detalle_reembolso").attr("style","display: none");
                    $(T_DETALLE_COMP).find(S_COD_RET_FTE).each(function()
                    {
                        $(this).prop('selectedIndex', 0);
                        $(this).attr("disabled",true);
                        $(this).selectpicker("refresh");
                        AgregarTitulo(this);
                    });
                    $(T_DETALLE_COMP).find(S_COD_RET_IVA).each(function()
                    {
                        $(this).prop('selectedIndex', 0);
                        $(this).attr("disabled",true);
                        $(this).selectpicker("refresh");
                        AgregarTitulo(this);
                    });
                    CalcularRetencion();
                }
                else
                {
                    $("#doc_mofi_hide").attr("style","display: none");
                    $("#doc_modifica_title").attr("style","display: none");
                    $("#detalle_reembolso").attr("style","display: none");
                    $(T_DETALLE_COMP+" tbody tr").each(function(){
                        var combo_ret_fte = $(this).find(S_COD_RET_FTE);
                        var combo_ret_iva = $(this).find(S_COD_RET_IVA);
                        $(combo_ret_fte).attr("disabled", false);
                        $(combo_ret_fte).selectpicker("refresh");
                        $(combo_ret_iva).attr("disabled", false);
                        $(combo_ret_iva).selectpicker("refresh");
                    });
                }
            }

        }
    });
}
/*******************************
 * Habilita o deshabilita
 * el combo ret_iva si es que
 * es sujeto a iva esa compra
 * @param obj
 */
function habilitar_bs_iva(obj)
{
    var combo_iva_padre = $(obj).parent().parent().get(0);
    var combo_iva = $(combo_iva_padre).find("select[data-name='combo_iva']");
    var combo_ret_iva = $(combo_iva_padre).find(S_COD_RET_IVA);
    var iva_porc = parseFloat(combo_iva.val().split("-")[1]);

    if(parseInt($(S_TIPO_DOC).val())!=41)
    {
        if(iva_porc == 0.0)
        {
            combo_ret_iva.prop('selectedIndex',0);
            $(combo_ret_iva).attr("disabled", true);
            $(combo_ret_iva).selectpicker("refresh");
            AgregarTitulo(combo_ret_iva);
        }
        else
        {
            $(combo_ret_iva).attr("disabled", false);
            $(combo_ret_iva).selectpicker("refresh");
            AgregarTitulo(combo_ret_iva);
        }
    }
}
/***********************************
 * Agrega puntos decimales a los
 * inputs
 * @param e
 */
function agregar_punto(e)
{
    var numero = $(e).val();
    var contador = 0.0;

    if(numero!=""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));


    $(T_DETALLE_COMP).find(".valor").each(function(){
        var val = $(this).val();
        if(val != ""){
            contador = contador + parseFloat(val);
        }
    });
    if (contador >= 1000){
        $("#forma_pago").attr("style","display: block");
    }
    else{
        $("#forma_pago").attr("style","display: none");
    }

}
/****************************
 * Función para calcular los totales
 * de la compra
 * @returns {boolean}
 * @constructor
 */

function CalcularTotales_viejo()
{
    var total_iva_0 = 0.0;
    var total_iva_12 = 0.0;
    var total_retencion = 0.0;
    var total_pagar = 0.0;
    var iva_actual = 0.0;
    $(T_DETALLE_COMP+" tbody tr").each(function(index)
    {
        var iva_select = $(this).find('select[data-name="combo_iva"] option:selected');
        var iva = 0.0;
        var cod_ret_fte_select = $(this).find(S_COD_RET_FTE + " option:selected");
        var porc_ret_fte = 0.0;
        var cod_ret_iva_select = $(this).find(S_COD_RET_IVA + " option:selected");
        var porc_ret_iva = 0.0;
        var valor = 0.0;
        if($($(this).find(".valor")).val()!=""){
            valor = parseFloat($($(this).find(".valor")).val());
        }
        iva = parseFloat($(iva_select).attr("data-porcentaje"));
        porc_ret_fte =  $(cod_ret_fte_select).attr("data-porcentaje"); // Obtiene el value del "option" seleccionado
        porc_ret_iva =  $(cod_ret_iva_select).attr("data-porcentaje");

        if(porc_ret_fte!=""){
            total_retencion = total_retencion + valor*parseFloat(porc_ret_fte/100);
        }
        if(porc_ret_iva!=""){
            total_retencion = total_retencion + valor*IVA*parseFloat(porc_ret_iva/100);
        }
        if(iva != 0){
            total_iva_12 = total_iva_12 + valor;
            iva_actual = iva;
        }
        else{
            total_iva_0 = total_iva_0 + valor;
        }
    });
    var total_iva = (total_iva_12 * iva_actual) / 100;

    total_pagar = (total_iva_0 + total_iva_12 + total_iva - total_retencion).toFixed(2);

    $("#total_iva_0").text(total_iva_0.toFixed(2));
    $("#total_iva_12").text(total_iva_12.toFixed(2));
    $("#total_iva").text(total_iva.toFixed(2));
    $("#total_retencion_det").text(total_retencion.toFixed(2));
    $("#total_pagar").text(total_pagar);

    if (total_retencion == 0)
        return false;
    else
        return true;
}
/************************
 * Agrega un tooltip a los
 * combos para que se pueda
 * observar all su contenido
 * @param e
 * @constructor
 */
function AgregarTitulo(e)
{
    var padre = $(e).parent().get(0);
    var boton_combo = $(padre).children()[1];
    var titulo = "";
    var div_texto = boton_combo.firstChild.firstChild;
    var codigo = "";
    $(e).find("option").each(function(){
      if(this.selected)
      {
          titulo = $(this).text();
      }
    });
    $(boton_combo).tooltip('hide')
    .attr('data-original-title', titulo)
    .tooltip('fixTitle');
}
/*************************************
 * Funcion para recalcular los ids
 */
function recalcular_ids()
{
    var contador = 0.0;
    $(T_DETALLE_COMP+" tbody tr").each(function(index)
    {
        var cont = index;
        var cont_ultimo = $(T_DETALLE_COMP+" tbody tr").length;
        $(this).find("td").each(function( index ) {
            var hijo = $(this).children()[0];
            if($(hijo).hasClass("valor"))
            {
                var val = $(hijo).val();
                if(val != ""){
                    contador = contador + parseFloat(val);
                }
            }

            if (hijo.tagName == "INPUT" || hijo.tagName == "SELECT")
            {
                var name_campo = $(hijo).attr("name").split("-"); //form-1-parada
                name_campo[1] = cont;
                var nombre_campo = name_campo[0]+"-"+name_campo[1]+"-"+name_campo[2];
                $(hijo).attr("name", nombre_campo);
                $(hijo).attr("id", "id_"+nombre_campo);
                if (name_campo[2] == "fte" && cont == (cont_ultimo-1)){
                    $(hijo).attr("disabled", false  );
                }
                if (hijo.tagName == "SELECT")
                {
                    $(hijo).selectpicker('refresh');
                    if($(this).children()[2]){
                        $(this).children()[2].remove();
                    }
                }
                $('#id_'+name_campo[0]+'-TOTAL_FORMS').val($(T_DETALLE_COMP+" tbody").children().length);
            }
        });
    });
    if (contador >= 1000){
        $("#forma_pago").attr("style","display: block");
    }
    else{
        $("#forma_pago").attr("style","display: none");
    }
};


$(document).ready(function()
{

    CargarAutorizacion(); // Carga la auorización por default

    cambio_ret_iva($('select[data-name="bien_serv"]'));
    $('select[data-name="bien_serv"]').change(function(){cambio_ret_iva(this);});

    CalcularTotales(); // Calcula los totales al inicio de la transacción

    if($("#id_plazo").attr("value")==undefined) // deshabilita plazo si la transaccion era contado al inicio
    {
        if($(S_FORMA_PAGO).val()==1)
            $("#id_plazo").attr("disabled",true);
    }

    /***********************
     * Para cargar automaticamente la autorización
     */
    $("#id_serie_ret").change(function(){CargarAutorizacion()});

    /***************
     * Para que se presente div pais que ingresó si la compra es del exterior
     */
    PagoExterior($("#id_pago"));

    /******************
     * Para que presente el div doc_modi al inicio si tiene NC/ND
     */
    CambioDocumento($(S_TIPO_DOC));

    $(T_DETALLE_COMP).find("select").change(function(){
     CalcularRetencion();
     AgregarTitulo(this)});
    $(T_DETALLE_COMP).find(S_COD_RET_FTE).find("option").each(function(){
        $(this).attr("title",$(this).attr("data-codigo"));
        $($(this).parent().get(0)).selectpicker("refresh");
    });
    $(T_DETALLE_COMP).find("select[data-name='combo_iva']").find("option").each(function(){
        $(this).attr("title",$(this).attr("data-porcentaje")+"%");
        $($(this).parent().get(0)).selectpicker("refresh");
    });
    $(T_DETALLE_COMP).find(S_COD_RET_IVA).find("option").each(function(){
        $(this).attr("title",$(this).attr("data-codigo"));
        $($(this).parent()).selectpicker("refresh");
    });
    $(T_DETALLE_REEM).find("select[data-name='combo_tipo_id']").find("option").each(function(){
        $(this).attr("title",$(this).text().split("-")[0]);
    });
    /**************************
     * Para que salga el combo
     * forma de pago sri al inicio
      */
    $(".valor").each(function(){
        agregar_punto(this);
    });

    /*************************
     * Agrega los punto decimal
     * a los valores ingresados
     */
    $(".valor").focusout(function(){
       agregar_punto(this);
       calcular_totales_reembolso();
    });
    $(".valor").change(function(){CalcularRetencion(this);
        CalcularTotales();
    });
    $('.selectpicker').selectpicker();

    $(NUM_DOC).mask('999-999-999999999');

    /************************
     * Funcion para autocompetar con ceros
     * el numero de serie
     */
    $(NUM_DOC).focusout(function(){
        var valor = $(this).val();
        var valor_final = valor.replace(/_/g , "");
        var separador = valor_final.split("-");
        if(parseInt(valor_final.split("-")[0])==0 || parseInt(valor_final.split("-")[1])==0)
        {
            $(this).val("");
            $("#tit-alert").html("Alerta");
            $("#body-alert").html("<strong>Número de serie inválido</strong>");
            $("#alert-yn").modal();
        }
        else
        {
            var a= ("00000000"+separador[2]);
            $(this).val(separador[0]+"-"+separador[1]+"-"+a.slice(-9));
        }
    });

    $(NUM_DOC_MODIF).mask('999-999-999999999');

    $(NUM_DOC_MODIF).focusout(function(){
        var valor = $(this).val();
        var valor_final = valor.replace(/_/g , "");
        var separador = valor_final.split("-");
        var a= ("00000000"+separador[2]);
        $(this).val(separador[0]+"-"+separador[1]+"-"+a.slice(-9));
    });

    $(NUM_RET).mask('999999999');

    $(NUM_RET).focusout(function(){
        var valor = $(this).val();
        var valor_final = valor.replace(/_/g , "");
        var a= ("00000000"+valor_final);
        $(this).val(a.slice(-9));
    });


    $("#id_proveedor").autocomplete({
        source: "/compras/buscarproveedor/",
        data:{tag:$(this).val()},
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            var padres = jQuery(event.target).parent().parent();
            var padre = padres[0];
            var padre_detalle_cta = padre.children[1];
            var cuenta = $(padre_detalle_cta.firstElementChild);
            $(this).val(ui.item.value);
                cuenta.val(ui.item.key);
                $(S_SUST_TRIB).prop('selectedIndex',0);
                $(S_SUST_TRIB).selectpicker("refresh");
                var $el = $(S_TIPO_DOC);
                /*
                if (ui.item.tipo=="R")
                {
                    tipo_id = 0;
                    cambiar_combo($el, 0,tipo_id);
                }
                if (ui.item.tipo=="C")
                {
                    tipo_id = 1;
                    cambiar_combo($el, 0,tipo_id);
                }

                if (ui.item.tipo=="P")
                {
                    tipo_id = 2;
                    cambiar_combo($el, 0,tipo_id);
                }
                */
            }
    });
    $(".b_s").attr('disabled', 'disabled');


    /***************************************************
     * Funcion para deshabilitar el combo Retencion Iva
     */
    var select_combo_iva = $(T_DETALLE_COMP).find("select[data-name='combo_iva']");
    $(select_combo_iva).change(function()
    {
        habilitar_bs_iva(this);

    });

    $(".numerico").keydown(function(event) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39) || (event.keyCode == 110))
            {
                 // let it happen, don't do anything
                 return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });
    $(".numero_natural").keydown(function(event){
        // Allow: backspace, delete, tab, escape, enter and.
        if ( $.inArray(event.keyCode,[46,8,9,27,13,127]) !== -1 ||
             // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
             // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39))
            {
                return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

});

    $(function () {
        $( "#fecha" ).datepicker();
    });
    $(document).ready(function(e){
        /*********************************************
         * Inhabilitar el "enter" en los inputs para no enviar
         * el formulario con un enter
         */
        $('form :input').keypress(function(e){
            if ( e.which == 13 ) e.preventDefault();
        });


        $('.cod_cta').mask('9.9.99.99.999');

        $( ".detalle_cta" ).autocomplete({
            source: "/compras/buscarcuenta/",
            data:{tag:$(this).val()},
            minLength: 2,
            select: function( event, ui ) {
                event.preventDefault();
                var padre = jQuery(event.target).parent().parent().get(0);
                var cuenta = $(padre).find(".cod_cta");
                var cuenta_string = ui.item.value.split("-")[1]
                $(this).val(cuenta_string);
                cuenta.val(ui.item.key);
                }
        });
        $( ".cod_cta" ).autocomplete({
            source: "/compras/buscarcuentacodigo/",
            data:{tag:$(this).val()},
            minLength: 2,
            select: function( event, ui ) {
                event.preventDefault();
                var padre = jQuery(event.target).parent().parent().get(0);
                var cuenta = $(padre).find(".detalle_cta");
                $(this).val(ui.item.value);
                cuenta.val(ui.item.key);
                }
        });
        $(".cod_cta").focusout(function(){
            var cod_cta = $(this);
            $.ajax({
                url: "/compras/buscarcuentacodigoexacto/",
                type: 'GET',
                async: false,
                timeout: 3000,
                dataType: "json",
                data: {
                    'term':$(this).val()
                },
                error: function(){
                    return true;
                },
                success: function(data){
                    var padre = $(cod_cta).parent().parent().get(0);
                    var detalle = $(padre).find(".detalle_cta");
                    $(detalle).val(data[0].key);
                     //alert(data.toSource());
                }
            });
        });

    });
$(document).ready(function()
    {
        $("#add").click(function(e)
        {
            e.preventDefault();
            var clonar_tr = $(T_DETALLE_COMP+" tbody tr:first").clone();
            clonar_tr.find('.cod_cta').mask('9.9.99.99.999');
            clonar_tr.find(".elimar").click(function(e)
                {
                    e.preventDefault();
                    if($(T_DETALLE_COMP+" tbody").children().length>1){
                        $(this).parent().parent().remove();
                        recalcular_ids();
                    }
                    else{
                        $("#tit-alert").html("Alerta");
                        $("#body-alert").html("<strong>No se puede eliminar</strong>");
                        $("#alert-yn").modal();
                    }
                });
            clonar_tr.find("input").each(function( index ) {
                $(this).val("");
                $(this).parent().find(".error").remove();
            });

            $(clonar_tr.find('.cod_cta')).autocomplete({
                source: "/compras/buscarcuentacodigo/",
                data:{tag:$(this).val()},
                minLength: 2,
                select: function( event, ui ) {
                    event.preventDefault();
                    var padre = jQuery(event.target).parent().parent().get(0);
                    var cuenta = $(padre).find(".detalle_cta");
                    $(this).val(ui.item.value);
                    cuenta.val(ui.item.key);
                    }
            });
            $( clonar_tr.find(".detalle_cta")).autocomplete({
                source: "/compras/buscarcuenta/",
                data:{tag:$(this).val()},
                minLength: 2,
                select: function( event, ui ) {
                    event.preventDefault();
                    var padre = jQuery(event.target).parent().parent().get(0);
                    var cuenta = $(padre).find(".cod_cta");
                    var cuenta_string = ui.item.value.split("-")[1]
                    $(this).val(cuenta_string);
                    cuenta.val(ui.item.key);
                    }
            });

            $(clonar_tr.find(".eliminar")).click(function(e){
                e.preventDefault();
                if($(T_DETALLE_COMP+" tbody").children().length>1){
                    $(this).parent().parent().remove();
                    recalcular_ids();
                    $("#id_form-TOTAL_FORMS").val($(T_DETALLE_COMP+" tbody").children().length);
                }
                else{
                    $("#tit-alert").html("Alerta");
                    $("#body-alert").html("<strong>No se puede eliminar</strong>");
                    $("#alert-yn").modal();
                }
                CalcularTotales();
                CalcularRetencion();
            });
            $(clonar_tr).find(S_COD_RET_FTE).prop('selectedIndex', 0).selectpicker("refresh");
            $(clonar_tr).find(S_COD_RET_IVA).prop('selectedIndex', 0).selectpicker("refresh");

            $(clonar_tr.find(".valor")).focusout(function(){
                agregar_punto(this);
                if(parseInt($(S_TIPO_DOC).val())==41)
                    calcular_totales_reembolso();
            });

            /**************************************
            * Para validar el cambio de bien o servicio
            * con el porc de retencion
            */
            cambio_ret_iva($(clonar_tr).find('select[data-name="bien_serv"]'));
            $(clonar_tr).find('select[data-name="bien_serv"]').change(function(){cambio_ret_iva(this);});


            $(clonar_tr.find(".valor")).change(function(){
                CalcularRetencion();
                CalcularTotales();
            });
            $(clonar_tr.find(".iva")).change(function(){
               CalcularTotales();
            });
            $(clonar_tr).find("select[data-name='combo_iva']").prop('selectedIndex', 0).selectpicker("refresh");
            $($(clonar_tr).find("select[data-name='combo_iva']")).change(function(){habilitar_bs_iva(this)});
            $(clonar_tr.find("select")).change(function()
            {
                //$('#collapseOne').collapse("hide");
                CalcularRetencion()
                AgregarTitulo(this);
            });
            $(T_DETALLE_COMP+" tbody").append(clonar_tr);
            recalcular_ids();

            $("#id_detalle_compra-TOTAL_FORMS").val($(T_DETALLE_COMP+" tbody").children().length);
            if(parseInt($(S_TIPO_DOC).val())==41 || parseInt($(S_TIPO_DOC).val())==5 || parseInt($(S_TIPO_DOC).val())==4){
                $(clonar_tr).find(S_COD_RET_FTE).attr("disabled", true).selectpicker("refresh");
                $(clonar_tr).find(S_COD_RET_IVA).attr("disabled", true).selectpicker("refresh");
            }
            else
            {
                $(clonar_tr).find(S_COD_RET_IVA).attr("disabled", false);
            }
        });

            $(".eliminar").click(function(e){
                e.preventDefault();
                if($(T_DETALLE_COMP+" tbody").children().length>1){
                    $(this).parent().parent().remove();
                    recalcular_ids();
                    $("#id_detalle_compra-TOTAL_FORMS").val($(T_DETALLE_COMP+" tbody").children().length);
                }
                else{
                    $("#tit-alert").html("Alerta");
                    $("#body-alert").html("<strong>No se puede eliminar</strong>");
                    $("#alert-yn").modal();
                }
                CalcularTotales();
                CalcularRetencion();
        });
    });
/***************************************
 * Funcion para mostar la fecha de vencimiento
 * del documento
 */

function MostrarFechaVenci(e)
{
    if($(S_FORMA_PAGO).val()==2){
        if($(e).val()!="")
        {
            $(e).removeClass("campo_requerido");
            var Fecha = new Date();
            // Ponemos la fecha
            var sFecha = $("#id_fecha").val();
            var aFecha = sFecha.split("-");
            Fecha.setDate(aFecha[2]);
            Fecha.setMonth(aFecha[1]-1);
            Fecha.setFullYear(aFecha[0]);

            // Sacamos los milisegundos, le sumamos los dias
            // y lo pones de nuevo en la fecha
            var fFecha=Fecha.getTime();
            var dias = parseInt($(e).val());
            fFecha=fFecha+(1000*60*60*24*dias);
            Fecha.setTime(fFecha);
            if(Fecha.getMonth()<9)
                $("#dia_vencimiento").html("<b>"+Fecha.getFullYear() + "-" + "0" +(Fecha.getMonth()+1) + "-" + Fecha.getDate()+"</b>");
            else
                $("#dia_vencimiento").html("<b>"+Fecha.getFullYear() + "-" + (Fecha.getMonth()+1) + "-" + Fecha.getDate()+"</b>");
        }
        else
        {
            $("#dia_vencimiento").html("");
            $(e).addClass("campo_requerido");
        }
    }
}


/******************************************
*   Funciones para Reembolso de Gastos    *
******************************************/
/******************************************
 *  Funcion para recalcular los ids
 */
function recalcular_ids_reembolso()
{
    $(T_DETALLE_REEM+" tbody tr").each(function(index)
    {
        var cont = index;
        var cont_ultimo = $(T_DETALLE_REEM+" tbody tr").length;
        $(this).find("td").each(function( index ) {
            var hijo = $(this).children()[0];
            if (hijo.tagName == "INPUT" || hijo.tagName == "SELECT")
            {
                var name_campo = $(hijo).attr("name").split("-"); //form-1-parada
                name_campo[1] = cont;
                var nombre_campo = name_campo[0]+"-"+name_campo[1]+"-"+name_campo[2];
                $(hijo).attr("name", nombre_campo);
                $(hijo).attr("id", "id_"+nombre_campo);
                if (name_campo[2] == "fte" && cont == (cont_ultimo-1)){
                    $(hijo).attr("disabled", false  );
                }
                if (hijo.tagName == "SELECT")
                {
                    $(hijo).selectpicker('refresh');
                    if($(this).children()[2]){
                        $(this).children()[2].remove();
                    };
                }
                $('#id_'+name_campo[0]+'-TOTAL_FORMS').val($(T_DETALLE_REEM+" tbody").children().length);
            }
        });

    });
};

/********************************************
 * Funcion para verificar que no digiten
 * documentos repetidos en el reembolso
 */
function verificar_seri_rep(){
    //preguntar si array.lenght >1 si no false
    var array = [];
    var prov = [];
    var serie = [];
    var tipo_doc = [];
    var acum = 0;
    var cont = 0;
    array.push(prov);
    array.push(serie);
    array.push(tipo_doc);

    $(T_DETALLE_REEM+" tbody").find("tr").each(function()
    {
        array[0][cont] = $(this).find(".proveedor").val();
        array[1][cont] = $(this).find(".serie").val();
        array[2][cont] = $(this).find(".tipo_documento").val();
        cont++;
    });
    if(array.length>1)
    {
        for (var i=0; i<cont; i++){
            for(var j=i+1; j<cont; j++){
                /* Si el numero de documento es igual y el proveedor es el mismo
                   y si es el mismo número de documento, entonces error esta ingresando dos veces el
                   mismo documento
                 */
                if(array[1][i] != "" && array[1][j] != "" && array[1][i] == array[1][j] && array[0][i] == array[0][j] && array[2][i] == array[2][j]){
                    acum++;
                }
            }
        }
        if(acum>0){
            $("#tit-alert").html("Alerta");
            $("#body-alert").html("<strong>No</strong>puede ingresar el mismo documento <strong>dos veces</strong> del mismo proveedor. Por favor verifique");
            $("#alert-yn").modal();
        }
        else{

        }
    }
}

$(document).ready(function()
{
    /***********************************
     * Función para validar la cédula
     */
    $(".proveedor").focusout(function(){
        var td = $($(this).parent().parent().get(0)).children()[1];
        validar_ruc(td, this);
    });
    $("select[data-name='combo_tipo_id']").change(function(){
        var td = $($(this).parent().parent().get(0)).children()[1];
        var ruc = $($(this).parent().parent().get(0)).children()[2].firstChild;
        validar_ruc(td, ruc);
    });

    calcular_totales_reembolso();



    /*************************************
     *  Aparecer div cuando este seleccionado
     *  el reembolso de gasto
     */
    if(parseInt($(S_TIPO_DOC).val())==41)
    {
        $("#detalle_reembolso").attr("style","display:block");
        $(T_DETALLE_COMP).find(S_COD_RET_FTE).each(function(){
            $(this).attr("disabled", true);
        });
        $(T_DETALLE_COMP).find(S_COD_RET_IVA).each(function(){
            $(this).attr("disabled", true);
        });
    }

    // Para mostrar la fecha de vencimiento al iniciar si es que existe un plazo ya definido "enviado desde el servidor"
    MostrarFechaVenci($("#id_plazo"));
    $("#id_plazo").focusout(function(){MostrarFechaVenci(this)});

    /*************************************************************
     * Funcion para Credito o Contado
     * Credito cod 2
     * Contado cod 1
     */
    $(S_FORMA_PAGO).change(function(){
       $(this).find("option").each(function(){
          if(this.selected){
           if($(this).attr("value")==2){
               $("#id_plazo").attr("disabled",false);
               $("#id_plazo").addClass("campo_requerido");
           }
           else{
                $("#id_plazo").val("");
                $("#id_plazo").removeClass("campo_requerido");
                $("#id_plazo").attr("disabled",true);
                $("#dia_vencimiento").html("");
           }
          }
       });
    });

    /*************************************************************
     * Funcion pago Local o Extrerior
     * Local cod 1
     * Exterior cod 2
     * Cambia el color del borde para que el usuario vea que tiene que
     * seleccionar un pais
     */
    $(S_PAIS).change(function(){
        $(this).find("option").each(function(){
            var boton_pais = $("button[data-id='id_pais']");
                if(this.selected){
                    if($(this).attr("value")=="")
                    {
                        $(boton_pais).addClass("campo_requerido");
                    }
                else{
                        $(boton_pais).removeClass("campo_requerido");
                    }
                }
        });
    });
    $("#id_pago").change(function(){PagoExterior(this);});
    $(S_TIPO_DOC).change(function(){CambioDocumento(this);});
    $(T_DETALLE_COMP).find("select").each(function(){AgregarTitulo(this)});
    $(S_SUST_TRIB).change(function(){
        $("#doc_mofi_hide").attr("style","display: none");
        $("#doc_modifica_title").attr("style","display: none");
    });
});


$(function()
{
    $(T_DETALLE_REEM).find(".serie ").focusout(function(){
            var valor = $(this).val();
            var valor_final = valor.replace(/_/g , "");
            var separador = valor_final.split("-");
            var a= ("00000000"+separador[2]);
            $(this).val(separador[0]+"-"+separador[1]+"-"+a.slice(-9));
            verificar_seri_rep();
        });
    /**********************
     * Valida la fecha de emision del documento
     */
    $(T_DETALLE_REEM).find(".fecha_emision").focusout(function(){validar_fecha_em(this)});
    /******************************************
     * Función para validar que cuadre
     * el reembolso de gastos
     */
    $("#grabar").click(function(event)
    {
        if(parseInt($(S_TIPO_DOC).val())==41)
        {
            var total_iva_0 = 0.0;
            var total_iva_12 = 0.0;
            var total_iva = 0.0;
            var total_retencion = 0.0;
            var total_pagar = 0.0;
            var bandera = false; // Variable que controla si se presenta o no el div de retenciones
            $(T_DETALLE_COMP+" tbody tr").each(function(index)
            {
                var iva_select = $(this).children()[4].firstChild;
                var iva = 0.0;
                var valor = 0.0;
                if($($(this).children()[8].firstChild).val()!=""){
                    valor = parseFloat($($(this).children()[8].firstChild).val());
                }
                iva = parseFloat($(iva_select).val().split("-")[1]);
                if(iva==12){
                    total_iva_12 = total_iva_12 + valor;
                }
                else{
                    total_iva_0 = total_iva_0 + valor;
                }
                });

            var trmontoi = 0.0;
            var trmonto0 = 0.0;
            $(T_DETALLE_REEM+" tbody tr").each(function()
            {
                var monto0 = parseFloat($(this).find(".base_cero ").val());
                var montoi = parseFloat($(this).find(".base_iva").val());
                trmonto0 = trmonto0 + monto0;
                trmontoi = trmontoi + montoi;
            });
            if(trmonto0 != total_iva_0)
                bandera = true;
            if(trmontoi != total_iva_12)
                bandera = true;
            if(bandera){
                event.preventDefault();
                $("#tit-alert").html("Alerta");
                $("#body-alert").html("<strong>Los valores del detalle de la compra no cuadra con los valores del reembolso</strong>");
                $("#alert-yn").modal();
            }
        }
    });

    /*************************************
     * Agregar los titulos y la fecha
     */
    $(T_DETALLE_REEM).find("select").each(function(){
         AgregarTitulo(this)});
    $(T_DETALLE_REEM).find("select").change(function(){
         AgregarTitulo(this)});
    $(T_DETALLE_REEM).find(".fecha_emision").each(function(){
        $(this).datepicker().on('changeDate', function (ev) {
            validar_fecha_em(this);
        });
        $(this).val($(this).attr("value"));
         });
    // Mascara a la serie
    $(T_DETALLE_REEM).find(".serie").each(function(){
         $(this).mask('999-999-999999999')});

    //Para que agrege automaticamente punto
    $(T_DETALLE_REEM).find(".base_cero").focusout(function(){
        agregar_punto(this);
        calcular_totales_reembolso();
    });
    $(T_DETALLE_REEM).find(".base_iva").focusout(function(){
        agregar_punto(this);
        calcular_totales_reembolso();
    });


    /***************************************************
     * Agregar un detalle de reembolso de gastos
     */
    $("#add_reembolso").click(function(e)
        {
            e.preventDefault();
            var clonar_tr = $(T_DETALLE_REEM+" tbody tr:first").clone();
            clonar_tr.find("input").each(function( index ) {
                $(this).val("");
                $(this).parent().find(".error").remove();
            });
            $(clonar_tr).find(".serie").mask('999-999-999999999');
            $(clonar_tr).find(".serie ").focusout(function(){
                var valor = $(this).val();
                var valor_final = valor.replace(/_/g , "");
                var separador = valor_final.split("-");
                var a= ("00000000"+separador[2]);
                $(this).val(separador[0]+"-"+separador[1]+"-"+a.slice(-9));
                verificar_seri_rep();
            });

            agregar_punto($(clonar_tr).find(".base_cero"));
            agregar_punto($(clonar_tr).find(".base_iva"));
        $(clonar_tr.find(".eliminar")).click(function(e){
            e.preventDefault();
            if($(T_DETALLE_REEM+" tbody").children().length>1){
                $(this).parent().parent().remove();
                recalcular_ids_reembolso();
                $("#id_reembolso_gasto-TOTAL_FORMS").val($(T_DETALLE_REEM+" tbody").children().length);
            }
            else{
                $("#tit-alert").html("Alerta");
                $("#body-alert").html("<strong>No se puede eliminar</strong>");
                $("#alert-yn").modal();
            }
            calcular_totales_reembolso();
        });
        $(clonar_tr).find("select[data-name='combo_tipo_doc']").prop('selectedIndex', 0).selectpicker("refresh");
        $(clonar_tr).find("select[data-name='combo_tipo_id']").prop('selectedIndex', 0).selectpicker("refresh");
        $(clonar_tr.find(".base_cero")).focusout(function(){
           agregar_punto(this);
           calcular_totales_reembolso();
        });
        $(clonar_tr.find(".base_iva")).focusout(function(){
           agregar_punto(this);
            calcular_totales_reembolso();
        });
        $(clonar_tr).find("select").each(function(){
         AgregarTitulo(this);
        });

        /*************************************
         * Valida la fecha de emisiond el documento
         */
        $(clonar_tr).find(".fecha_emision").datepicker().on('changeDate', function (ev) {
            validar_fecha_em(this);
        });

        /***********************************
         * Función para validar la cédula
         */
        $($($(clonar_tr).find(".proveedor").parent().parent().get(0)).children()[2].children[1]).remove(); // Elimina el popover que se crea al clonar
        $(clonar_tr).find(".proveedor").focusout(function(){
            var td = $($(this).parent().parent().get(0)).children()[1];
            validar_ruc(td, this);
        });
        $(clonar_tr).find("select[data-name='combo_tipo_id']").change(function(){
            var td = $($(this).parent().parent().get(0)).children()[1];
            var ruc = $($(this).parent().parent().get(0)).children()[2].firstChild;
            validar_ruc(td, ruc);
        });


        $(clonar_tr).find("select").change(function()
        {
            AgregarTitulo(this);
        });
        $(clonar_tr.find(".hasDatepicker")).each(function(){
            $(this).focusin(function(){
                $(this).removeClass('hasDatepicker'); // remove hasDatepicker class
                $(this).datepicker();
            });
        });

        $(T_DETALLE_REEM+" tbody").append(clonar_tr);
        $(T_DETALLE_REEM+" tbody tr:last").find(".fecha_emision").datepicker().on('changeDate', function (ev) {
            validar_fecha_em(this);
        });;

        recalcular_ids_reembolso();
        $("#id_reembolso_gasto-TOTAL_FORMS").val($(T_DETALLE_REEM+" tbody").children().length);
        });
});

$(document).ready(function(e)
{
    function validar_fecha_asiento()
        {
            var str_dia_asiento = $("#id_fecha_asiento").val().split("-");
            var dia_asiento = new Date(parseInt(str_dia_asiento[0]),parseInt(str_dia_asiento[1]),parseInt(str_dia_asiento[2]));
            if(dia_asiento > dia_actual_servidor)
            {
                $("#tit-alert").html("Alerta");
                $("#body-alert").html("<strong>NO</strong> puede ingresar una fecha superior a la <strong>fecha actual</strong>");
                $("#alert-yn").modal();
                $("#id_fecha_asiento").val(str_dia);
            }
        };

    $( "#id_fecha_asiento" ).datepicker().on('changeDate', function (ev) {
        $(this).datepicker("hide");
        CambioFechaAsiento();
        validar_fecha_asiento();
    });

    $( "#id_fecha_emision_retencion" ).datepicker().on('changeDate', function (ev) {
        validar_fecha_em_ret(this);
        $(this).datepicker("hide");
    });
    $( "#id_fecha" ).datepicker().on('changeDate', function (ev) {
        validar_fecha_em(this);
        $(this).datepicker("hide");
    });
    $( "#id_vence" ).datepicker().on('changeDate', function(ev){
        var stringfecha_emision =  $("#id_fecha").val();
        var fecha_comp_emision = stringfecha_emision.split("-");
        var dia_fecha_emision = new Date(parseInt(fecha_comp_emision[0]),parseInt(fecha_comp_emision[1]),parseInt(fecha_comp_emision[2]));

        var stringfecha =  $(this).val();
        var fecha_comp = stringfecha.split("-");
        var dia_fecha = new Date(parseInt(fecha_comp[0]),parseInt(fecha_comp[1]),parseInt(fecha_comp[2]));

        if(dia_fecha_emision > dia_fecha){
            $(this).val("");
            $("#tit-alert").html("Alerta");
            $("#body-alert").html("<strong>NO</strong> puede escojer una <strong>fecha de vencimiento</strong> inferior a la <strong>fecha de emisión del documento.</strong>");
            $("#alert-yn").modal();
            $(this).datepicker("hide");
        }
        $(this).datepicker("hide");
    });
    $( "#id_vence" ).val($( "#id_vence").attr("value"));


});