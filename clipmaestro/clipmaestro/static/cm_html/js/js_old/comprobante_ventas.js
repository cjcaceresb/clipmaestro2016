$(document).ready(function(){
    $(".numerico").keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,13,110,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
    });

    $(".numerico2").keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
    });
});
function validarEliminar(num2){
    var num = parseInt(num2);
    if (num == 1){
        $("#alert-yn").attr("data-id", $(this).attr("id"));
        $("#alert-yn").attr("data-target",1);
        $("#tit-alert").html("Alerta");
        $("#body-alert").html('<p>No puede eliminar la fila</p>');
        $("#alert-yn").modal();
    }
}

function AgregarTitulo(e){
    var padre = $(e).parent().get(0);
    var boton_combo = $(padre).children()[1];
    var titulo = "";
    var div_texto = boton_combo.firstChild.firstChild;
    var codigo = "";
    $(e).find("option").each(function(){
      if(this.selected)
      {
          titulo = $(this).text();
      }
    });
    $(boton_combo).tooltip('hide')
      .attr('data-original-title', titulo)
      .tooltip('fixTitle')
      //.tooltip('show');
      codigo = titulo.split("-")[0];

    $(div_texto).change(function(){
        $(this).text(codigo);
    });
}
//Clonar Detalle venta - AutocompletadoCliente - Validacion Enter -  HAbilitación Plazo - Vencimiento
$(document).ready(function(){

   $("#id_cliente").autocomplete({
        source: "/ventas/buscarcliente/",
        data:{tag:$(this).val()},
        minLength: 2,
        select: function( event, ui ) {
            event.preventDefault();
            var padres = jQuery(event.target).parent().parent();
            var padre = padres[0];
            var padre_detalle_cta = padre.children[1];
            var cuenta = $(padre_detalle_cta.firstElementChild);
            $(this).val(ui.item.value);
            cuenta.val(ui.item.key);
        }
    });

   $(".selectpicker").change(function(){
        AgregarTitulo(this);
    });

   function recalcular_ids_venta(){
        $("#tabla_formset_venta_test tbody tr").each(function(index)
        {
            var cont = index;
            cont_ultimo = $("#tabla_formset_venta_test tbody tr").length;
            $(this).find("td").each(function( index )
            {
                var hijo = $(this).children()[0];
                if (hijo.tagName == "INPUT" || hijo.tagName == "SELECT")
                {
                    var name_campo = $(hijo).attr("name").split("-");
                    name_campo[1] = cont;
                    var nombre_campo = name_campo[0]+"-"+name_campo[1]+"-"+name_campo[2];
                    $(hijo).attr("name", nombre_campo);
                    $(hijo).attr("id", "id_"+nombre_campo);

                    if (hijo.tagName == "SELECT")
                    {
                        $(hijo).selectpicker('refresh');
                        if($(this).children()[2]){
                            $(this).children()[2].remove();
                        };
                    }
                }
            });
        });
    };

    /*********************************************
     * Inhabilitar el "enter" en los inputs para no enviar
     * el formulario con un enter
     */
       $('form :input').keypress(function(e){
            if ( e.which == 13 ) e.preventDefault();
       });

       // Deshabilita plazo si la transaccion era contado al inicio
       if($("#id_plazo").attr("value")==undefined){
                if($("#id_forma_pago").val()==1)
                    $("#id_plazo").attr("disabled",true);
       }
       $(".eliminar").click(function(e){
            e.preventDefault();
            if($("#tabla_formset_venta_test tbody").children().length>1){
                $(this).parent().parent().remove();
                recalcular_ids_venta();
                calcularSubtotalesPepa();
                DescuetosPorVenta();
                $("id_detalle_venta_test-TOTAL_FORMS").val($("#tabla_formset_venta_test tbody").children().length);
            }
            else{
               validarEliminar(1);
            }
       });

    /******************************************* CLONAR DETALLE ***************************************************/
    $("#add").click(function(e){
        e.preventDefault();
        var clonar_tr2 = $("#tabla_formset_venta_test tbody tr:first").clone();
        clonar_tr2.find(".eliminar").click(function(e){
            e.preventDefault();
            if($("#tabla_formset_venta_test tbody").children().length>1){
                $(this).parent().parent().remove();
                recalcular_ids_venta();
                calcularSubtotalesPepa();
                DescuetosPorVenta();
                $("#id_detalle_venta_test-TOTAL_FORMS").val($("#tabla_formset_venta_test tbody").children().length);
            }
            else{
                 validarEliminar(1);
            }
        });
         $(clonar_tr2.find(".selectpicker")).change(function(){
                AgregarTitulo(this);
         });
         $(clonar_tr2.find(".iva")).val("");
         $(clonar_tr2.find(".ice")).val("");
         $(clonar_tr2.find(".subtotal")).val("");
         $(clonar_tr2.find(".descuento")).val("");
         $(clonar_tr2.find(".concepto")).val("");
         $(clonar_tr2.find('.precio_u').empty());
         //Llamados por Ajax de los Items
         $(clonar_tr2.find(".item")).change(function(e){
             var tr = $(this).parent().parent().get(0);
             var iva = $(tr).find(".iva");
             var precio = $(tr).find(".precio_u");
             var ice= $(tr).find(".ice");
             $.post("/ventas/buscaiva/", {
                'item': $(this).val()
            }, function(data){
                $(iva).text("");
                $(ice).text("");

                $(iva).val(data.iva);
                $(ice).val(data.ice);
                }
            );
              $.post("/ventas/buscarprecios/", {
                'item': $(this).val()
                }, function(data){
                    $(precio).val(data.precio1);
                    //llenarCombo(precio, data.precio1, data.precio2, data.precio3, data.precio4);
                }
            );
              calcularSubtotalesPepa();
              DescuetosPorVenta();
         });
         $(clonar_tr2.find(".selectpicker")).change(function(){
            AgregarTitulo(this);
        });
         clonar_tr2.find(".numerico2").keydown(function(event) {
            if ( $.inArray(event.keyCode,[46,8,9,27,13,190]) !== -1 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });
         $(clonar_tr2.find(".cantidad")).addClass("numerico2").val("");
         $(clonar_tr2.find(".cantidad")).focusout(function(e){
            $("#tabla_formset_venta_test").find(".descuento").each(function(){
                 if ($(this).val()==""){
                     $(this).val(0.0);
                 }
            });
            calcularSubtotalesPepa();
            DescuetosPorVenta();

         });
         $(clonar_tr2.find(".iva")).focusout(function(e){
            calcularSubtotalesPepa();
             DescuetosPorVenta();
         });
         $(clonar_tr2.find(".ice")).focusout(function(e){
              calcularSubtotalesPepa();
              DescuetosPorVenta();
         });
         $(clonar_tr2.find(".precio_u")).val("");
         $(clonar_tr2.find(".precio_u")).focusout(function(e){
             calcularSubtotalesPepa();
             DescuetosPorVenta();
         });
         $(clonar_tr2.find(".concepto")).focusout(function(e){
              calcularSubtotalesPepa();
              DescuetosPorVenta();
         });
         $(clonar_tr2.find(".descuento")).focusout(function(e){
            if ($(this).val()==""){
                $(this).val(0.0);
            }
             calcularSubtotalesPepa();
         });
         $("#tabla_formset_venta_test tbody").append(clonar_tr2);
         recalcular_ids_venta();
         $("#id_detalle_venta_test-TOTAL_FORMS").val($("#tabla_formset_venta_test tbody").children().length);
    });

});
/*************************************************** Fin Clonar Detalle *******************************************************************/

/****************************************************** Funciones *************************************************************************/

/**********************************************************
 * Funcion para mostar la fecha de vencimiento
 * del documento
 */
function MostrarFechaVenci(e){
    if($("#id_forma_pago").val()==2){
        if($(e).val()!="")
        {
            $(e).removeClass("campo_requerido");
            var Fecha = new Date();
            // Ponemos la fecha
            var sFecha = $("#id_fecha").val();
            var aFecha = sFecha.split("-");
            Fecha.setDate(aFecha[2]);
            Fecha.setMonth(aFecha[1]-1);
            Fecha.setFullYear(aFecha[0]);
            // Sacamos los milisegundos, le sumamos los dias
            // y lo pones de nuevo en la fecha
            var fFecha=Fecha.getTime();
            var dias = parseInt($(e).val());
            fFecha=fFecha+(1000*60*60*24*dias);
            Fecha.setTime(fFecha);
            if(Fecha.getMonth()<9)
                $("#dia_vencimiento").html("<b>"+Fecha.getFullYear() + "-" + "0" +(Fecha.getMonth()+1) + "-" + Fecha.getDate()+"</b>");
            else
                $("#dia_vencimiento").html("<b>"+Fecha.getFullYear() + "-" + (Fecha.getMonth()+1) + "-" + Fecha.getDate()+"</b>");
        }
        else
        {
            $("#dia_vencimiento").html("");
            $(e).addClass("campo_requerido");
        }
    }
}
/************************ FIN ****************************/

/*** Funcion_porcentajeDescuento para iteraccion de descuento ****/
function iteraccionCombosDescuento(){
    var porc_desce = $("#id_descuento_porc").val();
    var tipo_desce = $("#id_tipo_descuento").val();

    if(porc_desce == "" && tipo_desce == ""){
        $("#hide_input").attr("style","display:none");
        $("#hide_input2").attr("style","display:none");
        $("#hide_input_doce").attr("style","display:none");
    }

    if(porc_desce == "" && tipo_desce != ""){
        $("#id_monto_descuento").attr("disabled",false);
        $("#hide_input").attr("style","display:none");
        $("#camuflado").html("");
        ///////////////////////////////////////////
        $("#hide_input_doce").attr("style","display:none");
        $("#hide_input2").attr("style","display:none");
        $("#id_monto_descuento_cero").attr("disabled",false);
        $("#id_monto_descuento_doce").attr("disabled",false);
        $("#camuflado2").html("");
        $("#camuflado_3").html("");
    }
    if(porc_desce != "" && tipo_desce == ""){
        $("button[data-id='id_tipo_descuento']").addClass("campo_requerido");
        ///////////////////////////////////////////
        $("#hide_input").attr("style","display:none");
        $("#hide_input_doce").attr("style","display:none");
        $("#hide_input2").attr("style","display:none");
    }

    if(porc_desce == "1" && tipo_desce == 1){
        $("#hide_input2").attr("style","display: none");
        $("#hide_input_doce").attr("style","display:none");

        $("button[data-id='id_tipo_descuento']").removeClass("campo_requerido");
        $("#hide_input").attr("style","display:block");
        $("#camuflado").html("%");
        $("#simbolo_descuento").html("");
        $("#id_monto_descuento").addClass("campo_requerido");
        $("#id_monto_descuento").attr("disabled",false);
        $("#simbolo_descuento").append('<b>% </b>');
        /////////////////////////////////////////////
        $("#tarifa12").html("");
        $("#tarifa0").html("");
        //////////////////////////////////////////////
         $("#tabla_formset_venta_test").find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });

    }

    if(porc_desce == "2" && tipo_desce == 1){
        $("#hide_input").attr("style","display:none");
        $("#hide_input_doce").attr("style","display:block");
        $("#hide_input2").attr("style","display:block; margin-top:10px;");
        $("#tarifa12").html("12%");
        $("#tarifa0").html("0%");
        $("#camuflado").html("");
        $("#camuflado_3").html("$");
        $("#camuflado2").html("$");

        $("#simbolo_descuento").html("");
        $("#simbolo_descuento").append('<b>$ </b>');

        $("#id_monto_descuento_cero").addClass("campo_requerido");
        $("#id_monto_descuento_cero").val("");
        $("#id_monto_descuento_cero").attr("disabled",false);

        $("#id_monto_descuento_doce").addClass("campo_requerido");
        $("#id_monto_descuento_doce").val("");
        $("#id_monto_descuento_doce").attr("disabled",false);

        /////////////////////////////////////////////////////////////////
         $("#tabla_formset_venta_test").find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });
    }

     if(porc_desce == "2" && tipo_desce == ""){
        $("#simbolo_descuento").html("");
        $("button[data-id='id_tipo_descuento']").addClass("campo_requerido");
    }

     if(porc_desce == "1" && tipo_desce == 2){
        $("#camuflado").html("");
        $("#camuflado2").html("");
        $("#camuflado_3").html("");
        $("#camuflado").html("%");

        $("#simbolo_descuento").html("");
        $("#simbolo_descuento").append('<b>% </b>');

        $("#hide_input").attr("style","display: none");
        $("#hide_input_doce").attr("style","display:none");
        $("#hide_input2").attr("style","display: none");
        //////////////////////////////////////////////////////////////////
        $("#tabla_formset_venta_test").find(".descuento").each(function(){
            $(this).parent().attr("style","display: show");
        });
    }

    if(porc_desce == "2" && tipo_desce == 2){
        $("#simbolo_descuento").html("");
        $("#simbolo_descuento").append('<b>$ </b>');

        $("#camuflado").html("");
        $("#camuflado").html("$");
        $("#camuflado2").html("");
        $("#camuflado2").html("$");
        $("#camuflado_3").html("");
        $("#camuflado_3").html("$");

        $("#hide_input").attr("style","display: none");
        $("#hide_input2").attr("style","display: none");
        $("#hide_input_doce").attr("style","display:none");
        //////////////////////////////////////////////////////////////////
        $("#tabla_formset_venta_test").find(".descuento").each(function(){
            $(this).parent().attr("style","display: show");
        });
    }

}

function columnahide(){

    if ($("#id_descuento_porc").val()=="" && $("#id_tipo_descuento").val()==""){
        $("#tabla_formset_venta_test").find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });
        $("#hide_input_doce").attr("style","display: none");
        $("#hide_input").attr("style","display: none");
        $("#hide_input2").attr("style","display: none");


    }
    if ($("#id_descuento_porc").val()=="" && $("#id_tipo_descuento").val()!=""){
        $("#tabla_formset_venta_test").find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });
    }
    if ($("#id_descuento_porc").val()!="" && $("#id_tipo_descuento").val()==""){
        $("#tabla_formset_venta_test").find(".descuento").each(function(){
            $(this).parent().attr("style","display: none");
        });
    }


}
function calcularSubtotalesPepa(){
    var descuento_combo = $("#id_descuento_porc").val();
    var tipo_combo_descuento = $("#id_tipo_descuento").val();
    /******************** Sin Descuento ************************/
    var cantidad_porcentaje = 0.0;
    var precio_u_porcentaje = 0.0;
    var proc_iva = 0.0;
    var proc_ice = 0.0;
    var subtotales_iva_porcentaje = 0.0;
    var iva_total_procentaje = 0.0;
    var ice_total_porcentaje = 0.0;
    var x = 0.0;
    var subtotales_iva_porcentaje_cero = 0.0;
    var cantidad_porcentaje_cero = 0.0;
    var precio_u_porcentaje_cero = 0.0;
    var yz = 0.0;
    /**********************************************************/
    /*********************** Descuento % Producto *************/
    var cantidad_valores = 0.0;
    var precio_valores =0.0;
    var proc_iva_prod = 0.0;
    var proc_desc_prod = 0.0;
    var proc_ice_prod = 0.0;
    var subtotales_descuento_producto_pro = 0.0;
    var totales_desc = 0.0;
    var totales_ice = 0.0;
    var iva_desc = 0.0;
    var subtotales_descuento_producto_cero = 0.0;
    var cantidad_valores_cero = 0.0;
    var precio_valores_cero = 0.0;
    var proc_iva_prod_cero = 0.0;
    var proc_desc_prod_cero = 0.0;
    var totales_desc_cero = 0.0;
    var zdesc = 0.0;
    /**********************************************************/
    var bandera=0;
    var tmp = 0.0;
    var tmp_cero = 0.0;
    var cont = 0;
    var flag=false;

    if (descuento_combo==1 && tipo_combo_descuento==2){
        bandera=1;
    }
    if (descuento_combo==2 && tipo_combo_descuento==2){
        bandera=2;
    }

    $("#tabla_formset_venta_test tbody tr").each(function(index)
    {
        var cantidad = $(this).find(".cantidad");
        var precio = $(this).find(".precio_u");
        var iva = $(this).find(".iva");
        var ice = $(this).find(".ice");
        var descuento = $(this).find(".descuento"); // Descuento del detalle de la venta, interviene en el cálculo en el caso de Descuento por producto
        var subtotal = $(this).find(".subtotal");
        // Sin Descuento

        if ( descuento_combo == "" && tipo_combo_descuento == "" ){
            if($(cantidad).val() !="" && $(precio).val() !="" && $(iva).val() !="")
            {
                if ($(iva).val()!=0 ){
                    cantidad_porcentaje = parseInt($(cantidad).val());
                    precio_u_porcentaje = parseFloat($(precio).val());
                    proc_iva = parseFloat($(iva).val());

                    if ($(ice).val()==""){
                        proc_ice = 0.0;
                    }else{
                        proc_ice = parseFloat($(ice).val());
                    }

                    subtotales_iva_porcentaje= subtotales_iva_porcentaje + parseFloat(cantidad_porcentaje*precio_u_porcentaje);
                    iva_total_procentaje =  (subtotales_iva_porcentaje)*(proc_iva/100);
                    ice_total_porcentaje = ice_total_porcentaje + parseFloat((cantidad_porcentaje*precio_u_porcentaje)*(proc_ice/100));
                    $(subtotal).val(parseFloat(cantidad_porcentaje*precio_u_porcentaje));
                    $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                    $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                    $("#descuento").text("$"+0.0);
                    $("#iva").text("$"+iva_total_procentaje.toFixed(2));
                    x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));

                }else{
                     cantidad_porcentaje_cero = parseInt($(cantidad).val());
                     precio_u_porcentaje_cero = parseFloat($(precio).val());
                     subtotales_iva_porcentaje_cero = subtotales_iva_porcentaje_cero + parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero);

                     $(subtotal).val(parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero));
                     $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                     $("#descuento").text("$"+0.0);
                     x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                     $("#total_pagar").text("$"+x.toFixed(2));
                }
            yz = subtotales_iva_porcentaje+ subtotales_iva_porcentaje_cero;
            $("#total_subtotal").text("$"+yz.toFixed(2));
            }
        }

        else{

            // Descuento por Producto
            if ($(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && tipo_combo_descuento == 2){
                cantidad_valores = parseInt($(cantidad).val());
                precio_valores = parseFloat($(precio).val());
                proc_iva_prod = parseFloat($(iva).val());
                proc_desc_prod = parseFloat($(descuento).val());

                if(descuento_combo == 1){ // Porcentaje de Descuento por producto
                    if(proc_iva_prod != 0){ // Tiene IVA

                        if ($(ice).val()==""){
                            proc_ice_prod = 0.0;
                        }else{
                            proc_ice_prod = parseFloat($(ice).val());
                        }

                        if (bandera==1 && $(descuento).val()>100){
                            $(descuento).val(0.0);
                            tmp = parseFloat($(descuento).val());
                        }
                        else{
                            tmp = parseFloat($(descuento).val());
                        }

                        if(tmp <= 100 && tmp >= 0){
                            subtotales_descuento_producto_pro = subtotales_descuento_producto_pro
                            + parseFloat(cantidad_valores*precio_valores) - parseFloat((cantidad_valores*precio_valores)*tmp/100);
                            totales_desc = totales_desc + parseFloat((cantidad_valores*precio_valores)*(tmp/100));
                            iva_desc = parseFloat(subtotales_descuento_producto_pro*proc_iva_prod/100);
                            totales_ice = totales_ice + parseFloat((cantidad_valores*precio_valores)*(proc_ice_prod/100))
                            $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - parseFloat((cantidad_valores*precio_valores)*tmp/100));
                            $("#subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                            $("subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                            $("#total_subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                            $("#ice").text("$"+totales_ice.toFixed(2));
                            $("#descuento").text("$"+totales_desc.toFixed(2));
                            $("#iva").text("$"+iva_desc.toFixed(2));
                            x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                            $("#total_pagar").text("$"+x.toFixed(2));
                        }
                        else{
                            $("#alert-yn").attr("data-id", $(this).attr("id"));
                            $("#alert-yn").attr("data-target",2);
                            $("#tit-alert").html("Alerta");
                            $("#body-alert").html('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Por Favor ingrese un porcentaje de descuento válido.</span></strong></p>');
                            $("#alert-yn").modal();
                            $(descuento).val("");
                            $(descuento).val(0.0);
                            totales_desc = 0.0;
                        }

                    }else{ // IVA = 0.0
                            cantidad_valores_cero = parseInt($(cantidad).val());
                            precio_valores_cero = parseFloat($(precio).val());
                            proc_iva_prod_cero = parseFloat($(iva).val());
                            proc_desc_prod_cero = parseFloat($(descuento).val());

                            if (bandera==1 && $(descuento).val()>100){
                                $(descuento).val(0.0);
                                tmp_cero = parseFloat($(descuento).val());
                            }
                            else{
                                tmp_cero = parseFloat($(descuento).val());
                            }

                            if(tmp_cero < 100 && tmp_cero >= 0){
                                subtotales_descuento_producto_cero = subtotales_descuento_producto_cero + parseFloat(cantidad_valores_cero*precio_valores_cero)- parseFloat(cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100);
                                totales_desc_cero = totales_desc_cero + parseFloat((cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100));
                                $(subtotal).val(parseFloat(cantidad_valores_cero*precio_valores_cero) - parseFloat(cantidad_valores_cero*precio_valores_cero)*(tmp_cero/100));
                                $("#subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                                $("#subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                                $("#total_pagar").text("$"+x.toFixed(2));

                            }else{
                                $("#alert-yn").attr("data-id", $(this).attr("id"));
                                $("#alert-yn").attr("data-target",2);
                                $("#tit-alert").html("Alerta");
                                $("#body-alert").html('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Por Favor ingrese un porcentaje de descuento válido.</span></strong></p>');
                                $("#alert-yn").modal();
                                $(descuento).val("");
                                $(descuento).val(0.0);
                                $("#descuento").text("$"+totales_desc.toFixed(2));
                            }
                    }

                    $("#subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                    $("#subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                    $("#total_subtotal").text("$"+parseFloat((subtotales_descuento_producto_pro) + (subtotales_descuento_producto_cero)).toFixed(2));
                    $("#ice").text("$"+totales_ice.toFixed(2));
                    zdesc = totales_desc + totales_desc_cero;
                    $("#descuento").text("$"+zdesc.toFixed(2));
                    $("#iva").text("$"+iva_desc.toFixed(2));
                    x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));
                    cont++;
                }
                else
                {
                    // POR MONTO PRODUCTO
                    if(descuento_combo == 2){
                        if(proc_iva_prod != 0){
                            if ($(ice).val()==""){
                                proc_ice_prod = 0.0;
                            }else{
                                proc_ice_prod = parseFloat($(ice).val());
                            }

                            if(parseFloat(cantidad_valores*precio_valores)>$(descuento).val()){
                                 subtotales_descuento_producto_pro = subtotales_descuento_producto_pro
                                + parseFloat(cantidad_valores*precio_valores) - parseFloat($(descuento).val());
                                totales_desc = totales_desc + parseFloat($(descuento).val());
                                iva_desc = parseFloat(subtotales_descuento_producto_pro*proc_iva_prod/100);
                                totales_ice = totales_ice + parseFloat((cantidad_valores*precio_valores)*(proc_ice_prod/100))

                                $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - parseFloat($(descuento).val()));
                                $("#subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                                $("subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                                $("#total_subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                $("#descuento").text("$"+totales_desc.toFixed(2));
                                $("#iva").text("$"+iva_desc.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                                $("#total_pagar").text("$"+x.toFixed(2));
                            }
                            else{
                                   $("#alert-yn").attr("data-id", $(this).attr("id"));
                                    $("#alert-yn").attr("data-target",2);
                                    $("#tit-alert").html("Alerta");
                                    $("#body-alert").html('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Por Favor Ingrese un monto de descuento válido, acorde al subtotal.</span></strong></p>');
                                    $("#alert-yn").modal();
                                    $(descuento).val("");
                                    $(descuento).val(0.0);
                            }
                        }// IVA == 0
                        else{

                            if(parseFloat(cantidad_valores*precio_valores)>$(descuento).val()){
                                subtotales_descuento_producto_cero = subtotales_descuento_producto_cero
                                    + parseFloat(cantidad_valores*precio_valores)
                                    - $(descuento).val();
                                totales_desc_cero = totales_desc_cero + parseFloat($(descuento).val());
                                $(subtotal).val(parseFloat(cantidad_valores*precio_valores) - $(descuento).val() );
                                $("#subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                                $("#subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                                $("#ice").text("$"+totales_ice.toFixed(2));
                                x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero; // falta valores
                                $("#total_pagar").text("$"+x.toFixed(2));

                            }else{

                                $("#alert-yn").attr("data-id", $(this).attr("id"));
                                $("#alert-yn").attr("data-target",2);
                                $("#tit-alert").html("Alerta");
                                $("#body-alert").html('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Por Favor Ingrese un monto de descuento válido, acorde al subtotal.</span></strong></p>');
                                $("#alert-yn").modal();
                                $(descuento).val("");
                                $(descuento).val(0.0);
                            }
                        }
                        $("#subtotal").text("$"+subtotales_descuento_producto_pro.toFixed(2));
                        $("#subtotal_cero").text("$"+subtotales_descuento_producto_cero.toFixed(2));
                        $("#total_subtotal").text("$"+parseFloat((subtotales_descuento_producto_pro) + (subtotales_descuento_producto_cero)).toFixed(2));
                        $("#ice").text("$"+totales_ice.toFixed(2));
                        zdesc = totales_desc + totales_desc_cero;
                        $("#descuento").text("$"+zdesc.toFixed(2));
                        $("#iva").text("$"+iva_desc.toFixed(2));
                        x = subtotales_descuento_producto_pro + totales_ice + iva_desc + subtotales_descuento_producto_cero;
                        $("#total_pagar").text("$"+x.toFixed(2));

                    }
                }
            }
        }
    });
}
function DescuetosPorVenta(){
   var cantidad_porcentaje = 0.0;
   var precio_u_porcentaje = 0.0;
   var proc_iva = 0.0;
   var proc_ice = 0.0;
   var descuento_combo = $("#id_descuento_porc").val();
   var tipo_combo_descuento = $("#id_tipo_descuento").val();

   var monto_descuento = $("#id_monto_descuento").val();

   var monto_descuento_doce = $("#id_monto_descuento_doce").val();
   var monto_descuento_cero = $("#id_monto_descuento_cero").val();

   var subtotales_iva_porcentaje = 0.0;
   var iva_total_procentaje = 0.0;
   var ice_total_porcentaje = 0.0;
   var x = 0.0;
   var yz = 0.0;
   var subtotales_iva_porcentaje_cero = 0.0;
   var cantidad_porcentaje_cero = 0.0;
   var precio_u_porcentaje_cero = 0.0;
   var result_descuento_porc = 0.0;
   var result_iva_porc = 0.0;
   var tmp = 0.0;
   var z = 0.0;
   var result_descuento_porc_iva_cero = 0.0;
   var tmp_iva_cero = 0.0;
   var zdesc = 0.0;
   var bandera1=0;
   var cont=0;
   /***************************************************************/
   var tarifa12_monto = 0.0;
   var tarifa0_monto = 0.0;
   var tarifa12_monto_iva = 0.0;
   var descuento12_monto = 0.0;
   var descuento0_monto = 0.0;
   var total_descuento_monto_final = 0.0;
   var cont1 = 0;
   var tam = 0;
   var result_total_pagar = 0.0;
   var bandera_result=0;
   var bandera_result2=0;
   tam = parseInt($("#tabla_formset_venta_test tbody tr").length);

    $("#tabla_formset_venta_test tbody tr").each(function(index)
    {
        var cantidad = $(this).find(".cantidad");
        var precio = $(this).find(".precio_u");
        var iva = $(this).find(".iva");
        var ice = $(this).find(".ice");
        var subtotal = $(this).find(".subtotal");
        cont1++;

        if(tipo_combo_descuento==1 && $(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && descuento_combo !=""){
            if ($(iva).val()!=0 ){
                cantidad_porcentaje = parseInt($(cantidad).val());
                precio_u_porcentaje = parseFloat($(precio).val());
                proc_iva = parseFloat($(iva).val());
                if ($(ice).val()==""){
                     proc_ice = 0.0;
                }else{
                     proc_ice = parseFloat($(ice).val());
                }
                subtotales_iva_porcentaje= subtotales_iva_porcentaje + parseFloat(cantidad_porcentaje*precio_u_porcentaje);
                iva_total_procentaje =  (subtotales_iva_porcentaje)*(proc_iva/100);
                ice_total_porcentaje = ice_total_porcentaje + parseFloat((cantidad_porcentaje*precio_u_porcentaje)*(proc_ice/100));
                $(subtotal).val(parseFloat(cantidad_porcentaje*precio_u_porcentaje));
                $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                $("#descuento").text("$"+0.0);
                $("#iva").text("$"+iva_total_procentaje.toFixed(2));
                x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                $("#total_pagar").text("$"+x.toFixed(2));

            }else{
                 cantidad_porcentaje_cero = parseInt($(cantidad).val());
                 precio_u_porcentaje_cero = parseFloat($(precio).val());
                 subtotales_iva_porcentaje_cero = subtotales_iva_porcentaje_cero + parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero);
                 $(subtotal).val(parseFloat(cantidad_porcentaje_cero*precio_u_porcentaje_cero));
                 $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                 $("#descuento").text("$"+0.0);
                 x = subtotales_iva_porcentaje + ice_total_porcentaje + iva_total_procentaje + subtotales_iva_porcentaje_cero;
                 $("#total_pagar").text("$"+x.toFixed(2));
                }
            yz = subtotales_iva_porcentaje+ subtotales_iva_porcentaje_cero;
            $("#total_subtotal").text("$"+yz.toFixed(2));
        }

        if(tipo_combo_descuento==1 && $(cantidad).val()!="" && $(precio).val()!="" && $(iva).val()!="" && descuento_combo !="")
        {

            if(descuento_combo==1){//Porcentaje

                if ( $("#id_monto_descuento").val()==""){
                    $("#id_monto_descuento").val(0.0);
                }

                if($(iva).val()!=0 && monto_descuento <= 100 && monto_descuento >= 0){
                    result_descuento_porc = (subtotales_iva_porcentaje)*(monto_descuento/100);
                    tmp = subtotales_iva_porcentaje - result_descuento_porc;
                    result_iva_porc = (subtotales_iva_porcentaje - result_descuento_porc)*(proc_iva/100);
                    $("#descuento").text("$"+result_descuento_porc.toFixed(2));
                    $("#iva").text("$"+result_iva_porc.toFixed(2));
                    x = tmp  + ice_total_porcentaje + result_iva_porc + result_descuento_porc_iva_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));
                    bandera1++;
                }else{
                    //iva=0
                    result_descuento_porc_iva_cero = (subtotales_iva_porcentaje_cero)*(monto_descuento/100);
                    tmp_iva_cero = subtotales_iva_porcentaje_cero - result_descuento_porc_iva_cero;
                    $("#descuento").text("$"+result_descuento_porc_iva_cero.toFixed(2));
                    x = tmp_iva_cero  + ice_total_porcentaje + result_iva_porc + result_descuento_porc_iva_cero;
                    $("#total_pagar").text("$"+x.toFixed(2));
                }
                 cont++;

                 if (bandera1==0 && cont==1){
                    $("#alert-yn").attr("data-id", $(this).attr("id"));
                    $("#alert-yn").attr("data-target",1);
                    $("#tit-alert").html("Alerta");
                    $("#body-alert").html('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Por Favor ingrese un porcentaje válido.</span></strong></p>');
                    $("#alert-yn").modal();
                    $("#id_monto_descuento").val("");
                    $("#id_monto_descuento").addClass("campo_requerido");
                    $("#id_monto_descuento").val(0.0);
                 }

                $("#subtotal").text("$"+subtotales_iva_porcentaje.toFixed(2));
                $("#subtotal_cero").text("$"+subtotales_iva_porcentaje_cero.toFixed(2));
                $("#total_subtotal").text("$"+parseFloat((subtotales_iva_porcentaje) + (subtotales_iva_porcentaje_cero)).toFixed(2));
                $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                zdesc = result_descuento_porc + result_descuento_porc_iva_cero;
                $("#descuento").text("$"+zdesc.toFixed(2));
                $("#iva").text("$"+result_iva_porc.toFixed(2));
                z = tmp + tmp_iva_cero;
                x = z + ice_total_porcentaje + result_iva_porc ;
                $("#total_pagar").text("$"+x.toFixed(2));
            }

            if(descuento_combo==2){//Monto
                if (monto_descuento_doce == ""  && monto_descuento_cero==""){
                    $("#id_monto_descuento_doce").val(0.0);
                    $("#id_monto_descuento_cero").val(0.0);
                }

                if ($(iva).val()!=0){
                    tarifa12_monto = tarifa12_monto + parseFloat($(cantidad).val()*$(precio).val());
                    descuento12_monto = tarifa12_monto - $("#id_monto_descuento_doce").val();
                    tarifa12_monto_iva = (descuento12_monto)*($(iva).val()/100);
                }else{
                     tarifa0_monto = tarifa0_monto + parseFloat($(cantidad).val()*$(precio).val());
                     descuento0_monto = tarifa0_monto - $("#id_monto_descuento_cero").val();
                }

                 if(tarifa12_monto < $("#id_monto_descuento_doce").val() && cont1==tam){
                    $("#alert-yn").attr("data-id", $(this).attr("id"));
                    $("#alert-yn").attr("data-target",2);
                    $("#tit-alert").html("Alerta");
                    $("#body-alert").html('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Por Favor ingrese un monto de descuento válido según el subtotal_12%.</span></strong></p>');
                    $("#alert-yn").modal();
                    $("#id_monto_descuento_doce").val("");
                    $("#id_monto_descuento_doce").addClass("campo_requerido");
                    $("#id_monto_descuento_doce").val(0.0);
                    bandera_result++;
                 }

                 if(tarifa0_monto < $("#id_monto_descuento_cero").val()&& cont1==tam){
                     $("#alert-yn").attr("data-id", $(this).attr("id"));
                     $("#alert-yn").attr("data-target",3);
                     $("#tit-alert").html("Alerta");
                     $("#body-alert").html('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Por Favor ingrese un monto de descuento válido según el subtotal_0%.</span></strong></p>');
                     $("#alert-yn").modal();
                     $("#id_monto_descuento_cero").val("");
                     $("#id_monto_descuento_cero").addClass("campo_requerido");
                     $("#id_monto_descuento_cero").val(0.0);
                     bandera_result2++;
                 }

                 if(bandera_result==0 && bandera_result2==0){
                     $("#subtotal").text("$"+tarifa12_monto.toFixed(2));
                     $("#subtotal_cero").text("$"+tarifa0_monto.toFixed(2));
                     $("#total_subtotal").text("$"+parseFloat((tarifa12_monto) + (tarifa0_monto)).toFixed(2));
                     total_descuento_monto_final = parseFloat($("#id_monto_descuento_cero").val()) + parseFloat($("#id_monto_descuento_doce").val());
                     $("#descuento").text("$"+total_descuento_monto_final.toFixed(2));
                     $("#iva").text("$"+tarifa12_monto_iva.toFixed(2));
                     $("#ice").text("$"+ice_total_porcentaje.toFixed(2));
                     result_total_pagar = ice_total_porcentaje + descuento12_monto + tarifa12_monto_iva + descuento0_monto;
                     $("#total_pagar").text("$"+result_total_pagar.toFixed(2));
                 }
            }
        }

    });
}
/************************************************* FIN DE TODAS LAS FUNCIONES *********************************************************************/
var border, outline, box_shadow;
window.onload = function(){
    // Para mostrar la fecha de vencimiento al iniciar si es que existe un plazo ya definido "enviado desde el servidor"
    MostrarFechaVenci($("#id_plazo"));
    $("#id_plazo").focusout(function(){MostrarFechaVenci(this)});
    /*************************************************************
     * Funcion para Credito o Contado
     * Credito cod 2
     * Contado cod 1
     */
    $("#id_forma_pago").change(function(){
       $(this).find("option").each(function(){
          if(this.selected){
           if($(this).attr("value")==2){
               $("#id_plazo").attr("disabled",false);
               $("#id_plazo").addClass("campo_requerido");
           }
           else{
                if($(this).attr("value")==1){
                    $("#id_plazo").val("");
                    $("#id_plazo").removeClass("campo_requerido");
                    $("#id_plazo").attr("disabled",true);
                    $("#dia_vencimiento").html("");

                }
           }
          }
       });
    });
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////

    $("#id_descuento_porc").change(function(){
        iteraccionCombosDescuento();
        columnahide();
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });
    $("#id_tipo_descuento").change(function(){
        iteraccionCombosDescuento();
        columnahide();
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });
    $("#id_monto_descuento").focusout(function(){
        $(this).removeClass("campo_requerido");
            DescuetosPorVenta();
    });

    $("#id_monto_descuento_doce").focusout(function(){
        $(this).removeClass("campo_requerido");
            DescuetosPorVenta();
    });

    $("#id_monto_descuento_cero").focusout(function(){
        $(this).removeClass("campo_requerido");
            DescuetosPorVenta();
    });

    /************************* Validación número documento ************************************/
    $("#id_numero_documento").focusout(function(){
        var valor = $(this).val();
        var titulo = "El número de serie es inválido";
        $(this).popover('destroy');

        if( valor == 0 ){
            $(this).popover({title: titulo,placement: 'bottom'}).popover('show');
            $(this).val("");

        }else{
            $(this).popover('destroy');
             var a= ("00000000"+valor);
            $(this).val(a.slice(-9));
        }

    });
    /*******************************************************************************************/

    $(".cantidad").focusout(function(){

        $("#tabla_formset_venta_test").find(".descuento").each(function(){
             if ($(this).val()==""){
                 $(this).val(0.0);
             }
        });
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });
    $(".precio_u").focusout(function(){
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });

    $(".ice").focusin(function(){
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });
    $(".iva").focusin(function(){
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });
    $(".descuento").focusout(function(){
        if ($(this).val()==""){
            $(this).val(0.0);
        }
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });

    $(".concepto").focusout(function(){
        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });
    $(".centro_costo").focusin(function(){

        calcularSubtotalesPepa();
        DescuetosPorVenta();
    });
}
