    $(document).ready(function()
    {
        $('#arbol').treeview(
        {
            animated:"normal",
            persist: "cookie",
            control: "#treecontrol"
        });
        $(".numerico").keydown(function(event) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
                 // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                 // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39) || (event.keyCode == 110))
                {
                     // let it happen, don't do anything
                     return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });

    });
    $(function()
    {
        $('#id_descripcion').focusout(function(){
            if($(this).val()!=""){
                if($(this).hasClass("campo_requerido")){
                    $(this).removeClass("campo_requerido");
                }
            }
        });
        $('#alert-yn').on('hidden.bs.modal', function (e) {
            if($('#id_descripcion').hasClass("campo_requerido")){
                $('#id_descripcion').removeClass("campo_requerido");
            }
        });

        /*******************************************************
         *   Parametros del popup yes/no
         *   id = alert-yn
         *   id-cambio-titulo = tit-alert
         *   id-cambio-cont = body-alert
         *   id-aceptar = ok-alert
         *   data-action, contiene el parámetro de acción
         *              1: agregar,
         *              2: editar
         *   data-target, contiene el parámetro de tipo de cta
        */
        $(".agregar").click(function(){
            $("#alert-yn").attr("data-id", $(this).attr("name"));
            $("#alert-yn").attr("data-nivel", $(this).attr("data-nivel"));
            $("#alert-yn").attr("data-action", 1);
            $("#alert-yn").attr("data-tipo", $(this).attr("data-tipo"));

            $("#tit-alert").html("Agregar una nueva cuenta contable");

            $('#id_descripcion').val("");
            $("#id_tipo_retencion").val(0);
            $("#id_tipo_retencion").selectpicker("refresh");
            $("#id_porc_retencion").val("");

            if($('#id_descripcion').parent().hasClass("has-error"))
                $('#id_descripcion').parent().removeClass("has-error");
            if ($('#id_porc_retencion').parent().hasClass("has-error"))
                $('#id_porc_retencion').parent().removeClass("has-error");

            $('#id_cod_relacion_super_cia').val("");
            $("#body-alert").find(".ms_delete").each(function(){$(this).hide()});
            $("#body-alert").find("form").each(function(){$(this).show()});


            if($(this).attr("data-nivel")=="4")
            {
                $("#cod_rel").show();
                if($(this).attr("data-tipo")=="2")
                {
                    $("#tipo_ret").show();
                    $("#porc_ret").show();
                    $("#text-ret").show();
                }
                else
                {
                    $("#tipo_ret").hide();
                    $("#porc_ret").hide();
                    $("#text-ret").hide();
                }
            }
            else
            {
                $("#cod_rel").hide();
                $("#tipo_ret").hide();
                $("#porc_ret").hide();
                $("#text-ret").hide();
            }
            $("#alert-yn").modal();
        });
        $(".editar").click(function(){
            $("#alert-yn").attr("data-id", $(this).attr("name"));
            $("#alert-yn").attr("data-nivel", $(this).attr("data-nivel"));
            $("#alert-yn").attr("data-action", 2);
            $("#alert-yn").attr("data-tipo", $(this).attr("data-tipo"));
            $("#alert-yn").attr("data-codigo", $(this).attr("data-codigo"));
            $("#body-alert").find(".ms_delete").each(function(){$(this).hide()});
            $("#body-alert").find("form").each(function(){$(this).show()});
            $("#tit-alert").html("Editar cuenta contable");

            if($('#id_descripcion').parent().hasClass("has-error"))
                $('#id_descripcion').parent().removeClass("has-error");

            var id = $(this).attr("name");
            $.post('/plancuenta/obtenerdatos/',
            {
                'id': id
            },
            function(data){
                $("#id_descripcion").val(data.descripcion);
                $("#id_cod_relacion_super_cia").val(data.cod_relacion_super_cia);
                $("#id_naturaleza").val(data.naturaleza);
                $("#id_naturaleza").selectpicker("refresh");
            if(data.tipo_ret != 0)
                {
                    $("#id_tipo_retencion").val(data.tipo_ret);
                    $("#id_tipo_retencion").selectpicker("refresh");
                    $("#id_porc_retencion").val(data.porc_ret)
                }
                });
            if($(this).attr("data-nivel")=="4")
            {
                $("#cod_rel").show();
                if($(this).attr("data-tipo")=="2")
                {
                    $("#tipo_ret").show();
                    $("#porc_ret").show();
                    $("#text-ret").show();
                }
                else
                {
                    $("#tipo_ret").hide();
                    $("#porc_ret").hide();
                    $("#text-ret").hide();
                }
            }
            else
            {
                $("#cod_rel").hide();
                $("#tipo_ret").hide();
                $("#porc_ret").hide();
                $("#text-ret").hide();
            }
            $("#alert-yn").modal();
        });
        $(".eliminar").click(function(){
            $("#alert-yn").attr("data-id", $(this).attr("name"));
            $("#alert-yn").attr("data-nivel", $(this).attr("data-nivel"));
            $("#alert-yn").attr("data-action", 3);
            $("#alert-yn").attr("data-target", $(this).attr("data-target"));
            $("#tit-alert").html("Eliminar cuenta contable");
            $('#id_descripcion').val("");
            $('#id_cod_relacion_super_cia').val("");
            $("#cuenta").html($(this).attr("data-name"));
            $('#id_tipo_cuenta').prop('selectedIndex',0);
            $("#body-alert").find("form").each(function(){$(this).hide()});
            $("#body-alert").find(".ms_delete").each(function(){$(this).show()});

            $("#alert-yn").modal();
        });
        $('#ok-alert').click(function(){
            /***********************************************
            * Aqui va la acción ajax
            **********************************************/
            var id = $('#alert-yn').attr("data-id");
            var opcion = parseInt($('#alert-yn').attr("data-action")); // 1: agregar, 2: editar, 3: eliminar
            var tipo = 0;
            switch (opcion){
                case 1: // Opción Agregar
                    if($('#alert-yn').attr("data-tipo")!="")
                        tipo = parseInt($('#alert-yn').attr("data-tipo"));
                    switch(tipo)
                    {
                        case 0: // Cuenta que no tiene Tipo
                            if ($('#id_descripcion').val() != "")
                            {
                                $.post('/plancuenta/agregar/',
                                    {
                                        'id': id,
                                        'descripcion': $('#id_descripcion').val(),
                                        'clase_cuenta': $('#id_clase_cuenta').val(),
                                        'naturaleza': $('#id_naturaleza').val(),
                                        'cod_relacion_super_cia': $('#id_cod_relacion_super_cia').val()
                                    },
                                    function(data){
                                       window.location.href = '/plancuenta/';
                                });
                            }
                            else
                            {
                                $('#id_descripcion').parent().addClass("has-error");
                            }
                            break;
                        case 2: // Retencion
                            var tipo_ret = parseInt($('#id_tipo_retencion').val());
                            var porc = $('#id_porc_retencion').val();
                            var descripcion = $('#id_descripcion').val();
                            $('#id_descripcion').parent().removeClass("has-error");
                            $('#id_porc_retencion').parent().removeClass("has-error");

                            if(tipo_ret!=0) // Es de tipo retencion (fte o iva)
                            {
                                if( (descripcion != "") && (porc =! "") )
                                {
                                    $.post('/plancuenta/agregar/',
                                        {
                                            'id': id,
                                            'descripcion': $('#id_descripcion').val(),
                                            'clase_cuenta': $('#id_clase_cuenta').val(),
                                            'naturaleza': $('#id_naturaleza').val(),
                                            'cod_relacion_super_cia': $('#id_cod_relacion_super_cia').val(),
                                            'tipo_retencion': $('#id_tipo_retencion').val(),
                                            'porc_retencion': $('#id_porc_retencion').val()
                                        },
                                        function(data){
                                           window.location.href = '/plancuenta/';
                                    });
                                }
                                else
                                {
                                    $('#id_descripcion').parent().addClass("has-error");
                                    $('#id_porc_retencion').parent().addClass("has-error");
                                }
                            }
                            else{ // No es de tipo retención
                                if( (descripcion != ""))
                                {
                                    $.post('/plancuenta/agregar/',
                                        {
                                            'id': id,
                                            'descripcion': $('#id_descripcion').val(),
                                            'clase_cuenta': $('#id_clase_cuenta').val(),
                                            'naturaleza': $('#id_naturaleza').val(),
                                            'cod_relacion_super_cia': $('#id_cod_relacion_super_cia').val()
                                        },
                                        function(data){
                                           window.location.href = '/plancuenta/';
                                    });
                                }
                                else
                                {
                                    $('#id_descripcion').parent().addClass("has-error");
                                }
                            }
                            break;
                        default :
                            if ($('#id_descripcion').val() != "")
                            {
                                $.post('/plancuenta/agregar/',
                                    {
                                        'id': id,
                                        'descripcion': $('#id_descripcion').val(),
                                        'clase_cuenta': $('#id_clase_cuenta').val(),
                                        'naturaleza': $('#id_naturaleza').val(),
                                        'cod_relacion_super_cia': $('#id_cod_relacion_super_cia').val()
                                    },
                                    function(data){
                                       window.location.href = '/plancuenta/';
                                });
                            }
                            else
                            {
                                $('#id_descripcion').parent().addClass("form-group has-error");
                            }
                            break;
                    }
                    $('#id_descripcion').val('');
                    $('#id_cod_relacion_super_cia').val('');
                    break;
                case 2: // Opción Editar
                    var id = $('#alert-yn').attr("data-id");
                    var nivel = parseInt($('#alert-yn').attr("data-nivel"));
                    if(nivel != 5)
                    {
                        $.post('/plancuenta/editar/',
                            {
                                'id': id,
                                'descripcion': $('#id_descripcion').val(),
                                'naturaleza': $('#id_naturaleza').val()
                            },
                            function(data){
                                window.location.href = '/plancuenta/';
                            });
                    }
                    else
                    {
                        if($('#alert-yn').attr("data-tipo"))
                            tipo = parseInt($('#alert-yn').attr("data-tipo"));
                        alert(tipo);
                        switch (tipo)
                        {
                            case 0:// No tiene ret alguna
                                var id = $('#alert-yn').attr("data-id");
                                $.post('/plancuenta/editar/',
                                        {
                                            'id': id,
                                            'descripcion': $('#id_descripcion').val(),
                                            'clase_cuenta': $('#id_clase_cuenta').val(),
                                            'naturaleza': $('#id_naturaleza').val(),
                                            'cod_relacion_super_cia': $('#id_cod_relacion_super_cia').val()
                                        },
                                    function(data){
                                        window.location.href = '/plancuenta/';
                                    });
                                break;
                            case 2: // Retencion
                                var tipo_ret = parseInt($('#id_tipo_retencion').val());
                                var porc = $('#id_porc_retencion').val();
                                var descripcion = $('#id_descripcion').val();
                                $('#id_descripcion').parent().removeClass("has-error");
                                $('#id_porc_retencion').parent().removeClass("has-error");

                                if(tipo_ret!=0) // Es de tipo retencion (fte o iva)
                                {
                                    if( (descripcion != "") && (porc =! "") )
                                    {
                                        $.post('/plancuenta/editar/',
                                            {
                                                'id': id,
                                                'descripcion': $('#id_descripcion').val(),
                                                'clase_cuenta': $('#id_clase_cuenta').val(),
                                                'naturaleza': $('#id_naturaleza').val(),
                                                'cod_relacion_super_cia': $('#id_cod_relacion_super_cia').val(),
                                                'tipo_retencion': $('#id_tipo_retencion').val(),
                                                'porc_retencion': $('#id_porc_retencion').val()
                                            },
                                            function(data){
                                               window.location.href = '/plancuenta/';
                                        });
                                    }
                                    else
                                    {
                                        $('#id_descripcion').parent().addClass("has-error");
                                        $('#id_porc_retencion').parent().addClass("has-error");
                                    }
                                }
                                else{ // No es de tipo retención
                                    if( (descripcion != ""))
                                    {
                                        $.post('/plancuenta/editar/',
                                            {
                                                'id': id,
                                                'descripcion': $('#id_descripcion').val(),
                                                'clase_cuenta': $('#id_clase_cuenta').val(),
                                                'naturaleza': $('#id_naturaleza').val(),
                                                'cod_relacion_super_cia': $('#id_cod_relacion_super_cia').val()
                                            },
                                            function(data){
                                               window.location.href = '/plancuenta/';
                                        });
                                    }
                                    else
                                    {
                                        $('#id_descripcion').parent().addClass("has-error");
                                    }
                                }
                                break;
                        }
                    }
                    $("#alert-yn").modal('hide');
                    break;
                case 3: // Opcion Eliminar
                    var id = $('#alert-yn').attr("data-id");
                    $.post('/plancuenta/eliminar/',
                    {
                        'id':id
                        },
                        function(data)
                        {
                        window.location.href = '/plancuenta/';
                    });
                    break;
            }
        });
    });

