function validar_total_pagar(valor)
{
    var val = parseFloat($(valor).val());
    var tabla_cru_doc = $("#tabla_cruce_documentos");
    var tabla_doc_pagar = $("#tabla_formset_detalle_doc");
    var total_fact = 0.0;
    var total_doc = 0.0;

    $(tabla_cru_doc).find(".valor").each(function(){
        total_doc += parseFloat($(this).val());
    });
    $(tabla_doc_pagar).find(".valor").each(function(){
        total_fact += parseFloat($(this).val());
    });
    if(total_doc > total_fact)
    {
        $("#tit-alert").html("Alerta");
        $("#body-alert").html('<p>El valor excede el total de pago de la Factura</p>');
        $("#alert-yn").modal();
        $(valor).val("0.00");
    }
    else
    {
        if(val>0){
            var tr = $(valor).parent().parent();
            var check = $(tr).find(".checks");
            if (!$(check).is(':checked'))
                $(check).prop('checked', true);

        }
    }
}

/*******************************
 * Suma los totales a pagar
 */
function totales_cruce_doc()
{
    var totales = 0.0;
    var saldo_anterior = $("#saldo_anterior");
    var total_pago = $("#total_pago");
    var saldo_actual =$("#saldo_actual");
    var s_anterior = 0.0;
    var s_actual = 0.0;

    $("#tabla_cruce_documentos").find(".checks").each(function(){
        if($(this).is(':checked'))
        {
            var tr = $(this).parent().parent();
            var cant = parseFloat($(tr).find(".valor").val());
            totales = totales + cant;
        }
    });
    $(total_pago).text("$ " + totales.toFixed(2));

    s_anterior = parseFloat($(saldo_anterior).text().replace("$"," "));

    if(s_anterior!=0)
    {
        s_actual = s_anterior - totales;
        $(saldo_actual).text("$ " + s_actual.toFixed(2));
    }
    else
        $(saldo_actual).text("$ 0.00");
}
/***************************
 * Inhabilita las filas de una tabla
 * excepto la fila que le envian
 * @param tr
 * @constructor
 */
function InhabilitarFilas(tr)
{
    var tabla = $(tr).parent().parent().get(0);
    $(tabla).find("tbody").find("tr").each(function(){
        if(this!=tr){
            $(this).find("input").attr("readonly",true);
            $(this).find(".checks").attr("disabled",true);
        }
    });
}
/***************************
 * habilita las filas de una tabla
 *
 * @param tr
 * @constructor
 */
function HabilitarFilas(tr)
{
    var tabla = $(tr).parent().parent().get(0);
    $(tabla).find("tbody").find("tr").each(function(){
            $(this).find("input").attr("readonly",false);
            $(this).find(".checks").attr("disabled",false);
    });
}
/******************************
 * Función para copiar el valor
 * de un input de class1 a uno
 * de class2, el tr es para saber en
 * que fila me encuentro
 * @param tr
 * @param class1
 * @param class2
 * @constructor
 */
function CopiarValor(tr, class1, class2){
    var input_class2 = $(tr).find("."+class2);
    var input_class1 = $(tr).find("."+class1);
    $(input_class2).val(parseFloat($(input_class1).val()).toFixed(2));
    return null;
}
/***********************************
 * Agrega puntos decimales a los
 * inputs
 * @param e
 */
function agregar_punto(e)
{
    var numero = $(e).val();
    var contador = 0.0;

    if(numero!=""){
        if (!isNaN(numero))
            $(e).val(parseFloat(numero).toFixed(2));
        else
            $(e).val((0).toFixed(2));
    }
    else
        $(e).val((0).toFixed(2));
}
function solo_numero(event, input)
{
    var contador = 0;
    // Allow: backspace, delete, tab, escape, enter and.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39) || (event.keyCode == 110))
        {
             // let it happen, don't do anything
            if(event.keyCode == 110){
                var texto = $(input).val();
                for(var i=0; i<texto.length; i++)
                {
                    if(texto[i]==".")
                        contador++;
                }
            }
            if(contador > 0)
                event.preventDefault();
            else
                return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
function NumeroNatural(event, input)
{
    var contador = 0;
    // Allow: backspace, delete, tab, escape, enter and.
    if ( $.inArray(event.keyCode,[46,8,9,27,13,190,127]) !== -1 ||
         // Allow: Ctrl+A
        (event.keyCode == 65 && event.ctrlKey === true) ||
         // Allow: home, end, left, right
        (event.keyCode >= 35 && event.keyCode <= 39))
        {
            return;
    }
    else {
        // Ensure that it is a number and stop the keypress
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault();
        }
    }
}
/********************************************
 * Para el cambio de las columnas de
 *         la forma de pago
 ******************************************/
function cambio_combo(e){
        var n = parseInt($(e).val());
        var tabla_cuentas_pagar = $("#tabla_formset_detalle_doc");
        var tabla_cruce_cta = $("#tabla_cruce_cuenta");
        var tabla_cruce_doc = $("#tabla_cruce_documentos");
        var mensaje_sin_doc_cruzar = $("#mensaje_sin_doc_cruzar");
        var totales_observacion = $("#total_observacion");
        switch (n)
        {
            case 1:     // Cruce de Documentos
                $(tabla_cuentas_pagar).show();
                $("#contenedor_detalle_comp").show();
                if ($(tabla_cruce_doc).attr("data-show") == "1")
                    $(tabla_cruce_doc).show();
                $(tabla_cruce_cta).hide();
                if($(tabla_cruce_doc).attr("data-show")=="0")
                {
                    $(mensaje_sin_doc_cruzar).show();
                    $(totales_observacion).hide();
                }
                break;
            case 2:     // Cruce de Cuentas
                $(tabla_cuentas_pagar).hide();
                $("#contenedor_detalle_comp").hide();
                if ($(tabla_cruce_doc).attr("data-show") == "1")
                    $(tabla_cruce_doc).hide();
                $(tabla_cruce_cta).show();
                if($(tabla_cruce_doc).attr("data-show")=="0")
                {
                    $(mensaje_sin_doc_cruzar).hide();
                    $(totales_observacion).show();
                }
                break;
            default :
                break;
        }
}
function SendData(submit, event)
{
    var total_pago = parseFloat($("#total_pago").text().replace("$", ""));
    var saldo_anterior = parseFloat($("#saldo_anterior").text().replace("$", ""));
    var input_fecha = $("#id_fecha");
    var combo_forma_pago = $("#id_forma_pago_cabecera").val();
    var flag_fecha = false;
    var flag_total_pago = false;
    var flag_saldo_anterior = false;
    var flag_cuenta = false;
    var flag_cuenta_valor = false;

    if(combo_forma_pago==1) //Cruce de Documentos
    {
        if(total_pago == 0)
            flag_total_pago = true;
        if(saldo_anterior == 0)
            flag_saldo_anterior = true;
        if($(input_fecha).val()=="")
            flag_fecha = true;
        if(flag_fecha || flag_total_pago || flag_saldo_anterior){
            $("#tit-alert").html("Alerta");
            $("#body-alert").html('<p class="tick">Debe de ingresar los siguientes campos:</p>');
            if(flag_fecha)
            {
                $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Fecha</span></strong></p>');
                $(input_fecha).addClass("campo_requerido");
                $(input_fecha).focus();
            }
            if(flag_total_pago)
                $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Debe de ingresar un valor en los documentos cruzados</strong></span></p>');
            if(flag_saldo_anterior)
                $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Debe de seleccionar alguna factura para pagar.</strong></span></p>');
            $("#alert-yn").modal();
            event.preventDefault();
        }
    }
    else //Cruce de Cuentas
    {
        var cont = 0;
        var cont_doc = 0; //Cuenta cuantos estan en "check"
        if($(input_fecha).val()=="")
            flag_fecha = true;
        $("#tabla_cruce_cuenta tbody").find("tr").each(function(){
            var check = $(this).find(".checks");
            var cuenta = $(this).find(".cuenta").val();
            var valor = parseFloat($(this).find(".valor").val());

            if($(check).is(':checked'))
            {
                con_doc++;
                if(cuenta == "" || valor <= 0)
                    cont++;
            }
        });
        if(cont_doc==0)
        {
            $("#tit-alert").html("Alerta");
            $("#body-alert").html('<p>Debe seleccionar algun documento para poder hacer el cruce' +
                ' de cuentas</p>');
            $("#alert-yn").modal();
            event.preventDefault();
        }
        else
        {
            if(cont>0)
                flag_cuenta = true;
            if(flag_fecha || flag_cuenta)
            {
                $("#tit-alert").html("Alerta");
                $("#body-alert").html('<p class="tick">Debe de ingresar los siguientes campos:</p>');
                if(flag_fecha)
                {
                    $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Fecha</span></strong></p>');
                    $(input_fecha).addClass("campo_requerido");
                    $(input_fecha).focus();
                }
                if(flag_cuenta)
                {
                    $("#body-alert").append('<p><span class="glyphicon glyphicon-remove tick">    <strong class="tick-text">Ingrese una cuenta y un valor válido en el cruce de cuenta</strong></span></p>');
                }
                $("#alert-yn").modal();
                event.preventDefault();
            }
        }
    }
    event.preventDefault();
}

$(document).ready(function(e){
    cambio_combo($("#id_forma_pago_cabecera"));
    totales_cruce_doc();
    $("#tabla_cruce_documentos").find(".valor").each(function(){
        $(this).change(function(){
            validar_total_pagar(this);
            totales_cruce_doc();
        });
        $(this).focusout(function(){
            validar_total_pagar(this);
            totales_cruce_doc();
        });
    });
    $("#tabla_cruce_cuenta tbody").find(".checks").each(function(){
        var tr = $(this).parent().parent();
        var cuenta = $(tr).find(".cuenta");
        var valor = $(tr).find(".valor");
        $(this).click(function(){
            if($(this).is(':checked'))
            {
                $(cuenta).attr("disabled", false);
                $(valor).attr("disabled", false);
                $(cuenta).focus();
            }
            else
            {
                $(cuenta).val("");
                $(valor).val("0.00");
                $(cuenta).attr("disabled", true);
                $(valor).attr("disabled", true);
            }
        });
    });
    $("#tabla_cruce_documentos").find(".checks").each(function(){
        $(this).click(function(e){
            var saldo_anterior = 0.0;
            var tr = $(this).parent().parent();
            var total_doc_cruzar = parseFloat($(tr).find(".saldo").val());
            var tabla = $("#tabla_formset_detalle_doc");
            $(tabla).find(".checks").each(function(){
                var tr = $(this).parent().parent().get(0);
                if($(this).is(':checked'))
                {
                    saldo_anterior = parseFloat($(tr).find(".valor").val());
                }
            });
            if(saldo_anterior>0)
            {
                if($(this).is(':checked'))
                {
                    var saldo_actual = $("#saldo_actual").text().replace("$", "");
                    if (saldo_actual > total_doc_cruzar)
                    {
                        CopiarValor(tr,"saldo","valor");
                    }
                    else
                    {
                        $(tr).find(".valor").val(saldo_actual);
                    }
                    totales_cruce_doc();
                }
                else
                {
                    $(tr).find(".valor").val("0.00");
                    totales_cruce_doc();
                }
            }
            else
            {
                /*******************************************************
                *   Parametros del popup yes/no
                *   id = alert-yn
                *   id-cambio-titulo = tit-alert
                *   id-cambio-cont = body-alert
                *   id-aceptar = ok-alert
                */
                $("#tit-alert").html("Alerta");
                $("#body-alert").html('<p>Debe de seleccionar una factura para realizar el cruce de documentos</p>')
                $("#alert-yn").modal();
                e.preventDefault();
            }
        });
    });
    function log( message ) {
        $( "<div>" ).text( message ).prependTo( "#log" );
        $( "#log" ).scrollTop( 0 );
    }

    $( ".cuenta" ).autocomplete({
            source: "/cuentas_pagar/lista_proveedores_pagar/cruce_documentos/buscar_cta/",
            data:{tag:$(this).val()},
            minLength: 2,
            select: function( event, ui ) {
               log( ui.item ?
               "Selected: " + ui.item.value + " aka " + ui.item.id :
               "Nothing selected, input was " + this.value );
            }
        });


    /****************
     * Para inhabilitar que el enter haga submit
     */
    $("body").find("input").keypress(function(e){
         if ( e.which == 13 ) e.preventDefault();
    });
    $(".selectpicker").selectpicker();

    $("#grabar").click(function(e){
        SendData(this,e);

    });

    $(".numerico").keydown(function(event) {
        solo_numero(event, this);
    });

    $("#id_forma_pago_cabecera").change(function(){
        cambio_combo(this);
    });
    $("#id_fecha_cheque").datepicker();

    $(".numerico").focusout(function(event) {
        agregar_punto(this);
        var tr = $(this).parent().parent();
        var saldo = parseFloat($(tr).find(".saldo").val());
        if(parseFloat($(this).val()) > saldo){
            $("#alert-yn").attr("data-id", $(this).attr("id"));
            $("#alert-yn").attr("data-target",1);
            $("#tit-alert").html("Alerta");
            $("#body-alert").html('<p>El valor ingresado en superior al saldo del documento</p>');
            $("#alert-yn").modal();
            event.preventDefault();
            $(this).val("0.00");
            $(this).focus();
        }
    });
    var tabla = $("#tabla_formset_detalle_doc");
    var cont = 0;
    $(tabla).find(".checks").each(function(){
        /******************************
         * Script para que cuando haga "actualizar se mantengan
         * los valores de los totales
         * @type {*|jQuery}
         */
        var tr = $(this).parent().parent().get(0);
        if($(this).is(':checked'))
        {
            CopiarValor(tr,"saldo","valor");
            InhabilitarFilas(tr);
            $("#saldo_anterior").text("$ "+$(tr).find(".valor").val());
            totales_cruce_doc();
        }

        $("#total_pagar").text("$ " + $(tr).find(".valor").val());
        $(this).click(function(){
            var tr = $(this).parent().parent().get(0);
            if($(this).is(':checked'))
            {
                CopiarValor(tr,"saldo","valor");
                InhabilitarFilas(tr);
                $("#saldo_anterior").text("$ "+$(tr).find(".valor").val())
                totales_cruce_doc();
            }
            else
            {
                $(tr).find(".valor").val("0.00");
                HabilitarFilas(tr);
                $("#saldo_anterior").text($(tr).find(".valor").val())

                // Encera los valores de la tabla de cruce de documentos
                $("#tabla_cruce_documentos").find(".checks").each(function(){
                    var tr = $(this).parent().parent();
                    $(tr).find(".valor").val("0.00");
                    $(this).prop('checked', false);
                });

                totales_cruce_doc();
            }
            $("#total_pagar").text("$ " + $(tr).find(".valor").val());
        });
    });


    $("#id_fecha").datepicker();
    $("#id_fecha").change(function(){
        if($(this).val()!="" && $(this).hasClass("campo_requerido")){
            $(this).removeClass("campo_requerido");
        }
    });
    /*****************************************
     * Autocomplete de proveedores
     *****************************************/
    $("#id_proveedor").autocomplete({
    source: "/cuentas_pagar/pago_proveedores/buscarproveedores/",
    data:{tag:$(this).val()},
    minLength: 2,
    select: function( event, ui ) {
        event.preventDefault();
        var padres = jQuery(event.target).parent().parent();
        var padre = padres[0];
        var padre_detalle_cta = padre.children[1];
        var cuenta = $(padre_detalle_cta.firstElementChild);
        $(this).val(ui.item.value);
            cuenta.val(ui.item.key);
        }
    });
    // Carga la tabla de documentos por pagar
    $("#cargar_pagos").click(function(){
        cargar_tabla_pagos($("#id_proveedor"));
    });

});

