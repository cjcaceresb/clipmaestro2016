/*********************************************************************
 *
 * Las variables que representan a estos divs
 *
    <div class="row form-group">     //****** div_cont
        <div class="col-xs-5 col-sm-5 col-md-5">   //****** div_cont_princ
            <input id="id_detalle_ret-0-tipo" class="form-control cab_form_cont oculto" maxlength="20" name="detalle_ret-0-tipo" value="r">
            <input id="id_detalle_ret-0-codigo_ret" class="form-control cab_form_cont oculto" maxlength="20" name="detalle_ret-0-codigo_ret" value="322">
            <label class="col-xs-5 col-sm-5 col-md-5">  //***** contenido_label
                <b>Ret Fte. 322</b>
            </label>
            <div class="col-xs-7 col-sm-7 col-md-7"> //******** contenido_input
                //********** select_cont_prin
                <select id="id_detalle_ret-0-cod_cta" class="form-control cod_ret_cta selectpicker show-tick combo_ret_fte_detalle" data-style="slc-primary" name="detalle_ret-0-cod_cta" style="display: none;">
                <option value="9">Retencion Fuente 1%</option>
                </select>
            </div>
        </div>
        <div class="col-xs-7 col-sm-7 col-md-7">
            <div class="col-xs-4 col-sm-4 col-md-4">  //**** base_ret
                <input id="id_detalle_ret-0-base" class="form-control cab_form_cont" readonly="" maxlength="20" name="detalle_ret-0-base">   //******* input_base_ret
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">    //********** porc_ret
                <input id="id_detalle_ret-0-porcentaje" class="form-control cab_form_cont" readonly="" maxlength="20" name="detalle_ret-0-porcentaje">   //***** input_porc_ret
            </div>
            <div class="col-xs-4 col-sm-4 col-md-4">  //******* total_ret
                <input id="id_detalle_ret-0-total" class="form-control cab_form_cont" readonly="" maxlength="20" name="detalle_ret-0-total">   //******** input_total_ret
            </div>
        </div>
    </div>
 */


var cuentas_ret = [];

function LlenarArrayRetenciones(cod_retenciones, cod_ret_selec, i, valor, porc_ret_fte)
{
    if(i==0){ // La primera vez que ingresa una RETENCION (FUENTE o IVA)
        cod_retenciones[i] = new Array(3);
        cod_retenciones[i][0] = cod_ret_selec;
        cod_retenciones[i][1] = valor;
        cod_retenciones[i][2] = porc_ret_fte;
        i++;
    }
    else
    {
        var cont =0; // variable para contabilizar las concurrencias entre el codigo de las retenciones
        for (var j=0; j< cod_retenciones.length; j++){
            if(cod_retenciones[j][0] == cod_ret_selec)
            {
                cod_retenciones[j][1]= parseFloat(cod_retenciones[j][1])+parseFloat(valor);
                cont++;
            }
        }
        if(cont == 0){ // Si no existe ningun codigo de retención repetido se crea otra instancia
            cod_retenciones[i] = new Array(3);
            cod_retenciones[i][0]=cod_ret_selec;
            cod_retenciones[i][1]=valor;
            cod_retenciones[i][2] = porc_ret_fte;
            i++;
        }
    }
    return i;
}

function LlenarFilasRetencion(cod_retenciones, cuentas_ret, url_ajax, tipo, x, cont_forms)
{
    /**********************************************
     * Creacion del  variables de tags
     */
    var div_cont = $('<div class="row form-group"></div>');
    var div_cont_princ = $('<div class="col-xs-5 col-sm-5 col-md-5"></div>');
    var contenido_label = $('<label class="col-xs-5 col-sm-5 col-md-5"></label>');
    var contenido_input = $('<div class="col-xs-7 col-sm-7 col-md-7"></div>');
    var input_codigo = $('<input id="id_detalle_ret-'+cont_forms+'-codigo_ret" name="detalle_ret-'+cont_forms+'-codigo_ret" maxlength="20" class="form-control cab_form_cont oculto"/>');
    var input_tipo = $('<input id="id_detalle_ret-'+cont_forms+'-tipo" name="detalle_ret-'+cont_forms+'-tipo" maxlength="20" class="form-control cab_form_cont oculto"/>');
    var input_cont_prin = $('<input id="id_detalle_ret-'+cont_forms+'-cod_cta" name="detalle_ret-'+cont_forms+'-cod_cta" maxlength="20" class="form-control cab_form_cont buscar_ret cod_ret_cta" placeholder="Ingrese una Cuenta"/>');

    var select_cont_prin = $('<select id="id_detalle_ret-'+cont_forms+'-cod_cta" name="detalle_ret-'+cont_forms+'-cod_cta" class="form-control cod_ret_cta selectpicker show-tick combo_ret_fte_detalle" data-style="slc-primary"/>');

    var div_cont_detalle = $('<div class="col-xs-7 col-sm-7 col-md-7"></div>');
    var base_ret = $('<div class="col-xs-4 col-sm-4 col-md-4"></div>');
    var input_base_ret = $('<input id="id_detalle_ret-'+cont_forms+'-base" name="detalle_ret-'+cont_forms+'-base" maxlength="20" class="form-control cab_form_cont" readonly/>');
    var porc_ret = $('<div class="col-xs-4 col-sm-4 col-md-4"></div>');
    var input_porc_ret = $('<input id="id_detalle_ret-'+cont_forms+'-porcentaje" name="detalle_ret-'+cont_forms+'-porcentaje" maxlength="20" class="form-control cab_form_cont" readonly/>');
    var total_ret = $('<div class="col-xs-4 col-sm-4 col-md-4"></div>');
    var input_total_ret = $('<input id="id_detalle_ret-'+cont_forms+'-total" name="detalle_ret-'+cont_forms+'-total" maxlength="20" class="form-control cab_form_cont" readonly/>');
    /**************************************************
     *
     */
    $(input_codigo).val(cod_retenciones[x][0]);
    $(input_codigo).attr("value", cod_retenciones[x][0]);
    $(input_tipo).val(tipo);
    $(input_tipo).attr("value",tipo);
    $(contenido_label).append($('<b></b>').text("Ret Fte. "+cod_retenciones[x][0])); // Label de la retencion
    $(input_base_ret).val(parseFloat(cod_retenciones[x][1]).toFixed(2)); //input de la base de la retención fte
    $(input_porc_ret).val(cod_retenciones[x][2]); // Porcentaje de la retencion a la fuente
    $(input_total_ret).val(parseFloat(parseFloat(cod_retenciones[x][1])*parseFloat(cod_retenciones[x][2])/100).toFixed(2));  // Total de retencion a la fuente
    for(var i=0; i<cuentas_ret.length; i++){
        if(cuentas_ret[i][0]==cod_retenciones[x][0]){
            $(input_cont_prin).val(cuentas_ret[i][2])
            if(cuentas_ret[i][3]){
                $(input_cont_prin).addClass("campo_requerido")
            }
        }
    }
    var porc_ret_post = $(input_porc_ret).val();
    $.ajax({
        url: url_ajax,
        type: 'POST',
        async: false,
        cache: false,
        timeout: 300,
        data: {
            'porc_ret': porc_ret_post,
            'tipo': tipo
        },
        error: function(){
            return true;
        },
        success: function(data){
            for(i=0; i<data.length; i++)
            {
                select_cont_prin.append('<option value="' + data[i].id + '">' + data[i].descripcion + '</option>');
            }
        }
    });

    $(contenido_input).append(select_cont_prin);
    $(div_cont_princ).append(input_tipo);
    $(div_cont_princ).append(input_codigo);
    $(div_cont_princ).append(contenido_label);
    $(div_cont_princ).append(contenido_input);
    $(div_cont).append(div_cont_princ);
    $(base_ret).append(input_base_ret);
    $(porc_ret).append(input_porc_ret);
    $(total_ret).append(input_total_ret);
    $(div_cont_detalle).append(base_ret);
    $(div_cont_detalle).append(porc_ret);
    $(div_cont_detalle).append(total_ret);
    $(div_cont).append(div_cont_detalle);

    $("#retenciones").append(div_cont);
    cont_forms++;
    return cont_forms;
}


function CalcularRetencion()
{
    var bandera = CalcularTotales();
    if (!bandera){
        $("#iva_ret_detalle").addClass("oculto");
    }
    else{
        $("#iva_ret_detalle").removeClass("oculto");
        $("#id_fecha_emision_retencion").val($("#id_fecha_asiento").val());
    }
    var div_ret = $("#retenciones");
    if($(div_ret).html()!=""){
        var i=0;
        $(div_ret).find(".cod_ret_cta").each(function(){
            cuentas_ret[i] = new Array(4);
            var codigo = $(this).parent().parent().find("input").get(1);
            cuentas_ret[i][0] = $(codigo).val();
            var tipo = $(this).parent().parent().find("input").get(0);
            cuentas_ret[i][1] = $(tipo).val();
            cuentas_ret[i][2] = $(this).val();
            if($(this).hasClass("campo_requerido"))
            {
                cuentas_ret[i][3] = true;
            }
            else
            {
                cuentas_ret[i][3] = false;
            }
            i++;
        });
    }

   $("#retenciones").empty();

    var cod_retenciones = [];
    var cod_retenciones_iva_b = [];
    var cod_retenciones_iva_s = [];
    var i=0; // contador para el numero de ret_fte
    var p=0; // contador para el numero de ret_iva_b
    var k=0; // contador para el numero de ret_iva_s
    var cont_forms = 0; // Cuenta la cantidad de form


    $("#tabla_formset tbody tr").each(function(){
        var iva_valor_comb = $(this).find('select[data-name="combo_iva"]');
        var iva_valor;
        var retencion = $(this).find('select[data-name="combo_cod_ret_fte"]');
        var cod_ret_selec;
        var porc_ret_fte;
        var iva = $(this).find('select[data-name="combo_cod_ret_iva"]');
        var cod_ret_iva_selec;
        var por_ret_iva_selec;
        var bien_servicio = $(this).find('select[data-name="bien_serv"]');
        var bien_servicio_selec;
        var valor_input = $(this).find('.valor');
        var valor = 0.0;
        if($(valor_input).val()!=""){
            valor = parseFloat($(valor_input).val());
        }
        iva_valor = $(iva_valor_comb).find("option:selected").attr("data-porcentaje");

        cod_ret_selec = $(retencion).find("option:selected").attr("data-codigo");
        porc_ret_fte = $(retencion).find("option:selected").attr("data-porcentaje");

        cod_ret_iva_selec = $(iva).find("option:selected").attr("data-codigo");
        por_ret_iva_selec = $(iva).find("option:selected").attr("data-porcentaje");

        bien_servicio_selec = $(bien_servicio).find("option:selected").attr("value");

        if(cod_ret_iva_selec != ""){
            if(bien_servicio_selec==1){
                p = LlenarArrayRetenciones(cod_retenciones_iva_b, cod_ret_iva_selec, p, (valor*iva_valor/100), por_ret_iva_selec)
            }
            else{
                k = LlenarArrayRetenciones(cod_retenciones_iva_s, cod_ret_iva_selec, p, (valor*iva_valor/100), por_ret_iva_selec)
            }
        }
        if(cod_ret_selec!="")
        {
            i = LlenarArrayRetenciones(cod_retenciones, cod_ret_selec, i, valor, porc_ret_fte)
        }
    });
    for(var x=0; x<cod_retenciones.length; x++){
        if(cod_retenciones[x][2]!=0)
        {
            cont_forms = LlenarFilasRetencion(cod_retenciones, cuentas_ret, "/compras/buscar_retencion/", "r", x, cont_forms)
        }
    }
    for(var x=0; x<cod_retenciones_iva_b.length; x++){
        cont_forms = LlenarFilasRetencion(cod_retenciones_iva_b, cuentas_ret, "/compras/buscar_retencion/", 1, x, cont_forms)
    }
    for(var x=0; x<cod_retenciones_iva_s.length; x++){
        cont_forms = LlenarFilasRetencion(cod_retenciones_iva_s, cuentas_ret, "/compras/buscar_retencion/", 2, x, cont_forms)
    }
    $(".combo_ret_fte_detalle").selectpicker("refresh");
    $("#id_detalle_ret-TOTAL_FORMS").attr("value",cont_forms);
}
$(function ()
{
    CalcularRetencion();
    //$('#collapseOne').collapse("hide");
    $(".valor").focusout(function(){CalcularTotales()});
    $(".iva").focusout(function(){CalcularTotales()});
});

function ActualizarTRetenciones()
{
    var cod_retenciones = [];
    var cod_retenciones_iva_b = [];
    var cod_retenciones_iva_s = [];
    var i=0; // contador para el numero de ret_fte
    var p=0; // contador para el numero de ret_iva_b
    var k=0; // contador para el numero de ret_iva_s
    var cont_forms = 0; // Cuenta la cantidad de form


    $("#tabla_formset tbody tr").each(function(){
        var iva_valor_comb = $(this).find('select[data-name="combo_iva"]');
        var iva_valor;
        var retencion = $(this).find('select[data-name="combo_cod_ret_fte"]');
        var cod_ret_selec;
        var porc_ret_fte;
        var iva = $(this).find('select[data-name="combo_cod_ret_iva"]');
        var cod_ret_iva_selec;
        var por_ret_iva_selec;
        var bien_servicio = $(this).find('select[data-name="bien_serv"]');
        var bien_servicio_selec;
        var valor_input = $(this).find('.valor');
        var valor = 0.0;

        if($(valor_input).val()!=""){
            valor = parseFloat($(valor_input).val());
        }
        iva_valor = $(iva_valor_comb).find("option:selected").attr("data-porcentaje");

        cod_ret_selec = $(retencion).find("option:selected").attr("data-codigo");
        porc_ret_fte = $(retencion).find("option:selected").attr("data-porcentaje");

        cod_ret_iva_selec = $(iva).find("option:selected").attr("data-codigo");
        por_ret_iva_selec = $(iva).find("option:selected").attr("data-porcentaje");

        bien_servicio_selec = $(bien_servicio).find("option:selected").attr("value");

        if(cod_ret_iva_selec != ""){
            if(bien_servicio_selec == 1){  //Si es un BIEN
                p = LlenarArrayRetenciones(cod_retenciones_iva_b, cod_ret_iva_selec, p, (valor*iva_valor/100), por_ret_iva_selec)
            }
            else{ // Si es un SERVICIO
                k = LlenarArrayRetenciones(cod_retenciones_iva_s, cod_ret_iva_selec, p, (valor*iva_valor/100), por_ret_iva_selec)
            }
        }
        if(cod_ret_selec!="") // RETENCION FUENTE
        {
            i = LlenarArrayRetenciones(cod_retenciones, cod_ret_selec, i, valor, porc_ret_fte)
        }
    });

    /*********************************
     * Llenar la tabla de retenciones
     */
    for(var x=0; x<cod_retenciones.length; x++){
        var clonar_tr = $("#t_retenciones tbody tr:first").clone();

        if( cod_retenciones[x][2] != 0 ) // Si el porcentaje de retención a la fuente es diferente de cero.
        {
            var input_codigo = $(clonar_tr).find(".codigo_ret");
            $(input_codigo).val(cod_retenciones[x][0]);
            var input_tipo = $(clonar_tr).find(".tipo");
            $(input_tipo).val("r");
            $(input_tipo).attr("value","r");
            var input_base_ret = $(clonar_tr).find(".base");
            $(input_base_ret).val(parseFloat(cod_retenciones[x][1]).toFixed(4));
            var input_porcentaje = $(clonar_tr).find(".porcentaje");
            $(input_porcentaje).val(cod_retenciones[x][2])
            var input_total = $(clonar_tr).find(".total");
            $(input_total).val(parseFloat(parseFloat(cod_retenciones[x][1])*parseFloat(cod_retenciones[x][2])/100).toFixed(4));
            $("#t_retenciones tbody").append(clonar_tr);

        cont_forms++;
        }
    }
}
