#! /usr/bin/python
# -*- coding: UTF-8-*-
from datetime import timedelta
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.forms.util import ErrorList
from librerias.funciones.funciones_vistas import *
from django.http import HttpResponse

__author__ = 'Roberto'

def ProvisionarCompra(formulario_provision, tipo_c, empresa, fecha_actual,  request):
    '''
    Función quer permite guardar la cantidad a provisionar
    '''
    bandera = 0     # Validador
    cantidad_provision = 0
    num_doc_ret = None

    select_retencion = request.POST.get("tipo")


    if formulario_provision.getCantidadDocPRov() != "":
        cantidad_provision = int(formulario_provision.getCantidadDocPRov())

    vigencia_doc = VigenciaDocEmpresa.objects.get(id=formulario_provision.getSerie())

    if formulario_provision.getFecha() < fecha_actual:
        bandera += 1

        now = datetime.datetime.now()
        try:
            secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=1)  # Secuencia de Compras
            fecha_comp = secuencia.fecha_actual
        except SecuenciaTipoComprobante.DoesNotExist:
            fecha_comp = now.strftime("%Y-%m-%d")
        errors = formulario_provision._errors.setdefault("fecha", ErrorList())
        errors.append(u"Campo Requerido")
        messages.error(request, u"Error: La fecha de la provisión no puede ser menor a la fecha del último comprobante: "+str(fecha_comp)+u", por favor verifique.")

    if formulario_provision.getFecha() > vigencia_doc.fecha_vencimiento:
        bandera += 1
        messages.error(request, u"Error: La fecha de registro de la provisión no puede ser mayor "
                                u"a la fecha de vigencia del documento: "+str(vigencia_doc.fecha_vencimiento))

    if cantidad_provision > 0 and cantidad_provision < vigencia_doc.sec_final:
        for i in range(0, cantidad_provision):
            cabecera_compra = CompraCabecera()
            cabecera_compra.fecha_reg = formulario_provision.getFecha()

            if select_retencion == "1":  # Retención
                cabecera_compra.concepto = u"Provisionar compra con retención"
            else:
                cabecera_compra.concepto = u"Provisionar compra sin retención"

            cabecera_compra.fecha_emi = formulario_provision.getFecha()
            cabecera_compra.ats = None
            cabecera_compra.base0 = 0.0
            cabecera_compra.baseiva = 0.0
            cabecera_compra.monto_iva = 0.0
            ###################################
            #  Llama a la función num_comp    #
            ###################################
            num_comp = get_num_comp(tipo_c.id, cabecera_compra.fecha_reg, True, request)
            if num_comp != -1:
                cabecera_compra.num_comp = num_comp
            else:
                bandera += 1

            cabecera_compra.num_doc = ""
            cabecera_compra.tipo_pago = TipoPago.objects.get(id=1)  #Contado
            cabecera_compra.base_ice = 0.0
            cabecera_compra.monto_ice = 0.0
            cabecera_compra.fecha_creacion = datetime.datetime.now()
            cabecera_compra.usuario_creacion = request.user.username
            cabecera_compra.status = 3
            cabecera_compra.save()

            if select_retencion == "1":  # Retención
                cabecera_compra.documento = Documento.objects.filter(status=1).get(id=24)
                num_doc_ret = get_num_doc(vigencia_doc, cabecera_compra.fecha_reg, request)
                retencion_cabecera = RetencionCompraCabecera()
                retencion_cabecera.compra_cabecera = cabecera_compra
                retencion_cabecera.vigencia_doc_empresa = vigencia_doc
                retencion_cabecera.fecha_emi = cabecera_compra.fecha_emi

                if num_doc_ret != -1:
                    retencion_cabecera.numero_ret = str(num_doc_ret)
                else:
                    messages.error(request, u"Error, el número ret. supera a la cantidad "
                                            u"registrada en el block de documentos vigentes de la empresa")
                    bandera += 1

                retencion_cabecera.direccion = empresa.direccion
                retencion_cabecera.totalretfte = 0.0
                retencion_cabecera.totalretiva = 0.0
                retencion_cabecera.fecha_creacion = datetime.datetime.now()
                retencion_cabecera.usuario_creacion = request.user.username
                retencion_cabecera.status = 3
                retencion_cabecera.save()

    else:
        bandera += 1
        messages.error(request, u"Error, ingrese una cantidad correcta de documentos a provisionar.")


    if num_doc_ret == -1:
        bandera += 1
        messages.error(request, u"Error, el número ret. supera a la cantidad registrada "
                                        u"en el block de documentos vigentes de la empresa")


    return bandera

def ProvisionarVenta(formulario_provision,  tipo_c, fecha_actual, request):
    '''
        Función quer permite guardar la cantidad a provisionar
    '''
    bandera = 0     # Validador
    cantidad_provision = 0
    num_doc = None

    if formulario_provision.getCantidadDocPRov() != "":
        cantidad_provision = int(formulario_provision.getCantidadDocPRov())

    vigencia_doc = VigenciaDocEmpresa.objects.get(id=formulario_provision.getSerie())

    if formulario_provision.getFecha() < fecha_actual:
        bandera += 1
        now = datetime.datetime.now()
        try:
            secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=2)  # Secuencia de Compras
            fecha_comp = secuencia.fecha_actual
        except SecuenciaTipoComprobante.DoesNotExist:
            fecha_comp = now.strftime("%Y-%m-%d")
        errors = formulario_provision._errors.setdefault("fecha", ErrorList())
        errors.append(u"Campo Requerido")
        messages.error(request, u"Error: La fecha de registro de la provisión no puede ser menor a la fecha de último comprobante: "+str(fecha_comp)+u", por favor verifique.")

    if formulario_provision.getFecha() > vigencia_doc.fecha_vencimiento:
        bandera += 1
        messages.error(request, u"Error: La fecha de registro de la provisión no puede ser mayor "
                                u"a la fecha de vigencia del documento:  "+str(vigencia_doc.fecha_vencimiento))

    if cantidad_provision > 0 and  cantidad_provision < vigencia_doc.sec_final:
        for i in range(0, cantidad_provision):
            venta_cabecera = Venta_Cabecera()
            venta_cabecera.vigencia_doc_empresa = vigencia_doc
            venta_cabecera.fecha = formulario_provision.getFecha()
            venta_cabecera.documento = Documento.objects.get(id=formulario_provision.getTipoDoc())
            venta_cabecera.pago_cont_credito = TipoPago.objects.get(id=1)
            venta_cabecera.tipo_descuento_combo = 0
            venta_cabecera.porc_descuento = 0
            venta_cabecera.subtotal_tarifa12 = 0.0
            venta_cabecera.subtotal_tarifa0 = 0.0
            venta_cabecera.descuento_tarifa12 = 0.0
            venta_cabecera.descuento_tarifa0 = 0.0
            venta_cabecera.total_ice = 0.0
            venta_cabecera.total_iva = 0.0

            ################## NUM DOC ############################
            #num_doc
            num_doc = get_num_doc(vigencia_doc, venta_cabecera.fecha, request)
            if num_doc != -1:
                venta_cabecera.num_documento = num_doc
            else:
                bandera += 1

            ################## NUM COMP ###########################
            num_comp = get_num_comp(tipo_c.id, venta_cabecera.fecha, True, request)
            if num_comp != -1:
                venta_cabecera.numero_comprobante = num_comp
            else:
                bandera += 1

            if bandera == 0.0:
                venta_cabecera.usuario_creacion = request.user.username
                venta_cabecera.fecha_creacion = datetime.datetime.now()
                venta_cabecera.status = 3
                venta_cabecera.save()

    else:
        bandera += 1
        messages.error(request, u"Error, ingrese una cantidad correcta de documentos a provisionar.")


    if num_doc == -1:
        bandera += 1
        messages.error(request, u"Error, el número doc. supera a la cantidad registrada "
                                        u"en el block de documentos vigentes de la empresa")

    return bandera

def ProvisionarCheque(formulario_provision, tipo_c, fecha_actual, request):
    '''
    Función que realiza el procedimiento de reservar secuencias de cheques para un futuro ser utilizados
    :param formulario_provision:
    :param tipo_c:
    :param fecha_actual:
    :param request:
    :return:
    '''
    bandera = 0     # Validador
    cantidad_provision = 0
    num_doc = None

    if formulario_provision.getCantidadDocPRov() != "":
        cantidad_provision = int(formulario_provision.getCantidadDocPRov())

    if formulario_provision.getFecha() < fecha_actual:
        bandera += 1
        now = datetime.datetime.now()

        try:
            secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=tipo_c.id)  # Secuencia de Compras
            fecha_comp = secuencia.fecha_actual
        except SecuenciaTipoComprobante.DoesNotExist:
            fecha_comp = now.strftime("%Y-%m-%d")
        messages.error(request, u"Error: La fecha de registro de la provisión no puede ser menor a la fecha de último "
                                u"registro de comprobante: "+str(fecha_comp)+u", por favor verifique.")

    if cantidad_provision > 0:
        for i in range(0, cantidad_provision):
            banco = Banco()
            banco.fecha_reg = formulario_provision.getFecha()
            banco.cuenta_banco = Cuenta_Banco.objects.get(id=formulario_provision.getCuentaBanco())
            banco.tipo_comprobante = tipo_c

            # NUM COMP
            num_comp = get_num_comp(tipo_c.id, banco.fecha_reg, True, request)

            if num_comp != -1:
                banco.num_comp = num_comp
            else:
                bandera += 1

            # NUM CHEQUE
            num_cheque = get_next_cheque(banco.cuenta_banco, request)

            banco.num_cheque = num_cheque
            banco.concepto = u"Provisión de Egreso - Cheque"
            banco.valor = 0.0
            banco.naturaleza = 2
            banco.fecha_cheque = banco.fecha_reg

            if bandera == 0.0:
                banco.usuario_creacion = request.user.username
                banco.fecha_creacion = datetime.datetime.now()
                banco.status = 3
                banco.save()

    else:
        bandera += 1
        messages.error(request, u"Error, ingrese una cantidad correcta de documentos a provisionar.")


    if num_doc == -1:
        bandera += 1
        messages.error(request, u"Error, al obtener el numero de comprobante")

    return bandera


################################################### FUNCIONES AJAX #####################################################
@csrf_exempt
@login_required(login_url='/')
def ajax_seleccionar_ultima_secuencia(request):

    id = request.POST.get("id", "")
    tipo_comp_venta = TipoComprobante.objects.get(status=1, id=2)
    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp_venta)
        secuencia = secuencia_tipo.secuencia
        documento_vigente = VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=datetime.datetime.now()).get(id=id)
        response_data = {"id": documento_vigente.id, "serie": documento_vigente.serie,
                         "vencimiento": documento_vigente.fecha_vencimiento.strftime('%Y-%m-%d'),
                         "autorizacion": documento_vigente.autorizacion,
                         "secuencia": documento_vigente.sec_actual,
                         "secuencia_comp": secuencia,
                         "status": 1}
    except SecuenciaTipoComprobante.DoesNotExist:
        response_data = {"status": 0, "respuesta": 'danger', "mensaje": 'Existió un error'}

    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")

@csrf_exempt
@login_required(login_url='/')
def ajax_seleccionar_ultima_secuencia_compras(request):
    id = request.POST.get("id", "")
    tipo_comp = TipoComprobante.objects.get(status=1, id=1)

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
        secuencia = secuencia_tipo.secuencia
        documento_vigente = VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=datetime.datetime.now()).get(id=id)

        response_data = {"id": documento_vigente.id, "serie": documento_vigente.serie,
                         "vencimiento": documento_vigente.fecha_vencimiento.strftime('%Y-%m-%d'),
                         "autorizacion": documento_vigente.autorizacion,
                         "secuencia": documento_vigente.sec_actual,
                         "secuencia_comp": secuencia,
                         "status": 1}
    except SecuenciaTipoComprobante.DoesNotExist:
        response_data = {"status": 0, "respuesta": 'danger', "mensaje": 'Existió un error'}

    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")


@csrf_exempt
@login_required(login_url='/')
def seleccionar_documento_prov_venta(request):
    id = request.POST.get("id", "")
    try:
        documento_vigente = VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=datetime.datetime.now()).filter(documento_sri__venta=True).get(id=id)
        response_data = {"status": 1, "id": documento_vigente.id, "serie": documento_vigente.serie,
                         "vencimiento": documento_vigente.fecha_vencimiento.strftime('%Y-%m-%d'),
                         "autorizacion": documento_vigente.autorizacion}
    except:
        pass
        response_data = {"status": 0, "respuesta": 'danger', "mensaje": 'Existió un error'}
    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")

@csrf_exempt
@login_required(login_url='/')
def get_documentos_provision(request):
    id_documento = request.POST.get("id", "")
    q = request.POST.get("fecha", "")
    respuesta = []

    try:
        fecha_split = str(q).split("-")
        fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))
        documento = Documento.objects.get(id=id_documento)

        vigencia = VigenciaDocEmpresa.objects.filter(status=1).\
                                    filter(documento_sri=documento,
                                           fecha_emi__lte=fecha.strftime("%Y-%m-%d"),
                                           fecha_vencimiento__gte=fecha.strftime("%Y-%m-%d"),
                                           documento_sri__venta=True, sec_actual__lt=F("sec_final")).order_by("serie")

        if len(vigencia) == 0:
            respuesta.append({"status": 0, "descripcion": "No se encuentra definido el documento "
                                                          "con su vigencia por favor ingrese"})

        else:

            for obj in vigencia:
                respuesta.append({"status": 1, "id": obj.id, "serie": obj.serie})

    except:
        pass
        respuesta.append({"status": -1, "id": 0, "descripcion": "No se encuentra definido el documento "
                                                                "con su vigencia por favor ingrese"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@csrf_exempt
@login_required(login_url='/')
def get_documentos_provision_retencion(request):
    q = request.POST.get("fecha", "")
    respuesta = []
    try:
        fecha_split = str(q).split("-")
        fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))
        documento = Documento.objects.get(id=24)    #Retencion


        vigencia = VigenciaDocEmpresa.objects.filter(status=1).\
            filter(documento_sri=documento, fecha_emi__lte=fecha.strftime("%Y-%m-%d"),
                   fecha_vencimiento__gte=fecha.strftime("%Y-%m-%d"), sec_actual__lt=F("sec_final")).order_by("serie")

        if len(vigencia) == 0:
            respuesta.append({"status": 0, "descripcion": "No se encuentra definido el documento "
                                                          "con su vigencia por favor ingrese"})

        else:

            for obj in vigencia:
                respuesta.append({"status": 1, "id": obj.id, "serie": obj.serie})

    except:
        pass
        respuesta.append({"status": -1, "id": 0, "descripcion": "No se encuentra definido el documento "
                                                                "con su vigencia por favor ingrese"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")


