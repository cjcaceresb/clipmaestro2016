#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from contabilidad.formularios.AsientoContableForm import *
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
from django.db.models import Q, Count, Min, Sum, Avg
import ho.pisa as pisa
import cStringIO as StringIO
import cgi
from django.contrib.auth.models import User
from django.template import RequestContext
from librerias.funciones.validacion_formularios import *
from django.template.loader import render_to_string
import datetime
import time
from django.db import connection
from django.http import HttpResponse
from django import template
from librerias.funciones.validacion_rucs import *
register = template.Library()
from django.core.paginator import *
from librerias.funciones.funciones_vistas import *
import operator
from django.db import connection

class ErrorContabilidad(Exception):
    def __init__(self, valor):
        self.valor = valor
    def __str__(self):
        return str(self.valor)

__author__ = 'Roberto'

def instancia_detalle_formset(cabecera_asiento):
    '''
    Función que instancia los parametros del formset de detalle comprobante contable
    :return:
    '''
    det_form = []
    detalle_comprobante = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_asiento)

    for obj in detalle_comprobante:

        if obj.centro_costo is not None:

            if obj.dbcr == "H":
                det_form.append({"codigo_cta": obj.plan_cuenta.id, "concepto": obj.detalle,
                        "dbcr": obj.dbcr, "haber": obj.valor, "centro_costo": obj.centro_costo.id})
            else:
                det_form.append({"codigo_cta": obj.plan_cuenta_id, "concepto": obj.detalle,
                        "dbcr": obj.dbcr, "debe": obj.valor, "centro_costo": obj.centro_costo.id})
        else:

            if obj.dbcr == "H":
                det_form.append({"codigo_cta": obj.plan_cuenta.id, "concepto": obj.detalle,
                        "dbcr": obj.dbcr, "haber": obj.valor, "centro_costo": ""})
            else:
                det_form.append({"codigo_cta": obj.plan_cuenta.id, "concepto": obj.detalle,
                        "dbcr": obj.dbcr, "debe": obj.valor, "centro_costo": ""})

    return det_form

def busqueda_contabilidad(buscador, tipo):
    '''
    Función que permite realizar la
    búsqueda de los asientos contables
    :param buscador:
    :return:
    '''
    predicates = []
    if buscador.getFechaInicio() is not None:
        predicates.append(('fecha__gte',  (buscador.getFechaInicio())))
    predicates.append(('fecha__lte',  (buscador.getFechaFin())))

    if tipo == 1:
        if buscador.getCuenta() is not None:
            predicates.append(('detalle_comp_contable__plan_cuenta',  buscador.getCuenta()))
    predicates.append(('numero_comprobante__icontains', buscador.getNumComprobante()))
    if buscador.getTipoComprobante() is not None:
        predicates.append(('tipo_comprobante', buscador.getTipoComprobante()))
    q_list = [Q(x) for x in predicates]
    entries = Cabecera_Comp_Contable.objects.filter(status=1).filter(reduce(operator.and_, q_list)).distinct().order_by("-fecha", "-numero_comprobante", "-fecha_creacion").distinct()
    return entries



def GuardarCabeceraAsientoContable(formulario, cabecera_cont, tipo, dia_actual,  request):
    '''
    Función que permite guardar la información de la cabecera
    del asiento contable
    :param formulario:
    :param cabecera_cont:
    :param tipo_c:
    :param dia_actual:
    :param fecha_actual:
    :param request:
    :return:
    '''
    cabecera_cont.concepto_comprobante = formulario.getConceptoComprobante()
    contador = 0
    tipo_c = TipoComprobante.objects.filter(status=1).get(id=6)     # Asiento
    tipo_c_apertura = TipoComprobante.objects.filter(status=1).get(id=26)   # Asiento Apertura


    if tipo == 5:   # Asiento Inicial
        fecha_inicio_sistema = get_parametros_empresa().fecha_inicio_sistema - datetime.timedelta(days=1)
        cabecera_cont.tipo_comprobante = tipo_c_apertura
        cabecera_cont.fecha = fecha_inicio_sistema
        num_comp = get_num_comp_contable(tipo_c_apertura, cabecera_cont.fecha, request)
        cabecera_cont.numero_comprobante = num_comp

    else:

        if formulario.getFecha() is None:
            contador += 1
            messages.error(request, u"Por favor ingrese una fecha válida para realizar el asiento")

        else:

            if formulario.getConceptoComprobante() is not None:
                if unicode(formulario.getConceptoComprobante()).isspace():
                    contador += 1
                    messages.error(request, u"Por favor ingrese un concepto de comprobante válido...")

            if formulario.getFecha() > datetime.datetime.now().date():
                contador += 1
                messages.error(request, u"Error la fecha de registro es mayor a la fecha actual")

            cabecera_cont.fecha = formulario.getFecha()
            cabecera_cont.tipo_comprobante = tipo_c
            num_comp = get_num_comp_contable(tipo_c, cabecera_cont.fecha, request)  #get_num_comp_cont
            cabecera_cont.numero_comprobante = num_comp

        #############################################
        #  Se valida la fecha de cierre de la       #
        #               empresa                     #
        #############################################
        if not esta_periodo_contable(cabecera_cont.fecha):
            contador += 1
            errors = formulario._errors.setdefault("fecha", ErrorList())
            errors.append(u"Campo Requerido")
            parametro_empresa = get_parametros_empresa()
            messages.error(request, u"La fecha del asiento no puede ser menor a la la fecha de cierre contable: "+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))


    if contador == 0.0:
        cabecera_cont.fecha_creacion = dia_actual
        cabecera_cont.usuario_creacion = request.user.username
        cabecera_cont.save()

    return contador

def GuardarDetalleAsientoContable(formset, cabecera_cont, dia_actual, request, tipo):
    '''
    Función que Guarda los
    Detalles de Asiento Contable
    :param formset:
    :param cabecera_cont:
    :param dia_actual:
    :param request:
    :return:
    '''
    total_debe = 0.0
    total_haber = 0.0
    contador = 0

    for form in formset:
        informacion = form.cleaned_data
        detalle_comp = Detalle_Comp_Contable()
        detalle_comp.cabecera_contable = cabecera_cont
        detalle_comp.fecha_asiento = cabecera_cont.fecha
        detalle_comp.plan_cuenta = PlanCuenta.objects.filter(status=1).get(id=informacion.get("codigo_cta"))

        if informacion.get("concepto", "").isspace() or informacion.get("concepto", "") == "":
            detalle_comp.detalle = cabecera_cont.concepto_comprobante
        else:
            detalle_comp.detalle = informacion.get("concepto")

        try:
            debe = redondeo(float(informacion.get("debe", 0)), 2)
        except:
            debe = 0
        try:
            haber = redondeo(float(informacion.get("haber", 0)), 2)
        except:
            haber = 0

        if debe > 0:
            total_debe += debe
        if haber > 0:
            total_haber += haber

        temp = redondeo(total_debe, 2) - redondeo(total_haber, 2)
        if temp < 0:
            temp = (temp * -1)

        if temp >= 0.00:  # Valida si son iguales y no son cero
            pass
        else:
            contador += 1

        if contador == 0:
            if float(informacion.get("debe")) != 0.0:
                detalle_comp.valor = redondeo(float(informacion.get("debe")), 2)
                detalle_comp.dbcr = "D"     # Debito

            if float(informacion.get("haber")) != 0.0:
                detalle_comp.valor = redondeo(float(informacion.get("haber")), 2)
                detalle_comp.dbcr = "H"     # Credito

            if informacion.get("codigo_cta") is None:
                raise ErrorContabilidad(u"Error, se requiere que se llene los campos de ingreso de cuenta")

            if informacion.get("centro_costo") != "":
                detalle_comp.centro_costo = Centro_Costo.objects.get(id=int(informacion.get("centro_costo")))


            if tipo == 5:                           # Asiento Inicial
                if detalle_comp.plan_cuenta.tipo_id == 1:  # Cta Banco
                    GuardarCtaBanco(detalle_comp, dia_actual, request)

            detalle_comp.fecha_creacion = dia_actual
            detalle_comp.usuario_creacion = request.user.username
            detalle_comp.save()

        else:
            messages.error(request, u"Error en los valores del debe y el haber verifique que sean "
                                    u"iguales y mayores que 0.0 para continuar")

    return contador

def GuardarCtaBanco(detalle_comp, dia_actual, request):
    '''
    Función Utilizada en el escenario de Asiento Inicial
    siempre que sea Cta Banco
    :param detalle_comp:
    :param dia_actual:
    :param request:
    :return:
    '''
    banco = Banco()
    banco.fecha_reg = detalle_comp.cabecera_contable.fecha
    banco.cuenta_banco = Cuenta_Banco.objects.get(status=1, plan_cuenta=detalle_comp.plan_cuenta)
    banco.tipo_comprobante = detalle_comp.cabecera_contable.tipo_comprobante
    banco.num_comp = detalle_comp.cabecera_contable.numero_comprobante
    banco.concepto = detalle_comp.cabecera_contable.concepto_comprobante

    if detalle_comp.dbcr == "D":
        banco.naturaleza = 1
    else:
        banco.naturaleza = 2

    banco.valor = detalle_comp.valor

    banco.fecha_creacion = dia_actual
    banco.usuario_creacion = request.user.username
    banco.save()

def Anular_Asiento_Contable(cabecera_comprobante, dia_actual, request):
    '''
    Función que realiza el procedimiento de
    anular el asiento contable
    :return:
    '''
    contador = 0
    try:
        detalle_comp = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_comprobante)
        cabecera_comprobante.status = 2
        cabecera_comprobante.fecha_actualizacion = dia_actual
        cabecera_comprobante.usuario_actualizacion = request.user.username
        cabecera_comprobante.save()

        for obj in detalle_comp:
            obj.status = 2
            obj.fecha_actualizacion = dia_actual
            obj.usuario_actualizacion = request.user.username
            obj.save()

        try:
            bancos = Banco.objects.filter(status=1, num_comp=cabecera_comprobante.numero_comprobante)

            for b in bancos:
                b.status = 2
                b.fecha_actualizacion = dia_actual
                b.usuario_actualizacion = request.user.username
                b.save()
        except:
            pass

    except:
        contador += 1

    return contador

def redireccionar_contabilidad(request, cabecera):
    """
    Función que redirecciona dependiendo de la
    acción que desee el usuario sobre asientos contables
    :return:
    """
    if request.GET.get("grabar_agregar", "") == "1":
        full_redirect_url = reverse("registrar_asiento_contable")

    elif request.GET.get("grabar_imprimir", "") == "1":
        redirect_url = reverse("registrar_asiento_contable")
        extra_params = '?imp=%s' % cabecera.id
        full_redirect_url = '%s%s' % (redirect_url, extra_params)
    else:
        full_redirect_url = reverse("lista_asientos")

    return full_redirect_url

# FUNCIONES AJAX
@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta(request):
    '''
    Función Ajax para el autocompletado
    las cuentas disponibles en la contabilidad
    :param request:
    :return:
    '''
    respuesta = []
    q = request.GET.get("term", "")
    try:
        # Cambiar en la base una columna que me indique las cuentas de contabilidad
        cuentas = PlanCuenta.objects.exclude(tipo__alias=3).exclude(tipo__alias=4).exclude(tipo__alias=7).exclude(tipo__alias=13).exclude(tipo__alias=14).exclude(tipo__alias=16).filter(status=1, nivel=5).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))[0:5]

        if len(cuentas) == 0:
            respuesta.append({"value": "No se encuentra registrado la cuenta", "key": "No se encuentra registrado la cuenta"})
        else:
            for p in cuentas:
                respuesta.append({"value": p.codigo+" - "+p.descripcion, "key": p.codigo+" - "+p.descripcion})

    except:
        respuesta.append({"value": "error base", "key": "error"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def busqueda_cuenta_codigo_exacto(request):
    respuesta = []
    q = request.GET.get("term", "")
    codigo = q.split("-")[0].replace(" ", "")

    try:
        # Cambiar en la base una columna que me indique las cuentas de contabilidad
        cuenta = PlanCuenta.objects.filter(status=1).exclude(tipo__alias=3).exclude(tipo__alias=4).exclude(tipo__alias=7).exclude(tipo__alias=13).exclude(tipo__alias=14).exclude(tipo__alias=16).get(nivel=5, codigo__icontains=codigo)
        respuesta.append({"value": cuenta.codigo, "key": cuenta.codigo+" - "+cuenta.descripcion, "ret": False})
    except:
        respuesta.append({"value": "cuenta no encontrada", "key": "", "ret": False})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_fecha_secuencia_contabilidad(request):
    '''
    Función Ajax que consulta la última secuencia
    según la fecha de registro
    que reciba
    :param request:
    :return:
    '''
    tipo_c = TipoComprobante.objects.get(id=6)
    q = request.POST.get("fecha", "")
    fecha_split = str(q).split("-")
    fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))
    respuesta = []

    try:
        secuencia_t_comp = Seq_Comp_Contable.objects.get(anio=fecha.year, mes=fecha.strftime("%m"), tipo_comp=tipo_c)
        secuencia = secuencia_t_comp.secuencia
        fecha_actual = secuencia_t_comp.fecha_actual
        respuesta.append({"status": 1, "secuencia": secuencia, "fecha_actual": str(fecha_actual)})
    except Seq_Comp_Contable.DoesNotExist:
        secuencia = 0.0
        fecha_actual = datetime.datetime.now().date()
        respuesta.append({"status": 0, "secuencia": secuencia, "fecha_actual": str(fecha_actual), "descripcion": u"error"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")