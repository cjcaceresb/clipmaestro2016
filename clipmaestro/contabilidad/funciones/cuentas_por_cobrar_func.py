#! /usr/bin/python
# -*- coding: UTF-8-*-


from django.contrib import messages
from django.forms.util import ErrorList
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.forms.formsets import formset_factory
from contabilidad.formularios.CuentasxCobrarForm import *
from contabilidad.models import *
import operator
from librerias.funciones.funciones_modelos import redondeo
from librerias.funciones.funciones_vistas import *
from reportes.vistas.reporte_contabilidad import BuscadorReportes

__author__ = 'Clip Maestro'


class ErrorCobro(Exception):
    def __init__(self, valor):
        self.valor = valor
    def __str__(self):
        return "Error " + unicode(self.valor)


def busqueda_cobros(buscador):
    """
    Funcion de buscador de cobros
    :param buscador:
    :return:
    """
    predicates = []
    if buscador.getFechaIni() is not None:
        predicates.append(('fecha_reg__gte', buscador.getFechaIni()))

    predicates.append(('fecha_reg__lte', buscador.getFechaFinal()))

    predicates.append(('num_comp__icontains', buscador.getNumComp()))

    predicates.append(('cxc_cobro__cuentas_x_cobrar__num_doc__istartswith', buscador.getNumDoc()))
    predicates.append(('monto__range', (buscador.getValorIni(), buscador.getValorFinal())))

    if buscador.getEstado() != "":
        predicates.append(('status', buscador.getEstado()))
    if buscador.getCliente() is not None:
        predicates.append(('cliente', buscador.getCliente()))

    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]
    entries = Cobro.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).order_by("-fecha_reg", "-num_comp").distinct()
    return entries

@csrf_exempt
def cargar_documentos_cobrar(request):
    """
    Funcion que sirve para enviar por ajax
    los documentos a cobrar de un respectivo cliente
    :param request:
    :return:
    """
    id_cliente = request.POST.get("id")
    if id_cliente is not None:
        try:
            cliente = Cliente.objects.get(id=request.POST.get("id", ""))
            cuentas_cobrar = Cuentas_por_Cobrar.objects.filter(status=1, cliente=cliente).exclude(naturaleza=2).extra(where=["monto > cobrado"])
            formset_detalle_docs = formset_factory(DetalleCobro, extra=0)
            l_detalle_doc = []
            if cuentas_cobrar:
                for obj in cuentas_cobrar:
                    plazo = 0
                    fecha_vencimiento = ""
                    if obj.plazo is not None and obj.plazo > 0:
                        plazo = obj.plazo
                        fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)

                    l_detalle_doc.append({"id_cuenta_cobrar": obj.id, "check": False,
                                          "tipo_doc": obj.documento.descripcion_documento,
                                          "numero_doc": obj.num_doc, "fecha_reg": obj.fecha_reg,
                                          "fecha_ven":fecha_vencimiento,
                                          "total": "%.2f" % redondeo(obj.monto, 2),
                                          "saldo": "%.2f" % redondeo((obj.monto - obj.cobrado), 2),
                                          "referencia": obj.get_referencia(), "valor": "0.00"})

            # Se agrega una línea para anticipos
            l_detalle_doc.append({"id_cuenta_cobrar": -1, "check": False, "tipo_doc": "Anticipo",
                                  "numero_doc": "", "fecha_reg": "", "fecha_ven": "", "total": "", "saldo": "",
                                  "referencia": "", "valor": "0.00"})
            detalle_docs = formset_detalle_docs(initial=l_detalle_doc)

            return render_to_response('Cuentas_Cobrar/Clientes/tabla_cuentas_cobrar.html',
                                      {'forms': detalle_docs}, context_instance=RequestContext(request))
        except Cliente.DoesNotExist:
            pass
    else:
        pass


def CalcularTotalesCobro(detalle_docs):
    """
    Calcula los totales del cobro para la vista
    Cobro
    :param detalle_docs:
    :return:
    """
    total_cobrar = 0.0
    total_anticipo = 0.0
    for form in detalle_docs:
        informacion = form.cleaned_data
        if informacion.get("id_cuenta_cobrar") != -1:
            if informacion.get("valor") is not None:
                total_cobrar += float(informacion.get("valor"))
        else:
            if informacion.get("valor") is not None:
                total_anticipo += float(informacion.get("valor"))
    return total_cobrar, total_anticipo


def GuardarCobro(cobro, cabecera, forma_de_pago, detalle_docs, total_cobrar, total_anticipo, asiento, cursor, now, contador, request):
    """
    Guarda el cobro, esto se
    utiliza en la vista de cobro
    :param cobro:
    :param cabecera:
    :param forma_de_pago:
    :param total_cobrar:
    :param total_anticipo:
    :param asiento:
    :param cursor:
    :param now:
    :param contador:
    :param request:
    :return:
    """
    if cabecera.getFecha() > now.date():
        contador += 1
        messages.error(request, u"La fecha de registro es mayor a la fecha actual")
    try:
        cobro.fecha_reg = cabecera.getFecha()
        cliente = Cliente.objects.get(id=cabecera.getCliente())
        cobro.cliente = cliente
        cobro.monto = redondeo(total_cobrar + total_anticipo)

        if cabecera.getObservacion() == "" or cabecera.getObservacion().isspace():
            facturas = ""
            cont_anti = 0.0
            for form in detalle_docs:
                informacion = form.cleaned_data
                if informacion.get("id_cuenta_cobrar") != -1:
                    try:
                        valor = float(informacion.get("valor"))
                    except:
                        valor = 0
                    if valor > 0:
                        cuenta_cobrar = Cuentas_por_Cobrar.objects.get(id=informacion.get("id_cuenta_cobrar"))
                        facturas += cuenta_cobrar.num_doc + " "
                else:
                    try:
                        valor = float(informacion.get("valor"))
                    except:
                        valor = 0
                    if valor > 0:
                        cont_anti += valor

            if facturas != "":
                cobro.concepto = u"Pago de Cliente: " + cliente.razon_social + u" #Doc: " + facturas
            else:
                if cont_anti > 0:
                    cobro.concepto = u"Anticipo de Cliente: " + cliente.razon_social \
                                     + u" Por un valor de $" + str(cont_anti)
                else:
                    cobro.concepto = u"Pago de Cliente: " + cliente.razon_social

        else:
            cobro.concepto = cabecera.getObservacion()

        forma_pago = forma_de_pago.getFormaPago()

        if forma_de_pago.is_valid():
            if forma_pago in ("1", "4"):  # Cheque o transferencia bancaria
                if forma_de_pago.getFormaPago() == "1":
                    cobro.tipo_comprobante = TipoComprobante.objects.get(id=3)      # INGRESO
                    try:
                        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=3)  # Secuencia de Compras
                        fecha_comp = secuencia.fecha_actual
                    except SecuenciaTipoComprobante.DoesNotExist:
                        fecha_comp = now.strftime("%Y-%m-%d")
                else:
                    cobro.tipo_comprobante = TipoComprobante.objects.get(id=23)      # TRANSFERENCIA
                    try:
                        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=23)  # Secuencia de Compras
                        fecha_comp = secuencia.fecha_actual
                    except SecuenciaTipoComprobante.DoesNotExist:
                        fecha_comp = now.strftime("%Y-%m-%d")

                num_comp = get_num_comp(cobro.tipo_comprobante_id, cobro.fecha_reg, True, request)

                if num_comp == -1:
                    contador += 1
                    messages.error(request, u"La fecha de registro ingresada es menor a la última fecha "
                                            u"de registro de cobro por favor verifique "+str(fecha_comp))
                else:
                    cobro.num_comp = num_comp

                #cobro.tipo_comprobante = TipoComprobante.objects.get(id=3)
                try:
                    banco = Banco()
                    plan_cuenta = Cuenta_Banco.objects.get(id=forma_de_pago.getBanco()).plan_cuenta
                    banco.cuenta_banco = Cuenta_Banco.objects.get(id=forma_de_pago.getBanco(), status=1)
                    banco.concepto = cobro.concepto
                    banco.num_referencia = forma_de_pago.getReferencia()
                    banco.fecha_reg = cabecera.getFecha()
                    banco.forma_pago = FormaPago.objects.get(id=forma_de_pago.getFormaPago())
                    banco.naturaleza = 1
                    banco.tipo_comprobante = TipoComprobante.objects.get(id=3)
                    banco.num_comp = cobro.num_comp
                    banco.valor = redondeo(total_cobrar + total_anticipo)
                    banco.fecha_creacion = now
                    banco.usuario_creacion = request.user.username
                    banco.save()
                except (Banco.DoesNotExist, ValueError):
                    contador += 1
                    plan_cuenta = None
                    messages.error(request, u"Por favor seleccione una cuenta bancaria válida")
                    errors = forma_de_pago._errors.setdefault("bancos", ErrorList())
                    errors.append(u"Por favor seleccione una cuenta bancaria válida")

            elif forma_pago in ("2", "3"):  # Pago en Efectivo y Tarjeta de Crédito
                try:
                    secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=10)  # Secuencia de Compras
                    fecha_comp = secuencia.fecha_actual
                except SecuenciaTipoComprobante.DoesNotExist:
                    fecha_comp = now.strftime("%Y-%m-%d")
                cobro.tipo_comprobante = TipoComprobante.objects.get(id=10)     # COBRO
                num_comp = get_num_comp(cobro.tipo_comprobante_id, cobro.fecha_reg, True, request)
                if num_comp == -1:
                    contador += 1
                    messages.error(request, u"La fecha de registro ingresada es menor a la última fecha de registro "
                                            u" de cobro por favor verifique "+str(fecha_comp))
                else:
                    cobro.num_comp = num_comp

                try:
                    plan_cuenta = PlanCuenta.objects.get(id=forma_de_pago.getCajaChica(), status=1)
                except (PlanCuenta.DoesNotExist, ValueError):
                    contador += 1
                    plan_cuenta = None
                    messages.error(request, u"Por favor seleccione una cuenta de caja válida")
                    errors = forma_de_pago._errors.setdefault("caja", ErrorList())
                    errors.append(u"Por favor seleccione una cuenta de caja válida")

            else:
                contador += 1
                plan_cuenta = None
                messages.error(request, u"Opción de pago no válida")

            if contador == 0:
                cobro.forma_pago = FormaPago.objects.get(id=forma_pago)
                cobro.referencia = forma_de_pago.getReferencia()
                cobro.fecha_creacion = now
                cobro.usuario_creacion = request.user.username
                cobro.save()

                ###################################
                #    CABECERA  ASIENTO CONTABLE   #
                ###################################
                nombre_cliente = unicode(cobro.cliente.razon_social[0:15])
                asiento.concepto_comprobante = cobro.concepto
                asiento.fecha_asiento = cabecera.getFecha()
                asiento.tipo_comprobante = cobro.tipo_comprobante
                asiento.numero_comprobante = cobro.num_comp
                asiento.fecha = cobro.fecha_reg

                asiento.fecha_creacion = now
                asiento.fecha_actualizacion = now
                asiento.usuario_creacion = request.user.username
                asiento.usuario_actualizacion = request.user.username
                asiento.save()

                #############################
                #  ASIENTO CONTABLE CLIENTE #
                #############################
                if total_cobrar > 0:  # Podria solo haber hecho un anticipo al cliente
                    detalle_asiento1 = Detalle_Comp_Contable()
                    detalle_asiento1.cabecera_contable = asiento
                    detalle_asiento1.fecha_asiento = asiento.fecha
                    detalle_asiento1.dbcr = "H"
                    detalle_asiento1.plan_cuenta = Cliente.objects.get(id=cabecera.getCliente()).plan_cuenta # Plan de Cuenta Cliente
                    detalle_asiento1.detalle = cobro.concepto
                    detalle_asiento1.valor = redondeo(total_cobrar)

                    detalle_asiento1.fecha_creacion = now
                    detalle_asiento1.usuario_creacion = request.user.username
                    detalle_asiento1.save()

                #############################
                #  ASIENTO CONTABLE COBRO #
                #############################
                detalle_asiento2 = Detalle_Comp_Contable()
                detalle_asiento2.cabecera_contable = asiento
                detalle_asiento2.dbcr = "D"
                detalle_asiento2.fecha_asiento = asiento.fecha
                detalle_asiento2.plan_cuenta = plan_cuenta
                detalle_asiento2.valor = redondeo(cobro.monto)
                detalle_asiento2.detalle = cobro.concepto

                detalle_asiento2.fecha_creacion = now
                detalle_asiento2.usuario_creacion = request.user.username
                detalle_asiento2.save()

                if total_anticipo > 0:
                    detalle_asiento = Detalle_Comp_Contable()
                    cuenta_cobrar_anticipo = Cuentas_por_Cobrar()

                    detalle_asiento.dbcr = "H"
                    detalle_asiento.valor = redondeo(total_anticipo)
                    detalle_asiento.fecha_asiento = asiento.fecha
                    detalle_asiento.detalle = "Anticipo del Cliente: " + cobro.cliente.razon_social[0:15] \
                                              + " por la cantidad de: " + str(total_anticipo)
                    try:
                        detalle_asiento.plan_cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=13))  # Anticipo de cliente
                        detalle_asiento.cabecera_contable = asiento

                        detalle_asiento.usuario_creacion = request.user.username
                        detalle_asiento.fecha_creacion = now
                        detalle_asiento.save()

                        cuenta_cobrar_anticipo.monto = redondeo(total_anticipo)
                        cuenta_cobrar_anticipo.cliente = cobro.cliente
                        cuenta_cobrar_anticipo.documento = Documento.objects.get(codigo_documento="AN", status=1)
                        cuenta_cobrar_anticipo.cobrado = 0
                        cuenta_cobrar_anticipo.fecha_reg = cobro.fecha_reg
                        cuenta_cobrar_anticipo.fecha_emi = cobro.fecha_reg
                        cuenta_cobrar_anticipo.num_doc = cobro.num_comp
                        cuenta_cobrar_anticipo.naturaleza = 2
                        cuenta_cobrar_anticipo.usuario_creacion = request.user.username
                        cuenta_cobrar_anticipo.fecha_creacion = now
                        cuenta_cobrar_anticipo.save()

                        cxp_cobro = CXC_cobro()
                        cxp_cobro.monto = redondeo(total_anticipo)
                        cxp_cobro.anticipo = redondeo(total_anticipo)
                        cxp_cobro.cuentas_x_cobrar = cuenta_cobrar_anticipo
                        cxp_cobro.cobro = cobro
                        cxp_cobro.naturaleza = 1
                        cxp_cobro.concepto = "Anticipo del Cliente: " + cobro.cliente.razon_social[0:15] + \
                                             " por la cantidad de: " + str(total_anticipo)
                        cxp_cobro.plan_cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=13))  # Anticipo de clientes

                        cxp_cobro.fecha_creacion = now
                        cxp_cobro.usuario_creacion = request.user.username
                        cxp_cobro.save()
                    except PlanCuenta.DoesNotExist:
                        messages.error(request, u"No existe registrada la cuenta Anticipo de Clientes, por favor verifique, si "
                                                u"tiene alguna duda comuníquese con el administrador del sistema")
                        contador += 1

                ########################################
                #  DETALLE DE LOS DOCUMENTOS COBRADOS  #
                ########################################
                for form in detalle_docs:
                    informacion = form.cleaned_data
                    try:
                        if informacion.get("valor") is not None:
                            valor = float(informacion.get("valor"))
                        else:
                            valor = 0
                    except:
                        contador += 1
                        valor = 0.0
                        messages.error(request, u"Algunos datos recibidos son no válidos, por favor ingrese valores válidos en la transacción.")

                    if informacion.get("id_cuenta_cobrar") != -1:
                        cuenta_cobrar = Cuentas_por_Cobrar.objects.get(id=informacion.get("id_cuenta_cobrar"))
                        if valor > 0:
                            if cuenta_cobrar.status != 1:
                                contador += 1
                                messages.error(request, u"La cuenta por pagar que quiso utilizar estaba anulada al momento de realizar el pago, "
                                                            u"por favor revise si se anuló la venta.")
                                errors = form._errors.setdefault("valor", ErrorList())
                                errors.append(u"La cuenta por pagar que quiso utilizar estaba anulada al momento de realizar el pago, "
                                                            u"por favor revise si se anuló la venta.")
                            elif redondeo((cuenta_cobrar.cobrado + valor)) > redondeo(cuenta_cobrar.monto):
                                contador += 1
                                messages.error(request, u"El valor del pago del documento: "+ cuenta_cobrar.num_doc+ u", excede su monto")
                                errors = form._errors.setdefault("valor", ErrorList())
                                errors.append(u"La cuenta por pagar que quiso utilizar estaba anulada al momento de realizar el pago, "
                                                            u"por favor revise si se anuló la venta.")
                            elif cuenta_cobrar.fecha_reg > cabecera.getFecha():
                                contador += 1
                                messages.error(request, u"La fecha del documento a cobrar es mayor a la fecha "
                                                        u"de registro del cobro, por favor corríja")
                                errors1 = form._errors.setdefault("fecha_reg", ErrorList())
                                errors1.append(u"Error")

                                errors1 = cabecera._errors.setdefault("fecha", ErrorList())
                                errors1.append(u"Error")
                            else:
                                cuenta_cobrar.cobrado = redondeo(cuenta_cobrar.cobrado + valor)

                            cuenta_cobrar.usuario_actualizacion = request.user.username
                            cuenta_cobrar.fecha_actualizacion = now
                            cuenta_cobrar.save()

                            cxp_cobro = CXC_cobro()
                            cxp_cobro.monto = redondeo(valor)
                            cxp_cobro.concepto = "Cobro a "+cuenta_cobrar.documento.descripcion_documento \
                                                 + " #" + cuenta_cobrar.num_doc + " por: $" + str(valor)
                            cxp_cobro.cuentas_x_cobrar = cuenta_cobrar
                            cxp_cobro.cobro = cobro
                            cxp_cobro.plan_cuenta = plan_cuenta
                            cxp_cobro.naturaleza = 2

                            cxp_cobro.fecha_creacion = now
                            cxp_cobro.usuario_creacion = request.user.username
                            cxp_cobro.save()

        return contador

    except Cliente.DoesNotExist:
        messages.error(request, u"El cliente al cual ha hecho referencia se encuentra inactivo o no"
                                    u"se tiene registros de ese cliente, si tiene alguna duda, comuníquese con "
                                    u"el administrador del sistema")
        return HttpResponseRedirect(reverse("lista_cobro"))


###############################################################################
###########################   CRUCE DE DOCUMENTOS    ##########################
###############################################################################
def GuardarCobroCruceDocs(cabecera, cobro, cliente, num_comp, total_cobrar, asiento, now, request):
    """
    Guarda el cobro en cruce de documentos
    :param cabecera:
    :param cobro:
    :param cliente:
    :param total_rows:
    :param total_cobrar:
    :param asiento:
    :param now:
    :param request:
    :return:
    """
    cobro.fecha_pago = cabecera.getFecha()
    cobro.cliente = cliente
    cobro.monto = redondeo(total_cobrar)
    cobro.concepto = cabecera.getObservacion()

    if cabecera.getFormaPago() == "1":
        tipo_comprobante = TipoComprobante.objects.get(id=17)  # Comprobante tipo Cruce de documentos Cobros
        cobro.forma_pago = FormaPago.objects.get(id=5)  # Forma de pago Cruce de documentos
    else:
        tipo_comprobante = TipoComprobante.objects.get(id=18)  # Comprobante tipo Cruce de cuentas Cobros
        cobro.forma_pago = FormaPago.objects.get(id=6)  # Forma de pago Cruce de cuentas

    cobro.tipo_comprobante = tipo_comprobante
    cobro.num_comp = num_comp
    cobro.fecha_reg = cabecera.getFecha()
    cobro.fecha_creacion = now
    cobro.usuario_creacion = request.user.username
    cobro.save()

    ###########################
    #    ASIENTO CONTABLE     #
    ###########################
    nombre_cliente = unicode(cliente.razon_social[0:15])
    if cabecera.getObservacion() != "":
        asiento.concepto_comprobante = cabecera.getObservacion()
    else:
        if cabecera.getFormaPago() == "1":
            concepto = u"Cruce de Doc.: " + nombre_cliente + u" # identificación " + cliente.ruc[0:15] + u". La cantidad de " + u"$"+str(total_cobrar)
        else:
            concepto = u"Cruce de Cta.: " + nombre_cliente + u" # identificación " + cliente.ruc[0:15] + u". La cantidad de " + u"$"+str(total_cobrar)

        asiento.concepto_comprobante = concepto

    asiento.tipo_comprobante = tipo_comprobante
    asiento.fecha = cabecera.getFecha()
    asiento.numero_comprobante = num_comp

    asiento.fecha_creacion = now
    asiento.usuario_creacion = request.user.username
    asiento.save()

    ##################################
    #   Asiento de Cta del Clientes  #
    ##################################
    if cabecera.getFormaPago() == "1":  # Solo en cruce de documentos se hace este asiento
        detalle_asiento1 = Detalle_Comp_Contable()
        detalle_asiento1.cabecera_contable = asiento
        detalle_asiento1.dbcr = "H"
        detalle_asiento1.fecha_asiento = asiento.fecha
        detalle_asiento1.plan_cuenta = cliente.plan_cuenta  # Plan de Cuenta Cliente
        detalle_asiento1.detalle = detalle_asiento1.plan_cuenta.descripcion
        detalle_asiento1.valor = redondeo(total_cobrar)

        detalle_asiento1.fecha_creacion = now
        detalle_asiento1.usuario_creacion = request.user.username
        detalle_asiento1.save()


def GuardarCuentasCobrar(cuenta_cobrar, cobro, total_cobrar, now, contador, request):
    """
    Guarda la relación del cobro con la factura del cliente,
    actualizando la tabla de cuentas por cobrar y crea la relación entre
    cobro y cuenta por cobrar mediante la tabla "cxc_cobro"
    :param cuenta_cobrar:
    :param cobro:
    :param total_cobrar:
    :param now:
    :param contador:
    :param request:
    :return:
    """
    #########################################
    #   Validar que el valor que intenten   #
    #   enviar no supere el valor adeudado  #
    #########################################
    if redondeo(total_cobrar) > redondeo(cuenta_cobrar.monto - cuenta_cobrar.cobrado):
        contador += 1
        messages.error(request, u"Error, el valor del pago supera el monto de la deuda. El total a cruzar es: " + str(total_cobrar) +
                       u" y lo adeudado es: "+str(cuenta_cobrar.monto - cuenta_cobrar.cobrado))
    else:
        cuenta_cobrar.cobrado = cuenta_cobrar.cobrado + total_cobrar

    cuenta_cobrar.fecha_actualizacion = now
    cuenta_cobrar.usuario_actualizacion = request.user.username
    cuenta_cobrar.save()

    cxc_cobro = CXC_cobro()
    cxc_cobro.cobro = cobro
    cxc_cobro.cuentas_x_cobrar = cuenta_cobrar
    cxc_cobro.monto = total_cobrar
    cxc_cobro.concepto = u"Cruce de doc. en cobro a cliente: " + cobro.cliente.razon_social[0:15] + u". Por una " \
                                                                                                    u"cantidad de " + str(total_cobrar)
    cxc_cobro.plan_cuenta = cobro.cliente.plan_cuenta
    cxc_cobro.usuario_creacion = request.user.username
    cxc_cobro.fecha_creacion = now
    cxc_cobro.save()

    return contador


def GuardarDetalleDocCruzados(detalle_doc_cobrar, cobro, cliente, asiento,  contador, now, request):
    """
    Guarda los documentos que se utilizaron para el cruce de documentos del cliente,
    se actualiza la tabla cuentas_por_cobrar y se genera la respectiva relación con el cobro
    mediante la tabla "cxc_cobro"
    :param detalle_doc_cobrar:
    :param cobro:
    :param cliente:
    :param asiento:
    :param contador:
    :param now:
    :param request:
    :return:
    """

    total_cta_anticipo = 0.0  # Cuando es con anticipo
    total_cta_cliente = 0.0  # Cuando es una nota de Crédito

    for form in detalle_doc_cobrar:
        informacion = form.cleaned_data
        cxc_cobro = CXC_cobro()
        valor = redondeo(float(informacion.get("valor", 0)), 2)
        if valor > 0:
            id = int(informacion.get("id_cuenta_pagar"))
            cta_por_cobrar = Cuentas_por_Cobrar.objects.get(id=id)

            if cta_por_cobrar.documento.codigo_documento == "04":
                nota_credito = True
            else:  # Anticipo
                nota_credito = False

            if nota_credito:  # Nota de Crédito
                if cta_por_cobrar.status != 1:
                    raise ErrorCobro(u"la nota de Crédito que quiso utilizar estaba anulada al momento de realizar la transacción, "
                                        u"por favor intente el cruce nuevamente.")
                if valor > redondeo((cta_por_cobrar.monto - cta_por_cobrar.cobrado), 2):
                    messages.error(request, u"Error, datos enviados no válidos, el valor del anticipo"
                                        u" excede su saldo")
                    contador += 1
                    errors = form._errors.setdefault("valor", ErrorList())
                    errors.append(u"Error, datos enviados no válidos, el valor del anticipo")
                else:
                    cta_por_cobrar.cobrado += valor
                    total_cta_cliente += valor

                cta_por_cobrar.fecha_actualizacion = now
                cta_por_cobrar.usuario_actualizacion = request.user.username
                cta_por_cobrar.save()

                cxc_cobro.cuentas_x_cobrar = cta_por_cobrar
                cxc_cobro.cobro = cobro
                cxc_cobro.concepto = u"Cruce de documento con Nota de crédito por un valor de" + str(valor)
                cxc_cobro.monto = valor
                cxc_cobro.plan_cuenta = cliente.plan_cuenta
                cxc_cobro.fecha_creacion = now
                cxc_cobro.usuario_creacion = request.user.username
                cxc_cobro.status = 1
                cxc_cobro.save()

            else:  # Anticipo

                if cta_por_cobrar.status != 1:
                    messages.error(request, u"El Anticipo que quiso utilizar estaba anulado al momento de realizar la transacción, "
                                        u"por favor intente el cruce nuevamente.")
                    contador += 1

                if round(valor, 2) > round(float(float(cta_por_cobrar.monto) - float(cta_por_cobrar.cobrado)), 2):
                    messages.error(request, u"Error, datos enviados no válidos, el valor del anticipo"
                                        u" excede su saldo")
                    contador += 1
                    errors = form._errors.setdefault("valor", ErrorList())
                    errors.append(u"Error, datos enviados no válidos, el valor del anticipo")
                else:
                    cta_por_cobrar.cobrado += valor
                    total_cta_anticipo += valor

                cta_por_cobrar.fecha_actualizacion = now
                cta_por_cobrar.usuario_actualizacion = request.user.username
                cta_por_cobrar.save()

                cxc_cobro.cuentas_x_cobrar = cta_por_cobrar
                cxc_cobro.cobro = cobro
                cxc_cobro.concepto = u"Cruce de documento con Anticipo por un valor de " + str(valor)
                cxc_cobro.monto = valor
                cxc_cobro.plan_cuenta = PlanCuenta.objects.get(tipo=13)  # Tipo de cuenta anticipo de cliente

                cxc_cobro.status = 1
                cxc_cobro.fecha_creacion = now
                cxc_cobro.usuario_creacion = request.user.username
                cxc_cobro.save()

    ######################################
    #         Asiento por Anticipo       #
    ######################################
    if total_cta_anticipo > 0:
        detalle_doc_cruz = Detalle_Comp_Contable()
        detalle_doc_cruz.cabecera_contable = asiento
        detalle_doc_cruz.dbcr = "D"
        detalle_doc_cruz.plan_cuenta = PlanCuenta.objects.get(tipo=13)  # Tipo de cuenta anticipo de cliente
        detalle_doc_cruz.valor = total_cta_anticipo
        detalle_doc_cruz.detalle = u"Cruce de Documentos con Anticipos de Clientes"
        detalle_doc_cruz.fecha_asiento = asiento.fecha

        detalle_doc_cruz.fecha_creacion = now
        detalle_doc_cruz.usuario_creacion = request.user.username
        detalle_doc_cruz.save()

    ######################################
    #    Asiento por Notas de Crédito    #
    ######################################
    if total_cta_cliente > 0:
        detalle_doc_cruz = Detalle_Comp_Contable()
        detalle_doc_cruz.cabecera_contable = asiento
        detalle_doc_cruz.dbcr = "D"
        detalle_doc_cruz.plan_cuenta = cliente.plan_cuenta
        detalle_doc_cruz.valor = total_cta_cliente
        detalle_doc_cruz.fecha_asiento = asiento.fecha
        detalle_doc_cruz.detalle = u"Cruce de Documentos con Notas de crédito Clientes"

        detalle_doc_cruz.fecha_creacion = now
        detalle_doc_cruz.usuario_creacion = request.user.username
        detalle_doc_cruz.save()

    return contador


@csrf_exempt
def cargar_cruce_documentos_cobros(request):
    """
    Carga las facturas del cliente recibido por id ademas de los anticipos recibidos
    por ese cliente, y se lo envía en un formato json
    :param request:
    :return:
    """

    l_det_doc = []  # Lista de detalle de documento
    l_det_cr_doc = []  # Lista de documentos a cruzar

    formset_det_docs = formset_factory(DetalleCobro, extra=0)
    formset_det_docs_cob = formset_factory(DetalleCobroDocCruzarForm, extra=0)

    flag = False  # Bandera para saber si el cliente tiene facturas y ademas doc que pueda cruzar
    id_clt = int(request.POST.get("id"))
    client = Cliente.objects.get(id=id_clt)
    # Documentos que no estan cobrados a los clientes
    cuentas_cobrar = Cuentas_por_Cobrar.objects.exclude(naturaleza=2).filter(status=1, cliente=client).extra(where=["monto > cobrado"])
    # Documentos de Naturaleza Deudora Ej: "Anticipos y Notas de Crédito"
    doc_por_cruzar = Cuentas_por_Cobrar.objects.filter(status=1, cliente=client).filter(naturaleza=2).extra(where=["monto > cobrado"])

    ###############################################
    #  Llenando las formularios de doc por cruzar #
    ###############################################
    if len(cuentas_cobrar) != 0:
        for obj in cuentas_cobrar:
            plazo = 0
            fecha_vencimiento = ""
            if obj.plazo is not None and obj.plazo > 0:
                plazo = obj.plazo
                fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)
            l_det_doc.append({"id_cuenta_cobrar": obj.id, "tipo_doc": obj.documento.descripcion_documento,
                              "fecha_reg": obj.fecha_reg, "numero_doc": obj.num_doc,
                              "fecha_ven": fecha_vencimiento,
                              "total": "%.2f" % redondeo(obj.monto),
                              "saldo": "%.2f" % redondeo(obj.monto - obj.cobrado),
                              "referencia": obj.get_referencia(),
                              "valor": 0.00})

        detalle_docs = formset_det_docs(initial=l_det_doc, prefix="form_detalle_cta_cobrar")
    else:
        flag = True
        detalle_docs = formset_det_docs(prefix="form_detalle_cta_cobrar")

    if len(doc_por_cruzar) > 0:
        for obj in doc_por_cruzar:
            if obj.documento.codigo_documento != "AN":  # Si es una nota de crédito
                plazo = 0
                fecha_vencimiento = ""
                if obj.plazo is not None and obj.plazo > 0:
                    plazo = obj.plazo
                    fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)
                l_det_cr_doc.append({"id_cuenta_pagar": obj.id, "check": False,
                                     "tipo_doc": obj.documento.descripcion_documento, "fecha_reg": obj.fecha_reg,
                                     "fecha_ven": fecha_vencimiento,
                                     "numero_doc": obj.num_doc, "total": "%.2f" % redondeo(obj.monto),
                                     "saldo": "%.2f" % redondeo(obj.monto - obj.cobrado),
                                     "referencia": obj.get_referencia(), "valor": 0.00})
            else:
                plazo = 0
                fecha_vencimiento = ""
                if obj.plazo is not None and obj.plazo > 0:
                    plazo = obj.plazo
                    fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)
                l_det_cr_doc.append({"id_cuenta_pagar": obj.id, "check": False, "tipo_doc": "Anticipo",
                                     "numero_doc": obj.num_doc, "fecha_reg": obj.fecha_reg,
                                     "fecha_ven": fecha_vencimiento,
                                     "total": "%.2f" % redondeo(obj.monto),
                                     "saldo": "%.2f" % redondeo(obj.monto - obj.cobrado),
                                     "referencia": obj.get_referencia(), "valor": 0.00})

        detalle_doc_cobrar = formset_det_docs_cob(initial=l_det_cr_doc, prefix="form_detalle_cruce_doc")
    else:
        flag = True
        detalle_doc_cobrar = formset_det_docs_cob(prefix="form_detalle_cruce_doc")

    if flag:
        messages.success(request, u"No se encuentra documento alguno que pueda ser cruzados, o no existen cuentas "
                                u"por cobrar a ese cliente, intente con cruce de cuentas")

    return render_to_response('Cuentas_Cobrar/Clientes/tabla_cruce_doc_cta.html',
                              {"proveedor": client,
                               "bandera_doc_cruzar": flag,
                               'detalle_docs': detalle_docs,
                               "detalle_doc_cobrar": detalle_doc_cobrar,
                               }, context_instance=RequestContext(request))

@csrf_exempt
def cargar_cruce_cuentas_cobros(request):
    """
    Carga las facturas del cliente recibido por id ademas de los anticipos recibidos
    por ese cliente, y se lo envía en un formato json
    :param request:
    :return:
    """

    l_det_doc = []  # Lista de detalle de documento

    formset_det_docs = formset_factory(DetalleCobroCruceCta, extra=0)

    flag = False  # Bandera para saber si el cliente tiene facturas y ademas doc que pueda cruzar
    id_clt = int(request.POST.get("id"))
    client = Cliente.objects.get(id=id_clt)
    # Documentos que no estan cobrados a los clientes
    cuentas_cobrar = Cuentas_por_Cobrar.objects.filter(status=1, cliente=client).extra(where=["monto > cobrado"])

    ###############################################
    #  Llenando las formularios de doc por cruzar #
    ###############################################
    if len(cuentas_cobrar) != 0:
        for obj in cuentas_cobrar:
            plazo = 0
            fecha_vencimiento = ""
            if obj.plazo is not None and obj.plazo > 0:
                plazo = obj.plazo
                fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)
            l_det_doc.append({"id_cuenta_cobrar": obj.id, "tipo_doc": obj.documento.descripcion_documento,
                              "numero_doc": obj.num_doc, "fecha_reg": obj.fecha_reg,
                              "fecha_ven":fecha_vencimiento,
                              "total": "%.2f" % redondeo(obj.monto),
                              "saldo": "%.2f" % redondeo(obj.monto - obj.cobrado),
                              "referencia": obj.get_referencia(), "valor": 0.00})
        detalle_docs = formset_det_docs(initial=l_det_doc, prefix="form_detalle_cruce_cuenta_cobro")
    else:
        flag = True
        detalle_docs = formset_det_docs(prefix="form_detalle_cruce_cuenta_cobro")

    if flag:
        messages.success(request, u"No se encuentra documento alguno que pueda ser cruzados, o no existen cuentas "
                                u"por cobrar a ese cliente")

    return render_to_response('Cuentas_Cobrar/Clientes/tabla_cruce_cuenta_cobros.html',
                              {"proveedor": client,
                               "bandera_doc_cruzar": flag,
                               'detalle_docs': detalle_docs,
                               }, context_instance=RequestContext(request))


def busqueda_cuentas_cobrar_ini(buscador):
    """
    Funcion para la búsqueda del
    modulo pagos o cuentas por pagar
    :param buscador:
    :return:
    """
    empresa = get_parametros_empresa()
    predicates = []
    predicates.append(('num_doc__icontains', buscador.get_num_doc()))
    if buscador.get_tipo_doc() != "":
        predicates.append(('documento_id', buscador.get_tipo_doc()))
    if buscador.get_cliente() != "":
        predicates.append(('cliente_id', buscador.get_cliente()))
    predicates.append(('monto__range', (buscador.get_monto_ini(), buscador.get_monto_final())))
    predicates.append(('cobrado__range', (buscador.get_cobrado_ini(), buscador.get_cobrado_final())))
    predicates.append(('plazo__gte', buscador.get_plazo()))

    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]
    entries = Cuentas_por_Cobrar.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).filter(fecha_reg=empresa.fecha_inicio_sistema - timedelta(days=1))

    return entries

