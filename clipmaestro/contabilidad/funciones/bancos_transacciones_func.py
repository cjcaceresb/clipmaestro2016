#! /usr/bin/python
# -*- coding: UTF-8-*-
from datetime import timedelta
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from contabilidad.formularios.ClienteForm import *
from contabilidad.formularios.DireccionesForm import *
from contabilidad.formularios.VentaForm import *
from contabilidad.formularios.BancoForm import *
from django.forms.util import ErrorList
from librerias.funciones.paginacion import *
from librerias.funciones.validacion_rucs import *
from django.core.paginator import *
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from librerias.funciones.funciones_vistas import *
from django import template
import operator
register = template.Library()

__author__ = 'Roberto'

def GuardarCuentaBanco(formulario, banco_obj, plan_cuenta,  now, request):
    '''
    Function GuardarCuentaBanco(formulario, banco, now , request)
    Función que me permite guardar una cuenta de banco y asociarla al plan de cuenta
    :param formulario:
    :param banco_obj:
    :param plan_cuenta:
    :param now:
    :param request:
    :return:
    '''
    # 1.-) Si existe la cuenta banco está registrado previamente en plan cuenta (Contabilidad)
    # 2.-) Si no existe nada registrado en plan cuenta (Contabilidad) y Cuenta Banco
    contador = 0.0
    banco_obj.numero_cuenta = formulario.getNumeroCuenta()
    banco_obj.tipo_cuenta_banco = Tipo_Cuenta_Banco.objects.get(id=formulario.getTipo())

    if formulario.getFechaUltimoCierre() != "":
        banco_obj.fecha_ultimo_cierre = formulario.getFechaUltimoCierre()

    if formulario.getFechaUltimoConciliacion() != "":
        banco_obj.fecha_ultimo_conciliacion = formulario.getFechaUltimoConciliacion()

    if banco_obj.tipo_cuenta_banco.id == 2:         # Cuenta Corriente
        banco_obj.secuencia_cheque = formulario.getSecuenciaCheque()
        if Cuenta_Banco.objects.exclude(status=0).filter(Q(secuencia_cheque=banco_obj.secuencia_cheque), Q(numero_cuenta=banco_obj.numero_cuenta), Q(descripcion=banco_obj.descripcion)).exists():
            messages.error(request, u"Número de Secuencia ya ingresado, por favor verifique.")
            contador += 1
    else:                                           # Cuenta Ahorro
        if Cuenta_Banco.objects.exclude(status=0).filter(Q(descripcion=banco_obj.descripcion), Q(numero_cuenta=banco_obj.numero_cuenta)).exists():
            messages.error(request, u"Número de Cuenta ya ingresado, por favor verifique.")
            banco_obj.secuencia_cheque = ""
            contador += 1

    if formulario.getCuentas() is not None:
        banco_obj.plan_cuenta = formulario.getCuentas()
        banco_obj.descripcion = banco_obj.plan_cuenta.descripcion

    else:

        try:
            padre_cuenta = PlanCuenta.objects.get(tipo_id=1, nivel=4)   # Cuenta tipo Banco
            plan_cuenta.clase_cuenta = "M"
            plan_cuenta.tipo = padre_cuenta.tipo
            plan_cuenta.descripcion = formulario.getDescripcion()
            plan_cuenta.naturaleza = "D"
            plan_cuenta.nivel = padre_cuenta.nivel+1
            plan_cuenta.id_cta_grupo = padre_cuenta
            plan_cuenta.codigo = plan_cuenta.ObtenerCodigo()
            plan_cuenta.fecha_creacion = now
            plan_cuenta.usuario_creacion = request.user.username
            plan_cuenta.status = 1
            plan_cuenta.save()
        except PlanCuenta.DoesNotExist:
            contador += 1
            messages.error(request, u"Error la cuenta a ingresar en el sistema no tiene registrado "
                                    u" un tipo en el plan de cuenta")

        banco_obj.plan_cuenta = plan_cuenta
        banco_obj.descripcion = plan_cuenta.descripcion

    if contador == 0.0:
        banco_obj.usuario_creacion = request.user.username
        banco_obj.fecha_creacion = now
        banco_obj.save()

    return contador

########################################################################################################################
#                                              TRANSACCIONES DE BANCO                                                  #
########################################################################################################################
def instancia_cabecera_transaccion_banco(banco):
    '''
    Función que instancia el formulario de la cabecera
    de la transacción banco
    :return:
    '''
    fecha_cheque = ""
    tipo_mov = 0

    if banco.tipo_comprobante is not None:
        if banco.tipo_comprobante_id == 3:    # Ingreso
            tipo_mov = 1
        elif banco.tipo_comprobante.id == 4:  # Egreso
            tipo_mov = 2
        elif banco.tipo_comprobante.id == 23:  # Transferencia
            tipo_mov = 3
        elif banco.tipo_comprobante.id == 24:  # N/C
            tipo_mov = 4
        elif banco.tipo_comprobante.id == 25:  # N/D
            tipo_mov = 5

    if tipo_mov == 2:
        if banco.fecha_cheque is not None:
            fecha_cheque = banco.fecha_cheque.strftime("%Y-%m-%d")
        else:
            fecha_cheque = ""

    return {"tipo_movimiento": tipo_mov, "fecha": banco.fecha_reg.strftime("%Y-%m-%d"),
             "fecha_cheque": fecha_cheque, "cuenta_bancaria": banco.cuenta_banco.id,
             "paguese": banco.persona, "referencia": banco.concepto, "concepto": banco.concepto}

def instancia_detalle_transaccion_banco(cabecera_comp):
    '''
    Carga el formset del detalle de la transacción de banco
    :param banco:
    :return:
    '''
    det_form = []
    detalle_comprobante = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_comp)

    if detalle_comprobante:
        for obj in detalle_comprobante:
            if obj.centro_costo is not None:
                if obj.dbcr == "D":
                    det_form.append({"cuenta_id": obj.plan_cuenta.id, "concepto": obj.detalle,
                                     "valor": obj.valor, "centro_costo": obj.centro_costo.id, "debe": obj.valor})
                elif obj.dbcr == "H":
                    det_form.append({"cuenta_id": obj.plan_cuenta.id, "concepto": obj.detalle,
                                     "valor": obj.valor, "centro_costo": obj.centro_costo.id, "haber": obj.valor})
            else:
                if obj.dbcr == "D":
                    det_form.append({"cuenta_id": obj.plan_cuenta.id, "concepto": obj.detalle,
                                     "valor": obj.valor, "centro_costo": "", "debe": obj.valor})
                elif obj.dbcr == "H":
                    det_form.append({"cuenta_id": obj.plan_cuenta.id, "concepto": obj.detalle,
                                     "valor": obj.valor, "centro_costo": "", "haber": obj.valor})
    return det_form

def anular_transaccion(id, request):
    '''
    Función que realiza el proceso de Anular la transacción bancaria
    :param id:
    :param request:
    :return contador:
    '''
    banco = Banco.objects.get(id=id)

    cab_comp = Cabecera_Comp_Contable.objects.get(numero_comprobante=banco.num_comp)
    now = datetime.datetime.now()
    contador = 0

    if Cobro.objects.filter(num_comp=banco.num_comp).exists():
        contador += 1
        messages.error(request, u'No se puede anular este ingreso "' + banco.num_comp +
                                u'" desde este módulo ya que se encuentra registrado, '
                                u"en el módulo de Cuentas por Cobrar, por favor si lo desea anular, anulelo desde "
                                u"el módulo de Cuentas por Cobrar")

    elif Pago.objects.filter(num_comp=banco.num_comp).exists():
        contador += 1
        messages.error(request, u'No se puede anular este egreso "' + banco.num_comp +
                                u'" desde este módulo ya que se encuentra registrado, '
                                u"en el módulo de Cuentas por Pagar, por favor si lo desea anular, anulelo desde "
                                u"el módulo de Cuentas por Pagar")

    if banco.tipo_comprobante_id == 23:     # Transferencias Bancarias
        banco_transf = Banco.objects.filter(num_comp=banco.num_comp).exclude(id=id)

        for obj in banco_transf:
            obj.status = 2
            obj.fecha_actualizacion = now
            obj.usuario_actualizacion = request.user.username
            obj.save()


    if contador == 0:
        banco.status = 2
        banco.fecha_actualizacion = now
        banco.usuario_actualizacion =request.user.username
        banco.save()

        cab_comp.status = 2
        cab_comp.fecha_actualizacion = now
        cab_comp.usuario_actualizacion = request.user.username
        cab_comp.save()

        for obj in Detalle_Comp_Contable.objects.filter(cabecera_contable=cab_comp):
            obj.status = 2
            obj.fecha_actualizacion = now
            obj.usuario_actualizacion = request.user.username
            obj.save()

    return contador

def get_valor_banco(detalle_trans):
    """
    Obtiene el valor del débito o crédito del banco
    :param detalle_trans:
    :return: valor, naturaleza
    """
    if detalle_trans.is_valid():
        for form in detalle_trans:
            informacion = form.cleaned_data
            debe = informacion.get("debe", 0)
            haber = informacion.get("haber", 0)
            if debe > 0:
                return debe, 1
            if haber > 0:
                return haber, 2
            return 0

def validar_cuadre_transaccion(detalle_trans):
    """
    Obtiene el valor del débito o crédito del banco
    :param detalle_trans:
    :return: valor, naturaleza
    """
    total_debe = 0
    total_haber = 0

    if detalle_trans.is_valid():
        for form in detalle_trans:
            informacion = form.cleaned_data
            try:
                debe = redondeo(float(informacion.get("debe", 0)), 2)
            except:
                debe = 0
            try:
                haber = redondeo(float(informacion.get("haber", 0)), 2)
            except:
                haber = 0

            if debe > 0:
                total_debe += debe
            if haber > 0:
                total_haber += haber

    if son_iguales(total_debe, total_haber) and (total_debe + total_haber > 0):
        return True
    else:
        return False


def BusquedaTransaccionesBancarias(buscador):
    """
    Función de búsqueda para las transacciones bancarias
    :param buscador:
    :return:
    """
    predicates = []

    if buscador.get_fecha_ini() is not None:
        predicates.append(('fecha_reg__gte', buscador.get_fecha_ini()))

    predicates.append(('fecha_reg__lte', buscador.get_fecha_final()))
    predicates.append(('num_comp__icontains', buscador.get_num_comprobante()))

    if buscador.get_tipo_comp() != "":
        predicates.append(('tipo_comprobante_id', buscador.get_tipo_comp()))
    if buscador.get_cuenta_banco() != "":
        predicates.append(('cuenta_banco_id', buscador.get_cuenta_banco()))

    predicates.append(('concepto__icontains', buscador.get_concepto()))

    if buscador.get_estado() != "":
        predicates.append(('status', buscador.get_estado()))

    q_list = [Q(x) for x in predicates]
    entries = Banco.objects.exclude(status=0).exclude(status=3).filter(reduce(operator.and_, q_list)).distinct()\
        .order_by("-fecha_reg", "-num_comp")

    return entries

def BusquedaCuentaBancaria(buscador):
    '''
     Función Busqueda de Cuentas Banco
    :param buscador:
    :return:
    '''
    predicates = []
    predicates.append(('descripcion__icontains', buscador.getDescripcion()))

    if buscador.getNumeroCuenta() != "":
        predicates.append(('numero_cuenta__icontains', buscador.getNumeroCuenta()))

    if buscador.getTipoCuentaBanco() is not None:
        predicates.append(('tipo_cuenta_banco', buscador.getTipoCuentaBanco()))

    q_list = [Q(x) for x in predicates]
    entries = Cuenta_Banco.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).distinct()
    return entries

def validarFormularioCheque(cabecera, empresa_param, request):
    """
    Función validarFormularioCheque(cabecera,request)
    @ Return contador validador
    Details: Función para validar la cabecera de la transaccion
    """
    contador = 0
    if cabecera.getFechaCheque() is None:
        errors = cabecera._errors.setdefault("fecha_cheque", ErrorList())
        errors.append(u"fecha del cheque es dato obligaorio")
        contador += 1
    if cabecera.getFecha() is None:
        errors = cabecera._errors.setdefault("fecha", ErrorList())
        errors.append(u"Formato de fecha no válido")
        messages.error(request, u"Formato de fecha no válido")
        contador += 1
    if empresa_param.tipo_secuencia_cheque == 1:  # Número de cheque automático
        pass
    else:  # Número de cheque manual
        if cabecera.getNumeroCheque() == "":
            errors = cabecera._errors.setdefault("numero_cheque", ErrorList())
            errors.append(u"Número de cheque es dato obligatorio")
            contador += 1
    if cabecera.getPaguese() == "":
        errors = cabecera._errors.setdefault("paguese", ErrorList())
        errors.append(u"Número de cheque es dato obligatorio")
        contador += 1
    if contador == 0:
        return True
    else:
        return False

def GuardarBanco(banco, empresa_param, comprobante_cab, detalle_trans, cabecera, cursor, contador, tipo, now, request):
    '''
    Función que realiza el insert en las tablas correspondientes al proceso de la transacción bancaria
    :param banco:
    :param empresa_param:
    :param comprobante_cab:
    :param detalle_trans:
    :param cabecera:
    :param cursor:
    :param contador:
    :param tipo:
    :param now:
    :param request:
    :return:
    '''
    try:
        total = 0.0
        if detalle_trans.is_valid():
            for form in detalle_trans:
                informacion = form.cleaned_data
                if informacion.get("valor") is None:
                    errors = form._errors.setdefault("valor", ErrorList())
                    errors.append(u"valor requerido")
                    contador += 1
                else:
                    total += redondeo(float(informacion.get("valor")), 2)
                if informacion.get("cuenta_id") == "":
                    errors = form._errors.setdefault("cuenta_id", ErrorList())
                    errors.append(u"Cuenta contable requerido")
                    contador += 1
        else:
            contador += 1
            messages.error(request, u"Por favor verifique que están ingresados corectamente los datos del "
                                    u"detalle de la transacción")

        banco.cuenta_banco = Cuenta_Banco.objects.get(id=cabecera.getCuentaBanc(), status=1)
        banco.concepto = cabecera.getConcepto()
        banco.fecha_cheque = cabecera.getFechaCheque()
        banco.fecha_reg = cabecera.getFecha()
        banco.forma_pago = FormaPago.objects.get(id=cabecera.getMotivo())

        banco.persona = cabecera.getPaguese()
        banco.valor = total
        #############################################
        #  Se valida la fecha de cierre de la       #
        #               empresa                     #
        #############################################
        cursor.execute("select periodo_valido(%s);", [cabecera.getFecha().strftime("%Y-%m-%d")])
        total_rows = cursor.fetchone()
        if not total_rows[0]:
            messages.error(request, u"La fecha del registro no puede ser menor a la del "
                               u"cierre contable")
            errors = cabecera._errors.setdefault("fecha", ErrorList())
            errors.append(u"La fecha del registro no puede ser menor a la del "
                               u"cierre contable")
            contador += 1

        if cabecera.getFecha() > datetime.datetime.now().date():
            errors = cabecera._errors.setdefault("fecha", ErrorList())
            errors.append(u"La fecha del registro no puede ser mayor a la "
                               u"fecha actual")

            messages.error(request, u"La fecha del registro no puede ser mayor a la "
                               u"fecha actual")
            contador += 1

        if cabecera.getTipoMov() == "1":  # Ingreso
            banco.naturaleza = 1
            tipo_comp = TipoComprobante.objects.get(id=3)

        else:  # Egreso
            banco.naturaleza = 2
            tipo_comp = TipoComprobante.objects.get(id=4)

            if empresa_param.tipo_secuencia_cheque == 1:  # Automático
                banco.num_cheque = get_next_cheque(banco.cuenta_banco, request)
            else:  # Manual
                banco.num_cheque = cabecera.getNumeroCheque()

                if FormaPago.objects.get(id=cabecera.getMotivo()).id == 1:
                    if cabecera.getNumeroCheque() != "":
                        if banco.cheque_exist(cabecera.getNumeroCheque()):
                            errors = cabecera._errors.setdefault("numero_cheque", ErrorList())
                            errors.append(u"")
                            messages.error(request, u"El número de cheque ya se encuentra ingresado en el "
                                                    u"sistema")
                            contador += 1
                    else:

                        errors = cabecera._errors.setdefault("numero_cheque", ErrorList())
                        errors.append(u"")
                        messages.error(request, u"Error: Ingrese el número de cheque")
                        contador += 1

        banco.tipo_comprobante = tipo_comp
        banco.num_comp = get_num_comp(tipo_comp.id, cabecera.getFecha(), False, request)

        banco.usuario_creacion = request.user.username
        banco.fecha_creacion = now

        if tipo != 1:
            banco.usuario_actualizacion = request.user.username
            banco.fecha_actualizacion = now

        banco.save()

        comprobante_cab.fecha = cabecera.getFecha()
        comprobante_cab.numero_comprobante = banco.num_comp
        comprobante_cab.concepto_comprobante = cabecera.getConcepto()
        comprobante_cab.tipo_comprobante = tipo_comp

        comprobante_cab.fecha_creacion = now
        comprobante_cab.usuario_creacion = request.user.username

        if tipo != 1:
            comprobante_cab.fecha_actualizacion = now
            comprobante_cab.usuario_actualizacion = request.user.username

        comprobante_cab.save()

        detalle_cont_banco = Detalle_Comp_Contable()
        detalle_cont_banco.valor = total
        detalle_cont_banco.cabecera_contable = comprobante_cab
        detalle_cont_banco.fecha_asiento = comprobante_cab.fecha

        if cabecera.getTipoMov() == "1":
            detalle_cont_banco.dbcr = "D"
            detalle_cont_banco.detalle = u"Ingreso bancario por " + FormaPago.objects.get(id=cabecera.getMotivo()).descripcion
        else:
            detalle_cont_banco.dbcr = "H"
            detalle_cont_banco.detalle = u"Egreso bancario por " + FormaPago.objects.get(id=cabecera.getMotivo()).descripcion
        detalle_cont_banco.plan_cuenta = Cuenta_Banco.objects.get(id=cabecera.getCuentaBanc(), status=1).plan_cuenta

        detalle_cont_banco.usuario_creacion = request.user.username
        detalle_cont_banco.fecha_creacion = now
        if tipo != 1:
            detalle_cont_banco.usuario_actualizacion = request.user.username
            detalle_cont_banco.fecha_actualizacion = now

        detalle_cont_banco.save()

        if detalle_trans.is_valid():
            for form in detalle_trans:
                informacion = form.cleaned_data
                detalle_asiento = Detalle_Comp_Contable()
                try:
                    detalle_asiento.centro_costo = Centro_Costo.objects.get(id=informacion.get("centro_costo"), status=1)
                except:
                    pass
                detalle_asiento.cabecera_contable = comprobante_cab
                if cabecera.getTipoMov() == "1":
                    detalle_asiento.dbcr = "H"
                else:
                    detalle_asiento.dbcr = "D"

                if informacion.get("concepto") == "":
                    detalle_asiento.detalle = banco.concepto
                else:
                    detalle_asiento.detalle = informacion.get("concepto")

                detalle_asiento.valor = informacion.get("valor")
                detalle_asiento.fecha_asiento = comprobante_cab.fecha

                try:
                    detalle_asiento.plan_cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta_id"), status=1, nivel=5)
                except PlanCuenta.DoesNotExist:
                    contador += 1
                    messages.error(request, u"Por favor seleccione una cuenta")
                    errors = form._errors.setdefault("cuenta_id", ErrorList())
                    errors.append(u"El centro de costo que ingresó no es válido")

                detalle_asiento.fecha_creacion = now
                detalle_asiento.usuario_creacion = request.user.username

                if tipo != 1:
                    detalle_asiento.fecha_actualizacion = now
                    detalle_asiento.usuario_actualizacion = request.user.username

                detalle_asiento.save()
        else:
            contador += 1
            transaction.rollback()

    except Cuenta_Banco.DoesNotExist:
        messages.error(request, u"La cuenta de banco que eligió no se encuentra activa o no existe")
        transaction.rollback()
        contador += 1

    return contador

def guardar_cabecera_transaccion(banco, cabecera, total, comprobante_cab, naturaleza, contador, now, request):
    """
    Funcion para guardar la cabecera del comprobante y el banco principal en las transaaciones bancarias
    :param banco:
    :param cabecera:
    :param total:
    :param comprobante_cab:
    :param naturaleza: La naturaleza en transferencias bancarias
    :param now:
    :param request:
    :return:
    """
    banco.cuenta_banco = Cuenta_Banco.objects.get(id=cabecera.getCuentaBanc(), status=1)
    banco.concepto = cabecera.getConcepto()
    banco.valor = total

    if cabecera.getTipoMov() == "1":  # Ingreso
        banco.naturaleza = 1
        tipo_comp = TipoComprobante.objects.get(id=3)
        banco.fecha_reg = cabecera.getFecha()
        banco.persona = cabecera.getPaguese()

    elif cabecera.getTipoMov() == "2":  # Egreso
        banco.naturaleza = 2
        tipo_comp = TipoComprobante.objects.get(id=4)
        if cabecera.es_cheque_provisionado():  # Cheque provisionado
            banco.fecha_cheque = cabecera.getFechaCheque()
            banco.status = 1
        else:
            banco.fecha_reg = cabecera.getFecha()
            banco.num_cheque = get_next_cheque(banco.cuenta_banco, request)
        banco.persona = cabecera.getPaguese()

    elif cabecera.getTipoMov() == "3":  # Transferencia Bancaria
        banco.naturaleza = naturaleza
        tipo_comp = TipoComprobante.objects.get(id=23)  # Tipo de transaccion de Transaccion poner
        banco.persona = cabecera.getPaguese()
        banco.fecha_reg = cabecera.getFecha()

    elif cabecera.getTipoMov() == "4":  # N/C
        banco.naturaleza = naturaleza
        tipo_comp = TipoComprobante.objects.get(id=24)
        banco.fecha_reg = cabecera.getFecha()

    else:  # N/D
        banco.naturaleza = naturaleza
        banco.fecha_reg = cabecera.getFecha()
        tipo_comp = TipoComprobante.objects.get(id=25)

    banco.tipo_comprobante = tipo_comp

    if not cabecera.es_cheque_provisionado():  # Cheque provisionado
        num_comp = get_num_comp(tipo_comp.id, cabecera.getFecha(), True, request)
        if num_comp != -1:
            banco.num_comp = num_comp
        else:
            contador += 1
            now = datetime.datetime.now()
            try:
                secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=tipo_comp.id)  # Secuencia de Compras
                fecha_comp = secuencia.fecha_actual
            except SecuenciaTipoComprobante.DoesNotExist:
                fecha_comp = now.strftime("%Y-%m-%d")
            errors = cabecera._errors.setdefault("fecha", ErrorList())
            errors.append(u"Campo Requerido")
            messages.error(request, u'La fecha de registro del(la) '+unicode(tipo_comp.descripcion)+u' es menor a la fecha de su último registro: '
                                    +str(fecha_comp)+u', por favor verifique')


    banco.usuario_creacion = request.user.username
    banco.fecha_creacion = now
    banco.save()

    # COMPROBANTE
    comprobante_cab.fecha = banco.fecha_reg
    comprobante_cab.numero_comprobante = banco.num_comp

    if cabecera.getConcepto() == "":  # Si no pone nada en concepto se pone el tipo de comprobante y el número
        comprobante_cab.concepto_comprobante = banco.tipo_comprobante.descripcion + u": " + banco.num_comp
    else:
        comprobante_cab.concepto_comprobante = cabecera.getConcepto()

    comprobante_cab.tipo_comprobante = tipo_comp
    comprobante_cab.fecha_creacion = now
    comprobante_cab.usuario_creacion = request.user.username
    comprobante_cab.save()

    if comprobante_cab.fecha > datetime.datetime.now().date():
        errors = cabecera._errors.setdefault("fecha", ErrorList())
        errors.append(u"La fecha del registro no puede ser mayor a la "
                           u"fecha actual")
        messages.error(request, u"La fecha del registro no puede ser mayor a la "
                           u"fecha actual")
        contador += 1

    return contador


def guardar_detalle_transaccion(detalle_trans, comprobante_cab, banco, cabecera, contador, now, request):
    '''
    Función que guarda el detalle de la transacción según corresponda el motivo
    :param detalle_trans:
    :param comprobante_cab:
    :param banco:
    :param cabecera:
    :param contador:
    :param now:
    :param request:
    :return:
    '''
    cont = 0
    if detalle_trans.is_valid():
        for form in detalle_trans:
            informacion = form.cleaned_data
            debe = informacion.get("debe")
            haber = informacion.get("haber")
            detalle_asiento = Detalle_Comp_Contable()
            try:
                detalle_asiento.centro_costo = Centro_Costo.objects.get(id=informacion.get("centro_costo"), status=1)
            except:
                pass

            detalle_asiento.cabecera_contable = comprobante_cab

            if debe > 0:
                detalle_asiento.dbcr = "D"
                detalle_asiento.valor = debe
            else:
                detalle_asiento.dbcr = "H"
                detalle_asiento.valor = haber

            if informacion.get("concepto") == "":
                if cabecera.getTipoMov() == "1":  # Ingreso
                    concepto = banco.tipo_comprobante.descripcion + u": " + \
                               banco.num_comp + u". Recibido de: " + banco.persona
                elif cabecera.getTipoMov() == "2":  # Egreso
                    concepto = banco.tipo_comprobante.descripcion + u": " + \
                               banco.num_comp + u". Con número de cheque: " + \
                               str(banco.num_cheque) + u". Girado a: " + banco.persona
                else:
                    concepto = banco.tipo_comprobante.descripcion + u": " + banco.num_comp

                detalle_asiento.detalle = concepto
            else:
                detalle_asiento.detalle = informacion.get("concepto")

            detalle_asiento.fecha_asiento = comprobante_cab.fecha
            try:
                plan_cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta_id"), status=1, nivel=5)
                detalle_asiento.plan_cuenta = plan_cuenta
            except PlanCuenta.DoesNotExist:
                plan_cuenta = None
                contador += 1
                messages.error(request, u"Por favor seleccione una cuenta")
                errors = form._errors.setdefault("cuenta_id", ErrorList())
                errors.append(u"El centro de costo que ingresó no es válido")

            if cont > 0:  # Para que no vuelva a Registrar la transacción de banco que se registró en la cabecera
                if plan_cuenta:
                    if plan_cuenta.tipo_id == 1:  # Banco

                        if cabecera.getTipoMov() == "3": # INGRESO
                            bancos_transaccion = Banco()
                            bancos_transaccion.concepto = banco.concepto
                            bancos_transaccion.cuenta_banco = Cuenta_Banco.objects.get(plan_cuenta_id=detalle_asiento.plan_cuenta_id)
                            bancos_transaccion.fecha_reg = banco.fecha_reg

                            if detalle_asiento.dbcr == "D":
                                bancos_transaccion.naturaleza = 1
                                bancos_transaccion.valor = debe
                            else:
                                bancos_transaccion.naturaleza = 2
                                bancos_transaccion.valor = haber

                            bancos_transaccion.num_comp = banco.num_comp
                            bancos_transaccion.tipo_comprobante = banco.tipo_comprobante
                            bancos_transaccion.fecha_creacion = now
                            bancos_transaccion.usuario_creacion = request.user.username
                            bancos_transaccion.save()

                        else:
                            messages.error(request, u"No se puede utilizar una cuenta de "
                                                    u"banco en transacciones que no sean "
                                                    u"transferencias bancarias")
                            contador += 1

            detalle_asiento.fecha_creacion = now
            detalle_asiento.usuario_creacion = request.user.username
            detalle_asiento.save()
            cont += 1
    else:
        messages.error(request, u"Detalle de la transacción no válida")
        contador += 1
    return contador


# AJAX
@login_required(login_url="/")
@csrf_exempt
def get_ultimo_cheque_usado(request):
    """
    Retorna el número del último núm de cheque usado
    :return:
    """
    tipo_comp = TipoComprobante.objects.get(status=1, id=4)

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
        secuencia = secuencia_tipo.secuencia
    except SecuenciaTipoComprobante.DoesNotExist:
        secuencia = 0

    try:
        id_cta_banco = request.POST.get("id_cta_banco")
        respuesta = {"status": 1, "num_comp": str(secuencia),
                     "num_cheque": Cuenta_Banco.objects.get(id=id_cta_banco).secuencia_cheque}

        resultado = json.dumps(respuesta)
        transaction.rollback()
    except:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)
        transaction.rollback()
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url="/")
@csrf_exempt
def get_ultimo_comprobante_egreso_usado(request):
    """
    Retorna el número del último núm de cheque usado
    :return:
    """
    tipo_comp = TipoComprobante.objects.get(status=1, id=4)

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
        secuencia = secuencia_tipo.secuencia
        fecha_actual = secuencia_tipo.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        secuencia = 0
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")

    try:
        respuesta = {"status": 1, "num_comp": str(secuencia), "fecha_comp": str(fecha_actual)}
        resultado = json.dumps(respuesta)
        transaction.rollback()
    except:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)
        transaction.rollback()
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_transacciones_conciliar(request):
    '''
    Función de Ajax que obtiene los registro de las transacciones de banco según
    los parámetros que recibe desde el template el id_cta_banco, check_historial, fecha_corte
    :param request:
    :return tabla_html:
    '''
    id_cta = request.POST.get("id_cuenta_banco")
    check_historial = request.POST.get("check_historial")       # Check Historial para obtener todas las transacciones
                                                                # de banco si está activo sin importar si ya se ha
                                                                # conciliado o no, desactivado sólo filtra
                                                                # las transacciones que falta por conciliar

    fecha_corte = request.POST.get("fecha_corte")               # Fecha Corte validación <= fecha_corte
    parametro_hide = 0                                          # Parametro para validar si el query no me retorna
                                                                # elementos ocultar la tabla y muestra mensaje
    form_transacciones = []
    try:
        cuenta_banco = Cuenta_Banco.objects.get(status=1, id=id_cta)
    except:
        cuenta_banco = None

    if float(check_historial) == 1:     # Check Historial - Todas las transacciones
        transacciones_banco = Banco.objects.filter(cuenta_banco=cuenta_banco, status=1, fecha_reg__lte=fecha_corte).order_by("fecha_reg")
    else:                               # Sin Check Historial - Sólo las que faltan de conciliar
        transacciones_banco = Banco.objects.exclude(conciliado=True).filter(cuenta_banco=cuenta_banco,
                                                                            fecha_reg__lte=fecha_corte, status=1).order_by("fecha_reg")

    l_detalle_doc = []
    formset_detalle_docs = formset_factory(FormTransaccionesConciliar, extra=0)

    if cuenta_banco is None:
        parametro_hide = 2

    else:

        if len(transacciones_banco) == 0:
            parametro_hide = 1


        else:

            if transacciones_banco:
                for obj in transacciones_banco:

                    l_detalle_doc.append({"id_banco": obj.id, "conciliado": obj.conciliado,
                                          "check": obj.conciliado,
                                          "tipo_comprobante": obj.tipo_comprobante.descripcion,
                                          "num_comp": obj.num_comp, "fecha_reg": obj.fecha_reg,
                                          "num_cheque": obj.num_cheque,
                                          "beneficiario": obj.persona,
                                          "detalle": obj.concepto,
                                          "valor": obj.valor})

        form_transacciones = formset_detalle_docs(initial=l_detalle_doc)

    if parametro_hide == 1:
        messages.success(request, u"La cuenta seleccionada no posee transacciones para conciliar")
    elif parametro_hide == 2:
        messages.success(request, u"Seleccione una cuenta para realizar el procedimiento de la conciliación")

    return render_to_response('banco/tabla_conciliacion.html',
                                    {'forms': form_transacciones, "parametro_hide": parametro_hide},
                                    context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def get_fecha_transaccion(request):
    '''
    Función Ajax que obtiene la fecha de última transacción
    y la secuencia de cheque última según el escenario tomado (tipo_cuenta=Corriente)
    :param request:
    :return:
    '''

    opt = request.POST.get("id_tipo_comp")
    if opt == "1":  # Ingreso
        tipo_comprobante = TipoComprobante.objects.get(id=3)
    elif opt == "2":  # Egreso
        tipo_comprobante = TipoComprobante.objects.get(id=4)
    elif opt == "3":  # Transacción
        tipo_comprobante = TipoComprobante.objects.get(id=23)
    elif opt == "4":  # N/c
        tipo_comprobante = TipoComprobante.objects.get(id=24)
    else:  # N/D
        tipo_comprobante = TipoComprobante.objects.get(id=25)

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        secuencia = secuencia_tipo.secuencia
        fecha_actual = secuencia_tipo.fecha_actual

    except SecuenciaTipoComprobante.DoesNotExist:
        secuencia = 0
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")

    try:
        respuesta = {"status": 1, "secuencia": str(secuencia), "fecha_actual": str(fecha_actual)}
        resultado = json.dumps(respuesta)

    except:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)
        transaction.rollback()

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_secuencia_cheque_transaccion(request):
    '''
    Función Ajax que obtiene
    la secuencia de cheque última según el escenario tomado (tipo_cuenta=Corriente)
    :param request:
    :return:
    '''
    id_cta_banco = request.POST.get("id_cta_banco")                # Id Cta Banco

    tipo_comprobante = TipoComprobante.objects.get(id=4) # EGRESO

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        secuencia_comp = secuencia_tipo.secuencia

    except SecuenciaTipoComprobante.DoesNotExist:
        secuencia_comp = 0

    try:
        secuencia_cta_corriente = Cuenta_Banco.objects.get(id=id_cta_banco, tipo_cuenta_banco__id=2)  # Cta. Corriente
        secuencia = secuencia_cta_corriente.secuencia_cheque


        respuesta = {"status": 1, "secuencia": str(secuencia), "secuencia_comp": str(secuencia_comp)}
        resultado = json.dumps(respuesta)

    except Cuenta_Banco.DoesNotExist:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)
        transaction.rollback()

    return HttpResponse(resultado, mimetype='application/json')

def redireccionar_transaccion_bancaria(request, banco):
    """
    Función que redirecciona dependiendo de la
    acción que desee el usuario sobre la transacción bancaria
    :return:
    """
    if request.GET.get("grabar_agregar", "") == "1":
        full_redirect_url = reverse("transacciones_bancarias")
    elif request.GET.get("grabar_imprimir", "") == "1":
        redirect_url = reverse("transacciones_bancarias")
        extra_params = '?imp=%s' % banco.id
        full_redirect_url = '%s%s' % (redirect_url, extra_params)
    else:
        full_redirect_url = reverse("lista_transaccion_banco")

    return full_redirect_url
