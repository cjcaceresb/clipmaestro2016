#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.core.paginator import *
from contabilidad.models import *
from contabilidad.formularios.ClienteForm import *
from contabilidad.formularios.DireccionesForm import *
from contabilidad.formularios.ContactosForm import *
from sys import modules
from django.forms.formsets import formset_factory
from sys import path
import datetime
from django.forms.util import ErrorList
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
import time
from librerias.funciones.paginacion import *
from django.http import HttpResponse
import ho.pisa as pisa
import cStringIO as StringIO
import cgi
from librerias.funciones.validacion_rucs import *
from django.template.loader import render_to_string
from django.db import IntegrityError, transaction
import json
from django.core.validators import validate_email
__author__ = 'Roberto'

class ErrorClientes(Exception):
    def __init__(self, valor):
        self.valor = valor
    def __str__(self):
        return "Error " + str(self.valor)


def instancia_form_basica_cliente(cliente):
    '''
    Función que instancia la información básica del formulario cliente
    :param cliente:
    :return:
    '''
    pago = 0
    actividad = 0
    ciudad = 0

    if cliente.forma_pago is not None:
        pago = cliente.forma_pago.id
    if cliente.actividad is not None:
        actividad = cliente.actividad.id
    if cliente.ciudad is not None:
        ciudad = cliente.ciudad.id

    if cliente.email != "" or cliente.email is not None:
        email = cliente.email
    else:
        email = ""

    return {"nombre": cliente.nombre, "razon_social": cliente.razon_social, "Num_Identificacion": cliente.ruc,
            "telefono": cliente.telefono, "direccion": cliente.direccion, "forma_pago": pago,
            "monto_credito": cliente.monto_credito, "ciudad": ciudad, "email": email, "actividad": actividad,
            "tipo_cliente": cliente.tipo_cliente.id, "tipo_identificacion": cliente.tipo_identificacion.codigo,
            "grupo": cliente.plan_cuenta.id, "plazo": cliente.plazo, "margen_utilidad": cliente.margen_utilidad}

def instancia_formset_direcciones():
    '''

    :return:
    '''

"""
function EditarClientes(formulario, cliente, now , request)
@ Return contador Validador
Details: Función para Editar cliente
"""
def EditarCliente(formulario, nuevo, now, request):
    contador_editar = 0
    nuevo.tipo_identificacion = Identificacion.objects.get(codigo=formulario.getTipoIdentificacion())
    nuevo.ruc = formulario.getRuc()
    nuevo.nombre = formulario.getNombres()
    nuevo.razon_social = formulario.getRazonSocial()
    nuevo.plan_cuenta = PlanCuenta.objects.get(id=formulario.getGrupoClientes())

    if formulario.getActividad() != "":
        nuevo.actividad = Sector.objects.filter(status=1).get(id=formulario.getActividad())

    if Cliente.objects.exclude(status=0).filter(Q(ruc=nuevo.ruc), ~Q(id=nuevo.id)).exists():
        contador_editar += 1
        errors = formulario._errors.setdefault("ruc", ErrorList())
        errors.append(u"El Nº de Identificación ya ha sido ingresado.")
        messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación ya ha sido ingresado.")


    else:

        if nuevo.tipo_identificacion.codigo == "P":
            nuevo.tipo_cliente = TipoPersona.objects.get(id=formulario.getTipoCliente())
            contador_editar = 0

        elif nuevo.tipo_identificacion.codigo == "R":
            if len(nuevo.ruc) == 13:
                if isEmpresaPublica(nuevo.ruc) or isPersonaJuridica(nuevo.ruc):
                    nuevo.tipo_cliente = TipoPersona.objects.get(id=2)
                elif isPersonaNatural(nuevo.ruc):
                    nuevo.tipo_cliente = TipoPersona.objects.get(id=1)
                else:
                    contador_editar += 1
                    errors = formulario._errors.setdefault("ruc", ErrorList())
                    errors.append(u"El Nº de Identificación es incorrecto.")
                    messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación es incorrecto.")
            else:
                contador_editar += 1
                errors = formulario._errors.setdefault("ruc", ErrorList())
                errors.append(u"El Nº de Identificación es incorrecto.")
                messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación es incorrecto.")

        elif nuevo.tipo_identificacion.codigo == "C":
            if isCedula(nuevo.ruc) and len(nuevo.ruc) == 10:
                nuevo.tipo_cliente = TipoPersona.objects.get(id=1)
            else:
                contador_editar += 1
                errors = formulario._errors.setdefault("ruc", ErrorList())
                errors.append(u"El Nº de Identificación es incorrecto.")
                messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación es incorrecto.")

    if formulario.getFormaPago() != "":
        nuevo.forma_pago = TipoPago.objects.filter(status=1).get(id=formulario.getFormaPago()) # TipoPago

        if nuevo.forma_pago.id == 2:
            if formulario.getPlazo() == "" and formulario.getMontoCredito() == "":
                contador_editar += 1
                errors = formulario._errors.setdefault("monto_credito", ErrorList())
                errors.append(u"Ingrese un monto crédito.")
                errors = formulario._errors.setdefault("plazo", ErrorList())
                errors.append(u"Ingrese un plazo.")
                messages.error(request, u"Error INFORMACIÓN COMERCIAL: Ingrese un monto de crédito y plazo")

            if formulario.getPlazo() == "" and formulario.getMontoCredito() != "":
                nuevo.monto_credito = float(formulario.getMontoCredito())

                contador_editar += 1
                errors = formulario._errors.setdefault("plazo", ErrorList())
                errors.append(u"Ingrese un plazo.")
                messages.error(request, u"Error INFORMACIÓN COMERCIAL: Ingrese un plazo")

            if formulario.getPlazo() != "" and formulario.getMontoCredito() == "":
                nuevo.plazo = formulario.getPlazo()

                contador_editar += 1
                errors = formulario._errors.setdefault("monto_credito", ErrorList())
                errors.append(u"Ingrese un monto de crédito.")
                messages.error(request, u"Error INFORMACIÓN COMERCIAL: Ingrese un monto de crédito")

            if formulario.getPlazo() != "" and formulario.getMontoCredito() != "":
                nuevo.plazo = formulario.getPlazo()
                nuevo.monto_credito = float(formulario.getMontoCredito())

        else:
                nuevo.monto_credito = 0.0
                nuevo.plazo = ""
    else:
        nuevo.monto_credito = 0.0
        nuevo.plazo = ""

    if formulario.getMargenUtilidad() != "":
        nuevo.margen_utilidad = formulario.getMargenUtilidad()

    if formulario.get_email() != "":
        nuevo.email = formulario.get_email()

    if contador_editar == 0:
        nuevo.fecha_actualizacion = now
        nuevo.usuario_actualizacion = request.user.username
        nuevo.save()

    return contador_editar

"""
function GuardarClientes(formulario, cliente, now , request)
@ Return None
Details: Función para guardar cliente
"""
def GuardarCliente(formulario_cliente, cliente_obj, now, request):
    contador = 0

    cliente_obj.tipo_identificacion = Identificacion.objects.get(codigo=formulario_cliente.getTipoIdentificacion())
    cliente_obj.ruc = formulario_cliente.getRuc()
    cliente_obj.nombre = formulario_cliente.getNombres()
    cliente_obj.razon_social = formulario_cliente.getRazonSocial()
    cliente_obj.plan_cuenta = PlanCuenta.objects.get(id=formulario_cliente.getGrupoClientes())

    if formulario_cliente.getActividad() != "":
        cliente_obj.actividad = Sector.objects.filter(status=1).get(id=formulario_cliente.getActividad())

    if Cliente.objects.filter(Q(ruc=cliente_obj.ruc), Q(status=1) | Q(status=2)).exists():
        contador += 1
        errors = formulario_cliente._errors.setdefault("ruc", ErrorList())
        errors.append(u"El Nº de Identificación ya ha sido ingresado.")
        messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación ya ha sido ingresado.")

    else:

        if cliente_obj.tipo_identificacion.codigo == "P":
            cliente_obj.tipo_cliente = TipoPersona.objects.get(id=formulario_cliente.getTipoCliente())
            contador = 0

        elif cliente_obj.tipo_identificacion.codigo == "R":
            if len(cliente_obj.ruc) == 13:
                if isEmpresaPublica(cliente_obj.ruc) or isPersonaJuridica(cliente_obj.ruc):
                    cliente_obj.tipo_cliente = TipoPersona.objects.get(id=2)
                elif isPersonaNatural(cliente_obj.ruc):
                    cliente_obj.tipo_cliente = TipoPersona.objects.get(id=1)
                else:
                    contador += 1
                    errors = formulario_cliente._errors.setdefault("ruc", ErrorList())
                    errors.append(u"El Nº de Identificación es incorrecto.")
                    messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación es incorrecto.")

            else:
                contador += 1
                errors = formulario_cliente._errors.setdefault("ruc", ErrorList())
                errors.append(u"El Nº de Identificación es incorrecto.")
                messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación es incorrecto.")


        elif cliente_obj.tipo_identificacion.codigo == "C":
            if isCedula(cliente_obj.ruc) and len(cliente_obj.ruc) == 10:
                cliente_obj.tipo_cliente = TipoPersona.objects.get(id=1)
            else:
                contador += 1
                errors = formulario_cliente._errors.setdefault("ruc", ErrorList())
                errors.append(u"El Nº de Identificación es incorrecto.")
                messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación es incorrecto.")

    if formulario_cliente.getFormaPago() != "":
        cliente_obj.forma_pago = TipoPago.objects.filter(status=1).get(id=formulario_cliente.getFormaPago()) # TipoPago

        if cliente_obj.forma_pago.id == 2:
            if formulario_cliente.getPlazo() == "" and formulario_cliente.getMontoCredito() == "":
                contador += 1
                errors = formulario_cliente._errors.setdefault("monto_credito", ErrorList())
                errors.append(u"Ingrese un monto crédito.")
                errors = formulario_cliente._errors.setdefault("plazo", ErrorList())
                errors.append(u"Ingrese un plazo.")
                messages.error(request, u"Error INFORMACIÓN COMERCIAL: Ingrese un monto de crédito y plazo")

            if formulario_cliente.getPlazo() == "" and formulario_cliente.getMontoCredito() != "":
                    cliente_obj.monto_credito = float(formulario_cliente.getMontoCredito())
                    contador += 1
                    errors = formulario_cliente._errors.setdefault("plazo", ErrorList())
                    errors.append(u"Ingrese un plazo.")
                    messages.error(request, u"Error INFORMACIÓN COMERCIAL: Ingrese un plazo")

            if formulario_cliente.getPlazo() != "" and formulario_cliente.getMontoCredito() == "":
                cliente_obj.plazo = formulario_cliente.getPlazo()
                contador += 1

                errors = formulario_cliente._errors.setdefault("monto_credito", ErrorList())
                errors.append(u"Ingrese un monto de crédito.")
                messages.error(request, u"Error INFORMACIÓN COMERCIAL: Ingrese un monto de crédito")

            if formulario_cliente.getPlazo() != "" and formulario_cliente.getMontoCredito() != "":
                cliente_obj.plazo = formulario_cliente.getPlazo()
                try:
                    cliente_obj.monto_credito = float(formulario_cliente.getMontoCredito())
                except:
                    pass
        else:
                cliente_obj.monto_credito = 0.0
                cliente_obj.plazo = ""
    else:
        cliente_obj.monto_credito = 0.0
        cliente_obj.plazo = ""

    if formulario_cliente.getMargenUtilidad() != "":
        cliente_obj.margen_utilidad = formulario_cliente.getMargenUtilidad()

    if formulario_cliente.get_email() != "":
        cliente_obj.email = formulario_cliente.get_email()

    if contador == 0:
        cliente_obj.fecha_creacion = now
        cliente_obj.usuario_creacion = request.user.username
        cliente_obj.save()

    return contador

def GuardarDirecciones(formset, cliente, now, request):
    '''
    Función que registra las direcciones
    al cliente
    :param formset:
    :param cliente:
    :param now:
    :param request:
    :return:
    '''
    bandera = 0
    if formset.is_valid():
        for form in formset:
            direcciones = Cliente_Direccion()
            informacion = form.cleaned_data
            direcciones.cliente = cliente

            if informacion.get("descripcion") is None and informacion.get("direccion1") is None and informacion.get("ciudad_direccion") is None:
                bandera += 1
                errors = form._errors.setdefault("descripcion", ErrorList())
                errors.append(u"")
                errors = form._errors.setdefault("direccion1", ErrorList())
                errors.append(u"")
                errors = form._errors.setdefault("ciudad_direccion", ErrorList())
                errors.append(u"")
                messages.error(request, u"Error en UBICACIÓN: Ingrese la descripción, dirección y ciudad.")

            if informacion.get("descripcion") is not None:
                direcciones.descripcion = informacion.get("descripcion")

            if informacion.get("direccion1") is not None:
                direcciones.direccion1 = informacion.get("direccion1")

            if informacion.get("ciudad_direccion") is not None:
                 try:
                     direcciones.ciudad = Ciudad.objects.filter(status=1).get(id=informacion.get("ciudad_direccion"))
                 except:
                     pass
                     bandera += 1
                     errors = form._errors.setdefault("ciudad_direccion", ErrorList())
                     errors.append(u"")
                     messages.error(request, u"Error en UBICACIÓN: Ingrese una ciudad.")


            direcciones.telefono = informacion.get("telefono")
            direcciones.direccion2 = informacion.get("direccion2")
            direcciones.atencion = informacion.get("atencion")

            if bandera == 0:
                direcciones.fecha_creacion = now
                direcciones.usuario_creacion = request.user.username
                direcciones.save()

    else:

        bandera += 1
        messages.error(request, u"Error en UBICACIÓN: Revise los campos del formulario.")

    return bandera

"""
function GuardarContacto(contacto, formulario, cliente,  now , request)
@ Return flag validadora
Details: Función para Guardar el Contacto
"""
def GuardarContacto(formset, cliente,   now, request):
    flag_contacto = 0
    sw = 0
    if formset.is_valid():
        for form in formset:
            contacto = Contacto()
            informacion = form.cleaned_data

            if informacion.get("nombres") == "" and informacion.get("apellidos") == "":
                sw += 1

            if sw == 0:
                contacto.tipo = 1
                contacto.cliente_proveedor_id = cliente.id

                contacto.nombres = informacion.get("nombres")
                contacto.apellidos = informacion.get("apellidos")
                contacto.telefono = informacion.get("telefono_contacto")
                contacto.movil = informacion.get("movil")
                contacto.correo = informacion.get("email")

                try:
                    contacto.departamento = Departamento.objects.filter(status=1).get(id=informacion.get("departamento"))
                except:
                    pass

                contacto.fecha_creacion = now
                contacto.usuario_creacion = request.user.username
                contacto.save()

    else:
        flag_contacto += 1
        messages.error(request, u"Error en Formulario Contactos, ingrese la información correctamente.")

    return flag_contacto


def agregar_cliente_ajax(cliente, formulario_cliente , respuesta, direccion, formulario_direccion, request):
    """
    Función para agregar un cliente por ajax
    :param cliente:
    :param formulario_cliente:
    :param respuesta:
    :param direccion:
    :param formulario_direccion:
    :param request:
    :return:
    """
    cliente.plan_cuenta = PlanCuenta.objects.get(id=formulario_cliente.getGrupoClientes())
    cliente.tipo_identificacion = Identificacion.objects.filter(status=1).get(codigo=formulario_cliente.getTipoIdentificacion())
    cliente.plan_cuenta = PlanCuenta.objects.filter(status=1).get(id=formulario_cliente.getGrupoClientes())
    cont = 0

    if formulario_cliente.getRazonSocial() == "":
        respuesta.append({"status": 2, "mensaje": u"Razón Social es obligatoria"})
        cont += 1
    else:
        cliente.razon_social = formulario_cliente.getRazonSocial()

    if formulario_cliente.getRuc() == "":
        respuesta.append({"status": 2, "mensaje": u"El número de identificación es obligatoria"})
        cont += 1
    else:
        cliente.ruc = formulario_cliente.getRuc()

    if emite_docs_electronicos():
        if formulario_cliente.get_email() == "":
            respuesta.append({"status": 2, "mensaje": u"El Email es obligatorio"})
            cont += 1
        else:
            cliente.email = formulario_cliente.get_email()

    if cont == 0:

        if Cliente.objects.filter(Q(ruc=cliente.ruc), Q(status=1) | Q(status=2)).exists():
            respuesta.append({"status": 2, "mensaje": u"El cliente ya existe o está inactivo"})
            cont += 1

        else:

            if cliente.tipo_identificacion.codigo == "R" and len(cliente.ruc) == 13:
                if isEmpresaPublica(cliente.ruc) or isPersonaJuridica(cliente.ruc):
                    cliente.tipo_cliente = TipoPersona.objects.get(codigo="02")
                elif isPersonaNatural(cliente.ruc):
                    cliente.tipo_cliente = TipoPersona.objects.get(codigo="01")
                else:
                    respuesta.append({"status": 3, "mensaje": u"Error Número de identificación incorrecto"})
                    cont += 1

            elif cliente.tipo_identificacion.codigo == "C":
                if len(cliente.ruc) == 10 and isCedula(cliente.ruc):
                    cliente.tipo_cliente = TipoPersona.objects.get(codigo="01")
                else:
                    respuesta.append({"status": 3, "mensaje": u"Error Número de identificación incorrecto"})
                    cont += 1

            elif cliente.tipo_identificacion.codigo == "P":
                cliente.tipo_cliente = TipoPersona.objects.get(id=formulario_cliente.getTipoCliente())

            if formulario_direccion.getDireccion1() == "":
                respuesta.append({"status": 2, "mensaje": u"Dirección es obligatoria"})
                cont += 1
            else:
                direccion.direccion1 = formulario_direccion.getDireccion1()
                cliente.direccion = direccion.direccion1

            if formulario_direccion.getCiudad() == "":
                respuesta.append({"status": 2, "mensaje": u"Ciudad es obligatoria"})
                cont += 1
            else:
                direccion.ciudad = Ciudad.objects.get(id=formulario_direccion.getCiudad())
                cliente.ciudad = direccion.ciudad


        if cont == 0:
            cliente.fecha_creacion = datetime.datetime.now()
            cliente.usuario_creacion = request.user.username
            cliente.save()

            direccion.cliente = cliente
            direccion.descripcion = "Principal"
            direccion.fecha_creacion = datetime.datetime.now()
            direccion.usuario_creacion = request.user.username
            direccion.save()

            respuesta.append({"status": 1, "mensaje": u"Se registró exitosamente el Cliente",
                              "id": cliente.id})

            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')
        else:
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')
    else:
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')