#! /usr/bin/python
# -*- coding: UTF-8-*-
from datetime import timedelta
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from contabilidad.formularios.ClienteForm import *
from contabilidad.formularios.DireccionesForm import *
from contabilidad.formularios.VentaForm import *
from contabilidad.formularios.FormGuiaRemision import *
from django.forms.util import ErrorList
from librerias.funciones.validacion_rucs import *
from librerias.funciones.comprobantes_electronicos import *
from librerias.funciones.funciones_vistas import *
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from django.contrib import messages
from django import template
import operator
register = template.Library()
IVA = 0.12

class ErrorVentas(Exception):
    def __init__(self, valor):
        self.valor = valor

    def __str__(self):
        return "Error " + str(self.valor)

class Detalle_Cont_Inventario():
    """
    Clase que me ayuda a agrupar los valores de las cuentas
    repetidas con sus costos
    """
    def __init__(self, id_cuenta, costo):
        self.id_cuenta = id_cuenta
        self.costo = costo

__author__ = 'Roberto'

########################################################################################################################
#                                           INSTANCIAS FORMULARIOS                                                     #
########################################################################################################################
def instancia_form_cabecera_venta(venta_cabecera, tipo):
    '''
    Función que carga la información de la venta en el formulario de la Cabecera
    para utilizarlo en la acción Copiar o A/G de la Venta
    :param venta_cabecera:
    :return:
    '''
    descuento0 = 0.0                                # Variable que carga la informacion del input descuento tarifa0%
    descuento12 = 0.0                               # Variable que carga la informacion del input descuento tarifa12%
    select_desc = False                             # Variable que carga si el check de descuento está activado
    control = 0                                     # Variable que determina el escenario de la venta (Descuento)
    tipo_desc = 0                                   # Variable que carga el tipo de descuento de la venta
    fecha = ""

    # NOTA: Revisar como cargar la información del Empleado(Vendedor)
    if venta_cabecera.pago_cont_credito.id == 2:
        plazo = venta_cabecera.plazo
    else:
        plazo = ""

    # Contrato
    try:
        contrato_det = ContratoDetalle.objects.filter(modulo_id=id, tipo_comprobante_id=2)          # Venta
        if len(contrato_det) == 0:
            id_contrato = ""
        elif len(contrato_det) == 1:
            id_contrato = contrato_det[0].contrato_cabecera.id
        else:
            id_contrato = ""
            messages.success(request, u"La venta que seleccionada tiene asignado varios "
                                      u"contratos a ella, si la anula perderá"
                                      u" la relación a dichos contratos")
    except:
        id_contrato = ""

    if venta_cabecera.tipo_descuento_combo is not None:
        if venta_cabecera.tipo_descuento_combo == 0:
            control = 0
            tipo_desc = 0
        elif venta_cabecera.tipo_descuento_combo == 1:
            control = 1
            tipo_desc = 1
            select_desc = True
        elif venta_cabecera.tipo_descuento_combo == 2:
            control = 2
            tipo_desc = 1
            select_desc = True
            descuento0 = float(venta_cabecera.descuento_tarifa0)
            descuento12 = float(venta_cabecera.descuento_tarifa12)
        elif venta_cabecera.tipo_descuento_combo == 3:
            control = 1
            tipo_desc = 2
            select_desc = True
        elif venta_cabecera.tipo_descuento_combo == 4:
            control = 2
            tipo_desc = 2
            select_desc = True

    if tipo == 2:  # Anular - Generar
        num_doc = venta_cabecera.num_documento
    else:
        num_doc = ""

    if tipo == 4:   # Venta Provisionada
        cliente = 0
        direccion = 0
        fecha = venta_cabecera.fecha
    else:
        cliente = venta_cabecera.cliente.id
        direccion = venta_cabecera.cliente.getDirecciones()


    return {"fecha": fecha, "reembolso_gasto": venta_cabecera.reembolso_gasto, "cliente": cliente, "direccion": direccion,
            "concepto": venta_cabecera.concepto, "numero_documento": num_doc, "serie": venta_cabecera.vigencia_doc_empresa.id, "num_autorizacion": "", "fecha_vencimiento": "",
            "forma_pago": venta_cabecera.pago_cont_credito.id, "plazo": plazo, "vendedor": "", "nota": venta_cabecera.nota, "descuento_porc": control, "tipo_descuento": tipo_desc,
            "contrato": id_contrato, "monto_descuento_doce": descuento12, "monto_descuento_cero": descuento0, "monto_descuento": venta_cabecera.porc_descuento, "select_desc": select_desc}

def instancia_form_set_detalle_venta(venta_cabecera):
    '''
    Función que carga la información del detalle de  la venta
    para utilizarlo en la acción Copiar o A/G de la Venta
    :param venta_cabecera:
    :return:
    '''
    venta_detalle = Venta_Detalle.objects.filter(venta_cabecera=venta_cabecera)
    det_form = []
    if venta_detalle:
        for obj in venta_detalle:
            if obj.centro_costo is not None:
                centro_costo_id = obj.centro_costo.id
            else:
                centro_costo_id = ""

            if venta_cabecera.tipo_descuento_combo == 0:            # Sin Descuento

                if obj.item.TipoItem() == 1:    # Productos
                    det_form.append({"cantidad": obj.cantidad, "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": obj.cant_entregada})

                else:                            # Servicios
                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": ""})

            elif venta_cabecera.tipo_descuento_combo == 1:          # Descuento % al Total de la Vemta

                if obj.item.TipoItem() == 1:                        # Productos
                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": obj.cant_entregada})
                else:                                               # Servicios

                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": ""})

            elif venta_cabecera.tipo_descuento_combo == 2:          # Descuento $ al Total de la Vemta

                if obj.item.TipoItem() == 1:                        # Productos

                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": obj.cant_entregada})
                else:                                               # Servicios

                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": ""})

            elif venta_cabecera.tipo_descuento_combo == 3:          # Descuento % Por Producto

                if obj.item.TipoItem() == 1:                        # Productos
                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": obj.cant_entregada})
                else:                                               # Servicios

                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": ""})

            else:                                                  # DEscuento $ Por Producto

                if obj.item.TipoItem() == 1:                       # Productos

                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": obj.cant_entregada})
                else:                                              # Servicios

                    det_form.append({"cantidad": obj.cantidad,
                                     "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                     "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                     "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": "", "cant_entregada": ""})
    return det_form

def instancia_form_set_detalle_venta_2(venta_cabecera):
    '''
    Función que realiza la instancia del formset utilizado o sólo para servicios o
    sólo para productos no tiene la columna cantidad entregada
    :param venta_cabecera:
    :return det_form:
    '''
    det_form = []
    venta_detalle = Venta_Detalle.objects.filter(venta_cabecera=venta_cabecera)

    for obj in venta_detalle:
        if obj.centro_costo is not None:
            centro_costo_id = obj.centro_costo.id
        else:
            centro_costo_id = ""

        if venta_cabecera.tipo_descuento_combo == 0:    # Sin Descuento

            if obj.item.TipoItem() == 1:                # Productos
                det_form.append({"cantidad": obj.cantidad,
                                 "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                 "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                 "unidad": obj.item.unidad.descripcion,
                                 "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": ""})

        elif venta_cabecera.tipo_descuento_combo == 1:  # Descuento % Venta

            if obj.item.TipoItem() == 1:                # Productos
                det_form.append({"cantidad": obj.cantidad,
                                 "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                 "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                 "unidad": obj.item.unidad.descripcion,
                                 "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": ""})

        elif venta_cabecera.tipo_descuento_combo == 2:  # Descuento $ al total de la venta

            if obj.item.TipoItem() == 1:                # Productos
                det_form.append({"cantidad": obj.cantidad,
                                 "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                 "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                 "unidad": obj.item.unidad.descripcion,
                                 "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": ""})

        elif venta_cabecera.tipo_descuento_combo == 3:  # Descuento % Por Producto

            if obj.item.TipoItem() == 1:                # Productos
                det_form.append({"cantidad": obj.cantidad,
                                 "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                 "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                 "unidad": obj.item.unidad.descripcion,
                                 "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": ""})

        else:    # Tipo_Descuento = 4;  Descuento $ Por Producto

            if obj.item.TipoItem() == 1:                # Productos
                det_form.append({"cantidad": obj.cantidad,
                                 "item": obj.item.id, "tipo_item": obj.item.TipoItem(), "concepto": obj.concepto,
                                 "centro_costo": centro_costo_id, "precio_u": obj.precio_unitario, "iva": obj.item.iva,
                                 "unidad": obj.item.unidad.descripcion,
                                 "ice": obj.item.porc_ice, "porc_descuento": "", "subtotal": ""})

    return det_form

def instancia_form_guia_remision():
    '''
    Función de la instancia de la guia de remisión según los datos de la empresa
    :return:
    '''
    return {"ruc_remision": get_datos_empresa().rucci, "compania": get_datos_empresa().razon_social, "direccion_conductor": get_datos_empresa().direccion,
                                                    "ciudad": Ciudad.objects.get(id=6).id,  # Definida Guayaquil
                                                    "serie": ""}

########################################################################################################################
#                                           FUNCIONES VENTAS                                                           #
########################################################################################################################
def concepto_cuenta_asiento_venta(cabecera_venta):
    '''
    Función que me retorn el mensaje estándar
    para las cuentas del asiento contable realizado
    por la venta
    :param venta_cabecera:
    :return:
    '''
    return u"Cliente: "+unicode(cabecera_venta.cliente.razon_social)[0:22]+u"; "+u"# Doc: "+unicode(cabecera_venta.vigencia_doc_empresa.serie)+u"-"+unicode(cabecera_venta.num_documento)

def redireccionar_venta(request, venta_cabecera):
    """
    Función que redirecciona dependiendo de la
    acción que desee el usuario sobre la venta
    Ej: Grabar - Grabar y Agregar Otra - Grabar Imprimir.
    :return:
    """
    if request.GET.get("grabar_agregar", "") == "1":
        full_redirect_url = reverse("agregar_ventas")

    elif request.GET.get("grabar_imprimir", "") == "1":
        redirect_url = reverse("agregar_ventas")
        extra_params = '?imp=%s' % venta_cabecera.id
        full_redirect_url = '%s%s' % (redirect_url, extra_params)

    elif request.GET.get("facturar", "") == "1":
        redirect_url = reverse("agregar_ventas")
        extra_params = '?fact=%s' % venta_cabecera.id
        full_redirect_url = '%s%s' % (redirect_url, extra_params)
    else:
        full_redirect_url = reverse("lista_ventas")

    return full_redirect_url

def AnularVenta(id, now, request, contador):
    '''
    Función para anular la venta esta se utiliza dentro de la vista
    no responde a ningun request, esta función se utiliza en
    "Anular y generar Venta"
    :param venta:
    :param now:
    :param request:
    :param contador:
    :return:
    '''
    venta = Venta_Cabecera.objects.get(id=id)
    detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=venta)

    if venta.status == 3:       # venta provisionada
        venta.status = 2
        venta.fecha_actualizacion = now
        venta.usuario_actualizacion = request.user.username
        venta.save()

    else:

        try:
            cuenta_cobrar = Cuentas_por_Cobrar.objects.filter(status=1).get(venta_cabecera=venta)
            tipo_comp = TipoComprobante.objects.get(id=16)                          # Tipo Comprobante Retención Venta

            if venta.estado_firma >= 4:
                messages.error(request, u"No se puede anular ni generar la venta, "
                                        u"ya que ha sido autorizada por el SRI.")
                return HttpResponseRedirect(reverse("lista_ventas"))

            if controla_stock():
                cuenta_cobrar.fecha_actualizacion = now
                cuenta_cobrar.usuario_actualizacion = request.user.username
                cuenta_cobrar.status = 2
                cuenta_cobrar.save()

            else:

                if cuenta_cobrar.cobrado > 0:
                    messages.error(request, u"No se puede anular la venta, "
                                            u"ya que se han hecho cobros a ella")
                    contador += 1
                else:
                    cuenta_cobrar.fecha_actualizacion = now
                    cuenta_cobrar.usuario_actualizacion = request.user.username
                    cuenta_cobrar.status = 2
                    cuenta_cobrar.save()

            ############################################################################################################
            #                           Anular C_x_Cobro                                                               #
            ############################################################################################################
            lista_cxc_cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuenta_cobrar)
            for c in lista_cxc_cobro:
                c.status = 2
                c.fecha_actualizacion = now
                c.usuario_actualizacion = request.user.username
                c.save()
            ############################################################################################################

            ############################################################################################################
            #                           Anula el Cobro                                                                 #
            ############################################################################################################
            if CXC_cobro.objects.filter(cuentas_x_cobrar=cuenta_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
                cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuenta_cobrar, tipo_comprobante=tipo_comp, status=1)[0].cobro
                cobro.status = 2
                cobro.fecha_actualizacion = now
                cobro.usuario_actualizacion = request.user.username
                cobro.save()

        except Cuentas_por_Cobrar.DoesNotExist:
                messages.error(request, u"Existe un problema al anular la venta no encuentra la cuenta por "
                                        u"cobrar asociada a la venta seleccionada")
                contador += 1

        ################################################################################################################
        #                                    Anula Asiento de Venta                                                    #
        ################################################################################################################
        cabecera_asiento = Cabecera_Comp_Contable.objects.get(numero_comprobante=venta.numero_comprobante, status=1)
        detalle_asiento = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_asiento, status=1)

        venta.status = 2
        venta.usuario_actualizacion = request.user.username
        venta.fecha_actualizacion = now
        venta.save()

        for obj in detalle_venta:
            obj.status = 2
            obj.usuario_actualizacion = request.user.username
            obj.fecha_actualizacion = now
            obj.save()

        if venta.status == 2:
            cabecera_asiento.status = 2
            cabecera_asiento.fecha_actualizacion = now
            cabecera_asiento.usuario_actualizacion = request.user.username
            cabecera_asiento.save()

            for d in detalle_asiento:
                d.status = 2
                d.fecha_actualizacion = now
                d.usuario_actualizacion = request.user.username
                d.save()
        ################################################################################################################

        ############################################################################################################
        #                           Anula Guía de Remisióno                                                        #
        ############################################################################################################
        try:
            guia = CabeceraGuiaRemision.objects.get(venta_cabecera=venta, status=1)
            detalles_guia = DetalleGuiaRemision.objects.filter(guia_rem_cabecera=guia, status=1)
            guia.status = 2
            guia.fecha_actualizacion = now
            guia.usuario_actualizacion = request.user.username
            guia.save()
            for obj in detalles_guia:
                obj.status = 2
                obj.fecha_actualizacion = now
                obj.usuario_actualizacion = request.user.username
                obj.save()
        except:
            pass

        cadena_num_doc = venta.vigencia_doc_empresa.serie+"-"+venta.num_documento

        # Verifica si la venta realizó movimientos en inventario
        if Inventario_Cabecera.objects.filter(tipo_comprobante=TipoComprobante.objects.get(id=15), num_doc=cadena_num_doc).exists():
            try:
                Anula_Inventario(venta, request, now)
            except:
                contador += 1
                messages.error(request, u"Error al anular los movimientos del inventario")

        # Contrato
        try:
            contratos = ContratoDetalle.objects.filter(tipo_comprobante_id=2, modulo_id=venta.id)
            for obj in contratos:
                obj.status = 2
                obj.fecha_actualizacion = now
                obj.usuario_actualizacion = request.user.username
                obj.save()
        except:
            pass

    return contador

def Anula_Inventario(venta_cabecera, request, now):
    '''
    Función que Anula las Tablas de Inventario a partir de la venta
    seleccionada y actualiza en Item Bodega la cantidad que regresa
    a bodega
    :param venta_cabecera:
    :param request:
    :param now:
    :return:
    '''
    num_doc_vta = venta_cabecera.vigencia_doc_empresa.serie+"-"+venta_cabecera.num_documento
    detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=venta_cabecera)

    # 1.-) Update Cantidad Actual en Bodega
    for it in detalle_venta:
        item_bodega = Item_Bodega.objects.filter(status=1).get(item=it.item)
        item_bodega.cantidad_actual += it.cant_entregada
        item_bodega.fecha_actualizacion = now
        item_bodega.usuario_actualizacion = request.user.username
        item_bodega.save()

    # 2.-) Anular Cabecera Inventario y Detalle Inventario
    lista_inventario_cabecera = Inventario_Cabecera.objects.filter(num_doc=num_doc_vta)
    for cainv in lista_inventario_cabecera:
        cainv.status = 2
        cainv.fecha_actualizacion = now
        cainv.usuario_actualizacion = request.user.username
        cainv.save()

        for detinv in Inventario_Detalle.objects.filter(inventario_cabecera=cainv):
            detinv.status = 2
            detinv.fecha_actualizacion = now
            detinv.usuario_actualizacion = request.user.username
            detinv.save()

        # 3.-) Anular Cabecera Comprobante Contable y Detalle Comprobante Contable
        for cabcont in Cabecera_Comp_Contable.objects.filter(numero_comprobante=cainv.num_comp):
            cabcont.status = 2
            cabcont.usuario_actualizacion = request.user.username
            cabcont.fecha_actualizacion = now
            cabcont.save()

            for detcont in Detalle_Comp_Contable.objects.filter(cabecera_contable=cabcont):
                detcont.status = 2
                detcont.usuario_actualizacion = request.user.username
                detcont.fecha_actualizacion = now
                detcont.save()

def ValidacionFormsVentas(formulario, controlador):
    '''
     Función general validación de campos de formulario
    :param formulario:
    :param controlador:
    :return:
    '''
    bandera = 0
    #####################################
    #   Validación de la forma de pago  #
    #####################################
    if(int(formulario.getFormaPago()) == 2):
        if formulario.getPlazo() == "":
            errors = formulario._errors.setdefault("plazo", ErrorList())
            errors.append(u"Usted debe de ingresar un plazo si es pago a crédito")
            bandera += 1
    ##################################################
    #  Validación de Formas de Descuento.            #
    ##################################################
    if formulario.getTipoDescuento() != "" and formulario.getDescuentoPorcentaje() != "":
        if controlador == 2:
            if formulario.getMontoDescuento() == "":
                errors = formulario._errors.setdefault("monto_descuento", ErrorList())
                errors.append(u"Ingrese un porcentaje de descuento al total de la venta")
                bandera += 1

        if controlador == 3:
            if formulario.getMontoDescuentoDoce() == "" or formulario.getMontoDescuentoDoce() == 0.0:
                errors = formulario._errors.setdefault("monto_descuento_doce", ErrorList())
                errors.append(u"Ingrese los valores del descuento del subtotal_12% por venta.")
                bandera += 1

            if formulario.getMontoDescuentoCero() == "" or formulario.getMontoDescuentoCero() == 0.0:
                errors = formulario._errors.setdefault("monto_descuento_cero", ErrorList())
                errors.append(u"Ingrese los valores del descuento del subtotal_0% por venta.")
                bandera += 1

    ##################################################
    #       Validación del Concepto General.         #
    ##################################################
    if formulario.getConcepto() is not None:
        if unicode(formulario.getConcepto()).isspace():
            errors = formulario._errors.setdefault("concepto", ErrorList())
            errors.append(u"Ingrese un concepto de comprobante válido...")
            bandera += 1

    return bandera

def busqueda_ventas_reloaded(buscador):
    """
    function busqueda_ventas(buscador)
    """
    predicates = []

    if buscador.getFecha() is not None:
        predicates.append(('fecha__gte',  buscador.getFecha()))
    predicates.append(('fecha__lte',  buscador.getFechaFinal()))
    predicates.append(('numero_comprobante__icontains', buscador.getNumComprobante()))
    predicates.append(('num_documento__icontains', buscador.getNumeroDocumento()))

    if buscador.getReferencia() != "":
        predicates.append(('nota__icontains', buscador.getReferencia()))
    if buscador.getVentaIVA():
        predicates.append(('subtotal_tarifa12__gte', 0))
    if buscador.getVentaSinIVA():
        predicates.append(('subtotal_tarifa12', 0))
    if buscador.getReembolsoGasto():
        predicates.append(('reembolso_gasto', buscador.getReembolsoGasto()))
    if buscador.getEstado() != "":
        estado = Estados.objects.get(id=buscador.getEstado())
        predicates.append(('status', estado.codigo))
    if buscador.getCliente() is not None:
        predicates.append(('cliente', buscador.getCliente()))
    if buscador.getTipoDoc() is not None:
        predicates.append(('documento', buscador.getTipoDoc()))
    if buscador.getFormaPago() is not None:
        predicates.append(('pago_cont_credito', buscador.getFormaPago()))
    if buscador.getItem() is not None:
        predicates.append(('venta_detalle__item', buscador.getItem()))
    if buscador.getVendedorBuscador() is not None:
        predicates.append(('vendedor', buscador.getVendedorBuscador()))
    if emite_docs_electronicos():
        if buscador.getEstadoDocElect() != "":
            predicates.append(('estado_firma', buscador.getEstadoDocElect()))
    if buscador.getVigenciaDocEmp() is not None:
        predicates.append(('vigencia_doc_empresa', buscador.getVigenciaDocEmp()))

    q_list = [Q(x) for x in predicates]

    entries = Venta_Cabecera.objects.exclude(status=0).extra(where=["baseiva - descuentoiva + base0 - descuento0 + venta_cabecera.monto_iva + total_ice >= %s AND baseiva - descuentoiva + base0 - descuento0 + venta_cabecera.monto_iva + total_ice <= %s"], params=[buscador.getRangoMontoIni(), buscador.getRangoMontoHasta()]).filter(reduce(operator.and_, q_list)).distinct().order_by("-fecha", "-numero_comprobante","-num_documento").distinct()
    return entries

def CalculaTotales(formulario, detalleform):
    '''
    Función que recibe un formset y el formulario cabecera y calcula los totales
    de la venta dependiendo de la selección del usuario
    :param formulario:
    :param detalleform:
    :return:
    '''
    controlador = 0
    porc_monto_desc = 0
    tipo_prod_desc = 0
    subtotales = 0.0
    subtotales_cero = 0.0
    subtotal_simple = 0.0
    subtotal_simple_cero = 0.0
    iva_valor = 0.0
    ice_valor = 0.0
    resultado = 0.0
    descuento_iva = 0.0
    descuento_cero = 0.0
    monto_descuento = 0.0
    monto_descuento12 = 0.0
    monto_descuento0 = 0.0

    for form in detalleform:
        informacion = form.cleaned_data
        cantidad = float(informacion.get("cantidad", "0"))
        precio_u = redondeo(float(informacion.get("precio_u", "0")), 2)
        porc_iva = redondeo(float(informacion.get("iva", "0")), 2)

        if informacion.get("ice") != None:
            ice = redondeo(float((informacion.get("ice"))), 2)
        else:
            ice = 0.0
        if informacion.get("porc_descuento") != None:
            descuento_producto = redondeo(float((informacion.get("porc_descuento"))), 2)
        else:
            descuento_producto = 0.0

        if informacion.get("subtotal") != None:
            subtotal = redondeo(float((informacion.get("subtotal"))), 2)
        else:
            subtotal = 0.0

        if formulario.getDescuentoPorcentaje() != "":
            porc_monto_desc = redondeo(float(formulario.getDescuentoPorcentaje()), 2)

        if formulario.getTipoDescuento() != "":
            tipo_prod_desc = int(formulario.getTipoDescuento())

        if formulario.getMontoDescuento() != "":
            monto_descuento = redondeo(float(formulario.getMontoDescuento()), 2)
        if formulario.getMontoDescuentoDoce() != "":
            monto_descuento12 = redondeo(float(formulario.getMontoDescuentoDoce()), 2)
        if formulario.getMontoDescuentoCero() != "":
            monto_descuento0 = redondeo(float(formulario.getMontoDescuentoCero()), 2)

        if porc_monto_desc == 0 and tipo_prod_desc == 0:
            controlador = 1
            if porc_iva != 0.0:
                subtotales = subtotales + subtotal
                iva_valor = iva_valor + float((subtotal)*(porc_iva/100))
                ice_valor = ice_valor + float((subtotal)*(ice/100))
                resultado = subtotales_cero + ice_valor + iva_valor + subtotales
            else:
                subtotales_cero = subtotales_cero + subtotal
                resultado = subtotales_cero + ice_valor + iva_valor + subtotales

        if porc_monto_desc == 1 and tipo_prod_desc == 1:
            controlador = 2
            if porc_iva != 0.0:
                subtotal_simple = subtotal_simple + subtotal
                subtotales = subtotales + subtotal - float((subtotal*monto_descuento)/100)
                ice_valor = ice_valor + subtotal*ice/100
                iva_valor = iva_valor + (subtotal - subtotal*monto_descuento/100)*porc_iva/100
                descuento_iva = descuento_iva + float(subtotal*monto_descuento/100)
            else:
                subtotal_simple_cero = subtotal_simple_cero + subtotal
                subtotales_cero = subtotales_cero + subtotal - subtotal*monto_descuento/100
                descuento_cero = descuento_cero + float(subtotal*monto_descuento/100)
            resultado = subtotales + subtotales_cero + iva_valor + ice_valor

        if porc_monto_desc == 2 and tipo_prod_desc == 1:
            controlador = 3
            if porc_iva != 0.0:
                subtotales = subtotales + subtotal
                ice_valor = ice_valor + subtotal*ice/100
                descuento_iva = subtotales - monto_descuento12
                iva_valor = float((descuento_iva)*(porc_iva/100))
            else:
                subtotales_cero = subtotales_cero + subtotal
                descuento_cero = subtotales_cero - monto_descuento0
            resultado = descuento_iva + descuento_cero + iva_valor + ice_valor

        if porc_monto_desc == 1 and tipo_prod_desc == 2:
            controlador = 4
            if porc_iva != 0.0:
                subtotal_simple = subtotal_simple + cantidad*precio_u
                subtotales = subtotales + subtotal
                ice_valor = ice_valor + (cantidad*precio_u)*(ice/100)
                iva_valor = iva_valor + (subtotal)*porc_iva/100
                descuento_iva = descuento_iva + (cantidad*precio_u)*(descuento_producto/100)
            else:
                subtotal_simple_cero = subtotales_cero + cantidad*precio_u
                subtotales_cero = subtotales_cero + subtotal
                descuento_cero = descuento_cero + (cantidad*precio_u)*(descuento_producto/100)
            resultado = subtotales + subtotales_cero + iva_valor + ice_valor

        if porc_monto_desc == 2 and tipo_prod_desc == 2:
            controlador = 5
            if porc_iva != 0.0:
                subtotal_simple = subtotal_simple + cantidad*precio_u
                subtotales = subtotales + subtotal
                ice_valor = ice_valor + (cantidad*precio_u)*(ice/100)
                iva_valor = iva_valor + (subtotal)*porc_iva/100
                descuento_iva = descuento_iva + descuento_producto
            else:
                subtotal_simple_cero = subtotales_cero + cantidad*precio_u
                subtotales_cero = subtotales_cero + subtotal
                descuento_cero = descuento_cero + descuento_producto
            resultado = subtotales + subtotales_cero + iva_valor + ice_valor

    return redondeo(subtotal_simple, 2), redondeo(subtotal_simple_cero, 2), redondeo(subtotales, 2), redondeo(subtotales_cero, 2), redondeo(iva_valor, 2), redondeo(ice_valor, 2), redondeo(resultado, 2), redondeo(descuento_iva, 2), redondeo(descuento_cero, 2), redondeo(monto_descuento12, 2), redondeo(monto_descuento0, 2), controlador

def Guardar_Contrato_Venta(formulario, cabecera_venta, request, now):
    '''
    Función que asocia la venta a un contrato
    :param formulario:
    :param cabecera_venta:
    :param request:
    :param now:
    :return:
    '''
    detalle_cont = ContratoDetalle()
    detalle_cont.contrato_cabecera = formulario.getContrato()
    detalle_cont.tipo_comprobante = TipoComprobante.objects.get(id=2)  # Comprobante de Venta
    detalle_cont.valor = cabecera_venta.getTotalPagar()
    detalle_cont.modulo_id = cabecera_venta.id
    detalle_cont.usuario_creacion = request.user.username
    detalle_cont.fecha_creacion = now
    detalle_cont.save()
    messages.success(request, u"Se ha registrado la venta al contrato: " +
                     str(detalle_cont.contrato_cabecera.num_cont) + u" del cliente: " +
                     unicode(detalle_cont.contrato_cabecera.cliente.razon_social))

def GuardarVentasCabecera(cabecera_venta, formulario, id_doc, secuencia, subtotal_simple, subtotal_cero,  subtotales, subtotales_cero, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, tipo, dia_actual, controlador, request):
    '''
    Función Guardar Venta la Cabecera de la venta según los escenarios
    :param cabecera_venta:
    :param formulario:
    :param id_doc:
    :param secuencia:
    :param subtotal_simple:
    :param subtotal_cero:
    :param subtotales:
    :param subtotales_cero:
    :param iva_valor:
    :param ice_valor:
    :param descuento_iva:
    :param descuento_cero:
    :param monto_descuento12:
    :param monto_descuento0:
    :param tipo:
    :param dia_actual:
    :param controlador:
    :param request:
    :return:
    '''
    bandera = 0     # Validaciones
    cabecera_venta.concepto = formulario.getConcepto()
    cabecera_venta.fecha = formulario.getFecha()
    cabecera_venta.nota = formulario.getNota()
    cabecera_venta.documento = Documento.objects.filter(status=1).get(id=1)     # Factura

    if formulario.getReembolso() != "":
        cabecera_venta.reembolso_gasto = formulario.getReembolso()
    try:
        cabecera_venta.cliente = Cliente.objects.get(id=formulario.getCliente(), status=1)
    except:
        bandera += 1
        messages.error(request, u"El cliente que ingresó no se encuentra activo o no existe, por favor verifique.")
    try:
        cabecera_venta.pago_cont_credito = TipoPago.objects.filter(status=1).get(id=formulario.getFormaPago())
    except:
        pass
    try:
        cabecera_venta.vendedor = Colaboradores.objects.filter(status=1).get(id=formulario.getVendedor())
    except:
        pass
    try:
        cabecera_venta.cliente_direccion = Cliente_Direccion.objects.filter(status=1).get(id=formulario.getDireccion())
    except:
        pass

    if formulario.getContrato() is not None:
        cabecera_venta.contrato = formulario.getContrato()  # Guarda el id del contrato cabecera

    if tipo == 4: # VENTA PROVISIONADA
        cabecera_venta.status = 1
        cabecera_venta.fecha_actualizacion = dia_actual
        cabecera_venta.usuario_actualizacion = request.user.username

    else:

        if formulario.getFecha() > datetime.datetime.now().date():
            bandera += 1
            messages.error(request, u"La fecha de registro es mayor a la fecha actual")

        try:
            cabecera_venta.vigencia_doc_empresa = VigenciaDocEmpresa.objects.filter(documento_sri__id=1).get(status=1, id=formulario.getSerie())
        except VigenciaDocEmpresa.DoesNotExist:
            bandera += 1
            messages.error(request, u"Error verifique si el block de documento está vigente")


        #########################################################################
        #   Si el id_doc != 0 la venta viene de la función "anular y generar"   #
        #   por lo tanto guarda el # de documento y no se pierde.               #
        #########################################################################
        if id_doc == 0:
            if formulario.getFecha() < cabecera_venta.vigencia_doc_empresa.fecha_actual:
                bandera += 1
                messages.error(request, u"La fecha de registro es menor a la fecha "
                                        u"de vigencia del documento.")
            else:
                num_doc = get_num_doc(cabecera_venta.vigencia_doc_empresa, formulario.getFecha(), request)

                if num_doc != -1:
                    cabecera_venta.num_documento = num_doc
                else:
                    messages.error(request, u"La secuencia del documento es mayor a la cantidad "
                                            u"registrada en el block de documentos vigentes.")
                    bandera += 1
        else:
            if secuencia != 0.0:
                cabecera_venta.num_documento = secuencia


    num_comp = get_num_comp(2, formulario.getFecha(), False, request)
    if num_comp != -1:
        cabecera_venta.numero_comprobante = num_comp
    else:
        bandera += 1
        messages.error(request, u"Error la fecha de registro es menor al último registro de ventas "
                                u"por favor verifique la fecha de registro.")

    cabecera_venta.fecha_creacion = dia_actual
    cabecera_venta.usuario_creacion = request.user.username

    ##################################################
    #  Validación de Formas de Descuento.            #
    ##################################################
    if formulario.getTipoDescuento() != "" and formulario.getDescuentoPorcentaje() == "":
        bandera += 1
        errors = formulario._errors.setdefault("descuento_porc", ErrorList())
        errors.append(u"")
        messages.error(request, u"Seleccione la forma para realizar el descuento.")

    elif formulario.getTipoDescuento() == "" and formulario.getDescuentoPorcentaje() != "":
        bandera += 1
        errors = formulario._errors.setdefault("tipo_descuento", ErrorList())
        errors.append(u"")
        messages.error(request, u"Seleccione el tipo de descuento para la venta.")

    elif formulario.getTipoDescuento() != "" and formulario.getDescuentoPorcentaje() != "":
        cabecera_venta.tipo_descuento_combo = int(formulario.getTipoDescuento())
        if controlador == 2:
            cabecera_venta.porc_descuento = float(formulario.getMontoDescuento())
            if formulario.getMontoDescuento() == "" or float(formulario.getMontoDescuento()) == 0.0:
                bandera += 1
                messages.error(request, u"Ingrese un porcentaje de descuento por venta.")

        if controlador == 3:
            if formulario.getMontoDescuentoDoce() == "":
                bandera += 1
                cabecera_venta.valor_descuento = 0.0
                messages.error(request, u"Ingrese los valores de descuento del subtotal_12% por venta.")
            else:
                cabecera_venta.valor_descuento = float(formulario.getMontoDescuentoDoce())

            if formulario.getMontoDescuentoCero() == "":
                bandera += 1
                cabecera_venta.valor_descuento_cero = 0.0
                messages.error(request, u"Ingrese los valores de descuento del subtotal_0% por venta.")
            else:
                cabecera_venta.valor_descuento_cero = float(formulario.getMontoDescuentoCero())

    ##################################################
    #  Validación de Concepto Comprobante.           #
    ##################################################
    if formulario.getConcepto() is not None:
        concepto = unicode(formulario.getConcepto())
        if concepto.isspace():
            messages.error(request, u"Ingrese un concepto de comprobante válido...")
            bandera += 1

    #####################################
    #   Validación de la forma de pago  #
    #####################################
    if cabecera_venta.pago_cont_credito is not None:
        if cabecera_venta.pago_cont_credito.id == 2:
            if formulario.getPlazo() != "":
                cabecera_venta.plazo = int(float(formulario.getPlazo()))
            else:
                bandera += 1
                messages.error(request, u"Usted debe ingresar un plazo si es pago a crédito")

    if controlador == 1:
        cabecera_venta.porc_descuento = 0.0
        cabecera_venta.descuento_tarifa12 = 0.0
        cabecera_venta.descuento_tarifa0 = 0.0
        cabecera_venta.subtotal_tarifa12 = redondeo(subtotales, 2)
        cabecera_venta.subtotal_tarifa0 = redondeo(subtotales_cero, 2)
        cabecera_venta.iva_total = redondeo(iva_valor, 2)
        cabecera_venta.total_ice = redondeo(ice_valor, 2)
        cabecera_venta.tipo_descuento_combo = 0
    elif controlador == 2:
        cabecera_venta.tipo_descuento_combo = 1
        cabecera_venta.descuento_tarifa12 = redondeo(descuento_iva, 2)
        cabecera_venta.descuento_tarifa0 = redondeo(descuento_cero, 2)
        cabecera_venta.iva_total = redondeo(iva_valor, 2)
        cabecera_venta.total_ice = redondeo(ice_valor, 2)
        cabecera_venta.subtotal_tarifa12 = redondeo(subtotal_simple, 2)
        cabecera_venta.subtotal_tarifa0 = redondeo(subtotal_cero, 2)
    elif controlador == 3:
        cabecera_venta.descuento_tarifa12 = redondeo(monto_descuento12, 2)
        cabecera_venta.descuento_tarifa0 = redondeo(monto_descuento0, 2)
        cabecera_venta.tipo_descuento_combo = 2
        cabecera_venta.subtotal_tarifa12 = redondeo(subtotales, 2)
        cabecera_venta.subtotal_tarifa0 = redondeo(subtotales_cero, 2)
        cabecera_venta.iva_total = redondeo(iva_valor, 2)
        cabecera_venta.total_ice = redondeo(ice_valor, 2)
    elif controlador == 4:
        cabecera_venta.descuento_tarifa12 = redondeo(descuento_iva, 2)
        cabecera_venta.descuento_tarifa0 = redondeo(descuento_cero, 2)
        cabecera_venta.tipo_descuento_combo = 3
        cabecera_venta.subtotal_tarifa12 = redondeo(subtotal_simple, 2)
        cabecera_venta.subtotal_tarifa0 = redondeo(subtotal_cero, 2)
        cabecera_venta.iva_total = redondeo(iva_valor, 2)
        cabecera_venta.total_ice = redondeo(ice_valor, 2)
    else:
        cabecera_venta.descuento_tarifa12 = redondeo(descuento_iva, 2)
        cabecera_venta.descuento_tarifa0 = redondeo(descuento_cero, 2)
        cabecera_venta.tipo_descuento_combo = 4
        cabecera_venta.subtotal_tarifa12 = redondeo(subtotal_simple, 2)
        cabecera_venta.subtotal_tarifa0 = redondeo(subtotal_cero, 2)
        cabecera_venta.iva_total = redondeo(iva_valor, 2)
        cabecera_venta.total_ice = redondeo(ice_valor, 2)
    cabecera_venta.save()

    return bandera

def GuardarCuentasPorCobrar(cuentas_cobrar, cabecera_venta, resultado, dia_actual, flag_esta_cobrado, request):
    '''
    Función que graba la información en la tabla Cuentas_x_Cobrar
    :param cuentas_cobrar:
    :param cabecera_venta:
    :param resultado:
    :param dia_actual:
    :param flag_esta_cobrado:
    :param request:
    :return:
    '''
    ##################################
    #       Cuentas por Cobrar       #
    ##################################
    cuentas_cobrar.cliente = cabecera_venta.cliente
    cuentas_cobrar.venta_cabecera = cabecera_venta
    cuentas_cobrar.documento = cabecera_venta.documento
    cuentas_cobrar.monto = redondeo(resultado, 2)

    cuentas_cobrar.num_doc = cabecera_venta.vigencia_doc_empresa.serie+"-"+cabecera_venta.num_documento
    cuentas_cobrar.plazo = cabecera_venta.plazo
    cuentas_cobrar.naturaleza = 1
    cuentas_cobrar.fecha_emi = cabecera_venta.fecha
    cuentas_cobrar.fecha_reg = cabecera_venta.fecha

    if flag_esta_cobrado == 1:
        cuentas_cobrar.cobrado = cuentas_cobrar.monto
    else:
        cuentas_cobrar.cobrado = 0

    cuentas_cobrar.fecha_creacion = dia_actual
    cuentas_cobrar.usuario_creacion = request.user.username
    cuentas_cobrar.save()

def GuardarMayor(cabecera_comprobante, cabecera_venta, tipo_c, now, request):
    """
    function GuardarMayor(cabecera_comprobante, cabecera_venta, now, request)
    @Return None
    Details: Guarda los datos del mayor cabecera
    """
    cabecera_comprobante.concepto_comprobante = cabecera_venta.concepto
    cabecera_comprobante.fecha = cabecera_venta.fecha
    cabecera_comprobante.numero_comprobante = cabecera_venta.numero_comprobante
    cabecera_comprobante.tipo_comprobante = tipo_c
    cabecera_comprobante.fecha_creacion = now
    cabecera_comprobante.usuario_creacion = request.user.username
    cabecera_comprobante.save()

def UpdateDetalleVenta(detalleform, now, request):
    '''
    Función que Actualiza en Venta Detalle la cantidad entregada
    esta función es llamada al realizar la acción de realizar una guía de
    remisión a partir de una venta ya registrada en el sistema
    :param detalleform:
    :param now:
    :param request:
    :return:
    '''
    for form in detalleform:
        informacion2 = form.cleaned_data
        detalle_venta = Venta_Detalle.objects.get(id=informacion2.get("id_detalle_venta"))
        detalle_venta.cant_entregada += float(informacion2.get("cant_entregada"))
        detalle_venta.fecha_actualizacion = now
        detalle_venta.usuario_actualizacion = request.user.username
        detalle_venta.save()

def GuardarVentaDetalle(detalleform, formulario, cabecera_venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now, controlador, request):
    '''
    Función que Guarda los datos del Detalle de la Venta
    :param detalleform:
    :param formulario:
    :param cabecera_comprobante:
    :param cabecera_venta:
    :param subtotales:
    :param subtotales_cero:
    :param monto_descuento12:
    :param monto_descuento0:
    :param now:
    :param controlador:
    :param request:
    :return:
    '''
    porcentaje_desc = 0.0
    por_desc_result = 0.0
    por_desc_result_cero = 0.0
    valor_desc12 = 0.0
    valor_desc0 = 0.0
    valor_ice = 0.0
    parametro = 0
    flag_inventario = 0
    total_iva = 0.0
    total_cantidad = 0.0
    validador = 0   # Validador

    for form in detalleform:
        informacion = form.cleaned_data
        detalle_venta = Venta_Detalle()
        detalle_venta.venta_cabecera = cabecera_venta
        detalle_venta.cantidad = informacion.get("cantidad") # Cantidad Vendida
        try:
            detalle_venta.centro_costo = Centro_Costo.objects.get(id=informacion.get("centro_costo"))
        except:
            pass

        detalle_venta.item = Item.objects.filter(status=1).get(id=informacion.get("item"))

        # Actualiza la tabla Cliente - Item
        cliente_item = Cliente_Item.objects.filter(status=1).filter(cliente__id=cabecera_venta.cliente.id, item__id=int(informacion.get("item")))
        for c in cliente_item:
            if c.status == 1:
                c.delete()

        UpdateClienteItem(int(informacion.get('item')), cabecera_venta, float(informacion.get("precio_u")), now, request)

        if informacion.get('concepto') != "":
            detalle_venta.concepto = informacion.get('concepto')

        if informacion.get("precio_u") is None and informacion.get("subtotal") is None:
            raise ErrorVentas(u"Por favor ingrese un valor en el precio.")
        else:
            detalle_venta.precio_unitario = float(informacion.get("precio_u"))

        if informacion.get("ice") is not None:
            detalle_venta.porcentaje_ice = float(informacion.get("ice"))
        else:
            detalle_venta.porcentaje_ice = 0.0

        if informacion.get("iva") is not None:
            detalle_venta.porcentaje_iva = float(informacion.get("iva"))
        else:
            detalle_venta.porcentaje_iva = 0.0

        if formulario.getMontoDescuento() != "":
            porcentaje_desc = float(formulario.getMontoDescuento())

        if controlador == 1:
            detalle_venta.descuento_porc = 0.0
            detalle_venta.descuento_valor = 0.0

            if float(informacion.get("iva")) != 0.0:
                detalle_venta.monto_iva = float(informacion.get("subtotal"))*IVA
            else:
                detalle_venta.monto_iva = 0.0

        if controlador == 2:
            detalle_venta.descuento_porc = porcentaje_desc
            detalle_venta.descuento_valor = (float(informacion.get("subtotal")))*(float(porcentaje_desc/100))

            if float(informacion.get("iva")) != 0.0:
                detalle_venta.monto_iva = ((float(informacion.get("subtotal"))) - (float(informacion.get("subtotal")))*(float(porcentaje_desc/100)))*IVA
            else:
                detalle_venta.monto_iva = 0.0

        if controlador == 3:
            if float(informacion.get("iva")) != 0.0:
                por_desc_result = float(monto_descuento12*100)/subtotales
                valor_desc12 = (float(informacion.get("subtotal")))*(por_desc_result/100)
                iva_valor = ((float(informacion.get("subtotal"))) - valor_desc12)*IVA
                total_iva = total_iva + iva_valor
                detalle_venta.descuento_porc = por_desc_result
                detalle_venta.descuento_valor = valor_desc12
                detalle_venta.monto_iva = iva_valor
            else:
                por_desc_result_cero = float(monto_descuento0*100)/subtotales_cero
                valor_desc0 = (float(informacion.get("subtotal")))*(por_desc_result_cero/100)
                detalle_venta.descuento_porc = por_desc_result_cero
                detalle_venta.descuento_valor = valor_desc0
                detalle_venta.monto_iva = 0.0

        if controlador == 4:
            if float(informacion.get("iva")) != 0.0:
                detalle_venta.descuento_porc = float(informacion.get("porc_descuento"))
                detalle_venta.descuento_valor = (float(informacion.get("precio_u"))*float(informacion.get("cantidad")))*float(informacion.get("porc_descuento"))/100
                detalle_venta.monto_iva = float(informacion.get("subtotal"))*IVA
            else:
                detalle_venta.descuento_porc = float(informacion.get("porc_descuento"))
                detalle_venta.descuento_valor = (float(informacion.get("precio_u"))*float(informacion.get("cantidad")))*float(informacion.get("porc_descuento"))/100
                detalle_venta.monto_iva = 0.0

        if controlador == 5:
            if float(informacion.get("iva")) != 0.0:
                detalle_venta.descuento_valor = float(informacion.get("porc_descuento"))
                detalle_venta.monto_iva = float(((informacion.get("precio_u"))*(informacion.get("cantidad")) - float(informacion.get("porc_descuento")))*IVA)
            else:
                detalle_venta.descuento_valor = float(informacion.get("porc_descuento"))
                detalle_venta.monto_iva = 0.0

        if formulario.getGuiaRemision():
            parametro = 1
        else:
            parametro = 0

        if controla_stock():            # Escenario Venta de Items Controlando su Stock
            if detalle_venta.item.TipoItem() == 1:  # Item Productos
                flag_inventario = 1

                if Item_Bodega.objects.filter(status=1).filter(item=detalle_venta.item).exists():
                    item_bodega = Item_Bodega.objects.filter(status=1).get(item=detalle_venta.item)
                    cantidad_venta = float(informacion.get("cantidad"))

                    if cantidad_venta > float(item_bodega.cantidad_actual):
                        validador += 1
                        messages.error(request, u"El item: "+unicode(detalle_venta.item.nombre)+u" no posee cantidad disponible en bodega, "
                                                                                                u"debido a que la cantidad actual en bodega es: "+str(unicode(item_bodega.cantidad_actual)))

                    elif float(item_bodega.cantidad_actual) == 0.0:
                        validador += 1
                        #item = detalle_venta.item
                        #item.costo = 0.0
                        #item.fecha_actualizacion = now
                        #item.usuario_actualizacion = request.user.username
                        #item.save()
                        messages.error(request, u"El item: " +unicode(detalle_venta.item.codigo)+" - "+unicode(detalle_venta.item.nombre)
                                                + u" no posee cantidad disponible en bodega")

                else:
                    validador += 1
                    messages.error(request, u"El item: "+unicode(detalle_venta.item.codigo)+" - "+unicode(detalle_venta.item.nombre)
                                                        + u" no se encuentra registrado en bodega")

            if validador == 0:
                detalle_venta.cant_entregada = detalle_venta.cantidad

        else:               # No Controla el Stock de sus Items (Productos)

            # VENDE SIN CONSULTAR SU STOCK EN BODEGA
            if informacion.get("cant_entregada") is not None:
                detalle_venta.cant_entregada = float(informacion.get("cant_entregada"))
                total_cantidad += detalle_venta.cant_entregada

                if total_cantidad > 0.0:
                    flag_inventario = 1
                else:
                    flag_inventario = 0
            else:
                detalle_venta.cant_entregada = 0

        if validador == 0:
            detalle_venta.fecha_creacion = now
            detalle_venta.usuario_creacion = request.user.username
            detalle_venta.save()

    return validador, parametro, flag_inventario

def GuardarCuentasAsientoVentaItems(detalleform, cabecera_venta, cabecera_comprobante, flag_inventario, controlador, now, request):
    '''
    Función que registra las cuentas del item para formar el asiento contable de la Venta, además llama a la función
    GuardarCuentasItemInventario(detalleform, cabecera_comprobante, now, request, 2) si la venta mueve en inventario
    :param detalleform:
    :param cabecera_venta:
    :param cabecera_comprobante:
    :param controlador:
    :param now:
    :param request:
    :return:
    '''
    if flag_inventario == 1:    # Si se mueve el inventario
        GuardarCuentasItemInventario(detalleform, cabecera_venta, cabecera_comprobante, now, request, 2)

    #####################################################
    #         Detalle de comprobante Contable Items     #
    #####################################################
    i = 0
    a = -1
    arreglo_cuentas = []
    for form in detalleform:
        informacion2 = form.cleaned_data
        for i in range(0, len(detalleform)):
            total_subtotal_cuenta = 0.0
            id_item1 = int(informacion2.get("item"))
            item = Item.objects.get(id=id_item1)
            if not item.categoria_item.plan_cuenta_venta in arreglo_cuentas:
                arreglo_cuentas.append(item.categoria_item.plan_cuenta_venta)
                a += 1
                for lista in detalleform:
                    informacion3 = lista.cleaned_data
                    id_item2 = int(informacion3.get("item"))
                    item_lista = Item.objects.get(id=id_item2)
                    if (arreglo_cuentas[a] == item_lista.categoria_item.plan_cuenta_venta):
                        if controlador == 1 or controlador == 2 or controlador == 3:
                            total_subtotal_cuenta += float((informacion3.get("subtotal")))
                        else:
                            total_subtotal_cuenta += float((informacion3.get("cantidad"))*(informacion3.get("precio_u")))

                detalle_comp_items = Detalle_Comp_Contable()
                detalle_comp_items.cabecera_contable = cabecera_comprobante
                try:
                    detalle_comp_items.centro_costo = Centro_Costo.objects.get(id=informacion2.get("centro_costo"))
                except:
                    pass
                detalle_comp_items.plan_cuenta = arreglo_cuentas[a]
                detalle_comp_items.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
                detalle_comp_items.fecha_asiento = cabecera_comprobante.fecha
                detalle_comp_items.dbcr = "H"
                detalle_comp_items.valor = total_subtotal_cuenta
                detalle_comp_items.fecha_creacion = now
                detalle_comp_items.usuario_creacion = request.user.username
                detalle_comp_items.save()

def UpdateClienteItem(id_item, cabecera_venta, ultima_venta,  now, request):
    '''
    Función UpdateClienteItem(id_item, cabecera_venta, now, request)
    que actualiza la tabla Cliente_Item
    :param id_item:
    :param cabecera_venta:
    :param ultima_venta:
    :param now:
    :param request:
    :return:
    '''
    cliente_item = Cliente_Item()
    cliente_item.cliente = cabecera_venta.cliente
    item = Item.objects.filter(status=1).get(id=id_item)
    cliente_item.item = item
    cliente_item.fecha_ultima_venta = cabecera_venta.fecha

    cliente_item.ultima_venta = ultima_venta
    cliente_item.fecha_creacion = now
    cliente_item.fecha_actualizacion = now
    cliente_item.usuario_creacion = request.user.username
    cliente_item.usuario_actualizacion = request.user.username
    cliente_item.save()


def GuardarCuentasAsientoVenta(cabecera_comprobante, cabecera_venta,  resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request):
    '''
    Función que crea las cuentas establecidas para el asiento de la venta.
    EJ:
    CuentasxCobrar ->  D, IvaCobrado ->   H, Ice ->   H, Descuento en Venta -> D
    :param cabecera_comprobante:
    :param cabecera_venta:
    :param resultado:
    :param iva_valor:
    :param ice_valor:
    :param descuento_iva:
    :param descuento_cero:
    :param monto_descuento12:
    :param monto_descuento0:
    :param controlador:
    :param now:
    :param request:
    :return validador:
    '''
    validador = 0

    #############################################################
    #         Detalle de comprobante Contable - Clientes        #
    #############################################################
    detalle_comp_clientes = Detalle_Comp_Contable()
    detalle_comp_clientes.cabecera_contable = cabecera_comprobante
    try:
        detalle_comp_clientes.centro_costo = Centro_Costo.objects.get(id=2)
    except:
        pass
    detalle_comp_clientes.plan_cuenta = cabecera_venta.cliente.plan_cuenta
    detalle_comp_clientes.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
    detalle_comp_clientes.dbcr = 'D'
    detalle_comp_clientes.fecha_asiento = cabecera_comprobante.fecha
    detalle_comp_clientes.valor = redondeo(resultado, 2)

    detalle_comp_clientes.fecha_creacion = now
    detalle_comp_clientes.usuario_creacion = request.user.username
    detalle_comp_clientes.save()

    #############################################################
    #         Detalle de comprobante Contable - Iva_Cobrado     #
    #############################################################
    if iva_valor > 0.0:
        try:
            detalle_comp_iva_cobrado = Detalle_Comp_Contable()
            detalle_comp_iva_cobrado.cabecera_contable = cabecera_comprobante
            try:
                detalle_comp_iva_cobrado.centro_costo = Centro_Costo.objects.get(id=2)
            except:
                pass
            detalle_comp_iva_cobrado.plan_cuenta = PlanCuenta.objects.filter(status=1).get(tipo__alias=8)
            detalle_comp_iva_cobrado.fecha_asiento = cabecera_comprobante.fecha
            detalle_comp_iva_cobrado.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
            detalle_comp_iva_cobrado.dbcr = 'H'
            detalle_comp_iva_cobrado.valor = redondeo(iva_valor, 2)
            detalle_comp_iva_cobrado.fecha_creacion = now
            detalle_comp_iva_cobrado.usuario_creacion = request.user.username
            detalle_comp_iva_cobrado.save()
        except PlanCuenta.DoesNotExist:
            validador += 1
            messages.error(request, u"Error no se encuentra registrado el tipo del plan de cuenta para la "
                                    u"cuenta IVA COBRADO")

    #############################################################
    #         Detalle de comprobante Contable - ICE             #
    #############################################################
    if ice_valor > 0.0:
        try:
            detalle_comp_ice = Detalle_Comp_Contable()
            detalle_comp_ice.cabecera_contable = cabecera_comprobante
            try:
                detalle_comp_ice.centro_costo = Centro_Costo.objects.get(id=2)
            except:
                pass
            detalle_comp_ice.plan_cuenta = PlanCuenta.objects.get(status=1, tipo__alias=9)  # Impuesto a la Renta
            detalle_comp_ice.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
            detalle_comp_ice.dbcr = 'H'
            detalle_comp_ice.fecha_asiento = cabecera_comprobante.fecha
            detalle_comp_ice.valor = redondeo(ice_valor, 2)
            detalle_comp_ice.fecha_creacion = now
            detalle_comp_ice.usuario_creacion = request.user.username
            detalle_comp_ice.save()
        except PlanCuenta.DoesNotExist:
            validador += 1
            messages.error(request, u"Error no se encuentra registrado el tipo del plan de cuenta para la "
                                    u"cuenta ICE")

    #############################################################
    #         Detalle de comprobante Contable - Descuento       #
    #############################################################
    if controlador == 2 or controlador == 4 or controlador == 5:
        try:
            detalle_comp_descuento = Detalle_Comp_Contable()
            detalle_comp_descuento.cabecera_contable = cabecera_comprobante
            try:
                detalle_comp_descuento.centro_costo = Centro_Costo.objects.get(id=2)
            except:
                pass
            detalle_comp_descuento.plan_cuenta = PlanCuenta.objects.get(status=1, tipo__alias=10)
            detalle_comp_descuento.fecha_asiento = cabecera_comprobante.fecha
            detalle_comp_descuento.dbcr = 'D'

            if controlador == 2:
                detalle_comp_descuento.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
                detalle_comp_descuento.valor = redondeo((descuento_iva + descuento_cero), 2)

            if controlador == 4:
                detalle_comp_descuento.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
                detalle_comp_descuento.valor = redondeo((descuento_iva + descuento_cero), 2)

            if controlador == 5:
                detalle_comp_descuento.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
                detalle_comp_descuento.valor = redondeo((descuento_iva + descuento_cero), 2)

            detalle_comp_descuento.fecha_creacion = now
            detalle_comp_descuento.usuario_creacion = request.user.username
            detalle_comp_descuento.save()

        except PlanCuenta.DoesNotExist:
            validador += 1
            messages.error(request, u"Error no se encuentra registrado el tipo del plan de cuenta para la "
                                    u"cuenta de DESCUENTO VENTAS")

    elif controlador == 3:
        try:
            detalle_comp_descuento_monto = Detalle_Comp_Contable()
            detalle_comp_descuento_monto_cero = Detalle_Comp_Contable()
            if monto_descuento12 != 0.0:
                detalle_comp_descuento_monto.plan_cuenta = PlanCuenta.objects.get(status=1, tipo__alias=10)
                detalle_comp_descuento_monto.fecha_asiento = cabecera_comprobante.fecha
                detalle_comp_descuento_monto.dbcr = 'D'
                try:
                    detalle_comp_descuento_monto.centro_costo = Centro_Costo.objects.get(id=2)
                except:
                    pass
                detalle_comp_descuento_monto.cabecera_contable = cabecera_comprobante
                detalle_comp_descuento_monto.valor = redondeo(monto_descuento12, 2)
                detalle_comp_descuento_monto.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
                detalle_comp_descuento_monto.fecha_creacion = now
                detalle_comp_descuento_monto.usuario_creacion = request.user.username
                detalle_comp_descuento_monto.save()

            if monto_descuento0 != 0.0:
                detalle_comp_descuento_monto_cero.cabecera_contable = cabecera_comprobante
                detalle_comp_descuento_monto_cero.plan_cuenta = PlanCuenta.objects.get(status=1, tipo__alias=10)
                detalle_comp_descuento_monto_cero.fecha_asiento = cabecera_comprobante.fecha
                detalle_comp_descuento_monto_cero.dbcr = 'D'
                try:
                    detalle_comp_descuento_monto_cero.centro_costo = Centro_Costo.objects.get(id=2)
                except:
                    pass
                detalle_comp_descuento_monto_cero.valor = redondeo(monto_descuento0, 2)
                detalle_comp_descuento_monto_cero.detalle = concepto_cuenta_asiento_venta(cabecera_venta)
                detalle_comp_descuento_monto_cero.fecha_creacion = now
                detalle_comp_descuento_monto_cero.usuario_creacion = request.user.username
                detalle_comp_descuento_monto_cero.save()

        except PlanCuenta.DoesNotExist:
            validador += 1
            messages.error(request, u"Error no se encuentra registrado el tipo del plan de cuenta para la "
                                    u"cuenta de DESCUENTO VENTAS POR MONTO")

    return validador

# ANÁLIZAR Y OPTIMIZAR
def GuardarCabeceraGuiaRemision(formulario_guia, cabecera_guia_rem, cabecera_venta, now, request):
    '''
    Función para Guardar en la Cabecera Guia de Remisión y emitir la guía directamente
    desde la venta
    :param formulario_guia:
    :param cabecera_guia_rem:
    :param cabecera_venta:
    :param now:
    :param request:
    :return:
    '''
    flag = 0
    if formulario_guia.is_valid():
        cabecera_guia_rem.venta_cabecera = cabecera_venta
        cabecera_guia_rem.fecha_emi = cabecera_venta.fecha
        cabecera_guia_rem.cliente = cabecera_venta.cliente
        cabecera_guia_rem.concepto = cabecera_venta.concepto
        serie = VigenciaDocEmpresa.objects.filter(status=1).get(id=formulario_guia.getSerie())
        cabecera_guia_rem.vigencia_doc_empresa = serie

        if formulario_guia.getFechaIni() < serie.fecha_actual:
            flag += 1
            messages.error(request, u"Error en la Guia de Remisión, la fecha de inicio  no puede ser menor "
                                    u"a la fecha actual de vigencia del documento.")
        else:
            num_doc = get_num_doc(serie, formulario_guia.getFechaIni(), request)
            if num_doc != -1:
                cabecera_guia_rem.num_doc = num_doc
            else:
                flag += 1
                messages.error(request, u"Error en la Guia de Remisión, el num de documento "
                                        u"supera a la cantidad de blocks vigentes registrados.")

        cabecera_guia_rem.fecha_inicio = formulario_guia.getFechaIni()
        cabecera_guia_rem.fecha_final = formulario_guia.getFechaFin()
        cabecera_guia_rem.tipo_mov_guia_rem = TipoMovGuiaRemision.objects.get(id=1, status=1)

        ################################################################################################################
        #                            VALIDACIÓN  COMPAÑÍA (Nombre del Conductor)                                       #
        ################################################################################################################
        if formulario_guia.getCompania() is not None:
            if unicode(formulario_guia.getCompania()).isspace():
                flag += 1
                messages.error(request, u"Ingrese el nombre de la compañía en la Guía de Remisión")
            else:
                 cabecera_guia_rem.nombre_conductor = formulario_guia.getCompania()
        else:
            flag += 1

        ################################################################################################################
        #                            VALIDACIÓN   (Dirección del Conductor)                                            #
        ################################################################################################################
        if formulario_guia.getDireccionConductor() is not None:
            if unicode(formulario_guia.getDireccionConductor()).isspace():
                flag += 1
                messages.error(request, u"Ingrese la dirección del transportista en la Guía de Remisión...")
            else:
                 cabecera_guia_rem.direccion_conductor = formulario_guia.getDireccionConductor()
        else:
            flag += 1

        try:
            cabecera_guia_rem.ciudad = Ciudad.objects.filter(status=1).get(id=formulario_guia.getCiudad())
        except Ciudad.DoesNotExist:
            flag += 1
            messages.error(request, u"No encuentra registrada en el sistema la ciudad, por favor verifique")

        ################################################################################################################
        #                            VALIDACIÓN   (Dirección de la Compañía)                                           #
        ################################################################################################################
        try:
            direccion = Cliente_Direccion.objects.filter(status=1).get(id=formulario_guia.getDireccion())
            cabecera_guia_rem.direccion = direccion.descripcion+" - "+direccion.direccion1
        except Cliente_Direccion.DoesNotExist:
            flag += 1
            messages.error(request, u"Error en la Dirección de la Guía de Remisión.")

        #####################################################################################
        #                            Validación RUC                                         #
        #####################################################################################
        ruc = formulario_guia.getRuc()
        if isEmpresaPublica(ruc) or isPersonaJuridica(ruc) or isPersonaNatural(ruc):
            cabecera_guia_rem.identificacion_conductor = ruc
        else:
            flag += 1
            messages.error(request, u"Por Favor ingrese un N° de Identificación Válido en la "
                                    u"información de la Guía de Remisión.")

        if flag == 0.0:
            cabecera_guia_rem.fecha_creacion = now
            cabecera_guia_rem.usuario_creacion = request.user.username
            cabecera_guia_rem.save()
    else:
        flag += 1
        messages.error(request, u"Error en la Guía de Remisión, por favor revise que la información sea válida.")

    return flag

def GuardarDetalleGuiaRemision(detalleform, cabecera_guia_rem, now, request, tipo_venta):
    """
    function GuardarDetalleGuiaRemision(detalleform, cabecera_guia_rem, now, request)
    @ Return GuiaRemisionDetalle Object
    """
    for form in detalleform:
        detalle_guia_rem = DetalleGuiaRemision()
        informacion = form.cleaned_data
        detalle_guia_rem.guia_rem_cabecera = cabecera_guia_rem

        if tipo_venta == 1:
            try:
                item = Item.objects.filter(status=1).exclude(categoria_item__tipo_item__id=2).get(id=informacion.get("item"))
                detalle_guia_rem.item = item
            except:
                pass

        else:

            try:
                item2 = Item.objects.filter(status=1).exclude(categoria_item__tipo_item__id=2).get(id=informacion.get("id_item"))
                detalle_guia_rem.item = item2
            except:
                pass

        if informacion.get("cant_entregada") is not None and informacion.get("cantidad") is not None:
            detalle_guia_rem.cantidad = float(informacion.get("cant_entregada"))
        else:
            detalle_guia_rem.cantidad = 0

        detalle_guia_rem.fecha_creacion = now
        detalle_guia_rem.usuario_creacion = request.user.username
        detalle_guia_rem.save()

msj_ult_fecha_invent = u"La fecha ingresada es menor a la fecha de la última transacción de inventario"

def GuardarVentaInventarioCabecera(inventario_cabecera, empresa_parametro, cabecera_venta, now, request, tipo_venta):
    """
    Función GuardarVentaInventarioCabecera(inventario_cabecera, cabecera_venta, now, request, tipo_venta)
    @ Return None
    Details: Función para guardar en la cabecera_inventario
    """
    contador = 0    # Validador
    tipo_comprobante = TipoComprobante.objects.filter(status=1).get(id=15)
    inventario_cabecera.tipo_comprobante = tipo_comprobante
    inventario_cabecera.documento = cabecera_venta.documento
    inventario_cabecera.fecha_reg = cabecera_venta.fecha
    inventario_cabecera.cliente_proveedor_id = cabecera_venta.cliente.id
    num_doc = str(cabecera_venta.vigencia_doc_empresa.serie+"-"+cabecera_venta.num_documento)
    inventario_cabecera.num_doc = num_doc
    inventario_cabecera.detalle = u"Mov. Inventario Egreso por ventas: "+concepto_cuenta_asiento_venta(cabecera_venta)

    # Validar la fecha de ingreso de inventario
    if empresa_parametro.fecha_ultimo_inventario is not None:
        if inventario_cabecera.fecha_reg < empresa_parametro.fecha_ultimo_inventario:
            messages.error(request, msj_ult_fecha_invent)
            contador += 1
        else:
            inventario_cabecera.num_comp = get_num_comp(tipo_comprobante.id, inventario_cabecera.fecha_reg, False, request)
    else:
        contador += 1
        messages.error(request, u"Verifique con el administrador que la fecha de último inventario "
                                u" se encuentre registrado en el sistema")

    if inventario_cabecera.fecha_reg > datetime.datetime.now().date():
        contador += 1
        messages.error(request, u"La fecha ingresada es mayor a la fecha actual, "
                                u"por favor verifique la fecha")

    if tipo_venta == 1:
        # tipo_movimento_inventario_venta
        inventario_cabecera.tipo_mov_inventario = Tipo_Mov_Inventario.objects.filter(status=1).get(id=2)
    else:
        # tipo_mov_inventario_guia_rem_venta
        inventario_cabecera.tipo_mov_inventario = Tipo_Mov_Inventario.objects.filter(status=1).get(id=7)

    if contador == 0:
        inventario_cabecera.fecha_creacion = now
        inventario_cabecera.orden_comp = now
        inventario_cabecera.usuario_creacion = request.user.username
        inventario_cabecera.save()

        empresa_parametro.fecha_ultimo_inventario = inventario_cabecera.fecha_reg
        empresa_parametro.usuario_actualizacion = request.user.username
        empresa_parametro.fecha_actualizacion = now
        empresa_parametro.save()

    return contador


def GuardarVentaInventarioDetalle(detalleform, inventario_cabecera, now, request, tipo_venta):
    '''
    Función que me Guarda en Inventario  Detalle
    :param detalleform:
    :param inventario_cabecera:
    :param now:
    :param request:
    :param tipo_venta:
    :return:
    '''
    contador = 0
    cantidad_entrega_item = 0

    for form in detalleform:
        detalle_inventario = Inventario_Detalle()
        informacion = form.cleaned_data
        detalle_inventario.inventario_cabecera = inventario_cabecera
        item = Item.objects.filter(status=1).get(id=informacion.get("item"))

        if item.TipoItem() == 1:    # Solo Productos
            detalle_inventario.item = item
            detalle_inventario.plan_cuenta = item.categoria_item.plan_cuenta_compra

            if controla_stock():    # CONTROLA STOCK
                #############################################################################
                #   Verificar si el item se encuentra registrado en bodega                  #
                #############################################################################
                if Item_Bodega.objects.filter(status=1).filter(item=item).exists():
                    detalle_inventario.bodega = Bodega.objects.filter(status=1).get(id=1)       # Bodega de Prueba
                    bodega_obj = Item_Bodega.objects.filter(status=1).get(item=item)            # Item_bodega
                    detalle_inventario.costo = detalle_inventario.item.costo                    # Costo del item
                    cantidad_bodega = float(bodega_obj.cantidad_actual)
                    cantidad_entrega_item = float(informacion.get("cantidad"))

                    if cantidad_bodega == 0.0:  # No existe cantidad en bodega cantidad_actual= 0.0
                        contador += 1
                        messages.error(request, u"Error la Cantidad en Bodega del item: "+unicode(item.nombre)+u" "
                                                                        u"es de: "+unicode(cantidad_bodega))

                    elif cantidad_bodega > 0.0:  # Existe Cantidad disponible en bodega
                        if cantidad_bodega >= cantidad_entrega_item:
                            bodega_obj.cantidad_actual = cantidad_bodega - cantidad_entrega_item
                            bodega_obj.fecha_auctualizacion = now
                            bodega_obj.usuario_actualizacion = request.user.username
                            bodega_obj.save()
                        else:
                            contador += 1
                            messages.error(request, u"Error verifique el stock del item: "+ unicode(item.nombre))

                else:
                    contador += 1
                    messages.error(request, u"El item: "+unicode(item.nombre)+u" no se encuentra registrado en bodega")

                if contador == 0.0:
                    detalle_inventario.cantidad = cantidad_entrega_item
                    detalle_inventario.fecha_creacion = now
                    detalle_inventario.usuario_creacion = request.user.username
                    detalle_inventario.save()
                else:
                    transaction.rollback()

            else:

                # NO CONTROLA STOCK
                detalle_inventario.bodega = Bodega.objects.filter(status=1).get(id=1)       # Bodega de Prueba

                if informacion.get("cant_entregada") is not None and informacion.get("cantidad") is not None:
                    if float(informacion.get("cant_entregada")) > float(informacion.get("cantidad")):
                        raise ErrorVentas(u"Error: Verifique que la cantidad de venta con la cantidad "
                                          u"de entrega sean correctas.")
                    else:
                        detalle_inventario.cantidad = float(informacion.get("cant_entregada"))
                else:
                    detalle_inventario.cantidad = 0

                detalle_inventario.costo = detalle_inventario.item.costo

                try:
                    bodega_obj = Item_Bodega.objects.filter(status=1).get(item=item)     # Item_bodega
                    cantidad_bodega = float(bodega_obj.cantidad_actual)
                    cantidad_entrega_item = detalle_inventario.cantidad

                    if tipo_venta == 1:
                        if cantidad_bodega >= cantidad_entrega_item:
                            bodega_obj.cantidad_actual = cantidad_bodega - cantidad_entrega_item
                            bodega_obj.fecha_actualizacion = now
                            bodega_obj.usuario_actualizacion = request.user.username
                            bodega_obj.save()
                    else:
                        if cantidad_bodega >= cantidad_entrega_item:
                            bodega_obj.cantidad_actual = cantidad_bodega - cantidad_entrega_item
                            bodega_obj.fecha_actualizacion = now
                            bodega_obj.usuario_actualizacion = request.user.username
                            bodega_obj.save()
                except Item_Bodega.DoesNotExist:
                    pass
                    # ITEM NO ENCONTRADO EN BODEGA

                detalle_inventario.fecha_creacion = now
                detalle_inventario.usuario_creacion = request.user.username
                detalle_inventario.save()

def GuardarMayorInventario(cabecera_comprobante_inventario, inventario_cabecera, cabecera_venta, now, request):
    '''
    Función que Guarda la Cabecera Comprobante de Inventario
    :param cabecera_comprobante_inventario:
    :param inventario_cabecera:
    :param cabecera_venta:
    :param now:
    :param request:
    :return:
    '''
    tipo_c = TipoComprobante.objects.filter(status=1).get(id=15)    # EGRESO DE INVENTARIO
    cabecera_comprobante_inventario.tipo_comprobante = tipo_c
    cabecera_comprobante_inventario.concepto_comprobante = tipo_c.descripcion+u" desde la venta: "+\
                                                           concepto_cuenta_asiento_venta(cabecera_venta)
    cabecera_comprobante_inventario.fecha = cabecera_venta.fecha
    cabecera_comprobante_inventario.numero_comprobante = inventario_cabecera.num_comp
    cabecera_comprobante_inventario.fecha_creacion = now
    cabecera_comprobante_inventario.usuario_creacion = request.user.username
    cabecera_comprobante_inventario.save()

def GuardarCuentasItemInventario(detalleform, venta, cabecera_comprobante, now, request, tipo_venta):
    '''
    Esta función se ejecuta si el item ingresado en la venta
    realiza movimientos de inventario y genera las cuentas del asiento contable
    general de la venta o para el asiento del inventario
    Ej:
        * Cta.Comp.Inv => H y Cta.Costo.Vta => D (VENTA)
        * Cta.Comp.Inv => D y Cta.Costo.Vta => H (INVENTARIO)
    :param detalleform:
    :param cabecera_comprobante_inventario:
    :param now:
    :param request:
    :param tipo_venta:
    :return:
    '''
    #####################################################
    #         Detalle de Comprobante Contable Items     #
    #####################################################
    cuentas_item_ids_inventario = []                                    # Arreglo de cuenta de item_inventario
    cuentas_item_ids_costo_venta = []                                   # Arreglo de cuenta de item_costo_venta
    detalle_asiento_list_inventario = []                                # Arreglo de detalle asiento de cta_inventario
    detalle_asiento_list_costo_venta = []                               # Arreglo de detalle asiento de cta_costo_venta

    for form in detalleform:
        informacion2 = form.cleaned_data
        #   tipo_venta para leer el item según corresponda para la tabla de detalle guia remision y venta normal
        if tipo_venta == 1:
            item = Item.objects.get(id=informacion2.get("item"))
        else:
            item = Item.objects.get(id=informacion2.get("item"))

        if item.TipoItem() == 1:    # Producto
            # Si ya se encuentra un id nuevo de item
            if item.categoria_item.plan_cuenta_compra.id in cuentas_item_ids_inventario:
                for i in range(0, len(detalle_asiento_list_inventario)):
                    if detalle_asiento_list_inventario[i].id_cuenta == item.categoria_item.plan_cuenta_compra.id:
                        if controla_stock():
                            total_temp = detalle_asiento_list_inventario[i].costo + float(informacion2.get("cantidad")*item.costo)
                        else:
                            total_temp = detalle_asiento_list_inventario[i].costo + float(informacion2.get("cant_entregada")*item.costo)
                        detalle_asiento_list_inventario[i].costo = total_temp
            else:

                if controla_stock():    # Formset2
                    total_compra_inventario = informacion2.get("cantidad")*item.costo
                else:                   # Formset1
                    total_compra_inventario = informacion2.get("cant_entregada")*item.costo
                cuentas_item_ids_inventario.append(item.categoria_item.plan_cuenta_compra.id)
                detalle_asiento_list_inventario.append(Detalle_Cont_Inventario(item.categoria_item.plan_cuenta_compra.id, total_compra_inventario))

            # Si ya se encuentra un id nuevo de item
            if item.categoria_item.plan_cuenta_costo_venta.id in cuentas_item_ids_costo_venta:
                for i in range(0, len(detalle_asiento_list_costo_venta)):
                    if detalle_asiento_list_costo_venta[i].id_cuenta == item.categoria_item.plan_cuenta_costo_venta.id:
                        if controla_stock():    # Formset2
                            total_temp_costo_vta = detalle_asiento_list_costo_venta[i].costo + float(informacion2.get("cantidad")*item.costo)
                        else:                   # Formset1
                            total_temp_costo_vta = detalle_asiento_list_costo_venta[i].costo + float(informacion2.get("cant_entregada")*item.costo)
                        detalle_asiento_list_costo_venta[i].costo = total_temp_costo_vta
            else:

                if controla_stock():    # Formset2
                    total_costo_venta = informacion2.get("cantidad")*item.costo
                else:                   # Formset1
                    total_costo_venta = informacion2.get("cant_entregada")*item.costo
                cuentas_item_ids_costo_venta.append(item.categoria_item.plan_cuenta_costo_venta.id)
                detalle_asiento_list_costo_venta.append(Detalle_Cont_Inventario(item.categoria_item.plan_cuenta_costo_venta.id, total_costo_venta))

    for obj in detalle_asiento_list_inventario:
        detalle_asiento_inventario = Detalle_Comp_Contable()
        detalle_asiento_inventario.cabecera_contable = cabecera_comprobante
        detalle_asiento_inventario.fecha_asiento = cabecera_comprobante.fecha

        if cabecera_comprobante.tipo_comprobante_id == 2:    # Venta
            detalle_asiento_inventario.dbcr = 'H'
            detalle_asiento_inventario.detalle = concepto_cuenta_asiento_venta(venta)
        else:                    # Inventario
            detalle_asiento_inventario.detalle = cabecera_comprobante.tipo_comprobante.descripcion+u":  "+concepto_cuenta_asiento_venta(venta)
            detalle_asiento_inventario.dbcr = 'D'

        detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        detalle_asiento_inventario.valor = obj.costo
        # centro de costo
        detalle_asiento_inventario.fecha_creacion = now
        detalle_asiento_inventario.usuario_creacion = request.user.username
        detalle_asiento_inventario.save()

    for obj in detalle_asiento_list_costo_venta:
        detalle_asiento_inventario_costo = Detalle_Comp_Contable()
        detalle_asiento_inventario_costo.cabecera_contable = cabecera_comprobante
        detalle_asiento_inventario_costo.fecha_asiento = cabecera_comprobante.fecha

        if cabecera_comprobante.tipo_comprobante_id == 2:    # Venta
            detalle_asiento_inventario_costo.dbcr = 'D'
            detalle_asiento_inventario_costo.detalle = concepto_cuenta_asiento_venta(venta)
        else:                           # Inventario
            detalle_asiento_inventario_costo.dbcr = 'H'
            detalle_asiento_inventario_costo.detalle = cabecera_comprobante.tipo_comprobante.descripcion+u":  "+concepto_cuenta_asiento_venta(venta)

        detalle_asiento_inventario_costo.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
        # centro de costo
        detalle_asiento_inventario_costo.valor = obj.costo
        detalle_asiento_inventario_costo.fecha_creacion = now
        detalle_asiento_inventario_costo.usuario_creacion = request.user.username
        detalle_asiento_inventario_costo.save()


# FUNCIONES PARA EL MANTENIMIENTO DE VENTAS: CAMBIO FECHA Y REALIZA EL PROCESO DE INVENTARIO(PRODUCTOS)
class Item_Cant_Venta():
    def __init__(self, item=None, cantidad=0.0, costo=0.0):
        self.item = item
        self.cantidad = cantidad
        self.costo = costo
def reprocesarVentasInventario(request, id):
    '''
    Función que realiza el reproceso en las tablas de inventario detalle
    según la venta y recibe el num_comp
    :param request:
    :param num_comp:
    :return:
    '''
    contador = 0
    #try:
    venta_cabecera = Venta_Cabecera.objects.filter(status=1).get(id=id)
    num_doc_vta = venta_cabecera.vigencia_doc_empresa.serie+"-"+venta_cabecera.num_documento
    inventario_cabecera = Inventario_Cabecera.objects.filter(status=1).get(num_doc=num_doc_vta)

    if venta_cabecera.existe_movimientos_inventario():

        for detalle_vta in Venta_Detalle.objects.filter(status=1, venta_cabecera=venta_cabecera):
            det_inv = Inventario_Detalle.objects.filter(status=1).get(inventario_cabecera=inventario_cabecera, item=detalle_vta.item)
            det_inv.cantidad = detalle_vta.cant_entregada
            det_inv.costo = detalle_vta.item.costo
            det_inv.usuario_actualizacion = request.user.username
            det_inv.save()
    else:
        contador += 1
        messages.error(request, u"La venta seleccionada no encuentra movimientos en inventario")

    return contador
def procesoModificaFechaVenta(id, fecha_modificar, request):
    '''
    Función que realiza el procedimiento de cambiar la fecha de emisión de la venta y
    en todas las tablas correspondientes a esa transacción
    :return:
    '''
    # Update Venta_Cabecera -> Fecha
    venta_cabecera = Venta_Cabecera.objects.filter(status=1).get(id=id)
    venta_cabecera.fecha = fecha_modificar
    venta_cabecera.usuario_actualizacion = request.user.username
    venta_cabecera.fecha_actualizacion = datetime.datetime.now()
    venta_cabecera.save()

    # Update Cuentas_x_Cobrar -> Fecha
    cuentas_x_cobrar = Cuentas_por_Cobrar.objects.filter(status=1).get(venta_cabecera=venta_cabecera)
    cuentas_x_cobrar.fecha_reg = fecha_modificar
    cuentas_x_cobrar.fecha_emi = fecha_modificar
    cuentas_x_cobrar.usuario_actualizacion = request.user.username
    cuentas_x_cobrar.fecha_actualizacion = datetime.datetime.now()
    cuentas_x_cobrar.save()

    # Update Comprobante_Contable_Cabecera (VENTA) -> Fecha
    comprobante_cabecera_venta = Cabecera_Comp_Contable.objects.get(numero_comprobante=venta_cabecera.numero_comprobante)
    comprobante_cabecera_venta.fecha = fecha_modificar
    comprobante_cabecera_venta.usuario_actualizacion = request.user.username
    comprobante_cabecera_venta.fecha_actualizacion = datetime.datetime.now()
    comprobante_cabecera_venta.save()

    # Update Comprobante_Contable_Detalle (VENTA) -> Fecha
    for det_comp in Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=comprobante_cabecera_venta):
        det_comp.fecha_asiento = fecha_modificar
        det_comp.usuario_actualizacion = request.user.username
        det_comp.fecha_actualizacion = datetime.datetime.now()
        det_comp.save()

    if venta_cabecera.existe_movimientos_inventario():
        ################################################################################################################
        #                                               NOTA                                                           #
        ################################################################################################################
        # Update Inventario_Cabecera -> Fecha
        num_doc_vta = venta_cabecera.vigencia_doc_empresa.serie+"-"+venta_cabecera.num_documento

        if controla_stock():    # Si realiza la entrega y movimiento de inventario de toda la mercadería
                                # que salió al mismo instante en que se realizó la venta

            inventario_cabecera = Inventario_Cabecera.objects.filter(status=1).get(num_doc=num_doc_vta)
            inventario_cabecera.fecha_reg = fecha_modificar
            inventario_cabecera.usuario_actualizacion = request.user.username
            inventario_cabecera.fecha_actualizacion = datetime.datetime.now()
            inventario_cabecera.save()

            # Update Comprobante_Contable_Cabecera (INVENTARIO) -> Fecha
            comprobante_cabecera_inventario = Cabecera_Comp_Contable.objects.get(numero_comprobante=inventario_cabecera.num_comp)
            comprobante_cabecera_inventario.fecha = fecha_modificar
            comprobante_cabecera_inventario.usuario_actualizacion = request.user.username
            comprobante_cabecera_inventario.fecha_actualizacion = datetime.datetime.now()
            comprobante_cabecera_inventario.save()

            # Update Comprobante_Contable_Detalle (INVENTARIO) -> Fecha
            for det_comp_inv in Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=comprobante_cabecera_inventario):
                det_comp_inv.fecha_asiento = fecha_modificar
                det_comp_inv.usuario_actualizacion = request.user.username
                det_comp_inv.fecha_actualizacion = datetime.datetime.now()
                det_comp_inv.save()

        else:

            # POR IMPLEMENTAR EN EL CASO DE QUE POR UNA FACTURA SE REALICEN MÚLTIPLES MOVIMIENTOS DE INVENTARIO
            inventario_cabecera = Inventario_Cabecera.objects.filter(status=1).get(num_doc=num_doc_vta)
            inventario_cabecera.fecha_reg = fecha_modificar
            inventario_cabecera.usuario_actualizacion = request.user.username
            inventario_cabecera.fecha_actualizacion = datetime.datetime.now()
            inventario_cabecera.save()

            # Update Comprobante_Contable_Cabecera (INVENTARIO) -> Fecha
            comprobante_cabecera_inventario = Cabecera_Comp_Contable.objects.get(numero_comprobante=inventario_cabecera.num_comp)
            comprobante_cabecera_inventario.fecha = fecha_modificar
            comprobante_cabecera_inventario.usuario_actualizacion = request.user.username
            comprobante_cabecera_inventario.fecha_actualizacion = datetime.datetime.now()
            comprobante_cabecera_inventario.save()

            # Update Comprobante_Contable_Detalle (INVENTARIO) -> Fecha
            for det_comp_inv in Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=comprobante_cabecera_inventario):
                det_comp_inv.fecha_asiento = fecha_modificar
                det_comp_inv.usuario_actualizacion = request.user.username
                det_comp_inv.fecha_actualizacion = datetime.datetime.now()
                det_comp_inv.save()






