#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from contabilidad.formularios.ProveedorForm import *
from contabilidad.models import *
import datetime
from django.forms.util import ErrorList
from django.contrib.sessions.models import Session
import json
from django.db import IntegrityError, transaction
import operator
from django.db.models import Q
from django.contrib.auth.models import User
from librerias.funciones.validacion_formularios import *
from django.template.loader import render_to_string
import time
from django.http import HttpResponse
from django import template
from librerias.funciones.validacion_rucs import *
from django.db import connection
from librerias.funciones.permisos import *
from contabilidad.formularios.ContactosForm import FormContacto, ContactoFormSet, ContactoFormSetOnlyRead

__author__ = 'Roberto'

def GuardarProveedor(formulario, proveedor_obj, now, request):
    '''
    Función que realiza el proceso de guardar la información del proveedor
    :param formulario:
    :param proveedor_obj:
    :param now:
    :param request:
    :return:
    '''
    contador = 0
    proveedor_obj.tipo_identificacion = Identificacion.objects.get(codigo=formulario.getTipoIdentificacion())
    proveedor_obj.ruc = formulario.getRuc()
    proveedor_obj.nombre = formulario.getNombre()
    proveedor_obj.razon_social = formulario.getRazonSocial()
    proveedor_obj.plan_cuenta = PlanCuenta.objects.get(id=formulario.getGrupoProveedor())
    proveedor_obj.direccion = formulario.getDireccion()
    proveedor_obj.telefono = formulario.getTelefono()
    proveedor_obj.email = formulario.get_email()

    if formulario.getActividad() != "":
        proveedor_obj.actividad = Sector.objects.filter(status=1).get(id=formulario.getActividad())

    if formulario.getCiudad() != "":
        proveedor_obj.ciudad = Ciudad.objects.filter(status=1).get(id=formulario.getCiudad())


    if Proveedores.objects.filter(Q(ruc=proveedor_obj.ruc), Q(status=1) | Q(status=2)).exists():
        contador += 1
        errors = formulario._errors.setdefault("ruc", ErrorList())
        errors.append(u"El Nº de Identificación ya ha sido ingresado.")
        messages.error(request, u"Error: El Nº de Identificación ya ha sido ingresado.")

    else:

        if proveedor_obj.tipo_identificacion.codigo == "P":
            proveedor_obj.tipo_proveedor = TipoPersona.objects.get(id=formulario.getTipoProveedor())
            contador = 0

        elif proveedor_obj.tipo_identificacion.codigo == "R":

            if len(proveedor_obj.ruc) == 13:
                if isEmpresaPublica(proveedor_obj.ruc) or isPersonaJuridica(proveedor_obj.ruc):
                    proveedor_obj.tipo_proveedor = TipoPersona.objects.get(id=2)
                elif isPersonaNatural(proveedor_obj.ruc):
                    proveedor_obj.tipo_proveedor = TipoPersona.objects.get(id=1)
                else:
                    contador += 1
                    errors = formulario._errors.setdefault("ruc", ErrorList())
                    errors.append(u"El Nº de Identificación es incorrecto.")
                    messages.error(request, u"Error : El Nº de Identificación es incorrecto.")

            else:
                contador += 1
                errors = formulario._errors.setdefault("ruc", ErrorList())
                errors.append(u"El Nº de Identificación es incorrecto.")
                messages.error(request, u"Error : El Nº de Identificación es incorrecto.")


        elif proveedor_obj.tipo_identificacion.codigo == "C":

            if isCedula(proveedor_obj.ruc) and len(proveedor_obj.ruc) == 10:
                proveedor_obj.tipo_proveedor = TipoPersona.objects.get(id=1)
            else:
                contador += 1
                errors = formulario._errors.setdefault("ruc", ErrorList())
                errors.append(u"El Nº de Identificación es incorrecto.")
                messages.error(request, u"Error DATOS BÁSICOS: El Nº de Identificación es incorrecto.")

    if formulario.getFormaPago() != "":
        proveedor_obj.forma_pago = TipoPago.objects.filter(status=1).get(id=formulario.getFormaPago()) # TipoPago

        if proveedor_obj.forma_pago.id == 2:
            if formulario.getPlazo() == "" and formulario.getMontoCredito() == "":
                contador += 1
                errors = formulario._errors.setdefault("monto_credito", ErrorList())
                errors.append(u"Ingrese un monto crédito.")
                errors = formulario._errors.setdefault("plazo", ErrorList())
                errors.append(u"Ingrese un plazo.")
                messages.error(request, u"Error: Ingrese un monto de crédito y plazo")

            if formulario.getPlazo() == "" and formulario.getMontoCredito() != "":
                proveedor_obj.monto_credito = float(formulario.getMontoCredito())
                contador += 1
                errors = formulario._errors.setdefault("plazo", ErrorList())
                errors.append(u"Ingrese un plazo.")
                messages.error(request, u"Error: Ingrese un plazo")

            if formulario.getPlazo() != "" and formulario.getMontoCredito() == "":
                proveedor_obj.plazo = formulario.getPlazo()
                contador += 1

                errors = formulario._errors.setdefault("monto_credito", ErrorList())
                errors.append(u"Ingrese un monto crédito.")
                messages.error(request, u"Error: Ingrese un monto de crédito")

            if formulario.getPlazo() != "" and formulario.getMontoCredito() != "":
                proveedor_obj.plazo = formulario.getPlazo()
                try:
                    proveedor_obj.monto_credito = float(formulario.getMontoCredito())
                except:
                    pass
        else:
                proveedor_obj.monto_credito = 0.0
                proveedor_obj.plazo = ""
    else:
        proveedor_obj.monto_credito = 0.0
        proveedor_obj.plazo = ""


    if contador == 0:
        proveedor_obj.fecha_creacion = now
        proveedor_obj.usuario_creacion = request.user.username
        proveedor_obj.save()

    return contador


def EditarProveedor(formulario, nuevo, now, request):
    '''
    Función que realiza el proceso de editar la información del proveedor seleccionado
    :param formulario:
    :param nuevo:
    :param now:
    :param request:
    :return:
    '''
    contador_editar = 0
    nuevo.tipo_identificacion = Identificacion.objects.get(codigo=formulario.getTipoIdentificacion())
    nuevo.ruc = formulario.getRuc()
    nuevo.nombre = formulario.getNombre()
    nuevo.razon_social = formulario.getRazonSocial()
    nuevo.direccion = formulario.getDireccion()
    nuevo.telefono = formulario.getTelefono()
    nuevo.plan_cuenta = PlanCuenta.objects.get(id=formulario.getGrupoProveedor())
    nuevo.email = formulario.get_email()

    if formulario.getActividad() != "":
        nuevo.actividad = Sector.objects.filter(status=1).get(id=formulario.getActividad())

    if formulario.getCiudad() != "":
        nuevo.ciudad = Ciudad.objects.filter(status=1).get(id=formulario.getCiudad())

    bandera = Proveedores.objects.exclude(status=0).filter(Q(ruc=nuevo.ruc), ~Q(id=nuevo.id)).exists()


    if bandera:
        contador_editar += 1
        errors = formulario._errors.setdefault("ruc", ErrorList())
        errors.append(u"El Nº de Identificación ya ha sido ingresado.")
        messages.error(request, u"Error: El Nº de Identificación ya ha sido ingresado.")

    else:

        if nuevo.tipo_identificacion.codigo == "P":
            nuevo.tipo_proveedor = TipoPersona.objects.get(id=formulario.getTipoProveedor())
            contador_editar = 0

        elif nuevo.tipo_identificacion.codigo == "R":
            if len(nuevo.ruc) == 13:
                if isEmpresaPublica(nuevo.ruc) or isPersonaJuridica(nuevo.ruc):
                    nuevo.tipo_proveedor = TipoPersona.objects.get(id=2)
                elif isPersonaNatural(nuevo.ruc):
                    nuevo.tipo_proveedore = TipoPersona.objects.get(id=1)
                else:
                    contador_editar += 1
                    errors = formulario._errors.setdefault("ruc", ErrorList())
                    errors.append(u"El Nº de Identificación es incorrecto.")
                    messages.error(request, u"Error : El Nº de Identificación es incorrecto.")
            else:
                contador_editar += 1
                errors = formulario._errors.setdefault("ruc", ErrorList())
                errors.append(u"El Nº de Identificación es incorrecto.")
                messages.error(request, u"Error : El Nº de Identificación es incorrecto.")

        elif nuevo.tipo_identificacion.codigo == "C":
            if isCedula(nuevo.ruc) and len(nuevo.ruc) == 10:
                nuevo.tipo_proveedor = TipoPersona.objects.get(id=1)
            else:
                contador_editar += 1
                errors = formulario._errors.setdefault("ruc", ErrorList())
                errors.append(u"El Nº de Identificación es incorrecto.")
                messages.error(request, u"Error : El Nº de Identificación es incorrecto.")

    if formulario.getFormaPago() != "":
        nuevo.forma_pago = TipoPago.objects.filter(status=1).get(id=formulario.getFormaPago()) # TipoPago

        if nuevo.forma_pago.id == 2:
            if formulario.getPlazo() == "" and formulario.getMontoCredito() == "":
                contador_editar += 1
                errors = formulario._errors.setdefault("monto_credito", ErrorList())
                errors.append(u"Ingrese un monto crédito.")
                errors = formulario._errors.setdefault("plazo", ErrorList())
                errors.append(u"Ingrese un plazo.")
                messages.error(request, u"Error : Ingrese un monto de crédito y un plazo")

            if formulario.getPlazo() == "" and formulario.getMontoCredito() != "":
                nuevo.monto_credito = float(formulario.getMontoCredito())
                contador_editar += 1
                errors = formulario._errors.setdefault("plazo", ErrorList())
                errors.append(u"Ingrese un plazo.")
                messages.error(request, u"Error : Ingrese un plazo")

            if formulario.getPlazo() != "" and formulario.getMontoCredito() == "":
                nuevo.plazo = formulario.getPlazo()
                contador_editar += 1
                errors = formulario._errors.setdefault("monto_credito", ErrorList())
                errors.append(u"Ingrese un monto crédito.")
                messages.error(request, u"Error : Ingrese un monto de crédito")

            if formulario.getPlazo() != "" and formulario.getMontoCredito() != "":
                nuevo.plazo = formulario.getPlazo()
                nuevo.monto_credito = float(formulario.getMontoCredito())

        else:
                nuevo.monto_credito = 0.0
                nuevo.plazo = ""
    else:
        nuevo.monto_credito = 0.0
        nuevo.plazo = ""


    if contador_editar == 0:
        nuevo.fecha_actualizacion = now
        nuevo.usuario_actualizacion = request.user.username
        nuevo.save()

    return contador_editar

def agregar_proveedor_ajax(proveedor_cmp, formulario, request):
    """
    Función para agregar un proveedor por ajax
    :param proveedor_cmp:
    :param formulario:
    :param respuesta:
    :param request:
    :return:
    """
    proveedor_cmp.plan_cuenta = PlanCuenta.objects.get(id=formulario.getGrupoProveedor())
    proveedor_cmp.tipo_identificacion = Identificacion.objects.filter(status=1).get(codigo=formulario.getTipoIdentificacion())
    proveedor_cmp.plan_cuenta = PlanCuenta.objects.filter(status=1).get(id=formulario.getGrupoProveedor())
    cont = 0

    mensajes = []
    if formulario.getRazonSocial() == "":
        mensajes.append(u"Razón Social es obligatoria")
        cont += 1
    else:
        proveedor_cmp.razon_social = formulario.getRazonSocial()

    if formulario.getRuc() == "":
        mensajes.append(u"El número de identificación es obligatoria")
        cont += 1
    else:
        proveedor_cmp.ruc = formulario.getRuc()

    if emite_docs_electronicos():
        if formulario.get_email() == "":
            mensajes.append(u"El Email es obligatorio")
            cont += 1
        else:
            proveedor_cmp.email = formulario.get_email()

    if cont == 0:
        if Proveedores.objects.filter(Q(ruc=proveedor_cmp.ruc), Q(status=1) | Q(status=2)).exists():
            mensajes.append(u"El proveedor ya existe o está inactivo")
            cont += 1
        else:
            if proveedor_cmp.tipo_identificacion.codigo == "R" and len(proveedor_cmp.ruc) == 13:
                if isEmpresaPublica(proveedor_cmp.ruc) or isPersonaJuridica(proveedor_cmp.ruc):
                    proveedor_cmp.tipo_proveedor = TipoPersona.objects.get(codigo="02")
                elif isPersonaNatural(proveedor_cmp.ruc):
                    proveedor_cmp.tipo_proveedor = TipoPersona.objects.get(codigo="01")
                else:
                    mensajes.append(u"Error Número de identificación incorrecto")
                    cont += 1

            elif proveedor_cmp.tipo_identificacion.codigo == "C":
                if len(proveedor_cmp.ruc) == 10 and isCedula(proveedor_cmp.ruc):
                    proveedor_cmp.tipo_proveedor = TipoPersona.objects.get(codigo="01")
                else:
                    mensajes.append(u"Error Número de identificación incorrecto")
                    cont += 1

            elif proveedor_cmp.tipo_identificacion.codigo == "P":
                proveedor_cmp.tipo_proveedor = TipoPersona.objects.get(id=formulario.getTipoProveedor())

        if cont == 0:
            proveedor_cmp.fecha_creacion = datetime.datetime.now()
            proveedor_cmp.usuario_creacion = request.user.username
            proveedor_cmp.save()

            respuesta = {"status": 1, "mensaje": u"Se registró exitosamente el Proveedor", "id": proveedor_cmp.id}

            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')
        else:
            respuesta = {"status": 2, "mensaje": mensajes}
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')
    else:
        respuesta = {"status": 2, "mensaje": mensajes}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')
