#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from contabilidad.formularios.ComprasForm import *
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import transaction
from django.forms.util import ErrorList
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.core.paginator import *
from django import template
from administracion.models import *
from librerias.funciones.permisos import *
from contabilidad.formularios.CuentasxPagarForm import *
from django.template.loader import render_to_string
from librerias.funciones.paginacion import *
from librerias.funciones.funciones_vistas import *
import operator


class ErrorPagoProveedores(Exception):
    def __init__(self, valor):
        self.valor = unicode(valor)

    def __str__(self):
        return self.valor


class ErrorCruceDoc(Exception):
    def __init__(self, valor):
        self.valor = valor
    def __str__(self):
        mensaje = self.valor
        return mensaje


def busqueda_pagos(buscador):
    """
    Funcion para la búsqueda del
    modulo pagos o cuentas por pagar
    :param buscador:
    :return:
    """
    predicates = []
    if buscador.getFechaIni() is not None:
        predicates.append(('fecha_reg__gte', buscador.getFechaIni()))

    predicates.append(('fecha_reg__lte', buscador.getFechaFinal()))
    predicates.append(('num_comp__icontains', buscador.getNumComp()))
    predicates.append(('cxp_pago__cuentas_x_pagar__num_doc__istartswith', buscador.getNumDoc()))
    predicates.append(('monto__range', (buscador.getValorIni(), buscador.getValorFinal())))

    if buscador.getEstado() != "":
        predicates.append(('status', buscador.getEstado()))
    if buscador.getProveedor() is not None:
        predicates.append(('proveedor', buscador.getProveedor()))

    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]
    entries = Pago.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).distinct().order_by("-fecha_reg", "-num_comp")
    print entries.query
    return entries


def CalcularTotalPago(detalle_docs):
    """
    Función interna que sirve para
    calcular el monto a pagar en la
    vista de cobros
    :param detalle_docs:Formulario formset donde se encuentran los valores a pagar
    :return:
    """
    # Para obtener el total de los pagos
    total_pagar = 0
    total_anticipo = 0
    detalle_docs.is_valid()  # Para poder llamr a la función cleaned_data
    for form in detalle_docs:
        informacion = form.cleaned_data
        if informacion.get("id_cuenta_pagar") != -1:
            if informacion.get("valor") is not None:
                total_pagar += float(informacion.get("valor"))
        else:
            if informacion.get("valor") is not None:
                total_anticipo += float(informacion.get("valor"))

    return total_pagar, total_anticipo


def llenar_concepto_pago(cabecera, detalle_docs, proveedor):
    '''
    Función interna que me genera un concepto standar propio de clipmaestro
    :param cabecera:
    :param detalle_docs:
    :param proveedor:
    :return:
    '''
    if cabecera.getObservacion() == "" or cabecera.getObservacion().isspace():
        facturas_pagadas = ""
        cont_anticipo = 0.0

        if detalle_docs.is_valid():
            for form in detalle_docs:
                informacion = form.cleaned_data
                if informacion.get("id_cuenta_pagar") != -1:
                    try:
                        valor = float(informacion.get("valor"))
                    except TypeError:
                        valor = 0
                    if valor > 0:
                        facturas_pagadas += informacion.get("numero_doc") + " "
                else:
                    try:
                        valor = float(informacion.get("valor"))
                    except TypeError:
                        valor = 0
                    if valor > 0:
                        cont_anticipo += valor
        if facturas_pagadas != "":
            concepto = u"Pago/Proveedor: " + proveedor.razon_social + u" #Doc: " + facturas_pagadas
        else:
            if cont_anticipo > 0:
                concepto = u"Anticipo/Proveedor: " + proveedor.razon_social + u" por un valor de: $" + str(cont_anticipo)
            else:
                concepto = u"Pago/Proveedor: " + proveedor.razon_social
    else:
        concepto = cabecera.getObservacion()

    return concepto


def GuardarPago(pago, asiento, cabecera, forma_de_pago, detalle_docs, total_pagar, total_anticipo, contador, now, request):
    """
    Función interna que sirve para guardar en base
    la tabla de pago, dependiendo la forma de pago
    se guarda tambien en la tabla Bancos que es donde
    se registran las transacciones bancarias
    :param pago:
    :param cabecera:
    :param cursor:
    :param proveedor:
    :param forma_de_pago:
    :param total_pagar:
    :param total_anticipo:
    :param now:
    :param request:
    :return:
    """

    try:
        proveedor = Proveedores.objects.get(id=cabecera.getProveedor(), status=1)
        pago.proveedor = proveedor
        pago.monto = redondeo(total_pagar + total_anticipo)
        forma_pago = forma_de_pago.getFormaPago()
        pago.forma_pago = FormaPago.objects.get(id=forma_pago)
        forma_de_pago.is_valid()  # Para que lo reconosca como objeto

        # Si no ponen nada en concepto
        concepto = llenar_concepto_pago(cabecera, detalle_docs, proveedor)

        pago.concepto = concepto[0:250]
        pago.fecha_reg = forma_de_pago.get_fecha()

        if forma_pago in ("1", "4"):  # Cheque o transferencia bancaria
            cuenta_banco = Cuenta_Banco.objects.get(id=int(forma_de_pago.getBanco()), status=1)
            plan_cuenta = cuenta_banco.plan_cuenta
            if forma_pago == "1":  # Egreso - Cheque
                pago.tipo_comprobante = TipoComprobante.objects.get(id=4)
                if forma_de_pago.getFechaCheque() != "":
                    pago.fecha_pago = forma_de_pago.getFechaCheque()
                else:
                    contador += 1
                    messages.error(request, u"La fecha del cheque es obligatoria")
                    errors = forma_de_pago._errors.setdefault("fecha_cheque", ErrorList())
                    errors.append(u"La fecha del cheque es obligatoria")
                # Si es cheque provisionado
                if not forma_de_pago.es_provisionado():
                    banco = Banco()
                    num_comp = get_num_comp(pago.tipo_comprobante_id, pago.fecha_reg, True, request)

                    now = datetime.datetime.now()
                    try:
                        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=4)  # Secuencia de Compras
                        fecha_comp = secuencia.fecha_actual
                    except SecuenciaTipoComprobante.DoesNotExist:
                        fecha_comp = now.strftime("%Y-%m-%d")

                    if num_comp == -1:
                        contador += 1
                        messages.error(request, u"La fecha de registro ingresada es menor a la última fecha de registro de pago "
                                                u"por favor verifique "+str(fecha_comp))
                        errors = forma_de_pago._errors.setdefault("fecha", ErrorList())
                        errors.append(u"La fecha de registro ingresada es menor a la última fecha de registro de pago "
                                                u"por favor verifique")
                        pago.num_comp = ""
                    else:
                        pago.num_comp = num_comp


                    pago.num_cheque = get_next_cheque(cuenta_banco, request)
                    banco.num_cheque = pago.num_cheque
                    banco.num_comp = pago.num_comp
                    banco.fecha_reg = pago.fecha_reg
                    banco.fecha_cheque = pago.fecha_pago
                    banco.cuenta_banco = cuenta_banco
                    banco.forma_pago = FormaPago.objects.get(id=forma_de_pago.getFormaPago())
                    banco.tipo_comprobante = pago.tipo_comprobante
                    banco.naturaleza = 2
                    banco.persona = pago.proveedor.razon_social
                    banco.valor = pago.monto
                    banco.fecha_creacion = now
                    banco.usuario_creacion = request.user.username
                    banco.concepto = pago.concepto
                    banco.save()

                else:   # Cheque provisionado

                    try:
                        banco = Banco.objects.get(id=forma_de_pago.get_lista_cheques_prov(), status=3)
                        pago.fecha_reg = banco.fecha_reg
                        pago.num_comp = banco.num_comp
                        pago.num_cheque = banco.num_cheque
                        banco.num_comp = pago.num_comp
                        banco.status = 1

                        banco.fecha_cheque = pago.fecha_pago
                        banco.cuenta_banco = cuenta_banco
                        banco.forma_pago = FormaPago.objects.get(id=forma_de_pago.getFormaPago())
                        banco.tipo_comprobante = pago.tipo_comprobante
                        banco.naturaleza = 2
                        banco.persona = pago.proveedor.razon_social
                        banco.valor = pago.monto
                        banco.fecha_creacion = now
                        banco.usuario_creacion = request.user.username
                        banco.concepto = pago.concepto
                        banco.save()
                    except Banco.DoesNotExist:
                        contador += 1
                        messages.error(request, u"El cheque provisionado que eligió no existe o ya ha sido utilizado")

            else:  # Transferencia
                pago.tipo_comprobante = TipoComprobante.objects.get(id=23)
                pago.referencia = forma_de_pago.getReferencia()
                num_comp = get_num_comp(pago.tipo_comprobante_id, pago.fecha_reg, True, request)
                if num_comp == -1:
                    contador += 1
                    now = datetime.datetime.now()
                    try:
                        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=23)  # Secuencia de Compras
                        fecha_comp = secuencia.fecha_actual
                    except SecuenciaTipoComprobante.DoesNotExist:
                        fecha_comp = now.strftime("%Y-%m-%d")
                    messages.error(request, u"La fecha de registro ingresada es menor a la última fecha de registro de pago "+str(fecha_comp))
                    errors = forma_de_pago._errors.setdefault("fecha", ErrorList())
                    errors.append(u"La fecha de registro ingresada es menor a la última fecha de registro de pago "
                                            u"por favor verifique")
                    pago.num_comp = ""
                else:
                    pago.num_comp = num_comp

                banco = Banco()
                banco.num_comp = pago.num_comp
                banco.num_referencia = pago.referencia
                banco.fecha_reg = pago.fecha_reg
                banco.cuenta_banco = cuenta_banco
                banco.forma_pago = FormaPago.objects.get(id=forma_de_pago.getFormaPago())
                banco.tipo_comprobante = pago.tipo_comprobante
                banco.naturaleza = 2
                banco.persona = pago.proveedor.razon_social
                banco.valor = pago.monto
                banco.fecha_creacion = now
                banco.usuario_creacion = request.user.username
                banco.concepto = pago.concepto
                banco.save()

        elif forma_pago in ("2", "3"):  # Pago en Efectivo y Tarjeta de Crédito
            pago.tipo_comprobante = TipoComprobante.objects.get(id=9)  # Pago
            num_comp = get_num_comp(pago.tipo_comprobante_id, pago.fecha_reg, True, request)

            if num_comp == -1:
                contador += 1
                now = datetime.datetime.now()
                try:
                    secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=9)  # Secuencia de Compras
                    fecha_comp = secuencia.fecha_actual
                except SecuenciaTipoComprobante.DoesNotExist:
                    fecha_comp = now.strftime("%Y-%m-%d")

                messages.error(request, u"La fecha de registro ingresada es menor a la última fecha de registro de pago "+str(fecha_comp))
            else:
                pago.num_comp = num_comp

            if forma_de_pago.getFormaPago() == "2":     # Pago en Efectivo
                pago.referencia = forma_de_pago.getReferencia()
                try:
                    # Validar que haya ingresado un plan de cuenta de caja chica válida
                    plan_cuenta = PlanCuenta.objects.get(id=forma_de_pago.getCajaChica(), status=1)
                except (PlanCuenta.DoesNotExist, ValueError):
                    contador += 1
                    plan_cuenta = None
                    errors = cabecera._errors.setdefault("caja", ErrorList())
                    errors.append(u"Seleccione una cuenta de caja chica válida")
                    messages.error(request, u"Seleccione una cuenta de caja chica válida")

            elif forma_de_pago.getFormaPago() == "3":  # Pago con Tarjeta de Crédito
                if forma_de_pago.getInteres() == "" or forma_de_pago.getTarjetas() == "" or forma_de_pago.getPlazo() == "":
                    contador += 1
                    messages.error(request, u"Por favor llene todos los campos de la forma de pago")
                    plan_cuenta = None
                else:
                    pago.interes = forma_de_pago.getInteres()
                    plan_cuenta = Tarjeta_Credito(id=forma_de_pago.getTarjetas()).plan_cuenta
                    if forma_de_pago.getTermino() == "2":
                        pago.plazo_diferido = forma_de_pago.getPlazo()
            else:
                plan_cuenta = None
                messages.error(request, u"Opción no válida, por favor verifique")
                contador += 1
        else:
            plan_cuenta = None
            messages.error(request, u"Opción no válida, por favor verifique")
            contador += 1

        pago.fecha_creacion = now
        pago.usuario_creacion = request.user.username
        pago.save()

        # Validar las fechas del pago
        if pago.fecha_reg > now.date():
            contador += 1
            messages.error(request, u"La fecha de registro es mayor a la fecha actual")
            plan_cuenta = None
        if pago.fecha_reg > now.date():
            contador += 1
            messages.error(request, u"La fecha de registro es mayor a la fecha actual")

        ##################################
        #   Guardar Cabecera de Asiento  #
        ##################################

        asiento.concepto_comprobante = pago.concepto

        # Si no hay errores guardar en base
        if contador == 0:
            asiento.fecha = pago.fecha_reg
            asiento.tipo_comprobante = pago.tipo_comprobante
            asiento.numero_comprobante = pago.num_comp

            asiento.fecha_creacion = now
            asiento.usuario_creacion = request.user.username
            asiento.save()

            ##################################
            #   Guardar Asiento  Proveedor   #
            ##################################
            if total_pagar > 0:  # Podira solo haber hecho un anticipo
                detalle_asiento1 = Detalle_Comp_Contable()
                detalle_asiento1.cabecera_contable = asiento
                detalle_asiento1.dbcr = "D"
                detalle_asiento1.plan_cuenta = proveedor.plan_cuenta  # Plan de Cuenta Proveedores
                detalle_asiento1.detalle = pago.concepto
                detalle_asiento1.valor = redondeo(total_pagar)
                detalle_asiento1.fecha_asiento = asiento.fecha

                detalle_asiento1.fecha_creacion = now
                detalle_asiento1.usuario_creacion = request.user.username
                detalle_asiento1.save()

            ############################
            #   Guardar Asiento Pago   #
            ############################
            detalle_asiento2 = Detalle_Comp_Contable()
            detalle_asiento2.cabecera_contable = asiento
            detalle_asiento2.dbcr = "H"
            detalle_asiento2.plan_cuenta = plan_cuenta
            detalle_asiento2.fecha_asiento = asiento.fecha
            detalle_asiento2.valor = pago.monto
            detalle_asiento2.detalle = pago.concepto

            detalle_asiento2.fecha_creacion = now
            detalle_asiento2.usuario_creacion = request.user.username
            detalle_asiento2.save()

            if detalle_docs.is_valid():
                for form in detalle_docs:
                    informacion = form.cleaned_data
                    try:
                        valor = float(informacion.get("valor"))
                    except TypeError:
                        valor = 0
                    id = informacion.get("id_cuenta_pagar", "")

                    if id == -1:
                        ###################################
                        #   Asiento para el anticipo      #
                        ###################################
                        if valor > 0:
                            detalle_asiento3 = Detalle_Comp_Contable()
                            cuenta_pagar_anticipo = Cuentas_por_Pagar()
                            contador += GuardarAnticipo(detalle_asiento3, cuenta_pagar_anticipo, valor, pago,
                                                        asiento, total_anticipo, now, contador, request)
                    else:
                        if valor > 0:
                            cuenta_pagar = Cuentas_por_Pagar.objects.get(id=id)
                            if cuenta_pagar.status != 1:
                                messages.error(request, u"La cuenta por pagar que quiso utilizar estaba anulada al "
                                                           u"momento de realizar el pago.")
                                contador += 1
                            if redondeo(cuenta_pagar.pagado + valor) > redondeo(cuenta_pagar.monto):
                                messages.error(request, u"El valor del pago a la factura: " + cuenta_pagar.num_doc +
                                                           u", excede su monto")
                                contador += 1
                            else:
                                cuenta_pagar.pagado = redondeo(cuenta_pagar.pagado + valor)
                            #15 octubre estas2 validaciones son porque a veces el form trae fecha cheque y a veces no :o
                            if forma_de_pago.getFechaCheque():
                                if cuenta_pagar.fecha_reg > forma_de_pago.getFechaCheque():#forma_de_pago.get_fecha(): cambio 14 octubre
                                    messages.error(request, u"La fecha de registro del pago es menor a la fecha de registro "
                                                            u"del documento a pagar")
                                    errors1 = forma_de_pago._errors.setdefault("fecha", ErrorList())
                                    errors1.append(u"Error")

                                    errors2 = form._errors.setdefault("fecha_reg", ErrorList())
                                    errors2.append(u"Error")
                                    contador += 1
                                print 'existe la fecha de cheque'
                            if forma_de_pago.get_fecha():
                                if cuenta_pagar.fecha_reg > forma_de_pago.get_fecha():#forma_de_pago.get_fecha(): cambio 14 octubre
                                    messages.error(request, u"La fecha de registro del pago es menor a la fecha de registro "
                                                            u"del documento a pagar")
                                    errors1 = forma_de_pago._errors.setdefault("fecha", ErrorList())
                                    errors1.append(u"Error")

                                    errors2 = form._errors.setdefault("fecha_reg", ErrorList())
                                    errors2.append(u"Error")
                                    contador += 1
                                print 'existe la fecha normal'
                            '''
                            if cuenta_pagar.fecha_reg > forma_de_pago.getFechaCheque() or cuenta_pagar.fecha_reg > forma_de_pago.get_fecha():#forma_de_pago.get_fecha(): cambio 14 octubre
                                messages.error(request, u"La fecha de registro del pago es menor a la fecha de registro "
                                                        u"del documento a pagar")
                                errors1 = forma_de_pago._errors.setdefault("fecha", ErrorList())
                                errors1.append(u"Error")

                                errors2 = form._errors.setdefault("fecha_reg", ErrorList())
                                errors2.append(u"Error")
                                contador += 1
                            '''
                            cuenta_pagar.usuario_actualizacion = request.user.username
                            cuenta_pagar.fecha_actualizacion = now
                            cuenta_pagar.save()

                            cxp_pago = CXP_pago()
                            cxp_pago.monto = redondeo(valor)
                            cxp_pago.concepto = u"Pago a "+cuenta_pagar.documento.descripcion_documento +\
                                                u" #"+cuenta_pagar.num_doc + u" por: $" + "%.2f" % valor
                            cxp_pago.cuentas_x_pagar = cuenta_pagar
                            cxp_pago.pago = pago
                            cxp_pago.plan_cuenta = plan_cuenta
                            cxp_pago.naturaleza = 1

                            cxp_pago.fecha_creacion = now
                            cxp_pago.usuario_creacion = request.user.username
                            cxp_pago.save()
            else:
                contador += 1
                transaction.rollback()
                messages.error(request, u"Por favor llene los campos obligatorios de la forma de pago")

        return contador
    except Proveedores.DoesNotExist:
        messages.error(request, u"El proveedor al cual ha hecho referencia se encuentra inactivo o no"
                                    u"se tiene registros de ese proveedor, si tiene alguna duda, comuníquese con "
                                    u"el administrador del sistema")
        return HttpResponseRedirect(reverse("lista_pago_proveedores"))



def GuardarPago_16oct(pago, asiento, cabecera, forma_de_pago, detalle_docs, total_pagar, total_anticipo, contador, now, request):
    """
    Función interna que sirve para guardar en base
    la tabla de pago, dependiendo la forma de pago
    se guarda tambien en la tabla Bancos que es donde
    se registran las transacciones bancarias
    :param pago:
    :param cabecera:
    :param cursor:
    :param proveedor:
    :param forma_de_pago:
    :param total_pagar:
    :param total_anticipo:
    :param now:
    :param request:
    :return:
    """

    try:
        proveedor = Proveedores.objects.get(id=cabecera.getProveedor(), status=1)
        pago.proveedor = proveedor
        pago.monto = redondeo(total_pagar + total_anticipo)
        forma_pago = forma_de_pago.getFormaPago()
        pago.forma_pago = FormaPago.objects.get(id=forma_pago)
        forma_de_pago.is_valid()  # Para que lo reconosca como objeto

        # Si no ponen nada en concepto
        concepto = llenar_concepto_pago(cabecera, detalle_docs, proveedor)

        pago.concepto = concepto[0:250]
        pago.fecha_reg = forma_de_pago.get_fecha()

        if forma_pago in ("1", "4"):  # Cheque o transferencia bancaria
            cuenta_banco = Cuenta_Banco.objects.get(id=int(forma_de_pago.getBanco()), status=1)
            plan_cuenta = cuenta_banco.plan_cuenta
            if forma_pago == "1":  # Egreso - Cheque
                pago.tipo_comprobante = TipoComprobante.objects.get(id=4)
                if forma_de_pago.getFechaCheque() != "":
                    pago.fecha_pago = forma_de_pago.getFechaCheque()
                else:
                    contador += 1
                    messages.error(request, u"La fecha del cheque es obligatoria")
                    errors = forma_de_pago._errors.setdefault("fecha_cheque", ErrorList())
                    errors.append(u"La fecha del cheque es obligatoria")
                # Si es cheque provisionado
                if not forma_de_pago.es_provisionado():
                    banco = Banco()
                    num_comp = get_num_comp(pago.tipo_comprobante_id, pago.fecha_reg, True, request)

                    now = datetime.datetime.now()
                    try:
                        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=4)  # Secuencia de Compras
                        fecha_comp = secuencia.fecha_actual
                    except SecuenciaTipoComprobante.DoesNotExist:
                        fecha_comp = now.strftime("%Y-%m-%d")

                    if num_comp == -1:
                        contador += 1
                        messages.error(request, u"La fecha de registro ingresada es menor a la última fecha de registro de pago "
                                                u"por favor verifique "+str(fecha_comp))
                        errors = forma_de_pago._errors.setdefault("fecha", ErrorList())
                        errors.append(u"La fecha de registro ingresada es menor a la última fecha de registro de pago "
                                                u"por favor verifique")
                        pago.num_comp = ""
                    else:
                        pago.num_comp = num_comp


                    pago.num_cheque = get_next_cheque(cuenta_banco, request)
                    banco.num_cheque = pago.num_cheque
                    banco.num_comp = pago.num_comp
                    banco.fecha_reg = pago.fecha_reg
                    banco.fecha_cheque = pago.fecha_pago
                    banco.cuenta_banco = cuenta_banco
                    banco.forma_pago = FormaPago.objects.get(id=forma_de_pago.getFormaPago())
                    banco.tipo_comprobante = pago.tipo_comprobante
                    banco.naturaleza = 2
                    banco.persona = pago.proveedor.razon_social
                    banco.valor = pago.monto
                    banco.fecha_creacion = now
                    banco.usuario_creacion = request.user.username
                    banco.concepto = pago.concepto
                    banco.save()

                else:   # Cheque provisionado

                    try:
                        banco = Banco.objects.get(id=forma_de_pago.get_lista_cheques_prov(), status=3)
                        pago.fecha_reg = banco.fecha_reg
                        pago.num_comp = banco.num_comp
                        pago.num_cheque = banco.num_cheque
                        banco.num_comp = pago.num_comp
                        banco.status = 1

                        banco.fecha_cheque = pago.fecha_pago
                        banco.cuenta_banco = cuenta_banco
                        banco.forma_pago = FormaPago.objects.get(id=forma_de_pago.getFormaPago())
                        banco.tipo_comprobante = pago.tipo_comprobante
                        banco.naturaleza = 2
                        banco.persona = pago.proveedor.razon_social
                        banco.valor = pago.monto
                        banco.fecha_creacion = now
                        banco.usuario_creacion = request.user.username
                        banco.concepto = pago.concepto
                        banco.save()
                    except Banco.DoesNotExist:
                        contador += 1
                        messages.error(request, u"El cheque provisionado que eligió no existe o ya ha sido utilizado")

            else:  # Transferencia
                pago.tipo_comprobante = TipoComprobante.objects.get(id=23)
                pago.referencia = forma_de_pago.getReferencia()
                num_comp = get_num_comp(pago.tipo_comprobante_id, pago.fecha_reg, True, request)
                if num_comp == -1:
                    contador += 1
                    now = datetime.datetime.now()
                    try:
                        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=23)  # Secuencia de Compras
                        fecha_comp = secuencia.fecha_actual
                    except SecuenciaTipoComprobante.DoesNotExist:
                        fecha_comp = now.strftime("%Y-%m-%d")
                    messages.error(request, u"La fecha de registro ingresada es menor a la última fecha de registro de pago "+str(fecha_comp))
                    errors = forma_de_pago._errors.setdefault("fecha", ErrorList())
                    errors.append(u"La fecha de registro ingresada es menor a la última fecha de registro de pago "
                                            u"por favor verifique")
                    pago.num_comp = ""
                else:
                    pago.num_comp = num_comp

                banco = Banco()
                banco.num_comp = pago.num_comp
                banco.num_referencia = pago.referencia
                banco.fecha_reg = pago.fecha_reg
                banco.cuenta_banco = cuenta_banco
                banco.forma_pago = FormaPago.objects.get(id=forma_de_pago.getFormaPago())
                banco.tipo_comprobante = pago.tipo_comprobante
                banco.naturaleza = 2
                banco.persona = pago.proveedor.razon_social
                banco.valor = pago.monto
                banco.fecha_creacion = now
                banco.usuario_creacion = request.user.username
                banco.concepto = pago.concepto
                banco.save()

        elif forma_pago in ("2", "3"):  # Pago en Efectivo y Tarjeta de Crédito
            pago.tipo_comprobante = TipoComprobante.objects.get(id=9)  # Pago
            num_comp = get_num_comp(pago.tipo_comprobante_id, pago.fecha_reg, True, request)

            if num_comp == -1:
                contador += 1
                now = datetime.datetime.now()
                try:
                    secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=9)  # Secuencia de Compras
                    fecha_comp = secuencia.fecha_actual
                except SecuenciaTipoComprobante.DoesNotExist:
                    fecha_comp = now.strftime("%Y-%m-%d")

                messages.error(request, u"La fecha de registro ingresada es menor a la última fecha de registro de pago "+str(fecha_comp))
            else:
                pago.num_comp = num_comp

            if forma_de_pago.getFormaPago() == "2":     # Pago en Efectivo
                pago.referencia = forma_de_pago.getReferencia()
                try:
                    # Validar que haya ingresado un plan de cuenta de caja chica válida
                    plan_cuenta = PlanCuenta.objects.get(id=forma_de_pago.getCajaChica(), status=1)
                except (PlanCuenta.DoesNotExist, ValueError):
                    contador += 1
                    plan_cuenta = None
                    errors = cabecera._errors.setdefault("caja", ErrorList())
                    errors.append(u"Seleccione una cuenta de caja chica válida")
                    messages.error(request, u"Seleccione una cuenta de caja chica válida")

            elif forma_de_pago.getFormaPago() == "3":  # Pago con Tarjeta de Crédito
                if forma_de_pago.getInteres() == "" or forma_de_pago.getTarjetas() == "" or forma_de_pago.getPlazo() == "":
                    contador += 1
                    messages.error(request, u"Por favor llene todos los campos de la forma de pago")
                    plan_cuenta = None
                else:
                    pago.interes = forma_de_pago.getInteres()
                    plan_cuenta = Tarjeta_Credito(id=forma_de_pago.getTarjetas()).plan_cuenta
                    if forma_de_pago.getTermino() == "2":
                        pago.plazo_diferido = forma_de_pago.getPlazo()
            else:
                plan_cuenta = None
                messages.error(request, u"Opción no válida, por favor verifique")
                contador += 1
        else:
            plan_cuenta = None
            messages.error(request, u"Opción no válida, por favor verifique")
            contador += 1

        pago.fecha_creacion = now
        pago.usuario_creacion = request.user.username
        pago.save()

        # Validar las fechas del pago
        if pago.fecha_reg > now.date():
            contador += 1
            messages.error(request, u"La fecha de registro es mayor a la fecha actual")
            plan_cuenta = None
        if pago.fecha_reg > now.date():
            contador += 1
            messages.error(request, u"La fecha de registro es mayor a la fecha actual")

        ##################################
        #   Guardar Cabecera de Asiento  #
        ##################################

        asiento.concepto_comprobante = pago.concepto

        # Si no hay errores guardar en base
        if contador == 0:
            asiento.fecha = pago.fecha_reg
            asiento.tipo_comprobante = pago.tipo_comprobante
            asiento.numero_comprobante = pago.num_comp

            asiento.fecha_creacion = now
            asiento.usuario_creacion = request.user.username
            asiento.save()

            ##################################
            #   Guardar Asiento  Proveedor   #
            ##################################
            if total_pagar > 0:  # Podira solo haber hecho un anticipo
                detalle_asiento1 = Detalle_Comp_Contable()
                detalle_asiento1.cabecera_contable = asiento
                detalle_asiento1.dbcr = "D"
                detalle_asiento1.plan_cuenta = proveedor.plan_cuenta  # Plan de Cuenta Proveedores
                detalle_asiento1.detalle = pago.concepto
                detalle_asiento1.valor = redondeo(total_pagar)
                detalle_asiento1.fecha_asiento = asiento.fecha

                detalle_asiento1.fecha_creacion = now
                detalle_asiento1.usuario_creacion = request.user.username
                detalle_asiento1.save()

            ############################
            #   Guardar Asiento Pago   #
            ############################
            detalle_asiento2 = Detalle_Comp_Contable()
            detalle_asiento2.cabecera_contable = asiento
            detalle_asiento2.dbcr = "H"
            detalle_asiento2.plan_cuenta = plan_cuenta
            detalle_asiento2.fecha_asiento = asiento.fecha
            detalle_asiento2.valor = pago.monto
            detalle_asiento2.detalle = pago.concepto

            detalle_asiento2.fecha_creacion = now
            detalle_asiento2.usuario_creacion = request.user.username
            detalle_asiento2.save()

            if detalle_docs.is_valid():
                for form in detalle_docs:
                    informacion = form.cleaned_data
                    try:
                        valor = float(informacion.get("valor"))
                    except TypeError:
                        valor = 0
                    id = informacion.get("id_cuenta_pagar", "")

                    if id == -1:
                        ###################################
                        #   Asiento para el anticipo      #
                        ###################################
                        if valor > 0:
                            detalle_asiento3 = Detalle_Comp_Contable()
                            cuenta_pagar_anticipo = Cuentas_por_Pagar()
                            contador += GuardarAnticipo(detalle_asiento3, cuenta_pagar_anticipo, valor, pago,
                                                        asiento, total_anticipo, now, contador, request)
                    else:
                        if valor > 0:
                            cuenta_pagar = Cuentas_por_Pagar.objects.get(id=id)
                            if cuenta_pagar.status != 1:
                                messages.error(request, u"La cuenta por pagar que quiso utilizar estaba anulada al "
                                                           u"momento de realizar el pago.")
                                contador += 1
                            if redondeo(cuenta_pagar.pagado + valor) > redondeo(cuenta_pagar.monto):
                                messages.error(request, u"El valor del pago a la factura: " + cuenta_pagar.num_doc +
                                                           u", excede su monto")
                                contador += 1
                            else:
                                cuenta_pagar.pagado = redondeo(cuenta_pagar.pagado + valor)

                            if cuenta_pagar.fecha_reg > forma_de_pago.get_fecha():
                                messages.error(request, u"La fecha de registro del pago es menor a la fecha de registro "
                                                        u"del documento a pagar")
                                errors1 = forma_de_pago._errors.setdefault("fecha", ErrorList())
                                errors1.append(u"Error")

                                errors2 = form._errors.setdefault("fecha_reg", ErrorList())
                                errors2.append(u"Error")
                                contador += 1

                            cuenta_pagar.usuario_actualizacion = request.user.username
                            cuenta_pagar.fecha_actualizacion = now
                            cuenta_pagar.save()

                            cxp_pago = CXP_pago()
                            cxp_pago.monto = redondeo(valor)
                            cxp_pago.concepto = u"Pago a "+cuenta_pagar.documento.descripcion_documento +\
                                                u" #"+cuenta_pagar.num_doc + u" por: $" + "%.2f" % valor
                            cxp_pago.cuentas_x_pagar = cuenta_pagar
                            cxp_pago.pago = pago
                            cxp_pago.plan_cuenta = plan_cuenta
                            cxp_pago.naturaleza = 1

                            cxp_pago.fecha_creacion = now
                            cxp_pago.usuario_creacion = request.user.username
                            cxp_pago.save()
            else:
                contador += 1
                transaction.rollback()
                messages.error(request, u"Por favor llene los campos obligatorios de la forma de pago")

        return contador
    except Proveedores.DoesNotExist:
        messages.error(request, u"El proveedor al cual ha hecho referencia se encuentra inactivo o no"
                                    u"se tiene registros de ese proveedor, si tiene alguna duda, comuníquese con "
                                    u"el administrador del sistema")
        return HttpResponseRedirect(reverse("lista_pago_proveedores"))


def GuardarAnticipo(detalle_asiento, cuenta_pagar_anticipo, valor, pago, asiento, total_anticipo, now, contador, request):
    """
    Guarda el asiento del anticipo
    :param detalle_asiento:
    :param cuenta_pagar_anticipo:
    :param valor:
    :param pago:
    :param asiento:
    :param total_anticipo:
    :param now:
    :param request:
    :return:
    """
    detalle_asiento.dbcr = "D"
    detalle_asiento.valor = redondeo(valor)
    detalle_asiento.detalle = "Anticipo a proveedor: " + pago.proveedor.razon_social + \
                              " por la cantidad de: " + str(redondeo(valor))
    try:
        detalle_asiento.plan_cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=14))  # Anticipo a Proveedores
        detalle_asiento.cabecera_contable = asiento

        detalle_asiento.usuario_creacion = request.user.username
        detalle_asiento.usuario_actualizacion = request.user.username
        detalle_asiento.fecha_creacion = now
        detalle_asiento.fecha_asiento = pago.fecha_reg
        detalle_asiento.fecha_actualizacion = now
        detalle_asiento.save()

        cuenta_pagar_anticipo.monto = redondeo(total_anticipo)
        cuenta_pagar_anticipo.proveedor = pago.proveedor
        cuenta_pagar_anticipo.documento = Documento.objects.get(codigo_documento="AN", status=1)
        cuenta_pagar_anticipo.pagado = 0
        cuenta_pagar_anticipo.fecha_reg = pago.fecha_reg
        cuenta_pagar_anticipo.num_doc = pago.num_comp
        cuenta_pagar_anticipo.naturaleza = 1
        cuenta_pagar_anticipo.usuario_creacion = request.user.username
        cuenta_pagar_anticipo.fecha_creacion = now
        cuenta_pagar_anticipo.save()

        cxp_pago = CXP_pago()
        cxp_pago.monto = redondeo(valor)
        cxp_pago.anticipo = redondeo(valor)
        cxp_pago.cuentas_x_pagar = cuenta_pagar_anticipo
        cxp_pago.naturaleza = 2
        cxp_pago.pago = pago
        cxp_pago.plan_cuenta = PlanCuenta.objects.get(tipo=TipoCuenta.objects.get(id=14))  # Anticipo a Proveedores
        cxp_pago.concepto = "Anticipo a proveedor: " + pago.proveedor.razon_social[0:20] + " # : " + pago.proveedor.ruc + " por la cantidad de: " + str(total_anticipo)

        cxp_pago.fecha_creacion = now
        cxp_pago.usuario_creacion = request.user.username
        cxp_pago.save()

    except PlanCuenta.DoesNotExist:
        messages.error(request, u"No existe registrado una cuenta de tipo anticipo a proveedores, por favor verifíque, "
                                u"si tiene alguna duda comuníquese con el administrador del sistema")
        contador += 1

    return contador


def GuardarDetalleDocsPagados(id_cuenta, valor, pago, plan_cuenta, contador, cabecera, now, request):
    """
    Guarda la relación de la tabla pagos a cuentas por
    pagar, ademas actualiza la tabla cuentas por pagar
    :param id_cuenta:
    :param valor:
    :param pago:
    :param plan_cuenta:
    :param now:
    :param request:
    :return:
    """
    cuenta_pagar = Cuentas_por_Pagar.objects.get(id=id_cuenta)
    if cuenta_pagar.status != 1:
        messages.error(request, u"La cuenta por pagar que quiso utilizar estaba anulada al "
                                   u"momento de realizar el pago.")
        contador += 1
    if redondeo(cuenta_pagar.pagado + valor) > redondeo(cuenta_pagar.monto):
        messages.error(request, u"El valor del pago a la factura: " + cuenta_pagar.num_doc +
                                   u", excede su monto")
        contador += 1
    else:
        cuenta_pagar.pagado = redondeo(cuenta_pagar.pagado + valor)

    if cuenta_pagar.fecha_reg > cabecera.getFecha():
        messages.error(request, u"La fecha de registro del pago es menor a la fecha del registro del documento: "+
                       cuenta_pagar.num_doc + u" por favor verifique")
        contador += 1

    cuenta_pagar.usuario_actualizacion = request.user.username
    cuenta_pagar.fecha_actualizacion = now
    cuenta_pagar.save()

    cxp_pago = CXP_pago()
    cxp_pago.monto = redondeo(valor)
    cxp_pago.concepto = u"Pago a "+cuenta_pagar.documento.descripcion_documento +\
                        u" #"+cuenta_pagar.num_doc + u" por: $" + "%.2f" % valor
    cxp_pago.cuentas_x_pagar = cuenta_pagar
    cxp_pago.pago = pago
    cxp_pago.plan_cuenta = plan_cuenta

    cxp_pago.fecha_creacion = now
    cxp_pago.usuario_creacion = request.user.username
    cxp_pago.save()

    return contador


def GuardarPagoCruceDocs(cabecera, pago, proveedor, total_pagar, contador, asiento, now, request):
    """
    Guarda el pago realizado en un cruce de documentos
    :param cabecera:
    :param pago:
    :param proveedor:
    :param total_rows:
    :param total_pagar:
    :param asiento:
    :param now:
    :param request:
    :return:
    """
    pago.fecha_reg = cabecera.getFecha()
    pago.proveedor = proveedor
    pago.monto = redondeo(total_pagar)

    nombre_proveedor = unicode(pago.proveedor.razon_social[0:20])

    if cabecera.getObservacion() == "" or cabecera.getObservacion().isspace():
        pago.concepto = u"Pago a Proveedor: " + nombre_proveedor + u". La cantidad de " + u"$ "+str(total_pagar) + \
                                       u". Por cruce de documentos"

    else:
        pago.concepto = cabecera.getObservacion()

    pago.tipo_comprobante = TipoComprobante.objects.get(id=11)  # Comprobante tipo Cruce de documentos
    pago.forma_pago = FormaPago.objects.get(id=5)  # Forma de pago Cruce de documentos
    num_comp = get_num_comp(pago.tipo_comprobante_id, pago.fecha_reg, True, request)
    if num_comp != -1:
        pago.num_comp = num_comp
    else:
        now = datetime.datetime.now()
        try:
            secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=11)  # Secuencia de Compras
            fecha_comp = secuencia.fecha_actual
        except SecuenciaTipoComprobante.DoesNotExist:
            fecha_comp = now.strftime("%Y-%m-%d")

        messages.error(request, u"La fecha de registro ingresada es menor al último cruce de documentos:  "+str(fecha_comp)+
                                u", por favor verifique")
        contador += 1

    pago.fecha_pago = cabecera.getFecha()
    pago.fecha_creacion = now
    pago.usuario_creacion = request.user.username
    pago.save()

    ###########################
    #    ASIENTO CONTABLE     #
    ###########################
    asiento.concepto_comprobante = pago.concepto

    asiento.tipo_comprobante = TipoComprobante.objects.get(id=11)  # Comprobante tipo Cruce de documentos
    asiento.fecha = cabecera.getFecha()
    asiento.numero_comprobante = pago.num_comp

    asiento.fecha_creacion = now
    asiento.usuario_creacion = request.user.username
    asiento.save()

    ################################
    #   Asiento de Proveedores     #
    ################################
    detalle_asiento1 = Detalle_Comp_Contable()
    detalle_asiento1.cabecera_contable = asiento
    detalle_asiento1.dbcr = "D"
    detalle_asiento1.plan_cuenta = proveedor.plan_cuenta  # Plan de Cuenta Proveedores
    detalle_asiento1.detalle = detalle_asiento1.plan_cuenta.descripcion
    detalle_asiento1.valor = total_pagar
    detalle_asiento1.fecha_asiento = asiento.fecha

    detalle_asiento1.fecha_creacion = now
    detalle_asiento1.usuario_creacion = request.user.username
    detalle_asiento1.save()

    return contador

def GuardarCuentasPagar(cuenta_pagar, pago, total_pagar, contador, now, request):
    """
    Actualiza los datos de la cuenta por pagar
    :param cuenta_pagar:
    :param total_pagar:
    :param now:
    :param request:
    :return:
    """
    #########################################
    #   Validar que el valor que intenten   #
    #   enviar no supere el valor adeudado  #
    #########################################
    if redondeo(total_pagar, 2) > redondeo((cuenta_pagar.monto - cuenta_pagar.pagado), 2):
        messages.error(request, u"Error el valor del pago supera el monto de la deuda")
        contador += 1

    else:
        cuenta_pagar.pagado = redondeo(cuenta_pagar.pagado + total_pagar)

    cuenta_pagar.fecha_actualizacion = now
    cuenta_pagar.usuario_actualizacion = request.user.username
    cuenta_pagar.save()

    cxp_pago = CXP_pago()
    cxp_pago.pago = pago
    cxp_pago.cuentas_x_pagar = cuenta_pagar
    cxp_pago.concepto = "Cruce de documentos"
    cxp_pago.monto = redondeo(total_pagar)

    cxp_pago.usuario_creacion = request.user.username
    cxp_pago.fecha_creacion = now
    cxp_pago.save()

    return contador


def GuardarDetalleDocCruzados(detalle_doc_cobrar, pago, proveedor, asiento, contador, now, request):
    """
    Guarda el detalle de los docuementos que van a cruzarce
    :param detalle_doc_cobrar:
    :param pago:
    :param proveedor:
    :param asiento:
    :param now:
    :param request:
    :return:
    """

    for form in detalle_doc_cobrar:
        informacion = form.cleaned_data
        cxp_pago = CXP_pago()
        valor = informacion.get("valor")

        if valor is None:
            valor = 0

        if valor > 0:
            id = int(informacion.get("id_cuenta_cobrar"))
            cta_por_pagar = Cuentas_por_Pagar.objects.get(id=id)

            if cta_por_pagar.documento.codigo_documento == "04":  # Nota de Crédito
                if cta_por_pagar.status != 1:
                    messages.error(request, u"La nota de Crédito que quiso utilizar estaba anulada al momento de realizar la transacción, "
                                        u"por favor intente el cruce de documentos nuevamente.")
                    contador += 1
                elif redondeo(valor, 2) > redondeo((cta_por_pagar.monto - cta_por_pagar.pagado), 2):
                    messages.error(request, u"Error, datos enviados no válidos, el valor de la nota de crédito"
                                        u"excede su saldo")
                    contador += 1
                else:
                    cta_por_pagar.pagado += redondeo(valor)
                    cta_por_pagar.fecha_actualizacion = now
                    cta_por_pagar.usuario_actualizacion = request.user.username
                    cta_por_pagar.save()

                    cxp_pago.cuentas_x_pagar = cta_por_pagar
                    cxp_pago.pago = pago
                    cxp_pago.concepto = u"Cruce de documento con Nota de crédito por un valor de" + str(valor)
                    cxp_pago.monto = valor
                    cxp_pago.plan_cuenta = proveedor.plan_cuenta
                    cxp_pago.fecha_creacion = now
                    cxp_pago.usuario_creacion = request.user.username
                    cxp_pago.save()

                    ######################################
                    #    Asiento por cada doc Cruzado    #
                    ######################################

                    detalle_doc_cruz = Detalle_Comp_Contable()
                    detalle_doc_cruz.cabecera_contable = asiento
                    detalle_doc_cruz.dbcr = "H"
                    detalle_doc_cruz.plan_cuenta = cxp_pago.plan_cuenta
                    detalle_doc_cruz.valor = redondeo(valor)
                    detalle_doc_cruz.detalle = cxp_pago.concepto
                    detalle_doc_cruz.fecha_asiento = asiento.fecha

                    detalle_doc_cruz.fecha_creacion = now
                    detalle_doc_cruz.usuario_creacion = request.user.username
                    detalle_doc_cruz.save()

            else:  # Anticipo
                if cta_por_pagar.status != 1:
                    messages.error(request, u"El Anticipo que quiso utilizar estaba "
                                            u"anulado al momento de realizar la transacción, "

                                            u"por favor intente el cruce nuevamente.")
                    contador += 1
                elif redondeo(valor, 2) > redondeo((cta_por_pagar.monto - cta_por_pagar.pagado), 2):
                    messages.error(request, u"Error, datos enviados no válidos, el valor del anticipo"
                                        u"excede su saldo")
                    contador += 1
                else:
                    cta_por_pagar.pagado += valor

                cta_por_pagar.fecha_actualizacion = now
                cta_por_pagar.usuario_actualizacion = request.user.username
                cta_por_pagar.save()

                cta_por_pagar.fecha_actualizacion = now
                cta_por_pagar.usuario_actualizacion = request.user.username
                cta_por_pagar.save()

                cxp_pago.cuentas_x_pagar = cta_por_pagar
                cxp_pago.pago = pago
                cxp_pago.concepto = u"Cruce de documento con Anticipo por un valor de" + str(valor)
                cxp_pago.monto = redondeo(valor)
                cxp_pago.plan_cuenta = PlanCuenta.objects.get(tipo=14)
                cxp_pago.fecha_creacion = now
                cxp_pago.usuario_creacion = request.user.username
                cxp_pago.save()

                ######################################
                #         Asiento por Anticipo       #
                ######################################

                detalle_doc_cruz = Detalle_Comp_Contable()
                detalle_doc_cruz.cabecera_contable = asiento
                detalle_doc_cruz.dbcr = "H"
                detalle_doc_cruz.plan_cuenta = cxp_pago.plan_cuenta
                detalle_doc_cruz.valor = redondeo(valor)
                detalle_doc_cruz.detalle = cxp_pago.plan_cuenta.descripcion
                detalle_doc_cruz.fecha_asiento = asiento.fecha

                detalle_doc_cruz.fecha_creacion = now
                detalle_doc_cruz.usuario_creacion = request.user.username
                detalle_doc_cruz.save()
    return contador




            #####################################################
            #  FUNCIONES PARA EL CRUCE DE DOCUMENTOS Y CUENTAS  #
            #####################################################


def GuardarPagoCruceCtas(pago, proveedor, total_pagar, cabecera, asiento, contador, now, request):
    """
    Guarda el pago en la opción de cruce de cuentas
    :param pago:
    :param proveedor:
    :param cabecera:
    :param total_pagar:
    :param total_rows:
    :param asiento:
    :param now:
    :param request:
    :return:
    """

    if cabecera.getFecha() > datetime.datetime.now().date():
        contador += 1
        messages.error(request, u"La fecha de registro ingresada es mayor a la fecha actual, "
                                u"por favor verifique")
        errors = cabecera._errors.setdefault("fecha", ErrorList())
        errors.append(u"La fecha de registro ingresada es mayor a la fecha actual, "
                                u"por favor verifique")
    pago.fecha_pago = cabecera.getFecha()
    pago.proveedor = proveedor
    pago.monto = redondeo(total_pagar)

    nombre_proveedor = unicode(pago.proveedor.razon_social[0:20])
    if cabecera.getObservacion() == "":
        concepto = u"Pago a Proveedor: " + nombre_proveedor + u"# identificación " + pago.proveedor.ruc + \
                   u". La cantidad de " + u"$"+str(total_pagar) + u". Por cruce de cuentas"
    else:
        concepto = cabecera.getObservacion()

    pago.concepto = concepto
    pago.fecha_reg = cabecera.getFecha()
    pago.forma_pago = FormaPago.objects.get(id=6)  # Forma de pago Cruce de cuentas
    pago.tipo_comprobante = TipoComprobante.objects.get(id=12)  # Comprobante tipo Cruce de cuentas
    num_comp = get_num_comp(pago.tipo_comprobante_id, pago.fecha_reg, True, request)
    if num_comp != -1:
        pago.num_comp = num_comp
    else:
        now = datetime.datetime.now()
        try:
            secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=12)  # Secuencia de Compras
            fecha_comp = secuencia.fecha_actual
        except SecuenciaTipoComprobante.DoesNotExist:
            fecha_comp = now.strftime("%Y-%m-%d")
        messages.error(request, u"La fecha de registro ingresada es menor a la última fecha de registro de pago: "+str(fecha_comp)+
                                u", por favor verifique")
        contador += 1
    pago.fecha_creacion = now
    pago.usuario_creacion = request.user.username
    pago.save()

    ###########################
    #    ASIENTO CONTABLE     #
    ###########################

    asiento.concepto_comprobante = pago.concepto
    asiento.tipo_comprobante = TipoComprobante.objects.get(id=12)  # Tipo de comprobante Cruce de CTAs

    asiento.fecha = cabecera.getFecha()
    asiento.numero_comprobante = pago.num_comp

    asiento.fecha_creacion = now
    asiento.fecha_actualizacion = now
    asiento.usuario_creacion = request.user.username
    asiento.usuario_actualizacion = request.user.username
    asiento.save()

    return contador


def GuardardetallesCtasCruzados(detalle_cruce_cta, pago, asiento, contador, now, request):
    """
    Guarda el detalle de las cuentas por cruzar
    :param detalle_cruce_cta:
    :param pago:
    :param asiento:
    :param now:
    :param request:
    :return:
    """
    for form in detalle_cruce_cta:
        informacion = form.cleaned_data
        try:
            valor = float(informacion.get("valor"))
        except:
            valor = 0
        if valor > 0:
            ###################################
            #    Asiento del Cruce de ctas    #
            ###################################
            asiento_cruce_cta = Detalle_Comp_Contable()
            asiento_cruce_cta.cabecera_contable = asiento
            asiento_cruce_cta.dbcr = "H"
            try:
                asiento_cruce_cta.plan_cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
                asiento_cruce_cta.detalle = asiento_cruce_cta.plan_cuenta.descripcion
                cuenta_pagar = Cuentas_por_Pagar.objects.get(id=informacion.get("id_cuenta_pagar"))
                if redondeo(cuenta_pagar.monto - cuenta_pagar.pagado) < redondeo(valor):
                    contador += 1
                    messages.error(request, u"El valor ingresado de la cuenta supera el saldo"
                                        u" del documento")
                cuenta_pagar.pagado += valor
                cuenta_pagar.fecha_actualizacion = now
                cuenta_pagar.usuario_actualizacion = request.user.username
                cuenta_pagar.save()

                cxp_pago = CXP_pago()
                cxp_pago.monto = redondeo(float(informacion.get("valor")))
                cxp_pago.plan_cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
                cxp_pago.pago = pago
                cxp_pago.cuentas_x_pagar = cuenta_pagar
                cxp_pago.concepto = u"Cruce de documentos"
                cxp_pago.fecha_creacion = now
                cxp_pago.usuario_creacion = request.user.username
                if contador == 0:
                    cxp_pago.save()


                detalle_asiento = Detalle_Comp_Contable()
                detalle_asiento.cabecera_contable = asiento
                detalle_asiento.detalle = pago.concepto
                detalle_asiento.valor = redondeo(valor)

                if cuenta_pagar.naturaleza == 2:  # Facturas del Proveedor
                    detalle_asiento.dbcr = "D"
                else:  # Anticipos, Notas de Débito
                    detalle_asiento.dbcr = "H"

                if cuenta_pagar.documento.codigo_documento == "AN":
                    # Anticipo a Clientes
                    detalle_asiento.plan_cuenta = PlanCuenta.objects.get(tipo_id=14)  # Cuenta Anticipo Proveedores
                else:
                    detalle_asiento.plan_cuenta = pago.proveedor.plan_cuenta

                detalle_asiento.fecha_asiento = asiento.fecha

                detalle_asiento.fecha_creacion = now
                detalle_asiento.usuario_creacion = request.user.username
                detalle_asiento.save()

                # Contracuenta
                detalle_asiento2 = Detalle_Comp_Contable()
                detalle_asiento2.cabecera_contable = asiento
                detalle_asiento2.detalle = pago.concepto
                detalle_asiento2.valor = redondeo(valor)

                if cuenta_pagar.naturaleza == 2:  # Facturas del Proveedor
                    detalle_asiento2.dbcr = "H"
                else:  # Anticipos, Notas de Débito
                    detalle_asiento2.dbcr = "D"

                detalle_asiento2.fecha_asiento = asiento.fecha

                detalle_asiento2.plan_cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
                detalle_asiento2.fecha_creacion = now
                detalle_asiento2.usuario_creacion = request.user.username
                detalle_asiento2.save()
            except (PlanCuenta.DoesNotExist, ValueError) as e:
                contador += 1
                messages.error(request, u"Escoja una cuenta contable para hacer el cruce de cuenta")
                errors = form._errors.setdefault("cuenta", ErrorList())
                errors.append( u"Escoja una cuenta contable para hacer el cruce de cuenta")
            except (PlanCuenta.DoesNotExist, ValueError) as e:
                contador += 1
                messages.error(request, u"Error al intentar encontrar la cuenta por pagar")

    return contador


def busqueda_cuentas_pagar_ini(buscador):
    """
    Funcion para la búsqueda del
    modulo pagos o cuentas por pagar
    :param buscador:
    :return:
    """
    empresa = get_parametros_empresa()
    predicates = []
    predicates.append(('num_doc__icontains', buscador.get_num_doc()))
    if buscador.get_tipo_doc() != "":
        predicates.append(('documento_id', buscador.get_tipo_doc()))
    if buscador.get_proveedor() != "":
        predicates.append(('proveedor_id', buscador.get_proveedor()))
    predicates.append(('monto__range', (buscador.get_monto_ini(), buscador.get_monto_final())))
    predicates.append(('pagado__range', (buscador.get_pagado_ini(), buscador.get_pagado_final())))
    predicates.append(('plazo__gte', buscador.get_plazo()))

    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]
    entries = Cuentas_por_Pagar.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).filter(fecha_reg=empresa.fecha_inicio_sistema - timedelta(days=1))

    return entries


@login_required(login_url='/')
@csrf_exempt
def buscar_cuenta(request):
    """
    Función de búsqueda de cuenta en formato json
    :param request:
    :return:
    """
    respuesta = []
    q = str(request.GET.get("term", "")).replace(".", "")
    cuentas = PlanCuenta.objects.exclude(tipo__alias=2).filter(status=1, nivel=5).filter(Q(descripcion__istartswith=q)|Q(codigo__startswith=q))[0:5]
    for p in cuentas:
        respuesta.append({"value": p.codigo + "-" + p.descripcion, "key": p.codigo})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')
