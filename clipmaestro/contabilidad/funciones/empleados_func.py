#! /usr/bin/python
# -*- coding: UTF-8-*-
from datetime import timedelta
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from django.forms.util import ErrorList
from librerias.funciones.paginacion import *
from librerias.funciones.funciones_vistas import *
from django.core.paginator import *
import cStringIO as StringIO
from contabilidad.formularios.EmplreadosForm import *
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.contrib import messages
from django import template
import operator
register = template.Library()

__author__ = 'Roberto'




def generar_sueldo_mensual(detalles_empleados, formulario, request, now):
    '''
    Función que realiza la acción de guardar la información
    del sueldo mensual a los empleados
    :param detalles_empleados:
    :param formulario:
    :param request:
    :param now:
    :return:
    '''
    anio = formulario.getAnio()
    mes = formulario.getMes()
    contador = 0.0

    #### REalizar las validaciones de Mes para recibir el sueldo
    for form in detalles_empleados:
        informacion = form.cleaned_data
        sueldo_empleo = Sueldo_Empleado()

        sueldo_empleo.empleado = Empleados.objects.get(id=informacion("id_empleado"))
        sueldo_empleo.mes = mes
        sueldo_empleo.anio = anio





        if contador == 0.0:
            if not sueldo_empleo.usuario_creacion:
                sueldo_empleo.usuario_creacion = request.user.username
            sueldo_empleo.usuario_actualizacion = request.user.username

            if not sueldo_empleo.fecha_creacion:
                sueldo_empleo.fecha_creacion = now
            sueldo_empleo.fecha_actualizacion = now

            sueldo_empleo.save()


    return contador

@login_required(login_url='/')
@csrf_exempt
def get_detalle_empleados_sueldo(request):
    '''
    Función de Ajax que obtiene los registro de los empleados para
    generar el sueldo mensual
    :param request:
    :return tabla_html:
    '''
    parametro_hide = 0                                          # Parametro para validar si el query no me retorna
                                                                # elementos ocultar la tabla y muestra mensaje

    anio = request.POST.get("anio", "")
    mes = request.POST.get("mes", "")
    form_transacciones = []

    empleados = Empleados.objects.filter(status=1).order_by("apellidos_completos")

    l_detalle_doc = []
    formset_detalle_docs = formset_factory(EmpleadoDetalleForm, extra=0)

    if len(empleados) == 0:
        parametro_hide = 1

    else:

        if empleados:
            for obj in empleados:
                l_detalle_doc.append({"id_empleado": obj.id, "empleado": unicode(obj.num_id)+" - "+unicode(obj.apellidos_completos)+" "+ unicode(obj.nombres_completos),
                                        "sueldo": obj.sueldo})

    form_transacciones = formset_detalle_docs(initial=l_detalle_doc)

    return render_to_response('Empleados/tabla_empleados.html',
                                    {'forms': form_transacciones, "parametro_hide": parametro_hide},
                                    context_instance=RequestContext(request))