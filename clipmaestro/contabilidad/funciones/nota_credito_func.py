#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from contabilidad.models import *
import json
from django.forms.formsets import formset_factory
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from contabilidad.formularios.NotaCreditoForm import *
from django.forms.util import ErrorList
from librerias.funciones.funciones_vistas import *
from librerias.funciones.comprobantes_electronicos import *
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
import requests
from django.contrib import messages
from contabilidad.funciones.ventas_func import *
from django import template
import operator
register = template.Library()
IVA = 0.14
__author__ = 'Roberto'

########################################################################################################################
#                                           FUNCIONES NOTA DE CRÉDITO                                                  #
########################################################################################################################
def concepto_cuenta_asiento_nota_credito(ajuste_venta_cabecera):
    '''
    Función que me retorn el mensaje estándar
    para las cuentas del asiento contable realizado
    por la nota de crédito
    :param ajuste_venta_cabecera:
    :return:
    '''
    #20 noviembre he cambiado una cosa por smart_unicode para que entienda los simbolos raros en los nombres :o
    print smart_unicode(unicode(ajuste_venta_cabecera.cliente.razon_social))
    return u"Cliente: "+smart_unicode(unicode(ajuste_venta_cabecera.cliente.razon_social))[0:22]+u";  "+\
                                                 u"# Doc de la N/C: "+ajuste_venta_cabecera.vigencia_doc_empresa.serie+\
                                                 "-"+ajuste_venta_cabecera.num_doc+u";  "+\
                                                 u"# Doc a Modificar: "+str(ajuste_venta_cabecera.num_doc_modifica)

def busqueda_nota_credito(buscador):
    '''
    Función que realiza las búsquedas en los documentos nota de crédito
    :param buscador:
    :return:
    '''
    predicates = []
    if buscador.getFecha() is not None:
        predicates.append(('fecha_emi__range',  (buscador.getFecha(), buscador.getFechaFinal())))

    predicates.append(('num_comp__icontains', buscador.getNumComprobante()))
    predicates.append(('num_doc__icontains', buscador.getNumeroDocumento()))

    if buscador.getEstado() != "":
        estado = Estados.objects.get(id=buscador.getEstado())
        predicates.append(('status', estado.codigo))

    if buscador.getCliente() is not None:
        predicates.append(('cliente', buscador.getCliente()))
    if buscador.getVendedor() is not None:
        predicates.append(('venta_cabecera__vendedor', buscador.getVendedor()))
    if buscador.getItem() is not None:
        predicates.append(('venta_cabecera__venta_detalle__item', buscador.getItem()))

    q_list = [Q(x) for x in predicates]
    entries = AjusteVentaCabecera.objects.exclude(status=0).extra(
        where=["baseiva - descuentoiva + base0 - descuento0 + monto_iva + total_ice >= %s AND baseiva - descuentoiva + "
               "base0 - descuento0 + monto_iva + total_ice <= %s"], params=[buscador.getRangoMontoIni(), buscador.getRangoMontoHasta()]).\
        filter(reduce(operator.and_, q_list)).distinct().order_by("-fecha_emi", "-num_comp", "-num_doc").distinct()
    return entries

def ValidatorFormsetManual(formset):
    bandera = 0
    for form in formset:
        informacion = form.cleaned_data
        if informacion.get("item") is None:
            bandera += 1
    return bandera

def CalcularTotalesDevolucionVentaManual(formset, request):
    '''
    Función que realiza el cálculo de la nota de crédito de documentos
    que no se encuentran registrados en el sistema (Manual)
    :param formset:
    :return:
    '''
    subtotales = 0.0
    iva_valor = 0.0
    ice_valor = 0.0
    resultado = 0.0
    subtotal_simple = 0.0
    subtotales_cero = 0.0
    subtotal_simple_cero = 0.0
    cont = 1
    cantidad_descuento = 0.0
    print 'antes de los form del formset para calcular una suma de impuestos'

    for form in formset:
        informacion = form.cleaned_data
        print('lo de abajo es un renglon del combito')
        print str(informacion)
        print('lo de arriba es un renglon del combito')
        precio_u = redondeo(float(informacion.get("precio", "0")), 2)
        #sel = request.POST.get("tipo-"+str(cont))
        sel = request.POST.get("all_cantidad_man")
        print sel
        print 'lo del sel para el calculo :o'
        item = Item.objects.filter(status=1).get(id=informacion.get("item"))
        print 'el item del combito'
        print str(item)
        print 'ccccccc'
        print str(sel)
        print 'lo de la variable sel :o'
        #19 noviembre, ya no importa lo que sea de la bolita 1 o bolita 2
        #if sel == "1":   # Check en cantidad
        print 'he selecionado uno en cantidad'
        if informacion.get("cant_dev") != "":
            cant_dev = float(informacion.get("cant_dev"))
            cant_descuento = float(informacion.get("cant_descuento"))
            print str(cant_dev)
            print('lo de arriba es la cantidad devuelta')
            print str(precio_u)
            print('lo de arriba es la precio')
            print str(cant_descuento)
            print('lo de arriba es la cantidad descuento')
            if item.TieneIva():
                print 'item si tiene iva :o'
                subtotales = subtotales + float(cant_dev*(precio_u- cant_descuento) )
                iva_valor = iva_valor + float((cant_dev*(precio_u- cant_descuento))*(item.iva/100))
                print str(iva_valor)+' el iva de la cosa'
                print str(subtotales)+' el subtotal de la cosa'
            else:
                print 'item no tiene iva :o'
                subtotales_cero = subtotales_cero + float((cant_dev*(precio_u- cant_descuento)))
            resultado = subtotales_cero + ice_valor + iva_valor + subtotales
        #elif sel == "2":   # Check en costo
        if informacion.get("subtotal") != "" and informacion.get("subtotal") is not None:
            subtotal = float(informacion.get("subtotal"))
            print 'el otro subtotal de la ventana'
            print str(subtotal)

            if item.TieneIva():
                subtotales = subtotales + subtotal
                iva_valor = iva_valor + (subtotal)*(item.iva/100)
            else:
                subtotales_cero = subtotales_cero + subtotal
            #resultado = subtotales + subtotales_cero + iva_valor + ice_valor
        else:
            print 'he checado en el costo de la seleccion de las bolitas pero no hay datos :o'
        cont += 1

    print str(subtotales)+'el subtotales'+str(subtotales_cero)+'el subtotales cero'
    print "======================================== TOTALES ============================================"
    print "Subtotal_Simple_12%: ", subtotal_simple
    print "Subtotal_Simple_0%: ", subtotal_simple_cero
    print "Subtotal_12%: ", subtotales
    print "Subtotal_0%: ", subtotales_cero
    print "Total_sub_Total: ", subtotales + subtotales_cero
    print "IVA: ", iva_valor
    print "ICE: ", ice_valor
    print "total_pagar: ", resultado
    print "============================================================================================="

    return subtotales, subtotales_cero, iva_valor, ice_valor, resultado

def CalcularTotalesDevolucionVenta(formset, request):
    '''
    Función que realiza el proceso de calcular los
    totales de la nota de crédito procedente de una venta
    :param formset:
    :param venta_cabecera:
    :return:
    '''
    subtotales = 0.0
    iva_valor = 0.0
    ice_valor = 0.0
    resultado = 0.0
    subtotales_cero = 0.0
    subtotal_simple = 0.0
    subtotal_simple_cero = 0.0
    descuento_iva = 0.0
    descuento_cero = 0.0
    cont = 1
    # tipo desc -> 0, 1, 2, 3, 4 (Formas de Descuento en la Venta)

    for form in formset:
        informacion = form.cleaned_data
        precio_u = redondeo(float(informacion.get("precio", "0")), 2)
        valor_descuento = float(informacion.get("desc_valor"))
        sel = request.POST.get("tipo-"+str(cont))

        if sel == "1":   # Check en cantidad
            item = Item.objects.filter(status=1).get(id=informacion.get("id_item"))
            if informacion.get("cant_dev") != "":
                cant_dev = float(informacion.get("cant_dev"))

                if item.TieneICE():
                    porc_ice = redondeo(float(item.porc_ice), 2)
                else:
                    porc_ice = 0.0

                if valor_descuento == 0.0:

                    if item.TieneIva():
                        subtotales = subtotales + float(cant_dev*precio_u)
                        iva_valor = iva_valor + float((cant_dev*precio_u)*(item.iva/100))
                        ice_valor = ice_valor + float((cant_dev*precio_u)*(porc_ice/100))
                    else:
                        subtotales_cero = subtotales_cero + float((cant_dev*precio_u))
                    resultado = subtotales_cero + ice_valor + iva_valor + subtotales

                else:

                    if item.TieneIva():
                        subtotal_simple = subtotal_simple + float(cant_dev*precio_u)
                        subtotales = subtotales + float(cant_dev*precio_u) - float((float(cant_dev*precio_u)*valor_descuento)/100)
                        iva_valor = iva_valor + (float(cant_dev*precio_u) - float(cant_dev*precio_u)*valor_descuento/100)*item.iva/100
                        ice_valor = ice_valor + float(cant_dev*precio_u)*porc_ice/100
                        descuento_iva = descuento_iva + float((float(cant_dev*precio_u)*valor_descuento)/100)
                    else:
                        subtotal_simple_cero = subtotal_simple_cero + float(cant_dev*precio_u)
                        subtotales_cero = subtotales_cero + float(cant_dev*precio_u) - float((float(cant_dev*precio_u)*valor_descuento)/100)
                        descuento_cero = descuento_cero + float((float(cant_dev*precio_u)*valor_descuento)/100)
                    resultado = subtotales + subtotales_cero + iva_valor + ice_valor


        elif sel == "2":  # # Check en Costo
            item = Item.objects.filter(status=1).get(id=informacion.get("id_item"))

            if informacion.get("subtotal") != "" and informacion.get("subtotal") is not None:
                subtotal = float(informacion.get("subtotal"))

                if item.TieneICE():
                    porc_ice = redondeo(float(item.porc_ice), 2)
                else:
                    porc_ice = 0.0

                if valor_descuento == 0.0:
                    if item.TieneIva():
                        subtotales = subtotales + subtotal
                        iva_valor = iva_valor + (subtotal)*(item.iva/100)
                        ice_valor = ice_valor + (subtotal)*(porc_ice/100)
                    else:
                        subtotales_cero = subtotales_cero + subtotal
                    resultado = subtotales_cero + ice_valor + iva_valor + subtotales

                else:

                    if item.TieneIva():
                        subtotal_simple = subtotal_simple + float(cant_dev*precio_u)
                        subtotales = subtotales + float(cant_dev*precio_u) - float((float(cant_dev*precio_u)*valor_descuento)/100)
                        iva_valor = iva_valor + (float(cant_dev*precio_u) - float(cant_dev*precio_u)*valor_descuento/100)*item.iva/100
                        ice_valor = ice_valor + float(cant_dev*precio_u)*porc_ice/100
                        descuento_iva = descuento_iva + float((float(cant_dev*precio_u)*valor_descuento)/100)
                    else:
                        subtotal_simple_cero = subtotal_simple_cero + float(cant_dev*precio_u)
                        subtotales_cero = subtotales_cero + float(cant_dev*precio_u) - float((float(cant_dev*precio_u)*valor_descuento)/100)
                        descuento_cero = descuento_cero + float((float(cant_dev*precio_u)*valor_descuento)/100)
                    resultado = subtotales + subtotales_cero + iva_valor + ice_valor

        cont += 1

    print "======================================== TOTALES ============================================"
    print "Subtotal_Simple_12%: ", subtotal_simple
    print "Subtotal_Simple_0%: ", subtotal_simple_cero
    print "Subtotal_12%: ", subtotales
    print "Subtotal_0%: ", subtotales_cero
    print "Total_sub_Total: ", subtotales + subtotales_cero
    print "IVA: ", iva_valor
    print "ICE: ", ice_valor
    print "total_pagar: ", resultado
    print "============================================================================================="

    return redondeo(subtotales, 2), redondeo(subtotales_cero, 2), redondeo(iva_valor, 2), redondeo(ice_valor, 2), redondeo(resultado, 2), \
           redondeo(descuento_iva, 2), redondeo(descuento_cero, 2)

def UpdatesItemsDevolucion(detalle_venta, cantidad_devolver, request, now):
    '''
    Función que realiza las actualizaciones en Venta Detalle,
    Item_bodega
    :param detalle_venta:
    :param request:
    :param now:
    :return parametro:
    '''
    parametro = 0

    item_bodega = Item_Bodega.objects.filter(status=1).get(item=detalle_venta.item)
    print "ITEM: ", str(detalle_venta.item_id)+" - "+detalle_venta.item.nombre
    print "venta cantidad_venta: ", detalle_venta.cantidad
    print "venta cantidad_entregada: ", detalle_venta.cant_entregada
    print "venta cantidad_devuelte: ", detalle_venta.cant_devuelta
    print "CANTIDAD INPUT DEVOLVER: ", cantidad_devolver

    # 1.-) Incremento la cantidad devuelta
    detalle_venta.cant_devuelta += cantidad_devolver

    # 2.-) Condición para saber si se realiza el movimiento en el inventario
    if detalle_venta.cantidad - detalle_venta.cant_devuelta < detalle_venta.cant_entregada:
        parametro = 1   # Indicador de que existen movimientos del inventario

        # Obtener la cantidad que se realizará el proceso de inventario
        cantidad_temp = detalle_venta.cant_entregada

        print "cantidad_entregada_guardada: ", cantidad_temp
        # 3.-) Update en la Cantidad Entregada
        detalle_venta.cant_entregada = detalle_venta.cantidad - detalle_venta.cant_devuelta

        detalle_venta.usuario_actualizacion = request.user.username
        detalle_venta.fecha_actualizacion = now
        detalle_venta.save()

        # Update Item_Bodega
        print "cantidad_actual_antes bodega: ", cantidad_temp
        item_bodega.cantidad_actual += cantidad_temp - detalle_venta.cantidad - detalle_venta.cant_devuelta
        print "cantidad actual final bodega: ", cantidad_temp

        item_bodega.fecha_actualizacion = now
        item_bodega.usuario_actualizacion = request.user.username
        item_bodega.save()

    else:

        # Update Venta Detalle - cant_devolver
        detalle_venta.usuario_actualizacion = request.user.username
        detalle_venta.fecha_actualizacion = now
        detalle_venta.save()

    return parametro

def GuardarAjusteVentaCabecera(ajuste_cabecera, formulario, venta, subtotales, subtotales_cero, iva_valor, ice_valor, resultado, descuento_iva, descuento_cero,  dia_actual, request):
    '''
    Función que registra en el sistema la cabecera de la nota de créditos con sus totales
    es parametrizada si emite documentos electrónicos mediante la función emite_docs_electronicos()
    para tomar en cuenta las condiciones correspondientes
    :param ajuste_cabecera:
    :param formulario:
    :param subtotales:
    :param subtotales_cero:
    :param iva_valor:
    :param ice_valor:
    :param resultado:
    :param descuento_iva:
    :param descuento_cero:
    :param monto_descuento12:
    :param monto_descuento0:
    :param dia_actual:
    :param request:
    :return:
    '''
    contador = 0
    tipo_c = TipoComprobante.objects.get(id=22)             # Comprobante Nota Crédito
    ajuste_cabecera.fecha_emi = formulario.getFecha()
    ajuste_cabecera.cliente = Cliente.objects.get(id=formulario.getCliente())
    ajuste_cabecera.concepto = formulario.getConcepto()
    if emite_docs_electronicos():
        ajuste_cabecera.razon_modifica = formulario.getRazonModificar()

    try:
        ajuste_cabecera.vigencia_doc_empresa = VigenciaDocEmpresa.objects.get(id=formulario.getSerie(), status=1)
    except VigenciaDocEmpresa.DoesNotExist:
        contador += 1
        messages.error(request, u"Error: No tiene un Documento N/C habilitado ingrese al "
                                u"sistema un documento N/C vigente")

    if emite_docs_electronicos():   # Emite documentos electrónicos

        ajuste_cabecera.num_doc = get_num_doc(ajuste_cabecera.vigencia_doc_empresa, ajuste_cabecera.fecha_emi, request)

        if ajuste_cabecera.num_doc == -1:
            contador += 1
            messages.error(request, u"Error: La secuencia actual es mayor a la secuencia "
                                    u"final del block de documento")

        ajuste_cabecera.num_comp = get_num_comp(tipo_c.id, formulario.getFecha(), True, request)

        if ajuste_cabecera.num_comp == -1:
            contador += 1
            messages.error(request, u"Error: Al Obtener el número de comprobante")

    else:                        # No Emite documentos electrónicos

        if formulario.getFecha() < ajuste_cabecera.vigencia_doc_empresa.fecha_actual:
            contador += 1
            messages.error(request, u"La fecha de registro es menor a la "
                                    u"fecha de vigencia de documento.")
        else:

            # Número de Documento
            num_doc = get_num_doc(ajuste_cabecera.vigencia_doc_empresa, formulario.getFecha(), request)
            num_comp = get_num_comp(tipo_c.id, formulario.getFecha(), True, request)

            if num_doc != -1:
                ajuste_cabecera.num_doc = num_doc
            else:
                contador += 1
                messages.error(request, u"Error en num_doc.")

            if num_comp != -1:
                ajuste_cabecera.num_comp = num_comp
            else:
                contador += 1
                messages.error(request, u"Error en num_comp.")

    if contador == 0.0:

        if venta is not None:
            # Automatica
            ajuste_cabecera.venta_cabecera = venta
            ajuste_cabecera.num_doc_modifica = venta.vigencia_doc_empresa.serie+"-"+venta.num_documento
            ajuste_cabecera.fecha_doc_modifica = venta.fecha

            if ajuste_cabecera.venta_cabecera.tipo_descuento_combo == 0:  # Sin Descto.
                ajuste_cabecera.base0 = redondeo(subtotales_cero, 2)
                ajuste_cabecera.tarifa12 = redondeo(subtotales, 2)
                ajuste_cabecera.monto_iva = redondeo(iva_valor, 2)
                ajuste_cabecera.total_ice = redondeo(ice_valor, 2)
                ajuste_cabecera.descuento0 = 0
                ajuste_cabecera.descuentoiva = 0
            else:
                ajuste_cabecera.base0 = redondeo(subtotales_cero, 2)
                ajuste_cabecera.tarifa12 = redondeo(subtotales, 2)
                ajuste_cabecera.monto_iva = redondeo(iva_valor, 2)
                ajuste_cabecera.total_ice = redondeo(ice_valor, 2)
                ajuste_cabecera.descuento0 = redondeo(descuento_cero, 2)
                ajuste_cabecera.descuentoiva = redondeo(descuento_iva, 2)

        else:
            # Manual
            ajuste_cabecera.num_doc_modifica = formulario.getNumDocumentoModifica()
            ajuste_cabecera.fecha_doc_modifica = formulario.getFecha()
            ajuste_cabecera.base0 = redondeo(subtotales_cero, 2)
            ajuste_cabecera.tarifa12 = redondeo(subtotales, 2)
            ajuste_cabecera.monto_iva = redondeo(iva_valor, 2)
            ajuste_cabecera.total_ice = redondeo(ice_valor, 2)
            ajuste_cabecera.descuento0 = 0
            ajuste_cabecera.descuentoiva = 0

        ajuste_cabecera.fecha_creacion = dia_actual
        ajuste_cabecera.usuario_creacion = request.user.username
        ajuste_cabecera.save()

        # Guarda Cuentas por Cobrar
        cuentas_cobrar = Cuentas_por_Cobrar()
        cuentas_cobrar.monto = redondeo(resultado, 2)
        cuentas_cobrar.cliente = ajuste_cabecera.cliente
        cuentas_cobrar.fecha_emi = formulario.getFecha()
        cuentas_cobrar.fecha_reg = formulario.getFecha()
        cuentas_cobrar.documento = Documento.objects.get(id=4)  # Nota de Crédito
        cuentas_cobrar.num_doc = ajuste_cabecera.vigencia_doc_empresa.serie+"-"+ajuste_cabecera.num_doc
        cuentas_cobrar.naturaleza = 2
        cuentas_cobrar.fecha_creacion = dia_actual
        cuentas_cobrar.usuario_creacion = request.user.username
        cuentas_cobrar.save()
        print 'he guardado alguna cosa en cabeceras :o '
    return contador

def GuardarAjusteVentaCabecera_manual(ajuste_cabecera, formulario, venta, subtotales, subtotales_cero, iva_valor, ice_valor, resultado, descuento_iva, descuento_cero,  dia_actual, request):
    '''
    Función que registra en el sistema la cabecera de la nota de créditos con sus totales
    es parametrizada si emite documentos electrónicos mediante la función emite_docs_electronicos()
    para tomar en cuenta las condiciones correspondientes
    :param ajuste_cabecera:
    :param formulario:
    :param subtotales:
    :param subtotales_cero:
    :param iva_valor:
    :param ice_valor:
    :param resultado:
    :param descuento_iva:
    :param descuento_cero:
    :param monto_descuento12:
    :param monto_descuento0:
    :param dia_actual:
    :param request:
    :return:
    '''
    contador = 0
    tipo_c = TipoComprobante.objects.get(id=22)             # Comprobante Nota Crédito
    ajuste_cabecera.fecha_emi = formulario.getFecha()
    ajuste_cabecera.cliente = Cliente.objects.get(id=formulario.getCliente())
    ajuste_cabecera.concepto = formulario.getConcepto()
    if emite_docs_electronicos():
        ajuste_cabecera.razon_modifica = formulario.getRazonModificar()

    try:
        ajuste_cabecera.vigencia_doc_empresa = VigenciaDocEmpresa.objects.get(id=formulario.getSerie(), status=1)
    except VigenciaDocEmpresa.DoesNotExist:
        contador += 1
        messages.error(request, u"Error: No tiene un Documento N/C habilitado ingrese al "
                                u"sistema un documento N/C vigente")

    if emite_docs_electronicos():   # Emite documentos electrónicos

        ajuste_cabecera.num_doc = get_num_doc(ajuste_cabecera.vigencia_doc_empresa, ajuste_cabecera.fecha_emi, request)

        if ajuste_cabecera.num_doc == -1:
            contador += 1
            messages.error(request, u"Error: La secuencia actual es mayor a la secuencia "
                                    u"final del block de documento")

        ajuste_cabecera.num_comp = get_num_comp(tipo_c.id, formulario.getFecha(), True, request)

        if ajuste_cabecera.num_comp == -1:
            contador += 1
            messages.error(request, u"Error: Al Obtener el número de comprobante")

    else:                        # No Emite documentos electrónicos

        if formulario.getFecha() < ajuste_cabecera.vigencia_doc_empresa.fecha_actual:
            contador += 1
            messages.error(request, u"La fecha de registro es menor a la "
                                    u"fecha de vigencia de documento.")
        else:

            # Número de Documento
            num_doc = get_num_doc(ajuste_cabecera.vigencia_doc_empresa, formulario.getFecha(), request)
            num_comp = get_num_comp(tipo_c.id, formulario.getFecha(), True, request)

            if num_doc != -1:
                ajuste_cabecera.num_doc = num_doc
            else:
                contador += 1
                messages.error(request, u"Error en num_doc.")

            if num_comp != -1:
                ajuste_cabecera.num_comp = num_comp
            else:
                contador += 1
                messages.error(request, u"Error en num_comp.")

    if contador == 0.0:

        if venta is not None:
            # Automatica
            ajuste_cabecera.venta_cabecera = venta
            ajuste_cabecera.num_doc_modifica = venta.vigencia_doc_empresa.serie+"-"+venta.num_documento
            ajuste_cabecera.fecha_doc_modifica = venta.fecha

            if ajuste_cabecera.venta_cabecera.tipo_descuento_combo == 0:  # Sin Descto.
                ajuste_cabecera.base0 = redondeo(subtotales_cero, 2)
                ajuste_cabecera.tarifa12 = redondeo(subtotales, 2)
                ajuste_cabecera.monto_iva = redondeo(iva_valor, 2)
                ajuste_cabecera.total_ice = redondeo(ice_valor, 2)
                ajuste_cabecera.descuento0 = 0
                ajuste_cabecera.descuentoiva = 0
            else:
                ajuste_cabecera.base0 = redondeo(subtotales_cero, 2)
                ajuste_cabecera.tarifa12 = redondeo(subtotales, 2)
                ajuste_cabecera.monto_iva = redondeo(iva_valor, 2)
                ajuste_cabecera.total_ice = redondeo(ice_valor, 2)
                ajuste_cabecera.descuento0 = redondeo(descuento_cero, 2)
                ajuste_cabecera.descuentoiva = redondeo(descuento_iva, 2)

        else:
            # Manual
            ajuste_cabecera.num_doc_modifica = formulario.getNumDocumentoModifica()
            ajuste_cabecera.fecha_doc_modifica = formulario.getFecha()
            ajuste_cabecera.base0 = redondeo(subtotales_cero, 2)
            ajuste_cabecera.tarifa12 = redondeo(subtotales, 2)
            ajuste_cabecera.monto_iva = redondeo(iva_valor, 2)
            ajuste_cabecera.total_ice = redondeo(ice_valor, 2)
            ajuste_cabecera.descuento0 = 0
            ajuste_cabecera.descuentoiva = 0

        ajuste_cabecera.fecha_creacion = dia_actual
        ajuste_cabecera.usuario_creacion = request.user.username
        ajuste_cabecera.save()

        # Guarda Cuentas por Cobrar
        cuentas_cobrar = Cuentas_por_Cobrar()
        cuentas_cobrar.monto = redondeo(resultado, 2)
        cuentas_cobrar.cliente = ajuste_cabecera.cliente
        cuentas_cobrar.fecha_emi = formulario.getFecha()
        cuentas_cobrar.fecha_reg = formulario.getFecha()
        cuentas_cobrar.documento = Documento.objects.get(id=4)  # Nota de Crédito
        cuentas_cobrar.num_doc = ajuste_cabecera.vigencia_doc_empresa.serie+"-"+ajuste_cabecera.num_doc
        cuentas_cobrar.naturaleza = 2
        cuentas_cobrar.fecha_creacion = dia_actual
        cuentas_cobrar.usuario_creacion = request.user.username
        cuentas_cobrar.save()

    return contador


def Anula_Inventario_N_C(ajuste_cabecera, request, now):
    '''
    Función que Anula las Tablas de Inventario a partir de la venta
    seleccionada y actualiza en Item Bodega la cantidad que regresa
    a bodega
    :param venta_cabecera:
    :param request:
    :param now:
    :return:
    '''
    detalle_ajuste = AjusteVentaDetalle.objects.filter(ajuste_venta_cabecera=ajuste_cabecera)

    # 1.-) Update Cantidad Actual en Bodega
    for it in detalle_ajuste:
        item_bodega = Item_Bodega.objects.filter(status=1).get(item=it.item)
        item_bodega.cantidad_actual -= it.cantidad_devolver
        item_bodega.fecha_actualizacion = now
        item_bodega.usuario_actualizacion = request.user.username
        item_bodega.save()

    # 2.-) Anular Cabecera Inventario y Detalle Inventario
    lista_inventario_cabecera = Inventario_Cabecera.objects.filter(num_doc=ajuste_cabecera.num_doc)
    for cainv in lista_inventario_cabecera:
        cainv.status = 2
        cainv.fecha_actualizacion = now
        cainv.usuario_actualizacion = request.user.username
        cainv.save()

        for detinv in Inventario_Detalle.objects.filter(inventario_cabecera=cainv):
            detinv.status = 2
            detinv.fecha_actualizacion = now
            detinv.usuario_actualizacion = request.user.username
            detinv.save()

        # 3.-) Anular Cabecera Comprobante Contable y Detalle Comprobante Contable
        for cabcont in Cabecera_Comp_Contable.objects.filter(numero_comprobante=cainv.num_comp):
            cabcont.status = 2
            cabcont.usuario_actualizacion = request.user.username
            cabcont.fecha_actualizacion = now
            cabcont.save()

            for detcont in Detalle_Comp_Contable.objects.filter(cabecera_contable=cabcont):
                detcont.status = 2
                detcont.usuario_actualizacion = request.user.username
                detcont.fecha_actualizacion = now
                detcont.save()



def UpdateVentaDetalleNC(ajuste_cabecera, request):
    '''
    Función que realiza una actualización de la cantidad a devolver en Venta Detalle
    llamada cada vez que realice la opción anular nota de crédito
    :param ajuste_cabecera:
    :return:
    '''

    ajuste_detalle = AjusteVentaDetalle.objects.filter(status=1, ajuste_venta_cabecera=ajuste_cabecera)

    for ajd in ajuste_detalle:
        print ajd.ajuste_venta_cabecera




def GuardarAjusteVentaDetalle(formset, ajuste_cabecera, bandera, request, dia_actual):
    '''
    Función que guarda en la tabla de ajuste venta detalle
    :param formset:
    :param ajuste_cabecera:
    :param bandera:
    :param request:
    :param dia_actual:
    :return:
    '''
    cont = 1                        # Indice para obtener los radio button
    flag = 0                        # Validador
    parametro = 0                   # Indica si existe movimientos en inventario o no

    for form in formset:
        informacion = form.cleaned_data
        detalle_venta = Venta_Detalle.objects.filter(status=1).get(id=informacion.get("id_detalle_venta"))
        ajuste_detalle = AjusteVentaDetalle()
        ajuste_detalle.ajuste_venta_cabecera = ajuste_cabecera
        sel = request.POST.get("tipo-"+str(cont))   # Seleccion encargada de los radio button

        if sel == "1":   # Cantidad
            if informacion.get("cant_dev") != "":
                item = Item.objects.get(id=informacion.get("id_item"))

                if bandera == 1:     # Manual
                    item = Item.objects.get(id=informacion.get("id_item"))
                    ajuste_detalle.item = item
                    ajuste_detalle.valor = float(informacion.get("precio"))
                    ajuste_detalle.cantidad_devolver = float(informacion.get("cant_dev"))
                    ajuste_detalle.descuento = float(informacion.get("desc_valor"))# 19 noviembreesto no existe en el formulario
                    #cant_descuento 19 noviembre se llama la variable que puse en el form

                    if informacion.get("act_inven"):
                        parametro = 1

                    ajuste_detalle.usuario_creacion = request.user.username
                    ajuste_detalle.fecha_creacion = dia_actual
                    ajuste_detalle.save()

                else:                # Automática

                    if item.TipoItem() == 1:  # Productos
                        ajuste_detalle.item = item
                        ajuste_detalle.cantidad_devolver = float(informacion.get("cant_dev"))
                        ajuste_detalle.descuento = float(informacion.get("desc_valor"))

                        # VALOR UNITARIO
                        ajuste_detalle.valor = float(informacion.get("precio"))

                        if item.TieneIva():
                            ajuste_detalle.monto_iva = redondeo(float(informacion.get("precio"))*IVA, 2)
                        if item.TieneICE():
                            ajuste_detalle.porcentaje_ice = float(informacion.get("ice"))

                        ajuste_detalle.usuario_creacion = request.user.username
                        ajuste_detalle.fecha_creacion = dia_actual
                        ajuste_detalle.save()

                        # Actualiza las tablas Venta_Detalle - Item_Bodega a las cantidades correspondientes
                        parametro = UpdatesItemsDevolucion(detalle_venta, float(informacion.get("cant_dev")), request,
                                                           dia_actual)

                    else:                 # Servicios

                        ajuste_detalle.item = item
                        ajuste_detalle.cantidad_devolver = float(informacion.get("cant_dev"))
                        ajuste_detalle.descuento = float(informacion.get("desc_valor"))

                        # VALOR UNITARIO
                        ajuste_detalle.valor = float(informacion.get("precio"))

                        if item.TieneIva():
                            ajuste_detalle.monto_iva = redondeo(float(informacion.get("precio"))*IVA, 2)
                        if item.TieneICE():
                            ajuste_detalle.porcentaje_ice = float(informacion.get("ice"))

                        ajuste_detalle.usuario_creacion = request.user.username
                        ajuste_detalle.fecha_creacion = dia_actual
                        ajuste_detalle.save()

                        # UpdateDetalleVenta - Servicio
                        detalle_venta.cant_devuelta += float(informacion.get("cant_dev"))
                        detalle_venta.fecha_actualizacion = dia_actual
                        detalle_venta.usuario_actualizacion = request.user.username
                        detalle_venta.save()

            else:
                flag += 1
                messages.error(request, u"Error al seleccionar cantidad debe ingresar una cantidad a devolver "
                                        u"para realizar la nota de crédito")

        elif sel == "2":  # Check Costo
            item = Item.objects.get(id=informacion.get("id_item"))

            if informacion.get("subtotal") != "" and informacion.get("subtotal") is not None:
                if bandera == 1:     # Manual
                    item = Item.objects.get(id=informacion.get("id_item"))
                    ajuste_detalle.item = item
                    # REVISAR
                    ajuste_detalle.valor = float(informacion.get("subtotal"))

                    if informacion.get("cant_dev") != "":
                        ajuste_detalle.cantidad_devolver = float(informacion.get("cant_dev"))

                    ajuste_detalle.descuento = float(informacion.get("desc_valor"))
                    ajuste_detalle.usuario_creacion = request.user.username
                    ajuste_detalle.fecha_creacion = dia_actual
                    ajuste_detalle.fecha_actualizacion = dia_actual
                    ajuste_detalle.usuario_actualizacion = request.user.username
                    ajuste_detalle.save()

                else:                # Automática

                    if item.TipoItem() == 1:  # Productos
                        ajuste_detalle.item = item

                        # VALOR COSTO DIRECTO DEL FORM
                        ajuste_detalle.valor = float(informacion.get("subtotal"))

                        if informacion.get("cant_dev") != "" and informacion.get("cant_dev") is not None:
                            ajuste_detalle.cantidad_devolver = float(informacion.get("cant_dev"))

                        ajuste_detalle.descuento = float(informacion.get("desc_valor"))

                        if item.TieneIva():
                            ajuste_detalle.monto_iva = redondeo(float(informacion.get("subtotal"))*IVA, 2)
                        if item.TieneICE():
                            ajuste_detalle.porcentaje_ice = float(informacion.get("ice"))

                        ajuste_detalle.usuario_creacion = request.user.username
                        ajuste_detalle.fecha_creacion = dia_actual
                        ajuste_detalle.save()

                    else:                 # Servicios

                        ajuste_detalle.item = item

                        # VALOR COSTO DIRECTO DEL FORM
                        ajuste_detalle.valor = float(informacion.get("subtotal"))

                        if informacion.get("cant_dev") != "":
                            ajuste_detalle.cantidad_devolver = float(informacion.get("cant_dev"))

                            # Venta Detalle
                            detalle_venta.cant_devuelta += float(informacion.get("cant_dev"))
                            detalle_venta.fecha_actualizacion = dia_actual
                            detalle_venta.usuario_actualizacion = request.user.username
                            detalle_venta.save()

                        ajuste_detalle.descuento = float(informacion.get("desc_valor"))

                        if item.TieneIva():
                            ajuste_detalle.monto_iva = redondeo(float(informacion.get("subtotal"))*IVA, 2)
                        if item.TieneICE():
                            ajuste_detalle.porcentaje_ice = float(informacion.get("ice"))

                        ajuste_detalle.usuario_creacion = request.user.username
                        ajuste_detalle.fecha_creacion = dia_actual
                        ajuste_detalle.save()

            else:
                flag += 1
                messages.error(request, u"Error al seleccionar costo debe ingresar un valor "
                                        u"para realizar la nota de credito")
        cont += 1

    return flag, parametro

def GuardarMayorNotaCredito(formulario, cabecera_cont, ajuste_cabecera, dia_actual, request):
    '''
    Función que Genera el Asiento Contable de la Nota de Crédito
    :param formulario:
    :param cabecera_cont:
    :param ajuste_cabecera:
    :param cursor:
    :param dia_actual:
    :param request:
    :return:
    '''
    tipo_c = TipoComprobante.objects.get(id=22)             # Comprobante Nota Crédito

    if formulario.getConcepto() is None or formulario.getConcepto() == "":
        cabecera_cont.concepto_comprobante = concepto_cuenta_asiento_nota_credito(ajuste_cabecera)
    else:

        if len(unicode(formulario.getConcepto())) < 50:
            print ('posible lado donde se cae el sistema')
            print str(ajuste_cabecera.num_comp)
            print unicode(formulario.getConcepto())
            print 'el concepto unicode del fors'
            cabecera_cont.concepto_comprobante = concepto_cuenta_asiento_nota_credito(ajuste_cabecera)+ u" con concepto:"+ unicode(formulario.getConcepto())

            print (cabecera_cont.concepto_comprobante)
            print('lod e cabeceracomprobante')
        else:
            cabecera_cont.concepto_comprobante = concepto_cuenta_asiento_nota_credito(ajuste_cabecera)


    cabecera_cont.fecha = formulario.getFecha()
    cabecera_cont.tipo_comprobante = tipo_c
    cabecera_cont.numero_comprobante = ajuste_cabecera.num_comp  # Retorna el Num_Comp

    #############################################
    #  Se valida la fecha de cierre de la       #
    #               empresa                     #
    #############################################
    if not esta_periodo_contable(cabecera_cont.fecha):
        messages.error(request, u"La fecha del asiento no puede ser menor a la del " u"cierre contable")
        transaction.rollback()

    cabecera_cont.fecha_creacion = dia_actual
    cabecera_cont.usuario_creacion = request.user.username
    cabecera_cont.save()

def GuardarAsientoContableNotaCredito(cabecera_comprobante_nota_credito, ajuste_cabecera, monto_total, iva_valor, ice_valor, descuento_iva, descuento_cero, venta, request, dia_actual):
    '''
    Función que realiza el asiento Contable Nota Crédito
    con todas las cuentas correspondientes a la transacción
    :param cabecera_comprobante_nota_credito:
    :param ajuste_cabecera:
    :param monto_total:
    :param iva_valor:
    :param ice_valor:
    :param descuento_iva:
    :param descuento_cero:
    :param venta:
    :param request:
    :param dia_actual:
    :return contador:
    '''
    contador = 0
    #############################################################
    #         Detalle de comprobante Contable - CxC             #
    #############################################################
    detalle_comp_cxc = Detalle_Comp_Contable()
    detalle_comp_cxc.cabecera_contable = cabecera_comprobante_nota_credito
    try:
        detalle_comp_cxc.centro_costo = Centro_Costo.objects.filter(status=1).get(id=1)
    except:
        pass

    detalle_comp_cxc.plan_cuenta = ajuste_cabecera.cliente.plan_cuenta
    detalle_comp_cxc.detalle = concepto_cuenta_asiento_nota_credito(ajuste_cabecera)
    detalle_comp_cxc.dbcr = 'H'
    detalle_comp_cxc.fecha_asiento = cabecera_comprobante_nota_credito.fecha
    detalle_comp_cxc.valor = redondeo(monto_total, 2)
    detalle_comp_cxc.fecha_creacion = dia_actual
    detalle_comp_cxc.usuario_creacion = request.user.username
    detalle_comp_cxc.save()

    ######################################################################
    #         Detalle de comprobante Contable - Iva_Cobrado Nota-Crédito #
    ######################################################################
    if iva_valor > 0.0:
        try:
            detalle_comp_iva_cobrado = Detalle_Comp_Contable()
            detalle_comp_iva_cobrado.cabecera_contable = cabecera_comprobante_nota_credito
            try:
                detalle_comp_iva_cobrado.centro_costo = Centro_Costo.objects.get(id=1)
            except:
                pass
            detalle_comp_iva_cobrado.plan_cuenta = PlanCuenta.objects.filter(status=1).get(tipo__alias=8)  # IVA COBRADO
            detalle_comp_iva_cobrado.fecha_asiento = cabecera_comprobante_nota_credito.fecha
            detalle_comp_iva_cobrado.detalle = concepto_cuenta_asiento_nota_credito(ajuste_cabecera)
            detalle_comp_iva_cobrado.dbcr = 'D'
            detalle_comp_iva_cobrado.valor = redondeo(iva_valor)
            detalle_comp_iva_cobrado.fecha_creacion = dia_actual
            detalle_comp_iva_cobrado.usuario_creacion = request.user.username
            detalle_comp_iva_cobrado.save()
        except PlanCuenta.DoesNotExist:
            contador += 1
            messages.error(request, u"Error la cuenta de Iva Cobrado no posee un tipo del plan de cuenta asignado")

    ######################################################################
    #         Detalle de comprobante Contable - ICE_Nota Credito         #
    ######################################################################
    if ice_valor > 0.0:
        try:
            detalle_comp_ice = Detalle_Comp_Contable()
            detalle_comp_ice.cabecera_contable = cabecera_comprobante_nota_credito
            try:
                detalle_comp_ice.centro_costo = Centro_Costo.objects.filter(status=1).get(id=1)
            except:
                pass
            detalle_comp_ice.plan_cuenta = PlanCuenta.objects.get(status=1, tipo__alias=9)  # Impuesto al ICE
            detalle_comp_ice.detalle = concepto_cuenta_asiento_nota_credito(ajuste_cabecera)
            detalle_comp_ice.dbcr = 'D'
            detalle_comp_ice.fecha_asiento = cabecera_comprobante_nota_credito.fecha
            detalle_comp_ice.valor = redondeo(ice_valor, 2)
            detalle_comp_ice.fecha_creacion = dia_actual
            detalle_comp_ice.usuario_creacion = request.user.username
            detalle_comp_ice.save()
        except PlanCuenta.DoesNotExist:
            contador += 1
            messages.error(request, u"Error la cuenta de ICE no posee un tipo del plan de cuenta asignado")

    ############################################################################################
    # Revisar Asiento Contable de Descuentos                                                   #
    ############################################################################################
    if venta.tipo_descuento_combo != 0:
        try:
            detalle_comp_descuento = Detalle_Comp_Contable()
            detalle_comp_descuento.cabecera_contable = cabecera_comprobante_nota_credito
            try:
                detalle_comp_descuento.centro_costo = Centro_Costo.objects.get(id=1)
            except:
                pass
            detalle_comp_descuento.plan_cuenta = PlanCuenta.objects.get(status=1, tipo__alias=10)  # Descuento
            detalle_comp_descuento.fecha_asiento = cabecera_comprobante_nota_credito.fecha
            detalle_comp_descuento.dbcr = 'H'
            detalle_comp_descuento.detalle = concepto_cuenta_asiento_nota_credito(ajuste_cabecera)
            detalle_comp_descuento.valor = redondeo((descuento_iva + descuento_cero), 2)
            detalle_comp_descuento.fecha_creacion = dia_actual
            detalle_comp_descuento.usuario_creacion = request.user.username
            detalle_comp_descuento.save()
        except PlanCuenta.DoesNotExist:
            contador += 1
            messages.error(request, u"Error la cuenta de Descuento no posee un tipo del plan de cuenta asignado")

    return contador

def GuardarAsientoCtasServicioNotaCredito(cabecera_comprobante_nota_credito, ajuste_cabecera, flag_reg, formset, now, request):
    '''
    Función que Genera la Contra Cuenta para el escenario de Nota de Crédito
    si el item es un servicio donde se establece una cuenta definida para el
    escenario correspondiente
    :param cabecera_comprobante_nota_credito:
    :param flag_reg:
    :param formset:
    :param now:
    :param request:
    :return :
    '''
    total_costo = 0.0
    # Cuenta Asignada para realizar la devolución
    # (Nota - Crédito en el escenario de que sean servicios)
    cuenta = PlanCuenta.objects.get(status=1, tipo__alias=18)
    cont = 1

    for form in formset:
        informacion2 = form.cleaned_data
        sel = request.POST.get("tipo-"+str(cont))   # Seleccion encargada de los radio button

        if sel == "1":   # Cantidad
            if flag_reg == 1:   # Si es Manual
                pass    # Por implentar

            else:               # Automática

                if informacion2.get("cant_dev") != "":
                    total_costo += float(informacion2.get("cant_dev"))*float(informacion2.get("precio"))

        elif sel == "2":   # Costo

            if flag_reg == 1:   # Si es Manual
                pass    # Por implentar

            else:               # Automática

                if informacion2.get("subtotal") != "":
                    total_costo += float(informacion2.get("subtotal"))

    detalle_asiento_inventario = Detalle_Comp_Contable()
    detalle_asiento_inventario.cabecera_contable = cabecera_comprobante_nota_credito
    detalle_asiento_inventario.fecha_asiento = cabecera_comprobante_nota_credito.fecha
    detalle_asiento_inventario.plan_cuenta = cuenta
    detalle_asiento_inventario.dbcr = 'D'
    detalle_asiento_inventario.detalle = concepto_cuenta_asiento_nota_credito(ajuste_cabecera)
    detalle_asiento_inventario.valor = redondeo(total_costo, 2)
    detalle_asiento_inventario.fecha_creacion = now
    detalle_asiento_inventario.usuario_creacion = request.user.username
    detalle_asiento_inventario.save()


class Item_Cantidad():
    """
    Clase que me ayuda para  agrupar los items que realizan movimiento
    en inventario y la cantidad que debe ser movido
    """
    def __init__(self, item, cantidad, cantidad_devolver=0.0, costo=0.0):
        self.item = item

        self.cantidad = cantidad
        # Cta. Devolver (input)
        self.cantidad_devolver = cantidad_devolver
        self.costo = costo

def ItemsMovimientoInventario(detalleform, flag_reg, request):
    '''
    Función que me retorna la lista de los items y el parametro flag_reg
    determina si la nota de crédito es manual o automática
    :param detalleform:
    :param flag_reg:
    :param request:
    :return:
    '''
    cont = 0
    items = []

    for form in detalleform:
        informacion = form.cleaned_data
        sel = request.POST.get("tipo-"+str(cont))   # Seleccion encargada de los radio button
        if sel == "1":     # CANTIDAD
            if flag_reg == 1:   # MANUAL
                if informacion.get("cant_dev") != "" and informacion.get("cant_dev") is not None and informacion.get("act_inven"):
                    item = Item.objects.get(id=informacion.get("id_item"))

                    if item.TipoItem() == 1:       # Producto
                        item_cantidad_mov = Item_Cantidad(item, redondeo(informacion.get("cant_dev"), 2), informacion.get("subtotal"), redondeo(informacion.get("cant_dev"), 2))
                        items.append(item_cantidad_mov)

            else:               # AUTOMÁTICA

                detalle_venta = Venta_Detalle.objects.filter(status=1).get(id=informacion.get("id_detalle_venta"))
                if informacion.get("cant_dev") != "":
                    if detalle_venta.cantidad - detalle_venta.cant_devuelta < detalle_venta.cant_entregada:
                        item = Item.objects.get(id=detalle_venta.item_id)
                        if item.TipoItem() == 1:    # Producto
                            item_cantidad_mov = Item_Cantidad(item, detalle_venta.cant_entregada - detalle_venta.cantidad - detalle_venta.cant_devuelta, redondeo(informacion.get("cant_dev"), 2), informacion.get("subtotal"))
                            items.append(item_cantidad_mov)
        cont += 1

    return items


# Revisar implementación Asiento Nota Credito Movimiento de Ctas. Items (Producots)
def GuardarAsientoCtasInventarioNotaCredito(cabecera_comprobante, flag_reg, formset, now, request):
    '''
    Función que Guarda las Ctas de Inventario en el asiento de la Nota de Crédito
    si existen movimientos de inventario es decir si son PRODUCTOS, la bandera flag_reg
    determina si es manual (De ventas que no se encuentran registradas en el sistema) ó
    automática (De venta registradas en el sistema).
    Ej:
    flag_reg = 1 -> Manual
    flag_reg = 2 -> Automática
    :param cabecera_comprobante_inventario:
    :param flag_reg:
    :param formset:
    :param now:
    :param request:
    :return:
    '''
    cuentas_item_ids_inventario = []                                     # Arreglo de cuenta de item_inventario
    detalle_asiento_list_inventario = []                                 # Arreglo de detalle asiento de cta_inventario

    # Retorna lista de movimiento de items
    lista_items_movimiento = ItemsMovimientoInventario(formset, flag_reg, request)
    print "listaaaaaaa de items en movimiento"

    if len(lista_items_movimiento) > 0:

        for obj in lista_items_movimiento:
            if obj.item.categoria_item.plan_cuenta_venta.id in cuentas_item_ids_inventario:
                for i in range(0, len(detalle_asiento_list_inventario)):
                    if detalle_asiento_list_inventario[i].id_cuenta == obj.item.categoria_item.plan_cuenta_venta.id:
                        subtotal = float(obj.cantidad)*float(obj.costo)
                        total_temp = float(detalle_asiento_list_inventario[i].costo) + subtotal
                        detalle_asiento_list_inventario[i].costo = total_temp
            else:
                total = float(obj.cantidad)*float(obj.costo)
                cuentas_item_ids_inventario.append(obj.item.categoria_item.plan_cuenta_venta.id)
                detalle_asiento_list_inventario.append(Detalle_Cont_Inventario(obj.item.categoria_item.plan_cuenta_venta.id, total))

    if len(detalle_asiento_list_inventario) > 0:
        for obj in detalle_asiento_list_inventario:
            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = cabecera_comprobante
            detalle_asiento_inventario.fecha_asiento = cabecera_comprobante.fecha
            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
            detalle_asiento_inventario.dbcr = 'D'
            detalle_asiento_inventario.detalle = "Nota Credito: Cuenta "+unicode(detalle_asiento_inventario.plan_cuenta.descripcion)
            detalle_asiento_inventario.valor = obj.costo
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()


    for form in formset:
        informacion2 = form.cleaned_data
        item = Item.objects.filter(status=1).get(id=informacion2.get("id_item"))
        if flag_reg == 1:   # Si es Manual
            if informacion2.get("cant_dev") != "" and informacion2.get("act_inven"):  # Si tiene cantidad a devolver
                                                                                      # y el check de movimiento de inventario

                if item.TipoItem() == 1:                                                          # Si el item es de tipo producto

                    if item.categoria_item.plan_cuenta_venta.id in cuentas_item_ids_inventario:   # Si ya se encuentra un id nuevo de item
                        for i in range(0, len(detalle_asiento_list_inventario)):
                            if detalle_asiento_list_inventario[i].id_cuenta == item.categoria_item.plan_cuenta_venta.id:
                                total_temp = detalle_asiento_list_inventario[i].costo + float(informacion2.get("cant_dev"))*float(informacion2.get("precio"))
                                detalle_asiento_list_inventario[i].costo = total_temp
                    else:
                        total = float(informacion2.get("cant_dev"))*float(informacion2.get("precio"))
                        cuentas_item_ids_inventario.append(item.categoria_item.plan_cuenta_venta.id)
                        detalle_asiento_list_inventario.append(Detalle_Cont_Inventario(item.categoria_item.plan_cuenta_venta.id, total))

        else:   # Automatica

            if informacion2.get("cant_dev") != "":
                if float(informacion2.get("cant_dev")) > (float(informacion2.get("cant_vendida")) - float(informacion2.get("cant_ent"))):
                    if item.TipoItem() == 1:
                        if item.categoria_item.plan_cuenta_venta.id in cuentas_item_ids_inventario:   # Si ya se encuentra un id nuevo de item
                            for i in range(0, len(detalle_asiento_list_inventario)):
                                if detalle_asiento_list_inventario[i].id_cuenta == item.categoria_item.plan_cuenta_venta.id:
                                    total_temp = detalle_asiento_list_inventario[i].costo + float(informacion2.get("cant_dev")*float(informacion2.get("precio")))
                                    detalle_asiento_list_inventario[i].costo = total_temp
                        else:
                            total = float(informacion2.get("cant_dev"))*float(informacion2.get("precio"))
                            cuentas_item_ids_inventario.append(item.categoria_item.plan_cuenta_venta.id)
                            detalle_asiento_list_inventario.append(Detalle_Cont_Inventario(item.categoria_item.plan_cuenta_venta.id, total))

    if len(detalle_asiento_list_inventario) > 0:
        for obj in detalle_asiento_list_inventario:
            detalle_asiento_inventario = Detalle_Comp_Contable()
            detalle_asiento_inventario.cabecera_contable = ""
            detalle_asiento_inventario.fecha_asiento = ""
            detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
            detalle_asiento_inventario.dbcr = 'D'
            detalle_asiento_inventario.detalle = "Nota Credito: Cuenta "+unicode(detalle_asiento_inventario.plan_cuenta.descripcion)
            detalle_asiento_inventario.valor = obj.costo
            detalle_asiento_inventario.fecha_creacion = now
            detalle_asiento_inventario.usuario_creacion = request.user.username
            detalle_asiento_inventario.save()

# Inventario
def GuardarInventarioCabeceraNotaCredito(inventario_cabecera, ajuste_cabecera,  now,  request):
    '''
    Función que registra en Inventario Cabecera la transacción generada por la Nota de Crédito
    siempre y cuando se realicen movimientos en inventario (Productos que regresen al stock)
    :param inventario_cabecera:
    :param num_doc:
    :param formulario_dev:
    :param venta:
    :param now:
    :param request:
    :return:
    '''
    tipo_comprobante = TipoComprobante.objects.filter(status=1).get(id=14)  # Ingreso
    inventario_cabecera.tipo_comprobante = tipo_comprobante
    inventario_cabecera.fecha_reg = ajuste_cabecera.fecha_emi
    inventario_cabecera.num_comp = get_num_comp(tipo_comprobante.id, ajuste_cabecera.fecha_emi, True, request)
    # Documento: Nota de Crédito
    inventario_cabecera.documento = Documento.objects.get(id=4)
    inventario_cabecera.cliente_proveedor_id = ajuste_cabecera.cliente.id
    inventario_cabecera.num_doc = ajuste_cabecera.num_doc.serie+"-"+ajuste_cabecera.num_doc
    inventario_cabecera.detalle = u"Ingreso de Inventario: "+concepto_cuenta_asiento_nota_credito(ajuste_cabecera)

    ####################################################################################################################
    #                                                   NOTA                                                           #
    ####################################################################################################################
    # Consultar el tipo de movimiento de inventario que debe ser cuando se realiza la nota de crédito
    inventario_cabecera.tipo_mov_inventario = Tipo_Mov_Inventario.objects.filter(status=1).get(id=2)    # Ventas
    inventario_cabecera.fecha_creacion = now
    inventario_cabecera.usuario_creacion = request.user.username
    inventario_cabecera.save()


def GuardarInventarioDetalleNotaCredito(detalleform, flag_reg, cabecera_inventario, now, request):
    '''
    Función que guarda en Inventario Detalle según corresponda,
    utiliza una bandera que determina si la transacción se está
    haciendo de manera manual (Ventas que no se encuentran registradas en el sistema),
    o automática(Ventas registradas en el sistema) y si son productos que regresan al
    stock.
    :param detalleform:
    :param flag_reg:
    :param cabecera_inventario:
    :param now:
    :param request:
    :return:
    '''

    # Función que me retorna el objeto Item_Cantidad
    lista_items = ItemsMovimientoInventario(detalleform, flag_reg, request)

    if len(lista_items) > 0:
        for obj in lista_items:
            detalle_inventario = Inventario_Detalle()
            detalle_inventario.inventario_cabecera = cabecera_inventario
            detalle_inventario.bodega = Bodega.objects.filter(status=1).get(id=1)    # Bodega de Prueba
            detalle_inventario.item = obj.item
            detalle_inventario.plan_cuenta = obj.item.categoria_item.plan_cuenta_compra
            detalle_inventario.costo = obj.item.costo                                    # Costo del Item
            detalle_inventario.cantidad = obj.cantidad
            detalle_inventario.fecha_creacion = now
            detalle_inventario.usuario_creacion = request.user.username
            detalle_inventario.save()


# Contabilidad de Inventario
def GuardarMayorContableInventarioNotaCredito(cabecera_comprobante_inventario, inventario_cabecera, now, request):
    '''
    Función que guarda los datos del mayor cabecera inventario
    :param cabecera_comprobante_inventario:
    :param inventario_cabecera:
    :param now:
    :param request:
    :return:
    '''
    try:
        tipo_c = TipoComprobante.objects.filter(status=1).get(id=14)
        cabecera_comprobante_inventario.tipo_comprobante = tipo_c
    except:
        pass
    cabecera_comprobante_inventario.concepto_comprobante = "Ingreso de inventario por N/C"
    cabecera_comprobante_inventario.fecha = inventario_cabecera.fecha_reg
    cabecera_comprobante_inventario.numero_comprobante = inventario_cabecera.num_comp
    cabecera_comprobante_inventario.fecha_creacion = now
    cabecera_comprobante_inventario.usuario_creacion = request.user.username
    cabecera_comprobante_inventario.save()

def GuardarMayorInventarioDetalleNotaCredito(detalleform, flag_reg, cabecera_comprobante_inventario, now, request):
    '''
    Función que guarda en el asiento las cuentas de items para la transacción
    de Nota de Crédito dependiendo del parametro flag_reg si es automatica o manual
    :param detalleform:
    :param flag_reg:
    :param cabecera_comprobante_inventario:
    :param now:
    :param request:
    :return:
    '''
    #####################################################
    #         Detalle de Comprobante Contable Items     #
    #####################################################
    cuentas_item_ids_inventario = []                                    # Arreglo de cuenta de item_inventario
    cuentas_item_ids_costo_venta = []                                   # Arreglo de cuenta de item_costo_venta
    detalle_asiento_list_inventario = []                                # Arreglo de detalle asiento de cta_inventario
    detalle_asiento_list_costo_venta = []                               # Arreglo de detalle asiento de cta_costo_venta


    # Función que me retorna el objeto Item_Cantidad
    lista_items = ItemsMovimientoInventario(detalleform, flag_reg, request)

    if len(lista_items) > 0:
        for obj in lista_items:
            # Si ya se encuentra un id nuevo de item
            if obj.item.categoria_item.plan_cuenta_compra.id in cuentas_item_ids_inventario:
                for i in range(0, len(detalle_asiento_list_inventario)):
                    if detalle_asiento_list_inventario[i].id_cuenta == obj.item.categoria_item.plan_cuenta_compra.id:
                        total_temp = detalle_asiento_list_inventario[i].costo + float(obj.cantidad_devolver)*float(obj.item.costo)
                        detalle_asiento_list_inventario[i].costo = total_temp
            else:
                total_compra_inventario = float(obj.cantidad)*float(obj.item.costo)
                cuentas_item_ids_inventario.append(obj.item.categoria_item.plan_cuenta_compra.id)
                detalle_asiento_list_inventario.append(Detalle_Cont_Inventario(obj.item.categoria_item.plan_cuenta_compra.id, total_compra_inventario))

            # Si ya se encuentra un id nuevo de item
            if obj.item.categoria_item.plan_cuenta_costo_venta.id in cuentas_item_ids_costo_venta:
                for i in range(0, len(detalle_asiento_list_costo_venta)):
                    if detalle_asiento_list_costo_venta[i].id_cuenta == obj.item.categoria_item.plan_cuenta_costo_venta.id:
                        total_temp_costo_vta = detalle_asiento_list_costo_venta[i].costo + float(obj.cantidad)*float(obj.item.costo)
                        detalle_asiento_list_costo_venta[i].costo = total_temp_costo_vta

            else:
                total_costo_venta = float(obj.cantidad)*float(obj.item.costo)
                cuentas_item_ids_costo_venta.append(obj.item.categoria_item.plan_cuenta_costo_venta.id)
                detalle_asiento_list_costo_venta.append(Detalle_Cont_Inventario(obj.item.categoria_item.plan_cuenta_costo_venta.id, total_costo_venta))

        if len(detalle_asiento_list_inventario) > 0 and len(detalle_asiento_list_costo_venta) > 0:
            for obj in detalle_asiento_list_inventario:
                detalle_asiento_inventario = Detalle_Comp_Contable()
                detalle_asiento_inventario.cabecera_contable = cabecera_comprobante_inventario
                detalle_asiento_inventario.fecha_asiento = cabecera_comprobante_inventario.fecha
                detalle_asiento_inventario.dbcr = 'D'
                detalle_asiento_inventario.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
                detalle_asiento_inventario.detalle = "N/C: Cuenta "+unicode(detalle_asiento_inventario.plan_cuenta.descripcion)
                detalle_asiento_inventario.valor = obj.costo
                detalle_asiento_inventario.fecha_creacion = now
                detalle_asiento_inventario.usuario_creacion = request.user.username
                detalle_asiento_inventario.save()

            for obj in detalle_asiento_list_costo_venta:
                detalle_asiento_inventario_costo = Detalle_Comp_Contable()
                detalle_asiento_inventario_costo.cabecera_contable = cabecera_comprobante_inventario
                detalle_asiento_inventario_costo.fecha_asiento = cabecera_comprobante_inventario.fecha
                detalle_asiento_inventario_costo.dbcr = 'H'
                detalle_asiento_inventario_costo.plan_cuenta = PlanCuenta.objects.get(id=obj.id_cuenta)
                detalle_asiento_inventario_costo.detalle = "N/C: Cuenta "+unicode(detalle_asiento_inventario_costo.plan_cuenta.descripcion)
                detalle_asiento_inventario_costo.valor = obj.costo
                detalle_asiento_inventario_costo.fecha_creacion = now
                detalle_asiento_inventario_costo.usuario_creacion = request.user.username
                detalle_asiento_inventario_costo.save()
