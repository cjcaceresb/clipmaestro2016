#! /usr/bin/python
# -*- coding: UTF-8-*-

from reportes.formularios.reportes_form import *

from librerias.funciones.funciones_vistas import generar_pdf_get
from barpy import GS1128, Code128, Code128A, EAN13, QrCode, HtmlOutputter, PngOutputter
import requests
from xml.dom import minidom
from contabilidad.clases.class_ride_factura_electronica import *
import gc


def get_valueof_first_element_by_tagname(obj, tagname):
    try:
        return obj.getElementsByTagName(tagname)[0].firstChild.nodeValue
    except IndexError:
        return ""


def generar_ride_factura(request, venta):
    web_url = venta.web_url
    empresa = EmpresaXML()
    datos_fact = DatosFacturaXML()
    datos_cliente = DatosClienteXML()
    escontribuyenteespecial= ''
    empresa_general = EmpresaGeneral.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    logo_empresa = empresa_general.logotipo

    url = 'https://app.datil.co/ver/' + web_url + '/xml'
    a = requests.get(url, verify=False)

    url_logotipo = None
    if logo_empresa:
        url_logotipo = logo_empresa.name

    xmldoc = minidom.parseString(a.text.encode('UTF-8'))

    # Información del xml de la factura
    comprobante = xmldoc.getElementsByTagName('comprobante')[0]
    estado = xmldoc.getElementsByTagName('estado')[0].firstChild.nodeValue
    numero_auto = xmldoc.getElementsByTagName('numeroAutorizacion')[0].firstChild.nodeValue
    fecha_auto = xmldoc.getElementsByTagName('fechaAutorizacion')[0].firstChild.nodeValue

    factura = comprobante.childNodes[0]

    xmldoc_cdata = minidom.parseString(factura.data.encode('UTF-8')).childNodes[0]
    info_tributaria = xmldoc_cdata.getElementsByTagName('infoTributaria')[0]
    empresa.nombre_comer = empresa_general.nombre_comercial
    empresa.razon_social = empresa_general.razon_social
    empresa.direccion_matriz = empresa_general.direccion
    empresa.ruc = empresa_general.rucci

    datos_fact.ambiente = info_tributaria.getElementsByTagName('ambiente')[0].firstChild.nodeValue
    datos_fact.clave_acceso = info_tributaria.getElementsByTagName('claveAcceso')[0].firstChild.nodeValue
    datos_fact.emision = info_tributaria.getElementsByTagName('tipoEmision')[0].firstChild.nodeValue
    datos_fact.fecha_autorizacion = fecha_auto
    datos_fact.num_autorizacion = numero_auto
    estab = info_tributaria.getElementsByTagName('estab')[0].firstChild.nodeValue
    pto_emi = info_tributaria.getElementsByTagName('ptoEmi')[0].firstChild.nodeValue
    sec = info_tributaria.getElementsByTagName('secuencial')[0].firstChild.nodeValue
    datos_fact.num_factura = estab + "-" + pto_emi + "-" + sec

    info_factura = xmldoc_cdata.getElementsByTagName('infoFactura')[0]
    print 'es contribbb de las facturas?'
    try:
        if info_factura.getElementsByTagName('contribuyenteEspecial')[0].firstChild.nodeValue:
            escontribuyenteespecial = info_factura.getElementsByTagName('contribuyenteEspecial')[0].firstChild.nodeValue
        print escontribuyenteespecial
        print ':o :o'
    except:
        print 'no hay este dato en factura ride'
    datos_cliente.fecha_emision = info_factura.getElementsByTagName('fechaEmision')[0].firstChild.nodeValue
    datos_cliente.identificacion = info_factura.getElementsByTagName('identificacionComprador')[0].firstChild.nodeValue
    datos_cliente.razon_social = venta.cliente.razon_social
    datos_cliente.guia_remision = ""

    datos_fact.total_sin_impuesto = info_factura.getElementsByTagName('totalSinImpuestos')[0].firstChild.nodeValue
    datos_fact.total_descuento = info_factura.getElementsByTagName('totalDescuento')[0].firstChild.nodeValue
    datos_fact.propina = info_factura.getElementsByTagName('propina')[0].firstChild.nodeValue
    datos_fact.importe_total = info_factura.getElementsByTagName('importeTotal')[0].firstChild.nodeValue
    datos_fact.moneda = info_factura.getElementsByTagName('moneda')[0].firstChild.nodeValue

    total_con_impuesto = info_factura.getElementsByTagName('totalConImpuestos')[0]
    lista_total_con_impuesto = []
    for obj in total_con_impuesto.getElementsByTagName('totalImpuesto'):
        impuesto = ImpuestoDetalle()
        impuesto.codigo = obj.getElementsByTagName('codigo')[0].firstChild.nodeValue
        impuesto.codigo_porc = obj.getElementsByTagName('codigoPorcentaje')[0].firstChild.nodeValue
        impuesto.base_imponible = obj.getElementsByTagName('baseImponible')[0].firstChild.nodeValue
        impuesto.valor = obj.getElementsByTagName('valor')[0].firstChild.nodeValue
        lista_total_con_impuesto.append(impuesto)

    # Total IVA 12%
    subtotal_0 = 0.0
    subtotal_12 = 0.0
    no_objeto_imp = 0.0
    excento_iva = 0.0
    iva = 0.0
    ice = 0.0
    irbpnr = 0.0

    for obj in lista_total_con_impuesto:
        if obj.codigo == "2":  # IVA
            if obj.codigo_porc == "0":
                subtotal_0 += float(obj.valor)
            elif obj.codigo_porc == "2":
                subtotal_12 += float(obj.base_imponible)
                iva += float(obj.valor)
            elif obj.codigo_porc == "6":
                no_objeto_imp += float(obj.valor)
            elif obj.codigo_porc == "7":
                excento_iva += float(obj.valor)

        if obj.codigo == "3":  # ICE
            pass

        if obj.codigo == "5":  # IRBPNR
            irbpnr += float(obj.valor)

    subtotal = subtotal_0 + subtotal_12 + no_objeto_imp + excento_iva
    total = subtotal + ice + irbpnr + iva + float(datos_fact.propina)

    # Detalles de la factura
    detalles = xmldoc_cdata.getElementsByTagName('detalles')[0]
    lista_detalle_factura = []
    for obj in detalles.getElementsByTagName('detalle'):
        detalles_factura = DetallesFactura()
        detalles_factura.codigo_principal = get_valueof_first_element_by_tagname(obj, 'codigoPrincipal')
        detalles_factura.codigo_auxiliar = get_valueof_first_element_by_tagname(obj, 'codigoAuxiliar')
        detalles_factura.descripcion = get_valueof_first_element_by_tagname(obj, 'descripcion')
        detalles_factura.cantidad = get_valueof_first_element_by_tagname(obj, 'cantidad')
        detalles_factura.precio_unit = get_valueof_first_element_by_tagname(obj, 'precioUnitario')
        detalles_factura.descuento = get_valueof_first_element_by_tagname(obj, 'descuento')
        detalles_factura.precio_tot_sin_imp = get_valueof_first_element_by_tagname(obj, 'precioTotalSinImpuesto')

        impuestos = obj.getElementsByTagName('impuestos')[0]
        lista_impuestos = []
        for obj_imp in impuestos.getElementsByTagName('impuesto'):
            impuesto = ImpuestoDetalle()
            impuesto.codigo = get_valueof_first_element_by_tagname(obj_imp, 'codigo')
            impuesto.codigo_porc = get_valueof_first_element_by_tagname(obj_imp, 'codigoPorcentaje')
            impuesto.tarifa = get_valueof_first_element_by_tagname(obj_imp, 'tarifa')
            impuesto.base_imponible = get_valueof_first_element_by_tagname(obj_imp, 'baseImponible')
            impuesto.valor = get_valueof_first_element_by_tagname(obj_imp, 'valor')
            lista_impuestos.append(impuesto)

        detalles_factura.impuestos.append(lista_impuestos)

        lista_detalle_factura.append(detalles_factura)


    concepto = []

    try:
        info_adicional = xmldoc_cdata.getElementsByTagName('infoAdicional')[0]
        for obj in info_adicional.getElementsByTagName('campoAdicional'):
            #nombre = obj.attributes["nombre"].value
            valor = obj.firstChild.nodeValue
            concepto.append(valor)
    except:
        pass

    code = Code128(datos_fact.clave_acceso, "B")
    outputter = code.outputter_for("to_png")
    barcode_png = open("image.png", "wb")
    barcode_png.write(outputter.to_png(margin=5, height=200, width=500, xdim=2, ydim=2))
    barcode_png.close()

    data = {"request": request,
             "empresa": empresa,
             "empresa_general": empresa_general,
             "datos_fact": datos_fact,
             "lista_detalle_factura": lista_detalle_factura,
             "url_logotipo": url_logotipo,
             "subtotal_0": subtotal_0,
             "subtotal_12": subtotal_12,
             "no_objeto_imp": no_objeto_imp,
             "excento_iva": excento_iva,
             "iva": iva,
             "ice": ice,
             "irbpnr": irbpnr,
             "subtotal": subtotal,
             "concepto": concepto,
             "total": total,
             "datos_cliente": datos_cliente,
             "escontribuyenteespecial": escontribuyenteespecial
            }

    return data


def generar_ride_retencion(request, web_url):
    gc.collect()
    logo_empresa = Empresa.objects.all()[0].empresa_general.logotipo
    empresa_general = EmpresaGeneral.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    escontribuyenteespecial= ''
    url = 'https://app.datil.co/ver/' + web_url + '/xml'
    a = requests.get(url, verify=False)

    url_logotipo = None
    if logo_empresa:
        url_logotipo = logo_empresa.name

    xmldoc = minidom.parseString(a.text.encode('UTF-8'))

    # Información del xml de la factura
    comprobante = xmldoc.getElementsByTagName('comprobante')[0]
    estado = xmldoc.getElementsByTagName('estado')[0].firstChild.nodeValue
    numero_auto = xmldoc.getElementsByTagName('numeroAutorizacion')[0].firstChild.nodeValue
    fecha_auto = xmldoc.getElementsByTagName('fechaAutorizacion')[0].firstChild.nodeValue

    retencion = comprobante.childNodes[0]

    xmldoc_cdata = minidom.parseString(retencion.data.encode('UTF-8')).childNodes[0]
    info_tributaria_tag = xmldoc_cdata.getElementsByTagName('infoTributaria')[0]
    info_tributaria = InfoTributariaRetencionXml()
    info_tributaria.nombre_comercial = empresa_general.nombre_comercial
    info_tributaria.razon_social = empresa_general.razon_social
    info_tributaria.direccion_matriz = empresa_general.direccion
    info_tributaria.ruc = empresa_general.rucci

    info_tributaria.ambiente = info_tributaria_tag.getElementsByTagName('ambiente')[0].firstChild.nodeValue
    info_tributaria.clave_acceso = info_tributaria_tag.getElementsByTagName('claveAcceso')[0].firstChild.nodeValue
    info_tributaria.emision = info_tributaria_tag.getElementsByTagName('tipoEmision')[0].firstChild.nodeValue
    info_tributaria.fecha_autorizacion = fecha_auto
    info_tributaria.num_autorizacion = numero_auto
    info_tributaria.establecimiento = info_tributaria_tag.getElementsByTagName('estab')[0].firstChild.nodeValue
    info_tributaria.pto_emision = info_tributaria_tag.getElementsByTagName('ptoEmi')[0].firstChild.nodeValue
    info_tributaria.secuencial = info_tributaria_tag.getElementsByTagName('secuencial')[0].firstChild.nodeValue


    info_comp_ret_tag = xmldoc_cdata.getElementsByTagName('infoCompRetencion')[0]
    print 'es contribbb de retencionnn?'
    try:
        if info_comp_ret_tag.getElementsByTagName('contribuyenteEspecial')[0].firstChild.nodeValue:
            escontribuyenteespecial = info_comp_ret_tag.getElementsByTagName('contribuyenteEspecial')[0].firstChild.nodeValue
        print escontribuyenteespecial
        print ':o :o'
    except:
        print 'no tiene'
    info_comp_ret = InfoCompRetencion()
    info_comp_ret.fecha_emi = info_comp_ret_tag.getElementsByTagName('fechaEmision')[0].firstChild.nodeValue
    info_comp_ret.dir_establecimiento = info_comp_ret_tag.getElementsByTagName('dirEstablecimiento')[0].firstChild.nodeValue
    info_comp_ret.obligado_contabilidad = info_comp_ret_tag.getElementsByTagName('obligadoContabilidad')[0].firstChild.nodeValue
    info_comp_ret.tipo_ident_suj_rete = info_comp_ret_tag.getElementsByTagName('tipoIdentificacionSujetoRetenido')[0].firstChild.nodeValue
    info_comp_ret.razon_social_sujet_retenido = info_comp_ret_tag.getElementsByTagName('razonSocialSujetoRetenido')[0].firstChild.nodeValue
    info_comp_ret.identificacion_suj_ret = info_comp_ret_tag.getElementsByTagName('identificacionSujetoRetenido')[0].firstChild.nodeValue
    info_comp_ret.periodo_fiscal = info_comp_ret_tag.getElementsByTagName('periodoFiscal')[0].firstChild.nodeValue


    impuestos_tag = xmldoc_cdata.getElementsByTagName('impuestos')[0]
    lista_impuestos = []
    for obj in impuestos_tag.getElementsByTagName('impuesto'):
        impuesto = ImpuestoRetencionXml()
        impuesto.codigo = obj.getElementsByTagName('codigo')[0].firstChild.nodeValue
        impuesto.codigo_ret = obj.getElementsByTagName('codigoRetencion')[0].firstChild.nodeValue
        impuesto.base_imponible = obj.getElementsByTagName('baseImponible')[0].firstChild.nodeValue
        impuesto.porcentaje_retener = obj.getElementsByTagName('porcentajeRetener')[0].firstChild.nodeValue
        impuesto.valor_retenido = obj.getElementsByTagName('valorRetenido')[0].firstChild.nodeValue
        impuesto.cod_doc_sustento = obj.getElementsByTagName('codDocSustento')[0].firstChild.nodeValue
        impuesto.num_doc_sustento = obj.getElementsByTagName('numDocSustento')[0].firstChild.nodeValue
        impuesto.fecha_emi_sust = obj.getElementsByTagName('fechaEmisionDocSustento')[0].firstChild.nodeValue

        lista_impuestos.append(impuesto)

    concepto = []
    try:
        info_adicional = xmldoc_cdata.getElementsByTagName('infoAdicional')[0]
        for obj in info_adicional.getElementsByTagName('campoAdicional'):
            #nombre = obj.attributes["nombre"].value
            valor = obj.firstChild.nodeValue
            concepto.append(valor)
    except:
        pass

    code = Code128(info_tributaria.clave_acceso, "B")
    outputter = code.outputter_for("to_png")
    barcode_png = open("image.png", "wb")
    barcode_png.write(outputter.to_png(margin=5, height=200, width=500, xdim=2, ydim=2))
    barcode_png.close()

    data = {"request": request,
             "url_logotipo": url_logotipo,
             "empresa_general": empresa_general,
             "info_tributaria": info_tributaria,
             "info_comp_ret": info_comp_ret,
             "lista_impuestos": lista_impuestos,
             "concepto": concepto,
             "escontribuyenteespecial": escontribuyenteespecial
            }

    return data


def generar_ride_nota_credito(request, web_url):
    info_tributaria = InfoTributariaRetencionXml()
    info_nota_credito = InfoNotaCredito()
    empresa_general = EmpresaGeneral.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    logo_empresa = empresa_general.logotipo

    url = 'https://app.datil.co/ver/' + web_url + '/xml'
    a = requests.get(url, verify=False)
    escontribuyenteespecial= ''
    url_logotipo = None
    if logo_empresa:
        url_logotipo = logo_empresa.name

    xmldoc = minidom.parseString(a.text.encode('UTF-8'))

    # Información del xml de la factura
    comprobante = xmldoc.getElementsByTagName('comprobante')[0]
    estado = xmldoc.getElementsByTagName('estado')[0].firstChild.nodeValue
    numero_auto = xmldoc.getElementsByTagName('numeroAutorizacion')[0].firstChild.nodeValue
    fecha_auto = xmldoc.getElementsByTagName('fechaAutorizacion')[0].firstChild.nodeValue

    nota_credito = comprobante.childNodes[0]

    xmldoc_cdata = minidom.parseString(nota_credito.data.encode('UTF-8')).childNodes[0]
    info_tributaria_tag = xmldoc_cdata.getElementsByTagName('infoTributaria')[0]
    info_nota_tag = xmldoc_cdata.getElementsByTagName('infoNotaCredito')[0]
    print ':o :o :o'
    #if info_tributaria_tag.getElementsByTagName('contribuyenteEspecial')[0].firstChild.nodeValue is not None:
    #print info_factura_tag.getElementsByTagName('obligadoContabilidad')[0].firstChild.nodeValue
    #else:
        #print 'no tiene el dato de contrib especial :o'
    print 'es o no obligado'
    print 'aaa :o'
    info_tributaria.ambiente = info_tributaria_tag.getElementsByTagName('ambiente')[0].firstChild.nodeValue
    info_tributaria.tipo_emision = info_tributaria_tag.getElementsByTagName('tipoEmision')[0].firstChild.nodeValue
    info_tributaria.razon_social = empresa_general.razon_social
    info_tributaria.nombre_comercial = empresa_general.nombre_comercial
    info_tributaria.ruc = empresa_general.rucci
    info_tributaria.clave_acceso = info_tributaria_tag.getElementsByTagName('claveAcceso')[0].firstChild.nodeValue
    info_tributaria.cod_doc = info_tributaria_tag.getElementsByTagName('codDoc')[0].firstChild.nodeValue
    info_tributaria.establecimiento = info_tributaria_tag.getElementsByTagName('estab')[0].firstChild.nodeValue
    info_tributaria.pto_emision = info_tributaria_tag.getElementsByTagName('ptoEmi')[0].firstChild.nodeValue
    info_tributaria.secuencial = info_tributaria_tag.getElementsByTagName('secuencial')[0].firstChild.nodeValue
    info_tributaria.direccion_matriz = empresa_general.direccion

    info_nota_credito_tag = xmldoc_cdata.getElementsByTagName('infoNotaCredito')[0]
    try:
        print 'es contribbb?'
        if info_nota_credito_tag.getElementsByTagName('contribuyenteEspecial')[0].firstChild.nodeValue:
            escontribuyenteespecial = info_nota_credito_tag.getElementsByTagName('contribuyenteEspecial')[0].firstChild.nodeValue
        print escontribuyenteespecial
        print ':o :o'
    except:
        print 'no es contribbb nota credito?'
    info_nota_credito.fecha_emision = info_nota_credito_tag.getElementsByTagName('fechaEmision')[0].firstChild.nodeValue
    info_nota_credito.dir_establecimiento = info_nota_credito_tag.getElementsByTagName('dirEstablecimiento')[0].firstChild.nodeValue
    info_nota_credito.tipo_identificacion_comprador = info_nota_credito_tag.getElementsByTagName('tipoIdentificacionComprador')[0].firstChild.nodeValue
    info_nota_credito.razon_social_comprador = info_nota_credito_tag.getElementsByTagName('razonSocialComprador')[0].firstChild.nodeValue
    info_nota_credito.identificacion_comprador = info_nota_credito_tag.getElementsByTagName('identificacionComprador')[0].firstChild.nodeValue
    info_nota_credito.obligado_contabilidad = info_nota_credito_tag.getElementsByTagName('obligadoContabilidad')[0].firstChild.nodeValue
    info_nota_credito.cod_doc_modificado = info_nota_credito_tag.getElementsByTagName('codDocModificado')[0].firstChild.nodeValue
    info_nota_credito.num_doc_modificado = info_nota_credito_tag.getElementsByTagName('numDocModificado')[0].firstChild.nodeValue
    info_nota_credito.fecha_emision_doc_sustento = info_nota_credito_tag.getElementsByTagName('fechaEmisionDocSustento')[0].firstChild.nodeValue
    info_nota_credito.total_sin_imp = info_nota_credito_tag.getElementsByTagName('totalSinImpuestos')[0].firstChild.nodeValue
    info_nota_credito.valor_modificacion = info_nota_credito_tag.getElementsByTagName('valorModificacion')[0].firstChild.nodeValue
    info_nota_credito.moneda = info_nota_credito_tag.getElementsByTagName('moneda')[0].firstChild.nodeValue
    info_nota_credito.motivo = info_nota_credito_tag.getElementsByTagName('motivo')[0].firstChild.nodeValue

    total_con_impuesto = info_nota_credito_tag.getElementsByTagName('totalConImpuestos')[0]
    lista_total_con_impuesto = []
    for obj in total_con_impuesto.getElementsByTagName('totalImpuesto'):
        impuesto = ImpuestoDetalle()
        impuesto.codigo = obj.getElementsByTagName('codigo')[0].firstChild.nodeValue
        impuesto.codigo_porc = obj.getElementsByTagName('codigoPorcentaje')[0].firstChild.nodeValue
        impuesto.base_imponible = obj.getElementsByTagName('baseImponible')[0].firstChild.nodeValue
        impuesto.valor = obj.getElementsByTagName('valor')[0].firstChild.nodeValue
        lista_total_con_impuesto.append(impuesto)

    # Total IVA 12%
    subtotal_0 = 0.0
    subtotal_12 = 0.0
    no_objeto_imp = 0.0
    excento_iva = 0.0
    iva = 0.0
    ice = 0.0
    irbpnr = 0.0

    for obj in lista_total_con_impuesto:
        if obj.codigo == "2":  # IVA
            if obj.codigo_porc == "0":
                subtotal_0 += float(obj.valor)
            elif obj.codigo_porc == "2":
                subtotal_12 += float(obj.base_imponible)
                iva += float(obj.valor)
            elif obj.codigo_porc == "6":
                no_objeto_imp += float(obj.valor)
            elif obj.codigo_porc == "7":
                excento_iva += float(obj.valor)

        if obj.codigo == "3":  # ICE
            pass

        if obj.codigo == "5":  # IRBPNR
            irbpnr += float(obj.valor)

    subtotal = subtotal_0 + subtotal_12 + no_objeto_imp + excento_iva
    total = subtotal + ice + irbpnr + iva
    descuentototal = 0.00
    # Detalles de la factura
    detalles = xmldoc_cdata.getElementsByTagName('detalles')[0]
    lista_detalle_nota_credito = []
    for obj in detalles.getElementsByTagName('detalle'):
        detalles_nota_credito = DetalleNotaCredito()
        detalles_nota_credito.codigo_interno = get_valueof_first_element_by_tagname(obj, 'codigoInterno')
        detalles_nota_credito.descripcion = get_valueof_first_element_by_tagname(obj, 'descripcion')
        detalles_nota_credito.cantidad = get_valueof_first_element_by_tagname(obj, 'cantidad')
        detalles_nota_credito.precio_unit = get_valueof_first_element_by_tagname(obj, 'precioUnitario')
        detalles_nota_credito.descuento = get_valueof_first_element_by_tagname(obj, 'descuento')
        detalles_nota_credito.precio_total_sin_imp = get_valueof_first_element_by_tagname(obj, 'precioTotalSinImpuesto')
        descuentototal = descuentototal + float(detalles_nota_credito.descuento) # 30 noviembre se supone que esto es un acumulador de los descuentos :o
        print 'detalle descuento a ride'
        print detalles_nota_credito.descuento
        impuestos = obj.getElementsByTagName('impuestos')[0]
        lista_impuestos = []
        for obj_imp in impuestos.getElementsByTagName('impuesto'):
            impuesto = ImpuestoDetalle()
            impuesto.codigo = get_valueof_first_element_by_tagname(obj_imp, 'codigo')
            impuesto.codigo_porc = get_valueof_first_element_by_tagname(obj_imp, 'codigoPorcentaje')
            impuesto.tarifa = get_valueof_first_element_by_tagname(obj_imp, 'tarifa')
            impuesto.base_imponible = get_valueof_first_element_by_tagname(obj_imp, 'baseImponible')
            impuesto.valor = get_valueof_first_element_by_tagname(obj_imp, 'valor')
            lista_impuestos.append(impuesto)

        detalles_nota_credito.impuestos.append(lista_impuestos)

        lista_detalle_nota_credito.append(detalles_nota_credito)

    concepto = []

    try:
        info_adicional = xmldoc_cdata.getElementsByTagName('infoAdicional')[0]
        for obj in info_adicional.getElementsByTagName('campoAdicional'):
            valor = obj.firstChild.nodeValue
            concepto.append(valor)
    except:
        pass

    code = Code128(info_tributaria.clave_acceso, "B")
    outputter = code.outputter_for("to_png")
    barcode_png = open("image.png", "wb")
    barcode_png.write(outputter.to_png(margin=5, height=200, width=500, xdim=2, ydim=2))
    barcode_png.close()

    data = {"request": request,
             "info_tributaria": info_tributaria,
             "empresa_general": empresa_general,
             "info_nota_credito": info_nota_credito,
             "lista_detalle_nota_credito": lista_detalle_nota_credito,
             "url_logotipo": url_logotipo,
             "numero_auto": numero_auto,
             "fecha_auto": fecha_auto,
             "subtotal_0": subtotal_0,
             "subtotal_12": subtotal_12,
             "no_objeto_imp": no_objeto_imp,
             "excento_iva": excento_iva,
             "iva": iva,
             "ice": ice,
             "irbpnr": irbpnr,
             "subtotal": subtotal,
             "total": total,
             "concepto": concepto,
            "descuentototal": descuentototal,
            "escontribuyenteespecial": escontribuyenteespecial
            }

    return data


def get_ride_factura_pdf(request, venta):
    """
    Funcion que devuelve el ride de la factura electrónica
    en formato Documento PDF
    """
    data = generar_ride_factura(request, venta)
    html = render_to_string('documentos_electronicos/factura.html', data, context_instance=RequestContext(request))

    return generar_pdf_adjunto_mail(html)


@login_required(login_url='/')
def ride_factura_electronica(request, id):
    """
    Funcion que devuelve el ride de la factura electrónica
    en formato Objeto PDF
    """
    venta = Venta_Cabecera.objects.get(id=id)
    data = generar_ride_factura(request, venta)
    html = render_to_string('documentos_electronicos/factura.html', data, context_instance=RequestContext(request))
    return generar_pdf_get(html)


@login_required(login_url='/')
def ride_retencion_electronica(request, web_url):
    """
    Create barcode examples and embed in a PDF
    """
    data = generar_ride_retencion(request, web_url)

    html = render_to_string('documentos_electronicos/retencion_ride.html', data, context_instance=RequestContext(request))
    return generar_pdf_get(html)


def get_ride_retencion_electronica(request, web_url):
    """
    Create barcode examples and embed in a PDF
    """
    data = generar_ride_retencion(request, web_url)
    html = render_to_string('documentos_electronicos/retencion_ride.html', data, context_instance=RequestContext(request))

    return generar_pdf_adjunto_mail(html)


def get_ride_nota_credito_pdf(request, web_url):
    """
    Funcion que devuelve el ride de la factura electrónica
    en formato Documento PDF
    """
    data = generar_ride_nota_credito(request, web_url)
    html = render_to_string('documentos_electronicos/nota_credito.html', data, context_instance=RequestContext(request))

    return generar_pdf_adjunto_mail(html)


@login_required(login_url='/')
def ride_nota_credito_electronica(request, web_url):
    """
    Funcion que devuelve el ride de la factura electrónica
    en formato Objeto PDF
    """
    data = generar_ride_nota_credito(request, web_url)
    print 'en el ride de la nota de credito :o '
    html = render_to_string('documentos_electronicos/nota_credito.html', data, context_instance=RequestContext(request))
    return generar_pdf_get(html)

import requests


url = 'http://104.130.172.230:8080/jasperserver/rest_v2/reportExecutions/reports/interactive/Dilexa_Ventas.pdf'
requestXml =  '<resourceDescriptor name="budget_overzicht_per_klant" wsType="reportUnit" uriString="/Declaraties/12change/Klant/budget_overzicht_per_klant"n'
requestXml += ' isNew="false">n'
requestXml += "   <label>null</label>n"
requestXml += '   <parameter name="klantid">14</parameter>n'
requestXml += '   <parameter name="start">20120101</parameter>n'
requestXml += '   <parameter name="eind">20120302</parameter>n'
requestXml += '   <parameter name="Titel">Test 123</parameter>n'


import requests
import xml.etree.ElementTree as ET


def get_pdf_compras(request):

    # Path to resource rest service:
    url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports/reports/clientes/dilexsa/'

    # Report to process:
    report = 'comprasporfecha.pdf'

    # Authorisation credentials:
    auth = ('jasperadmin', 'jasperadmin')

    # Params
    params = {'fecha_inicio': '2014-01-01',
              'fecha_final': '2015-01-01'
              }

    # Init session so we have no need to auth again and again:
    s = requests.Session()
    r = s.get(url=url+report, auth=auth, params=params)

    # init empty dictionary
    files = {}
    print "XXXXXXXXXXXXXXXXXXXXXXXXXX"
    print r
    print "XXXXXXXXXXXXXXXXXXXXXXXXXX"
    print "XXXXXXXXXXXXXXXX"

    """
    Cuando el response es 400 puede ser que se exedió el # total de paguinas
    y se puede jugar con ese estado para mostar el numero total de paginas porque muestra
    el total de paguinas en xml

    """

    #pdf

    response = HttpResponse(r.content, content_type='application/pdf')
    return response


    #html

    #return HttpResponse(r.content)


    #Excel
    """
    response = HttpResponse(r.content, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Reporte.xls"'
    return response
    """

def get_pdf_ventas2(request):
    url = 'http://104.130.172.230:8080/jasperserver/rest_v2/reports/reports/interactive/'
    report = 'ComprasporFecha.html'
    auth = ('jasperadmin', 'jasperadmin')
    params = {'fecha_inicio': '2014-01-01',
              'fecha_final': '2014-01-31'}

    # Init session so we have no need to auth again and again:
    s = requests.Session()
    r = s.get(url=url+report, auth=auth, params=params)

    #tree = ET.fromstring(r.content)

    #return HttpResponse(r"<div>Done</div")
    #pdf
    """
    response = HttpResponse(r.content, content_type='application/pdf')
    return response
    """

    #html

    return HttpResponse(r.content)


    #Excel
    """
    response = HttpResponse(r.content, content_type='application/vnd.ms-excel')
    response['Content-Disposition'] = 'attachment; filename="Reporte.xls"'
    return response
    """

@transaction.commit_on_success
def reprocesar_detalle_contrato(request):
    for obj in ContratoDetalle.objects.filter(Q(tipo_comprobante_id=15) | Q(tipo_comprobante_id=14)):
        obj.delete()

    for obj in Inventario_Cabecera.objects.filter(status=1):
        contador = 0.0
        if obj.tipo_mov_inventario_id == 3:  # Produccion
            for obj2 in Inventario_Detalle.objects.filter(inventario_cabecera=obj, status=1):
                contador += redondeo(obj2.cantidad * obj2.costo)

        if contador > 0:
            try:
                contrato_detalle = ContratoDetalle()
                contrato_detalle.contrato_cabecera = ContratoCabecera.objects.get(num_cont=obj.num_doc)
                contrato_detalle.tipo_comprobante = obj.tipo_comprobante
                contrato_detalle.modulo_id = obj.id
                contrato_detalle.valor = contador
                contrato_detalle.usuario_creacion = request.user.username
                contrato_detalle.fecha_creacion = datetime.datetime.now()
                contrato_detalle.save()
            except ContratoCabecera.DoesNotExist:
                pass

    return HttpResponse("<body><div>Success</div></body>")

import xlrd
import os
from mmap import mmap, ACCESS_READ
from xlrd import open_workbook
import os.path

def leer_excel(request):

    path = os.path.join('C:/Users/Roberto/Desktop/TABLAS_SRI.xlsx')
    open_file(path)
    return HttpResponse("<body><div>Success</div></body>")

#----------------------------------------------------------------------
def open_file(path):
    """
    Open and read an Excel file
    """
    book = xlrd.open_workbook(path)

    # print number of sheets
    print "____________________________________"
    print "# Hojas Excel"
    print book.nsheets
    # print sheet names
    print "____________________________________"
    print "Nombre de Hojas Excel"
    print book.sheet_names()
    # get the first worksheet
    print "____________________________________"
    print "Obtener el primer elemento"
    first_sheet = book.sheet_by_index(0)

    # read a row
    print "____________________________________"
    print "Obtener el valor del primer elemento"
    print first_sheet.row_values(0)

    # read a cell
    cell = first_sheet.cell(0, 0)
    print "____________________________________"
    print "CELL"
    print cell
    print cell.value

    # read a row slice
    print first_sheet.row_slice(rowx=0, start_colx=0, end_colx=2)
    cells = first_sheet.row_slice(rowx=0, start_colx=0, end_colx=2)

    for cell in cells:
        print "CELDAAAAAAS"
        print cell.value

