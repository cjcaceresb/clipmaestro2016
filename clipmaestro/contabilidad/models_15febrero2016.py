# -*- coding: UTF-8-*-

from datetime import timedelta
from django.db import models
import datetime
from librerias.funciones.funciones_modelos import *
from django.db.models import Q
from django.db.models import Sum
from django.db.models import F
from librerias.funciones.to_word import *
from django.db import IntegrityError

# Create your models here

class Estados(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=50)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now(), null=True)
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "estados"
        verbose_name = "Estado"
        verbose_name_plural = "Estados"

class FormaPago(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    banco_modulo = models.BooleanField(db_column="banco", default=None)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "forma_pago"


class TipoMovimientoBancario(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    alias = models.IntegerField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "tipo_mov_bancario"


class Identificacion(models.Model):

    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=1, db_column="codigo")
    descripcion = models.CharField(max_length=150, db_column="descripcion")
    status = models.IntegerField()
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "identificacion"


class Ciudad(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "ciudad"


class EstadoDocElectronico(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.IntegerField()
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "estado_doc_electronico"


class TipoPersona(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=3)
    descripcion = models.CharField(max_length=50)

    status = models.IntegerField()
    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "tipo_persona"


# VENDEDORES
class Colaboradores(models.Model):
    id = models.AutoField(primary_key=True)
    num_id = models.CharField(max_length=50)
    nombres_completos = models.CharField(max_length=200)
    apellidos_completos = models.CharField(max_length=200)
    direccion = models.CharField(max_length=200)
    sexo = models.CharField(max_length=100)
    fecha_nacimiento = models.DateField()
    email = models.EmailField()
    estado_civil = models.CharField(max_length=220, null=True)
    telefono = models.CharField(max_length=50, null=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        # vendedores
        db_table = "colaboradores"
        verbose_name = "Vendedor"
        verbose_name_plural = "Vendedores"


class TipoCuenta(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    alias = models.IntegerField()
    status = models.IntegerField(default=1)
    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True, blank=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True, blank=True)

    modulo_compra = models.NullBooleanField(null=True, blank=True)

    class Meta:
        db_table = "tipo_cuenta"


class PlanCuenta(models.Model):
    id = models.AutoField(primary_key=True)
    id_cta_grupo = models.ForeignKey('self', blank=True, null=True, db_column="cta_grupo_id")
    codigo = models.CharField(max_length=9)
    descripcion = models.CharField(max_length=100, blank=True, null=True)
    nivel = models.IntegerField()
    naturaleza = models.CharField(max_length=1)
    status = models.IntegerField(default=1)
    tipo = models.ForeignKey(TipoCuenta, null=True, blank=True)
    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True, blank=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True, blank=True)

    def save(self, *args, **kwargs):
        try:
            if self.id is None:
                get_next_id(self)
            super(PlanCuenta, self).save(*args, **kwargs)
        except IntegrityError:
            super(PlanCuenta, self).save(*args, **kwargs)

    def getPadre(self):
        """
        Devuelve el nombre de la cuenta de grupo relaciona a esa cuenta
        :return:
        """
        try:
            return unicode(self.id_cta_grupo.id_cta_grupo.descripcion)
        except:
            return ""

    def getHijos(self):
        """
        Devuelve las cuentas "hijos" de una cuenta de grupo
        :return:
        """
        try:
            hijos = PlanCuenta.objects.filter(id_cta_grupo=self.id, status=1).order_by("codigo")
            return hijos
        except:
            return []

    def get_Hijos_nivel_5(self):
        """
        Función recursiva que retorna una lista con las cuentas de nivel 5
        :return:
        """
        a = []
        if self.nivel == 4:
            return self.getHijos()
        else:
            if len(self.getHijos()) > 0:
                for obj in self.getHijos():
                    a += obj.get_Hijos_nivel_5()
            else:
                return []
        return a

    def get_cta_padre_n(self, n):
        """
        Devuelve el padre cuenta en el nivel n que ingrese el usuario
        :return:
        """
        try:
            if self.nivel > n:  # La cuenta padre debe ser un nivel mayor al propio
                if self.id_cta_grupo.nivel == n:
                    cuenta = PlanCuenta.objects.get(id=self.id_cta_grupo_id)
                    return cuenta
                else:
                    return self.id_cta_grupo.get_cta_padre_n(n)
            else:
                return None
        except:
            return None

    def get_descripcion_cta_n_padre(self, n):
        """
        Devuelve la descripcion padre cuenta en el nivel n que ingrese el usuario
        :return:
        """

        if self.get_cta_padre_n(n) is not None:
            return self.get_cta_padre_n(n).descripcion
        else:
            return ""

    def get_saldo_cuenta_antes_fecha(self, fecha_ini):
        """
        Devuelve el saldo inicial de la cuenta anterior a la fecha ingresada, se toma de la
        tabla SaldoCuentas la columna saldo_inicial si existiere de lo contrario se toma ese
        valor como cero
        :param fecha_ini:
        :return:
        """
        try:
            saldo_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=self, anio=fecha_ini.year).saldo_inicial)

        except SaldoCuenta.DoesNotExist:
            saldo_anio = 0.0

        saldo_antes_fecha_debe = Detalle_Comp_Contable.objects.filter(
            plan_cuenta=self,
            fecha_asiento__range=(str(fecha_ini.year)+"-01-01",
                                  fecha_ini),
            dbcr="D",
            status=1).exclude(fecha_asiento=fecha_ini).aggregate(Sum('valor', field="valor"))["valor__sum"]
        saldo_antes_fecha_haber = Detalle_Comp_Contable.objects.filter(
            plan_cuenta=self,
            fecha_asiento__range=(str(fecha_ini.year)+"-01-01",
                                  fecha_ini),
            dbcr="H",
            status=1).exclude(fecha_asiento=fecha_ini).aggregate(Sum('valor', field="valor"))["valor__sum"]

        if saldo_antes_fecha_debe is None:  # Si el query es vacio el resultado es None no cero
            saldo_antes_fecha_debe = 0.0

        if saldo_antes_fecha_haber is None:  # Si el query es vacio el resultado es None no cero
            saldo_antes_fecha_haber = 0.0

        if self.naturaleza == "D":
            saldo_antes_fecha = saldo_antes_fecha_debe - saldo_antes_fecha_haber
        else:
            saldo_antes_fecha = saldo_antes_fecha_haber - saldo_antes_fecha_debe

        return saldo_anio + saldo_antes_fecha

    def ObtenerCodigo(self):
        """
        Devuelve el código de la cuenta añadida a su plan de
        cuentas
        :return:
        """
        codigo = ""
        codigo_padre = ""
        codigos = []
        #############################
        # Pregunta si es de nivel   #
        #   mayor a 1 para obtener  #
        #   el codigo del padre y   #
        #    de  los de su nivel    #
        #############################
        if self.nivel > 1:
            padre = PlanCuenta.objects.get(id=self.id_cta_grupo.id)
            codigo_padre = padre.codigo
            planes_nivel = padre.getHijos()
        else:
            planes_nivel = PlanCuenta.objects.filter(nivel=1, status=1).order_by("codigo")
        #########################################
        # obtener los codigos segun cada nivel  #
        #########################################
        for cod in planes_nivel:
            if self.nivel == 1:
                codigos.append(int(cod.codigo[0]))
            elif self.nivel == 2:
                codigos.append(int(cod.codigo[1]))
            elif self.nivel == 3:
                codigos.append(int(cod.codigo[2:4]))
            elif self.nivel == 4:
                codigos.append(int(cod.codigo[4:6]))
            elif self.nivel == 5:
                codigos.append(int(cod.codigo[7:9]))
        # Si no tiene ninguna cuenta hijo
        if len(codigos) == 0:
            may = 1
        else:
            may = codigos[0]
            n_sig = 1
            for i in range(1, len(codigos)):
                if codigos[i] > may:
                    may = codigos[i]
            for i in range(0, len(codigos)):
                if codigos[i] > n_sig:
                    break
                n_sig += 1

            if n_sig < may:
                may = n_sig
            else:
                may += 1

        if planes_nivel is None:
            may = 1
        #############################################
        #   Retornar cadena segun nivel
        ############################################
        if self.nivel == 1:
            codigo = str(may) + "00000000"
        elif self.nivel == 2:
            codigo = codigo_padre[0] + str(may) + "0000000"
        elif self.nivel == 3:
            if may >= 10:
                codigo = codigo_padre[0:2] + str(may) + "00000"
            else:
                codigo = codigo_padre[0:2] + "0" + str(may) + "00000"
        elif self.nivel == 4:
            if may >= 10:
                codigo = codigo_padre[0:4] + str(may) + "000"
            else:
                codigo = codigo_padre[0:4]+"0" + str(may) + "000"
        elif self.nivel == 5:
            if may >= 100:
                codigo = codigo_padre[0:6] + str(may)
            elif may >= 10:
                codigo = codigo_padre[0:6] + "0" + str(may)
            else:
                codigo = codigo_padre[0:6] + "00" + str(may)
        ###################
        #Retorna el codigo
        ###################
        return codigo

    class Meta:
        db_table = "plan_cuenta"
        verbose_name = "Plan de Cuentas"
        verbose_name_plural = "Planes de Cuentas"


class TipoPago(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)
    status = models.IntegerField(default=1)

    class Meta:
        db_table = "tipo_pago" # Cambiar el nombre de las tablas


class Sector(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)
    status = models.IntegerField(default=1)

    class Meta:
        db_table = "sector"  # Cambiar el nombre de las tablas


class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_cliente = models.ForeignKey(TipoPersona, db_column="tipo_persona_id", null=True)
    tipo_identificacion = models.ForeignKey(Identificacion, db_column="identificacion_id")
    ruc = models.CharField(max_length=200, verbose_name="N° de Identificación", db_column="num_id")
    razon_social = models.CharField(max_length=220, verbose_name="Razón Social")
    nombre = models.CharField(max_length=220, verbose_name="Nombre Comercial", db_column="nombre_comercial", null=True)
    direccion = models.CharField(max_length=220, verbose_name="Dirección", null=True)
    telefono = models.CharField(max_length=220, verbose_name="Teléfono", null=True)
    ciudad = models.ForeignKey(Ciudad, db_column="ciudad_id", null=True)
    actividad = models.ForeignKey(Sector, db_column="sector_id", null=True)
    plan_cuenta = models.ForeignKey(PlanCuenta, db_column="plan_cuenta_id", null=True)
    forma_pago = models.ForeignKey(TipoPago, verbose_name="Forma de Pago", null=True, db_column="tipo_pago_id")
    margen_utilidad = models.FloatField(null=True)
    monto_credito = models.FloatField(null=True, verbose_name="Monto de Crédito")
    plazo = models.CharField(max_length=150, null=True)
    email = models.EmailField(max_length=255, null=True, blank=True)

    fecha_ultima_venta = models.DateTimeField(blank=True, null=True)
    fecha_ultimo_cobro = models.DateTimeField(blank=True, null=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField()
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)


    def getDirecciones(self):
        direcciones = Cliente_Direccion.objects.filter(status=1, cliente=self)
        cad = ""

        for d in direcciones:
            cad += d.descripcion+" - "+d.direccion1 + " "
            break
        return cad

    def get_num_comprobantes_cliente(self, mes, anio):
        """
        Retorna la cantidad de ventas activas por mes, año y cliente
        :param mes:
        :param anio:
        :return:
        """
        return len(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self))

    def get_num_comprobantes_cliente_nota_credito(self, mes, anio):
        """
        Retorna la cantidad de ventas activas por mes, año y cliente
        :param mes:
        :param anio:
        :return:
        """
        return len(AjusteVentaCabecera.objects.filter(status=1).filter(fecha_emi__month=mes, fecha_emi__year=anio, cliente=self))


    ################ TOTAL BASE IVA (baseImpGrav) ##################################
    def get_total_base_iva(self,  mes, anio):
        """
        Retorna el total de  base_iva  de ventas activas por mes, año y cliente
        :param mes:
        :param anio:
        :param cliente:
        :return:
        """
        sumabasecero = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self)\
                                                                .aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
        sumadescuentocero = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self)\
                                                                .aggregate(Sum('descuento_tarifa0', field="descuento0"))["descuento_tarifa0__sum"])
        sumabaseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self)\
                                                                .aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
        sumadescuentoiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self)\
                                                                .aggregate(Sum('descuento_tarifa12', field="descuentoiva"))["descuento_tarifa12__sum"])
        print str('sumabaseiva')+str(sumabaseiva)
        print str('sumadtoiva')+str(sumadescuentoiva)
        return convert_cero_none(sumabaseiva-sumadescuentoiva)

        '''
        return convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self)\
                                                                .aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
        '''

    ################ TOTAL BASE 0 (baseImponible) ##################################
    def get_total_base_0(self,  mes, anio):
        """
        Retorna el total de  base_0  de ventas activas por mes, año y cliente
        :param mes:
        :param anio:
        :param cliente:
        :return:
        """
        sumabasecero = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self)\
                                                                .aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
        sumadescuentocero = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self)\
                                                                .aggregate(Sum('descuento_tarifa0', field="descuento0"))["descuento_tarifa0__sum"])

        return convert_cero_none(sumabasecero-sumadescuentocero)
        '''
        return convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes,
                                                              fecha__year=anio,
                                                              cliente=self).aggregate(Sum('subtotal_tarifa0', field="base0-descuento0"))["subtotal_tarifa0__sum"])
        '''

        ################ TOTAL MONTO IVA (monto_iva) ###################################
    def get_total_monto_iva(self,  mes, anio):
        """
        Retorna el total de  monto_iva  de ventas activas por mes, año y cliente
        :param mes:
        :param anio:
        :param cliente:
        :return:
        """
        return convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, cliente=self)\
                                                                .aggregate(Sum('iva_total', field="monto_iva"))["iva_total__sum"])


    # NOTA DE CRÉDITO
    def get_total_monto_iva_nota_credito(self,  mes, anio):
        """
        Retorna el total de  monto_iva  de N/C activas por mes, año y cliente
        :param mes:
        :param anio:
        :param cliente:
        :return:
        """
        return convert_cero_none(AjusteVentaCabecera.objects.filter(status=1).filter(fecha_emi__month=mes, fecha_emi__year=anio,
                                                                                     cliente=self).aggregate(Sum('monto_iva', field="monto_iva"))["monto_iva__sum"])

    def get_total_base_iva_nota_credito(self,  mes, anio):
        """
        Retorna el total de  base_iva  de N/C activas por mes, año y cliente
        :param mes:
        :param anio:
        :param cliente:
        :return:
        """
        return convert_cero_none(AjusteVentaCabecera.objects.filter(status=1).filter(fecha_emi__month=mes, fecha_emi__year=anio, cliente=self)\
                                                                .aggregate(Sum('tarifa12', field="baseiva"))["tarifa12__sum"])

    def get_total_base_0_nota_credito(self,  mes, anio):
        """
        Retorna el total de  base_0  de N/C activas por mes, año y cliente
        :param mes:
        :param anio:
        :param cliente:
        :return:
        """
        return convert_cero_none(AjusteVentaCabecera.objects.filter(status=1).filter(fecha_emi__month=mes,
                                                                                     fecha_emi__year=anio,
                                                                                    cliente=self).aggregate(
                                                                                    Sum('base0', field="base0"))
                                                                                    ["base0__sum"])


    ################ TOTAL RETENCION VENTA  ########################################
    def get_totales_retencion_venta(self, mes, anio):
        """
        Retorna el total de  los valores de retencion en ventas activas por mes, año y cliente
        :return:
        """
        total_valor_ret_iva = 0.0
        total_valor_ret_renta = 0.0

        for obj in Cobro.objects.filter(status=1, cliente=self, fecha_reg__year=anio, fecha_reg__month=mes):
            total_valor_ret_iva += float(obj.get_total_iva_ret())
            total_valor_ret_renta += float(obj.get_total_ret_renta())

        return redondeo(total_valor_ret_iva, 2), redondeo(total_valor_ret_renta, 2)

    def getRetencionVentaClienteFecha(self, mes, anio):
        return CXC_cobro.objects.filter(cobro__tipo_comprobante_id=16,
                                        cobro__fecha_reg__month=mes,
                                        cobro__fecha_reg__year=anio,
                                        cobro__cliente=self,
                                        status=1).exists()

    class Meta:
        db_table = "cliente"  # Cambiar el nombre de las tablas


class Cliente_Direccion(models.Model):
    id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(Cliente)
    descripcion = models.CharField(max_length=250, null=True)
    direccion1 = models.CharField(max_length=220)
    direccion2 = models.CharField(max_length=220, null=True)
    atencion = models.CharField(max_length=220, null=True)
    telefono = models.CharField(max_length=150, null=True)
    ciudad = models.ForeignKey(Ciudad)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "cliente_direccion"


class Proveedores(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_proveedor = models.ForeignKey(TipoPersona, db_column="tipo_persona_id")
    tipo_identificacion = models.ForeignKey(Identificacion, db_column="identificacion_id")
    ruc = models.CharField(max_length=200, verbose_name="N° de Identificación", db_column="num_id")
    razon_social = models.CharField(max_length=220, verbose_name="Razón Social", db_column="razon_social")
    nombre = models.CharField(max_length=220, verbose_name="Nombre Comercial", db_column="nombre_comercial", null=True)
    direccion = models.CharField(max_length=220, verbose_name="Dirección", null=True)
    telefono = models.CharField(max_length=200, verbose_name="Teléfono", null=True)
    ciudad = models.ForeignKey(Ciudad, null=True,db_column="ciudad_id")
    actividad = models.ForeignKey(Sector, null=True, db_column="sector_id")
    plan_cuenta = models.ForeignKey(PlanCuenta, db_column="plan_cuenta_id", null=True)
    forma_pago = models.ForeignKey(TipoPago, null=True, db_column="tipo_pago_id")
    monto_credito = models.FloatField(null=True, verbose_name="Monto de Crédito")
    plazo = models.CharField(max_length=150, verbose_name="Plazo", null=True)
    email = models.EmailField(max_length=254)

    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    fecha_ultima_compra = models.DateField(blank=True, null=True)
    fecha_ultimo_pago = models.DateField(blank=True, null=True)
    status = models.IntegerField(default=1)

    def getCuentasPagar(self):
        return Cuentas_por_Pagar.objects.filter(status=1, proveedor=self, esta_pagado=False)

    def TienePago(self):
        cuentas_pagar = Cuentas_por_Pagar.objects.exclude(Q(documento__codigo_documento="04") | Q(num_doc="0")).filter(status=1, proveedor=self)
        if len(cuentas_pagar) != 0:
            return True
        else:
            return False

    class Meta:
        db_table = "proveedor" # Cambiar el nombre de las tablas


class Departamento(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "departamento"


class Contacto(models.Model):
    id = models.AutoField(primary_key=True)
    ############## Tipo y cliente_provedor_id ##############
    tipo = models.IntegerField(null=True)
    cliente_proveedor_id = models.IntegerField(null=True)
    ########################################################
    apellidos = models.CharField(max_length=150, null=True, db_column="apellido")
    nombres = models.CharField(max_length=150, null=True, db_column="nombre")
    departamento = models.ForeignKey(Departamento, null=True)
    correo = models.EmailField(null=True)
    telefono = models.CharField(max_length=50, null=True)
    movil = models.CharField(max_length=50, null=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "contacto"


class Documento(models.Model):
    id = models.AutoField(primary_key=True)
    codigo_documento = models.CharField(max_length=2, db_column='codigo')
    descripcion_documento = models.CharField(max_length=50, db_column='descripcion')
    cod_transaccion = models.CharField(max_length=50)
    compra = models.BooleanField(default=None)
    venta = models.BooleanField(default=None)
    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "documento_sri"


class VigenciaDocEmpresa(models.Model):
    id = models.AutoField(primary_key=True)
    documento_sri = models.ForeignKey(Documento, db_column="documento_sri_id")
    serie = models.CharField(max_length=7) #7caracreres 3 en estab los ultimos 3 en ptoemi
    sec_actual = models.IntegerField(default=0)
    fecha_actual = models.DateField()
    autorizacion = models.CharField(max_length=50)
    sec_inicial = models.IntegerField(default=0)
    sec_final = models.IntegerField(default=0)
    fecha_emi = models.DateField()
    fecha_vencimiento = models.DateField()

    status = models.IntegerField(default=1)
    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "vigencia_doc_empresa"
        verbose_name = "Vigencia Documento Empresa"
        verbose_name_plural = "Vigencias Documento Empresa"

    def actualizar_estado(self):
        if self.fecha_vencimiento < datetime.datetime.now().date():
            self.status = 4
            self.save()
        return ""

    ################ TOTAL VENTA POR MES SIN IVA  ##################################
    def get_totales_por_mes_sin_iva(self, mes, anio):

        total_venta_mes_baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, vigencia_doc_empresa__documento_sri__venta=True,  vigencia_doc_empresa__documento_sri__id=1)\
                                                                .aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])

        total_venta_mes_descuentoiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, vigencia_doc_empresa__documento_sri__venta=True,  vigencia_doc_empresa__documento_sri__id=1)\
                                                                .aggregate(Sum('descuento_tarifa12', field="descuentoiva"))["descuento_tarifa12__sum"])


        total_venta_mes_base0 = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, vigencia_doc_empresa__documento_sri__venta=True,  vigencia_doc_empresa__documento_sri__id=1)\
                                                                .aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])

        total_venta_mes_descuento0 = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, vigencia_doc_empresa__documento_sri__venta=True,  vigencia_doc_empresa__documento_sri__id=1)\
                                                                .aggregate(Sum('descuento_tarifa0', field="descuento0"))["descuento_tarifa0__sum"])



        total_nota_credito_mes_baseiva = convert_cero_none(AjusteVentaCabecera.objects.filter(status=1).filter(fecha_emi__month=mes, fecha_emi__year=anio,  vigencia_doc_empresa__documento_sri__venta=True,
                                                                                             vigencia_doc_empresa__documento_sri__id=4).aggregate(Sum('tarifa12', field="baseiva"))["tarifa12__sum"])

        total_nota_credito_mes_base0 = convert_cero_none(AjusteVentaCabecera.objects.filter(status=1).filter(fecha_emi__month=mes, fecha_emi__year=anio, vigencia_doc_empresa__documento_sri__venta=True,
                                                                                           vigencia_doc_empresa__documento_sri__id=4).aggregate(Sum('base0', field="base0"))["base0__sum"])

        return redondeo(total_venta_mes_baseiva, 2), redondeo(total_venta_mes_base0, 2), redondeo(total_nota_credito_mes_baseiva, 2), redondeo(total_nota_credito_mes_base0, 2)#, redondeo(total_venta_mes_descuentoiva, 2), redondeo(total_venta_mes_descuento0, 2)

    #28 diciembre cambiada por 2 params nuevos
    #los finales :o :o
    def get_totales_por_mes_sin_iva_28dic(self, mes, anio):

        total_venta_mes_baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, vigencia_doc_empresa__documento_sri__venta=True,  vigencia_doc_empresa__documento_sri__id=1)\
                                                                .aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])

        total_venta_mes_descuentoiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, vigencia_doc_empresa__documento_sri__venta=True,  vigencia_doc_empresa__documento_sri__id=1)\
                                                                .aggregate(Sum('descuento_tarifa12', field="descuentoiva"))["descuento_tarifa12__sum"])


        total_venta_mes_base0 = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, vigencia_doc_empresa__documento_sri__venta=True,  vigencia_doc_empresa__documento_sri__id=1)\
                                                                .aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])

        total_venta_mes_descuento0 = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__month=mes, fecha__year=anio, vigencia_doc_empresa__documento_sri__venta=True,  vigencia_doc_empresa__documento_sri__id=1)\
                                                                .aggregate(Sum('descuento_tarifa0', field="descuento0"))["descuento_tarifa0__sum"])



        total_nota_credito_mes_baseiva = convert_cero_none(AjusteVentaCabecera.objects.filter(status=1).filter(fecha_emi__month=mes, fecha_emi__year=anio,  vigencia_doc_empresa__documento_sri__venta=True,
                                                                                             vigencia_doc_empresa__documento_sri__id=4).aggregate(Sum('tarifa12', field="baseiva"))["tarifa12__sum"])

        total_nota_credito_mes_base0 = convert_cero_none(AjusteVentaCabecera.objects.filter(status=1).filter(fecha_emi__month=mes, fecha_emi__year=anio, vigencia_doc_empresa__documento_sri__venta=True,
                                                                                           vigencia_doc_empresa__documento_sri__id=4).aggregate(Sum('base0', field="base0"))["base0__sum"])

        return redondeo(total_venta_mes_baseiva, 2), redondeo(total_venta_mes_base0, 2), redondeo(total_nota_credito_mes_baseiva, 2), redondeo(total_nota_credito_mes_base0, 2), redondeo(total_venta_mes_descuentoiva, 2), redondeo(total_venta_mes_descuento0, 2)



class Tipos_Retencion(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=50)

    status = models.IntegerField(default=1)
    fecha_creacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    fecha_actualizacion  =   models.DateTimeField()
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "tipo_codigo_sri"


class VigenciaRetencionSRI(models.Model):
   id = models.AutoField(primary_key=True, db_column="id")
   tipo_codigo_sri = models.ForeignKey(Tipos_Retencion)
   bloque = models.CharField(max_length=10, null=True)

   fecha_inicio = models.DateTimeField(db_column="fecha_inicio")
   fecha_final = models.DateTimeField(db_column="fecha_final")
   status = models.IntegerField(default=1)
   class Meta:
        db_table = "vigencia_retencion_sri"


class Codigo_SRI(models.Model):
    id = models.AutoField(primary_key=True)
    vigencia_retencion = models.ForeignKey(VigenciaRetencionSRI, db_column="id_vigencia_retencion")
    codigo = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=250)
    porcentaje = models.FloatField()
    orden = models.FloatField(null=True)
    porc_variable = models.BooleanField(default=None)
    porc_min = models.FloatField(null=True, blank=True)
    porc_max = models.FloatField(null=True, blank=True)
    iva_al_gasto = models.NullBooleanField()

    status = models.IntegerField(default=1)
    fecha_creacion  =  models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    fecha_actualizacion  =  models.DateTimeField()
    usuario_actualizacion = models.CharField(max_length=50, null=True)
    class Meta:
        db_table = "codigo_sri"


class TipoComprobante(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=5)
    descripcion = models.CharField(max_length=200)

    status = models.IntegerField(default=1)
    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    def has_add_permission(self, request):
        return False

    class Meta:
        db_table = "tipo_comprobante"


class SecuenciaTipoComprobante(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    secuencia = models.IntegerField()
    fecha_actual = models.DateField()

    status = models.IntegerField(default=1)
    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "secuencia_tipo_comprobante"
        verbose_name = "Secuencia Tipo Comprobante Contable"
        verbose_name_plural = "Secuencias Tipo Comprobante Contable"

    def save(self, *args, **kwargs):
        try:
            if self.id is None:
                get_next_id(self)
            super(SecuenciaTipoComprobante, self).save(*args, **kwargs)
        except IntegrityError:
            super(SecuenciaTipoComprobante, self).save(*args, **kwargs)


class Tipo_Cuenta_Banco(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion  =  models.DateTimeField()
    fecha_actualizacion  =  models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "tipo_cuenta_banco"


class Cuenta_Banco(models.Model):
    id = models.AutoField(primary_key=True)
    plan_cuenta = models.ForeignKey(PlanCuenta)
    descripcion = models.CharField(max_length=200)
    tipo_cuenta_banco = models.ForeignKey(Tipo_Cuenta_Banco, null=True, db_column="tipo_cuenta_banco_id")

    numero_cuenta = models.CharField(max_length=50)
    secuencia_cheque = models.CharField(max_length=100, null=True)
    fecha_ultimo_cierre = models.DateTimeField(null=True)
    fecha_ultimo_conciliacion = models.DateTimeField(null=True)

    status = models.IntegerField(default=1)
    fecha_creacion  =  models.DateTimeField()
    fecha_actualizacion  =  models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "cuenta_banco"
        verbose_name = 'Cuenta Bancaria'
        verbose_name_plural = 'Cuentas Bancarias'

    def save(self, *args, **kwargs):
        try:
            if self.id is None:
                get_next_id(self)
            super(Cuenta_Banco, self).save(*args, **kwargs)
        except IntegrityError:
            super(Cuenta_Banco, self).save(*args, **kwargs)

    def get_movimientos_por_fecha(self, fecha_ini, fecha_fin):
        """
        retorna una lista con todas las transacciones que se hicieron
        en un determinado rango de fecha
        :param fecha_ini:
        :param fecha_fin:
        :return:
        """
        return Banco.objects.exclude(status=0).filter(cuenta_banco=self,
                                                      fecha_reg__range=(fecha_ini, fecha_fin)).order_by("fecha_reg", "num_comp")

    def get_list_cheques_por_fecha(self, fecha_ini, fecha_fin):
        """
        retorna una lista con todas las transacciones que se hicieron con cheques guirados
        en un determinado rango de fecha
        :param fecha_ini:
        :param fecha_fin:
        :return:
        """
        return Banco.objects.filter(cuenta_banco=self,
                                    fecha_reg__range=(fecha_ini, fecha_fin),
                                    tipo_comprobante_id=4).order_by("-fecha_reg", "-num_comp")

    def get_saldos_por_fecha(self, fecha_ini, fecha_fin):
        """
        Retorna los saldos inicial, deudor, acreedor de la cuenta de banco
        :param fecha_ini:
        :param fecha_fin:
        :return:
        """

        try:
            saldo_inicial_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=self.plan_cuenta, anio=fecha_ini.year).saldo_inicial)
        except:
            saldo_inicial_anio = 0

        saldo_deudor_ini = convert_cero_none(Banco.objects.exclude(fecha_reg=fecha_ini).filter(cuenta_banco=self,
                                                                                               fecha_reg__range=(str(fecha_ini.year)+"-01-01",
                                                                                                                 fecha_ini),  # Desde el primer dia del año
                                                                                               naturaleza=1,
                                                                                               status=1).aggregate(Sum('valor', field="valor"))["valor__sum"])


        saldo_acreedor_ini = convert_cero_none(Banco.objects.exclude(fecha_reg=fecha_ini).filter(cuenta_banco=self,
                                                                                                 fecha_reg__range=(str(fecha_ini.year)+"-01-01",
                                                                                                                   fecha_ini),  # Desde el primer dia del año
                                                                                                 naturaleza=2,
                                                                                                 status=1).aggregate(Sum('valor', field="valor"))["valor__sum"])

        total_debe = convert_cero_none(Banco.objects.filter(cuenta_banco=self,
                                                            fecha_reg__range=(fecha_ini, fecha_fin),
                                                            naturaleza=1,
                                                            status=1).aggregate(Sum('valor', field="valor"))["valor__sum"])

        total_haber = convert_cero_none(Banco.objects.filter(cuenta_banco=self,
                                                             fecha_reg__range=(fecha_ini, fecha_fin),
                                                             naturaleza=2,
                                                             status=1).aggregate(Sum('valor', field="valor"))["valor__sum"])

        saldo_ini = saldo_inicial_anio + saldo_deudor_ini - saldo_acreedor_ini

        saldo = saldo_ini + (total_debe - total_haber)

        return saldo_ini, total_debe, total_haber, saldo
#cosa que trabaja la parte decimal del valor, le pone ceros si es que debe
def splits(string, splitter, i):
    numero = ''
    try:
        if len(str(string).split(splitter)[i]) < 2:
            numero = (str(string).split(splitter)[i])+"0"
        else:
            numero = (str(string).split(splitter)[i])
        return numero #str(string).split(splitter)[i]
    except:
        return ""

class Banco(models.Model):
    id = models.AutoField(primary_key=True)
    cuenta_banco = models.ForeignKey(Cuenta_Banco)
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    num_comp = models.CharField(max_length=100)
    fecha_reg = models.DateField()
    num_referencia = models.CharField(max_length=150, null=True, blank=True)
    num_cheque = models.CharField(max_length=150, null=True, blank=True)
    fecha_cheque = models.DateField(null=True, blank=True)
    persona = models.CharField(max_length=220, null=True, blank=True)
    concepto = models.CharField(max_length=220, null=True, blank=True)
    valor = models.FloatField(default=0)
    naturaleza = models.IntegerField()
    forma_pago = models.ForeignKey(FormaPago)
    conciliado = models.BooleanField(default=False)
    fecha_conciliacion = models.DateField(null=True, blank=True)

    status = models.IntegerField(default=1)
    fecha_creacion  =  models.DateTimeField()
    fecha_actualizacion  =  models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "banco"
        verbose_name = u"Transacción Bancaria"
        verbose_name_plural = u"Transacciones Bancarias"

    def cheque_exist(self, num_cheque):
        return Banco.objects.exclude(status=0).filter(num_cheque=num_cheque, cuenta_banco=self.cuenta_banco ).exists() # Tambien se comparan con los anulados

    def monto_to_word(self):
        return to_word(int(self.valor))


    def getTipoComprobanteId(self):
        return self.tipo_comprobante.id


    def parteentera(self):
        try:
            valor= self.valor
            if (len(str(int(self.valor)))< 4):
                return str(int(self.valor))+'.'+splits(self.valor,'.',1)
            else:
                return str(int(self.valor))[0:len(str(int(self.valor)))-3 ]+','+str(int(self.valor))[len(str(int(self.valor)))-3:len(str(int(self.valor)))]+'.'+splits(self.valor,'.',1)
                #return 'algomasgrandequecuatro'
        except:
            print 'este cheque es incorrecto'
            return 'este cheque es incorrecto'


class Saldo_Banco(models.Model):
    id = models.AutoField(primary_key=True)
    plan_cuenta = models.ForeignKey(PlanCuenta)
    anio = models.IntegerField(null=True)
    mes = models.IntegerField(null=True)
    fecha_ultimo_cierre = models.DateTimeField(null=True)
    fecha_ultimo_conciliacion = models.DateTimeField(null=True)
    saldo = models.FloatField(null=True)

    fecha_creacion  =  models.DateTimeField(null=True)
    fecha_actualizacion  =  models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "saldo_banco"  # Cambiar el nombre de las tablas


class Cabecera_Comp_Contable(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_comprobante = models.ForeignKey(TipoComprobante, db_column="tipo_comprobante_id")
    numero_comprobante = models.CharField(max_length=20, db_column="num_comp")
    fecha = models.DateField(db_column="fecha_reg")
    concepto_comprobante = models.TextField(db_column="concepto")

    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)
    status = models.IntegerField(default=1)

    class Meta:
        db_table = "comprobante_contable_cabecera" # Cambiar el nombre de las tablas
        verbose_name = 'Cabecera Comprobante Contable'
        verbose_name_plural = 'Cabecera Comprobantes Contables'

    def getDetalleComp(self):
        try:
            return Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=self)
        except (Detalle_Comp_Contable.DoesNotExist, ValueError):
            return None

    def get_total_debe(self):
        total_debe = 0.0
        lista = Detalle_Comp_Contable.objects.filter(cabecera_contable=self)

        for d in lista:
            if d.dbcr == "D":
                total_debe += d.valor
        return redondeo(total_debe, 2)

    def get_total_haber(self):
        total_haber = 0.0
        lista = Detalle_Comp_Contable.objects.filter(cabecera_contable=self)

        for d in lista:
            if d.dbcr == "H":
                total_haber += d.valor
        return redondeo(total_haber, 2)


    def getStatusModulo(self):
        # Función que revisa si el asiento posee cuentas de módulo
        # return 1: Sí existió una cuenta de módulo en ese asiento
        # returun 0: Si no posee cuentas de módulo

        cont = 0

        for d in Detalle_Comp_Contable.objects.filter(cabecera_contable=self):
            #18 agosto se pusieron mas restricciones en los excludes, los de 2,8,11,19 agosto las 15, 22
            if PlanCuenta.objects.filter(status=1, nivel=5).\
                    filter(tipo__in=TipoCuenta.objects.filter(status=1).exclude(id=20).exclude(id=6).exclude(id=19).exclude(id=22).exclude(id=15).exclude(id=11).exclude(id=8).exclude(id=2).exclude(id=21).exclude(id=17)).\
                    filter(id=d.plan_cuenta.id).exists():
                cont += 1

        if cont > 0:
            return 1
        else:
            return 0


class Centro_Costo(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)
    status = models.IntegerField(default=1)

    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "centro_costo" # Cambiar el nombre de las tablas


class Detalle_Comp_Contable(models.Model):
    id = models.AutoField(primary_key=True)
    cabecera_contable = models.ForeignKey(Cabecera_Comp_Contable, db_column="comprobante_contable_cabecera_id")
    plan_cuenta = models.ForeignKey(PlanCuenta)
    centro_costo = models.ForeignKey(Centro_Costo, null=True, blank=True)
    detalle = models.TextField(db_column="concepto", null=True, blank=True)
    dbcr = models.CharField(max_length=100)
    valor = models.FloatField()
    fecha_asiento  =   models.DateField(db_column="fecha_reg")

    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)
    status = models.IntegerField(default=1)

    class Meta:
        db_table = "comprobante_contable_detalle"


    def save(self, *args, **kwargs):
        try:
            if self.id is None:
                get_next_id(self)
            super(Detalle_Comp_Contable, self).save(*args, **kwargs)
        except IntegrityError:
            super(Detalle_Comp_Contable, self).save(*args, **kwargs)


class Seq_Comp_Contable(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_comp = models.ForeignKey(TipoComprobante, db_column="tipo_comprobante_id")
    anio = models.CharField(max_length=50)
    mes = models.CharField(max_length=150)
    secuencia = models.CharField(max_length=15)
    fecha_actual = models.DateField()

    fecha_creacion  =   models.DateTimeField()
    fecha_actualizacion  =   models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    status = models.IntegerField(default=1)

    class Meta:
        db_table = "secuencia_comprobante_contable"  # Cambiar el nombre de las tablas
        verbose_name = "Secuencia Comprobante Contable"
        verbose_name_plural = "Secuencias Comprobante Contable"


class Cuenta_Retencion(models.Model):
    id = models.AutoField(primary_key=True)
    plan_cuenta = models.ForeignKey(PlanCuenta)
    tipo_codigo_sri = models.ForeignKey(Tipos_Retencion)
    porcentaje = models.FloatField(null=True)
    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "cuenta_retencion"
        verbose_name = u"Cuenta de Retención"
        verbose_name_plural = u"Cuentas de Retención"


class Sustento_Tributario(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=250)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "sustento_tributario_sri"


class Sustento_Documento(models.Model):
    id = models.AutoField(primary_key=True)
    sustento = models.ForeignKey(Sustento_Tributario)
    documento = models.ForeignKey(Documento)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "sustento_documento"


class Tipo_Pago_SRI(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=3)
    descripcion = models.CharField(max_length=50)
    status = models.IntegerField()
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "tipo_pago_sri"


class Forma_Pago_SRI(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=2)
    descripcion = models.CharField(max_length=100)

    status = models.IntegerField()
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "forma_pago_sri"


class Tipo_Transaccion(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=1, db_column="codigo")
    descripcion = models.CharField(max_length=150, db_column="descripcion")

    status = models.IntegerField()
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "tipo_transaccion"


class Identificacion_Transaccion(models.Model):

    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=1, db_column="codigo")
    transaccion = models.ForeignKey(Tipo_Transaccion, db_column="transaccion_id")
    identificacion = models.ForeignKey(Identificacion, db_column="identificacion_id")

    status = models.IntegerField()
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "identificacion_transaccion"


class Documento_Id_Transaccion(models.Model):

    id = models.AutoField(primary_key=True)
    documento = models.ForeignKey(Documento, db_column="documento_id")
    identificacion_transaccion = models.ForeignKey(Identificacion_Transaccion, db_column="identificacion_transaccion_id")

    status = models.IntegerField()
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "documento_id_transaccion"


class Pais(models.Model):

    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=3, db_column="codigo")
    descripcion = models.CharField(max_length=150, db_column="descripcion")

    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField( null=True)
    usuario_creacion = models.CharField(max_length=20, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)
    status = models.IntegerField()

    class Meta:
        db_table = "pais"


class ContratoCabecera(models.Model):
    id = models.AutoField(primary_key=True)
    num_cont = models.CharField(max_length=50)
    fecha_reg = models.DateField()
    fecha_entrega = models.DateField(null=True, blank=True)
    fecha_cierre = models.DateField(null=True, blank=True)
    cliente = models.ForeignKey(Cliente, null=True, blank=True)
    tipo_pago = models.ForeignKey(TipoPago, null=True, blank=True)
    plazo = models.IntegerField(null=True, blank=True)
    vendedor = models.ForeignKey(Colaboradores, null=True, blank=True)
    referencia = models.CharField(max_length=20, null=True, blank=True)
    responsable = models.CharField(max_length=100, null=True, blank=True)
    concepto = models.CharField(max_length=600, null=True, blank=True)
    costo_estimado = models.FloatField(default=0, null=True, blank=True)
    base0 = models.FloatField(default=0)
    descuento_porc0 = models.FloatField(default=0)
    monto_descuento0 = models.FloatField(default=0)
    baseiva = models.FloatField(default=0)
    descuento_porciva = models.FloatField(default=0)
    monto_descuentoiva = models.FloatField(default=0)
    monto_iva = models.FloatField(default=0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "contrato_cabecera"
        verbose_name = "Cabecera de Contrato"
        verbose_name_plural = "Cabeceras de Contrato"

    def to_string(self):
        num_cont = self.num_cont
        try:
            razon_social_cliente = self.cliente.razon_social[0:30]
            return num_cont + " - " + razon_social_cliente
        except:
            return num_cont

    def getDetalles(self):
        detalles = ContratoDetalle.objects.filter(status=1, contrato_cabecera_id=self.id)
        return detalles

    def getTotal(self):
        return self.baseiva + self.base0 + self.monto_iva


    def getTotalDetallesCabecera(self):
        total = 0.0
        try:
            if ContratoDetalle.objects.filter(status=1, contrato_cabecera_id=self.id).exists():

                lista = ContratoDetalle.objects.filter(status=1, contrato_cabecera_id=self.id)
                for detalle in lista:
                    if detalle.tipo_comprobante.id == 1 or detalle.tipo_comprobante.id == 15:
                        total += float(detalle.valor)
                    elif detalle.tipo_comprobante.id == 14:
                        total -= float(detalle.valor)
                    else:
                        total += 0.0
            else:
               pass
        except:
            pass


        return total

    def getNumerosDocumentosVentasContrato(self):
        contador =1
        numeros = ""

        detalles = ContratoDetalle.objects.filter(status=1, contrato_cabecera_id=self.id)

        if len(detalles) > 0:
            for detalle in detalles:
                try:
                    if detalle.tipo_comprobante_id == 2:
                        numero = Venta_Cabecera.objects.get(status=1,id=detalle.modulo_id).num_documento
                        if contador > 1:
                            numeros = numeros +" / "+ numero
                        else:
                            numeros = numeros + numero
                        contador+=1
                except:
                    pass
        return numeros


class ContratoDetalle(models.Model):
    id = models.AutoField(primary_key=True)
    contrato_cabecera = models.ForeignKey(ContratoCabecera)
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    modulo_id = models.IntegerField()
    valor = models.FloatField(default=0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    def get_compra(self):
        try:
            return CompraCabecera.objects.get(num_comp=self.comp_cabecera.numero_comprobante)
        except CompraCabecera.DoesNotExist:
            return None

    def get_modulo(self):
        try:
            tipo_comp = TipoComprobante.objects.get(id=self.tipo_comprobante.id)
            if tipo_comp.id == 1:   # Compra
                cabecera_compra = CompraCabecera.objects.filter(status=1).get(id=self.modulo_id)
                return cabecera_compra

            elif tipo_comp.id == 2:     # Venta
                cabecera_venta = Venta_Cabecera.objects.filter(status=1).get(id=self.modulo_id)
                return cabecera_venta
        except:
            return None

    class Meta:
        db_table = "contrato_detalle"


class CompraCabecera(models.Model):
    id = models.AutoField(primary_key=True)
    num_comp = models.CharField(max_length=20)
    fecha_reg = models.DateField()
    proveedor = models.ForeignKey(Proveedores, null=True, blank=True)
    documento = models.ForeignKey(Documento, null=True, blank=True)
    ats_id = models.IntegerField(null=True, blank=True)
    num_doc = models.CharField(max_length=20, db_column="num_doc", null=True, blank=True)
    fecha_emi = models.DateField()
    tipo_pago = models.ForeignKey(TipoPago, null=True, blank=True)
    plazo = models.IntegerField(null=True, blank=True)
    concepto = models.CharField(max_length=250)
    base0 = models.FloatField(default=0.0)
    baseiva = models.FloatField(default=0.0)
    monto_iva = models.FloatField(default=0.0)
    base_ice = models.FloatField(default=0.0)
    monto_ice = models.FloatField(default=0.0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    contrato = models.ForeignKey(ContratoCabecera, null=True, blank=True)

    class Meta:
        db_table = "compra_cabecera"
        verbose_name = 'Compra'
        verbose_name_plural = 'Compras'

    def get_ats(self):
        try:
            return ATS.objects.filter(compra_cabecera=self)[0]
        except IndexError:
            return None

    def getComprasRet0(self):
        total = 0.00
        try:
            detalle_comp = CompraDetalle.objects.filter(status=1, compra_cabecera=self, codigo_ret_fte__porcentaje=0)
            for obj in detalle_comp:
                total += obj.valor
        except:
            pass
        return total

    def getRetencion(self):
        try:
            return RetencionCompraCabecera.objects.filter(compra_cabecera=self).order_by("id").reverse()[0]
        except:
            return None

    def get_num_retencion(self):
        try:
            reten = RetencionCompraCabecera.objects.filter(compra_cabecera=self).order_by("id").reverse()[0]
            return str(reten.vigencia_doc_empresa.serie) + "-" + str(reten.numero_ret)
        except:
            return ""

    def get_estado_retencion_electronica(self):
        try:
            reten = RetencionCompraCabecera.objects.filter(compra_cabecera=self).order_by("id").reverse()[0]
            if reten.estado_firma == 1:
                return '<span title="Guardado">G</span>'
            elif reten.estado_firma == 2:
                return '<span title="Firmado">F</span>'
            elif reten.estado_firma == 3:
                return '<span title="Enviado SRI">E</span>'
            elif reten.estado_firma == 4:
                return '<span title="Autorizado">A</span>'
            elif reten.estado_firma == 5:
                return '<i class="fa fa-envelope-o" title="Enviado al Correo"></i>'
            else:
                return ""
        except RetencionCompraCabecera.DoesNotExist:
            return ""

    def get_total_ret(self):
        try:
            total_ret = 0.0

            ret_cab = RetencionCompraCabecera.objects.filter(compra_cabecera=self).order_by("id").reverse()[0]
            for obj in RetencionCompraDetalle.objects.filter(retencion_cabecera_compra=ret_cab, status=1):
                total_ret += obj.valor_retenido

            return round(total_ret, 2)

        except Exception, e:
            return 0

    def getSubtotal(self):
        total_detalle = 0.0
        detallecompra = CompraDetalle.objects.filter(compra_cabecera=self)
        for obj in detallecompra:
            total_detalle += obj.valor

        return total_detalle

    def getTotal(self):
        total_detalle = 0.0
        total_retencion = 0.0
        detallecompra = CompraDetalle.objects.filter(compra_cabecera=self)
        for obj in detallecompra:
            total_detalle += obj.valor

        try:
            retencion = RetencionCompraCabecera.objects.get(compra_cabecera=self, status=1)
            detalle_retencion = RetencionCompraDetalle.objects.filter(status=1, retencion_cabecera_compra=retencion)
            for obj in detalle_retencion:
                total_retencion += obj.valor_retenido
        except:
            pass

        return total_detalle + self.monto_iva + self.monto_ice - total_retencion

    ######## GET TOTAL RETENCION DE CÓDIGO 30% #########
    def getTotalRetencionxCodigo_30_x_sust_28dic(self, sustento_tributario_sri_id):
        total_retencion_30 = 0.0
        detalle_compra = CompraDetalle.objects.filter(compra_cabecera=self,
                                                      sustento_tributario_id=sustento_tributario_sri_id)
        for obj in detalle_compra:
            if obj.codigo_ret_iva is not None:
                if obj.codigo_ret_iva.porcentaje == 30:
                    total_retencion_30 += obj.monto_ret_iva

        return total_retencion_30

    def getTotalRetencionxCodigo_30_y_70_y_100_x_sust(self, sustento_tributario_sri_id):
        total_retencion_30 = 0.0
        total_retencion_70 = 0.0
        total_retencion_100 = 0.0
        detalle_compra = CompraDetalle.objects.filter(compra_cabecera=self,
                                                      sustento_tributario_id=sustento_tributario_sri_id)
        print 'la longitod del detalle de la compra---'
        print str(len(detalle_compra))
        if len(detalle_compra) > 0:
            for obj in detalle_compra:
                if obj.codigo_ret_iva is not None:
                    print 'el codretiva es nonone'
                    print str(obj.codigo_ret_iva.porcentaje)+'el porcentajedelcodigoooo'
                    if obj.codigo_ret_iva.porcentaje == 30:
                        total_retencion_30 += obj.monto_ret_iva
                    else:
                        if obj.codigo_ret_iva.porcentaje == 70:
                            total_retencion_70 += obj.monto_ret_iva
                        else:
                            if obj.codigo_ret_iva.porcentaje == 100:
                                total_retencion_100 += obj.monto_ret_iva
                else:
                    print 'tiene cod none de ret iva'
        return total_retencion_30,total_retencion_70, total_retencion_100

    def getTotalRetencionxCodigo_10_y_20_y_30_y_70_y_100_x_sust(self, sustento_tributario_sri_id):
        total_retencion_10 = 0.0
        total_retencion_20 = 0.0
        total_retencion_30 = 0.0
        total_retencion_70 = 0.0
        total_retencion_100 = 0.0
        detalle_compra = CompraDetalle.objects.filter(compra_cabecera=self,
                                                      sustento_tributario_id=sustento_tributario_sri_id)
        #print 'la longitod del detalle de la compra---'
        #print str(len(detalle_compra))
        if len(detalle_compra) > 0:
            for obj in detalle_compra:
                if obj.codigo_ret_iva is not None:
                    #print 'el codretiva es nonone'
                    #print str(obj.codigo_ret_iva.porcentaje)+'el porcentajedelcodigoooo'
                    '''
                    if obj.codigo_ret_iva.porcentaje == 30:
                        total_retencion_30 += obj.monto_ret_iva
                    else:
                        if obj.codigo_ret_iva.porcentaje == 70:
                            total_retencion_70 += obj.monto_ret_iva
                        else:
                            if obj.codigo_ret_iva.porcentaje == 100:
                                total_retencion_100 += obj.monto_ret_iva
                            else:
                                if obj.codigo_ret_iva.porcentaje == 10:
                                    total_retencion_10 += obj.monto_ret_iva
                                else:
                                    if obj.codigo_ret_iva.porcentaje == 20:
                                        total_retencion_10 += obj.monto_ret_iva
                    '''
                    if obj.codigo_ret_iva.porcentaje == 30:
                        total_retencion_30 += obj.monto_ret_iva

                    if obj.codigo_ret_iva.porcentaje == 70:
                        total_retencion_70 += obj.monto_ret_iva

                    if obj.codigo_ret_iva.porcentaje == 100:
                        total_retencion_100 += obj.monto_ret_iva

                    if obj.codigo_ret_iva.porcentaje == 10:
                        total_retencion_10 += obj.monto_ret_iva

                    if obj.codigo_ret_iva.porcentaje == 20:
                        total_retencion_20 += obj.monto_ret_iva

                else:
                    print 'tiene cod none de ret iva'
        return total_retencion_30,total_retencion_70, total_retencion_100, total_retencion_10, total_retencion_20


    ######## GET TOTAL RETENCIONES DE CÓDIGO  70% #########
    def getTotalRetencionxCodigo_70_x_sust(self, sustento_tributario_sri_id):
        total_retencion_70 = 0.0
        try:
            detalle_compra = CompraDetalle.objects.filter(compra_cabecera=self,
                                                          sustento_tributario_id=sustento_tributario_sri_id)
            for obj in detalle_compra:
                if obj.codigo_ret_iva.porcentaje == 70:
                    total_retencion_70 += obj.monto_ret_iva

            return total_retencion_70
        except:
            return total_retencion_70

    ######## GET TOTAL RETENCIONES DE CÓDIGO  100% #########
    def getTotalRetencionxCodigo_100_x_sust(self, sustento_tributario_sri_id):
        total_retencion_100 = 0.0
        try:
            detalle_compra = CompraDetalle.objects.filter(compra_cabecera=self,
                                                          sustento_tributario_id=sustento_tributario_sri_id)
            for obj in detalle_compra:
                if obj.codigo_ret_iva.porcentaje == 100:
                    total_retencion_100 += obj.monto_ret_iva

            return total_retencion_100
        except:
            return total_retencion_100

    ######## GET TOTAL RETENCION DE CÓDIGO 30% #########
    def getTotalRetencionxCodigo_30(self):
        total_retencion_30 = 0.0
        try:
            retencion = RetencionCompraCabecera.objects.get(compra_cabecera=self, status=1)
            detalle_retencion = RetencionCompraDetalle.objects.filter(status=1, retencion_cabecera_compra=retencion)
            for obj in detalle_retencion:
                if obj.codigo_sri.porcentaje == 30:
                    total_retencion_30 += obj.valor_retenido
            return total_retencion_30
        except:
            pass

    ######## GET TOTAL RETENCIONES DE CÓDIGO  70% #########
    def getTotalRetencionxCodigo_70(self):
        total_retencion_70 = 0.0
        try:
            retencion = RetencionCompraCabecera.objects.get(compra_cabecera=self, status=1)
            detalle_retencion = RetencionCompraDetalle.objects.filter(status=1, retencion_cabecera_compra=retencion)
            for obj in detalle_retencion:
                if obj.codigo_sri.porcentaje == 70:    # Codigo de 70%
                    total_retencion_70 += obj.valor_retenido
            return total_retencion_70
        except:
            pass

    ######## GET TOTAL RETENCIONES DE CÓDIGO  100% #########
    def getTotalRetencionxCodigo_100(self):
        total_retencion_100 = 0.0
        try:
            retencion = RetencionCompraCabecera.objects.get(compra_cabecera=self, status=1)
            detalle_retencion = RetencionCompraDetalle.objects.filter(status=1, retencion_cabecera_compra=retencion)
            for obj in detalle_retencion:
                if obj.codigo_sri.porcentaje == 100:    # Codigo de 100%
                    total_retencion_100 += obj.valor_retenido
            return total_retencion_100
        except:
            pass


class CompraDetalle(models.Model):
    id = models.AutoField(primary_key=True)
    compra_cabecera = models.ForeignKey(CompraCabecera)
    plan_cuenta = models.ForeignKey(PlanCuenta)
    centro_costo = models.ForeignKey(Centro_Costo, null=True, blank=True)
    valor = models.FloatField()
    codigo_iva = models.ForeignKey(Codigo_SRI, related_name="codigo_iva")
    monto_iva = models.FloatField(default=0)
    tipo_iva = models.IntegerField()  # 1 Bien y 2 Servicios
    codigo_ret_fte = models.ForeignKey(Codigo_SRI, related_name="codigo_ret_fte", null=True, blank=True)
    monto_ret_fte = models.FloatField(default=0)
    codigo_ret_iva = models.ForeignKey(Codigo_SRI, related_name="codigo_ret_iva", null=True, blank=True)
    monto_ret_iva = models.FloatField(default=0)
    sustento_tributario = models.ForeignKey(Sustento_Tributario)
    porc_ret_fte = models.FloatField(default=0, blank=True, null=True)
    porc_ret_iva = models.FloatField(default=0, blank=True, null=True)

    status = models.IntegerField(default=1)

    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "compra_detalle"

    def getTotal(self):
        return self.valor + ((self.valor*self.porcentaje_iva)/100)


class ATS(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    compra_cabecera = models.ForeignKey(CompraCabecera)
    sustento_tributario_sri = models.ForeignKey(Sustento_Tributario)
    identificacion = models.ForeignKey(Identificacion)
    num_id = models.CharField(max_length=120)
    documento = models.ForeignKey(Documento)
    num_doc = models.CharField(max_length=50)
    fecha_reg = models.DateField()
    fecha_emi = models.DateField()
    autorizacion = models.CharField(max_length=37)
    vence = models.DateField()
    forma_pago_sri = models.ForeignKey(Forma_Pago_SRI, null=True, blank=True)
    tipo_pago_sri = models.ForeignKey(Tipo_Pago_SRI, null=True)
    pais = models.ForeignKey(Pais, null=True, blank=True)
    doble_tributacion = models.BooleanField(default=False, blank=True)
    sujeto_retencion = models.BooleanField(default=False, blank=True)
    base0 = models.FloatField(default=0)
    baseiva = models.FloatField(default=0)
    monto_iva = models.FloatField(default=0)
    base_ice = models.FloatField(default=0)
    monto_ice = models.FloatField(default=0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "ats"

    def getDatos(self):
        return DetalleCajaChica.objects.get(status=1, ats=self)


class RetencionCompraCabecera(models.Model):
    id = models.AutoField(primary_key=True)
    numero_ret = models.CharField(max_length=250) #secretencion1
    compra_cabecera = models.ForeignKey(CompraCabecera)
    fecha_emi = models.DateField() #fechaemiRet1
    direccion = models.CharField(max_length=200)
    vigencia_doc_empresa = models.ForeignKey(VigenciaDocEmpresa)
    totalretfte = models.FloatField(default=0.0)
    totalretiva = models.FloatField(default=0.0)

    # Campos de documentos electrónicos  -- Comentar antes de hacer cualquier commit
    web_url = models.CharField(max_length=100)
    autorizacion = models.CharField(max_length=100) #aut<retencion
    xml_firmado = models.CharField(max_length=255)
    estado_firma = models.IntegerField(default=0)
    #############################################

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "retencion_compra_cabecera"
        verbose_name = u'Retención Cabecera'
        verbose_name_plural = u'Retenciones'

    def get_num_doc(self):
        return self.vigencia_doc_empresa.serie + '-' + self.numero_ret

    def get_num_ret(self):
        try:
            return self.numero_ret
        except:
            return 'nooo'

    def getTotalIvaS(self):
        try:
            detalle_ret = RetencionCompraDetalle.objects.filter(status=1, retencion_cabecera=self)
            tot_iv_ser = 0.0
            for obj in detalle_ret:
                if obj.codigo_sri.vigencia_retencion.tipo_codigo_sri.id == 2:
                    if obj.codigo_retencion == 723:
                        tot_iv_ser += obj.total
            return tot_iv_ser
        except:
            return 0

    def getTotalIvaB(self):
        try:
            detalle_ret = RetencionCompraDetalle.objects.filter(status=1, retencion_cabecera=self)
            tot_iv_bie = 0.0
            for obj in detalle_ret:
                if obj.tipo_retencion == 2:
                    if obj.codigo_retencion == 721:
                        tot_iv_bie += obj.total
            return tot_iv_bie
        except:
            return 0.00

    def getTotalIva100(self):
        try:
            detalle_ret = RetencionCompraDetalle.objects.filter(status=1, retencion_cabecera=self)
            tot_iv_100 = 0.0
            for obj in detalle_ret:
                if obj.tipo_retencion == 2:
                    if obj.codigo_retencion == 725:
                        tot_iv_100 += obj.total
            return tot_iv_100
        except:
            return 0.00

    def is_DocRep(self, numero, serie):
        try:
            retencion = RetencionCompraCabecera.objects.filter(Q(status=1) | Q(status=2), numero_ret=numero, vigencia_doc_empresa=serie)
            if len(retencion) != 0:
                return True
            else:
                return False
        except RetencionCompraCabecera.DoesNotExist:
            return False


class RetencionCompraDetalle(models.Model):
    id = models.AutoField(primary_key=True)
    retencion_cabecera_compra = models.ForeignKey(RetencionCompraCabecera)
    plan_cuenta = models.ForeignKey(PlanCuenta)
    codigo_sri = models.ForeignKey(Codigo_SRI)
    tipo_iva = models.IntegerField(null=True, blank=True)  # 1 Bien y 2 Servicios
    base = models.FloatField()
    valor_retenido = models.FloatField()
    porc_ret_fte = models.FloatField(default=0, blank=True, null=True)
    porc_ret_iva = models.FloatField(default=0, blank=True, null=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "retencion_compra_detalle"

    def get_porcentaje_ret(self):
        if self.codigo_sri.porc_variable:
            if self.tipo_iva is not None:
                return self.porc_ret_iva
            else:
                return self.porc_ret_fte
        else:
            return self.codigo_sri.porcentaje


class Dividendos(models.Model):
    id = models.AutoField(primary_key=True)
    compra_cabecera = models.ForeignKey(CompraCabecera)
    fecha_pago = models.DateField()
    ir_asociado = models.FloatField()
    anio_utilidad = models.IntegerField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "dividendo"
        verbose_name = "Dividendo"
        verbose_name_plural = "Dividendos"


class Doc_Modifica(models.Model):
    id = models.AutoField(primary_key=True)
    compra_cabecera = models.ForeignKey(CompraCabecera)
    tipo_documento = models.ForeignKey(Documento)
    numero_documento = models.CharField(max_length=50, db_column="num_doc")
    autorizacion = models.CharField(max_length=37)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "documento_modifica"
        verbose_name = "Documento que Modifica"
        verbose_name_plural = "Documento que Modifica"


class Reembolso_gasto_compra(models.Model):
    id = models.AutoField(primary_key=True)
    compra_cabecera = models.ForeignKey(CompraCabecera)
    identificacion = models.ForeignKey(Identificacion)
    num_id = models.CharField(max_length=50)
    documento = models.ForeignKey(Documento)
    num_doc = models.CharField(max_length=50)
    fecha_emi = models.DateField()
    autorizacion = models.CharField(max_length=37)
    base0 = models.FloatField()
    baseiva = models.FloatField()
    monto_iva = models.FloatField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "reembolso_gasto_compra"
        verbose_name = "Reembolso de Gasto"
        verbose_name_plural = "Reembolsos de Gasto"


class CodigoImpuesto(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=10)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now(), null=True)
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "codigo_impuesto"


class CodigoTarifaIva(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=10)
    # No Objeto Impuesto = NOI
    # Exento de IVA = EIVA
    identificador_porcentaje = models.CharField(max_length=10)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now(), null=True)
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "codigo_tarifa_iva"


class Tipo_Item(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=120)
    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "tipo_item"


class Categoria_Item(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)
    tipo_item = models.ForeignKey(Tipo_Item)

    # Ctas. Producto Inventario
    plan_cuenta_inventario_proceso = models.ForeignKey(PlanCuenta, related_name="plan_cuenta_inventario_proceso", db_column="plan_cuenta_inventario_proceso_id", blank=True)
    plan_cuenta_compra = models.ForeignKey(PlanCuenta, related_name="cuenta_compra", db_column="plan_cuenta_compra_id", blank=True)
    plan_cuenta_costo_venta = models.ForeignKey(PlanCuenta, related_name="cuenta_costo_venta", db_column="plan_cuenta_costo_venta_id", blank=True)
    plan_cuenta_producto_terminado = models.ForeignKey(PlanCuenta, related_name="plan_cuenta_producto_terminado", db_column="plan_cuenta_producto_terminado_id", blank=True)

    # Ctas. Ventas
    plan_cuenta_venta = models.ForeignKey(PlanCuenta, related_name="cuenta_venta", db_column="plan_cuenta_venta_id", blank=True)
    plan_cuenta_devolucion_venta = models.ForeignKey(PlanCuenta, related_name="plan_cuenta_devolucion_venta", db_column="plan_cuenta_devolucion_venta_id", blank=True)
    plan_cuenta_descuento_venta = models.ForeignKey(PlanCuenta, related_name="plan_cuenta_descuento_venta", db_column="plan_cuenta_descuento_venta_id", blank=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "categoria_item"
        verbose_name = "Categoria de Item"
        verbose_name_plural = "Categorias de Item"

    def save(self, *args, **kwargs):
        try:
            if self.id is None:
                get_next_id(self)
            super(Categoria_Item, self).save(*args, **kwargs)
        except IntegrityError:
            super(Categoria_Item, self).save(*args, **kwargs)


    def isCategoriaTransaccion(self):
        '''
        Está función determina si la Categoría tiene movimientos
        '''
        if Item.objects.filter(status=1).filter(categoria_item=self).exists():
            items = Item.objects.filter(status=1).filter(categoria_item=self)
            for i in items:
                if Detalle_Comp_Contable.objects.filter(status=1).filter(plan_cuenta=i.categoria_item.plan_cuenta_compra).exists() or \
                        Detalle_Comp_Contable.objects.filter(status=1).filter(plan_cuenta=i.categoria_item.plan_cuenta_venta).exists():
                    return True
                else:
                    return False
        else:
            return False

    def getCuentaVenta(self):
        if self.plan_cuenta_venta is not None:
            return self.plan_cuenta_venta

    def getCuentaCostoVenta(self):
        if self.plan_cuenta_costo_venta is not None:
            return self.plan_cuenta_costo_venta

    def getCuentaCompInv(self):
        if self.plan_cuenta_compra is not None:
            return self.plan_cuenta_compra

    def getTipoCategoria(self):

        if self.tipo_item_id == 1:
            # Producto
            return 1
        elif self.tipo_item_id == 2:
            # Servicio
            return 2


class Tipo_Unidad(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)
    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "tipo_unidad"


class Unidad(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)
    tipo_unidad = models.ForeignKey(Tipo_Unidad, db_column="tipo")
    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "unidad"


class PropiedadItem1(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "propiedad1_item"


class PropiedadItem2(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "propiedad2_item"


class PropiedadItem3(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "propiedad3_item"


class PropiedadItem4(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "propiedad4_item"


class Item(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=100)
    categoria_item = models.ForeignKey(Categoria_Item, db_column="categoria_id")
    descripcion = models.CharField(max_length=220, null=True, blank=True)
    nombre = models.CharField(max_length=200, null=True, blank=True)
    min_inventario = models.IntegerField(null=True, blank=True)
    unidad = models.ForeignKey(Unidad)
    iva = models.FloatField(null=True, default=0, blank=True)
    porc_ice = models.FloatField(null=True, default=0, blank=True)
    costo = models.FloatField(null=True, default=0, blank=True)

    precio1 = models.FloatField(db_column="pvp1", default=0, null=True, blank=True)
    precio2 = models.FloatField(null=True, db_column="pvp2", default=0, blank=True)
    precio3 = models.FloatField(null=True, db_column="pvp3", default=0, blank=True)

    propiedad1 = models.ForeignKey(PropiedadItem1, null=True, blank=True)
    propiedad2 = models.ForeignKey(PropiedadItem2, null=True, blank=True)
    propiedad3 = models.ForeignKey(PropiedadItem3, null=True, blank=True)
    propiedad4 = models.ForeignKey(PropiedadItem4, null=True, blank=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "item"

    def ExisteItem(self):
        try:
            bodega = Item_Bodega.objects.get(item=self, status=1)
            flag = False

            if Item_Bodega.objects.filter(item=self, status=1).exists():
                if bodega.cantidad_actual > 0:
                    flag = True
            return flag
        except:
            pass

    def cantidadBodega(self):
        if Item_Bodega.objects.filter(item=self, status=1).exists():
            return Item_Bodega.objects.get(item=self, status=1).cantidad_actual
        else:
            return None


    def TieneIva(self):
        if self.iva > 0.0:
            flag = True
        else:
            flag = False
        return flag

    def TieneICE(self):

        if self.porc_ice is not None:
            if self.porc_ice > 0.0:
                return True
            else:
                return False
        else:
            return False

    def TipoItem(self):
        return self.categoria_item.tipo_item.id

    def getTotal(self):
        try:
            return round(float(self.precio1) + float(self.precio1*0.12), 2)
        except:
            return 0.0

    def cantidad_item_inicial_anual(self, anio):
        # Función que me retorna la cantidad incial del item
        if Item_Bodega_Anual.objects.filter(status=1, anio=anio, item=self).exists():
            item_bodega = Item_Bodega_Anual.objects.get(status=1, anio=anio, item=self)
            # DATOS INICIALES
            cantidad_inicial = item_bodega.cantidad_actual
        else:
            cantidad_inicial = 0

        return cantidad_inicial

    def costo_item_inicial_anual(self, anio):
        # Función que me retorna el costo incial del item
        if Item_Anual.objects.filter(status=1, anio=anio, item=self).exists():
            item_anual = Item_Anual.objects.get(status=1, anio=anio, item=self)
            # DATOS INICIALES
            costo_inicial = item_anual.costo_actual
        else:
            costo_inicial = 0.0

        return costo_inicial


class Cliente_Item(models.Model):
    id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(Cliente)
    item = models.ForeignKey(Item)
    ultima_venta = models.FloatField()
    fecha_ultima_venta = models.DateField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "cliente_item"


class Precios(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.ForeignKey(Item)
    precio1 = models.FloatField()
    precio2 = models.FloatField()
    precio3 = models.FloatField()
    precio4 = models.FloatField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "precios"


class Proveedor_Item(models.Model):
    id = models.AutoField(primary_key=True)
    proveedor = models.ForeignKey(Proveedores)
    item = models.ForeignKey(Item)
    cantidad_provision = models.FloatField()
    ultimo_costo = models.FloatField()
    fecha_ultima_compra = models.DateField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "proveedor_item"


class Bodega(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=100)
    responsable = models.CharField(max_length=200)
    direccion = models.CharField(max_length=200)
    ciudad = models.ForeignKey(Ciudad)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "bodega"


class Item_Bodega_Anual(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.ForeignKey(Item)
    bodega = models.ForeignKey(Bodega)
    anio = models.IntegerField()
    cantidad_actual = models.FloatField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "item_bodega_anual"



class Item_Anual(models.Model):
    id = models.AutoField(primary_key=True)
    anio = models.IntegerField()
    item = models.ForeignKey(Item)
    costo_actual = models.FloatField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "item_anual"


class Venta_Cabecera(models.Model):
    id = models.AutoField(primary_key=True)
    numero_comprobante = models.CharField(max_length=20, db_column="num_comp")
    fecha = models.DateField(db_column="fecha_emi")
    cliente = models.ForeignKey(Cliente, null=True, blank=True)
    vigencia_doc_empresa = models.ForeignKey(VigenciaDocEmpresa, db_column="vigencia_documento_empresa_id")
    reembolso_gasto = models.BooleanField(default=False)
    documento = models.ForeignKey(Documento)
    num_documento = models.CharField(max_length=50, db_column="num_doc")
    plazo = models.IntegerField(null=True, default=0)
    pago_cont_credito = models.ForeignKey(TipoPago, db_column="tipo_pago_id", null=True, blank=True)
    vendedor = models.ForeignKey(Colaboradores, db_column="vendedor_id", null=True, blank=True)
    nota = models.CharField(max_length=200, db_column="referencia", null=True, blank=True)
    concepto = models.TextField(null=True, blank=True)
    tipo_descuento_combo = models.IntegerField(null=True, db_column="tipo_descuento", default=0)  # Combo tipo de descuento Por producto o venta
    porc_descuento = models.FloatField(null=True, db_column="descuento_porc", default=0)  # Porcentaje del Descuento x VEnta
    subtotal_tarifa12 = models.FloatField(db_column="baseiva", default=0)
    subtotal_tarifa0 = models.FloatField(db_column="base0", default=0)
    descuento_tarifa12 = models.FloatField(null=True, db_column="descuentoiva", default=0)
    descuento_tarifa0 = models.FloatField(null=True, db_column="descuento0", default=0)
    total_ice = models.FloatField(default=0)
    iva_total = models.FloatField(default=0, db_column="monto_iva")
    cliente_direccion = models.ForeignKey(Cliente_Direccion, null=True, blank=True)
    contrato = models.ForeignKey(ContratoCabecera, null=True, blank=True)

    ############## Campos de documentos electrónicos  - Comentar antes de hacer cualquier commit #######################
    web_url = models.CharField(max_length=100, null=True, blank=True)
    autorizacion = models.CharField(max_length=100, null=True, blank=True)
    xml_firmado = models.CharField(max_length=255, null=True, blank=True)
    estado_firma = models.IntegerField(default=0)
    ####################################################################################################################

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "venta_cabecera"
        verbose_name = 'Venta'
        verbose_name_plural = 'Ventas'

    def get_num_doc(self):
        return self.vigencia_doc_empresa.serie + "-" + self.num_documento

    def getDetalleVenta(self):
        return Venta_Detalle.objects.filter(venta_cabecera=self)

    def getDocumentoFactura(self):
        try:
            return VigenciaDocEmpresa.objects.filter(venta_cabecera=self).order_by("id").reverse()[0]
        except:
            return None

    def existeItemsEntregar(self):
        detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=self, status=1).extra(where=["cant_vendida>cant_entregada"])
        sw = 0

        for c in detalle_venta:
            if c.item.categoria_item.tipo_item.id == 1:
                sw = 1

        if sw == 1:
            if len(detalle_venta) == 0:
                return False
            else:
                return True

    def cantidadItemsEntregar(self):
        detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=self, status=1).extra(where=["cant_vendida>cant_entregada"])
        sw = 0
        result = 0.0

        for c in detalle_venta:
            if c.item.categoria_item.tipo_item.id == 1:
                sw = 1

        if sw == 1:
            if len(detalle_venta) > 0:
                for c in detalle_venta:
                    result = result + (c.cantidad - c.cant_entregada)
            return result

    def cantidadItemsPendiente(self):
        detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=self)
        result = 0.0
        for c in detalle_venta:
            if c.item.TipoItem() == 1:
                result = (c.cantidad - c.cant_entregada)
        return result

    def getDescuentoTotal(self):
        try:
            return self.descuento_tarifa12 + self.descuento_tarifa0
        except:
            return 0.0

    def getTotalPagar(self):
        try:
            return self.subtotal_tarifa12 + self.subtotal_tarifa0 + self.iva_total + self.total_ice - self.descuento_tarifa0 - self.descuento_tarifa12
        except:
            return 0.0

    def getIdCobro(self):
        try:
            tipo_comp = TipoComprobante.objects.get(id=16)
            cuentas_por_cobrar = Cuentas_por_Cobrar.objects.exclude(naturaleza=2).get(venta_cabecera=self)

            if CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
                cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1)[0].cobro
                return cobro.id
            else:
                return 0
        except:
            return 0

    def getNumCompRetVta(self):
        try:
            tipo_comp = TipoComprobante.objects.get(id=16)
            cuentas_por_cobrar = Cuentas_por_Cobrar.objects.exclude(naturaleza=2).get(venta_cabecera=self)

            if CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
                cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1)[0].cobro
                return cobro.num_comp
            else:
                return 0
        except:
            return 0

    def getRetencionVta(self):
        try:
            tipo_comp = TipoComprobante.objects.get(id=16)
            cuentas_por_cobrar = Cuentas_por_Cobrar.objects.exclude(naturaleza=2).get(venta_cabecera=self)

            if CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
                cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1)[0].cobro
                return cobro

        except:
            return None

    def existeGuiaREm(self):
        return CabeceraGuiaRemision.objects.filter(venta_cabecera=self).exists()

    def ExisteDevolucionVenta(self):
        try:
            if Cuentas_por_Cobrar.objects.filter(naturaleza=2, venta_cabecera=self).exists():
                return True
            else:
                return False
        except:
            return False

    def getRetencionVenta(self):
        cuentas_por_cobrar = Cuentas_por_Cobrar.objects.exclude(naturaleza=2).get(venta_cabecera=self)
        return CXC_cobro.objects.filter(cobro__tipo_comprobante_id=16, cuentas_x_cobrar=cuentas_por_cobrar, status=1).exists()

    def get_estado_factura_electronica(self):

        if self.estado_firma == 1:
            return '<span title="Guardado">G</span>'
        elif self.estado_firma == 2:
            return '<span title="Firmado">F</span>'
        elif self.estado_firma == 3:
            return '<span title="Enviado SRI">E</span>'
        elif self.estado_firma == 4:
            return '<span title="Autorizado">A</span>'
        elif self.estado_firma == 5:
            return '<i class="fa fa-envelope-o" title="Autorizado y Enviado al Correo"></i>'
        else:
            return ""

    def existe_movimientos_inventario(self):
        num_doc_vta = self.vigencia_doc_empresa.serie+"-"+self.num_documento
        return Inventario_Cabecera.objects.filter(status=1, num_doc=num_doc_vta, tipo_mov_inventario__id=2).exists()

class Venta_Detalle(models.Model):
    id = models.AutoField(primary_key=True)
    venta_cabecera = models.ForeignKey(Venta_Cabecera)
    item = models.ForeignKey(Item)
    concepto = models.CharField(max_length=200, null=True, blank=True)
    centro_costo = models.ForeignKey(Centro_Costo, null=True, blank=True)

    #cantidad = models.IntegerField(db_column="cant_vendida")
    cantidad = models.FloatField(db_column="cant_vendida")
    #cant_entregada = models.IntegerField(null=True, default=0)
    #cant_devuelta = models.IntegerField(null=True, default=0)
    cant_entregada = models.FloatField(null=True, default=0)
    cant_devuelta = models.FloatField(null=True, default=0)

    precio_unitario = models.FloatField()
    descuento_porc = models.FloatField(default=0)
    descuento_valor = models.FloatField(default=0)
    porcentaje_ice = models.FloatField(default=0)
    monto_iva = models.FloatField(default=0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "venta_detalle"


    def getVentaCabecera(self):
        if Venta_Detalle.objects.filter(venta_cabecera=self.venta_cabecera).exists():
            venta = Venta_Detalle.objects.filter(venta_cabecera=self.venta_cabecera)[0].venta_cabecera
            return venta
        else:
            return 0


class Cuentas_por_Pagar(models.Model):
    id = models.AutoField(primary_key=True)
    proveedor = models.ForeignKey(Proveedores)
    naturaleza = models.IntegerField() # 1 debe, 2 haber
    documento = models.ForeignKey(Documento, null=True)
    num_doc = models.CharField(max_length=20)
    compra_cabecera = models.ForeignKey(CompraCabecera, null=True, blank=True)

    fecha_reg = models.DateField(null=True)
    fecha_emi = models.DateField(null=True)
    plazo = models.IntegerField(null=True)
    monto = models.FloatField()
    pagado = models.FloatField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "cuentas_x_pagar"
        verbose_name = "Cuenta por Pagar"
        verbose_name_plural = "Cuentas por Pagar"

    def get_referencia(self):
        """
        Obtiene la referencia del objeto (concepto en compras)
        :return:
        """
        try:
            if self.naturaleza == 2:
                ref = self.compra_cabecera.concepto
            else:
                if self.documento_id == 4:  # Nota de crédito
                    ref = "Nota de Crédito"
                elif self.documento_id == 39:
                    try:
                        comp_contable = Cabecera_Comp_Contable.objects.get(numero_comprobante=self.num_doc)
                        ref = comp_contable.concepto_comprobante
                    except (Cabecera_Comp_Contable.DoesNotExist, ValueError):
                        ref = ""
                else:
                    ref = ""
        except:
            ref = ""
        return ref

    def get_lista_pagos_x_fecha(self, fecha_ini, fecha_fin):
        """
        Función que me devuelve la lista de pagos que se han hecho a una determinada
        cuenta por pagar, por fech
        :return:
        """
        return CXP_pago.objects.filter(cuentas_x_pagar=self,
                                       pago__fecha_reg__range=(fecha_ini, fecha_fin))\
            .exclude(cuentas_x_pagar__num_doc=F('pago__num_comp'))

    def get_pago_fecha(self, fecha_ini, fecha_fin):
        """
        Funcion que me permite obtener la cantidad de pago que se hizo en ese
        lapso de tiempo con ese proveedor

        #### if cuentas_x_pagar.naturaleza == 1: ####
        Se pregunta por la naturaleza ya que cuando es
        un anticipo se crea automaticamente un registro en cxp_pago, justo ese registro es el que
        se tiene que ignorar ya que es el registro donde se registro el anticipo, al contrario en
        compras cuando se registra una factura, no se crea un registro en cxp_pago
        """
        total_pago = 0.0
        if self.naturaleza == 1:  # Anticipo
            # reg_ant: es el registro cuando se hizo el anticipo, ese no se debe de contar en el total de pago
            reg_ant = CXP_pago.objects.filter(cuentas_x_pagar=self,
                                               pago__fecha_reg__range=(fecha_ini,
                                                                       fecha_fin)).order_by("id").reverse()[0]

            for obj in CXP_pago.objects.filter(cuentas_x_pagar=self,
                                               pago__fecha_reg__range=(fecha_ini, fecha_fin),
                                               status=1).order_by("id"):
                total_pago += obj.monto
            total_pago -= reg_ant.monto
        else:
            for obj in CXP_pago.objects.filter(cuentas_x_pagar=self,
                                               pago__fecha_reg__range=(fecha_ini, fecha_fin),
                                               status=1):
                total_pago += obj.monto
        return total_pago


class AjusteVentaCabecera(models.Model):
    id = models.AutoField(primary_key=True)
    num_comp = models.CharField(max_length=20)
    fecha_emi = models.DateField()
    vigencia_doc_empresa = models.ForeignKey(VigenciaDocEmpresa)
    cliente = models.ForeignKey(Cliente)
    venta_cabecera = models.ForeignKey(Venta_Cabecera, null=True, blank=True)

    num_doc = models.CharField(max_length=100, null=True, blank=True)
    num_doc_modifica = models.CharField(max_length=50, null=True, blank=True)
    fecha_doc_modifica = models.DateField(null=True, blank=True)
    concepto = models.CharField(max_length=500)
    razon_modifica = models.CharField(max_length=300)

    base0 = models.FloatField(default=0)
    descuento0 = models.FloatField(default=0)
    tarifa12 = models.FloatField(default=0, db_column="baseiva")
    descuentoiva = models.FloatField(default=0)
    monto_iva = models.FloatField(default=0)  # ambigua
    total_ice = models.FloatField(default=0)  # ambigua

    # Campos de documentos electrónicos
    web_url = models.CharField(max_length=100, null=True, blank=True)
    autorizacion = models.CharField(max_length=100, null=True, blank=True)
    xml_firmado = models.CharField(max_length=255, null=True, blank=True)
    estado_firma = models.IntegerField(default=0, null=True, blank=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "ajuste_venta_cabecera"
        verbose_name = "Ajuste de Venta"
        verbose_name_plural = "Ajuste de Ventas"

    def getTotalNotaCredito(self):
        try:
            return self.tarifa12 + self.base0 + self.monto_iva + self.total_ice - self.descuento0 - self.descuentoiva
        except:
            return 0.0

    def getTotalDescuentoNotaCredito(self):
        try:
            return self.descuentoiva + self.descuento0
        except:
            return 0.0

    def get_num_doc(self):
        return self.vigencia_doc_empresa.serie + "-" + self.num_doc

    def get_estado_nota_credito_electronica(self):
        if self.estado_firma == 1:
            return '<span title="Guardado">G</span>'
        elif self.estado_firma == 2:
            return '<span title="Firmado">F</span>'
        elif self.estado_firma == 3:
            return '<span title="Enviado SRI">E</span>'
        elif self.estado_firma == 4:
            return '<span title="Autorizado">A</span>'
        elif self.estado_firma == 5:
            return '<i class="fa fa-envelope-o" title="Autorizado y Enviado al Correo"></i>'
        else:
            return ""


class AjusteVentaDetalle(models.Model):
    id = models.AutoField(primary_key=True)
    ajuste_venta_cabecera = models.ForeignKey(AjusteVentaCabecera)
    item = models.ForeignKey(Item)
    centro_costo = models.ForeignKey(Centro_Costo, null=True, blank=True)
    cantidad_devolver = models.FloatField(null=True, blank=True)
    #precio_u = models.FloatField()
    valor = models.FloatField()
    descuento_valor = models.FloatField(default=0, null=True, blank=True)
    monto_iva = models.FloatField(default=0)
    porcentaje_ice = models.FloatField(default=0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "ajuste_venta_detalle"


class Tarjeta_Credito(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)
    numero_tarjeta = models.CharField(max_length=50)
    fecha_expiracion = models.DateField()
    dia_corte = models.IntegerField()
    status = models.IntegerField(default=1)
    plan_cuenta = models.ForeignKey(PlanCuenta)
    fecha_ultimo_cierre = models.DateField()
    fecha_ultimo_conciliacion = models.DateField()

    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "tarjeta_credito"


class Pago(models.Model):
    id = models.AutoField(primary_key=True)
    proveedor = models.ForeignKey(Proveedores)
    fecha_reg = models.DateField()
    tipo_comprobante = models.ForeignKey(TipoComprobante, null=True)  # Para hacer un match con el asiento
    num_comp = models.CharField(max_length=20, null=True)  # Para hacer un match con el asiento
    forma_pago = models.ForeignKey(FormaPago)
    monto = models.FloatField()
    num_cheque = models.IntegerField()
    referencia = models.CharField(max_length=50)
    fecha_pago = models.DateField()
    plazo_diferido = models.IntegerField()
    interes = models.FloatField()
    concepto = models.CharField(max_length=255)
    pago_id = models.ForeignKey('self', blank=True, null=True, db_column="pago_id")

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "pago"
        verbose_name = "Pago"
        verbose_name_plural = "Pagos"

    def esta_reversado(self):
        """
        Retorna True si el cobro ya ha sido reversado con anticipación
        :return:
        """
        return Pago.objects.filter(pago_id=self.id, status=1).exists()

    def parteentera(self):
        try:
            print str(self.monto)
            valor= self.monto
            if (len(str(int(self.monto)))< 4):
                return str(int(self.monto))+'.'+splits(self.monto,'.',1)
            else:
                return str(int(self.monto))[0:len(str(int(self.monto)))-3 ]+','+str(int(self.monto))[len(str(int(self.monto)))-3:len(str(int(self.monto)))]+'.'+splits(self.monto,'.',1)
                #return 'algomasgrandequecuatro'
        except:
            print 'este pago es incorrecto'
            return 'este pago es incorrecto'

    def get_status(self):
        """
        Obtiene el status del cobro
        :return:
        """
        if self.esta_reversado():
            return "Reversado"
        else:
            return Estados.objects.using("base_central").get(codigo=self.status).descripcion

    def get_str_forma_pago(self):
        """
        Retorna en un string la forma de pago
        :return:
        """
        if self.referencia is not None:
            referencia = unicode(self.referencia)
        else:
            referencia = ""

        plan_cuenta = ""
        for obj in self.getCxPpago():
            try:
                if obj.plan_cuenta.tipo_id == 1:
                    plan_cuenta = obj.plan_cuenta
            except:
                pass

        if self.forma_pago_id == 1:  # Cheque
            if plan_cuenta != "":
                return unicode(self.forma_pago.descripcion)  # + " #" + str(self.num_cheque)[0:9] + " " + plan_cuenta.descripcion
            else:
                return unicode(self.forma_pago.descripcion)  # + " #" + str(self.num_cheque)[0:9]
        elif self.forma_pago_id == 4:  # Transferencia
            return unicode(self.forma_pago.descripcion)
        elif self.forma_pago_id == 5:  # Cruce de documentos
            return unicode(self.forma_pago.descripcion)
        elif self.forma_pago_id == 8:  # Depósito
            return unicode(self.forma_pago.descripcion)
        else:
            return unicode(self.forma_pago.descripcion)

    def getCxPpago(self):
        return CXP_pago.objects.filter(pago=self, cuentas_x_pagar__proveedor=self.proveedor, status=1)


    def monto_to_word(self):
        return to_word(int(self.monto))


class CXP_pago(models.Model):
    id = models.AutoField(primary_key=True)
    cuentas_x_pagar = models.ForeignKey(Cuentas_por_Pagar)
    pago = models.ForeignKey(Pago)
    monto = models.FloatField()
    anticipo = models.FloatField(default=0)
    concepto = models.CharField(max_length=100)
    plan_cuenta = models.ForeignKey(PlanCuenta)
    naturaleza = models.IntegerField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    def get_pagos_ant(self):
        cxp_pag = CXP_pago.objects.exclude(id=self.id).filter(cuentas_x_pagar=self.cuentas_x_pagar,
                                                              pago__fecha_reg__lte=self.pago.fecha_reg,
                                                              pago__status=1, status=1)
        pagos_ant = 0.0
        for obj in cxp_pag:
            pagos_ant += obj.monto

        return pagos_ant

    def get_saldo_doc(self):
        monto_doc = self.cuentas_x_pagar.monto
        pagos_ant = self.get_pagos_ant()

        if self.cuentas_x_pagar.naturaleza == 1 and self.cuentas_x_pagar.compra_cabecera_id is None:
            # Se duplica valor ya que el anticipo se guarda en cuentas por cobrar y cobros
            return 2*monto_doc - (pagos_ant + self.monto)
        else:
            return monto_doc - (pagos_ant + self.monto)


    class Meta:
        db_table = "cxp_pago"


class Cuentas_por_Cobrar(models.Model):
    id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(Cliente)
    naturaleza = models.IntegerField()
    venta_cabecera = models.ForeignKey(Venta_Cabecera)
    documento = models.ForeignKey(Documento)
    num_doc = models.CharField(max_length=20)
    fecha_emi = models.DateField(null=True)
    fecha_reg = models.DateField(null=True)
    plazo = models.IntegerField(null=True)
    monto = models.FloatField()
    cobrado = models.FloatField(default=0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    def get_referencia(self):
        try:
            if self.naturaleza == 1:
                ref = self.venta_cabecera.concepto
            else:
                if self.documento_id == 4: # Nota de crédito
                    ref = "Nota de Crédito"
                elif self.documento_id == 39:
                    try:
                        comp_contable = Cabecera_Comp_Contable.objects.get(numero_comprobante=self.num_doc)
                        ref = comp_contable.concepto_comprobante
                    except (Cabecera_Comp_Contable.DoesNotExist, ValueError):
                        ref = ""
                else:
                    ref = ""
        except:
            ref = ""
        return ref

    class Meta:
        db_table = "cuentas_x_cobrar"
        verbose_name = "Cuentas por Cobrar"
        verbose_name_plural = "Cuentas por Cobrar"


class Cobro(models.Model):
    id = models.AutoField(primary_key=True)
    cliente = models.ForeignKey(Cliente)
    fecha_reg = models.DateField()
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    num_comp = models.CharField(max_length=20)
    forma_pago = models.ForeignKey(FormaPago)
    monto = models.FloatField()
    referencia = models.CharField(max_length=50)
    fecha_pago = models.DateField()
    concepto = models.CharField(max_length=255)
    cobro_id = models.ForeignKey('self', blank=True, null=True, db_column="cobro_id") # 1 septirmbre este campo tiene algo raro, al borrarlo ejecuta bien se cae porque esta coplumna no existe :o

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    def getCxPcobro(self):
        return CXC_cobro.objects.filter(cobro=self, status=1)

    def esta_reversado(self):
        """
        Retorna True si el cobro ya ha sido reversado con anticipación
        :return:
        """
        return Cobro.objects.filter(cobro_id=self.id, status=1).exists()

    def get_status(self):
        """
        Obtiene el status del cobro
        :return:
        """
        if self.esta_reversado():
            return "Reversado"
        else:
            return Estados.objects.using("base_central").get(codigo=self.status).descripcion

    def get_str_forma_pago(self):
        """
        Retorna en un string la forma de pago
        :return:
        """
        if self.referencia is not None:
            referencia = str(self.referencia)
        else:
            referencia = ""
        try:
            plan_cuenta = self.getCxPcobro()[0].plan_cuenta.descripcion
        except:
            plan_cuenta = ""
        if self.forma_pago_id == 1:  # Cheque
            return self.forma_pago.descripcion + " #" + str(self.num_comp) + " - " + plan_cuenta
        elif self.forma_pago_id == 4:  # Transferencia
            return self.forma_pago.descripcion + " - " + plan_cuenta + referencia[0:9]
        elif self.forma_pago_id == 5:  # Cruce de documentos
            return self.forma_pago.descripcion + " - " + referencia[0:9]
        elif self.forma_pago_id == 8:  # Depósito
            return self.forma_pago.descripcion + " - " + plan_cuenta + referencia[0:9]
        else:
            return self.forma_pago.descripcion + " - " + plan_cuenta + referencia[0:9]

    def get_total_iva_ret(self):
        """
        Retorna el total de  valor_ret_iva
        de ventas activas por mes, año y cliente
        """
        tipo_comprobante = TipoComprobante.objects.filter(status=1).get(id=16)    # Ret en Venta
        tipo_cuenta_ret_iva = TipoCuenta.objects.get(status=1, id=22)             # Tipo Cuenta Retención IVA en Ventas

        lista_c_x_c = CXC_cobro.objects.filter(status=1, cobro=self,
                                               cobro__tipo_comprobante=tipo_comprobante,
                                               plan_cuenta__tipo=tipo_cuenta_ret_iva)

        if CXC_cobro.objects.filter(status=1, cobro=self, cobro__tipo_comprobante=tipo_comprobante,
                                    plan_cuenta__tipo=tipo_cuenta_ret_iva).exists():
            for c in lista_c_x_c:
                return float(c.monto)
        else:
            return 0.0

    def get_total_ret_renta(self):
        """
        Retorna el total de  valor_ret_renta
        de ventas activas por mes, año y cliente
        """
        tipo_comprobante = TipoComprobante.objects.filter(status=1).get(id=16)  # Ret en Venta
        tipo_cuenta_ret_fte = TipoCuenta.objects.get(status=1, id=15)           # Tipo Cuenta Retención Fuente en Ventas

        lista_c_x_c = CXC_cobro.objects.filter(status=1, cobro=self,
                                               cobro__tipo_comprobante=tipo_comprobante,
                                               plan_cuenta__tipo=tipo_cuenta_ret_fte)

        if CXC_cobro.objects.filter(status=1, cobro=self, cobro__tipo_comprobante=tipo_comprobante,
                                    plan_cuenta__tipo=tipo_cuenta_ret_fte).exists():
            for c in lista_c_x_c:
                return float(c.monto)
        else:
            return 0.0

    class Meta:
        db_table = "cobro"
        verbose_name = "Cobro"
        verbose_name_plural = "Cobros"


class CXC_cobro(models.Model):
    id = models.AutoField(primary_key=True)
    cuentas_x_cobrar = models.ForeignKey(Cuentas_por_Cobrar)
    cobro = models.ForeignKey(Cobro)
    monto = models.FloatField()
    anticipo = models.FloatField(default=0)
    concepto = models.CharField(max_length=100)
    plan_cuenta = models.ForeignKey(PlanCuenta)
    naturaleza = models.IntegerField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    def get_cobros_ant(self):
        cxc_cob = CXC_cobro.objects.exclude(id=self.id).filter(cuentas_x_cobrar=self.cuentas_x_cobrar,
                                                               cobro__fecha_reg__lte=self.cobro.fecha_reg,
                                                               status=1)
        cobros_ant = 0.0
        for obj in cxc_cob:
            cobros_ant += obj.monto

        return cobros_ant

    def get_saldo_doc(self):
        monto_doc = self.cuentas_x_cobrar.monto
        cobros_ant = self.get_cobros_ant()

        if self.cuentas_x_cobrar.naturaleza == 2 and self.cuentas_x_cobrar.venta_cabecera_id is None:
            # Se duplica valor ya que el anticipo se guarda en cuentas por cobrar y cobros
            return 2*monto_doc - (cobros_ant + self.monto)
        else:
            return monto_doc - (cobros_ant + self.monto)

    class Meta:
        db_table = "cxc_cobro"

class Codigo_Tablas(models.Model):
    id = models.AutoField(primary_key=True)
    nombre_tabla = models.CharField(max_length=100)
    nombre_columna = models.CharField(max_length=100)
    codigo = models.CharField(max_length=20)
    descripcion = models.CharField(max_length=100)

    class Meta:
        db_table = "codigo_tablas"

class Cabecera_Caja_Chica(models.Model):
    id = models.AutoField(primary_key=True)
    numero_comprobante = models.CharField(max_length=20, db_column="num_comp")
    fecha = models.DateField(db_column="fecha_reg")
    plan_cuenta = models.ForeignKey(PlanCuenta, db_column="plan_cuenta_id")
    concepto = models.CharField(max_length=350)
    total_pagar = models.FloatField(null=True, db_column="valor")

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)


    class Meta:
        db_table = "caja_chica_cabecera"

    def getComprasCajaChicaRet0(self):
        total = 0.00
        try:
            detalle_comp = DetalleCajaChica.objects.filter(status=1, cabecera_caja_chica=self)
            for obj in detalle_comp:
                total += obj.valor
        except:
            pass
        return total

    def getBaseTotalBase0(self):
        total = 0.0
        for c in DetalleCajaChica.objects.filter(cabecera_caja_chica=self):
            total += c.ats.base0
        return total

    def getBaseTotalBaseIVA(self):
        total = 0.0
        for c in DetalleCajaChica.objects.filter(cabecera_caja_chica=self):
            total += c.ats.baseiva
        return total

class DetalleCajaChica(models.Model):
    id = models.AutoField(primary_key=True)
    cabecera_caja_chica = models.ForeignKey(Cabecera_Caja_Chica, db_column="cabecera_caja_chica_id")
    ats = models.ForeignKey(ATS)
    plan_cuenta = models.ForeignKey(PlanCuenta, db_column="plan_cuenta_id")
    codigo_sri = models.ForeignKey(Codigo_SRI, db_column="codigo_sri_id")
    iva_cg = models.IntegerField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "caja_chica_detalle"

class VigenciaDocPersona(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    cliente_proveedor_id = models.IntegerField()
    documento_sri = models.ForeignKey(Documento)
    serie = models.CharField(max_length=7)
    autorizacion = models.CharField(max_length=50)
    sec_inicial = models.CharField(max_length=50)
    sec_final = models.CharField(max_length=50)
    fecha_emi = models.DateField()
    fecha_vencimiento = models.DateField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "vigencia_doc_persona"

class TipoMovGuiaRemision(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "tipo_mov_guia_rem"

class CabeceraGuiaRemision(models.Model):
    id = models.AutoField(primary_key=True)
    venta_cabecera = models.ForeignKey(Venta_Cabecera, null=True)
    fecha_emi = models.DateField()
    vigencia_doc_empresa = models.ForeignKey(VigenciaDocEmpresa)
    num_doc = models.CharField(max_length=50)
    cliente = models.ForeignKey(Cliente)
    direccion = models.CharField(max_length=220)
    ciudad = models.ForeignKey(Ciudad)
    fecha_inicio = models.DateField()
    fecha_final = models.DateField()
    tipo_mov_guia_rem = models.ForeignKey(TipoMovGuiaRemision)
    nombre_conductor = models.CharField(max_length=220)
    identificacion_conductor = models.CharField(max_length=50)
    direccion_conductor = models.CharField(max_length=220, null=True)
    concepto = models.CharField(max_length=250)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "guia_rem_cabecera"
        verbose_name = u"Guia de Remisión"
        verbose_name_plural = u"Guias de Remisión"

    def getCantidadesDetalleGuiaRem(self):
        lista_detalle = DetalleGuiaRemision.objects.filter(guia_rem_cabecera=self)
        cantidad = 0

        for c in lista_detalle:
            cantidad += c.cantidad

        return cantidad


    def cantidadItemVenta(self):
        cant = 0.0
        lista_detalle = Venta_Detalle.objects.filter(venta_cabecera=self.venta_cabecera)

        for c in lista_detalle:
            if c.item.TipoItem() == 1:
                cant += c.cantidad
        return cant

    def cantidadItemsEntregar2(self):
        detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=self.venta_cabecera, status=1).extra(where=["cant_vendida>cant_entregada"])
        sw = 0
        result = 0.0

        for c in detalle_venta:
            if c.item.categoria_item.tipo_item.id == 1:
                sw = 1
        if sw == 1:
            if len(detalle_venta) > 0:
                for c in detalle_venta:
                    result = result + (c.cantidad - c.cant_entregada)

            return result

class DetalleGuiaRemision(models.Model):
    id = models.AutoField(primary_key=True)
    guia_rem_cabecera = models.ForeignKey(CabeceraGuiaRemision)
    item = models.ForeignKey(Item, null=True)
    cantidad = models.IntegerField()
    num_serie = models.CharField(max_length=100, null=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "guia_rem_detalle"


class Tipo_Mov_Inventario(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    alias = models.IntegerField()
    modifica_costo = models.BooleanField(default=None)
    tipo_inventario = models.IntegerField()
    status = models.IntegerField(default=1)

    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "tipo_mov_inventario"

class Item_Bodega(models.Model):
    id = models.AutoField(primary_key=True)
    item = models.ForeignKey(Item)
    bodega = models.ForeignKey(Bodega)
    cantidad_actual = models.FloatField(default=0)
    cantidad_provisionada = models.FloatField(default=0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "item_bodega"
        verbose_name = 'Item Bodega'
        verbose_name_plural = 'Items Bodega'

    def get_item_bodega(self):
        if Item_Bodega.objects.filter(status=1, item=self.item).exists():
            return self.item

    get_item_bodega.short_description = "ITEM"

class Inventario_Cabecera(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    num_comp = models.CharField(max_length=20)
    fecha_reg = models.DateField()
    tipo_mov_inventario = models.ForeignKey(Tipo_Mov_Inventario)
    documento = models.ForeignKey(Documento)
    num_doc = models.CharField(max_length=20)
    cliente_proveedor_id = models.IntegerField()
    detalle = models.CharField(max_length=220)
    contrato = models.ForeignKey(ContratoCabecera, null=True, blank=True)
    orden_comp = models.DateTimeField(default=datetime.datetime.now())

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    def getProveedor(self):
        try:
            return Proveedores.objects.get(id=self.cliente_proveedor_id)
        except:
            return None

    def get_cliente_prov(self):
        try:
            if self.tipo_mov_inventario_id == 1 or self.tipo_mov_inventario_id == 6 or self.tipo_mov_inventario_id == 2:
                return Proveedores.objects.get(id=self.cliente_proveedor_id).razon_social[0:30]
            elif self.tipo_mov_inventario_id == 7:
                return Cliente.objects.get(id=self.cliente_proveedor_id).razon_social[0:30]
            else:
                return None
        except:
            return None

    def getInventarioDetalles(self):
        try:
           detalles = Inventario_Detalle.objects.filter(status=1,inventario_cabecera_id= self.id)
           return detalles
        except:
            return None

    class Meta:
        db_table = "inventario_cabecera"
        verbose_name = "Cabecera de Inventario"
        verbose_name_plural = "Cabeceras de Inventario"

class Inventario_Detalle(models.Model):
    id = models.AutoField(primary_key=True)
    inventario_cabecera = models.ForeignKey(Inventario_Cabecera)
    bodega = models.ForeignKey(Bodega)
    item = models.ForeignKey(Item)
    cantidad = models.FloatField(default=0)
    costo = models.FloatField(default=0)
    plan_cuenta = models.ForeignKey(PlanCuenta, null=True, blank=True)


    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "inventario_detalle"

class Cotizacion_Cabecera(models.Model):
    id = models.AutoField(primary_key=True)
    fecha_emi = models.DateField()
    cliente = models.ForeignKey(Cliente)
    vendedor = models.ForeignKey(Colaboradores, db_column="vendedor_id")
    concepto = models.CharField(max_length=250)
    tipo_descuento = models.IntegerField()
    descuento_porc = models.FloatField()
    base0 = models.FloatField()
    descuento0 = models.FloatField()
    baseiva = models.FloatField()
    descuentoiva = models.FloatField()
    montoiva = models.FloatField()
    total_ice = models.FloatField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "cotizacion_cabecera"

class Cotizacion_Detalle(models.Model):
    id = models.AutoField(primary_key=True)
    cotizacion_cabecera = models.ForeignKey(Cotizacion_Cabecera)
    item = models.ForeignKey(Item)
    cantidad = models.IntegerField()
    precio_unitario = models.FloatField()
    descuento_porc = models.FloatField()
    descuento_valor = models.FloatField()
    porcentaje_ice = models.FloatField()
    monto_iva = models.FloatField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "cotizacion_detalle"

class CompraDetalleContrato(models.Model):
    """
    Clase que guarda el la relacioón con el detalle de la compra
    para luego ser utilizado en Costos de Producción
    """
    id = models.AutoField(primary_key=True)
    compra_detalle = models.ForeignKey(CompraDetalle)
    contrato_cabecera = models.ForeignKey(ContratoCabecera)
    valor = models.FloatField(default=0.0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "contrato_detalle_compra"

class CostoDistribuirCompras(models.Model):
    """
    Clase que guarda el costo por distribuir a los contratos
    en el módulo de proyectos
    """
    id = models.AutoField(primary_key=True)
    compra_cabecera = models.ForeignKey(CompraCabecera)
    monto = models.FloatField()
    distribuido = models.FloatField(default=0.0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "costo_distribuir_compras"

class SaldoCuenta(models.Model):
    id = models.AutoField(primary_key=True)
    anio = models.IntegerField()
    plan_cuenta = models.ForeignKey(PlanCuenta)
    saldo_inicial = models.FloatField()
    debe1 = models.FloatField()
    haber1 = models.FloatField()
    debe2 = models.FloatField()
    haber2 = models.FloatField()
    debe3 = models.FloatField()
    haber3 = models.FloatField()
    debe4 = models.FloatField()
    haber4 = models.FloatField()
    debe5 = models.FloatField()
    haber5 = models.FloatField()
    debe6 = models.FloatField()
    haber6 = models.FloatField()
    debe7 = models.FloatField()
    haber7 = models.FloatField()
    debe8 = models.FloatField()
    haber8 = models.FloatField()
    debe9 = models.FloatField()
    haber9 = models.FloatField()
    debe10 = models.FloatField()
    haber10 = models.FloatField()
    debe11 = models.FloatField()
    haber11 = models.FloatField()
    debe12 = models.FloatField()
    haber12 = models.FloatField()

    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "saldo_cuenta"

    def get_saldo_debe_acum(self, mes):
        """
        Función que me devuelve el saldo acumulado deudor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        total = 0.0
        if mes == 1:  # Enero
            total += convert_cero_none(self.debe1)
        elif mes == 2:  # Febrero
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2)
        elif mes == 3:  # Marzo
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3)
        elif mes == 4:  # Abril
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4)
        elif mes == 5:  # Mayo
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5)
        elif mes == 6:  # Junio
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6)
        elif mes == 7:  # Julio
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7)
        elif mes == 8:  # Agosto
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8)
        elif mes == 9:  # Septiembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9)
        elif mes == 10:  # Octubre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10)
        elif mes == 11:  # Noviembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10) + convert_cero_none(self.debe11)
        elif mes == 12:  # Diciembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10) + convert_cero_none(self.debe11) + convert_cero_none(self.debe12)
        else:  # Sin mes
            return 0.00
        return total

    def get_saldo_haber_acum(self, mes):
        """
        Función que me devuelve el saldo acumulado acreedor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        total = 0.0
        if mes == 1:  # Enero
            total += convert_cero_none(self.haber1)
        elif mes == 2:  # Febrero
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2)
        elif mes == 3:  # Marzo
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + + convert_cero_none(self.haber3)
        elif mes == 4:  # Abril
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4)
        elif mes == 5:  # Mayo
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5)
        elif mes == 6:  # Junio
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6)
        elif mes == 7:  # Julio
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7)
        elif mes == 8:  # Agosto
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8)
        elif mes == 9:  # Septiembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9)
        elif mes == 10:  # Octubre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10)
        elif mes == 11:  # Noviembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10) + convert_cero_none(self.haber11)
        elif mes == 12:  # Diciembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10) + convert_cero_none(self.haber11) + convert_cero_none(self.haber12)
        else:  # Sin mes
            return 0.00
        return total

    def get_saldo_debe_mes(self, mes):
        """
        Función que me devuelve el saldo deudor por mes de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """

        if mes == 1:  # Enero
            return convert_cero_none(self.debe1)
        elif mes == 2:  # Febrero
            return convert_cero_none(self.debe2)
        elif mes == 3:  # Marzo
            return convert_cero_none(self.debe3)
        elif mes == 4:  # Abril
            return convert_cero_none(self.debe4)
        elif mes == 5:  # Mayo
            return convert_cero_none(self.debe5)
        elif mes == 6:  # Junio
            return convert_cero_none(self.debe6)
        elif mes == 7:  # Julio
            return convert_cero_none(self.debe7)
        elif mes == 8:  # Agosto
            return convert_cero_none(self.debe8)
        elif mes == 9:  # Septiembre
            return convert_cero_none(self.debe9)
        elif mes == 10:  # Octubre
            return convert_cero_none(self.debe10)
        elif mes == 11:  # Noviembre
            return convert_cero_none(self.debe11)
        elif mes == 12:  # Diciembre
            return convert_cero_none(self.debe12)
        else:  # Sin mes
            return 0.00

    def get_saldo_haber_mes(self, mes):
        """
        Función que me devuelve el saldo acumulado acreedor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        if mes == 1:  # Enero
            return convert_cero_none(self.haber1)
        elif mes == 2:  # Febrero
            return convert_cero_none(self.haber2)
        elif mes == 3:  # Marzo
            return convert_cero_none(self.haber3)
        elif mes == 4:  # Abril
            return convert_cero_none(self.haber4)
        elif mes == 5:  # Mayo
            return convert_cero_none(self.haber5)
        elif mes == 6:  # Junio
            return convert_cero_none(self.haber6)
        elif mes == 7:  # Julio
            return convert_cero_none(self.haber7)
        elif mes == 8:  # Agosto
            return convert_cero_none(self.haber8)
        elif mes == 9:  # Septiembre
            return convert_cero_none(self.haber9)
        elif mes == 10:  # Octubre
            return convert_cero_none(self.haber10)
        elif mes == 11:  # Noviembre
            return convert_cero_none(self.haber11)
        elif mes == 12:  # Diciembre
            return convert_cero_none(self.haber12)
        else:  # Sin mes
            return 0.00
    #Funciones para Saldos de Cuentas
    def get_saldo_acumulado_debe_plan_cuentas(self, mes):
        """
        Función que me devuelve el saldo acumulado deudor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        total = 0.00
        if mes == 1:  # Enero
            total += convert_cero_none(self.debe1)
        elif mes == 2:  # Febrero
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2)
        elif mes == 3:  # Marzo
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3)
        elif mes == 4:  # Abril
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4)
        elif mes == 5:  # Mayo
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5)
        elif mes == 6:  # Junio
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6)
        elif mes == 7:  # Julio
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7)
        elif mes == 8:  # Agosto
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8)
        elif mes == 9:  # Septiembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9)
        elif mes == 10:  # Octubre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10)
        elif mes == 11:  # Noviembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10) + convert_cero_none(self.debe11)
        elif mes == 12:  # Diciembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10) + convert_cero_none(self.debe11) + convert_cero_none(self.debe12)
        else:  # Sin mes
            return 0.00
        return total

    def get_saldo_acumulado_haber_plan_cuentas(self, mes):
        """
        Función que me devuelve el saldo acumulado acreedor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        total = 0.0
        if mes == 1:  # Enero
            total += convert_cero_none(self.haber1)
        elif mes == 2:  # Febrero
            total += convert_cero_none(self.haber1)+ convert_cero_none(self.haber2)
        elif mes == 3:  # Marzo
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2)+ convert_cero_none(self.haber3)
        elif mes == 4:  # Abril
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3)+ \
                     convert_cero_none(self.haber4)
        elif mes == 5:  # Mayo
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5)
        elif mes == 6:  # Junio
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6)
        elif mes == 7:  # Julio
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6)+ \
                     convert_cero_none(self.haber7)
        elif mes == 8:  # Agosto
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8)
        elif mes == 9:  # Septiembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9)
        elif mes == 10:  # Octubre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10)
        elif mes == 11:  # Noviembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10) + convert_cero_none(self.haber11)
        elif mes == 12:  # Diciembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10) + convert_cero_none(self.haber11) + convert_cero_none(self.haber12)
        else:  # Sin mes
            return 0.00
        return total


class SaldoCuentaCentroCosto(models.Model):
    id = models.AutoField(primary_key=True)
    anio = models.IntegerField()
    plan_cuenta = models.ForeignKey(PlanCuenta)
    centro_costo = models.ForeignKey(Centro_Costo)
    saldo_inicial = models.FloatField()
    debe1 = models.FloatField()
    haber1 = models.FloatField()
    debe2 = models.FloatField()
    haber2 = models.FloatField()
    debe3 = models.FloatField()
    haber3 = models.FloatField()
    debe4 = models.FloatField()
    haber4 = models.FloatField()
    debe5 = models.FloatField()
    haber5 = models.FloatField()
    debe6 = models.FloatField()
    haber6 = models.FloatField()
    debe7 = models.FloatField()
    haber7 = models.FloatField()
    debe8 = models.FloatField()
    haber8 = models.FloatField()
    debe9 = models.FloatField()
    haber9 = models.FloatField()
    debe10 = models.FloatField()
    haber10 = models.FloatField()
    debe11 = models.FloatField()
    haber11 = models.FloatField()
    debe12 = models.FloatField()
    haber12 = models.FloatField()

    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "saldo_cuenta_centro_costo"

    def get_saldo_debe_acum(self, mes):
        """
        Función que me devuelve el saldo acumulado deudor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        total = 0.0
        if mes == 1:  # Enero
            total += convert_cero_none(self.debe1)
        elif mes == 2:  # Febrero
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2)
        elif mes == 3:  # Marzo
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3)
        elif mes == 4:  # Abril
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4)
        elif mes == 5:  # Mayo
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5)
        elif mes == 6:  # Junio
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6)
        elif mes == 7:  # Julio
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7)
        elif mes == 8:  # Agosto
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8)
        elif mes == 9:  # Septiembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9)
        elif mes == 10:  # Octubre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10)
        elif mes == 11:  # Noviembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10) + convert_cero_none(self.debe11)
        elif mes == 12:  # Diciembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10) + convert_cero_none(self.debe11) + convert_cero_none(self.debe12)
        else:  # Sin mes
            return 0.00
        return total

    def get_saldo_haber_acum(self, mes):
        """
        Función que me devuelve el saldo acumulado acreedor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        total = 0.0
        if mes == 1:  # Enero
            total += convert_cero_none(self.haber1)
        elif mes == 2:  # Febrero
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2)
        elif mes == 3:  # Marzo
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + + convert_cero_none(self.haber3)
        elif mes == 4:  # Abril
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4)
        elif mes == 5:  # Mayo
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5)
        elif mes == 6:  # Junio
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6)
        elif mes == 7:  # Julio
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7)
        elif mes == 8:  # Agosto
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8)
        elif mes == 9:  # Septiembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9)
        elif mes == 10:  # Octubre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10)
        elif mes == 11:  # Noviembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10) + convert_cero_none(self.haber11)
        elif mes == 12:  # Diciembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10) + convert_cero_none(self.haber11) + convert_cero_none(self.haber12)
        else:  # Sin mes
            return 0.00
        return total

    def get_saldo_debe_mes(self, mes):
        """
        Función que me devuelve el saldo deudor por mes de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """

        if mes == 1:  # Enero
            return convert_cero_none(self.debe1)
        elif mes == 2:  # Febrero
            return convert_cero_none(self.debe2)
        elif mes == 3:  # Marzo
            return convert_cero_none(self.debe3)
        elif mes == 4:  # Abril
            return convert_cero_none(self.debe4)
        elif mes == 5:  # Mayo
            return convert_cero_none(self.debe5)
        elif mes == 6:  # Junio
            return convert_cero_none(self.debe6)
        elif mes == 7:  # Julio
            return convert_cero_none(self.debe7)
        elif mes == 8:  # Agosto
            return convert_cero_none(self.debe8)
        elif mes == 9:  # Septiembre
            return convert_cero_none(self.debe9)
        elif mes == 10:  # Octubre
            return convert_cero_none(self.debe10)
        elif mes == 11:  # Noviembre
            return convert_cero_none(self.debe11)
        elif mes == 12:  # Diciembre
            return convert_cero_none(self.debe12)
        else:  # Sin mes
            return 0.00

    def get_saldo_haber_mes(self, mes):
        """
        Función que me devuelve el saldo acumulado acreedor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        if mes == 1:  # Enero
            return convert_cero_none(self.haber1)
        elif mes == 2:  # Febrero
            return convert_cero_none(self.haber2)
        elif mes == 3:  # Marzo
            return convert_cero_none(self.haber3)
        elif mes == 4:  # Abril
            return convert_cero_none(self.haber4)
        elif mes == 5:  # Mayo
            return convert_cero_none(self.haber5)
        elif mes == 6:  # Junio
            return convert_cero_none(self.haber6)
        elif mes == 7:  # Julio
            return convert_cero_none(self.haber7)
        elif mes == 8:  # Agosto
            return convert_cero_none(self.haber8)
        elif mes == 9:  # Septiembre
            return convert_cero_none(self.haber9)
        elif mes == 10:  # Octubre
            return convert_cero_none(self.haber10)
        elif mes == 11:  # Noviembre
            return convert_cero_none(self.haber11)
        elif mes == 12:  # Diciembre
            return convert_cero_none(self.haber12)
        else:  # Sin mes
            return 0.00
    #Funciones para Saldos de Cuentas
    def get_saldo_acumulado_debe_plan_cuentas(self, mes):
        """
        Función que me devuelve el saldo acumulado deudor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        total = 0.00
        if mes == 1:  # Enero
            total += convert_cero_none(self.debe1)
        elif mes == 2:  # Febrero
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2)
        elif mes == 3:  # Marzo
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3)
        elif mes == 4:  # Abril
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4)
        elif mes == 5:  # Mayo
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5)
        elif mes == 6:  # Junio
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6)
        elif mes == 7:  # Julio
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7)
        elif mes == 8:  # Agosto
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8)
        elif mes == 9:  # Septiembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9)
        elif mes == 10:  # Octubre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10)
        elif mes == 11:  # Noviembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10) + convert_cero_none(self.debe11)
        elif mes == 12:  # Diciembre
            total += convert_cero_none(self.debe1) + convert_cero_none(self.debe2) + convert_cero_none(self.debe3) + \
                     convert_cero_none(self.debe4) + convert_cero_none(self.debe5) + convert_cero_none(self.debe6) + \
                     convert_cero_none(self.debe7) + convert_cero_none(self.debe8) + convert_cero_none(self.debe9) + \
                     convert_cero_none(self.debe10) + convert_cero_none(self.debe11) + convert_cero_none(self.debe12)
        else:  # Sin mes
            return 0.00
        return total

    def get_saldo_acumulado_haber_plan_cuentas(self, mes):
        """
        Función que me devuelve el saldo acumulado acreedor de una determinada cuenta
        dependiendo del mes
        :param mes:
        :return:
        """
        total = 0.0
        if mes == 1:  # Enero
            total += convert_cero_none(self.haber1)
        elif mes == 2:  # Febrero
            total += convert_cero_none(self.haber1)+ convert_cero_none(self.haber2)
        elif mes == 3:  # Marzo
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2)+ convert_cero_none(self.haber3)
        elif mes == 4:  # Abril
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3)+ \
                     convert_cero_none(self.haber4)
        elif mes == 5:  # Mayo
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5)
        elif mes == 6:  # Junio
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6)
        elif mes == 7:  # Julio
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6)+ \
                     convert_cero_none(self.haber7)
        elif mes == 8:  # Agosto
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8)
        elif mes == 9:  # Septiembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9)
        elif mes == 10:  # Octubre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10)
        elif mes == 11:  # Noviembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10) + convert_cero_none(self.haber11)
        elif mes == 12:  # Diciembre
            total += convert_cero_none(self.haber1) + convert_cero_none(self.haber2) + convert_cero_none(self.haber3) + \
                     convert_cero_none(self.haber4) + convert_cero_none(self.haber5) + convert_cero_none(self.haber6) + \
                     convert_cero_none(self.haber7) + convert_cero_none(self.haber8) + convert_cero_none(self.haber9) + \
                     convert_cero_none(self.haber10) + convert_cero_none(self.haber11) + convert_cero_none(self.haber12)
        else:  # Sin mes
            return 0.00
        return total


class Tmp_rpt_kardex_por_item(models.Model):
    codigo_item = models.CharField(max_length=50)
    nombre_item = models.CharField(max_length=200)
    fecha_registro = models.CharField(max_length=200)
    tipo_comprobante = models.CharField(max_length=200)
    num_comprobante = models.CharField(max_length=200)
    tipo_movimiento = models.CharField(max_length=200)
    num_documento = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=200)
    cantidad = models.FloatField()
    costo = models.FloatField()
    debito = models.FloatField()
    credito = models.FloatField()
    saldo = models.FloatField()

    class Meta:
        db_table = "tmp_rpt_kardex_por_item"

class Empleados(models.Model):
    id = models.AutoField(primary_key=True)
    nombres_completos = models.CharField(max_length=220)
    apellidos_completos = models.CharField(max_length=220)
    num_id = models.CharField(max_length=20)
    sueldo = models.FloatField()

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "empleados"
        verbose_name = "Empleado"
        verbose_name_plural = "Empleados"

class Sueldo_Empleado(models.Model):
    id = models.AutoField(primary_key=True)
    anio = models.IntegerField()
    mes = models.IntegerField()
    empleado = models.ForeignKey(Empleados)
    sueldo = models.FloatField()
    valor_distribuir = models.FloatField(default=0.0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "sueldo_empleado"

class CostoContratoCabecera(models.Model):
    id = models.AutoField(primary_key=True)
    tipo_comprobante = models.ForeignKey(TipoComprobante)
    num_comp = models.CharField(max_length=20)
    fecha_reg = models.DateField()
    proveedor = models.ForeignKey(Proveedores, null=True)
    empleado = models.ForeignKey(Empleados, null=True)
    compra_cabecera = models.ForeignKey(CompraCabecera, null=True)
    concepto = models.TextField(null=True)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "costo_contrato_cabecera"

class CostoContratoDetalle(models.Model):
    id = models.AutoField(primary_key=True)
    costo_contrato_cabecera = models.ForeignKey(CostoContratoCabecera)
    contrato_cabecera = models.ForeignKey(ContratoCabecera)
    horas = models.CharField(null=True, max_length=50)
    valor = models.FloatField(default=0.0)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField()
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "costo_contrato_detalle"


        ##############################
        #    Modelos para reportes   #
        ##############################

class BalanceGeneral(models.Model):
    id = models.AutoField(primary_key=True)
    pagina = models.FloatField()
    linea = models.FloatField()
    cod_cta = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255)
    tipo = models.CharField(max_length=255)
    operacion = models.CharField(max_length=255)
    columna = models.CharField(max_length=255)
    caracteristica = models.CharField(max_length=255)
    campo9 = models.CharField(max_length=255)
    campo10 = models.FloatField()
    plan_cuenta = models.ForeignKey(PlanCuenta)

    class Meta:
        db_table = "balance_general"


class PerdidasGanancias(models.Model):
    id = models.AutoField(primary_key=True)
    pagina = models.FloatField()
    linea = models.FloatField()
    cod_cta = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255)
    tipo = models.CharField(max_length=255)
    operacion = models.CharField(max_length=255)
    columna = models.CharField(max_length=255)
    caracteristica = models.CharField(max_length=255)
    campo9 = models.CharField(max_length=255)
    campo10 = models.FloatField()
    plan_cuenta = models.ForeignKey(PlanCuenta)

    class Meta:
        db_table = "perdidas_ganancias"


class ComparativoEstadoResultado(models.Model):
    id = models.AutoField(primary_key=True)
    pagina = models.FloatField()
    linea = models.FloatField()
    cod_cta = models.CharField(max_length=255)
    descripcion = models.CharField(max_length=255)
    tipo = models.CharField(max_length=255)
    operacion = models.CharField(max_length=255)
    columna = models.CharField(max_length=255)
    caracteristica = models.CharField(max_length=255)
    campo9 = models.CharField(max_length=255)
    campo10 = models.FloatField()
    plan_cuenta = models.ForeignKey(PlanCuenta)

    class Meta:
        db_table = "comparativo_estado_resultado"


class Tmp_Detalle_Reproceso(models.Model):
    id_detalle_inv = models.IntegerField(db_column="id_detalle")#estaba id
    inventario_cabecera = models.ForeignKey(Inventario_Cabecera)
    item = models.ForeignKey(Item)
    tipo_mov_inventario = models.ForeignKey(Tipo_Mov_Inventario)
    bodega = models.ForeignKey(Bodega)
    num_comp = models.CharField(max_length=255)
    fecha_reg = models.DateField()
    orden_comp = models.DateTimeField()

    # INVENTARIO - CANTIDAD INICIAL
    cantidad = models.FloatField()
    # INVENTARIO - COSTO INICIAL
    costo = models.FloatField()


    costo_reproceso = models.FloatField()
    cantidad_actual = models.FloatField()

    # Cantidad * Costo
    saldo_actual = models.FloatField()

    status = models.IntegerField(default=1)


    class Meta:
        db_table = "tmp_detalle_reproceso"
