#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.core.paginator import *
from contabilidad.models import *
from administracion.models import *
from contabilidad.formularios.BancoForm import *
from clipmaestro.templatetags.formulario_base import *
from sys import modules
from sys import path
import operator
import datetime
import time
from django.forms.util import ErrorList
from django.core.paginator import *
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from librerias.funciones.paginacion import *
from django.forms.formsets import formset_factory
from django.db import connection
from django.db import IntegrityError, transaction
from librerias.funciones.funciones_vistas import *
from contabilidad.funciones.bancos_transacciones_func import *
import json
from librerias.funciones.permisos import *

########################################################################################################################
#                                              Cuentas Bancarias                                                       #
########################################################################################################################
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def lista_bancos(request):
    '''
     Vista que lista todas las cuentas de banco registradas
     en el sistema
    :param request:
    :return:
    '''
    buscador = FormBuscadorCuentaBanco(request.GET)
    bancos = BusquedaCuentaBancaria(buscador)
    paginacion = Paginator(bancos, 10)
    numero_pagina = request.GET.get("page")

    try:
        bancos = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        bancos = paginacion.page(1)
    except EmptyPage:
        bancos = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = bancos.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('banco/lista_banco.html',
                              {"objetos": bancos, "buscador": buscador,
                               "total_paginas": total_paginas,
                               "numero": numero,
                               "arreglo_paginado": lista,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def detalle_banco(request, id, numero):
    tipo = 3
    secuenicia_cheque = ""
    try:
        banco = Cuenta_Banco.objects.get(id=id)

        if banco.tipo_cuenta_banco_id == 2:     # Corriente
            if banco.secuencia_cheque is not None:
                secuenicia_cheque = banco.secuencia_cheque
            else:
                secuenicia_cheque = ""

        if banco.fecha_ultimo_cierre is not None:
            fecha_ultimo_cierre = banco.fecha_ultimo_cierre.strftime("%Y-%m-%d")
        else:
            fecha_ultimo_cierre = ""

        if banco.fecha_ultimo_conciliacion is not None:
            fecha_ultimo_conciliacion = banco.fecha_ultimo_conciliacion.strftime("%Y-%m-%d")
        else:
            fecha_ultimo_conciliacion = ""

        formulario = FormBancoOnlyRead(initial={
            "descripcion": banco.descripcion,
            "numero_cuenta": banco.numero_cuenta,
            "tipo": banco.tipo_cuenta_banco.id,
            "secuencia_cheque": secuenicia_cheque,
            "fecha_conciliacion": fecha_ultimo_conciliacion,
            "fecha_ultimo_cierre": fecha_ultimo_cierre,
            "cuentas": banco.plan_cuenta_id
        })
    except:
        messages.error(request, u"Error al obtener la información de la cuenta bancaria")
        return HttpResponseRedirect(reverse("lista_bancos"))

    return render_to_response('banco/registrar_banco.html',
                              {"objeto": banco, "id": id,
                               "numero": numero,
                               "formulario": formulario,
                               "tipo": tipo,
                               "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
@transaction.commit_on_success
def registrar_banco(request):
    today = datetime.datetime.now()
    tipo = 1
    formulario = FormBanco()
    banco_obj = Cuenta_Banco()
    plan_cuenta = PlanCuenta()

    if not PlanCuenta.objects.filter(tipo_id=1, nivel=4).exists():   # Cuenta tipo Banco
        messages.error(request, u"Error: No está definida la cuenta bancos correctamente "
                                u", para continuar proceda a definir la cuenta de bancos y asocie el tipo de cuenta "
                                u"correspondiente")

        return HttpResponseRedirect(reverse("lista_bancos"))

    if request.method == "POST":
        formulario = FormBanco(request.POST)

        if formulario.is_valid():
            contador = GuardarCuentaBanco(formulario, banco_obj, plan_cuenta, today, request)

            if contador == 0.0:
                messages.success(request, u'La Cuenta Bancaria: "'+unicode(banco_obj.descripcion)+
                                      u' se registró exitosamente')
                return HttpResponseRedirect(reverse("lista_bancos"))
            else:
                transaction.rollback()
        else:
            lista_errores = "Por favor verifique los siguientes campos: "
            for i in formulario.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "
            if formulario.errors:
                messages.error(request, unicode(lista_errores[0:-2]))
            for i in formulario:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
            transaction.rollback()

    return render_to_response("banco/registrar_banco.html",
                              {"formulario": formulario,
                               "request": request,
                               "tipo": tipo}, context_instance=RequestContext(request))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
@csrf_exempt
def modificar_banco(request, id, numero):
    now_ac = datetime.datetime.now()
    cont = 0
    tipo = 2
    secuenicia_cheque = ""

    try:
        nuevo = Cuenta_Banco.objects.get(id=id)

        if nuevo.tipo_cuenta_banco_id == 2:     # Corriente
            if nuevo.secuencia_cheque is not None:
                secuenicia_cheque = nuevo.secuencia_cheque
            else:
                secuenicia_cheque = ""

        if nuevo.fecha_ultimo_cierre is not None:
            fecha_ultimo_cierre = nuevo.fecha_ultimo_cierre
        else:
            fecha_ultimo_cierre = ""

        if nuevo.fecha_ultimo_conciliacion is not None:
            fecha_ultimo_conciliacion = nuevo.fecha_ultimo_conciliacion
        else:
            fecha_ultimo_conciliacion = ""

        if Detalle_Comp_Contable.objects.filter(status=1).filter(plan_cuenta=nuevo.plan_cuenta).exists():
            messages.error(request, u"La Cuenta Bancaria no puede ser editada, dado que posee transacciones.")
            return HttpResponseRedirect(reverse("lista_bancos"))

        else:

            formulario = FormBanco(initial={
                "descripcion": nuevo.descripcion,
                "numero_cuenta": nuevo.numero_cuenta,
                "tipo": nuevo.tipo_cuenta_banco_id,
                "secuencia_cheque": secuenicia_cheque,
                "fecha_conciliacion": fecha_ultimo_conciliacion,
                "fecha_ultimo_cierre": fecha_ultimo_cierre,
                "cuentas": nuevo.plan_cuenta_id,
            })
    except:
        messages.error(request, u"Error, La Cuenta Bancaria no se encuentra registrado. ")
        return HttpResponseRedirect(reverse("lista_bancos"))

    #try:
    if request.method == "POST":
        formulario = FormBanco(request.POST)
        if formulario.is_valid():

            if formulario.getCuentas() is not None:
                nuevo.descripcion = formulario.getCuentas().descripcion
            else:
                nuevo.descripcion = formulario.getDescripcion()

            nuevo.numero_cuenta = formulario.getNumeroCuenta()
            nuevo.tipo_cuenta_banco = Tipo_Cuenta_Banco.objects.get(id=formulario.getTipo())

            if formulario.getFechaUltimoCierre() != "":
                nuevo.fecha_ultimo_cierre = formulario.getFechaUltimoCierre()
            if formulario.getFechaUltimoConciliacion() != "":
                nuevo.fecha_ultimo_conciliacion = formulario.getFechaUltimoConciliacion()

            nuevo.fecha_actualizacion = now_ac
            nuevo.usuario_actualizacion = request.user.username

            if nuevo.tipo_cuenta_banco.id == 2:  # Corriente
                nuevo.secuencia_cheque = formulario.getSecuenciaCheque()

                if Cuenta_Banco.objects.exclude(status=0).filter(~Q(id=nuevo.id), Q(secuencia_cheque=nuevo.secuencia_cheque), Q(numero_cuenta=nuevo.numero_cuenta), Q(descripcion=nuevo.descripcion)).exists():
                    messages.error(request, u"Número de Secuencia ya ingresado, por favor verifique.")
                    cont += 1

            else:                                # Ahorro

                nuevo.secuencia_cheque = ""

                if Cuenta_Banco.objects.exclude(status=0).filter(~Q(id=nuevo.id), Q(descripcion=nuevo.descripcion), Q(numero_cuenta=nuevo.numero_cuenta)).exists():
                    messages.error(request, u"Número de Cuenta ya ingresado, por favor verifique.")
                    cont += 1

            if cont == 0:
                nuevo.fecha_actualizacion = now_ac
                nuevo.usuario_creacion = request.user.username
                nuevo.save()
                messages.success(request, u"La información de la Cuenta Bancaria ha sido  modificada exitosamente")
                return HttpResponseRedirect(reverse("lista_bancos"))
            else:
                transaction.rollback()

        else:
             lista_errores = "Por favor verifique los siguientes campos: "
             for i in formulario.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "
             if formulario.errors:
                messages.error(request, unicode(lista_errores[0:-2]))
             for i in formulario:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
    #except:
    #    messages.error(request, u"Existieron errores al editar la cuenta bancaria")
    #    transaction.rollback()

    return render_to_response("banco/registrar_banco.html",
                              {"formulario": formulario,
                               "obj": nuevo,
                               "numero": numero,
                               "id": id,
                               "tipo": tipo,
                               "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
@csrf_exempt
def eliminar_banco(request, id):
    now_ac = datetime.datetime.now()
    try:
        banco = Cuenta_Banco.objects.get(id=id)

        if Detalle_Comp_Contable.objects.filter(status=1).filter(plan_cuenta=banco.plan_cuenta).exists():
            messages.error(request, u"La Cuenta Bancaria no puede ser eliminada, dado que posee transacciones...")
            return HttpResponseRedirect(reverse("lista_bancos"))

        else:

            if banco.status != 0:
                banco.status = 0
                banco.usuario_actualizacion = request.user.username
                banco.fecha_actualizacion = now_ac
                banco.save()

                cuenta = PlanCuenta.objects.filter(status=1).get(id=banco.plan_cuenta.id)
                cuenta.status = 0
                cuenta.save()

                messages.success(request, u"La Cuenta Bancaria ha sido eliminado exitosamente")

            return HttpResponseRedirect(reverse("lista_bancos"))
    except:
        messages.error(request, u"Error, la Cuenta no se encuentra registrada")
        return HttpResponseRedirect(reverse("lista_bancos"))


########################################################################################################################
#                                          Transacciones Bancarias                                                     #
########################################################################################################################
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def lista_transaccion_banco(request):
    buscador = FormBuscadorTransaccionBanco(request.GET)
    bancos = BusquedaTransaccionesBancarias(buscador)
    form_anula_cheque = FormAnularChequeBanco()
    paginacion = Paginator(bancos, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response("banco/lista_transaccion.html",
                              {"objetos": lista_cabecera, "total_paginas": total_paginas,
                               "arreglo_paginado": lista, "numero": numero,
                               "form_anula_cheque": form_anula_cheque,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               "buscador": buscador}, context_instance=RequestContext(request))

@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def detalle_transaccion_banco(request, id):
    try:
        banco = Banco.objects.get(id=id)
        cabecera_comp = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=banco.num_comp)
        lista_detalle = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera_comp).order_by("dbcr", "plan_cuenta__codigo")

        return render_to_response("banco/detalle_transaccion_banco.html", {"banco": banco,
                                                                           "mayor_cab": cabecera_comp,
                                                                           "lista_detalle": lista_detalle}, context_instance=RequestContext(request))
    except Banco.DoesNotExist:
        raise Http404

@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def detalle_transaccion_banco_reportepdf(request, id):
    try:
        banco = Banco.objects.get(id=id)
        cabecera_comp = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=banco.num_comp)
        lista_detalle = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera_comp).order_by("dbcr", "plan_cuenta__codigo")
        empresa_id = Empresa.objects.all()[0].empresa_general_id
        empresa = Empresa_Parametro.objects.using("base_central").get(id=empresa_id)
        usuario = request.user.first_name+' '+request.user.last_name
        html = render_to_string("banco/detalle_transaccion_reporte_pdf.html",
                                {'pagesize': 'A4', "banco": banco, "empresa": empresa,
                                 "usuario": usuario, "mayor_cab": cabecera_comp, "lista_detalle": lista_detalle,
                                 "fecha_actual": datetime.datetime.now()}, context_instance=RequestContext(request))

        return generar_pdf_get(html)

    except Banco.DoesNotExist:
       raise Http404

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
@transaction.commit_on_success
def transacciones_bancarias(request):
    '''
    Vista que realiza el procedimiento de generar las transacciones bancarias
    :param request:
    :return:
    '''
    try:  # Inicializa la última fecha de registro
        secuencia_tipo_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=3)
        fecha_reg = secuencia_tipo_comp.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        fecha_reg = ""

    cabecera = CabeceraTransaccionForm(initial={"fecha": fecha_reg})

    detalle_trans_formset = formset_factory(FormDetalleTransaccion, extra=2, formset=BaseDetalleTransFormSet)
    detalle_trans = detalle_trans_formset(prefix="detalle_transacion")
    now = datetime.datetime.now()
    contador = 0
    flag_banco_t = False
    tipo = 1
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    if request.method == "POST":
        try:
            cabecera = CabeceraTransaccionForm(request.POST)
            detalle_trans = detalle_trans_formset(request.POST, request.FILES, prefix="detalle_transacion")
            flag_banco_t = True

            if cabecera.is_valid():
                #validar fecha de cierre
                if esta_periodo_contable(cabecera.getFecha()):

                    if validar_cuadre_transaccion(detalle_trans):

                        # Se inicializa la clase banco si es provisionado solo se hace un update
                        if cabecera.getTipoMov() == "2":  # Egreso
                            if cabecera.es_cheque_provisionado():  # Cheque provisionado
                                # Se hace un update del registro
                                banco = Banco.objects.get(id=cabecera.get_cheque_provisionado(), status=3)
                            else:
                                banco = Banco()
                        else:
                            banco = Banco()

                        comprobante_cab = Cabecera_Comp_Contable()
                        total, naturaleza = get_valor_banco(detalle_trans)

                        contador += guardar_cabecera_transaccion(banco, cabecera, total, comprobante_cab, naturaleza,
                                                                 contador, now, request)

                        contador += guardar_detalle_transaccion(detalle_trans, comprobante_cab, banco, cabecera, contador,
                                                                now, request)

                        if contador == 0:
                            if cabecera.getTipoMov() == "2":
                                mensaje = u"Se ha realizado la transacción bancaria con # Comprobante: " \
                                          u"" + banco.num_comp + u". Y número de cheque: " + str(banco.num_cheque)
                            else:
                                mensaje = u"Se ha realizado la transacción bancaria con # Comprobante: " + banco.num_comp

                            messages.success(request, mensaje)

                            # Redireccionando la transaccion banco
                            return HttpResponseRedirect(redireccionar_transaccion_bancaria(request, banco))


                        else:
                            transaction.rollback()
                    else:
                        messages.error(request, u"No ha ingresado toda la información requerida o "
                                                u"los valores ingresados no cuadran, por favor verifique")
                        transaction.rollback()
                else:
                    errors = cabecera._errors.setdefault("fecha", ErrorList())
                    errors.append(u"Campo Requerido")
                    parametro_empresa = get_parametros_empresa()
                    messages.error(request, u'Por favor verifique el campo Fecha, debe ser mayor a la fecha de cierre: '+str(get_mes(parametro_empresa.mes_cierre))+" " +str(parametro_empresa.anio_cierre))
        except:
            transaction.rollback()
            messages.error(request, u"Error interno del servidor, por favor comuníquese "
                                    u"con el administrador de sistemas")

    return render_to_response("banco/transaccion_banco.html",
                            {"formulario": cabecera,
                             "formulario_detalle": detalle_trans,
                             "dia": now.strftime('%Y-%m-%d'),
                             "tipo": tipo,
                             "empresa_param": empresa,
                             "flag_banco": flag_banco_t}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def anular_transaccion_bancaria(request, id):
    try:
        contador = anular_transaccion(id, request)
        if contador != 0:
            transaction.rollback()
        return HttpResponseRedirect(reverse("lista_transaccion_banco"))
    except:
        transaction.rollback()
        messages.error(request, u"Error al anular la transacción bancaria")
        return HttpResponseRedirect(reverse("lista_transaccion_banco"))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def copiar_transaccion_bancaria(request, id):
    tipo = 2            # Copiar
    now = datetime.datetime.now()
    contador = 0
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa_param = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)
    flag_banco_t = False
    try:
        try:
            banco = Banco.objects.get(id=id)
            cabecera_comp = Cabecera_Comp_Contable.objects.get(numero_comprobante=banco.num_comp)
            cabecera = CabeceraTransaccionForm(initial=instancia_cabecera_transaccion_banco(banco))
            detalle_trans_formset = formset_factory(FormDetalleTransaccion, extra=0, formset=BaseDetalleTransFormSet)
            detalle_trans = detalle_trans_formset(initial=instancia_detalle_transaccion_banco(cabecera_comp), prefix="detalle_transacion")
        except:
            messages.error(request, u"Exisitó un error al obtener la información de la transacción bancaria")
            return HttpResponseRedirect(reverse("lista_transaccion_banco"))

        if request.method == "POST":
            cabecera = CabeceraTransaccionForm(request.POST)
            detalle_trans = detalle_trans_formset(request.POST, request.FILES, prefix="detalle_transacion")
            flag_banco_t = True

            if cabecera.is_valid():
                if validar_cuadre_transaccion(detalle_trans):
                    banco = Banco()
                    comprobante_cab = Cabecera_Comp_Contable()
                    total, naturaleza = get_valor_banco(detalle_trans)

                    if cabecera.getFecha() > datetime.datetime.now().date():
                        errors = cabecera._errors.setdefault("fecha", ErrorList())
                        errors.append(u"La fecha del registro no puede ser mayor a la fecha actual")
                        messages.error(request, u"La fecha del registro no puede ser mayor a la fecha actual")
                        contador += 1
                    else:
                        contador += guardar_cabecera_transaccion(banco, cabecera, total, comprobante_cab, naturaleza,
                                                                 contador, now, request)
                        contador += guardar_detalle_transaccion(detalle_trans, comprobante_cab, banco, cabecera,
                                                                contador, now, request)
                    if contador == 0:
                        messages.success(request, u"La transacción se ha hecho satisfactoriamente")
                        # Redireccionando la transaccion banco
                        return HttpResponseRedirect(redireccionar_transaccion_bancaria(request, banco))

                    else:
                        transaction.rollback()
                else:
                    messages.error(request, u"Los valores ingresados no cuadran, por favor verifique")
                    transaction.rollback()
            else:
                messages.error(request, u"Por favor verifique que se encuentran ingresados correctamente los datos de "
                                                u"la transacción bancaria")
                transaction.rollback()

    except Banco.DoesNotExist:
        raise Http404

    return render_to_response("banco/transaccion_banco.html", {"formulario": cabecera, "tipo": tipo, "id": id,
                                                               "flag_banco": flag_banco_t,
                                                                "empresa_param": empresa_param,
                                                                "formulario_detalle": detalle_trans},
                                                                 context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def anular_generar_transaccion_bancaria(request, id):
    tipo = 3
    now = datetime.datetime.now()
    contador = 0
    flag_banco_t = False
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa_param = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)
    try:
        try:
            banco = Banco.objects.get(id=id)
            cabecera_comp = Cabecera_Comp_Contable.objects.get(numero_comprobante=banco.num_comp)
            cabecera = CabeceraTransaccionForm(initial=instancia_cabecera_transaccion_banco(banco))
            detalle_trans_formset = formset_factory(FormDetalleTransaccion, extra=0, formset=BaseDetalleTransFormSet)
            detalle_trans = detalle_trans_formset(initial=instancia_detalle_transaccion_banco(cabecera_comp), prefix="detalle_transacion")
        except:
            messages.error(request, u"Exisitó un error al obtener la información de la transacción bancaria")
            return HttpResponseRedirect(reverse("lista_transaccion_banco"))

        if request.method == "POST":
            cabecera = CabeceraTransaccionForm(request.POST)
            detalle_trans = detalle_trans_formset(request.POST, request.FILES, prefix="detalle_transacion")
            flag_banco_t = True

            if cabecera.is_valid():
                if validar_cuadre_transaccion(detalle_trans):
                    banco_nuevo = Banco()
                    comprobante_cab = Cabecera_Comp_Contable()
                    total, naturaleza = get_valor_banco(detalle_trans)

                    if cabecera.getFecha() > datetime.datetime.now().date():
                        errors = cabecera._errors.setdefault("fecha", ErrorList())
                        errors.append(u"La fecha del registro no puede ser mayor a la fecha actual")
                        messages.error(request, u"La fecha del registro no puede ser mayor a la fecha actual")
                        contador += 1
                    else:
                        contador += guardar_cabecera_transaccion(banco_nuevo, cabecera, total, comprobante_cab,
                                                                 naturaleza, contador, now, request)
                        contador += guardar_detalle_transaccion(detalle_trans, comprobante_cab, banco_nuevo, cabecera,
                                                                contador, now, request)

                    if contador == 0:
                        # Anula el Comprobante
                        contador += anular_transaccion(banco.id, request)
                        if contador > 0:
                            transaction.rollback()
                        else:
                            messages.success(request, u"Se ha anulado exitosamente la transacción bancaria de N°"
                                  u"de comprobante: "+str(banco.num_comp)+u" y generado una nueva"
                                  u" transacción con N° de comprobante: "+str(banco_nuevo.num_comp))
                            # Redireccionando la transaccion banco
                            return HttpResponseRedirect(redireccionar_transaccion_bancaria(request, banco))
                    else:
                        transaction.rollback()
                else:
                    messages.error(request, u"Los valores ingresados no cuadran, por favor verifique")
                    transaction.rollback()
            else:
                messages.error(request, u"Por favor verifique que se encuentran ingresados correctamente los datos de "
                                                u"la transacción bancaria")
                transaction.rollback()

        return render_to_response("banco/transaccion_banco.html", {"formulario": cabecera,
                                                                   "tipo": tipo, "empresa_param": empresa_param,
                                                                   "flag_banco": flag_banco_t,
                                                                   "id": id, "formulario_detalle": detalle_trans},
                                                                    context_instance=RequestContext(request))
    except Banco.DoesNotExist:
        transaction.rollback()
        raise Http404


@login_required(login_url='/')
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def cambiar_estado_cuenta_banco(request, id):
    try:
        banco = Cuenta_Banco.objects.get(id=id)
        now_ac = datetime.datetime.now()
        if banco.status == 1:
            banco.status = 2
            banco.usuario_actualizacion = request.user.username
            banco.fecha_actualizacion = now_ac
            messages.success(request, u'Se inactivó la cuenta bancaria: "'+unicode(banco.descripcion) + u'" exitosamente')

        elif banco.status == 2:
            banco.status = 1
            banco.usuario_actualizacion = request.user.username
            banco.fecha_actualizacion = now_ac
            messages.success(request, u'Se activó la cuenta bancaria: "'+unicode(banco.descripcion) + u'" exitosamente')

        banco.save()
    except:
        messages.error(request, u"Existió un problema en el servidor")
    return HttpResponseRedirect(reverse("lista_bancos"))

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def anular_cheque(request):
    '''
    Vista Ajax que anula directamente un cheque sin que este se encuentre registrado en el sistema
    :param request:
    :return:
    '''
    now = datetime.datetime.now()
    form_anular = FormAnularChequeBanco(request.POST)
    tipo_comp = TipoComprobante.objects.get(status=1, id=4)
    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
        fecha_actual = secuencia_tipo.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")

    try:

        if form_anular.is_valid():
            if form_anular.getFechaReg() > datetime.datetime.now().date():
                respuesta = {"status": 3, "mensaje": u"La fecha del registro no puede ser mayor a la fecha actual"}
                resultado = json.dumps(respuesta)
                transaction.rollback()
            elif form_anular.getFechaReg() < fecha_actual:
                respuesta = {"status": 4, "mensaje": u"La fecha del registro no puede ser menor a la "
                                                     u"fecha del comprobante: "+str(fecha_actual)}
                resultado = json.dumps(respuesta)
                transaction.rollback()
            else:
                id_cta_banco = form_anular.getCuentaBanco()
                cta_banco = Cuenta_Banco.objects.get(id=id_cta_banco)
                num_cheque = get_next_cheque(cta_banco, request)
                comp_contable = Cabecera_Comp_Contable()
                comp_contable.fecha = form_anular.getFechaReg()
                comp_contable.tipo_comprobante = tipo_comp
                comp_contable.numero_comprobante = get_num_comp(comp_contable.tipo_comprobante_id,
                                                                form_anular.getFechaReg(), True, request)

                comp_contable.concepto_comprobante = u"Anulación de Cheque"
                comp_contable.fecha_creacion = now
                comp_contable.usuario_creacion = request.user.username
                comp_contable.status = 2
                comp_contable.save()

                banco = Banco()
                banco.cuenta_banco = cta_banco
                banco.fecha_reg = form_anular.getFechaReg()
                banco.fecha_cheque = banco.fecha_reg
                banco.concepto = u"Anulación del Cheque de cuenta: " + str(cta_banco.plan_cuenta.codigo)+" - "+\
                                 unicode(cta_banco.plan_cuenta.descripcion)+" - " + str(num_cheque)

                banco.tipo_comprobante = TipoComprobante.objects.get(id=4)  # EGRESO
                banco.num_comp = comp_contable.numero_comprobante
                banco.num_cheque = num_cheque
                banco.persona = u"Anulado"
                banco.naturaleza = 2
                # Id de Cheque
                banco.forma_pago = FormaPago.objects.filter(status=1, banco_modulo=True).get(id=1)
                banco.fecha_creacion = now
                banco.usuario_creacion = request.user.username
                banco.status = 2
                banco.save()
                respuesta = {"status": 1, "num_cheque": num_cheque, "num_comp": comp_contable.numero_comprobante}
                resultado = json.dumps(respuesta)
                messages.success(request, u"Se anuló satisfactoriamente el cheque de la cuenta: " +
                                          str(cta_banco.plan_cuenta.codigo)+" - "+
                                 unicode(cta_banco.plan_cuenta.descripcion)+u" con # de Cheque: " + str(num_cheque))

        else:
            respuesta = {"status": 3, "mensaje": u"Existió un error al realizar la transacción "
                                                 u" verifique al llenar el formulario"}
            resultado = json.dumps(respuesta)

    except:
        respuesta = {"status": 2, "mensaje": u"Existió un error en el servidor, "
                                             u"por favor comuniquese con el administrador"}
        resultado = json.dumps(respuesta)
        transaction.rollback()

    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def conciliacion_bancaria(request):
    '''
     Vista que realiza la conciliación bancaria
    :param request:
    :return:
    '''
    formulario_cabecera = FormConciliacionBanco()
    now = datetime.datetime.now()
    dia_actual = now.strftime("%Y-%m-%d")
    flag = False        # Variable que me dice si los datos ya han sido cargados por POST
    detalle_form_set = formset_factory(FormTransaccionesConciliar, extra=1)
    detalles_transacciones = detalle_form_set()

    if request.method == "POST":
        formulario_cabecera = FormConciliacionBanco(request.POST)
        detalles_transacciones = detalle_form_set(request.POST, request.FILES)
        flag = True

        if formulario_cabecera.is_valid():
            if detalles_transacciones.is_valid():
                for form in detalles_transacciones:
                    informacion = form.cleaned_data
                    banco = Banco.objects.filter(status=1).get(id=informacion.get("id_banco"))

                    if informacion.get("check"):
                        banco.conciliado = True
                        banco.fecha_conciliacion = formulario_cabecera.getFechaCierre()
                        banco.fecha_actualizacion = now
                        banco.usuario_actualizacion = request.user.username
                        banco.save()

                    else:
                        banco.conciliado = False
                        banco.fecha_actualizacion = now
                        banco.usuario_actualizacion = request.user.username
                        banco.save()

            messages.success(request, u"La Conciliación Bancaria ha sido realizada con éxito")
            return HttpResponseRedirect(reverse("lista_transaccion_banco"))

    return render_to_response('banco/conciliacion_bancaria.html', {"formulario": formulario_cabecera,
                                                                    "forms": detalles_transacciones,
                                                                    "flag": flag,
                                                                    "dia_actual": dia_actual},
                                                                    context_instance=RequestContext(request))
