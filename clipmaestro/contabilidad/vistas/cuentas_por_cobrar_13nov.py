#! /usr/bin/python
# -*- coding: UTF-8-*-

__author__ = 'Clip Maestro'

from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.core.paginator import *
from librerias.funciones.permisos import *
from librerias.funciones.paginacion import *
from contabilidad.funciones.cuentas_por_cobrar_func import *

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def lista_cobro(request):
    """
    Funcion para ver la lista de cobros
    :param request:
    :return:
    """
    buscador = BuscadorCobros(request.GET)
    cobros = busqueda_cobros(buscador)
    paginacion = Paginator(cobros, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    reversion_cobro_form = ReversionCuentasCobrarForm()

    try:
        cobros = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        cobros = paginacion.page(1)
    except EmptyPage:
        cobros = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = cobros.number
    arreglo_num_paginado = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Cuentas_Cobrar/Clientes/lista_cobros.html',
                              {"objetos": cobros, "buscador": buscador,
                               "total_paginas": total_paginas, "numero": numero,
                               "arreglo_paginado": arreglo_num_paginado,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               'reversion_cobro_form': reversion_cobro_form
                               }, context_instance=RequestContext(request))

@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def detalle_cobro(request, id):
    """
    Función para mostar el detalle del cobro
    :param request:
    :param id:
    :return:
    """
    try:
        cobro = Cobro.objects.get(id=id)

        cliente = cobro.cliente
        cxp_cobro = CXC_cobro.objects.filter(cobro=cobro)
        plan_cuenta = cxp_cobro[0].plan_cuenta
        cabecera_asiento = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=cobro.num_comp)
        detalle_mayor = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera_asiento).order_by("dbcr")
        number_dec = str(cobro.monto).split(".")[1]
        return render_to_response('Cuentas_Cobrar/Clientes/comprobante_detalle_pago_del_cliente.html',
                                  {"cobro": cobro,
                                   "mayor_cab": cabecera_asiento,
                                   "detalle_may": detalle_mayor,
                                   "detalle_pago": cxp_cobro,
                                   "cuenta": plan_cuenta,
                                   "decimal": number_dec,
                                   "cliente": cliente}, context_instance=RequestContext(request))
    except Cobro.DoesNotExist:
        raise Http404
    except CXC_cobro.DoesNotExist:
        raise Http404
    except Cabecera_Comp_Contable.DoesNotExist:
        raise Http404



@csrf_exempt
@login_required(login_url='/')
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def cobro_clientes(request):
    """
    Vista para los cobros a los clientes
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    asiento = Cabecera_Comp_Contable()
    cursor = connection.cursor()
    contador = 0

    formset_detalle_docs = formset_factory(DetalleCobro, extra=0)
    detalle_docs = formset_detalle_docs()
    forma_de_pago = FormaCobroForm()
    fecha_actual = now.strftime("%Y-%m-%d")

    cabecera = CabeceraCobrosForm(initial={"fecha": fecha_actual, "observaciones": ""})
    flag = True

    if request.method == "POST":
        flag = False
        detalle_docs = formset_detalle_docs(request.POST)
        cabecera = CabeceraCobrosForm(request.POST)
        forma_de_pago = FormaCobroForm(request.POST)
        cobro = Cobro()

        try:
            if cabecera.is_valid():
                #################################################
                #  Se valida la fecha de cierre de la  empresa  #
                #################################################
                if not esta_periodo_contable(cabecera.getFecha()):
                    parametro_empresa = get_parametros_empresa()

                    messages.error(request, u"La fecha del asiento no puede ser menor a la del "
                                            u"cierre contable: "+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))
                    errors = cabecera._errors.setdefault("fecha", ErrorList())
                    errors.append(u"La fecha del asiento no puede ser menor a la del "
                                  u"cierre contable")
                    return render_to_response('Cuentas_Cobrar/Clientes/cobro_clientes.html',
                              {"cabecera":cabecera,
                               'forms': detalle_docs,
                               'flag': flag,
                               "formularios": forma_de_pago}, context_instance=RequestContext(request))

                if detalle_docs.is_valid():

                    total_cobrar, total_anticipo = CalcularTotalesCobro(detalle_docs)
                    if total_cobrar <= 0 and total_anticipo <= 0:
                        contador += 1
                        messages.error(request, u"Se debe ingresar un valor mayor a cero para realizar la transacción")
                    else:
                        #Guardar Cobro
                        contador += GuardarCobro(cobro, cabecera, forma_de_pago, detalle_docs, total_cobrar, total_anticipo, asiento, cursor, now, contador, request)
                else:
                    contador += 1
                    messages.error(request, u"Existen algunos errores en el detalle de los documentos a cobrar")

                if contador == 0:
                    messages.success(request, u"Se ha realizado con éxito la transacción")
                    return HttpResponseRedirect(reverse("lista_cobro"))
                else:
                    transaction.rollback()
            else:
                messages.error(request, u"Por favor llene los datos obligatorios para el cobro")
                transaction.rollback()
        except:
            transaction.rollback()
            messages.error(request, u"Existió un error al generar la transacción, por favor intente "
                                    u"más tarde o comuníquese con el administrador del sistema")
            return HttpResponseRedirect(reverse("lista_cobro"))

    return render_to_response('Cuentas_Cobrar/Clientes/cobro_clientes.html',
                              {"cabecera": cabecera,
                               'forms': detalle_docs,
                               'flag': flag,
                               "formularios": forma_de_pago}, context_instance=RequestContext(request))


def get_total_cobro(detalle_doc_cobrar):
    """
    Obtiene el total del pago
    :param detalle_doc_cobrar:
    :return: total_pago
    """
    total_pago = 0.0

    if detalle_doc_cobrar.is_valid():
        for form in detalle_doc_cobrar:
            informacion = form.cleaned_data
            check = informacion.get("check")
            if check:
                total_pago += float(informacion.get("valor"))

    return total_pago


def validacion_fechas_documentos(detalle_doc_cobrar, cabecera, contador, detalle_docs, request):
    """
    Valida que las fechas de los documentos que se vayan a cruzar sean menor o iguales a la fecha de
    registro del cobro
    :param detalle_doc_cobrar:
    :param cabecera:
    :param contador:
    :param detalle_docs:
    :param request:
    :return:
    """
    if detalle_doc_cobrar.is_valid():
        for form in detalle_doc_cobrar:
            informacion = form.cleaned_data
            check = informacion.get("check")
            if check:
                try:
                    cuenta_cobrar = Cuentas_por_Cobrar.objects.get(id=informacion.get("id_cuenta_pagar"))
                    if cuenta_cobrar.fecha_reg > cabecera.getFecha():
                        contador += 1
                        messages.error(request, u"La fecha del documento a cruzar es mayor a la "
                                                u"fecha de registro del cobro, por favor verifique")
                        errors1 = form._errors.setdefault("fecha_reg", ErrorList())
                        errors1.append(u"Error")

                        errors2 = cabecera._errors.setdefault("fecha", ErrorList())
                        errors2.append(u"Error")
                except (Cuentas_por_Cobrar.DoesNotExist, ValueError):
                    contador += 1

    if detalle_docs.is_valid():
        for form in detalle_docs:
            informacion = form.cleaned_data
            valor = informacion.get("valor")

            if valor > 0:
                try:
                    cuenta_cobrar = Cuentas_por_Cobrar.objects.get(id=informacion.get("id_cuenta_cobrar"))
                    if cuenta_cobrar.fecha_reg > cabecera.getFecha():
                        contador += 1
                        messages.error(request, u"La fecha del documento a cruzar es mayor a la "
                                                u"fecha de registro del cobro, por favor verifique")
                        errors1 = form._errors.setdefault("fecha_reg", ErrorList())
                        errors1.append(u"Error")

                        errors2 = cabecera._errors.setdefault("fecha", ErrorList())
                        errors2.append(u"Error")

                except (Cuentas_por_Cobrar.DoesNotExist, ValueError):
                    contador += 1
    return contador


@csrf_exempt
@transaction.commit_on_success
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def cruce_documentos_cobros(request):
    """
    Vista para hacer el cruce de documentos
    con las facturas que estan por cobrar
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    formset_detalle_docs = formset_factory(DetalleCobro, extra=0)
    formset_detalle_docs_cobrar = formset_factory(DetalleCobroDocCruzarForm, extra=0)
    formset_det_cruce_cta = formset_factory(DetalleCobroCruceCta, extra=0)
    contador = 0
    cabecera = CabeceraCurceDocFormCobro(initial={"fecha": now.strftime("%Y-%m-%d"),
                                                 "observeciones": "", "forma_cobro_cabecera": 1})

    detalle_docs = formset_detalle_docs(prefix="form_detalle_cta_cobrar")
    detalle_doc_cobrar = formset_detalle_docs_cobrar(prefix="form_detalle_cruce_doc")
    detalle_cta_cruzar = formset_det_cruce_cta(prefix="form_detalle_cruce_cuenta_cobro")
    flag = True

    if request.method == "POST":
        flag = False
        cabecera = CabeceraCurceDocFormCobro(request.POST)
        cobro = Cobro()
        asiento = Cabecera_Comp_Contable()
        try:
            ##########################################################################################################
            # Ocurre una excepción cuando no se envian por post los formularios                                      #
            # En el caso de Cruce de Documentos:                                                                     #
            #     detalle_cta_cruzar = formset_det_cruce_cta(request.POST, prefix="form_detalle_cruce_cuenta_cobro") #
            # En el case de Cruce de Cuentas:                                                                        #
            #     detalle_docs = formset_detalle_docs(request.POST, prefix="form_detalle_cta_cobrar")                #
            #     detalle_doc_cobrar = formset_detalle_docs_cobrar(request.POST, prefix="form_detalle_cruce_doc")    #
            # Esto ocurre por la llamada de esos formularios son netamente por ajax, se lo resuelve con un try-except#
            ##########################################################################################################
            try:
                detalle_cta_cruzar = formset_det_cruce_cta(request.POST, prefix="form_detalle_cruce_cuenta_cobro")
            except:
                pass
            try:
                detalle_docs = formset_detalle_docs(request.POST, prefix="form_detalle_cta_cobrar")
                detalle_doc_cobrar = formset_detalle_docs_cobrar(request.POST, prefix="form_detalle_cruce_doc")
            except:
                pass

            if cabecera.is_valid():
                cliente = cabecera.getCliente()
                # Para obtener el total de los pagos
                total_cobrar = get_total_cobro(detalle_doc_cobrar)

                if not esta_periodo_contable(cabecera.getFecha()):
                    parametro_empresa = get_parametros_empresa()
                    messages.error(request, u"La fecha del asiento no puede ser menor a la del "
                                            u"cierre contable "+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))
                    errors = cabecera._errors.setdefault("fecha", ErrorList())
                    errors.append(u"La fecha del asiento no puede ser menor a la del "
                                  u"cierre contable")

                    return render_to_response('Cuentas_Cobrar/Clientes/cruce_documentos.html',
                              {"cabecera": cabecera,
                               "detalle_docs": detalle_docs,
                               "detalle_doc_cobrar": detalle_doc_cobrar,
                               "detalle_cta_cruzar": detalle_cta_cruzar,
                               'flag': flag
                               }, context_instance=RequestContext(request))

                if cabecera.getFormaPago() == "1":  # Cruce de Documentos
                    num_comp = get_num_comp(17, cabecera.getFecha(), True, request)
                    if num_comp == -1:
                        contador += 1
                        messages.error(request, u"La fecha ingresada es menor a la del último cruce de "
                                                u"documentos, la fecha del último cruce es : " + get_fecha_tipo_comp(17))
                    elif total_cobrar > 0:
                        contador += validacion_fechas_documentos(detalle_doc_cobrar, cabecera, contador, detalle_docs, request)
                        GuardarCobroCruceDocs(cabecera, cobro, cliente, num_comp, total_cobrar, asiento, now, request)

                        if detalle_docs.is_valid():
                            for form in detalle_docs:
                                informacion = form.cleaned_data
                                valor = float(informacion.get("valor"))
                                if valor > 0:
                                    cuenta_cobrar = Cuentas_por_Cobrar.objects.get(id=informacion.get("id_cuenta_cobrar"))

                                    # Guardar Cuenta por Cobrar
                                    contador += GuardarCuentasCobrar(cuenta_cobrar, cobro, total_cobrar, now, contador, request)

                                    if detalle_doc_cobrar.is_valid():
                                        # Guarda los detalles de los documentos que han sido cruzados
                                        contador += GuardarDetalleDocCruzados(detalle_doc_cobrar, cobro, cliente, asiento, contador, now, request)
                                    else:
                                        messages.error(request, u"Existe algún error en la transacción por favor revise.")
                                        contador += 1
                                    break
                        else:
                            contador += 1
                            messages.error(request, u"Existen algunos errores en el formulario, por favor corríja")
                    else:
                        messages.error(request, u"El valor total a pagar debe ser mayor a $ 0.0, "
                                                u"por favor corríja")
                        contador += 1

                else:  # Cruce de cuentas
                    num_comp = get_num_comp(18, cabecera.getFecha(), True, request)
                    if num_comp == -1:
                        contador += 1

                        now = datetime.datetime.now()
                        try:
                            secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=18)  # Secuencia de Compras
                            fecha_comp = secuencia.fecha_actual
                        except SecuenciaTipoComprobante.DoesNotExist:
                            fecha_comp = now.strftime("%Y-%m-%d")

                        messages.error(request, u"La fecha ingresada es menor a la del último cruce de "
                                                u"cuentas: "+str(fecha_comp)+ u", por favor corríja")
                        errors = cabecera._errors.setdefault("fecha", ErrorList())
                        errors.append(u"La fecha ingresada es menor a la del último cruce de cuentas.")
                    total_cruce_cta = get_total_cobro(detalle_cta_cruzar)
                    GuardarCobroCruceDocs(cabecera, cobro, cliente, num_comp, total_cruce_cta, asiento, now, request)

                    if detalle_cta_cruzar.is_valid():
                        if total_cruce_cta > 0:
                            for form in detalle_cta_cruzar:
                                informacion = form.cleaned_data
                                valor = float(informacion.get("valor"))
                                if valor > 0:
                                    cuenta_cobrar = Cuentas_por_Cobrar.objects.get(id=informacion.get("id_cuenta_cobrar"))
                                    if cuenta_cobrar.fecha_reg > cabecera.getFecha():
                                        messages.error(request, u"La fecha del documento a cruzar es mayor a la "
                                                                u"fecha de registro del cobro, por favor verifique")
                                        contador += 1
                                        errors1 = form._errors.setdefault("fecha_reg", ErrorList())
                                        errors1.append(u"Error")

                                        errors2 = cabecera._errors.setdefault("fecha", ErrorList())
                                        errors2.append(u"Error")

                                    try:
                                        cuenta_cruzar = PlanCuenta.objects.get(id=informacion.get("cuenta"), status=1)
                                    except PlanCuenta.DoesNotExist:
                                        cuenta_cruzar = None
                                        errors = form._errors.setdefault("cuenta", ErrorList())
                                        errors.append(u"Por favor ingrese una cuenta")
                                        contador += 1

                                    if contador == 0:
                                        #########################################
                                        #   Validar que el valor que intenten   #
                                        #   enviar no supere el valor adeudado  #
                                        #########################################
                                        if redondeo(valor) > redondeo(cuenta_cobrar.monto - cuenta_cobrar.cobrado):
                                            contador += 1
                                            errors = form._errors.setdefault("valor", ErrorList())
                                            errors.append(u"El valor ingresado supera el monto")
                                            messages.error(request, u"Error, el valor del pago supera el monto de la deuda. El total a cruzar es: " + str(total_cobrar) +
                                                           u" y lo adeudado es: "+str(cuenta_cobrar.monto - cuenta_cobrar.cobrado))
                                        else:
                                            cuenta_cobrar.cobrado += redondeo(valor)

                                        cuenta_cobrar.fecha_actualizacion = now
                                        cuenta_cobrar.usuario_actualizacion = request.user.username
                                        cuenta_cobrar.save()

                                        cxc_cobro = CXC_cobro()
                                        cxc_cobro.cobro = cobro
                                        cxc_cobro.cuentas_x_cobrar = cuenta_cobrar
                                        cxc_cobro.monto = valor
                                        cxc_cobro.concepto = u"Cruce de cta. en cobro a cliente: " + cobro.cliente.razon_social[0:10] + u". Por una " \
                                                                                                                                        u"cantidad de " + str(valor)
                                        cxc_cobro.plan_cuenta = cuenta_cruzar
                                        cxc_cobro.usuario_creacion = request.user.username
                                        cxc_cobro.fecha_creacion = now
                                        cxc_cobro.save()

                                        detalle_asiento = Detalle_Comp_Contable()
                                        detalle_asiento.cabecera_contable = asiento
                                        detalle_asiento.detalle = cxc_cobro.concepto
                                        detalle_asiento.valor = valor

                                        if cuenta_cobrar.naturaleza == 1:  # Facturas del cliente
                                            detalle_asiento.dbcr = "H"
                                        else:  # Anticipos, Notas de Débito
                                            detalle_asiento.dbcr = "D"

                                        if cuenta_cobrar.documento.codigo_documento == "AN":
                                            # Anticipo a Clientes
                                            detalle_asiento.plan_cuenta = PlanCuenta.objects.get(tipo_id=13)
                                        else:
                                            detalle_asiento.plan_cuenta = cliente.plan_cuenta

                                        detalle_asiento.fecha_asiento = asiento.fecha

                                        detalle_asiento.fecha_creacion = now
                                        detalle_asiento.usuario_creacion = request.user.username
                                        detalle_asiento.save()

                                        # Contracuenta
                                        detalle_asiento2 = Detalle_Comp_Contable()
                                        detalle_asiento2.cabecera_contable = asiento
                                        detalle_asiento2.detalle = cxc_cobro.concepto
                                        detalle_asiento2.valor = valor

                                        if cuenta_cobrar.naturaleza == 1:  # Facturas del cliente
                                            detalle_asiento2.dbcr = "D"
                                        else:  # Anticipos, Notas de Débito
                                            detalle_asiento2.dbcr = "H"

                                        detalle_asiento2.fecha_asiento = asiento.fecha

                                        detalle_asiento2.plan_cuenta = cuenta_cruzar
                                        detalle_asiento2.fecha_creacion = now
                                        detalle_asiento2.usuario_creacion = request.user.username
                                        detalle_asiento2.save()

                                else:
                                    pass  # Validar cuando agregen valores con cero
                        else:
                            messages.error(request, u"Necesita ingresar un valor mayor a cero dólares para realizar "
                                                    u"esta transacción")
                            contador += 1
                    else:
                        messages.error(request, u"Existió errores en el cruce de cuentas, por favor revise.")
                        contador += 1
                if contador == 0:
                    messages.success(request, u"Se ha realizado con éxito la transacción con número "
                                              u"de comprobante: " + num_comp)
                    return HttpResponseRedirect(reverse("lista_cobro"))
                else:
                    transaction.rollback()
            else:
                messages.error(request, u"Por favor llene todos los campos requeridos")
        except Cliente.DoesNotExist:
            transaction.rollback()
            messages.error(request, u"Existió un problema al generar la transacción")
            return HttpResponseRedirect(reverse("lista_cobro"))

    return render_to_response('Cuentas_Cobrar/Clientes/cruce_documentos.html',
                              {"cabecera": cabecera,
                               "detalle_docs": detalle_docs,
                               "detalle_doc_cobrar": detalle_doc_cobrar,
                               "detalle_cta_cruzar": detalle_cta_cruzar,
                               'flag': flag
                               }, context_instance=RequestContext(request))


@login_required(login_url='/')
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def anular_cobro(request, id):
    """
    Vista que sirve para anular un cobro
    :param request:
    :param id:
    :return:
    """
    try:
        cobro = Cobro.objects.get(id=id)
        cxp_cobro = CXC_cobro.objects.filter(cobro=cobro)
        now = datetime.datetime.now()

        if not esta_periodo_contable(cobro.fecha_reg):
            messages.error(request, u"No se puede anular este cobro ya que la fecha de registro esta dentro del "
                                    u"periodo de cierre contable")
            return HttpResponseRedirect(reverse("lista_cobro"))
        else:
            # caso cruce de documento y cruce de cuentas
            if cobro.forma_pago_id == 5 or cobro.forma_pago_id == 6:
                for obj in cxp_cobro:
                    cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(id=obj.cuentas_x_cobrar_id)
                    cuentas_por_cobrar.cobrado = cuentas_por_cobrar.cobrado - obj.monto

                    cuentas_por_cobrar.fecha_actualizacion = datetime.datetime.now()
                    cuentas_por_cobrar.usuario_actualizacion = request.user.username
                    cuentas_por_cobrar.save()

                    obj.status = 2
                    obj.fecha_actualizacion = now
                    obj.usuario_actualizacion = request.user.username
                    obj.save()
            else:
                for obj in cxp_cobro:
                    cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(id=obj.cuentas_x_cobrar.id)

                    if cuentas_por_cobrar.naturaleza == 1:  # No es un anticipo o Nota de Crédito
                        cuentas_por_cobrar.cobrado = cuentas_por_cobrar.cobrado - obj.monto

                    # caso anticipo
                    # Si el anticipo que desea anular esta cruzado con algún documento, no se
                    # podría anular el pago, primero se necesitaría anular el cruce de documento.
                    else:
                        # Como es un anticipo anulo la cuenta por pagar ya que se anuló el pago
                        cuentas_por_cobrar.status = 2
                        if cuentas_por_cobrar.cobrado > 0:
                            raise ErrorCobro(u"No se puede anular el cobro, ya que posee un cruce de documentos"
                                             u", anule dicho cruce para que pueda anular"
                                             u" este cobro")

                    cuentas_por_cobrar.fecha_actualizacion = now
                    cuentas_por_cobrar.usuario_actualizacion = request.user.username
                    cuentas_por_cobrar.save()

                    obj.status = 2
                    obj.fecha_actualizacion = now
                    obj.usuario_actualizacion = request.user.username
                    obj.save()


            # Anula la transacción bancaria
            try:
                banco = Banco.objects.get(num_comp=cobro.num_comp)
                banco.status = 2
                banco.fecha_actualizacion = now
                banco.usuario_actualizacion = request.user.username
                banco.save()
            except Banco.DoesNotExist:
                pass

            cobro.status = 2
            cobro.fecha_actualizacion = datetime.datetime.now()
            cobro.usuario_actualizacion = request.user.username
            cobro.save()

            cabecera_asiento = Cabecera_Comp_Contable.objects.get(numero_comprobante=cobro.num_comp)
            cabecera_asiento.status = 2
            cabecera_asiento.fecha_actualizacion = datetime.datetime.now()
            cabecera_asiento.usuario_actualizacion = request.user.username
            cabecera_asiento.save()

            detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_asiento)
            for obj in detalle_mayor:
                obj.status = 2
                obj.fecha_actualizacion = datetime.datetime.now()
                obj.usuario_actualizacion = request.user.username
                obj.save()

            messages.success(request, u"Se anuló el cobro satisfactoriamente")
            return HttpResponseRedirect(reverse("lista_cobro"))
    except ErrorCobro, e:
        transaction.rollback()
        messages.error(request, e)
        if e == "":
            messages.error(request, u"Recurso no encontrado")
        return HttpResponseRedirect(reverse("lista_cobro"))
    except Cobro.DoesNotExist:
        transaction.rollback()
        raise Http404


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def reversar_cobro(request, id):
    """
    Vista que sirve para reversar un cobro
    :param request:
    :param id:
    :return:
    """
    try:
        cobro = Cobro.objects.get(id=id)
        now = datetime.datetime.now()
        new_cobro = copiar_modelo(cobro)
        if cobro.esta_reversado():
            respuesta = {"status": 2,
                         "mensaje": u"El cobro que quiere reversar ya se encuentra reversado, "
                                    u"por favor verifique"}
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')

        elif cobro.cobro_id is not None or cobro.tipo_comprobante_id == 16:  # Es una reversion o una Ret en ventas
            respuesta = {"status": 2,
                         "mensaje": u"No se puede reversar este comprobante, por favor verifíque"}
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')

        else:
            cxc_cobro = CXC_cobro.objects.filter(cobro=cobro)
            if request.method == 'POST':
                reversar_form = ReversionCuentasCobrarForm(request.POST)
                if reversar_form.is_valid():
                    if not esta_periodo_contable(reversar_form.getFecha()):
                        respuesta = {"status": 2,
                                     "mensaje": u"No se puede reversar este cobro ya que la fecha de "
                                                u"registro es menor igual al cierre contable"}
                        resultado = json.dumps(respuesta)
                        return HttpResponse(resultado, mimetype='application/json')
                    else:
                        new_cobro.num_comp = get_num_comp(27, reversar_form.getFecha(), True, request)

                        if cobro.fecha_reg > reversar_form.getFecha():
                            respuesta = {"status": 2,
                                         "mensaje": u"La fecha que ingresó es menor a la fecha del cobro que quiere "
                                                    u"reversar por favor verifique"}
                            resultado = json.dumps(respuesta)
                            return HttpResponse(resultado, mimetype='application/json')
                        elif new_cobro.num_comp == -1:
                            respuesta = {"status": 2,
                                         "mensaje": u"La fecha que ingresó es menor a la última reversión que "
                                                    u"realizó, por favor verifique"}
                            resultado = json.dumps(respuesta)
                            return HttpResponse(resultado, mimetype='application/json')

                        new_cobro.tipo_comprobante_id = 27
                        new_cobro.fecha_reg = reversar_form.getFecha()
                        new_cobro.fecha_creacion = now
                        new_cobro.cobro_id = cobro
                        new_cobro.fecha_actualizacion = None
                        new_cobro.usuario_actualizacion = ''
                        new_cobro.usuario_creacion = request.user.username
                        if reversar_form.getCconcepto() == "" or reversar_form.getCconcepto().isspace():
                            new_cobro.concepto = u"Reversar: " + new_cobro.num_comp + u" " + cobro.concepto
                        else:
                            new_cobro.concepto = reversar_form.getCconcepto()
                        new_cobro.save()

                        # caso cruce de documento y cruce de cuentas
                        if cobro.forma_pago_id == 5 or cobro.forma_pago_id == 6:
                            for obj in cxc_cobro:
                                cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(id=obj.cuentas_x_cobrar_id)
                                cuentas_por_cobrar.cobrado = cuentas_por_cobrar.cobrado - obj.monto

                                cuentas_por_cobrar.fecha_actualizacion = datetime.datetime.now()
                                cuentas_por_cobrar.usuario_actualizacion = request.user.username
                                cuentas_por_cobrar.save()

                                new_cxc_cobro = copiar_modelo(obj)
                                new_cxc_cobro.cobro = new_cobro
                                new_cxc_cobro.concepto = u"Reversado: " + new_cxc_cobro.concepto
                                new_cxc_cobro.naturaleza = new_cxc_cobro.cuentas_x_cobrar.naturaleza  # Tendra la misma naturaleza que su cta x cobrar
                                new_cxc_cobro.save()
                        else:
                            for obj in cxc_cobro:
                                cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(id=obj.cuentas_x_cobrar.id)
                                new_cxc_cobro = copiar_modelo(obj)

                                if cuentas_por_cobrar.naturaleza == 1:  # No es un anticipo o Nota de Crédito
                                    cuentas_por_cobrar.cobrado = cuentas_por_cobrar.cobrado - obj.monto
                                    new_cxc_cobro.naturaleza = 1

                                # caso anticipo
                                # Si el anticipo que desea anular esta cruzado con algún documento, no se
                                # podría anular el pago, primero se necesitaría anular el cruce de documento.
                                else:
                                    # Como es un anticipo anulo la cuenta por pagar ya que se anuló el pago
                                    if cuentas_por_cobrar.cobrado > 0:
                                        transaction.rollback()
                                        respuesta = {"status": 2,
                                                     "mensaje": u"No se puede reversar el cobro, ya que posee un cruce de documentos"
                                                                u", anule dicho cruce para que pueda anular"
                                                                u" este cobro"}
                                        resultado = json.dumps(respuesta)
                                        return HttpResponse(resultado, mimetype='application/json')
                                    else:
                                        cuentas_por_cobrar.cobrado = cuentas_por_cobrar.monto
                                    new_cxc_cobro.naturaleza = 2

                                cuentas_por_cobrar.fecha_actualizacion = now
                                cuentas_por_cobrar.usuario_actualizacion = request.user.username
                                cuentas_por_cobrar.save()

                                new_cxc_cobro.cobro = new_cobro
                                new_cxc_cobro.concepto = u"Reversado: " + new_cxc_cobro.concepto
                                new_cxc_cobro.save()

                        cabecera_asiento = Cabecera_Comp_Contable.objects.get(numero_comprobante=cobro.num_comp)

                        new_cabecera_asiento = copiar_modelo(cabecera_asiento)
                        new_cabecera_asiento.concepto_comprobante = reversar_form.getCconcepto()
                        new_cabecera_asiento.fecha = reversar_form.getFecha()
                        new_cabecera_asiento.numero_comprobante = new_cobro.num_comp
                        new_cabecera_asiento.tipo_comprobante_id = 27
                        new_cabecera_asiento.save()

                        detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_asiento)

                        for obj in detalle_mayor:
                            new_obj = copiar_modelo(obj)
                            new_obj.fecha_asiento = reversar_form.getFecha()
                            new_obj.cabecera_contable = new_cabecera_asiento
                            if new_obj.dbcr == 'D':
                                new_obj.dbcr = 'H'
                                new_obj.save()
                            else:
                                new_obj.dbcr = 'D'
                                new_obj.save()

                        messages.success(request, u"Se reversó el cobro satisfactoriamente con "
                                                  u"número de comprobante "+new_cobro.num_comp)

                        respuesta = {"status": 1, "mensaje": ""}
                        resultado = json.dumps(respuesta)
                        return HttpResponse(resultado, mimetype='application/json')
                else:
                    respuesta = {"status": 2, "mensaje": u"Ingrese la fecha de la reversión del cobro"}
                    resultado = json.dumps(respuesta)
                    return HttpResponse(resultado, mimetype='application/json')

    except Cobro.DoesNotExist:
        transaction.rollback()
        respuesta = {"status": 2, "mensaje": u"Recurso no encontrado"}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def detalle_cobro_pdf(request, id):
    """
    Vista que devuelve el PDF de un cobro
    :param request:
    :param id:
    :return:
    """
    try:
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

        usuario = request.user.first_name+' '+request.user.last_name
        cobro = Cobro.objects.get(id=int(id))
        now = datetime.datetime.now()
        cliente = cobro.cliente
        cxp_cobro = CXC_cobro.objects.exclude(status=0).filter(cobro=cobro)
        plan_cuenta = cxp_cobro[0].plan_cuenta
        cabecera_asiento = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=cobro.num_comp)
        detalle_mayor = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera_asiento).order_by("dbcr", "plan_cuenta__codigo")

        html = render_to_string('Cuentas_Cobrar/Clientes/comprobante_detalle_pago_del_cliente_pdf.html',
                                {'pagesize': 'A4',
                                 "cobro": cobro,
                                 "empresa": empresa,
                                 "usuario": usuario,
                                 "mayor_cab": cabecera_asiento,
                                 "detalle_may": detalle_mayor,
                                 "detalle_pago": cxp_cobro,
                                 "cuenta": plan_cuenta,
                                 "cliente": cliente,
                                 "now": now
                                 }, context_instance=RequestContext(request))
        return generar_pdf_get(html)
    except ValueError:
        raise Http404
    except Cliente.DoesNotExist:
        raise Http404


@csrf_exempt
@login_required(login_url='/')
def lista_cuentas_x_cobrar_iniciales(request):
    """
    Vista en la cual se enlistan
    todos los pagos
    :param request:
    :return:
    """
    buscador = BuscadorCuentasxCobrarinicialPagos(request.GET)

    cxc_lista = busqueda_cuentas_cobrar_ini(buscador)
    paginacion = Paginator(cxc_lista, 10)
    numero_pagina = request.GET.get("page")

    try:
        pagos = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        pagos = paginacion.page(1)
    except EmptyPage:
        pagos = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = pagos.number
    arreglo_num_paginado = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Cuentas_Cobrar/saldos_iniciales/lista_saldos_iniciales_cxc.html',
                              {"objetos": pagos, "buscador": buscador, "total_paginas": total_paginas,
                               "numero": numero, "arreglo_paginado": arreglo_num_paginado},
                              context_instance=RequestContext(request))


def tiene_doc_repetidos(cta_por_pagar):
    a = []
    for form in cta_por_pagar:
        informacion = form.cleaned_data
        tipo_doc = informacion.get("tipo_doc")
        num_doc = informacion.get("num_doc")

        if (tipo_doc, num_doc) in a:
            errors = form._errors.setdefault("num_doc", ErrorList())
            errors.append(u"El documento que ingresó ya se encuentra registrado "
                          u"en el sistema")
            return True

        a.append((tipo_doc, num_doc))

    return False


def redireccionar_cuenta_pagar_inicial(request):
    """
    redirecciona dependiendo de la acción que desee el usuario
    :return:
    """
    if request.GET.get("grabar_agregar", "") == "1":
        full_redirect_url = reverse("cuentas_x_cobrar_iniciales")

    else:
        full_redirect_url = reverse("lista_cuentas_x_cobrar_iniciales")

    return full_redirect_url


@login_required(login_url="/")
@csrf_exempt
@transaction.commit_on_success
def cuentas_x_cobrar_iniciales(request):
    empresa = get_parametros_empresa()
    cabecera = CabeceraCXCInicialForm(initial={"fecha_reg": empresa.fecha_inicio_sistema - timedelta(days=1)})
    cta_por_cobrar_formset = formset_factory(CuentasCobrarInicialForm, extra=1)
    cta_por_cobrar = cta_por_cobrar_formset()
    now = datetime.datetime.now()
    cont_doc = 0
    contador = 0

    if request.method == "POST":
        cabecera = CabeceraCXCInicialForm(request.POST)
        cta_por_cobrar = cta_por_cobrar_formset(request.POST)

        if cabecera.is_valid():
            cliente = Cliente.objects.get(id=cabecera.getCliente())
            if cta_por_cobrar.is_valid():
                if not tiene_doc_repetidos(cta_por_cobrar):  # Si no tiene documentos repetidos ingresados
                    for form in cta_por_cobrar:
                        cxc = Cuentas_por_Cobrar()
                        informacion = form.cleaned_data
                        documento = Documento.objects.get(id=informacion.get("tipo_doc"))
                        cxc.documento = documento
                        if Cuentas_por_Cobrar.objects.filter(num_doc=informacion.get("num_doc"),
                                                             cliente=cliente,
                                                             documento=documento).exists():
                            errors = form._errors.setdefault("num_doc", ErrorList())
                            errors.append(u"El documento que ingresó ya se encuentra registrado "
                                          u"en el sistema, por favor verifique")
                            cont_doc += 1

                        cxc.num_doc = informacion.get("num_doc")
                        cxc.cliente = cliente
                        cxc.monto = informacion.get("monto")
                        cxc.cobrado = 0
                        if documento.codigo_documento == "04" or documento.codigo_documento == "AN":
                            cxc.naturaleza = 2
                        else:
                            cxc.naturaleza = 1
                        cxc.plazo = informacion.get("plazo")
                        cxc.fecha_reg = empresa.fecha_inicio_sistema - timedelta(days=1)
                        cxc.fecha_creacion = now
                        cxc.usuario_creacion = request.user.username
                        cxc.save()
                else:
                    messages.error(request, u"No puede ingresar dos documentos iguales del "
                                            u"mismo proveedor, por favor verifique")
                    contador += 1

                if cont_doc > 0:
                    messages.error(request, u"El documento que ingresó ya se "
                                            u"encuentra registrado en el sistema, por favor verifique")
                    transaction.rollback()

                elif contador > 0:
                    transaction.rollback()

                else:
                    messages.success(request, u"Se ha guardado los saldos de las cuentas por pagar satisfactoriamente")
                    return HttpResponseRedirect(redireccionar_cuenta_pagar_inicial(request))
        else:
            pass

    return render_to_response('Cuentas_Cobrar/saldos_iniciales/saldos_iniciales_cxc.html',
                              {"cabecera": cabecera,
                               'cta_por_cobrar': cta_por_cobrar,
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
def eliminar_cuentas_x_cobrar_iniciales(request, id):
    """
    Vista en la cual se enlistan
    todos los pagos
    :param request:
    :return:
    """

    cxp = Cuentas_por_Cobrar.objects.get(id=id)
    empresa = get_datos_empresa()
    parametros_emp = Empresa_Parametro.objects.using("base_central").get(id=empresa.id)

    if cxp.cobrado == 0:
        if cxp.fecha_reg == parametros_emp.fecha_inicio_sistema - timedelta(days=1):
            cxp.status = 0
            cxp.save()
            messages.success(request, u"Se ha eliminado con éxito la cuenta por cobrar " +
                             cxp.documento.descripcion_documento + u" #"
                             + cxp.num_doc + u" del cliente: " + cxp.cliente.razon_social)

        else:
            messages.error(request, u"Imposible realizar esta operación, cuenta no encontrada")
    else:
        messages.error(request, u"La cuenta por pagar que quiere eliminar tiene cobros a su favor, "
                                u"por favor verifique")

    return HttpResponseRedirect(reverse('lista_cuentas_x_cobrar_iniciales'))
