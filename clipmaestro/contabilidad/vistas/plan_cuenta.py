#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from contabilidad.formularios.PlanCuentaForm import *
from contabilidad.models import *
from librerias.funciones.permisos import *
from librerias.funciones.funciones_vistas import *
from django.db import transaction
import json
import datetime

__author__ = 'Clip Maestro'
@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def lista_plan_cuenta(request):
    formulario = PlanCuentaForm()
    nivel1 = PlanCuenta.objects.filter(nivel=1, status=1).order_by("codigo")
    return render_to_response("plan cuenta/lista_plan_cuenta.html",{"nivel1":nivel1,"formulario": formulario, "request": request},context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso, es_ajax=True)
def agregar_plan(request):
    cuenta = PlanCuenta()
    movimientoform = PlanCuentaForm(request.POST)

    if movimientoform.is_valid():
        try:
            if movimientoform.getidCta() is not None:
                cta_grupo = PlanCuenta.objects.get(id=movimientoform.getidCta())
                cuenta.id_cta_grupo = cta_grupo
                cuenta.nivel = cuenta.id_cta_grupo.nivel + 1
                if movimientoform.get_tipo_cuenta() is not None:  # Cuando se selecciona un tipo de cuenta
                    cuenta.tipo = movimientoform.get_tipo_cuenta()
                objetos = PlanCuenta.objects.filter(nivel=cuenta.nivel, status=1, id_cta_grupo=cuenta.id_cta_grupo)
                cantidad = len(objetos)

                if (cuenta.nivel == 1 or cuenta.nivel == 2) and cantidad >= 9:
                    response_data = {"status": 2, "mensaje": u'No se puede agregar mas de 9 cuentas a este nivel'}
                    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")

                elif (cuenta.nivel == 3 or cuenta.nivel == 4) and cantidad >= 99:
                    response_data = {"status": 2, "mensaje": u'No se puede agregar mas de 99 cuentas a este nivel'}
                    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")

                elif cuenta.nivel == 5 and cantidad >= 999:
                    response_data = {"status": 2, "mensaje": u'No se puede agregar mas de 999 cuentas a este nivel'}
                    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")

                if cta_grupo.tipo is not None:
                    cuenta.tipo = cta_grupo.tipo
            else:
                cuenta.nivel = 1
                cuenta.id_plan_cuenta = None
                objetos = PlanCuenta.objects.filter(nivel=cuenta.nivel, status=1)
                cantidad = len(objetos)
                if cantidad >= 9:
                    messages.error(request, u"No se puede agregar mas de 9 cuentas a este nivel")
                    response_data = {"status": 2, "mensaje": u'No se puede agregar mas de 9 cuentas a este nivel'}
                    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")

            cuenta.naturaleza = movimientoform.getNaturaleza()
            cuenta.descripcion = movimientoform.getDescripcion()
            cuenta.codigo = cuenta.ObtenerCodigo()

            if cuenta.nivel != 5:
                cuenta.clase_cuenta = "G"
            else:
                cuenta.clase_cuenta = "M"

            cuenta.status = 1
            cuenta.fecha_creacion = datetime.datetime.now()
            cuenta.usuario_creacion = request.user.username
            cuenta.save()

            response_data = {"status": 1, "mensaje": u'Se guardó con exito la cuenta o grupo de cuentas'}
            messages.success(request, u"Se agregó con éxito la cuenta o grupo de cuentas")

        except:
            response_data = {"status": 2, "mensaje": u'Error General'}
            return HttpResponse(json.dumps(response_data),content_type="application/json; charset=utf-8")
    else:
        response_data = {"status": 2, "mensaje": u'Por favor ingrese los campos obligatorios'}
    return HttpResponse(json.dumps(response_data),content_type="application/json; charset=utf-8")


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso, es_ajax=True)
def editar_plan(request):
    cont = 0  # cuenta las coincidencias
    movimientoform = PlanCuentaForm(request.POST)
    now = datetime.datetime.now()
    if movimientoform.is_valid():
        try:
            detalle_mayor = Detalle_Comp_Contable.objects.filter(status=1)
            cuenta = PlanCuenta.objects.get(id=movimientoform.getidCta())
            if cuenta.nivel == 5:
                for obj in detalle_mayor:
                    if obj.plan_cuenta.id == cuenta.id:
                        cont += 1
            else:
                lista_hijos = cuenta.getHijos()
                #################################################################
                # Se utiliza la función tiene_mov si las cuentas de movimiento  #
                #      a las que estan por debajo de ella tienen saldos         #
                #################################################################
                if lista_hijos is not None:
                    for objhijos in lista_hijos:
                        for objdetalle in detalle_mayor:
                            if tiene_mov(objhijos, objdetalle.id):
                                cont += 1
            if cont == 0:
                cuenta.descripcion = movimientoform.getDescripcion()

                tipo_banco = TipoCuenta.objects.get(id=1)
                if cuenta.nivel == 5 and cuenta.tipo == tipo_banco:
                    cta_banco = Cuenta_Banco.objects.get(plan_cuenta=cuenta)
                    cta_banco.descripcion = cuenta.descripcion

                    cta_banco.usuario_actualizacion = request.user.username
                    cta_banco.fecha_actualizacion = now
                    cta_banco.save()
                cuenta.naturaleza = movimientoform.getNaturaleza()

                cuenta.fecha_actualizacion = now
                cuenta.usuario_actualizacion = request.user.username

                if movimientoform.get_tipo_cuenta() is not None:
                    cuenta.tipo = movimientoform.get_tipo_cuenta()
                else:
                    cuenta.tipo = None

                cuenta.save()
                messages.success(request, u"Se editó la cuenta con éxito")
                response_data = {"respuesta": 1, "mensaje": u'Cuenta contable modificada con éxito'}
            else:
                messages.error(request,u"No se puede editar esa cuenta, ya que existen movimientos contables con ella")
                response_data = {"respuesta": 2, "mensaje": u'No se puede editar esa cuenta, ya que existen movimientos contables con ella'}
        except:
            messages.error(request, u"Error en la modificación de la cuenta, por favor intente más tarde")
            response_data = {"status": 1, "mensaje": u'Error en la modificación de la cuenta, por favor intente más tarde'}
    else:
        response_data = {"status": 2, "mensaje": u'Por favor llene los campos obligatorios'}
    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")


@login_required(login_url='/')
@csrf_exempt
def obtener_datos(request):
    if request.POST['id'] is None:
        response_data = {"respuesta": 1, "mensaje": u'Existió un error, id no válido'}
    else:
        try:
            cuenta = PlanCuenta.objects.get(id=request.POST['id'])
            if cuenta.tipo is None:
                tipo = 0
            else:
                tipo = cuenta.tipo_id

            response_data = {"naturaleza": cuenta.naturaleza,
                             "descripcion": cuenta.descripcion,
                             "tipo": tipo}
        except PlanCuenta.DoesNotExist:
            response_data = {"respuesta": 1, "mensaje": u'Cuenta contable no encontrada'}
    return HttpResponse(json.dumps(response_data),content_type="application/json; charset=utf-8")

@transaction.commit_on_success
@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso, es_ajax=True)
def eliminar_cuenta(request):
    cont = 0
    try:
        detalle_mayor = Detalle_Comp_Contable.objects.filter(status=1)
        cuenta = PlanCuenta.objects.get(id=request.POST['id'])
        ########################################################
        #    Revisa si existen transacciones con esa cuenta    #
        ########################################################
        if cuenta.nivel == 5:
            for obj in detalle_mayor:
                if obj.plan_cuenta.id == cuenta.id:
                    cont += 1
        else:
            lista_hijos = cuenta.getHijos()
            #################################################################
            # Se utiliza la función tiene_mov si las cuentas de movimiento  #
            # a las que estan por debajo de ella tienen saldos              #
            #################################################################
            if lista_hijos != None:
                for objhijos in lista_hijos:
                    for objdetalle in detalle_mayor:
                        if tiene_mov(objhijos, objdetalle.plan_cuenta.id):
                            cont += 1
        #########################################
        # Inactivar tambien a todos sus "hijos" #
        #########################################
        if cont == 0:
            cuenta.fecha_actualizacion = datetime.datetime.now()
            cuenta.usuario_actualizacion = request.user.username
            eliminarHijos(cuenta, request.user.username)
            cuenta.status = False
            if cuenta.tipo is not None:
                if cuenta.tipo.alias == 1:
                    banco = Banco.objects.get(plan_cuenta=cuenta)
                    banco.status = 0
                    banco.fecha_actualizacion = datetime.datetime.now()
                    banco.usuario_actualizacion = request.user.username
                    banco.save()
            cuenta.save()
            messages.success(request, u"Se eliminó con éxito la cuenta")
            response_data = {"respuesta": 'sucess', "mensaje": u'La cuenta ha sido eliminada'}
        else:
            messages.error(request, u"No se puede eliminar la cuenta, ya existen movimientos con ella")
            response_data = {"respuesta": 'sucess', "mensaje": u'La cuenta no ha sido eliminada'}
        return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")
    except PlanCuenta.DoesNotExist:
        transaction.rollback()
        messages.error(request, u"Existió un error, cuenta no encontrada")
        response_data = {"respuesta": 1, "mensaje": u'Existió un error, cuenta no encontrada'}
        return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")


##########################################
# Funcion recursiva que elimina todos los
# nodos("hijos") del arbol
##########################################
def eliminarHijos(plan, usuario):
    fecha = datetime.datetime.now()
    if plan.nivel == 5:
        plan.fecha_actualizacion = fecha
        plan.usuario_actualizacion = usuario
        plan.status = False
        return 1
    else:
        for obj in plan.getHijos():
            obj.fecha_actualizacion = fecha
            obj.usuario_actualizacion = usuario
            obj.status = False
            obj.save()
            if obj.nivel < 5:
                eliminarHijos(obj, usuario)
        return 0


@login_required(login_url='/')
@csrf_exempt
def get_tipo_cuenta(request):
    if request.POST['id'] is None:
        response_data = {"status": 0, "mensaje": u'Existió un error, id no válido'}
    else:
        try:
            cuenta = PlanCuenta.objects.get(id=request.POST['id'])
            if cuenta.tipo is None:
                tipo = 0
            else:
                tipo = cuenta.tipo.id

            response_data = {"status": 1, "tipo": tipo}
        except PlanCuenta.DoesNotExist:
            response_data = {"status": 0, "mensaje": u'Cuenta contable no encontrada'}
    return HttpResponse(json.dumps(response_data),content_type="application/json; charset=utf-8")