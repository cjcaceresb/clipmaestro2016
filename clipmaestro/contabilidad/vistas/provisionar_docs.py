#! /usr/bin/python
# -*- coding: UTF-8-*-
from datetime import timedelta
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from contabilidad.formularios.ClienteForm import *
from contabilidad.formularios.DireccionesForm import *
from contabilidad.formularios.VentaForm import *
from contabilidad.formularios.FormGuiaRemision import *
from contabilidad.formularios.ProvisionesForm import *
from django.forms.util import ErrorList
from librerias.funciones.paginacion import *
from librerias.funciones.validacion_rucs import *
from django.core.paginator import *
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from django import template
from contabilidad.funciones.ventas_func import *
from librerias.funciones.funciones_vistas import *
import operator
from django import forms
from django.utils.html import *
from contabilidad.models import *
from librerias.funciones.permisos import *
from contabilidad.funciones.provisiones_func import *


@csrf_exempt
@transaction.commit_on_success
@login_required(login_url='/')
def provision_doc_venta(request):
    tipo_comprobante = TipoComprobante.objects.get(id=2)
    bandera = 0             # Validador
    flag_post = 0
    try:
        secuencia_t_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        fecha_actual = secuencia_t_comp.fecha_actual
        formulario_provision = FormProvisionVenta(initial={"fecha": fecha_actual})
    except SecuenciaTipoComprobante.DoesNotExist:
        formulario_provision = FormProvisionVenta()
        fecha_actual = datetime.datetime.now().date()

    try:
        if request.method == "POST":
            formulario_provision = FormProvisionVenta(request.POST)
            flag_post = 1
            if formulario_provision.is_valid():
                if esta_periodo_contable(formulario_provision.getFecha()):
                    bandera += ProvisionarVenta(formulario_provision,  tipo_comprobante, fecha_actual, request)

                    if bandera == 0:
                        messages.success(request, u"Se provisionaron exitosamente los documentos.")
                        return HttpResponseRedirect(reverse("lista_ventas"))
                    else:
                        transaction.rollback()
                else:
                    errors = formulario_provision._errors.setdefault("fecha", ErrorList())
                    errors.append(u"Campo Requerido")
                    parametro_empresa = get_parametros_empresa()
                    messages.error(request, u"Por favor llene los campos requeridos para continuar: "+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))

            else:
                messages.error(request, u"Por favor llene los campos requeridos para continuar")
    except:
        transaction.rollback()
        messages.error(request, u"Existen errores en el servidor al momento de aprovisionar el documento, "
                                u"por favor intente nuevamente")

    return render_to_response('provision_documentos/aprovision_venta.html',
                              {"formulario_provision": formulario_provision,
                               "flag_post": flag_post}, context_instance=RequestContext(request))

@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def provision_compra(request):
    tipo_comprobante = TipoComprobante.objects.get(id=1)  # tipo comprobante de compra
    bandera = 0     # Validador
    try:
        secuencia_t_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        fecha_actual = secuencia_t_comp.fecha_actual
        formulario_provision = FormProvisionCompra(initial={"fecha": fecha_actual})
    except SecuenciaTipoComprobante.DoesNotExist:
        formulario_provision = FormProvisionCompra()
        fecha_actual = datetime.datetime.now().date()

    try:
        empresa_id = Empresa.objects.all()[0].empresa_general_id
        empresa = EmpresaGeneral.objects.get(status=1, id=empresa_id)
    except:
        messages.error(request, u"Por favor, llene los datos de la empresa antes de hacer cualquier transacción")
        return HttpResponseRedirect(reverse("configuracion_empresa"))

    if request.method == "POST":
        formulario_provision = FormProvisionCompra(request.POST)
        if formulario_provision.is_valid():
            if esta_periodo_contable(formulario_provision.getFecha()):
                bandera += ProvisionarCompra(formulario_provision, tipo_comprobante, empresa, fecha_actual, request)
                if bandera == 0:
                    messages.success(request, u"Se provisionó exitosamente los documentos.")
                    return HttpResponseRedirect(reverse("lista_compras"))
                else:
                    transaction.rollback()
            else:
                errors = formulario_provision._errors.setdefault("fecha", ErrorList())
                errors.append(u"Campo Requerido")
                parametro_empresa = get_parametros_empresa()
                messages.error(request, u"Por favor llene los campos requeridos para continuar: "+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))
        else:
            messages.error(request, u"Por favor llene los campos requeridos para continuar")

    return render_to_response('provision_documentos/aprovision_compra.html',
                              {"formulario_provision": formulario_provision}, context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
def provision_cheque(request):
    '''
    Vista por implementar que realiza el procedimiento de provisionar cheque para reservar la
    secuencia
    :param request:
    :return:
    '''
    tipo_comprobante = TipoComprobante.objects.get(id=4)  # tipo comprobante EGRESO
    bandera = 0     # Validador
    try:
        secuencia_t_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        fecha_actual = secuencia_t_comp.fecha_actual
        formulario_provision = FormProvisionCheque(initial={"fecha": fecha_actual})

    except SecuenciaTipoComprobante.DoesNotExist:
        formulario_provision = FormProvisionCheque()
        fecha_actual = datetime.datetime.now().date()

    if request.method == "POST":
        formulario_provision = FormProvisionCheque(request.POST)

        if formulario_provision.is_valid():
            if esta_periodo_contable(formulario_provision.getFecha()):
                bandera += ProvisionarCheque(formulario_provision, tipo_comprobante, fecha_actual, request)

                if bandera == 0:
                    messages.success(request, u"Se provisionó exitosamente los documentos.")
                    return HttpResponseRedirect(reverse("lista_transaccion_banco"))
                else:
                    transaction.rollback()
            else:
                errors = formulario_provision._errors.setdefault("fecha", ErrorList())
                errors.append(u"Campo Requerido")
                parametro_empresa = get_parametros_empresa()
                messages.error(request, u'Por favor verifique el campo Fecha, debe ser mayor a la fecha de cierre: '+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))

        else:
            messages.error(request, u"Por favor llene los campos requeridos para continuar")

    return render_to_response('provision_documentos/provision_cheque.html',
                              {"formulario_provision": formulario_provision}, context_instance=RequestContext(request))