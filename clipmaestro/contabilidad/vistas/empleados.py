#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.core.paginator import *
from contabilidad.models import *
from administracion.models import *
from contabilidad.formularios.EmplreadosForm import *
from clipmaestro.templatetags.formulario_base import *
from sys import modules
from sys import path
import operator
import datetime
import time
from django.forms.util import ErrorList
from django.core.paginator import *
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.forms.formsets import formset_factory
from django.core.context_processors import request
from django.shortcuts import render_to_response
from django.http import *
from django.forms.util import ErrorList
from django.db import connection
from django.db import IntegrityError, transaction
from librerias.funciones.funciones_vistas import *
from contabilidad.funciones.empleados_func import *
from librerias.funciones.permisos import *
from librerias.funciones.paginacion import *

import json

__author__ = 'Roberto'

def busqueda_empleados(buscador):
    '''
    Función que realiza las búsquedas
    :param buscador:
    :return:
    '''
    predicates = []

    predicates.append(('nombres_completos__icontains', buscador.getNombres()))
    predicates.append(('apellidos_completos__icontains', buscador.getApellidos()))
    if buscador.getNumID() != "":
        predicates.append(('num_id__icontains', buscador.getNumID()))
    q_list = [Q(x) for x in predicates]
    entries = Empleados.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).distinct()
    return entries

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def lista_empleados(request):
    '''
    Vista que se encarga de listar a
    todos los empleados
    :param request:
    :return:
    '''
    buscador = FormBuscadorEmpleados(request.GET)
    # Realiza las consultas y llena la lista "empleados"
    empleados = busqueda_empleados(buscador)
    # Objeto paginator recibe la lista de "empleados" y la cantidad de a limitar get_num_filas_template()
    paginacion = Paginator(empleados, get_num_filas_template())
    numero_pagina = request.GET.get("page")

    try:
        empleados = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        empleados = paginacion.page(1)
    except EmptyPage:
        empleados = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = empleados.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Empleados/lista_empleados.html',
                              {"total_paginas": total_paginas, "objetos": empleados, "buscador": buscador,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               "arreglo_paginado": lista, "numero": numero}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def agregar_empleado(request):
    '''
    Vista que realiza la función
    de registar a un empleado en el
    sistema
    :param request:
    :return:
    '''
    tipo = 1
    formulario = FormEmpleados()
    contador = 0.0

    if request.method == "POST":
        formulario = FormEmpleados(request.POST)

        if formulario.is_valid():
            empleado = Empleados()
            empleado.nombres_completos = formulario.getNombres()
            empleado.apellidos_completos = formulario.getApellidos()
            empleado.num_id = formulario.getNumID()

            if Empleados.objects.filter(status=1).exclude(Q(num_id="9999999999")|Q(num_id="9999999999999")).filter(num_id=empleado.num_id).exists():
                contador += 1
                messages.error(request, u"El N° de Identificación ya ha sido registrado")


            else:

                if len(empleado.num_id) == 10:

                    if not isCedula(empleado.num_id):
                        contador += 1
                        errors = formulario._errors.setdefault("num_id", ErrorList())
                        errors.append(u"")
                        messages.error(request, u"Ingrese un N° de Identificación válido.")

                elif len(empleado.num_id) == 13:

                    if not isPersonaNatural(empleado.num_id):
                        contador += 1
                        errors = formulario._errors.setdefault("num_id", ErrorList())
                        errors.append(u"")
                        messages.error(request, u"Ingrese un N° de Identificación válido.")

                else:
                    contador += 1
                    errors = formulario._errors.setdefault("num_id", ErrorList())
                    errors.append(u"")
                    messages.error(request, u"Ingrese un N° de Identificación válido.")

            if contador == 0.0:
                empleado.sueldo = formulario.getSueldo()
                empleado.fecha_creacion = datetime.datetime.now()
                empleado.usuario_creacion = request.user.username
                empleado.save()
                messages.success(request, u"Se agrego satisfactoriamente al empleado: "+ unicode(empleado.nombres_completos)+" "+unicode(empleado.apellidos_completos)+u"")
                return HttpResponseRedirect(reverse("lista_empleados"))

            else:
                transaction.rollback()

        else:
            transaction.rollback()
            messages.error(request, u"Error llene correctamente la información del formulario")


    return render_to_response("Empleados/agregar_empleados.html",
                              {"formulario": formulario, "tipo": tipo}, context_instance=RequestContext(request))

@login_required(login_url='/')
def detalle_empleado(request, id):
    '''
    Vista que muestra la información
    del empleado
    :param request:
    :param id:
    :return:
    '''
    tipo = 3

    try:
        empleado = Empleados.objects.get(id=id)
        formulario = FormEmpleadoOnly(initial={
            "num_id": empleado.num_id,
            "nombres": empleado.nombres_completos,
            "apellidos": empleado.apellidos_completos,
            "sueldo": empleado.sueldo,
        })
    except:
        messages.error(request, u"El Empleado no se encuentra registrado")
        return HttpResponseRedirect(reverse("lista_empleados"))


    return render_to_response('Empleados/agregar_empleados.html',
                              {"objeto": empleado,
                               "id": id,
                               "formulario": formulario,
                               "tipo": tipo,
                               "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def editar_empleado(request, id):
    '''
    Vista que edita la información
    básica del empleado
    :param request:
    :param id:
    :return:
    '''
    tipo = 2
    contador = 0.0

    try:
        nuevo = Empleados.objects.filter(status=1).get(id=id)
        formulario = FormEmpleados(initial={"num_id": nuevo.num_id, "nombres": nuevo.nombres_completos, "apellidos": nuevo.apellidos_completos,
                                            "sueldo": nuevo.sueldo})

    except:
        messages.error(request, u"Existió un error al obtener la información del empleado")
        return HttpResponseRedirect(reverse("lista_empleados"))

    if request.method == "POST":
        formulario = FormEmpleados(request.POST)

        if formulario.is_valid():
            nuevo.nombres_completos = formulario.getNombres()
            nuevo.apellidos_completos = formulario.getApellidos()
            nuevo.num_id = formulario.getNumID()
            nuevo.sueldo = formulario.getSueldo()

            if Empleados.objects.exclude(status=0).exclude(Q(num_id="9999999999")|Q(num_id="9999999999999")).\
                    filter(Q(num_id=nuevo.num_id), ~Q(id=nuevo.id)).exists():

                contador += 1
                messages.error(request, u"El N° de Identificación ya ha sido ingresado.")

            else:


                if len(nuevo.num_id) == 10:

                    if not isCedula(nuevo.num_id):
                        contador += 1
                        errors = formulario._errors.setdefault("num_id", ErrorList())
                        errors.append(u"")
                        messages.error(request, u"Ingrese un N° de Identificación válido.")

                elif len(nuevo.num_id) == 13:

                    if not isPersonaNatural(nuevo.num_id):
                        contador += 1
                        errors = formulario._errors.setdefault("num_id", ErrorList())
                        errors.append(u"")
                        messages.error(request, u"Ingrese un N° de Identificación válido.")

                else:
                    contador += 1
                    errors = formulario._errors.setdefault("num_id", ErrorList())
                    errors.append(u"")
                    messages.error(request, u"Ingrese un N° de Identificación válido.")

            if contador == 0.0:
                nuevo.usuario_actualizacion = request.user.username
                nuevo.fecha_actualizacion = datetime.datetime.now()
                nuevo.save()
                messages.success(request, u"La información del empleado ha sido editada exitosamente")
                return HttpResponseRedirect(reverse("lista_empleados"))

            else:
                transaction.rollback()

        else:
            transaction.rollback()
            messages.error(request, u"Error al editar la información del empleado")

    return render_to_response('Empleados/agregar_empleados.html', {'formulario': formulario, "tipo": tipo, "id": id},
                               context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def generar_sueldo_empleado(request):
    '''
    Vista que realiza la acción de generar
    el sueldo mensualemente
    :param request:
    :return:
    '''
    now = datetime.datetime.now()
    tipo_comprobante = TipoComprobante.objects.get(id=20, status=1)     # Rol
    flag = 0        # Variable que me dice si los datos ya han sido cargados por POST
    detalle_form_set = formset_factory(EmpleadoDetalleForm, extra=1)
    detalles_empleados = detalle_form_set()

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        fecha_actual = secuencia_tipo.fecha_actual
        fin_de_mes = last_day_of_month(fecha_actual)
    except SecuenciaTipoComprobante.DoesNotExist:
        secuencia_tipo = None
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")
        fin_de_mes = last_day_of_month(fecha_actual)

    formulario = FormGenerarSueldoEmpleados(initial={"anio": datetime.datetime.now().year,
                                                     "mes": get_mes(fin_de_mes.month)})

    try:
        if request.method == "POST":
            formulario = FormGenerarSueldoEmpleados(request.POST)
            detalles_empleados = detalle_form_set(request.POST, request.FILES)
            flag = 1
            if formulario.is_valid():
                if detalles_empleados.is_valid():
                    #contador = generar_sueldo_mensual(detalles_empleados, formulario, now, request)
                    for form in detalles_empleados:
                        informacion = form.cleaned_data
                        sueldo_empleo = Sueldo_Empleado()
                        sueldo_empleo.empleado = Empleados.objects.get(id=informacion("id_empleado"))
                        sueldo_empleo.mes = fin_de_mes.month
                        sueldo_empleo.anio = formulario.getAnio()
                        sueldo_empleo.sueldo = informacion.get("sueldo")

                        if not sueldo_empleo.usuario_creacion:
                            sueldo_empleo.usuario_creacion = request.user.username

                        sueldo_empleo.usuario_actualizacion = request.user.username

                        if not sueldo_empleo.fecha_creacion:
                            sueldo_empleo.fecha_creacion = now

                        sueldo_empleo.fecha_actualizacion = now
                        sueldo_empleo.save()

                    fecha_siguientes = now + datetime.timedelta(days=35)
                    secuencia_tipo.fecha_actual = fecha_siguientes.strftime("%Y-%m-%d")
                    secuencia_tipo.usuario_actualizacion = request.user.username
                    secuencia_tipo.save()

                    messages.success(request, u"EL proceso de generar sueldos ha sido realizado exitosamente")
                    return HttpResponseRedirect(reverse("lista_empleados"))

                else:
                    transaction.rollback()
                    messages.error(request, u"Error")

            else:
                transaction.rollback()
                messages.error(request, u"Error elija correctamente el año y mes para realizar "
                                        u"la transacción de los sueldos")

    except:
        transaction.rollback()
        messages.error(request, u"Existieron errores durante la transacción por favor intente nuevamente.")


    return render_to_response('Empleados/generar_sueldo.html',
                              {"formulario": formulario,
                               "forms": detalles_empleados,
                               "flag_post": flag}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
def eliminar_empleado(request, id):
    '''
    Vista que realiza la función de hacer un
    borrado lógico es decir status = 0 , de un
    empleado
    :param request:
    :param id:
    :return:
    '''
    now_ac = datetime.datetime.now()
    try:
        empleado = Empleados.objects.get(id=id)

        if empleado.status != 0:
            empleado.status = 0
            empleado.usuario_actualizacion = request.user.username
            empleado.fecha_actualizacion = now_ac
            empleado.save()
            messages.success(request, u"La información del empleado: "+
                                      unicode(empleado.nombres_completos)+" "+unicode(empleado.apellidos_completos)+
                                      u"   ha sido eliminado exitósamente")

            return HttpResponseRedirect(reverse("lista_empleados"))
    except:
        messages.error(request, u"Error al elminar la información del empleado")

    return HttpResponseRedirect(reverse("lista_empleados"))


@login_required(login_url='/')
@csrf_exempt
def cambiar_estado_empleado(request, id):
    try:
        persona_select = Empleados.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if persona_select.status == 1:
            persona_select.status = 2
            persona_select.usuario_actualizacion = request.user.username
            persona_select.fecha_actualizacion = now_ac
            messages.success(request, u'Se inactivó al empleado : "'+unicode(persona_select.nombres_completos)+" "+unicode(persona_select.apellidos_completos)+u'" exitosamente')

        elif persona_select.status == 2:
            persona_select.status = 1
            persona_select.usuario_actualizacion = request.user.username
            persona_select.fecha_actualizacion = now_ac
            messages.success(request, u'Se activó al empleado : "'+unicode(persona_select.nombres_completos)+" "+unicode(persona_select.apellidos_completos)+u'" exitosamente')

        persona_select.save()
    except:
        messages.error(request, u"Error al cambiar de estado.")
    return HttpResponseRedirect(reverse("lista_empleados"))
