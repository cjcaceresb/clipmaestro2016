#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt, csrf_protect
import json
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *

from django.db import transaction
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context

from django.core.paginator import *
from django import template
from administracion.models import *
from librerias.funciones.permisos import *
from librerias.funciones.paginacion import *
from contabilidad.funciones.contratos_func import *
from contabilidad.funciones.compras_func import *
from contabilidad.formularios.ContratoForm import *
from django.contrib import messages

__author__ = 'Clip Maestro'


@login_required(login_url='/')
def lista_contrato(request):
    '''
    Vista encargada de listar los registros
    de contratos en el sistema
    :param request:
    :return:
    '''
    buscador = BuscadorContrato(request.GET)
    buscador.is_valid()
    contratos = busqueda_contrato(buscador)                     # Query para la búsqueda
    paginacion = Paginator(contratos, get_num_filas_template())
    numero_pagina = request.GET.get("page")

    try:
        lista = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista = paginacion.page(1)
    except EmptyPage:
        lista = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista.number
    arreglo = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('contrato/lista_contratos.html',
                              {"objetos": lista, "total_paginas": total_paginas, "numero": numero,
                               "arreglo_paginado": arreglo,
                               "muestra_paginado": muestra_paginacion_template(paginacion.count),
                               'buscador': buscador}, context_instance=RequestContext(request))

@login_required(login_url="/")
@csrf_exempt
@csrf_protect
def agregar_contrato(request):

    tipo_comprobante = TipoComprobante.objects.get(id=19, status=1)

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        sec_ini = secuencia_tipo.secuencia
        fecha_actual = secuencia_tipo.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        sec_ini = 0
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")

    formulario = ContratoForm(initial={"fecha_reg": fecha_actual, "secuencia": sec_ini,
                                        "valor": 0.0, "costo_estimado": 0.0, "utilidad": 0.0, "iva": "01", "monto_iva": 0.0})

    contador = 0
    tipo = 1                    # Agregar Contrato

    if request.method == "POST":
        contrato = ContratoCabecera()
        now = datetime.datetime.now()
        formulario = ContratoForm(request.POST)
        formulario.is_valid()
        if formulario.is_valid():
            if esta_periodo_contable(formulario.getFechaReg()):
                contrato.fecha_reg = formulario.getFechaReg()
                contrato.fecha_entrega = formulario.getFechaEnt()
                contrato.cliente = formulario.getCliente()
                contrato.tipo_pago = formulario.getTipoPago()
                contrato.concepto = formulario.getDescripcion()
                contrato.responsable = formulario.getResponsable()
                contrato.vendedor = formulario.getVendedor()
                contrato.descuento_porc0 = 0.0
                contrato.monto_descuento0 = 0.0
                contrato.descuento_porciva = 0.0
                contrato.monto_descuentoiva = 0.0


                if formulario.getFechaEnt() < formulario.getFechaReg():
                    contador += 1
                    messages.error(request, u"La fecha de entrega no puede ser menor a la de registro")

                contrato.fecha_reg = formulario.getFechaReg()

                num_comp_validador = get_num_contrato(tipo_comprobante.id, formulario.getFechaReg(), True, request)

                if num_comp_validador == -1:
                    contador += 1
                    messages.error(request, u"La fecha de registro es menor al último registro: "
                                    +str(fecha_actual)+u", por favor verifique la fecha")

                else:
                    contrato.num_cont = num_comp_validador

                if formulario.getIva() == "01":
                    contrato.monto_iva = float(formulario.getMontoIva())
                    contrato.baseiva = float(formulario.getValor())

                elif formulario.getIva() == "02":
                    contrato.monto_iva = 0.0
                    contrato.base0 = float(formulario.getValor())


                contrato.costo_estimado = formulario.getCostoEst()

                if formulario.getTipoPago().id == 2:
                    contrato.plazo = formulario.getPlazo()
                else:
                    contrato.plazo = 0

                contrato.fecha_creacion = now
                contrato.usuario_creacion = request.user.username

                if contador == 0:
                    contrato.save()
                    messages.success(request, u"Se ha agregado con éxito el contrato: "+contrato.num_cont)
                    return HttpResponseRedirect(reverse("lista_contrato"))
            else:
                errors = formulario._errors.setdefault("fecha", ErrorList())
                errors.append(u"Campo Requerido")
                parametro_empresa = get_parametros_empresa()
                messages.error(request, u'Por favor verifique el campo Fecha, debe ser mayor a la fecha de cierre: '+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))
        else:
            messages.error(request, u"Existen algunos errores en el formulario, por favor verifique")
    return render_to_response('contrato/contrato.html',
                              {"formulario": formulario,
                               'tipo': tipo}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@csrf_protect
def detalle_contrato(request, id):
    try:
        contrato = ContratoCabecera.objects.get(id=id)
        contrato_detalle = ContratoDetalle.objects.filter(contrato_cabecera=contrato, status=1)

        return render_to_response('contrato/detalle_contrato.html',
                                {"formulario": contrato,
                                "contrato_detalle": contrato_detalle}, context_instance=RequestContext(request))

    except ContratoCabecera.DoesNotExist:
        raise Http404

@login_required(login_url='/')
@csrf_exempt
@csrf_protect
def editar_contrato(request, id):
    contador = 0.0

    try:
        contrato = ContratoCabecera.objects.get(id=id)
        tipo = 2  # Editar Contrato
        valor = contrato.baseiva if contrato.baseiva > 0 else contrato.base0
        monto_iva = contrato.monto_iva if contrato.baseiva > 0 else 0
        impuesto = "01" if contrato.baseiva > 0 else "02"

        formulario = ContratoForm(initial={"fecha_reg": contrato.fecha_reg,
                                           "fecha_ent": contrato.fecha_entrega,
                                           "cliente": contrato.cliente_id,
                                           "tipo_pago": contrato.tipo_pago_id,
                                           "plazo": contrato.plazo,
                                           "vendedor": contrato.vendedor_id,
                                           "responsable": contrato.responsable,
                                           "descripcion": contrato.concepto,
                                           "valor": valor,
                                           "iva": impuesto,
                                           "monto_iva": monto_iva,
                                           "total": "",
                                           "costo_estimado": contrato.costo_estimado,
                                           "utilidad": ""})


        if request.method == "POST":
            now = datetime.datetime.now()
            formulario = ContratoForm(request.POST)
            if formulario.is_valid():
                contrato.fecha_reg = formulario.getFechaReg()
                contrato.fecha_entrega = formulario.getFechaEnt()
                contrato.cliente = formulario.getCliente()
                contrato.tipo_pago = formulario.getTipoPago()
                contrato.concepto = formulario.getDescripcion()
                contrato.responsable = formulario.getResponsable()
                contrato.baseiva = formulario.getValor()
                contrato.monto_iva = formulario.getIva()
                contrato.vendedor = formulario.getVendedor()

                if formulario.getFechaEnt() < formulario.getFechaReg():
                    contador += 1
                    messages.error(request, u"La fecha de entrega no puede ser menor a la de registro")
                contrato.valor = formulario.getValor()
                contrato.costo_est = formulario.getCostoEst()
                if formulario.getTipoPago().id == 2:
                    contrato.plazo = formulario.getPlazo()

                contrato.fecha_creacion = now
                contrato.usuario_creacion = request.user.username

                if contador == 0:
                    contrato.save()
                    messages.success(request, u"Se ha editado con éxito el contrato")
                    return HttpResponseRedirect(reverse("lista_contrato"))
            else:
                messages.error(request, u"Existe algunos errores en el formulario, por favor verifique")
        return render_to_response('contrato/contrato.html',
                                  {"formulario": formulario,
                                   "tipo": tipo}, context_instance=RequestContext(request))

    except ContratoCabecera.DoesNotExist:
        raise Http404

@login_required(login_url='/')
def anular_contrato(request, id):
    try:
        contrato = ContratoCabecera.objects.get(id=id)
        contrato_detalle = ContratoDetalle.objects.filter(contrato_cabecera=contrato, status=1)
        now = datetime.datetime.now()

        contrato.status = 2
        contrato.fecha_actualizacion = now
        contrato.usuario_actualizacion = request.user.username
        contrato.save()

        for obj in contrato_detalle:
            obj.status = 2
            obj.fecha_actualizacion = now
            obj.usuario_actualizacion = request.user.username
            obj.save()
        messages.success(request, u"El cotrato ha sido eliminado satisfactoriamente")
        return HttpResponseRedirect(reverse("lista_contrato"))

    except ContratoCabecera.DoesNotExist:
        raise Http404


@login_required(login_url='/')
@csrf_exempt
def costo_contrato(request):
    '''
    Vista de transacción para cargar el
    costo al Contrato
    Costos Adicionales de Contrato
    :param request:
    :return:
    '''
    now = datetime.datetime.now()
    tipo_comprobante = TipoComprobante.objects.get(id=21)   # Costo Contrato

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        fecha_actual = secuencia_tipo.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")

    formulario = CostoContratoForm(initial={"fecha_reg": fecha_actual})
    flag_post = 0
    detalle_contrato_proveedor_formset = formset_factory(CostoContratoDetalleProveedorForm, extra=1)
    detalle_contrato_costo_proveedor = detalle_contrato_proveedor_formset(prefix="detalle_contrato_costo_proveedor")
    detalle_contrato_empleado_formset = formset_factory(CostoContratoDetalleEmpleadoForm, extra=1)
    detalle_contrato_costo_empleado = detalle_contrato_empleado_formset(prefix="detalle_contrato_costo_empleado")

    if request.method == "POST":
        formulario = CostoContratoForm(request.POST)
        detalle_contrato_costo_proveedor = detalle_contrato_proveedor_formset(request.POST, prefix="detalle_contrato_costo_proveedor")
        detalle_contrato_costo_empleado = detalle_contrato_empleado_formset(request.POST, prefix="detalle_contrato_costo_empleado")
        flag_post = 1

        if formulario.is_valid():
            costo_contrato_cabecera = CostoContratoCabecera()
            contador = Guardar_costo_contrato_cabecera(formulario, costo_contrato_cabecera, request, now)

            if formulario.getTipo() == "01":
                if detalle_contrato_costo_proveedor.is_valid():
                    total = Guardar_detalle_costo_contrato_externa(detalle_contrato_costo_proveedor,
                                                           costo_contrato_cabecera, request, now)

                    # Actualizo CostoDristribuirCompras
                    costo_distribuir = CostoDistribuirCompras.objects.filter(status=1).get(compra_cabecera__id=costo_contrato_cabecera.compra_cabecera_id)

                    if total > float(costo_distribuir.monto):
                        contador += 1
                        messages.error(request, u"Error el valor ingresado al contrato es mayor al total de la compra")

                    else:

                        costo_distribuir.distribuido += total
                        costo_distribuir.usuario_actualizacion = request.user.username
                        costo_distribuir.fecha_actualizacion = now
                        costo_distribuir.save()

                    ### Guarda Contabilidad
                    cabecera_asiento = Cabecera_Comp_Contable()
                    Guardar_asiento_contable_cabecera_costo_contrato(cabecera_asiento, costo_contrato_cabecera, request, now)
                    Guardar_detalle_asiento_contable_costo_contrato(detalle_contrato_costo_proveedor, cabecera_asiento, request, now)

                    if contador > 0:
                        transaction.rollback()
                    else:
                        messages.success(request, u"La Transacción de Costo de Contrato ha sido realizada exitosamente")
                        return HttpResponseRedirect(reverse("lista_contrato"))
                else:
                    transaction.rollback()
                    messages.error(request, u"Error llene correctamente la información del detalle de la transacción")

            else:

                if detalle_contrato_costo_empleado.is_valid():
                    Guardar_detalle_costo_contrato_interna(detalle_contrato_costo_empleado,
                                                           costo_contrato_cabecera, request, now)
                else:
                    transaction.rollback()
                    messages.error(request, u"")


            if contador > 0:
                transaction.rollback()

        else:
            transaction.rollback()
            messages.error(request, u"Error al llenar el formulario de la cabecera")

    return render_to_response('contrato/costo_contrato.html',
                             {"formulario": formulario,
                              "detalle_contrato_costo_proveedor": detalle_contrato_costo_proveedor,
                              "detalle_contrato_costo_empleado": detalle_contrato_costo_empleado,
                              "flag_post": flag_post}, context_instance=RequestContext(request))