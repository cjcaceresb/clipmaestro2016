#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.core.paginator import *
from contabilidad.models import *
from contabilidad.formularios.ClienteForm import *
from contabilidad.formularios.DireccionesForm import *
from contabilidad.formularios.ContactosForm import *
from sys import modules
from django.forms.formsets import formset_factory
from sys import path
import datetime
from django.forms.util import ErrorList
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
import time
from librerias.funciones.paginacion import *
from django.http import HttpResponse
import ho.pisa as pisa
import cStringIO as StringIO
import cgi
from librerias.funciones.validacion_rucs import *
from django.template.loader import render_to_string
from django.db import IntegrityError, transaction
import json
from django.core.validators import validate_email
from librerias.funciones.funciones_vistas import *
from contabilidad.funciones.clientes_func import *
from librerias.funciones.permisos import *


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def clientes(request):
    '''
    Vista que me permite listar todos los clientes registrados en el sistema
    :param request:
    :return:
    '''
    tipo = 1                                                # Para seleccionar Busqueda de Cliente en la función
                                                            # de búsqueda en "funciones_vistas"
    buscador = BuscadorFormClienteProveedor(request.GET)
    clientes = busqueda_clientes_proveedores(buscador, tipo)
    paginacion = Paginator(clientes, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    try:
        clientes = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        clientes = paginacion.page(1)
    except EmptyPage:
        clientes = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = clientes.number
    lista = arreglo_paginas_template(total_paginas, numero)
    return render_to_response('cliente/cliente.html', {"objetos": clientes, "total_paginas": total_paginas,
                               "numero": numero, "request": request, "buscador": buscador,
                                "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                                "arreglo_paginado": lista}, context_instance=RequestContext(request))

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def registrar_cliente(request):
    '''
    Vista que permite guardar en el sistema un cliente
    :param request:
    :return:
    '''
    today = datetime.datetime.now()
    formulario_cliente = FormClientes()
    detalle_form_test = formset_factory(FormDireccion, extra=1)
    formulario_direcciones = detalle_form_test(prefix="formset_direcciones")
    formset_contacto = formset_factory(FormContacto, extra=1, formset=ContactoFormSet)
    formulario_contactos = formset_contacto(prefix="formset_contactos")
    tipo = 1
    bandera_direccion = 0
    flag_contacto = 0

    try:
        if request.method == "POST":
            formulario_cliente = FormClientes(request.POST)
            formulario_direccion_ajax = FormDireccion(request.POST)

            if request.is_ajax():
                respuesta = []
                cliente_vta = Cliente()
                direccion_vta = Cliente_Direccion()

                try:
                    return agregar_cliente_ajax(cliente_vta, formulario_cliente, respuesta, direccion_vta, formulario_direccion_ajax,  request)
                except:
                    respuesta.append({"status": 0, "mensaje": "Error General"})
                    resultado = json.dumps(respuesta)
                    return HttpResponse(resultado, mimetype='application/json')

            else:   # FORM POST

                formulario_direcciones = detalle_form_test(request.POST,  prefix="formset_direcciones")
                formulario_contactos = formset_contacto(request.POST, prefix="formset_contactos")

                if formulario_cliente.is_valid():
                    cliente_obj = Cliente()
                    contador = GuardarCliente(formulario_cliente, cliente_obj, today, request)

                    if contador == 0:
                        bandera_direccion = GuardarDirecciones(formulario_direcciones, cliente_obj, today, request)

                        if bandera_direccion == 0:
                            flag_contacto = GuardarContacto(formulario_contactos, cliente_obj, today, request)

                            if flag_contacto == 0:
                                messages.success(request, u"El Cliente se registró exitosamente")
                                return HttpResponseRedirect(reverse("clientes"))
                else:
                    lista_errores = "Por favor verifique los siguientes campos: "
                    for i in formulario_cliente.errors:
                        lista_errores = lista_errores +(unicode(i)) + ", "
                    if formulario_cliente.errors:
                        messages.error(request, unicode(lista_errores[0:-2]))
                    for i in formulario_cliente:
                        if i.errors:
                            i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
                    raise ErrorClientes(u"")

                if contador > 0 or bandera_direccion > 0 or flag_contacto > 0:
                    transaction.rollback()

    except ErrorClientes, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    return render_to_response("cliente/registrar_cliente.html", {"formulario": formulario_cliente,
                                                                 "formulario_direcciones": formulario_direcciones,
                                                                "formulario_contactos": formulario_contactos,
                                                                 "tipo": tipo, "request": request},
                                                                context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def modificar_cliente(request, id, numero):
    '''
    Vista que permite modificar la información registrada del cliente seleccionado
    :param request:
    :param id:
    :param numero:
    :return:
    '''
    now_ac = datetime.datetime.now()
    contador = 0
    existe_contacto = 0
    existe_direccion = 0
    tipo = 2                                # Variable tipo que me indica la vista donde es llamado y reutilizar
                                            # el mismo html
    bandera_direccion = 0
    flag_contacto = 0
    detalle_form_test = formset_factory(FormDireccion, extra=1)
    formulario_direcciones = detalle_form_test(prefix="formset_direcciones")
    formset_contacto = formset_factory(FormContacto, extra=1, formset=ContactoFormSet)
    formulario_contactos = formset_contacto(prefix="formset_contactos")

    try:
        nuevo = Cliente.objects.get(id=id)
        formulario = FormClientes(initial=instancia_form_basica_cliente(nuevo))
        direcciones = []
        try:
            det_form = []
            multiform = formset_factory(FormDireccion, extra=0)
            direcciones = Cliente_Direccion.objects.filter(status=1).filter(cliente=nuevo)

            if (direcciones):
                existe_direccion = 1
                for obj in direcciones:
                    det_form.append({"descripcion": obj.descripcion, "direccion1": obj.direccion1,
                                         "direccion2": obj.direccion2, "atencion": obj.atencion,
                                         "ciudad_direccion": obj.ciudad.id, "telefono": obj.telefono})
            formulario_direcciones = multiform(initial=det_form, prefix="formset_direcciones")

        except:
            pass
            existe_direccion = 0

        if existe_direccion == 0:
            formulario_direcciones = detalle_form_test(prefix="formset_direcciones")

        ############# FORM CONTACTOS #####################
        contactos = []
        try:
            det_form_contacto = []
            formset = formset_factory(FormContacto, extra=0)
            contactos = Contacto.objects.filter(status=1).filter(cliente_proveedor_id=nuevo.id, tipo=1)
            if contactos:
                existe_contacto = 1
                for c in contactos:
                    det_form_contacto.append({"nombres": c.nombres, "apellidos": c.apellidos, "email": c.correo,
                                              "telefono_contacto": c.telefono, "movil": c.movil, "departamento": c.departamento.id})

            formulario_contactos = formset(initial=det_form_contacto, prefix="formset_contactos")
        except:
            existe_contacto = 0

        if existe_contacto == 0:
            formulario_contactos = formset_contacto(prefix="formset_contactos")

    except:
       messages.error(request, u"Error al modificar  al cliente.")
       return HttpResponseRedirect(reverse("clientes"))

    try:
        if request.method == "POST":
            formulario = FormClientes(request.POST)
            formulario_direcciones = detalle_form_test(request.POST,  prefix="formset_direcciones")
            formulario_contactos = formset_contacto(request.POST, prefix="formset_contactos")

            if formulario.is_valid():
                contador = EditarCliente(formulario, nuevo, now_ac, request)

                for i in formulario:
                    if i.errors:
                        i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

                if contador == 0:
                    for d in direcciones:
                        d.status = 2
                        d.usuario_actualizacion = request.user.username
                        d.fecha_actualizacion = now_ac
                        d.save()
                        #if nuevo.id == d.cliente.id and d.status == 2:
                        #    d.delete()

                    bandera_direccion = GuardarDirecciones(formulario_direcciones, nuevo, now_ac, request)

                    if bandera_direccion == 0:
                        if existe_contacto == 1:
                            for c in contactos:
                                c.status = 2
                                c.usuario_actualizacion = request.user.username
                                c.fecha_actualizacion = now_ac
                                c.save()

                                if nuevo.id == c.cliente_proveedor_id and c.tipo == 1 and c.status == 2:
                                    c.delete()

                        flag_contacto = GuardarContacto(formulario_contactos, nuevo, now_ac, request)

                        if flag_contacto == 0:
                            messages.success(request, u"El Cliente "+ unicode(nuevo.razon_social)+
                                                      u" se  modificó exitosamente")
                            return HttpResponseRedirect(reverse("clientes"))

            else:

                lista_errores = u"Por favor verifique los siguientes campos: "
                for i in formulario.errors:
                    lista_errores = lista_errores +(unicode(i)) + ", "
                    if formulario.errors:
                        messages.error(request, unicode(lista_errores[0:-2]))
                    for i in formulario:
                        if i.errors:
                            i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

            if contador > 0 or bandera_direccion > 0 or flag_contacto > 0:
                transaction.rollback()

    except:
         transaction.rollback()
         messages.error(request, u"Existieron errores al modificar al cliente")

    return render_to_response("cliente/registrar_cliente.html",
                              {"formulario": formulario,
                               "formulario_direcciones": formulario_direcciones,
                               "formulario_contactos": formulario_contactos,
                               "obj": nuevo,
                               "tipo": tipo,
                               "numero": numero, "id": id}, context_instance=RequestContext(request))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def detalle_cliente(request, id, numero):
    '''
    Vista que presenta la información del cliente
    :param request:
    :param id:
    :param numero:
    :return:
    '''
    tipo = 3                                # Variable tipo que me indica la vista donde es llamado y reutilizar
                                            # el mismo html
    existe_contacto = 0
    existe_direccion = 0
    detalle_form_test = formset_factory(DireccionesFormSetOnlyRead, extra=1)
    formulario_direcciones = detalle_form_test(prefix="formset_direcciones")
    formset_contacto = formset_factory(ContactoFormSetOnlyRead, extra=1)
    formulario_contactos = formset_contacto(prefix="formset_contactos")
    try:
        cliente = Cliente.objects.get(id=id)
        formulario_cliente = FormClienteOnlyRead(initial=instancia_form_basica_cliente(cliente))
        ################### FORM DIRECCIONES #####################
        try:
            det_form = []
            multiform = formset_factory(DireccionesFormSetOnlyRead, extra=0)

            direcciones = Cliente_Direccion.objects.filter(status=1).filter(cliente=cliente)
            if (direcciones):
                existe_direccion = 1
                for obj in direcciones:
                    det_form.append({"descripcion": obj.descripcion, "direccion1": obj.direccion1,
                                     "direccion2": obj.direccion2, "atencion": obj.atencion,
                                     "ciudad_direccion": obj.ciudad.id, "telefono": obj.telefono})
            formulario_direcciones = multiform(initial=det_form, prefix="formset_direcciones")
        except:
            existe_direccion = 0

        if existe_direccion == 0:
            formulario_direcciones = detalle_form_test(prefix="formset_direcciones")

        ###################### FORM CONTACTOS #####################
        try:
            det_form_contacto = []
            formset = formset_factory(ContactoFormSetOnlyRead, extra=0)
            contactos = Contacto.objects.filter(status=1).filter(cliente_proveedor_id=cliente.id, tipo=1)

            if (contactos):
                existe_contacto = 1
                for c in contactos:
                    if c.departamento is None:
                        det_form_contacto.append({"nombres": c.nombres, "apellidos": c.apellidos, "email": c.correo,
                                                  "telefono_contacto": c.telefono, "movil": c.movil, "departamento": ""})
                    else:
                        det_form_contacto.append({"nombres": c.nombres, "apellidos": c.apellidos, "email": c.correo,
                                                  "telefono_contacto": c.telefono, "movil": c.movil,
                                                  "departamento": c.departamento.id})
            formulario_contactos = formset(initial=det_form_contacto, prefix="formset_contactos")
        except:
            existe_contacto = 0

        if existe_contacto == 0:
            formulario_contactos = formset_contacto(prefix="formset_contactos")
    except:
         messages.error(request, u"Error al cargar la información del cliente.")
         return HttpResponseRedirect(reverse("clientes"))

    return render_to_response('cliente/registrar_cliente.html',
                              {"objeto": cliente, "id": id,
                               "numero": numero,
                               "formulario": formulario_cliente,
                               "formulario_direcciones": formulario_direcciones,
                               "formulario_contactos": formulario_contactos,
                               "tipo": tipo,
                               "request": request}, context_instance=RequestContext(request))



@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def eliminar_cliente(request, id):
    '''
    Vista que realiza la opción de cambiar estado a eliminado
        Ej: status = 0
    :param request:
    :param id:
    :return:
    '''
    now_ac = datetime.datetime.now()
    try:
        cliente = Cliente.objects.get(id=id)

        if Venta_Cabecera.objects.filter(status=1).filter(cliente=cliente).exists():
            messages.error(request, u"El cliente no puede ser eliminado, posee transacciones.")
            return HttpResponseRedirect(reverse("clientes"))

        elif Cuentas_por_Cobrar.objects.filter(status=1).filter(venta_cabecera__cliente=cliente).exists():
            messages.error(request, u"El cliente no puede ser eliminado, posee Cuentas por Cobrar.")
            return HttpResponseRedirect(reverse("clientes"))

        else:

            print

            if cliente.status != 0:
                cliente.status = 0
                cliente.usuario_actualizacion = request.user.username
                cliente.fecha_actualizacion = now_ac
            cliente.save()

            if cliente.status == 0:
                direcciones = Cliente_Direccion.objects.filter(status=1).filter(cliente=cliente)
                for d in direcciones:
                    d.status = 0
                    d.usuario_actualizacion = request.user.username
                    d.fecha_actualizacion = now_ac
                    d.save()

                try:
                    contactos = Contacto.objects.filter(status=1).filter(tipo=1, cliente_proveedor_id=cliente.id)
                    for c in contactos:
                        c.status = 0
                        c.fecha_actualizacion = now_ac
                        c.usuario_actualizacion = request.user.username
                        c.save()
                except:
                    pass

            messages.success(request, u"El cliente ha sido eliminado exitosamente")
            return HttpResponseRedirect(reverse("clientes"))

    except:
        messages.error(request, u"Existió un error al eliminar al cliente")
        return HttpResponseRedirect(reverse("clientes"))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def cambiar_estado_cliente(request, id):
    try:
        cliente = Cliente.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if cliente.status == 1:
            cliente.status = 2
            cliente.usuario_actualizacion = request.user.username
            cliente.fecha_actualizacion = now_ac

            direcciones = Cliente_Direccion.objects.filter(cliente=cliente)
            for d in direcciones:
                d.status = 2
                d.usuario_actualizacion = request.user.username
                d.fecha_actualizacion = now_ac
                d.save()

            try:
                contactos = Contacto.objects.filter(status=1).filter(tipo=1, cliente_proveedor_id=cliente.id)
                for c in contactos:
                    c.status = 2
                    c.usuario_actualizacion = request.user.username
                    c.fecha_actualizacion = now_ac
                    c.save()
            except:
                pass

            messages.success(request, u'Se inactivó al cliente : "'+unicode(cliente.razon_social) + u'" exitosamente')

        elif cliente.status == 2:
            cliente.status = 1
            cliente.usuario_actualizacion = request.user.username
            cliente.fecha_actualizacion = now_ac

            direcciones = Cliente_Direccion.objects.filter(cliente=cliente)
            for d in direcciones:
                d.status = 1
                d.usuario_actualizacion = request.user.username
                d.fecha_actualizacion = now_ac
                d.save()

            try:
                contactos = Contacto.objects.filter(status=1).filter(tipo=1, cliente_proveedor_id=cliente.id)
                for c in contactos:
                    c.status = 1
                    c.usuario_actualizacion = request.user.username
                    c.fecha_actualizacion = now_ac
                    c.save()
            except:
                pass
            messages.success(request, u'Se activó al cliente : "'+unicode(cliente.razon_social) + u'" exitosamente')
        cliente.save()
    except:
        messages.error(request, u"Existieron problemas al cambiar el estado al cliente")
    return HttpResponseRedirect(reverse("clientes"))

