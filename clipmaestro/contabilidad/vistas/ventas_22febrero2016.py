#! /usr/bin/python
# -*- coding: UTF-8-*-
from datetime import timedelta
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from contabilidad.formularios.ClienteForm import *
from contabilidad.formularios.DireccionesForm import *
from contabilidad.formularios.VentaForm import *
from contabilidad.formularios.FormGuiaRemision import *
from contabilidad.formularios.ProvisionesForm import *
from django.forms.util import ErrorList
from librerias.funciones.paginacion import *
from librerias.funciones.validacion_rucs import *
from django.core.paginator import *
from django.template import RequestContext
from django.conf import settings
import os
import time
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from django import template
from contabilidad.funciones.ventas_func import *
from librerias.funciones.funciones_vistas import *
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from reportes.formularios.reportes_form import *
from librerias.funciones.comprobantes_electronicos import *
from contabilidad.formularios.VigenciaDocumentoEmpresa_Form import *
from librerias.funciones.permisos import *
from io import BytesIO
from librerias.funciones.to_word import *
register = template.Library()
IVA = 0.12

#########################################
# Variables para manejar el titulo      #
# del template de ventas                #
# 1 ventas normales                     #
# 2 anular y generar                    #
# 3 copiar venta                        #
# 4 Uso de provisión                    #
# nombre de la variable = "tipo"        #
#########################################

#####################################################
#                   NOTA                            #
#                                                   #
# 1.- La función controla_stock()                   #
# indica la forma de realizar el proceso de la      #
# venta.                                            #
#                                                   #
# 2.- La función 'emite_docs_electronicos()'        #
#  indica si la venta realiza factura               #
#  electrónica.                                     #
#                                                   #
#####################################################

######### ATENCIÖN: REVISAR GUIA DE REMISION CORREGIR FUNCIONES

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def lista_ventas(request):
    '''
    Vista que lista todas las transacciones de ventas
    :param request:
    :return:
    '''
    buscador = FormBuscadoresVenta(request.GET)
    retencion_cabecera = RetencionVentaCabForm()
    retencion_formset = formset_factory(RetencionVentaForm, extra=1)
    retencion = retencion_formset(prefix="detalle_ret")
    venta = busqueda_ventas_reloaded(buscador)
    form_anular_factura = AnularVentaFacturaForm()
    form_email_factura = FormEnvioCorreoFactura()
    paginacion = Paginator(venta, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)
    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Ventas/lista_ventas.html',
                              {"objetos": lista_cabecera, "total_paginas": total_paginas,
                               "retencion_cabecera": retencion_cabecera,
                               "retencion": retencion,
                               "arreglo_paginado": lista,
                               "form_anular_factura": form_anular_factura,
                               "form_correo": form_email_factura,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               "numero": numero, "request": request, "buscador": buscador}, context_instance=RequestContext(request))

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def agregar_ventas(request):
    '''
    Vista que realiza agregar la venta al sistema
    :param request:
    :return:
    '''
    ####################################################################################################################
    #                                                VARIABLES                                                         #
    ####################################################################################################################
    tipo = 1                                        # Tipo que me indica si es el html : agregar - copiar - A/G
    now = datetime.datetime.now()                   # Obtengo la fecha actual
    bandera = 0                                     # Validaciones Generales de la transacción venta
    controlador = 0                                 # Variable que me indica que tipo de venta es con o sin descuento
    tipo_venta = 1                                  # Parametro para indicar el item viende desde guia remisión
    flag_post_venta = 0                             # Parametro para indicar el metodo POST
    flag_esta_cobrado = 0                           # Parametro para asignar que se cobro al mismo momento en
                                                    # que se realiza la venta
    flag_es_stock = 0                               # Parametro para indicar si es  venta controlando stock o no
    ####################################################################################################################

    ####################################################################################################################
    #                                                FORMULARIOS                                                       #
    ####################################################################################################################

    now = datetime.datetime.now()

    try:
        secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=2)  # Secuencia de Compras
        fecha_comp = secuencia.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        fecha_comp = now.strftime("%Y-%m-%d")

    formulario = FormVentasCabecera(initial={"fecha": fecha_comp})
    form_cliente = FormClientes()
    form_direccion = FormDireccion()
    formulario_guia = FormGuiaRemision()
    detalle_form_test = formset_factory(FormDetalleVentaTest, extra=1, formset=BaseDetalleVentaFormSet)
    formset_detalle_test = detalle_form_test(prefix="detalle_venta_test")
    detalle_form_test_stock = formset_factory(FormDetalleVentaStock, extra=1, formset=BaseDetalleVentaFormSet)
    formset_detalle_test_stock = detalle_form_test_stock(prefix="detalle_venta_stock")
    ####################################################################################################################
    try:
        tipo_c = TipoComprobante.objects.get(id=2)
        empresa_parametro = get_parametros_empresa()
        formulario_guia = FormGuiaRemision(initial=instancia_form_guia_remision())

        if controla_stock():
            flag_es_stock = 1
            flag_esta_cobrado = 1
        else:
            flag_es_stock = 0

        if request.method == "POST":
            cabecera_venta = Venta_Cabecera()
            formulario = FormVentasCabecera(request.POST)
            formset_detalle_test = detalle_form_test(request.POST, request.FILES, prefix="detalle_venta_test")
            formset_detalle_test_stock = detalle_form_test_stock(request.POST, request.FILES, prefix="detalle_venta_stock")
            formulario_guia = FormGuiaRemision(request.POST)
            flag_post_venta = 1
            subtotales = 0.0
            subtotales_cero = 0.0
            iva_valor = 0.0
            ice_valor = 0.0
            resultado = 0.0
            descuento_iva = 0.0
            descuento_cero = 0.0
            monto_descuento = 0.0
            monto_descuento12 = 0.0
            monto_descuento0 = 0.0
            subtotal_simple = 0.0
            subtotal_simple_cero = 0.0

            if formulario.is_valid():
                #validar fecha de cierre
                if esta_periodo_contable(formulario.getFecha()):
                    if flag_es_stock:
                        if formset_detalle_test_stock.is_valid():   # Formset2
                            (subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor,
                             resultado, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador) \
                                = CalculaTotales(formulario, formset_detalle_test_stock)
                        else:
                            bandera += 1
                            messages.error(request, u"Error al ingresar la información del detalle de la venta")
                    else:

                        if formset_detalle_test.is_valid():     # Formset1
                            (subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor,
                             resultado, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador) \
                                = CalculaTotales(formulario, formset_detalle_test)
                        else:
                            bandera += 1
                            messages.error(request, u"Error al ingresar la información del detalle de la venta")

                    bandera += GuardarVentasCabecera(cabecera_venta, formulario, 0, 0, subtotal_simple, subtotal_simple_cero,
                                                    subtotales, subtotales_cero, iva_valor, ice_valor, descuento_iva,
                                                    descuento_cero, monto_descuento12, monto_descuento0,
                                                    tipo, now, controlador, request)

                    #############################################
                    #  Se valida la fecha de cierre de la       #
                    #               empresa                     #
                    #############################################
                    if not esta_periodo_contable(cabecera_venta.fecha):
                        bandera += 1
                        messages.error(request, u"Error la fecha de emisión es incorrecta, "
                                                u"por favor revise el periodo contable")

                    else:

                        if bandera == 0:
                            ##############################################################
                            #        Guardar Comprobante Contable Cabecera de la Venta   #
                            ##############################################################
                            cabecera_comprobante = Cabecera_Comp_Contable()
                            GuardarMayor(cabecera_comprobante, cabecera_venta, tipo_c,  now, request)
                            ################################################################################################
                            #                                 Guardar Cuentas Por Cobrar                                   #
                            ################################################################################################
                            cuentas_cobrar = Cuentas_por_Cobrar()
                            GuardarCuentasPorCobrar(cuentas_cobrar, cabecera_venta, resultado, now, flag_esta_cobrado, request)

                            ##########################################
                            #        Detalle de la Venta             #
                            ##########################################
                            if flag_es_stock == 1:    # Llamado al proceso CONTROLA STOCK si son productos venta al instante
                                (validador, parametro_guia_rem, flag_inventario) = GuardarVentaDetalle(formset_detalle_test_stock, formulario, cabecera_venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now,  controlador, request)

                                if validador == 0:
                                    # Guardar Cuentas Definidas para el Asiento de la Venta
                                    validador_cuentas_asientos = GuardarCuentasAsientoVenta(cabecera_comprobante, cabecera_venta, resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request)

                                    if validador_cuentas_asientos == 0:
                                        # Guardar Cuentas De los Items para el Asiento de la Venta
                                        GuardarCuentasAsientoVentaItems(formset_detalle_test_stock, cabecera_venta, cabecera_comprobante, flag_inventario, controlador, now, request)
                                    else:
                                        bandera += 1
                                else:
                                    bandera += 1

                            else:                    # Proceso sin revisar el stock en los productos

                                (validador, parametro_guia_rem, flag_inventario) = GuardarVentaDetalle(formset_detalle_test, formulario, cabecera_venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now,  controlador, request)

                                if validador == 0:
                                    # Guardar Cuentas Definidas para el Asiento de la Venta
                                    validador_cuentas_asientos = GuardarCuentasAsientoVenta(cabecera_comprobante, cabecera_venta, resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request)

                                    if validador_cuentas_asientos == 0:
                                        # Guardar Cuentas De los Items para el Asiento de la Venta
                                        GuardarCuentasAsientoVentaItems(formset_detalle_test, cabecera_venta, cabecera_comprobante, flag_inventario, controlador, now, request)
                                    else:
                                        bandera += 1
                                else:
                                    bandera += 1

                            ################################################################################################
                            #                                   Guardar Inventario                                         #
                            ################################################################################################
                            if bandera == 0 and flag_inventario == 1:
                                inventario_cabecera = Inventario_Cabecera()
                                cabecera_comprobante_inventario = Cabecera_Comp_Contable()
                                # Cabecera Inventario
                                bandera += GuardarVentaInventarioCabecera(inventario_cabecera, empresa_parametro, cabecera_venta, now,
                                                               request, tipo_venta)
                                # Guardar Cabecera del Comprobante de inventario
                                GuardarMayorInventario(cabecera_comprobante_inventario, inventario_cabecera, cabecera_venta,
                                                       now, request)
                                if bandera == 0:
                                    if flag_es_stock == 1:  # Controla Stock
                                        # Guarda en la tabla de Detalle Inventario
                                        GuardarVentaInventarioDetalle(formset_detalle_test_stock, inventario_cabecera, now,
                                                                      request, tipo_venta)
                                        # Guarda las cuentas del Item en Inventario
                                        GuardarCuentasItemInventario(formset_detalle_test_stock, cabecera_venta,
                                                                     cabecera_comprobante_inventario, now, request, tipo_venta)
                                    else:
                                        # Detalle Inventario
                                        GuardarVentaInventarioDetalle(formset_detalle_test, inventario_cabecera, now, request, tipo_venta)
                                        # Guarda las cuentas del Item en Inventario
                                        GuardarCuentasItemInventario(formset_detalle_test, cabecera_venta,
                                                                     cabecera_comprobante_inventario, now, request, tipo_venta)

                                if parametro_guia_rem == 1:  # Si existe Guia de Remision
                                    cabecera_guia = CabeceraGuiaRemision()
                                    bandera += GuardarCabeceraGuiaRemision(formulario_guia, cabecera_guia, cabecera_venta,
                                                                           now, request)
                                    if bandera == 0:
                                        if flag_es_stock == 1:  # Controla Stock
                                            GuardarDetalleGuiaRemision(formset_detalle_test_stock, cabecera_guia, now,
                                                                       request, tipo_venta)
                                        else:
                                            GuardarDetalleGuiaRemision(formset_detalle_test, cabecera_guia, now, request,
                                                                       tipo_venta)
                                    else:
                                        raise ErrorVentas(u"Existen errores con la información de la Guía de Remisión")

                            if bandera == 0:
                                ########################
                                #       CONTRATO       #
                                ########################
                                if formulario.getContrato() is not None:
                                    Guardar_Contrato_Venta(formulario, cabecera_venta, request, now)

                else:
                    bandera += 1
                    errors = formulario._errors.setdefault("fecha", ErrorList())
                    errors.append(u"Campo Requerido")
                    parametro_empresa = get_parametros_empresa()
                    messages.error(request, u'Por favor verifique el campo Fecha, debe ser mayor a la fecha de cierre: '+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))
            ###############################################################
            # Si no es valido se debe de mostrar los respectivos errores  #
            ###############################################################
            else:

                bandera += 1
                bandera += ValidacionFormsVentas(formulario, controlador)
                lista_errores = "Por favor verifique los siguientes campos: "
                for i in formulario.errors:
                    if i == "cliente":
                        messages.error(request, u'El cliente que ingresó se encuentra inactivo o no existe')
                    lista_errores = lista_errores +(unicode(i)) + ", "
                if formulario.errors:
                    messages.error(request, unicode(lista_errores[0:-2]))

            if bandera > 0:
                transaction.rollback()
                raise ErrorVentas(u"")
            else:
                messages.success(request, u"La Venta con número de comprobante: "+str(cabecera_venta.numero_comprobante)+
                                          u" y con número de documento: "+str(cabecera_venta.num_documento)+u" se ha grabado con éxito")
                # Redireccionando la Venta
                return HttpResponseRedirect(redireccionar_venta(request, cabecera_venta))


    except ErrorVentas, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    f_actual = datetime.datetime.now().strftime("%Y-%m-%d")
    f_comp = fecha_comp.strftime("%Y-%m-%d")

    fc = str(f_actual)
    fn = str(f_actual)
    try:
        fc = str(f_comp)
    except:
        pass

    return render_to_response('Ventas/agregar_venta.html', {"formulario_cabecera": formulario,
                                                            "dia_actual": now.strftime('%Y-%m-%d'),
                                                            "formulario_detalle_test": formset_detalle_test,
                                                            "formset_detalle_test_stock": formset_detalle_test_stock,
                                                            "tipo": tipo, "formulario_guia": formulario_guia,
                                                            "form_cliente": form_cliente,
                                                            "form_direccion": form_direccion,
                                                            "flag_post_venta": flag_post_venta,
                                                            "flag_es_stock": flag_es_stock,
                                                            "fn" : fn,
                                                            "fc" : fc,
                                                            "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def copia_ventas(request, id):
    '''
    Vista Copiar General que llama a las dos funciones de ventas
    dependiendo del parámetro de la empresa para elegir
    copiar ventas de producto - servicio y las de sólo productos
    :param request:
    :param id:
    :return:
    '''
    ####################################################################################################################
    #                                                VARIABLES                                                         #
    ####################################################################################################################
    tipo = 3                                        # Tipo que me indica si es el html : agregar - copiar - A/G
    now = datetime.datetime.now()                   # Obtengo la fecha actual
    bandera = 0                                     # Validaciones Generales de la transacción venta
    controlador = 0                                 # Variable que me indica que tipo de venta es con o sin descuento
    tipo_venta = 1                                  # Parametro para indicar el item viende desde guia remisión
    flag_post_venta = 0                             # Parametro para indicar el metodo POST
    flag_esta_cobrado = 0                           # Parametro para asignar que se cobro al mismo momento en
                                                    # que se realiza la venta
    ####################################################################################################################

    ####################################################################################################################
    #                                                OBJETOS                                                           #
    ####################################################################################################################
    tipo_c = TipoComprobante.objects.get(id=2)
    venta_cabecera = Venta_Cabecera.objects.get(id=id)
    empresa_parametro = get_parametros_empresa()
    ####################################################################################################################
    # Formularios
    form_cliente = FormClientes()
    form_direccion = FormDireccion()

    # Parametro para indicar si es  venta controlando stock o no, eso me determina que formset utilizar
    if controla_stock():
        flag_es_stock = 1
        flag_esta_cobrado = 1
    else:
        flag_es_stock = 0

    try:
        formulario_remision = FormGuiaRemision(initial=instancia_form_guia_remision())
        formulario = FormVentasCabecera(initial=instancia_form_cabecera_venta(venta_cabecera, tipo))
        detalle_form_test = formset_factory(FormDetalleVentaTest, extra=0, formset=BaseDetalleVentaFormSet)
        formset_detalle_test = detalle_form_test(initial=instancia_form_set_detalle_venta(venta_cabecera), prefix="detalle_venta_test")
        detalle_form_test_stock = formset_factory(FormDetalleVentaStock, extra=0, formset=BaseDetalleVentaFormSet)
        formset_detalle_test_stock = detalle_form_test_stock(initial=instancia_form_set_detalle_venta_2(venta_cabecera), prefix="detalle_venta_stock")
    except:
        messages.error(request, u"Existe un error al obtener la informacion de la venta")
        return HttpResponseRedirect(reverse("lista_ventas"))

    try:
        if request.method == "POST":
            cabecera_venta = Venta_Cabecera()
            formulario = FormVentasCabecera(request.POST)
            formset_detalle_test = detalle_form_test(request.POST, request.FILES, prefix="detalle_venta_test")
            formset_detalle_test_stock = detalle_form_test_stock(request.POST, request.FILES, prefix="detalle_venta_stock")
            formulario_remision = FormGuiaRemision(request.POST)
            flag_post_venta = 1
            subtotales = 0.0
            subtotales_cero = 0.0
            iva_valor = 0.0
            ice_valor = 0.0
            resultado = 0.0
            descuento_iva = 0.0
            descuento_cero = 0.0
            monto_descuento = 0.0
            monto_descuento12 = 0.0
            monto_descuento0 = 0.0
            subtotal_simple = 0.0
            subtotal_simple_cero = 0.0


            if formulario.is_valid():
                if flag_es_stock == 1:
                    if formset_detalle_test_stock.is_valid():  # Fromset2 - venta_test_stock
                        (subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor, resultado, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador) = CalculaTotales(formulario, formset_detalle_test_stock)
                    else:
                        bandera += 1
                        messages.error(request, u"Error al ingersar la información del detalle de la venta")
                else:
                    if formset_detalle_test.is_valid():  # Fromset1 - venta_test
                        (subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor, resultado, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador) = CalculaTotales(formulario, formset_detalle_test)
                    else:
                        bandera += 1
                        messages.error(request, u"Error al ingersar la información del detalle de la venta")

                bandera += GuardarVentasCabecera(cabecera_venta, formulario, 0, 0,  subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, tipo, now, controlador, request)
                cabecera_venta.fecha_actualizacion = now
                cabecera_venta.usuario_actualizacion = request.user.username
                cabecera_venta.save()

                #############################################
                #  Se valida la fecha de cierre de la       #
                #               empresa                     #
                #############################################
                if not esta_periodo_contable(cabecera_venta.fecha):
                    bandera += 1
                    messages.error(request, u"Error la fecha de emisión es incorrecta, "
                                            u"por favor revise el periodo contable")
                else:

                    if bandera == 0:
                        ##################################################
                        #        Guardar Comprobante Contable Cabecera   #
                        ##################################################
                        cabecera_comprobante = Cabecera_Comp_Contable()
                        GuardarMayor(cabecera_comprobante, cabecera_venta, tipo_c,  now, request)
                        ################################################################################################
                        #                                 Guardar Cuentas Por Cobrar                                   #
                        ################################################################################################
                        cuentas_cobrar = Cuentas_por_Cobrar()
                        GuardarCuentasPorCobrar(cuentas_cobrar, cabecera_venta, resultado, now,
                                                flag_esta_cobrado, request)

                        ##########################################
                        #        Detalle de la Venta             #
                        ##########################################
                        if flag_es_stock == 1:

                            (validador, parametro_guia_rem, flag_inventario) = GuardarVentaDetalle(formset_detalle_test_stock, formulario, cabecera_venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now, controlador, request)
                            if validador == 0:
                                GuardarCuentasAsientoVenta(cabecera_comprobante, cabecera_venta,  resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request)
                                GuardarCuentasAsientoVentaItems(formset_detalle_test_stock, cabecera_venta, cabecera_comprobante, flag_inventario, controlador, now, request)
                            else:
                                bandera += 1
                        else:

                            (validador, parametro_guia_rem, flag_inventario) = GuardarVentaDetalle(formset_detalle_test, formulario, cabecera_venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now, controlador, request)
                            if validador == 0:
                                GuardarCuentasAsientoVenta(cabecera_comprobante, cabecera_venta,  resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request)
                                GuardarCuentasAsientoVentaItems(formset_detalle_test, cabecera_venta, cabecera_comprobante, flag_inventario, controlador, now, request)
                            else:
                                bandera += 1

                        ################################################################################################
                        #                                   Guardar Inventario                                         #
                        ################################################################################################
                        if bandera == 0 and flag_inventario == 1:
                            inventario_cabecera = Inventario_Cabecera()
                            cabecera_comprobante_inventario = Cabecera_Comp_Contable()
                            # Cabecera Inventario
                            bandera += GuardarVentaInventarioCabecera(inventario_cabecera, empresa_parametro, cabecera_venta,  now,
                                                           request, tipo_venta)
                            # Cabecera Asiento Contable Inventario
                            GuardarMayorInventario(cabecera_comprobante_inventario, inventario_cabecera, cabecera_venta,
                                                   now, request)

                            if bandera == 0:
                                if flag_es_stock == 1:  # Controla Stock
                                    # Detalle Inventario
                                    GuardarVentaInventarioDetalle(formset_detalle_test_stock, inventario_cabecera, now,
                                                                  request, tipo_venta)
                                    # Guarda las cuentas del Item en Inventario
                                    GuardarCuentasItemInventario(formset_detalle_test_stock, cabecera_venta,
                                                                 cabecera_comprobante_inventario, now, request, tipo_venta)

                                else:
                                    # Detalle Inventario
                                    GuardarVentaInventarioDetalle(formset_detalle_test, inventario_cabecera, now, request,
                                                                  tipo_venta)
                                    # Guarda las cuentas del Item en Inventario
                                    GuardarCuentasItemInventario(formset_detalle_test, cabecera_venta,
                                                                 cabecera_comprobante_inventario, now, request, tipo_venta)

                            if parametro_guia_rem == 1:      # Guia Remision
                                cabecera_guia = CabeceraGuiaRemision()
                                bandera += GuardarCabeceraGuiaRemision(formulario_remision, cabecera_guia, cabecera_venta,
                                                                       now, request)
                                cabecera_guia.fecha_actualizacion = now
                                cabecera_guia.usuario_actualizacion = request.user.username

                                if bandera == 0:
                                    if flag_es_stock == 1:  # Controla Stock
                                        GuardarDetalleGuiaRemision(formset_detalle_test_stock, cabecera_guia, now,
                                                                   request, tipo_venta)
                                    else:
                                        GuardarDetalleGuiaRemision(formset_detalle_test, cabecera_guia, now, request,
                                                                   tipo_venta)
                                else:
                                    raise ErrorVentas(u"Existen errores con la información de la Guía de Remisión")

                        if bandera == 0:
                            # Contrato
                            if formulario.getContrato() is not None:
                                Guardar_Contrato_Venta(formulario, cabecera_venta, request, now)

            ###############################################################
            # Si no es valido se debe de mostrar los respectivos errores  #
            ###############################################################
            else:
                bandera += 1
                bandera += ValidacionFormsVentas(formulario, controlador)
                lista_errores = "Por favor verifique los siguientes campos: "
                for i in formulario.errors:
                    if i == "cliente":
                        messages.error(request, u'El cliente que ingresó se encuentra inactivo o no existe')
                    lista_errores = lista_errores + (unicode(i)) + "      "
                if formulario.errors:
                    messages.error(request, unicode(lista_errores))

            if bandera > 0:
                transaction.rollback()
                raise ErrorVentas(u"")
            else:
                messages.success(request, u"La Venta con número de comprobante: "+str(cabecera_venta.numero_comprobante)+u" y con número de documento: "+str(cabecera_venta.num_documento)+u" se ha grabado con éxito")
                # Redireccionando la Venta
                return HttpResponseRedirect(redireccionar_venta(request, cabecera_venta))

    except ErrorVentas, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    return render_to_response('Ventas/agregar_venta.html', {"formulario_cabecera": formulario, "tipo": tipo,
                                     "id": id, "formulario_detalle_test": formset_detalle_test, "dia_actual": now.strftime("%Y-%m-%d"),
                                     "form_cliente": form_cliente,
                                     "formset_detalle_test_stock": formset_detalle_test_stock,
                                     "form_direccion": form_direccion,
                                     "formulario_guia": formulario_remision,
                                     "flag_post_venta": flag_post_venta,
                                     "flag_es_stock": flag_es_stock,
                                     "request": request}, context_instance=RequestContext(request))


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def anular_generar(request, id):
    '''
    Vista Anular Generar General que llama a las dos funciones de ventas
    dependiendo del parámetro de la empresa para elegir
    anular generar, ventas de producto - servicio y las de sólo productos
    :param request:
    :param id:
    :return:
    '''
    ####################################################################################################################
    #                                                VARIABLES                                                         #
    ####################################################################################################################
    now = datetime.datetime.now().strftime('%Y-%m-%d')
    tipo = 2                                                      # Tipo Anular - Generar
    bandera = 0                                                   # Validaciones Generales
    controlador = 0                                               # Indicador modo venta tipos de descuentos
    tipo_venta = 1                                                # Parametro que indica al realizar la guia de remision
    flag_esta_cobrado = 0
    ####################################################################################################################

    ####################################################################################################################
    #                                                FORMULARIOS                                                       #
    ####################################################################################################################
    form_cliente = FormClientes()
    form_direccion = FormDireccion()
    ####################################################################################################################

    ####################################################################################################################
    #                                                OBJETOS                                                           #
    ####################################################################################################################
    tipo_c = TipoComprobante.objects.get(id=2)
    empresa_parametro = get_parametros_empresa()
    ####################################################################################################################

    # Parametro para indicar si es  venta controlando stock o no
    if controla_stock():
        flag_es_stock = 1
        flag_esta_cobrado = 1
    else:
        flag_es_stock = 0

    try:
        venta_cabecera = Venta_Cabecera.objects.get(id=id)
        formulario_remision = FormGuiaRemision(initial=instancia_form_guia_remision())
        ###########################################################
        #   Validar que tenga registrados bloc de documentos      #
        ###########################################################
        bloc_documentos = VigenciaDocEmpresa.objects.filter(status=1)
        if len(bloc_documentos) == 0:
            messages.error(request, u"Debe tener registrado el/los documentos vigentes "
                                    u"de la empresa para realizar la venta")
            return HttpResponseRedirect(reverse("lista_ventas"))

        # Validar que la venta anterior tenga num_doc para mantener el # documento
        try:
            id_doc = Venta_Cabecera.objects.get(id=id).getDocumentoFactura().id
            secuencia = venta_cabecera.num_documento
        except:
            id_doc = 0
            secuencia = 0

        formulario = FormVentasCabecera(initial=instancia_form_cabecera_venta(venta_cabecera, tipo))
        multiform = formset_factory(FormDetalleVentaTest, extra=0, formset=BaseDetalleVentaFormSet)
        formset_detalle_test = multiform(initial=instancia_form_set_detalle_venta(venta_cabecera), prefix="detalle_venta_test")
        detalle_form_test_stock = formset_factory(FormDetalleVentaStock, extra=0, formset=BaseDetalleVentaFormSet)
        formset_detalle_test_stock = detalle_form_test_stock(initial=instancia_form_set_detalle_venta_2(venta_cabecera), prefix="detalle_venta_stock")
    except:
        messages.error(request, u"Existe un problema al cargar la información de la venta")
        return HttpResponseRedirect(reverse("lista_ventas"))

    try:
        if request.method == "POST":
            cabecera_venta = Venta_Cabecera()
            formulario = FormVentasCabecera(request.POST)
            formset_detalle_test = multiform(request.POST, request.FILES, prefix="detalle_venta_test")
            formset_detalle_test_stock = detalle_form_test_stock(request.POST, request.FILES, prefix="detalle_venta_stock")
            formulario_remision = FormGuiaRemision(request.POST)
            subtotales = 0.0
            subtotales_cero = 0.0
            iva_valor = 0.0
            ice_valor = 0.0
            resultado = 0.0
            descuento_iva = 0.0
            descuento_cero = 0.0
            monto_descuento = 0.0
            monto_descuento12 = 0.0
            monto_descuento0 = 0.0
            subtotal_simple = 0.0
            subtotal_simple_cero = 0.0

            ###########################################
            #          Anula el comprobante           #
            ###########################################
            bandera = AnularVenta(id, now, request, bandera)

            if bandera > 0:
                transaction.rollback()

            if formulario.is_valid():

                if flag_es_stock == 1:

                    if formset_detalle_test_stock.is_valid():
                        (subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor,
                         resultado, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador) \
                            = CalculaTotales(formulario, formset_detalle_test_stock)
                    else:
                        bandera += 1
                        messages.error(request, u"Error al ingersar la información del detalle de la venta")

                else:

                    if formset_detalle_test.is_valid():
                        (subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor,
                         resultado, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador) \
                            = CalculaTotales(formulario, formset_detalle_test)
                    else:
                        bandera += 1
                        messages.error(request, u"Error al ingersar la información del detalle de la venta")

                # Guardar Venta Cabecera
                bandera += GuardarVentasCabecera(cabecera_venta, formulario, id_doc, secuencia, subtotal_simple,
                                                 subtotal_simple_cero, subtotales, subtotales_cero, iva_valor,
                                                 ice_valor, descuento_iva, descuento_cero, monto_descuento12,
                                                 monto_descuento0, tipo, now, controlador, request)

                #############################################
                #  Se valida la fecha de cierre de la       #
                #               empresa                     #
                #############################################
                if not esta_periodo_contable(cabecera_venta.fecha):
                    bandera += 1
                    messages.error(request, u"Error la fecha de emisión es incorrecta, "
                                            u"por favor revise el periodo contable")
                else:

                    if bandera == 0:
                        ##################################################
                        #        Guardar Comprobante Contable Cabecera   #
                        ##################################################
                        cabecera_comprobante = Cabecera_Comp_Contable()
                        GuardarMayor(cabecera_comprobante, cabecera_venta, tipo_c,  now, request)
                        ################################################################################################
                        #                                 Guardar Cuentas Por Cobrar                                   #
                        ################################################################################################
                        cuentas_cobrar = Cuentas_por_Cobrar()
                        GuardarCuentasPorCobrar(cuentas_cobrar, cabecera_venta, resultado, now, flag_esta_cobrado, request)
                        ##########################################
                        #        Detalle de la Venta             #
                        ##########################################
                        if flag_es_stock == 1:
                            (validador, parametro_guia_rem, flag_inventario) = GuardarVentaDetalle(formset_detalle_test_stock, formulario, cabecera_venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now, controlador, request)

                            if validador == 0:
                                GuardarCuentasAsientoVenta(cabecera_comprobante, cabecera_venta,  resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request)
                                GuardarCuentasAsientoVentaItems(formset_detalle_test_stock, cabecera_venta, cabecera_comprobante, flag_inventario, controlador, now, request)
                            else:
                                bandera += 1
                        else:
                            (validador, parametro_guia_rem, flag_inventario) = GuardarVentaDetalle(formset_detalle_test, formulario, cabecera_venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now, controlador, request)

                            if validador == 0:
                                GuardarCuentasAsientoVenta(cabecera_comprobante, cabecera_venta,  resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request)
                                GuardarCuentasAsientoVentaItems(formset_detalle_test, cabecera_venta, cabecera_comprobante, flag_inventario, controlador, now, request)
                            else:
                                bandera += 1

                        ################################################################################################
                        #                                   Guardar Inventario                                         #
                        ################################################################################################
                        if bandera == 0 and flag_inventario == 1:
                            inventario_cabecera = Inventario_Cabecera()
                            cabecera_comprobante_inventario = Cabecera_Comp_Contable()

                            # Cabecera de Inventario
                            bandera += GuardarVentaInventarioCabecera(inventario_cabecera, empresa_parametro, cabecera_venta,
                                                                      now, request, tipo_venta)

                            # Cabecera de Asiento Contable Inventario
                            GuardarMayorInventario(cabecera_comprobante_inventario, inventario_cabecera, cabecera_venta,
                                                   now, request)
                            if bandera == 0:
                                if flag_es_stock == 1:  # Controla Stock
                                    # Detalle Inventario
                                    GuardarVentaInventarioDetalle(formset_detalle_test_stock, inventario_cabecera, now,
                                                                  request, tipo_venta)
                                    # Guarda las cuentas del Item en Inventario
                                    GuardarCuentasItemInventario(formset_detalle_test_stock, cabecera_venta,
                                                                 cabecera_comprobante_inventario, now, request, tipo_venta)
                                else:
                                    # Detalle Inventario
                                    GuardarVentaInventarioDetalle(formset_detalle_test, inventario_cabecera, now, request,
                                                                  tipo_venta)
                                    # Guarda las cuentas del Item en Inventario
                                    GuardarCuentasItemInventario(formset_detalle_test, cabecera_venta,
                                                                 cabecera_comprobante_inventario, now, request, tipo_venta)

                        if bandera == 0 and parametro_guia_rem == 1:
                            cabecera_guia = CabeceraGuiaRemision()
                            bandera += GuardarCabeceraGuiaRemision(formulario_remision, cabecera_guia, cabecera_venta, now, request)
                            cabecera_guia.fecha_actualizacion = now
                            cabecera_guia.usuario_actualizacion = request.user.username

                            if bandera == 0:
                                if flag_es_stock == 1:
                                    GuardarDetalleGuiaRemision(formset_detalle_test_stock, cabecera_guia, now,
                                                               request, tipo_venta)
                                else:
                                    GuardarDetalleGuiaRemision(formset_detalle_test, cabecera_guia, now, request, tipo_venta)
                            else:
                                raise ErrorVentas(u"Existen errores con la información de la Guía de Remisión")

                        if bandera == 0:
                            if formulario.getContrato() is not None:
                                Guardar_Contrato_Venta(formulario, cabecera_venta, request, now)

            ###############################################################
            # Si no es valido se debe de mostrar los respectivos errores  #
            ###############################################################
            else:
                bandera += 1
                bandera += ValidacionFormsVentas(formulario, controlador)
                lista_errores = "Por favor verifique los siguientes campos: "

                for i in formulario.errors:
                    if i == "cliente":
                        messages.error(request, u'El cliente que ingresó se encuentra inactivo o no existe')
                    lista_errores = lista_errores + (unicode(i)) + "      "
                if formulario.errors:
                    messages.error(request, unicode(lista_errores))

            if bandera > 0:
                transaction.rollback()
            else:
                # Redireccioando la venta
                messages.success(request, u"La venta  se ha anulado y generado con éxito")
                return HttpResponseRedirect(redireccionar_venta(request, cabecera_venta))

    except ErrorVentas, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    return render_to_response('Ventas/agregar_venta.html', {"formulario_cabecera": formulario, "tipo": tipo,
                                                                "id": id, "formulario_detalle_test": formset_detalle_test,
                                                                "formset_detalle_test_stock": formset_detalle_test_stock,
                                                                "formulario_guia": formulario_remision,
                                                                "flag_es_stock": flag_es_stock,
                                                                "form_cliente": form_cliente,
                                                                "form_direccion": form_direccion,
                                                                "dia_actual": now}, context_instance=RequestContext(request))

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def provision_venta(request, id):
    '''
    Vista general de provision venta llama a las funciones de agregar
    venta provisionada según el parámetro de la empresa para determinar
    el modelo de la venta si es sólo productos y productos - servicio
    :param request:
    :param id:
    :return:
    '''
    ####################################################################################################################
    #                                                VARIABLES                                                         #
    ####################################################################################################################
    tipo = 4                                                # Tipo que me indica si es el html : agregar - copiar - A/G
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    bandera = 0     # validaciones
    controlador = 0
    tipo_venta = 1
    flag_post_venta = 0                                     # Parametro para indicar el metodo POST
    now = datetime.datetime.now()
    flag_esta_cobrado = 0

    # Formularios
    form_cliente = FormClientes()
    form_direccion = FormDireccion()
    detalle_form_test = formset_factory(FormDetalleVentaTest, extra=1, formset=BaseDetalleVentaFormSet)
    formset_detalle_test = detalle_form_test(prefix="detalle_venta_test")
    detalle_form_test_stock = formset_factory(FormDetalleVentaStock, extra=1, formset=BaseDetalleVentaFormSet)
    formset_detalle_test_stock = detalle_form_test_stock(prefix="detalle_venta_stock")

    ####################################################################################################################
    #                                                OBJETOS                                                           #
    ####################################################################################################################
    empresa_parametro = get_parametros_empresa()        # Obtiene Objeto Empresa Parametro

    #######################################################
    #   Validar que tenga los datos de la empresa llenos  #
    #######################################################
    formulario_guia = FormGuiaRemision(initial=instancia_form_guia_remision())

    try:
        venta = Venta_Cabecera.objects.filter(status=3).get(id=id)
        formulario = FormVentasCabecera(initial=instancia_form_cabecera_venta(venta, tipo))
    except:
        messages.error(request, u"Existe un problema al cargar la venta")
        return HttpResponseRedirect(reverse("lista_ventas"))

    # Parametro para indicar si es  venta controlando stock o no
    if controla_stock():
        flag_es_stock = 1
        flag_esta_cobrado = 1
    else:
        flag_es_stock = 0

    try:
        if request.method == "POST":
            formulario = FormVentasCabecera(request.POST)
            formset_detalle_test = detalle_form_test(request.POST, request.FILES, prefix="detalle_venta_test")
            formset_detalle_test_stock = detalle_form_test_stock(request.POST, request.FILES, prefix="detalle_venta_stock")
            formulario_guia = FormGuiaRemision(request.POST)
            flag_post_venta = 1
            subtotales = 0.0
            subtotales_cero = 0.0
            iva_valor = 0.0
            ice_valor = 0.0
            resultado = 0.0
            descuento_iva = 0.0
            descuento_cero = 0.0
            monto_descuento = 0.0
            monto_descuento12 = 0.0
            monto_descuento0 = 0.0
            subtotal_simple = 0.0
            subtotal_simple_cero = 0.0

            if formulario.is_valid():
                if flag_es_stock == 1:
                    if formset_detalle_test_stock.is_valid():  # Fromset2 - venta_test_stock
                        (subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor, resultado, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador) = CalculaTotales(formulario, formset_detalle_test_stock)
                    else:
                        bandera += 1
                        messages.error(request, u"Error al ingersar la información del detalle de la venta")
                else:

                    if formset_detalle_test.is_valid():  # Fromset1 - venta_test
                        (subtotal_simple, subtotal_simple_cero, subtotales, subtotales_cero, iva_valor, ice_valor, resultado, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador) = CalculaTotales(formulario, formset_detalle_test)
                    else:
                        bandera += 1
                        messages.error(request, u"Error al ingersar la información del detalle de la venta")

                bandera += GuardarVentasCabecera(venta, formulario, 0, 0, subtotal_simple, subtotal_simple_cero,  subtotales, subtotales_cero, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, tipo, dia_actual, controlador, request)

                #############################################
                #  Se valida la fecha de cierre de la       #
                #               empresa                     #
                #############################################
                if not esta_periodo_contable(venta.fecha):
                    bandera += 1
                    messages.error(request, u"Error la fecha de emisión es incorrecta, "
                                            u"por favor revise el periodo contable")
                else:

                    if bandera == 0:
                        ##################################################
                        #        Guardar Comprobante Contable Cabecera   #
                        ##################################################
                        tipo_comprobante = TipoComprobante.objects.get(status=1, id=2)
                        cabecera_comprobante = Cabecera_Comp_Contable()
                        cabecera_comprobante.concepto_comprobante = venta.concepto
                        cabecera_comprobante.fecha = venta.fecha
                        cabecera_comprobante.numero_comprobante = venta.numero_comprobante
                        cabecera_comprobante.tipo_comprobante = tipo_comprobante
                        cabecera_comprobante.fecha_creacion = dia_actual
                        cabecera_comprobante.usuario_creacion = request.user.username
                        cabecera_comprobante.save()

                        ################################################################################################
                        #                                 Guardar Cuentas Por Cobrar                                   #
                        ################################################################################################
                        cuentas_cobrar = Cuentas_por_Cobrar()
                        GuardarCuentasPorCobrar(cuentas_cobrar, venta, resultado, now, flag_esta_cobrado, request)

                        ##########################################
                        #        Detalle de la Venta             #
                        ##########################################
                        if flag_es_stock == 1:    # Llamado al proceso CONTROLA STOCK si son productos
                            (validador, parametro_guia_rem, flag_inventario) = GuardarVentaDetalle(formset_detalle_test_stock, formulario, venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now,  controlador, request)

                            if validador == 0:
                                # Guardar Cuentas Definidas para el Asiento de la Venta
                                validador_cuentas_asientos = GuardarCuentasAsientoVenta(cabecera_comprobante, venta, resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request)

                                if validador_cuentas_asientos == 0:
                                    # Guardar Cuentas De los Items para el Asiento de la Venta
                                    GuardarCuentasAsientoVentaItems(formset_detalle_test_stock, venta, cabecera_comprobante, flag_inventario, controlador, now, request)
                                else:
                                    bandera += 1
                            else:
                                bandera += 1

                        else:                    # Proceso sin revisar el stock en los productos
                            (validador, parametro_guia_rem, flag_inventario) = GuardarVentaDetalle(formset_detalle_test, formulario, venta, subtotales, subtotales_cero, monto_descuento12, monto_descuento0, now,  controlador, request)

                            if validador == 0:
                                # Guardar Cuentas Definidas para el Asiento de la Venta
                                validador_cuentas_asientos = GuardarCuentasAsientoVenta(cabecera_comprobante, venta, resultado, iva_valor, ice_valor, descuento_iva, descuento_cero, monto_descuento12, monto_descuento0, controlador, now, request)

                                if validador_cuentas_asientos == 0:
                                    # Guardar Cuentas De los Items para el Asiento de la Venta
                                    GuardarCuentasAsientoVentaItems(formset_detalle_test, venta, cabecera_comprobante, flag_inventario, controlador, now, request)
                                else:
                                    bandera += 1
                            else:
                                bandera += 1

                        ################################################################################################
                        #                                   Guardar Inventario                                         #
                        ################################################################################################
                        if bandera == 0 and validador == 0:
                            if flag_inventario == 1:
                                inventario_cabecera = Inventario_Cabecera()
                                cabecera_comprobante_inventario = Cabecera_Comp_Contable()

                                # Cabecera Inventario
                                bandera += GuardarVentaInventarioCabecera(inventario_cabecera, empresa_parametro, venta,  dia_actual,
                                                               request, tipo_venta)
                                # Guardar Cabecera del Comprobante de inventario
                                GuardarMayorInventario(cabecera_comprobante_inventario, inventario_cabecera, venta,
                                                       dia_actual, request)

                                if bandera == 0:
                                    if flag_es_stock == 1:  # Controla Stock
                                        # Guarda en la tabla de Detalle Inventario
                                        GuardarVentaInventarioDetalle(formset_detalle_test_stock, inventario_cabecera, now,
                                                                      request, tipo_venta)
                                        # Guarda las cuentas del Item en Inventario
                                        GuardarCuentasItemInventario(formset_detalle_test_stock, venta,
                                                                     cabecera_comprobante_inventario, now, request, tipo_venta)
                                    else:
                                        # Detalle Inventario
                                        GuardarVentaInventarioDetalle(formset_detalle_test, inventario_cabecera, now, request,
                                                                      tipo_venta)
                                        # Guarda las cuentas del Item en Inventario
                                        GuardarCuentasItemInventario(formset_detalle_test, venta,
                                                                     cabecera_comprobante_inventario, now, request, tipo_venta)

                            if parametro_guia_rem == 1:
                                cabecera_guia = CabeceraGuiaRemision()
                                bandera += GuardarCabeceraGuiaRemision(formulario_guia, cabecera_guia, venta, dia_actual, request)

                                if bandera == 0:
                                    if flag_es_stock == 1:  # Controla Stock
                                        GuardarDetalleGuiaRemision(formset_detalle_test_stock, cabecera_guia, now,
                                                                   request, tipo_venta)
                                    else:
                                        GuardarDetalleGuiaRemision(formset_detalle_test, cabecera_guia, now, request,
                                                                   tipo_venta)
                                else:
                                    bandera += 1
                                    messages.error(request, u"Existen errores con la información de la Guía de Remisión")

                        if bandera == 0:
                            ########################
                            #       CONTRATO       #
                            ########################
                            if formulario.getContrato() is not None:
                                Guardar_Contrato_Venta(formulario, venta, request, dia_actual)

            ###############################################################
            # Si no es valido se debe de mostrar los respectivos errores  #
            ###############################################################
            else:
                bandera += 1
                bandera += ValidacionFormsVentas(formulario, controlador)
                lista_errores = u"Por favor verifique los siguientes campos: "
                for i in formulario.errors:
                    if i == "cliente":
                        messages.error(request, u'El cliente que ingresó se encuentra inactivo o no existe')
                    lista_errores = lista_errores + (unicode(i)) + "      "
                if formulario.errors:
                    messages.error(request, unicode(lista_errores))

            if bandera > 0:
                transaction.rollback()
                raise ErrorVentas(u"")
            else:
                messages.success(request, u"La venta se ha grabado con éxito con número de comprobante: "+str(venta.numero_comprobante)+
                                          u" y con número de documento: "+str(venta.num_documento))
                return HttpResponseRedirect(redireccionar_venta(request, venta))

    except ErrorVentas, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    return render_to_response('Ventas/agregar_venta.html', {"formulario_cabecera": formulario, "tipo": tipo, "id": id,
                               "formulario_detalle_test": formset_detalle_test,
                               "formset_detalle_test_stock": formset_detalle_test_stock,
                               "flag_post_venta": flag_post_venta, "formulario_guia": formulario_guia,
                               "form_cliente": form_cliente, "flag_es_stock": flag_es_stock,
                               "form_direccion": form_direccion, "dia_actual": dia_actual, "request": request,
                               "venta": venta}, context_instance=RequestContext(request))

@login_required(login_url='/')
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def detalle_venta(request, id):
    '''
    Vista que presenta la información de la venta
    completa y todas las transacciones adicionales
    realizadas a la misma
    :param request:
    :param id:
    :return:
    '''
    cxc_cobros = None
    cobro = None
    detalle_may_retencion_venta = None
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa_parametro = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    try:
        venta_cabecera = Venta_Cabecera.objects.get(id=id)
        guia_rem = CabeceraGuiaRemision()

        try:
            detalle_contrato = ContratoDetalle.objects.get(modulo_id=id, tipo_comprobante_id=2)
            contrato = ContratoCabecera.objects.get(id=detalle_contrato.contrato_cabecera.id)
        except ContratoDetalle.DoesNotExist, ContratoCabecera.DoesNotExist:
            contrato = None

        try:
            if venta_cabecera.existeGuiaREm():
                guia_rem = CabeceraGuiaRemision.objects.filter(venta_cabecera=venta_cabecera)
        except CabeceraGuiaRemision.DoesNotExist:
            guia_rem = None

        try:
            cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(venta_cabecera=venta_cabecera, status=1)
            tipo_comp = TipoComprobante.objects.get(id=16)

            if CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
                cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1)[0].cobro
                cxc_cobros = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1)

                cabecera_comp_ret = Cabecera_Comp_Contable.objects.get(numero_comprobante=cobro.num_comp)
                detalle_may_retencion_venta = cabecera_comp_ret.getDetalleComp()

        except:
            pass
            cxc_cobros = None
            cobro = None
            detalle_may_retencion_venta = None

        if venta_cabecera.status == 1:
            detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=venta_cabecera, status=1)
            mayor = Cabecera_Comp_Contable.objects.get(numero_comprobante=venta_cabecera.numero_comprobante, status=1)
            detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=mayor, status=1).order_by("dbcr", "plan_cuenta__codigo")

        elif venta_cabecera.status == 2:
            detalle_venta = Venta_Detalle.objects.exclude(status=0).filter(venta_cabecera=venta_cabecera)
            mayor = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=venta_cabecera.numero_comprobante)
            detalle_mayor = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=mayor).order_by("dbcr", "plan_cuenta__codigo")

        else:
            detalle_venta = None
            mayor = None
            detalle_mayor = None


    except:
        messages.error(request, u"Error al cargar el comprobante de la venta")
        transaction.rollback()
        return HttpResponseRedirect(reverse("lista_ventas"))


    return render_to_response('Ventas/detalle_venta.html',
                              {"venta": venta_cabecera, "request": request,
                               "flag_stock": empresa_parametro.stock,
                               "detalles": detalle_venta,
                               "mayor_cab": mayor,
                               "guia_rem": guia_rem,
                               "contrato": contrato,
                               "lista_cxc_cobros": cxc_cobros,
                               "cobro": cobro,
                               "detalle_may_retencion_venta": detalle_may_retencion_venta,
                               "detalle_may": detalle_mayor}, context_instance=RequestContext(request))

@login_required(login_url='/')
@transaction.commit_on_success
def detalle_venta_reportepdf(request, id):
    '''
    Vista que genera el comprobante de la venta
    completo con todas las transacciones en pdf
    :param request:
    :param id:
    :return:
    '''
    guia_rem = None
    cxc_cobros = None
    cobro = None
    detalle_may_retencion_venta = None
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = EmpresaGeneral.objects.get(id=id_empresa)

    try:
        try:
            usuario = request.user.first_name+' '+request.user.last_name
            venta_cabecera = Venta_Cabecera.objects.get(id=id)
            try:
                detalle_contrato = ContratoDetalle.objects.get(modulo_id=id, tipo_comprobante_id=2)
                contrato = ContratoCabecera.objects.get(id=detalle_contrato.contrato_cabecera.id)
            except ContratoDetalle.DoesNotExist, ContratoCabecera.DoesNotExist:
                contrato = None
            try:
                if venta_cabecera.existeGuiaREm():
                    guia_rem = CabeceraGuiaRemision.objects.filter(venta_cabecera=venta_cabecera)
            except CabeceraGuiaRemision.DoesNotExist:
                guia_rem = None

            try:
                cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(venta_cabecera=venta_cabecera, status=1)
                tipo_comp = TipoComprobante.objects.get(id=16)
                if CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
                    cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1)[0].cobro
                    cxc_cobros = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1)
                    cabecera_comp_ret = Cabecera_Comp_Contable.objects.get(numero_comprobante=cobro.num_comp)
                    detalle_may_retencion_venta = cabecera_comp_ret.getDetalleComp()
            except:
                pass
                cxc_cobros = None
                cobro = None
                detalle_may_retencion_venta = None


            if venta_cabecera.status == 1:
                detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=venta_cabecera, status=1)
                mayor = Cabecera_Comp_Contable.objects.get(numero_comprobante=venta_cabecera.numero_comprobante, status=1)
                detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=mayor, status=1).order_by("dbcr", "plan_cuenta__codigo")
            elif venta_cabecera.status == 2:
                detalle_venta = Venta_Detalle.objects.exclude(status=0).filter(venta_cabecera=venta_cabecera)
                mayor = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=venta_cabecera.numero_comprobante)
                detalle_mayor = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=mayor).order_by("dbcr", "plan_cuenta__codigo")
            else:
                detalle_venta = None
                mayor = None
                detalle_mayor = None
                guia_rem = None
        except:
            transaction.rollback()
            return HttpResponseRedirect(reverse("lista_ventas"))

        html = render_to_string('Ventas/venta_detalle_pdf.html',
                                {'pagesize': 'A4', "empresa": empresa, "usuario": usuario, "venta": venta_cabecera,
                                "request": request, "detalles": detalle_venta, "mayor_cab": mayor,
                                "detalle_may": detalle_mayor, "contrato": contrato,
                                "lista_cxc_cobros": cxc_cobros,
                                "cobro": cobro,
                                "detalle_may_retencion_venta": detalle_may_retencion_venta,
                                "guia_rem": guia_rem}, context_instance=RequestContext(request))

        return generar_pdf_get(html)

    except:
        messages.error(request, u"No se encontró el recurso solicitado")
        return HttpResponseRedirect(reverse("lista_ventas"))

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def anular_venta(request, id):
    '''
    Vista que realiza la funcionalida de anular toda la venta
    que se encuentra registrada y todas las transacciones que posee.
    :param request:
    :param id:
    :return:
    '''
    contador = 0
    now = datetime.datetime.now()
    try:
        venta = Venta_Cabecera.objects.get(id=id)
        contador = AnularVenta(id,  now, request, contador)

        if contador == 0:
            messages.success(request, u"La Venta con número comprobante:  "+ str(venta.numero_comprobante)+
                                      u" se ha anulado con éxito")
            return HttpResponseRedirect(reverse("lista_ventas"))
        else:
            transaction.rollback()
    except:
        transaction.rollback()
        messages.error(request, u"Existe un problema al anular la venta")
    return HttpResponseRedirect(reverse("lista_ventas"))

@login_required(login_url='/')
def lista_guias_remision(request, id):
    '''
    Vista que enlista las guías de remisión asociadas a una venta
    :param request:
    :param id:
    :return:
    '''
    buscador = FormBuscadorGuiaRemVenta(request.GET)
    venta_cabecera = Venta_Cabecera.objects.get(id=id)
    guia_remision = CabeceraGuiaRemision.objects.filter(venta_cabecera=venta_cabecera).order_by("-fecha_emi", "num_doc").distinct()

    paginacion = Paginator(guia_remision, 50)
    numero_pagina = request.GET.get("page")
    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)
    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('cm_guia_remision/lista_guia_remision.html',
                              {"objetos": lista_cabecera, "total_paginas": total_paginas,
                               "arreglo_paginado": lista,
                               "venta": venta_cabecera,
                               "numero": numero, "request": request, "buscador": buscador},
                                context_instance=RequestContext(request))


# Vista por revisar
@login_required(login_url='/')
@csrf_exempt
def agregar_guia_rem_venta(request, id):
    fecha_actual = datetime.datetime.now()
    formulario_venta_guia_cabecera = FormCabeceraguiaRemVenta()     # FormGuiaRemisionDesdeLaVenta
    detalle_form_test = formset_factory(FormDetalleVentaTest, extra=1)
    formset_detalle_test = detalle_form_test(prefix="detalle_venta_test_guia")
    form_guia = FormGuiaRemision()  # FormGuiaRemisión
    formulario_doc = VigenciaRetencionEmpresa_Form()
    cursor = connection.cursor()
    flag = 0
    tipo_venta = 2

    try:
        cabecera_venta = Venta_Cabecera.objects.get(id=id, status=1)
        detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=cabecera_venta, status=1).extra(where=["cant_vendida>cant_entregada"])

        formulario_venta_guia_cabecera = FormCabeceraguiaRemVenta(initial={"num_doc": cabecera_venta.num_documento,
                                "cliente": cabecera_venta.cliente.ruc+" - "+cabecera_venta.cliente.razon_social})

        det_form = []
        multiform = formset_factory(FormDetalleGuiaRemVenta, extra=0)

        if detalle_venta:
            for obj in detalle_venta:
                if obj.item.TipoItem() == 1:  #Items que son sólo tipo producto

                    if cabecera_venta.tipo_descuento_combo == 0:
                        det_form.append({"cantidad": obj.cantidad,
                                         "id_detalle_venta": obj.id,
                                         "id_item": obj.item.id,
                                         "item": obj.item.codigo+" - "+obj.item.nombre, "unidad": obj.item.unidad.descripcion,
                                          "cant_entregada1": obj.cant_entregada})

                    if cabecera_venta.tipo_descuento_combo == 1:
                        det_form.append({"cantidad": obj.cantidad,
                                         "id_detalle_venta":obj.id,
                                         "id_item": obj.item.id,
                                         "item": obj.item.codigo+" - "+obj.item.nombre, "unidad": obj.item.unidad.descripcion,
                                         "cant_entregada1": obj.cant_entregada})

                    if cabecera_venta.tipo_descuento_combo == 2:
                        det_form.append({"cantidad": obj.cantidad,
                                         "id_detalle_venta":obj.id,
                                         "id_item": obj.item.id,
                                         "item": obj.item.codigo+" - "+obj.item.nombre, "unidad": obj.item.unidad.descripcion,
                                         "cant_entregada1": obj.cant_entregada})

                    if cabecera_venta.tipo_descuento_combo == 3:
                        det_form.append({"cantidad": obj.cantidad,
                                         "id_detalle_venta":obj.id,
                                         "id_item": obj.item.id,
                                         "item": obj.item.codigo+" - "+obj.item.nombre, "unidad": obj.item.unidad.descripcion,
                                         "cant_entregada1": obj.cant_entregada})

                    if cabecera_venta.tipo_descuento_combo == 4:
                        det_form.append({"cantidad": obj.cantidad,
                                         "id_detalle_venta":obj.id,
                                         "id_item": obj.item.id,
                                         "item": obj.item.codigo+" - "+obj.item.nombre, "unidad": obj.item.unidad.descripcion,
                                         "cant_entregada1": obj.cant_entregada})

        formulario_detalle_venta = multiform(initial=det_form, prefix="detalle_venta_test_guia")

        #######################################################
        #   Validar que tenga los datos de la empresa llenos  #
        #######################################################
        try:
            empresa = Empresa.objects.filter(status=1)[0]
            empresa_general = EmpresaGeneral.objects.get(id=empresa.empresa_general_id)
            form_guia = FormGuiaRemision(initial={
                "ruc_remision": empresa_general.rucci,
                "compania": empresa_general.razon_social,
                "direccion_conductor": empresa_general.direccion,
                "ciudad": Ciudad.objects.get(id=6).id
            })
        except:
            messages.error(request, u"Por favor, llene los datos de la empresa antes de hacer cualquier transacción")
            return HttpResponseRedirect(reverse("configuracion_empresa"))


        if request.method == "POST":
            formulario_venta_guia_cabecera = FormCabeceraguiaRemVenta(request.POST)
            form_guia = FormGuiaRemision(request.POST)
            formset_detalle_test = multiform(request.POST, request.FILES, prefix="detalle_venta_test_guia")

            if formset_detalle_test.is_valid():
                inventario_cabecera = Inventario_Cabecera()
                GuardarVentaInventarioCabecera(inventario_cabecera,  cabecera_venta,  fecha_actual, request, tipo_venta)
                GuardarVentaInventarioDetalle(formset_detalle_test, inventario_cabecera, fecha_actual, request, tipo_venta)

                ################################### Cabecera Asiento Contable Inventario #######################
                cabecera_comprobante_inventario = Cabecera_Comp_Contable()
                GuardarMayorInventario(cabecera_comprobante_inventario, inventario_cabecera, cabecera_venta, fecha_actual, request)

                ### OJO REVISAR
                ##################################### Detalle Inventario #######################################
                GuardarMayorInventarioDetalle(formset_detalle_test, cabecera_comprobante_inventario, fecha_actual, request, tipo_venta)

                cabecera_guia = CabeceraGuiaRemision()
                flag += GuardarCabeceraGuiaRemision(form_guia, cabecera_guia, cabecera_venta, cursor, fecha_actual, request)

                if flag == 0:
                    GuardarDetalleGuiaRemision(formset_detalle_test, cabecera_guia, fecha_actual, request, tipo_venta)
                    UpdateDetalleVenta(formset_detalle_test, fecha_actual, request)

                    messages.success(request, u"Se realizó exitosamente la guía de remisión en la venta")
                    return HttpResponseRedirect(reverse("lista_ventas"))

                else:
                    raise ErrorVentas(u"Existen errores con la información de la Guía de Remisión")

            else:

                messages.error(request, u"Error, al ingresar la cantidad de entrega de la guía de remisión, por favor ingrese una cantidad válida..")


    except ErrorVentas, e:
        transaction.rollback()
        messages.error(request, unicode(e.valor))

    return render_to_response('cm_guia_remision/guia_remision.html', {
                              "formulario_guia_venta": formulario_venta_guia_cabecera,
                              "formulario_detalle_guia_venta": formulario_detalle_venta,
                              "formulario_guia": form_guia,
                              "id": id,
                              "id_cliente": cabecera_venta.cliente.id,
                              "formulario_doc": formulario_doc,
                              "fecha_actual": fecha_actual.strftime("%Y-%m-%d")}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
def anular_guia_rem_venta(request, id):
    cabecera_guia_rem = CabeceraGuiaRemision.objects.get(status=1, id=id)
    cabecera_guia_rem.status = 2
    cabecera_guia_rem.fecha_actualizacion = datetime.datetime.now()
    cabecera_guia_rem.usuario_actualizacion = request.user.username
    cabecera_guia_rem.save()

    venta = Venta_Cabecera.objects.get(id=cabecera_guia_rem.venta_cabecera.id)
    detalles_ventas = Venta_Detalle.objects.filter(venta_cabecera=venta)

    ############################## Changes status guia remision y detalle_guia_remision  ###############################
    lista_detalles = DetalleGuiaRemision.objects.filter(guia_rem_cabecera=cabecera_guia_rem, status=1)
    for l in lista_detalles:
        l.status = 2
        l.usuario_actualizacion = request.user.username
        l.fecha_actualizacion = datetime.datetime.now()
        l.save()
    ####################################################################################################################

    # Changes status cabecera inventario y detalle_inventario
    cabecera_inventario = Inventario_Cabecera.objects.filter(status=1, num_doc=venta.num_documento)  #Lista de inventario_cabecera

    for i in cabecera_inventario:
        i.status = 2
        i.usuario_actualizacion = request.user.username
        i.fecha_actualizacion = datetime.datetime.now()
        i.save()

        lista_detalles_inventario = Inventario_Detalle.objects.filter(status=1, inventario_cabecera=i)
        for v in lista_detalles_inventario:
            v.status = 2
            v.usuario_actualizacion = request.user.username
            v.fecha_actualizacion = datetime.datetime.now()
            v.save()

        # Changes statues Comprobantes Contables
        cabecera_comp_contable = Cabecera_Comp_Contable.objects.get(status=1, numero_comprobante=i.num_comp)
        cabecera_comp_contable.status = 2
        cabecera_comp_contable.usuario_actualizacion = request.user.username
        cabecera_comp_contable.fecha_actualizacion = datetime.datetime.now()
        cabecera_comp_contable.save()

        detalles_comprobantes = Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=cabecera_comp_contable)
        for m in detalles_comprobantes:
            m.status = 2
            m.usuario_actualizacion = request.user.username
            m.fecha_actualizacion = datetime.datetime.now()
            m.save()

    for det in detalles_ventas:
        for lg in lista_detalles:
            # Updates_Detalle_Venta_Cantidad_Entregada
            if det.item.id == lg.item.id:
                det.cant_entregada = det.cant_entregada - lg.cantidad
                det.usuario_actualizacion = request.user.username
                det.fecha_actualizacion = datetime.datetime.now()
                det.save()

    # Updates_Items_Bodega_Cantidad_Actual
        item_bodega = Item_Bodega.objects.filter(status=1).get(item=det.item)
        cantidad_actual_bodega = item_bodega.cantidad_actual
        cantidad_entregada = float(det.cant_entregada)
        cantidad_actual_2 = cantidad_actual_bodega + cantidad_entregada

        item_bodega.cantidad_actual = cantidad_actual_2
        item_bodega.fecha_actualizacion = datetime.datetime.now()
        item_bodega.usuario_actualizacion = request.user.username
        item_bodega.save()

    messages.success(request, u'"La Guía de Remisión: "'+str(cabecera_guia_rem.num_doc)+u'" asociada a la venta: "'+str(venta.numero_comprobante)+
                              u'"  ha sido anulada exitosamente.')

    return HttpResponseRedirect(reverse("lista_ventas"))

@login_required(login_url='/')
@csrf_exempt
def mantenimiento_ventas_inventario(request):
    '''
    Vista que llama a la función de reprocesar venta
    realiza una actualización en inventario detalle
    :param request:
    :param id:
    :return:
    '''
    query = []
    contador = 0
    indicador = 0
    cont = 1
    selected = 0
    buscador = BuscadorReportes(request.GET)

    if buscador.is_valid():
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (buscador.getFechaIni(), buscador.getFechaFinal()):
            query = Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).\
                order_by("fecha", "num_documento")

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        if buscador.is_valid():
            if None not in (buscador.getFechaIni(), buscador.getFechaFinal()):
                fecha_modificar = buscador.getFechaModificar()
                query = Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(buscador.getFechaIni(),
                                                                       buscador.getFechaFinal())).order_by("fecha", "num_documento")

                for cab_vta in query:
                    sel_vta = request.POST.get("venta-"+str(cont))
                    if sel_vta:
                        selected = 1
                        sel_inventario = request.POST.get("inventario-"+str(cont))

                        if fecha_modificar is not None:
                            #proceso A
                            procesoModificaFechaVenta(cab_vta.id, fecha_modificar, request)
                        if sel_inventario is not None:
                            #proceso B
                            contador += reprocesarVentasInventario(request, cab_vta.id)
                        elif fecha_modificar is None and sel_inventario is None:
                            contador += 1
                            messages.error(request, u"Error no ha seleccionado ningún parámetro para "
                                                    u"realizar el proceso")
                    cont += 1

                if selected == 0:
                    contador += 1
                    messages.error(request, u"Debe Seleccionar al menos una venta para continuar "
                                            u"con el proceso")
        if contador > 0:
            transaction.rollback()
        else:
            messages.success(request, u"Se realizó exitosamente el procedimiento")

    return render_to_response('Ventas/mantenimiento_ventas/mantenimiento_ventas_fecha.html',
                              {"buscador": buscador,
                               "objetos": query, "indicador": indicador},
                              context_instance=RequestContext(request))

########################################################################################################################
#                                       VISTAS FACTURA ELECTRÓNICA                                                     #
########################################################################################################################
@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
@csrf_exempt
@transaction.commit_on_success
def enviar_factura_electronica_sri(request, id):
    '''
    Vista que llama a las funciones de
    factura electrónica que realiza el
    proceso según el API de Factora
    :param request:
    :param id:
    :return:
    '''
    try:
        venta = Venta_Cabecera.objects.filter(status=1).get(id=id)
    except Venta_Cabecera.DoesNotExist:
        messages.error(request, u"Seleccione la venta con estado activo para realizar la facturación electrónica")
        venta = None

    if venta is not None:
        if venta.estado_firma == 4:
            messages.success(request, u"La venta ya está autorizada por el SRI")
            return HttpResponseRedirect(reverse("lista_ventas"))
        else:
            print 'el estado de esta venta es'
            print str(venta.estado_firma)
            print 'xxxxxxxxxxx en la facturaaa electronicaaaa :o'
            status_envio = envio_factura(venta)
            if status_envio[0] == 1:  # Se envió con éxito la factura al SRI
                messages.success(request, u"Se ha realizado exitosamente la factura electrónica")
                return HttpResponseRedirect(reverse("lista_ventas"))
            else:
                error = status_envio[1]
                messages.error(request, unicode(error))
                return HttpResponseRedirect(reverse("lista_ventas"))
    else:
        print 'se cae por no exiasitr ventas :o'
        messages.error(request, u"Error la venta que hizo referencia no puede ser enviada para que se realice la  "
                                u"facturación electrónica"
                                u", por favor verifique")
        return HttpResponseRedirect(reverse("lista_ventas"))

@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
@csrf_exempt
def enviar_correo_factura(request):
    '''
    Vista Ajax que es llamada para enviar la información
    de la factua electrónica al cliente mediante
    su correo electrónico
    :param request:
    :param id:
    :return:
    '''


    form_correo = FormEnvioCorreoFactura(request.POST)
    form_correo.is_valid()
    id = form_correo.get_id()
    print 'la funcion del envi de correo'
    venta = Venta_Cabecera.objects.get(id=id)
    if request.method == "POST":
        if venta.status == 1:
            if venta.estado_firma >= 4:

                if enviar_correo_doc_electronico_factura(request, venta):
                    respuesta = {"status": 1, "mensaje": ""}
                    resultado = json.dumps(respuesta)
                    return HttpResponse(resultado, mimetype='application/json')
                else:
                    messages.error(request, u"La Factura no se encuentra todavía autorizada por el SRI")
        else:
            messages.error(request, u"La venta que seleccionó se encuentra anulada")
        respuesta = {"status": 2, "mensaje": ""}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')
########################################################################################################################

########################################################################################################################
#                                                   VISTA DE AJAX                                                      #
########################################################################################################################
@csrf_exempt
@login_required(login_url='/')
def ajax_seleccionar(request):
    '''
    Vista AJAX que recibe el id del Documento Vigente y presenta
    la información requerida del documento
    :param request:
    :return:
    '''
    id = request.POST.get("id", "")
    try:
        # FACTURA
        documento_vigente = VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=datetime.datetime.now()).\
            filter(documento_sri__id=1).get(id=id)

        response_data = {"id": documento_vigente.id, "serie": documento_vigente.serie,
                         "vencimiento": documento_vigente.fecha_vencimiento.strftime('%Y-%m-%d'),
                         "autorizacion": documento_vigente.autorizacion, "secuencia": str(documento_vigente.sec_actual)}
    except:
        pass
        response_data = {"respuesta": 'danger', "mensaje": 'Existió un error'}
    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")

@csrf_exempt
@login_required(login_url='/')
def ajax_seleccionar_documentos(request):
    '''
    Vista AJAX que permite mediante la fecha carga los documentos vigentes
    :param request:
    :return:
    '''
    #id_documento = request.POST.get("id", "")
    q = request.POST.get("fecha", "")
    respuesta = []
    try:
        fecha_split = str(q).split("-")
        fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))

        # FACTURA
        vigencia = VigenciaDocEmpresa.objects.filter(status=1).\
                            filter(fecha_emi__lte=fecha.strftime("%Y-%m-%d"),
                                   fecha_vencimiento__gte=fecha.strftime("%Y-%m-%d"), documento_sri__id=1,
                                   sec_actual__lt=F("sec_final")).order_by("serie")

        if len(vigencia) == 0:
            respuesta.append({"status": 0, "id": 0, "descripcion": u"No se encuentra definido el "
                                                                   u"documento con su vigencia, favor ingrese"})
        else:
            for obj in vigencia:
                respuesta.append({"status": 1, "id": obj.id, "serie": obj.serie})

    except:
        respuesta.append({"status": -1, "id": -1, "descripcion": u"No se encuentra definido el documento "
                                                                 u"con su vigencia, favor ingrese"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@login_required(login_url='/')
@csrf_exempt
def ajax_seleccionar_direccion(request):
    '''
    Vista AJAX consulta según el cliente para seleccionar
    la dirección correspondiente
    :param request:
    :return:
    '''
    id_cliente = request.POST.get("id", "")
    respuesta = []
    try:
        cliente = Cliente.objects.get(id=id_cliente)
        direcciones = Cliente_Direccion.objects.filter(status=1).filter(cliente=cliente)

        if len(direcciones) == 0:
            respuesta.append({"id": 0, "status": 0,
                              "descripcion": u"No se encuentra definidas las direcciones del cliente"})
        else:
            for obj in direcciones:
                respuesta.append({"status": 1, "id": obj.id,
                                  "descripcion": unicode(obj.descripcion), "direccion1": unicode(obj.direccion1),
                                  "direccion2": unicode(obj.direccion2), "atencion": obj.atencion,
                                  "telefono": obj.telefono})

    except (Cliente_Direccion.DoesNotExist, ValueError):
        respuesta.append({"status": -1, "id": -1, "descripcion": u"No se encuentra definidas las "
                                                                 u"direcciones para el cliente"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@login_required(login_url='/')
@csrf_exempt
def buscar_cliente_item_venta(request):
    '''
    Vista AJAX que realiza la consulta de los items en la venta
    :param request:
    :return:
    '''
    id_item = request.POST.get("id_item", "")
    respuesta = []
    try:
        items = Item.objects.filter(id=int(id_item), status=1)
        if len(items) == 0:
            respuesta.append({"id": 0, "descripcion": u"No se encuentra definido el item"})

        else:

            for obj in items:
                if obj.porc_ice is None:
                    ice = 0.0
                else:
                    ice = obj.porc_ice

                if obj.iva is None:
                    iva = 0.0
                else:
                    iva = obj.iva

                if obj.precio1 is None:
                    precio = 0.0
                else:
                    precio = obj.precio1
                respuesta.append({"id": obj.id, "nombre": obj.nombre,
                                  "iva": iva, "precio": precio, "ice": ice, "tipo": obj.categoria_item.tipo_item.id})
    except:
        respuesta.append({"id": 0, "descripcion": u"No se encuentra definido el item"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def buscar_item_venta(request):
    id_item = request.POST.get("id_item", "")
    #id_cliente = request.POST.get("cliente", "")
    #cursor = connection.cursor()
    respuesta = []
    try:
        items = Item.objects.filter(id=int(id_item), status=1)

        if len(items) == 0:
            respuesta.append({"id": 0, "descripcion": "No se encuentra definido el item"})
        else:
            for obj in items:
                if obj.porc_ice is None:
                    ice = 0.0
                else:
                    ice = obj.porc_ice

                if obj.iva is None:
                    iva = 0.0
                else:
                    iva = obj.iva
                #try:
                #    cursor.execute("select get_precio("+str(int(obj.id))+", "+str(int(id_cliente))+");")
                #    total_rows = cursor.fetchone()
                #    precio = round(float(total_rows[0]), 2)
                #except:
                if obj.precio1 is None:
                    precio = 0.0
                else:
                    precio = obj.precio1

                respuesta.append({"id": obj.id, "nombre": obj.nombre,
                                  "iva": iva, "precio": precio, "ice": ice, "tipo": obj.TipoItem()})
    except:
        pass
        respuesta.append({"id": 0, "descripcion": "No se encuentra definido el item"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_existe_item_bodega(request):
    respuesta = []
    id = request.POST.get("id_item", "")
    try:
        if controla_stock():
            item = Item.objects.get(status=1, id=id)
            if item.categoria_item.tipo_item.id == 1:
                if Item_Bodega.objects.filter(status=1, item__id=item.id, item__categoria_item__tipo_item__id=1).exists():
                    respuesta.append({"id": id, "status": 1, "mensaje": "Existe en Bodega."})
                    resultado = json.dumps(respuesta)
                    return HttpResponse(resultado, mimetype='application/json')
                else:
                    respuesta.append({"id": id, "status": 0, "item": item.nombre, "mensaje": "El Item no está "
                                                                                             "registrado en bodega."})
                    resultado = json.dumps(respuesta)
                    return HttpResponse(resultado, mimetype='application/json')
            else:
                respuesta.append({"id": id, "status": 2, "mensaje": "Item Servicio."})
                resultado = json.dumps(respuesta)
                return HttpResponse(resultado, mimetype='application/json')
        else:
            respuesta.append({"id": id, "status": 1, "mensaje": ""})
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')
    except:
        respuesta.append({"status": -1, "mensaje": "Error en el servidor."})
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_cantidades_bodega(request):
    '''
    Vista Ajax  que realiza la validación de la cantidad (stock) si es necesario de los productos
     a vender
    :param request:
    :return:
    '''
    respuesta = []
    id = request.POST.get("id_item", "")
    cantidad_venta = request.POST.get("cantidad_venta", "")
    cantidad_entregada = request.POST.get("cantidad_entregada", "")
    item = Item.objects.filter(status=1).get(id=id)

    if item.TipoItem() == 1:  # PRODUCTO
        if Item_Bodega.objects.filter(item=item).exists():
            bodega_tem = Item_Bodega.objects.get(item=item)
            cant_bodega = float(bodega_tem.cantidad_actual)
            if cantidad_entregada != "":
                if float(str(cantidad_entregada)) <= cant_bodega:
                    respuesta.append({"id": id, "status": 2, "cantidad_entregada": cantidad_entregada,
                                      "cantidad_venta": cantidad_venta, "cantidad_bodega": cant_bodega})
                else:
                    respuesta.append({"id": id, "status": 3, "cantidad_entregada": bodega_tem.cantidad_actual,
                                      "cantidad_venta": cantidad_venta, "cantidad_bodega": cant_bodega})
            else:

                if cantidad_venta != "":
                    if float(str(cantidad_venta)) <= cant_bodega:
                        respuesta.append({"id": id, "status": 1, "cantidad_entregada": cantidad_entregada,
                                          "cantidad_venta": cantidad_venta, "cantidad_bodega": cant_bodega})
                    else:
                        respuesta.append({"id": id, "status": 4, "cantidad_entregada": bodega_tem.cantidad_actual,
                                          "cantidad_venta": cantidad_venta, "cantidad_bodega": cant_bodega})

            if cantidad_entregada == "":
                respuesta.append({"status": -2, "cantidad_bodega": cant_bodega})

            if cantidad_venta == "":
                respuesta.append({"status": -1, "cantidad_bodega": cant_bodega})
        else:
            respuesta.append({"status": -3, "mensaje": u"El item no se encuentra registrado en bodega"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_clientes(request):
    respuesta = []

    for obj in Cliente.objects.filter(status=1).only("id", "ruc", "razon_social", "ciudad", "direccion"):
        if obj.ciudad is not None:
            ciudad = obj.ciudad.descripcion
        else:
            ciudad = ""
        respuesta.append({"id": obj.id, "ruc": obj.ruc, "razon_social": obj.razon_social, "ciudad": ciudad, "direccion": obj.direccion})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_info_cliente(request):
    id = request.POST.get("id", "")
    respuesta = []
    cliente = Cliente.objects.get(status=1, id=id)
    print "cliente: ", cliente.razon_social
    respuesta.append({"id": cliente.id, "ruc": cliente.ruc, "razon_social": cliente.razon_social,
                      "ciudad": cliente.ciudad_id, "direccion": cliente.direccion})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
def get_retencion_venta(request):
    id = request.POST.get("id", "")
    try:
        venta = Venta_Cabecera.objects.get(id=id)
        cta_por_cobrar = Cuentas_por_Cobrar.objects.get(venta_cabecera=venta)
        tipo_comp = TipoComprobante.objects.get(id=16)
        respuesta = []
        for obj in CXC_cobro.objects.filter(cuentas_x_cobrar=cta_por_cobrar, cobro__tipo_comprobante=tipo_comp):
            respuesta.append({"cuenta": obj.plan_cuenta.id, "valor_retenido": obj.monto})
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')
    except:
        pass

@login_required(login_url='/')
@csrf_exempt
def tiene_retencion_venta(request):
    id = request.POST.get("id", "")
    try:
        venta = Venta_Cabecera.objects.get(id=id, status=1)
        cta_por_cobrar = Cuentas_por_Cobrar.objects.get(venta_cabecera=venta)
        tipo_comp = TipoComprobante.objects.get(id=16)
        if CXC_cobro.objects.filter(cuentas_x_cobrar=cta_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
            respuesta = {"status": 1}
        else:
            respuesta = {"status": 0}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')

    except Venta_Cabecera.DoesNotExist:
        respuesta = {"status": 2}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')

    except Cuentas_por_Cobrar.DoesNotExist:
        respuesta = {"status": 3}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def tiene_guia_rem(request):
    id = request.POST.get("id", "")
    try:
        venta = Venta_Cabecera.objects.get(id=id, status=1)

        if CabeceraGuiaRemision.objects.filter(venta_cabecera=venta, status=1).exists():
            respuesta = {"status": 1}
        else:
            respuesta = {"status": 0}
        resultado = json.dumps(respuesta)

        return HttpResponse(resultado, mimetype='application/json')

    except Venta_Cabecera.DoesNotExist:
        respuesta = {"status": 2}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def existe_guia_rem_venta(request):
    id = request.POST.get("id", "")
    try:
        cabecera_guia = CabeceraGuiaRemision.objects.get(id=id, status=1)

        try:
            if Venta_Cabecera.objects.filter(id=cabecera_guia.venta_cabecera.id, status=1).exists():
                respuesta = {"status": 1}
                resultado = json.dumps(respuesta)
                return HttpResponse(resultado, mimetype='application/json')
        except:
            pass
            respuesta = {"status": 0}
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')

    except CabeceraGuiaRemision.DoesNotExist:
        respuesta = {"status": 2}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def ajax_seleccion_contratos_cliente(request):
    '''
    Vista Ajax que realiza la consulta de los contratos que tenga asociado
    a un cliente
    :param request:
    :return:
    '''
    id_cliente = request.POST.get("id", "")
    respuesta = []

    try:
        cliente = Cliente.objects.get(id=id_cliente)
        contratos = ContratoCabecera.objects.filter(status=1).filter(cliente=cliente).order_by("num_cont").distinct()

        if len(contratos) == 0:
            respuesta.append({"status": 0, "id": 0,
                              "descripcion": "No se encuentra  registrado contratos  al cliente"})
        else:
            for obj in contratos:
                respuesta.append({"status": 1, "id": obj.id, "num_cont": obj.num_cont,
                                  "cliente": obj.cliente.razon_social, "descripcion": u"Consulta exitosa"})
    except:
        pass
        respuesta.append({"status": -1, "descripcion": u"No se encuentra definidas las direcciones del cliente"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@login_required(login_url="/")
@csrf_exempt
def get_ultima_factura_usada(request):
    """
    Retorna el número de la última factura usada
    :param request: el cual tiene el id del bloque de retenciones
    :return:
    """
    tipo_comp = TipoComprobante.objects.get(status=1, id=2)

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
        secuencia = secuencia_tipo.secuencia
        id_vigencia = request.POST.get("id_vigencia")
        respuesta = {"status": 1, "num_doc": VigenciaDocEmpresa.objects.get(id=id_vigencia).sec_actual,
                     "num_comp": secuencia}
        resultado = json.dumps(respuesta)
        transaction.rollback()
    except SecuenciaTipoComprobante.DoesNotExist:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)
        transaction.rollback()
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url="/")
@csrf_exempt
def get_ultima_comprobante_venta_usado(request):
    """
    Retorna el número de la última factura usada
    :param request: el cual tiene el id del bloque de retenciones
    :return:
    """
    tipo_comp = TipoComprobante.objects.get(status=1, id=2)

    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
        secuencia = secuencia_tipo.secuencia
        fecha_actual = secuencia_tipo.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        secuencia = 0
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")

    try:

        respuesta = {"status": 1, "num_comp": str(secuencia), "fecha_comp": str(fecha_actual)}
        resultado = json.dumps(respuesta)
    except:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)
        transaction.rollback()

    return HttpResponse(resultado, mimetype='application/json')

@csrf_exempt
@login_required(login_url='/')
def valida_documentos_vigentes(request):
    q = request.POST.get("fecha", "")
    respuesta = []
    #try:
    fecha_split = str(q).split("-")
    fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))

    # FACTURA
    vigencia = VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__gte=q,
                               fecha_vencimiento__lte=q, documento_sri__id=1,
                               sec_actual__lt=F("sec_final")).order_by("serie")

    for v in VigenciaDocEmpresa.objects.filter(status=1, documento_sri__id=1, sec_actual__lt=F("sec_final")):
        print v.documento_sri.descripcion_documento
        print "FECHA_EMI: ", v.fecha_emi
        print "FECHA_VENCIMIENTO: ", v.fecha_vencimiento
        print "FECHA_ ACTUAL: ", v.fecha_actual

    if len(vigencia) == 0:
        respuesta.append({"status": 0, "id": 0, "descripcion": u"No se encuentra definido"
                                                               u" el documento con su vigencia, favor ingrese"})
    else:
        for obj in vigencia:
            respuesta.append({"status": 1, "id": obj.id, "serie": obj.serie})

    #except VigenciaDocEmpresa.DoesNotExist:
    #    respuesta.append({"status": -1, "id": -1, "descripcion": u"No se encuentra ingresado la "
    #                                                             u"factura vigtente, favor ingrese"})


    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def rectificar_venta(request):
    '''
     Función Ajax que realiza la anulación de la venta directamente
     cuando tengamos una factura físicamente anulada (dañada)
    :param request:
    :return:
    '''
    now = datetime.datetime.now()
    form_anular = AnularVentaFacturaForm(request.POST)
    tipo_comp = TipoComprobante.objects.get(id=2)  # Tipo de comprobante de venta
    try:
        if form_anular.is_valid():
            id_vig = form_anular.getDocumento()
            vigencia = VigenciaDocEmpresa.objects.get(id=id_vig)

            if form_anular.getFechaREg() > datetime.datetime.now().date():
                respuesta = {"status": 3, "mensaje": u"La fecha del registro no puede ser mayor a la fecha actual"}
                resultado = json.dumps(respuesta)
                transaction.rollback()

            elif form_anular.getFechaREg() < vigencia.fecha_actual:
                respuesta = {"status": 4, "mensaje": u"La fecha del registro no puede ser menor a la "
                                                     u"fecha de vigencia de documento: "+str(vigencia.fecha_actual)}
                resultado = json.dumps(respuesta)
                transaction.rollback()
            else:

                try:
                    secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comp)
                    if form_anular.getFechaREg() < secuencia_tipo.fecha_actual:
                        respuesta = {"status": 4, "mensaje": u"La fecha del registro:"
                                                             + form_anular.getFechaREg().strftime("%Y-%m-%d") +
                                                             u" no puede ser menor a la última fecha ingresada de "
                                                             u"facturas la cual es: " +
                                                             u"" + secuencia_tipo.fecha_actual.strftime("%Y-%m-%d")}
                        resultado = json.dumps(respuesta)
                        transaction.rollback()
                        return HttpResponse(resultado, mimetype='application/json')

                except SecuenciaTipoComprobante.DoesNotExist:
                    pass

                num_doc = get_num_doc(vigencia, form_anular.getFechaREg(), request)
                comp_contable = Cabecera_Comp_Contable()
                comp_contable.fecha = form_anular.getFechaREg()
                comp_contable.tipo_comprobante = TipoComprobante.objects.get(id=2)
                comp_contable.numero_comprobante = get_num_comp(comp_contable.tipo_comprobante_id,
                                                                form_anular.getFechaREg(), True, request)

                comp_contable.concepto_comprobante = u"Anulación de Factura"
                comp_contable.fecha_creacion = now
                comp_contable.usuario_creacion = request.user.username
                comp_contable.status = 2
                comp_contable.save()

                venta = Venta_Cabecera()
                venta.fecha = form_anular.getFechaREg()
                venta.concepto = u"Anulación de la Factura #" + \
                                  str(vigencia.serie) + "-" + num_doc

                venta.documento = Documento.objects.get(id=1)
                venta.vigencia_doc_empresa = vigencia
                venta.num_documento = num_doc
                venta.numero_comprobante = comp_contable.numero_comprobante
                venta.fecha_creacion = now
                venta.usuario_creacion = request.user.username
                venta.status = 2
                venta.save()

                respuesta = {"status": 1, "num_doc": num_doc, "num_comp": comp_contable.numero_comprobante}
                resultado = json.dumps(respuesta)
                messages.success(request, u"Se anuló satisfactoriamente la factura #: " +
                                 str(vigencia.serie) + "-" + num_doc)

        else:
            respuesta = {"status": -1, "mensaje":  u"Llene correctamente el formulario"}
            resultado = json.dumps(respuesta)
            messages.success(request, u"Llene correctamente el formulario")

    except:
        respuesta = {"status": 2, "mensaje": u"Existió un error en el servidor, por favor "
                                             u"comuniquese con el administrador"}
        resultado = json.dumps(respuesta)
        transaction.rollback()

    return HttpResponse(resultado, mimetype='application/json')

@transaction.commit_on_success
@login_required(login_url='/')
@csrf_exempt
def agregar_retencion(request):
    '''
    Vista Ajax que permite agregar una retención a la venta seleccionada
    :param request:
    :return:
    '''
    try:
        retencion_form_cab = RetencionVentaCabForm(request.POST)
        retencion_formset = formset_factory(RetencionVentaForm, formset=BaseDetalleRetenFormSet)
        retencion = retencion_formset(request.POST, prefix="detalle_ret")
        venta = Venta_Cabecera.objects.get(id=request.POST.get("id_venta"), status=1)
        numerodocumento = unicode(venta.num_documento)
        print 'numeroooo en ret'
        print numerodocumento
        nombrecliente = unicode(venta.cliente.razon_social)
        print 'nombreee en ret'
        print nombrecliente
        palabrasquemadas = u'Retencion en venta'
        print 'quemadass en ret'
        #print palabrasquemadas
        frasefinal = nombrecliente+', '+palabrasquemadas+': '+numerodocumento
        cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(venta_cabecera=venta, naturaleza=1)
        now = datetime.datetime.now()
        total_retencion = 0.0
        cursor = connection.cursor()
        cabecera_asiento = Cabecera_Comp_Contable()
        tipo_comp = TipoComprobante.objects.get(id=16)
        print 'pensando en agregar la retencion :o'
        if CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
            respuesta = {"status": 3}
            print 'existe un cxc cobro estatus tres'
        else:
            #############################################
            #  Se valida la fecha de cierre de la       #
            #               empresa                     #
            #############################################
            if retencion_form_cab.is_valid() and retencion.is_valid():
                cursor.execute("select periodo_valido(%s);", [retencion_form_cab.getFecha().strftime("%Y-%m-%d")])
                total_rows = cursor.fetchone()
                cursor.close()

                num_comp = get_num_comp(tipo_comp.id, retencion_form_cab.getFecha(), True, request)
                if retencion_form_cab.getFecha() > now.date():
                    transaction.rollback()
                    respuesta = {"status": 2, "mensaje": u"No se puede agregar la retención a esta factura, "
                                                         u"ya que la fecha de la retención es "
                                                         u"mayor a la fecha actual del sistema"}
                #elif not total_rows[0]:
                #    respuesta = {"status": 3, "mensaje": u"No se puede agregar la retención a esta factura con esa fecha de registro, "
                #                                         u"ya que es menor a la fecha del cierre contable"}

                elif venta.fecha > retencion_form_cab.getFecha():
                    transaction.rollback()
                    respuesta = {"status": 2, "mensaje": u"No se puede agregar la retención a esta factura, "
                                                         u"ya que la fecha de la retención es "
                                                         u"menor a la de la fecha del registro de la factura"}
                elif num_comp == -1:
                    transaction.rollback()
                    respuesta = {"status": 2, "mensaje": u"La fecha de la retención es menor a la fecha "
                                                         u"de la última retención, por favor verifique"}
                else:
                    for form in retencion:
                        informacion = form.cleaned_data
                        valor_retenido = float(informacion.get("valor_retenido", 0))
                        total_retencion += valor_retenido

                    cabecera_asiento.concepto_comprobante = u"Retención a la venta al cliente: " + venta.cliente.razon_social + u", valor: " + str(total_retencion)
                    cabecera_asiento.fecha = retencion_form_cab.getFecha().strftime("%Y-%m-%d")
                    cabecera_asiento.numero_comprobante = num_comp

                    cabecera_asiento.tipo_comprobante = tipo_comp

                    cabecera_asiento.usuario_creacion = request.user.username
                    cabecera_asiento.fecha_creacion = now
                    cabecera_asiento.save()
                    if total_retencion > 0.0 and (redondeo(total_retencion, 2) <= redondeo((cuentas_por_cobrar.monto - cuentas_por_cobrar.cobrado), 2)):
                        cobro = Cobro()
                        cobro.cliente = venta.cliente
                        cobro.concepto = unicode(frasefinal)
                        print(unicode(frasefinal))
                        cobro.fecha_pago = retencion_form_cab.getFecha()
                        cobro.fecha_reg = retencion_form_cab.getFecha()
                        cobro.forma_pago = FormaPago.objects.get(id=7)
                        cobro.monto = total_retencion
                        cobro.tipo_comprobante = TipoComprobante.objects.get(id=16)  #  Tipo de comprobante retencion en venta
                        cobro.num_comp = cabecera_asiento.numero_comprobante

                        cobro.usuario_creacion = request.user.username
                        cobro.fecha_creacion = now
                        cobro.save()

                        if retencion.is_valid():
                            for form in retencion:
                                informacion = form.cleaned_data
                                valor_retenido = float(informacion.get("valor_retenido"))
                                cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
                                detalle_asiento = Detalle_Comp_Contable()
                                cxp_cobro = CXC_cobro()
                                cxp_cobro.cobro = cobro
                                cxp_cobro.monto = valor_retenido
                                #cxp_cobro.concepto = u"Retención de venta a la cuenta " + cuenta.descripcion hasta el 12 de enero 2016
                                cxp_cobro.concepto = frasefinal
                                cxp_cobro.cuentas_x_cobrar = cuentas_por_cobrar
                                cxp_cobro.plan_cuenta = cuenta

                                cxp_cobro.fecha_creacion = now
                                cxp_cobro.usuario_creacion = request.user.username
                                cxp_cobro.save()
                                print 'poniendo la frase en el ccxcobro :o'
                                #detalle_asiento.detalle = u"Retención de venta a la cuenta " + cuenta.descripcion hasta 12 enero 2016
                                detalle_asiento.detalle = frasefinal
                                detalle_asiento.plan_cuenta = cuenta
                                detalle_asiento.valor = valor_retenido
                                detalle_asiento.dbcr = "D"
                                detalle_asiento.cabecera_contable = cabecera_asiento
                                detalle_asiento.fecha_asiento = retencion_form_cab.getFecha()

                                detalle_asiento.fecha_creacion = now
                                detalle_asiento.usuario_creacion = request.user.username
                                detalle_asiento.save()

                            detalle_asiento_cliente = Detalle_Comp_Contable()
                            '''
                            detalle_asiento_cliente.detalle = u"Retención de venta del " \
                                                              u"cliente: " \
                                                              u"" + venta.cliente.razon_social[0:25] + u"Por un total de: " + str(total_retencion)
                            '''
                            #lo de arriba estaba hasta el 12 de enero
                            detalle_asiento_cliente.detalle = frasefinal
                            detalle_asiento_cliente.plan_cuenta = venta.cliente.plan_cuenta
                            detalle_asiento_cliente.valor = total_retencion
                            detalle_asiento_cliente.fecha_asiento = retencion_form_cab.getFecha()
                            detalle_asiento_cliente.dbcr = "H"
                            detalle_asiento_cliente.cabecera_contable = cabecera_asiento

                            detalle_asiento_cliente.usuario_creacion = request.user.username
                            detalle_asiento_cliente.fecha_creacion = now
                            detalle_asiento_cliente.save()

                            cuentas_por_cobrar.cobrado += total_retencion
                            cuentas_por_cobrar.fecha_actualizacion = now
                            cuentas_por_cobrar.usuario_actualizacion = request.user.username
                            cuentas_por_cobrar.save()
                        respuesta = {"status": 1}
                        messages.success(request, u"Se ha agregado con éxito la retención a la venta")
                    else:
                        respuesta = {"status": 2, "mensaje": u"Verifique que el total de la retención ingresada no "
                                                             u"supere el total de la venta o que supere la cuenta por cobrar"}
                        transaction.rollback()
            else:
                transaction.rollback()
                respuesta = {"status": 2, "mensaje": u"Algunos datos de la retención "
                                                     u"no son válidos, por favor revise si están llenos todos los campos,"
                                                     u" o si tienen valores válidos."}
    except Venta_Cabecera.DoesNotExist:
        transaction.rollback()
        respuesta = {"status": 3, "mensaje": u"No se puede asignar una retención a una venta anulada."}
    except:
        transaction.rollback()
        respuesta = {"status": 3, "mensaje": u"Error General"}

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def anular_retencion_venta(request, id):
    '''
    Vista Ajax que se encarga de anular la retención a la
    venta seleccionada
    :param request:
    :param id:
    :return:
    '''
    try:
        venta = Venta_Cabecera.objects.get(id=id, status=1)
        cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(venta_cabecera=venta, status=1)
        now = datetime.datetime.now()
        tipo_comp = TipoComprobante.objects.get(id=16)

        if CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1).exists():
            cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1)[0].cobro
            cobro.status = 2
            cobro.fecha_actualizacion = now
            cobro.usuario_actualizacion = request.user.username
            cobro.save()
            for obj in CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_por_cobrar, cobro__tipo_comprobante=tipo_comp, status=1):
                cuentas_por_cobrar.cobrado -= obj.monto
                obj.status = 2
                obj.fecha_actualizacion = now
                obj.usuario_actualizacion = request.user.username
                obj.save()
            cuentas_por_cobrar.fecha_actualizacion = now
            cuentas_por_cobrar.usuario_actualizacion = request.user.username
            cuentas_por_cobrar.save()

            cabecera_comp = Cabecera_Comp_Contable.objects.get(numero_comprobante=cobro.num_comp)
            cabecera_comp.status = 2
            cabecera_comp.fecha_actualizacion = now
            cabecera_comp.usuario_actualizacion = request.user.username
            cabecera_comp.save()

            for obj_det in cabecera_comp.getDetalleComp():
                obj_det.status = 2
                obj_det.fecha_actualizacion = now
                obj_det.usuario_actualizacion = request.user.username
                obj_det.save()
            messages.success(request, u"Se anuló la retención satisfactoriamente")
        else:
            messages.success(request, u"La venta no tiene retenciones asignadas en el sistema, "
                                      u"por favor verifique.")
        return HttpResponseRedirect(reverse("lista_ventas"))
    except Venta_Cabecera.DoesNotExist:
        raise Http404

@login_required(login_url='/')
@csrf_exempt
def cantidad_disponible_bodega_stock(request):
    '''
    Función de Ajax que me valida la cantidad disponible en stock para
    realizar la venta sólo productos
    :return:
    '''
    respuesta = []
    id = request.POST.get("id_item", "")
    cantidad_venta = request.POST.get("cantidad_venta", "")

    #try:
    bodega_tem = Item_Bodega.objects.get(item__id=id)
    item = Item.objects.filter(status=1).get(id=id)
    cant_bodega = float(bodega_tem.cantidad_actual)

    if item.TipoItem() == 1:                            # Productos
        if float(str(cantidad_venta)) <= cant_bodega:   # Ok
            respuesta.append({"id": id, "status": 1, "item": unicode(item.nombre), "cantidad_venta": cantidad_venta, "cantidad_bodega": cant_bodega})

        elif cant_bodega == 0.0:                        # Valida cantidad_bodega: 0.0
            respuesta.append({"id": id, "status": 2, "item": unicode(item.nombre), "cantidad_bodega": cant_bodega})

        else:   # La cantidad de venta es mayor a la cantidad disponible en bodega
            respuesta.append({"id": id, "status": 3,  "item": unicode(item.nombre), "cantidad_venta": cantidad_venta, "cantidad_bodega": cant_bodega})

        if cantidad_venta == "":
            respuesta.append({"status": 4, "cantidad_bodega": cant_bodega, "mensaje": u"Cantidad de venta vacía"})
    else:
        respuesta.append({"status": 5, "mensaje": u"Item tipo servicio"})
    #except:
    #    pass
    respuesta.append({"status": 0, "cantidad_bodega": 0, "mensaje": u"El Item no se encuentra registrado en bodega"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def get_correo_cliente(request):
    '''
    Vista Ajax que retorna el correo del cliente a donde se va a enviar
    :param request:
    :return:
    '''
    id_venta = request.POST.get("id", "")
    try:
        venta = Venta_Cabecera.objects.get(id=id_venta)
        if venta.cliente.email:
            respuesta = {"status": 1, "correo": venta.cliente.email}
        else:
            respuesta = {"status": 1, "correo": u"El cliente no tiene asignado un correo electrónico "
                                                u"por favor verifique"}
    except Venta_Cabecera.DoesNotExist:
        respuesta = {"status": 0, "mensaje": "El cliente no existe"}

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

############################################# FACTURA REPORTLAB ########################################################
import PIL
from reportlab.lib.pagesizes import letter
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch, cm
from reportlab.lib.enums import TA_JUSTIFY
from reportlab.platypus import *
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib import utils
def funcion_formato_factura(request, id):
    '''
     Vista para generar factura usando reportLab
     factura formato general sin cuadricula en el detalle
    :param request:
    :param id:
    :return:
    '''
    # Create the HttpResponse object with the appropriate PDF headers.
    venta = Venta_Cabecera.objects.get(id=id)
    detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=venta)
    fecha = str(venta.fecha.strftime("%d-%m-%Y"))
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename='+"Factura: "+unicode(venta.num_documento)+'.pdf'
    buffer = BytesIO()

    p = canvas.Canvas(buffer)

    p.setFont("Courier-Bold", 12)

    ###################################### CABEZA DE LA FACTURA ########################################################
    p.drawString(1.7*cm, 23.5*cm, "Cliente:")                                               # Etiqueta Cliente
    p.drawString(4*cm, 23.5*cm, unicode(venta.cliente.razon_social))                        # Cliente


    p.drawString(14.5*cm, 23.5*cm, "Fecha:")                                                # Etiqueta Fecha
    p.drawString(16*cm, 23.5*cm, fecha)                                                     # Fecha

    p.drawString(1.7*cm, 22.6*cm, "Ruc:")                                                   # Etiqueta Ruc
    p.drawString(4*cm, 22.6*cm, venta.cliente.ruc)                                          # Ruc
    if venta.cliente.telefono is None:
        telefono = ""
    else:
        telefono = str(venta.cliente.telefono)

    p.drawString(14.5*cm, 22.6*cm, u"Teléfono:")                                             # Etiqueta Teléfono
    p.drawString(16*cm, 22.6*cm, telefono)                                    # Teléfono

    if venta.cliente.getDirecciones() is None:
        direccion = ""
    else:
        direccion = venta.cliente.getDirecciones()

    p.drawString(1.7*cm, 21.8*cm, u"Dirección:")                                             # Etiqueta Dirección
    p.drawString(4*cm, 21.8*cm, unicode(direccion))                    # Dirección
    ####################################################################################################################

    ##############################################  Encabezado Detalle  ################################################
    p.line(1.7*cm, 20.8*cm, 20.5*cm, 20.8*cm)                                           # Línea
    p.drawString(1.7*cm, 20.4*cm, u"Cant.")
    p.drawString(4.7*cm, 20.4*cm, u"Descripción")
    p.drawString(15.5*cm, 20.4*cm, u"P.U.")
    p.drawString(18*cm, 20.4*cm, u"Total")
    p.line(1.7*cm, 20.3*cm, 20.5*cm, 20.3*cm)
    ####################################################################################################################

    ###################################### DETALLE DE FACTURA ##########################################################
    pos_det = 18.6*cm
    pos_y = 1*cm

    for d in detalle_venta:
        p.drawString(1.9*cm, pos_det + pos_y, str(d.cantidad))
        #concepto_tmp = d.concepto
        #concepto = concepto_tmp.split('!=.$')

        p.drawRightString(16.3*cm, pos_det + pos_y, "$  "+str(format(d.precio_unitario, ".2f")))
        p.drawRightString(19*cm, pos_det + pos_y, "$  "+str(format(d.precio_unitario*d.cantidad, ".2f")))


        p.drawString(3.7*cm, pos_det + pos_y, unicode(d.item.codigo+" - "+d.item.nombre))
        pos_y -= 0.35*cm

        ############################################# Totales ##########################################################
        p.drawRightString(17.5*cm, 5.6*cm, "Subtotal 12%:")
        p.drawRightString(18*cm, 5.6*cm, "$")
        p.drawRightString(19.7*cm, 5.6*cm, str(format(venta.subtotal_tarifa12, '.2f')))                            ## subt12

        p.drawRightString(17.5*cm, 4.8*cm, "Subtotal 0%:")
        p.drawRightString(18*cm, 4.8*cm, "$")
        p.drawRightString(19.7*cm, 4.8*cm, str(format(venta.subtotal_tarifa0, '.2f')))                             ## subt0

        p.drawRightString(17.5*cm, 4*cm, "Sub-Total:")
        p.drawRightString(18*cm, 4*cm, "$")
        p.drawRightString(19.7*cm, 4*cm, str(format((venta.subtotal_tarifa12+venta.subtotal_tarifa0), '.2f')))

        p.drawRightString(17.5*cm, 3.2*cm, "Descuento :")
        p.drawRightString(18*cm, 3.2*cm, "$")
        p.drawRightString(19.7*cm, 3.2*cm, str(format((venta.descuento_tarifa0+venta.descuento_tarifa12), '.2f')))   ## desc

        p.drawRightString(17.5*cm, 2.4*cm, "I.V.A. 12%:")
        iva = format(venta.iva_total, '.2f')
        p.drawRightString(18*cm, 2.4*cm, "$")
        p.drawRightString(19.7*cm, 2.4*cm, str(iva))                          ## iva12%

        p.drawRightString(17.5*cm, 1.6*cm, "Total:")
        p.drawRightString(18*cm, 1.6*cm, "$")
        p.drawRightString(19.7*cm, 1.6*cm, str(format(venta.getTotalPagar(), '.2f')))                    ## total
        ################################################################################################################
    p.line(1.7*cm, 6.7*cm, 20.5*cm, 6.7*cm)

    ###################################### Palabras Total a Pagar ######################################################
    p.drawString(1.6*cm, 5.2*cm, "SON:")
    ###################################### Palabras Total a Pagar ######################################################
    resultado_tmp = round((float(venta.getTotalPagar()) - int(venta.getTotalPagar()))*100, 2)
    cent = "0."+str(resultado_tmp).replace(".0", "")+"/100"
    p.drawString(2.8*cm, 5.2*cm, str(to_word(int(venta.getTotalPagar())))+",  "+cent)      ##total a pagar
    p.drawString(13*cm, 5.2*cm, u"Dólares")      ##total a pagar
    ####################################################################################################################

    ######################################### PIE DE PÁGINA ############################################################
    p.line(1.9*cm, 3*cm, 5.7*cm, 3*cm)              # Linea del REcibí Conforme
    p.drawCentredString(4*cm, 2.7*cm, u"Recibí Conforme")

    p.line(9.3*cm, 3*cm, 13.5*cm, 3*cm)              # Linea de Firma Autorizada
    p.drawCentredString(11.3*cm, 2.7*cm, u"Firma Autorizada")

    # Close the PDF object cleanly.
    p.showPage()
    p.save()

    # Get the value of the BytesIO buffer and write it to the response.
    pdf = buffer.getvalue()

    buffer.close()
    response.write(pdf)

    return response
def factura(request, id):
    '''
    Vista para generar un formato factura
    usando reportlab formato con cuadrícula
    :param request:
    :param id:
    :return:
    '''
    # Create the HttpResponse object with the appropriate PDF headers.
    response = HttpResponse(content_type='application/pdf')
    #response['Content-Disposition'] = 'attachment; filename="factura.pdf"'
    buffer = BytesIO()
    venta = Venta_Cabecera.objects.get(id=id)
    detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=venta)
    fecha = str(venta.fecha.strftime("%d-%m-%Y"))

    p = canvas.Canvas(buffer)
    p.setFont("Courier-Bold", 9)


    ###################################### CABEZA DE LA FACTURA ########################################################
    p.drawString(3*cm, 25.4*cm, unicode(venta.cliente.razon_social))                        # Cliente
    p.drawString(15.5*cm, 25.4*cm, fecha)                                          # Fecha
    p.drawString(3*cm, 24.6*cm, venta.cliente.ruc)                                 # Ruc

    if venta.cliente.telefono is not None:
        telefono = unicode(venta.cliente.telefono)
    else:
        telefono = ""

    p.drawString(15.5*cm, 24.6*cm, telefono)                         # Telefono
    p.drawString(3*cm, 23.8*cm, unicode(venta.cliente.getDirecciones()))                    # Dirección
    ####################################################################################################################

    ###################################### DETALLE DE FACTURA ##########################################################
    pos_det = 21*cm
    pos_y = 1*cm

    for d in detalle_venta:
        p.drawRightString(2*cm, pos_det + pos_y, str(d.cantidad))

        concepto_tmp = d.concepto
        concepto = concepto_tmp.split('!=.$')

        p.drawRightString(16.3*cm, pos_det + pos_y, "$"+str(format(d.precio_unitario, '.2f')))
        p.drawRightString(19.3*cm, pos_det + pos_y, "$"+str(format(d.precio_unitario*d.cantidad, '.2f')))

        for obj in concepto:
            p.drawString(3*cm, pos_det + pos_y, unicode(obj))
            pos_y -= 0.35*cm

        ###################################### Palabras Total a Pagar ##################################################

        resultado_tmp = round((float(venta.getTotalPagar()) - int(venta.getTotalPagar()))*100, 2)
        cent = "0."+str(resultado_tmp).replace(".0", "")+"/100"

        p.drawString(2.4*cm, 4.8*cm, str(to_word(int(venta.getTotalPagar())))+",  "+cent)      ##total a pagar
        ################################################################################################################

        ############################################# Totales ##########################################################
        p.drawRightString(19.5*cm, 5.6*cm, "$"+str(format(venta.subtotal_tarifa12, '.2f')))                            ## subt12
        p.drawRightString(19.5*cm, 4.8*cm, "$"+str(format(venta.subtotal_tarifa0, '.2f')))                           ## subt0
        p.drawRightString(19.5*cm, 4*cm, "$"+str(format(venta.descuento_tarifa0+venta.descuento_tarifa12, '.2f')))   ## desc
        p.drawRightString(19.5*cm, 3.2*cm, "$"+str(format(venta.subtotal_tarifa12+venta.subtotal_tarifa0, '.2f')))     ## subtotal
        p.drawRightString(19.5*cm, 2.4*cm, "$"+str(format(venta.iva_total, '.2f')))                          ## iva12%
        p.drawRightString(19.5*cm, 1.6*cm, "$"+str(format(venta.getTotalPagar(), '.2f')))                    ## total
        ################################################################################################################
        #pos_y -= 0.05*cm
    ####################################################################################################################

    # Close the PDF object cleanly.

    p.showPage()
    p.save()

    # Get the value of the BytesIO buffer and write it to the response.
    pdf = buffer.getvalue()

    buffer.close()
    response.write(pdf)

    return response

"""
Funciones para migrar datos(retenciones en ventas) de geoges
"""
class RetVenta(models.Model):
    ncomp = models.CharField(max_length=10)
    fecha = models.DateField()
    id_vta = models.IntegerField()
    ndoc = models.CharField(max_length=20)
    ruc = models.CharField(max_length=13)
    detalle = models.CharField(max_length=40)
    base12 = models.FloatField()
    id_cta = models.IntegerField()
    valor = models.FloatField()

    class Meta:
        db_table = "rvt2014"
class RetencionVentaCabFormMigra():
    def __init__(self, fecha_ret, n_comp):
        self.fecha_ret = fecha_ret
        self.n_comp = n_comp

    def getFecha(self):
        value = self.fecha_ret
        return value

    def getNcomp(self):
        value = self.n_comp
        return value
class RetencionVentaFormMigra():
    def getCuenta(self):
        value = self.cuenta
        return value

    def getValor(self):
        value = self.valor_retenido
        return value

    def getDetalle(self):
        value = self.detalle
        return value

    def __init__(self, cuenta, valor_retenido, detalle):
        self.cuenta = cuenta
        self.valor_retenido = valor_retenido
        self.detalle = detalle

@transaction.commit_on_success
@login_required(login_url='/')
@csrf_exempt
def migrar_ret_ventas(request):
    '''
     Vista Ajax que permite agregar una retención
     a la venta seleccionada
    :param request:
    :return:
    '''
    try:
        cont = 1
        lista_num_comp = []
        lista_objetos = RetVenta.objects.all()
        lista_ordenada = []
        for i in range(0, len(lista_objetos)):
            if lista_objetos[i].ncomp not in lista_num_comp:
                lista_num_comp.append(lista_objetos[i].ncomp)
                lista_detalle_ret = []
                for i2 in range(i, len(lista_objetos)):
                    if lista_objetos[i2].ncomp == lista_objetos[i].ncomp:
                        lista_detalle_ret.append(lista_objetos[i2])
                lista_ordenada.append([lista_objetos[i], lista_detalle_ret])
        t_cont = len(lista_ordenada)

        print "tamanio: ", t_cont
        for obj in lista_ordenada:
            cont += 1
            print "%.2f" % ((float(cont*100)/float(t_cont))), "% ..."

            retencion_form_cab = RetencionVentaCabFormMigra(obj[0].fecha, obj[0].ncomp)

            det_ret = []
            for obj_det in obj[1]:
                retencion_detalle = RetencionVentaFormMigra(int(obj_det.id_cta), obj_det.valor, obj_det.detalle)
                det_ret.append(retencion_detalle)

            retencion = det_ret
            venta = Venta_Cabecera.objects.get(id=obj[0].id_vta, status=1)
            cuentas_por_cobrar = Cuentas_por_Cobrar.objects.get(venta_cabecera=venta, naturaleza=1)
            now = datetime.datetime.now()
            total_retencion = 0.0
            cabecera_asiento = Cabecera_Comp_Contable()
            tipo_comp = TipoComprobante.objects.get(id=16)

            if True:
                num_comp = retencion_form_cab.getNcomp()
                if False: #datetime.date(venta.fecha.year, venta.fecha.month, venta.fecha.day )> retencion_form_cab.getFecha():
                    raise Exception(u"No se puede agregar la retención a esta factura, "
                                    u"ya que la fecha de la retención es "
                                    u"menor a la de la fecha del registro de la factura")
                else:
                    for form in retencion:
                        valor_retenido = float(form.getValor())
                        total_retencion += valor_retenido

                    cabecera_asiento.concepto_comprobante = u"Retención a la venta al cliente: " + venta.cliente.razon_social + u", valor: " + str(total_retencion)
                    cabecera_asiento.fecha = retencion_form_cab.getFecha().strftime("%Y-%m-%d")
                    cabecera_asiento.numero_comprobante = num_comp

                    cabecera_asiento.tipo_comprobante = tipo_comp

                    cabecera_asiento.usuario_creacion = 'ifarez'
                    cabecera_asiento.fecha_creacion = now
                    cabecera_asiento.save()
                    if total_retencion > 0.0 and (redondeo(total_retencion, 2) <= redondeo((cuentas_por_cobrar.monto - cuentas_por_cobrar.cobrado), 2)):
                        cobro = Cobro()
                        cobro.cliente = venta.cliente
                        cobro.concepto = u"Retención a la Venta"
                        cobro.fecha_pago = retencion_form_cab.getFecha()
                        cobro.fecha_reg = retencion_form_cab.getFecha()
                        cobro.forma_pago = FormaPago.objects.get(id=7)
                        cobro.monto = total_retencion
                        cobro.referencia = u'Migrado'
                        cobro.tipo_comprobante = TipoComprobante.objects.get(id=16)  #  Tipo de comprobante retencion en venta
                        cobro.num_comp = cabecera_asiento.numero_comprobante

                        cobro.usuario_creacion = 'ifarez'
                        cobro.fecha_creacion = now
                        cobro.save()

                        if True:
                            for form in retencion:
                                valor_retenido = form.getValor()
                                cuenta = PlanCuenta.objects.get(id=form.getCuenta())
                                detalle_asiento = Detalle_Comp_Contable()
                                cxp_cobro = CXC_cobro()
                                cxp_cobro.cobro = cobro
                                cxp_cobro.monto = valor_retenido
                                cxp_cobro.concepto = form.getDetalle()
                                cxp_cobro.cuentas_x_cobrar = cuentas_por_cobrar
                                cxp_cobro.plan_cuenta = cuenta

                                cxp_cobro.fecha_creacion = now
                                cxp_cobro.usuario_creacion = 'ifarez'
                                cxp_cobro.save()

                                detalle_asiento.detalle = form.getDetalle()
                                detalle_asiento.plan_cuenta = cuenta
                                detalle_asiento.valor = valor_retenido
                                detalle_asiento.dbcr = "D"
                                detalle_asiento.cabecera_contable = cabecera_asiento
                                detalle_asiento.fecha_asiento = retencion_form_cab.getFecha()

                                detalle_asiento.fecha_creacion = now
                                detalle_asiento.usuario_creacion = 'ifarez'
                                detalle_asiento.save()

                            detalle_asiento_cliente = Detalle_Comp_Contable()
                            detalle_asiento_cliente.detalle = u"Retención de venta del " \
                                                              u"cliente: " \
                                                              u"" + venta.cliente.razon_social[0:25] + u"Por un total de: " + str(total_retencion)
                            detalle_asiento_cliente.plan_cuenta = venta.cliente.plan_cuenta
                            detalle_asiento_cliente.valor = total_retencion
                            detalle_asiento_cliente.fecha_asiento = retencion_form_cab.getFecha()
                            detalle_asiento_cliente.dbcr = "H"
                            detalle_asiento_cliente.cabecera_contable = cabecera_asiento

                            detalle_asiento_cliente.usuario_creacion = 'ifarez'
                            detalle_asiento_cliente.fecha_creacion = now
                            detalle_asiento_cliente.save()

                            cuentas_por_cobrar.cobrado += total_retencion
                            cuentas_por_cobrar.fecha_actualizacion = now
                            cuentas_por_cobrar.usuario_actualizacion = 'ifarez'
                            cuentas_por_cobrar.save()
                    else:
                        transaction.rollback()
                        raise Exception(u"Verifique que el total de la retención ingresada no "
                                        u"supere el total de la venta")
            else:
                transaction.rollback()
                raise Exception(u"Algunos datos de la retención "
                                u"no son válidos, por favor revise si están llenos todos los campos,"
                                u" o si tienen valores válidos.")
        return HttpResponse('<body> Sucess </body>')
    except Venta_Cabecera.DoesNotExist:
        transaction.rollback()
        raise Exception(u"No se puede asignar una retención a una venta anulada.")
