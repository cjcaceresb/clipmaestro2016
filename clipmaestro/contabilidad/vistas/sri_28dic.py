#! /usr/bin/python
# -*- coding: UTF-8-*-

from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection

from librerias.funciones.comprobantes_electronicos import *
from librerias.funciones.permisos import *

from django.core.context_processors import request
from django.shortcuts import render_to_response
from xml.etree import ElementTree
import xml.etree.cElementTree as ET
from xml.dom import minidom
from xml.etree.ElementTree import Element, SubElement
from administracion.models import *
from django.db import connection
from reportes.formularios.reportes_form import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from contabilidad.formularios.SRIForm import *

register = template.Library()

__author__ = 'Roberto'


def prettify(elem):
    """
    Return a pretty-printed XML string for the Element.
    Función que imprime el archivo xml
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

"""
#aqui va baseImpExe = '0.00' puesto el 1 septiembre
        bimpexe = SubElement(detalle_compra, 'baseImpExe')
        bimpexe.text = '0.00'
"""


def generadorATS(mes, anio):
    cursor = connection.cursor()
    year = int(anio)
    month = int(mes)

    lista_compras = ATS.objects.filter(status=1,
                                       compra_cabecera__fecha_reg__year=year,
                                       compra_cabecera__fecha_reg__month=month).exclude(tipo_pago_sri__codigo="2")

    tipo_transaccion_venta = Tipo_Transaccion.objects.get(id=2)   # Objeto Transacción Venta

    cursor.execute("select get_suma_ventas_por_mes_anio_diferencia("+str(int(month))+","+str(int(year))+");")
    total_rows = cursor.fetchone()
    total_ventas_mes = total_rows[0]

    id_empresa = Empresa.objects.filter(status=1)[0]
    empresa = EmpresaGeneral.objects.get(id=id_empresa.empresa_general_id)

    fecha_ingreso = datetime.date(year, month, 1)
    fecha_corte = datetime.date(2015, 3, 1)

    if fecha_ingreso < fecha_corte:
        return generadorXml(month, year, empresa, total_ventas_mes, lista_compras, tipo_transaccion_venta)
    else:
        return generadorXml_marzo_2015(month, year, empresa, total_ventas_mes, lista_compras, tipo_transaccion_venta)

def generadorXml(month, year, empresa, total_ventas_mes, lista_compras, tipo_transaccion_venta):
    """
    Función que genera la estructura del
    archivo xml ATS
    :param request:
    :param month:
    :return:
    """
    top = Element('iva')
    tipo_id_inf = SubElement(top, 'TipoIDInformante')
    tipo_id_inf.text = "R"
    id_informante = SubElement(top, 'IdInformante')
    id_informante.text = empresa.rucci
    razon_social = SubElement(top, 'razonSocial')
    razon_social.text = empresa.razon_social.replace(".", " ")
    anio = SubElement(top, 'Anio')
    anio.text = str(year)
    mes = SubElement(top, 'Mes')
    if len(str(month)) < 2:#como lo de abajo para que le concatene un cero si es un mes de un digito
        month = '0'+str(month)
    mes.text = str(month)

    """
    Preguntar que es numEstabRuc
    """
    num_estab_ruc = SubElement(top, 'numEstabRuc')
    num_estab_ruc.text = "001"

    total_venta = SubElement(top, 'totalVentas')
    try:
        total_venta.text = "%.2f" % total_ventas_mes
    except:
        total_venta.text = "%.2f" % 0

    codigo_op = SubElement(top, 'codigoOperativo')
    codigo_op.text = 'IVA'
    compras = SubElement(top, 'compras')
    ventas = SubElement(top, 'ventas')
    ventas_establecimiento = SubElement(top, 'ventasEstablecimiento')

    #######################################
    #       Detalle de la Compra          #
    #######################################
    for obj in lista_compras:
        detalle_compra = SubElement(compras, 'detalleCompras')
        cod_sus_tri = SubElement(detalle_compra, 'codSustento')
        cod_sus_tri.text = obj.sustento_tributario_sri.codigo
        tpid_prov = SubElement(detalle_compra, 'tpIdProv')
        tpid_prov.text = Identificacion_Transaccion.objects.filter(status=1).get(transaccion=Tipo_Transaccion.objects.get(status=1, codigo=1), identificacion=obj.compra_cabecera.proveedor.tipo_identificacion).codigo
        id_prov = SubElement(detalle_compra, 'idProv')
        id_prov.text = obj.compra_cabecera.proveedor.ruc
        tipo_conp = SubElement(detalle_compra, 'tipoComprobante')
        tipo_conp.text = obj.compra_cabecera.documento.codigo_documento
        #parte_rel = 'NO' la etiqueta se llama parteRel

        if obj.compra_cabecera.proveedor.tipo_identificacion.codigo == "P":
            tipo_prov = SubElement(detalle_compra, 'tipoProv')
            tipo_prov.text = "01"
            parte_rel = SubElement(detalle_compra, 'parteRel')
            parte_rel.text = "NO"

        fecha_regi = SubElement(detalle_compra, 'fechaRegistro')
        fecha_regi.text = obj.compra_cabecera.fecha_reg.strftime("%d/%m/%Y")
        establecimiento = SubElement(detalle_compra, 'establecimiento')
        establecimiento.text = obj.compra_cabecera.num_doc.split("-")[0]
        punto_emision = SubElement(detalle_compra, 'puntoEmision')
        punto_emision.text = obj.compra_cabecera.num_doc.split("-")[1]
        secuencial = SubElement(detalle_compra, 'secuencial')
        secuencial.text = obj.compra_cabecera.num_doc.split("-")[2]
        fecha_emision = SubElement(detalle_compra, 'fechaEmision')
        fecha_emision.text = obj.compra_cabecera.fecha_emi.strftime("%d/%m/%Y")
        autorizacion = SubElement(detalle_compra, 'autorizacion')
        autorizacion.text = obj.autorizacion
        bngiva = SubElement(detalle_compra, 'baseNoGraIva')
        bngiva.text = '0.00'
        bimponible = SubElement(detalle_compra, 'baseImponible')
        bimponible.text = str('%.2f' % obj.base0)
        bimpgrav = SubElement(detalle_compra, 'baseImpGrav')
        bimpgrav.text = str('%.2f' % obj.baseiva)
        montoice = SubElement(detalle_compra, 'montoIce')
        montoice.text = str('%.2f' % obj.monto_ice)
        montoiva = SubElement(detalle_compra, 'montoIva')
        montoiva.text = str('%.2f' % ((obj.baseiva) * 0.12))  ### Cambiar quemado iva ###
        valor_ret_b = SubElement(detalle_compra, 'valorRetBienes')
        valor_rets = SubElement(detalle_compra, 'valorRetServicios')
        valor_ret_serv100 = SubElement(detalle_compra, 'valRetServ100')
        air = None

        #####################
        #   Retenciones     #
        #####################
        try:
            retencion = RetencionCompraCabecera.objects.get(compra_cabecera=obj.compra_cabecera, status=1)
            valor_ret_b.text = str('%.2f' % obj.compra_cabecera.getTotalRetencionxCodigo_30_x_sust(obj.sustento_tributario_sri_id))
            valor_rets.text = str('%.2f' % obj.compra_cabecera.getTotalRetencionxCodigo_70_x_sust(obj.sustento_tributario_sri_id))
            valor_ret_serv100.text = str('%.2f' % obj.compra_cabecera.getTotalRetencionxCodigo_100_x_sust(obj.sustento_tributario_sri_id))
        except RetencionCompraCabecera.DoesNotExist:
            retencion = None
            valor_ret_b.text = "0.00"
            valor_rets.text = "0.00"
            valor_ret_serv100.text = "0.00"

        ############################
        #    Pago Local_Ext        #
        ############################
        pago_ext = SubElement(detalle_compra, 'pagoExterior')
        pago_loc_ext = SubElement(pago_ext, 'pagoLocExt')
        pais = SubElement(pago_ext, 'paisEfecPago')
        aplic_convdotrib = SubElement(pago_ext, 'aplicConvDobTrib')
        pag_ext_suj_ret = SubElement(pago_ext, 'pagExtSujRetNorLeg')

        if obj.tipo_pago_sri.id == 1:
            pago_loc_ext.text = "01"
            pais.text = "NA"
            pag_ext_suj_ret.text = "NA"
            aplic_convdotrib.text = "NA"
        else:
            pago_loc_ext.text = "02"
            pais.text = str(obj.ats.pais.codigo)

            if obj.doble_tributacion:
                aplic_convdotrib.text = "SI"
            else:
                aplic_convdotrib.text = "NO"

            if obj.sujeto_retencion:
                pag_ext_suj_ret.text = "SI"
            else:
                pag_ext_suj_ret.text = "NO"
        ###########################
        #     Formas de Pago      #
        ###########################
        if obj.forma_pago_sri:
            formas_pago = SubElement(detalle_compra, 'formasDePago')
            forma_pago = SubElement(formas_pago, 'formaPago')
            forma_pago.text = obj.forma_pago_sri.codigo
        ##############################
        #   Reembolsos de Gastos     #
        ##############################
        if int(obj.documento.codigo_documento) == 41:
            reembolsos = Reembolso_gasto_compra.objects.filter(status=1, compra_cabecera=obj.compra_cabecera)
            reemb = SubElement(detalle_compra, 'reembolsos')
            for obj_reem in reembolsos:
                reembolso_xml = SubElement(reemb, 'reembolso')
                tipo_comp_reem = SubElement(reembolso_xml, 'tipoComprobanteReemb')
                tipo_comp_reem.text = obj_reem.documento.codigo_documento
                td_id_provee = SubElement(reembolso_xml, 'tpIdProvReemb')
                codigo = Identificacion_Transaccion.objects.filter(status=1).get(transaccion_id=1, identificacion=obj_reem.identificacion).codigo
                td_id_provee.text = codigo
                id_prvo_reem = SubElement(reembolso_xml, 'idProvReemb')
                id_prvo_reem.text = obj_reem.num_id
                estab_reem = SubElement(reembolso_xml, 'establecimientoReemb')
                estab_reem.text = obj_reem.num_doc.split("-")[0]
                punto_emit_reem = SubElement(reembolso_xml, 'puntoEmisionReemb')
                punto_emit_reem.text = obj_reem.num_doc.split("-")[1]
                secuencial_reem = SubElement(reembolso_xml, 'secuencialReemb')
                secuencial_reem.text = obj_reem.num_doc.split("-")[2]
                fecha_emi_reemb = SubElement(reembolso_xml, 'fechaEmisionReemb')
                fecha_emi_reemb.text = obj_reem.fecha_emi.strftime("%d/%m/%Y")
                aut_reemb = SubElement(reembolso_xml, 'autorizacionReemb')
                aut_reemb.text = obj_reem.autorizacion
                base_imp_reem = SubElement(reembolso_xml, 'baseImponibleReemb')
                base_imp_reem.text = str('%.2f' % 0.00)
                base_imp_grav_reem = SubElement(reembolso_xml, 'baseImpGravReemb')
                base_imp_grav_reem.text = str('%.2f' % obj_reem.baseiva)
                base_no_grev_reem = SubElement(reembolso_xml, 'baseNoGraIvaReemb')
                base_no_grev_reem.text = str('%.2f' % obj_reem.base0)
                monto_ice_reem = SubElement(reembolso_xml, 'montoIceRemb')
                monto_ice_reem.text = str('%.2f' % 0.00)
                monto_iva_reem = SubElement(reembolso_xml, 'montoIvaRemb')
                """
                ### Quemado el valor del IVA  ###
                """
                monto_iva_reem.text = str('%.2f' % (obj_reem.baseiva*0.12))

        ##################################
        #    Detalle del detalle de      #
        #       las Retenciones          #
        ##################################
        if retencion is not None:
            air = SubElement(detalle_compra, 'air')

            detalle_compra = CompraDetalle.objects.filter(compra_cabecera=obj.compra_cabecera,
                                                          sustento_tributario_id=obj.sustento_tributario_sri_id)
            lista_ret_x_sustento_id_fte = []
            lista_ret_x_sustento_fte = []

            for obj_det_compra in detalle_compra:

                if obj_det_compra.codigo_ret_fte_id not in lista_ret_x_sustento_id_fte:
                    lista_ret_x_sustento_id_fte.append(obj_det_compra.codigo_ret_fte_id)
                    lista_ret_x_sustento_fte.append([obj_det_compra.codigo_ret_fte_id,
                                                     obj_det_compra.valor,  # valor de la base de ret
                                                     obj_det_compra.monto_ret_fte])  # monto de la ret
                else:
                    for obj_lista_ret_sustento in lista_ret_x_sustento_fte:
                        if obj_lista_ret_sustento[0] == obj_det_compra.codigo_ret_fte_id:
                            obj_lista_ret_sustento[1] += obj_det_compra.valor  # valor de la base de ret
                            obj_lista_ret_sustento[2] += obj_det_compra.monto_ret_fte  # monto de la ret

            for obj_det_ret in lista_ret_x_sustento_fte:
                codigo_sri = Codigo_SRI.objects.get(id=obj_det_ret[0])
                det_ret = SubElement(air, 'detalleAir')
                cod_ret_air = SubElement(det_ret, 'codRetAir')

                cod_ret_air.text = str(codigo_sri.codigo)
                base_imp_air = SubElement(det_ret, 'baseImpAir')
                base_imp_air.text = ('%.2f' % obj_det_ret[1])
                porcent = SubElement(det_ret, 'porcentajeAir')
                porcent.text = str('%.2f' % codigo_sri.porcentaje)
                valret = SubElement(det_ret, 'valRetAir')
                valret.text = str('%.2f' % obj_det_ret[2])

                # Dividendos
                if codigo_sri.codigo == "345":
                    try:
                        dividendo = Dividendos.objects.get(compra_cabecera=obj.compra_cabecera)
                        fecha_pago_div = SubElement(det_ret, 'fechaPagoDiv')
                        fecha_pago_div.text = str(dividendo.fecha_pago.strftime("%d/%m/%Y"))
                        im_renta_soc = SubElement(det_ret, 'imRentaSoc')
                        im_renta_soc.text = str('%.2f' % dividendo.ir_asociado)
                        anio_ut_div = SubElement(det_ret, 'anioUtDiv')
                        anio_ut_div.text = str(dividendo.anio_utilidad)
                    except Dividendos.DoesNotExist:  # Si por algun motivo no puso ese codigo en la retencion
                        pass

        #################################
        #    Retencion de tipo 332      #
        #################################
        t_doc = int(obj.compra_cabecera.documento.codigo_documento)
        if air is None:
            if obj.compra_cabecera.getComprasRet0() != 0 and t_doc != 41 and t_doc != 4 and t_doc != 5:
                air = SubElement(detalle_compra, 'air')
                det_ret = SubElement(air, 'detalleAir')
                cod_ret_air = SubElement(det_ret, 'codRetAir')
                cod_ret_air.text = str(332)
                base_imp_air = SubElement(det_ret, 'baseImpAir')
                base_imp_air.text = ('%.2f' % obj.compra_cabecera.getComprasRet0())
                porcent = SubElement(det_ret, 'porcentajeAir')
                porcent.text = str('%.2f' % 0.00)
                valret = SubElement(det_ret, 'valRetAir')
                valret.text = str('%.2f' % 0.00)
        ###############################
        #   Documento que modifica    #
        ###############################
        try:
            doc_modif = Doc_Modifica.objects.get(compra_cabecera=obj.compra_cabecera)
            doc_modif_xml = SubElement(detalle_compra, 'docModificado')
            doc_modif_xml.text = str(doc_modif.tipo_documento.codigo_documento)
            estab_modif = SubElement(detalle_compra, 'estabModificado')
            estab_modif.text = str(doc_modif.numero_documento).split("-")[0]
            pto_modif = SubElement(detalle_compra, 'ptoEmiModificado')
            pto_modif.text = str(doc_modif.numero_documento).split("-")[1]
            sec_modif = SubElement(detalle_compra, 'secModificado')
            sec_modif.text = str(doc_modif.numero_documento).split("-")[2]
            aut_modif = SubElement(detalle_compra, 'autModificado')
            aut_modif.text = str(doc_modif.autorizacion)
        except:
            pass

    ############################################ VENTAS ################################################################
    array_ventas_cliente_id = []
    array_cod_doc_id = []
    array_docs = []
    array_ventas = []

    array_n_c_cliente_id = []
    array_n_c = []

    # NOTA DE CRÉDITO
    for obj in AjusteVentaCabecera.objects.filter(status=1, fecha_emi__year=year, fecha_emi__month=month).order_by("fecha_emi"):
        # Si ya se encuentra el id de ese cliente en una N/C
        if obj.cliente.id not in array_n_c_cliente_id:
            array_n_c_cliente_id.append(obj.cliente.id)
            array_n_c.append(obj.cliente)


    for obj_v in Venta_Cabecera.objects.filter(status=1, fecha__year=year, fecha__month=month).order_by("fecha"):
        # Si ya se encuentra el id de ese cliente en una venta
        if obj_v.cliente.id not in array_ventas_cliente_id:
            array_ventas_cliente_id.append(obj_v.cliente.id)
            array_ventas.append(obj_v.cliente)



    for obj_cliente in Cobro.objects.filter(status=1, fecha_reg__year=year, fecha_reg__month=month, tipo_comprobante_id=16):
        # Si ya se encuentra el id de ese cliente en una venta
        if obj_cliente.cliente.id not in array_ventas_cliente_id:
            array_ventas_cliente_id.append(obj_cliente.cliente.id)
            array_ventas.append(obj_cliente.cliente)

    for cliente in array_ventas:
        detalle_venta = SubElement(ventas, 'detalleVentas')
        cod_id_cliente = SubElement(detalle_venta, 'tpIdCliente')
        cod_id_cliente.text = codigo_identificacion_sri(tipo_transaccion_venta, cliente.tipo_identificacion)
        id_cliente = SubElement(detalle_venta, 'idCliente')
        id_cliente.text = str(cliente.ruc)

        ############## Preguntar si es tipo_comp ####################
        cod_tipo_comprobante = SubElement(detalle_venta, 'tipoComprobante')
        cod_tipo_comprobante.text = "18"  # VENTA
        cod_num_comprobantes = SubElement(detalle_venta, 'numeroComprobantes')
        cod_num_comprobantes.text = str(cliente.get_num_comprobantes_cliente(month, year))

        ######## Base No Objeto a Iva ################################
        cod_base_no_grava_iva = SubElement(detalle_venta, 'baseNoGraIva')
        cod_base_no_grava_iva.text = "0.00"

        ######## Base_0 ##############################################
        cod_base_imponible = SubElement(detalle_venta, 'baseImponible')
        cod_base_imponible.text = "%.2f" % (cliente.get_total_base_0(month, year))

        ######## Base_Iva ############################################
        cod_base_imp_grav = SubElement(detalle_venta, 'baseImpGrav')
        cod_base_imp_grav.text = "%.2f" % (cliente.get_total_base_iva(month, year))

        ######## MontoIva #############################################
        cod_monto_iva = SubElement(detalle_venta, 'montoIva')
        cod_monto_iva.text = "%.2f" % (cliente.get_total_monto_iva(month, year))

        if cliente.getRetencionVentaClienteFecha(month, year):
            (total_valor_ret_iva, total_valor_ret_renta) = cliente.get_totales_retencion_venta(month, year)
            cod_valor_ret_iva = SubElement(detalle_venta, 'valorRetIva')
            cod_valor_ret_iva.text = str(format(total_valor_ret_iva, ".2f"))
            cod_valor_ret_renta = SubElement(detalle_venta, 'valorRetRenta')
            cod_valor_ret_renta.text = str(format(total_valor_ret_renta, ".2f"))
        else:
            cod_valor_ret_iva = SubElement(detalle_venta, 'valorRetIva')
            cod_valor_ret_iva.text = "0.00"
            cod_valor_ret_renta = SubElement(detalle_venta, 'valorRetRenta')
            cod_valor_ret_renta.text = "0.00"


    for cliente in array_n_c:
        detalle_venta = SubElement(ventas, 'detalleVentas')
        cod_id_cliente = SubElement(detalle_venta, 'tpIdCliente')
        cod_id_cliente.text = codigo_identificacion_sri(tipo_transaccion_venta, cliente.tipo_identificacion)
        id_cliente = SubElement(detalle_venta, 'idCliente')
        id_cliente.text = str(cliente.ruc)

        ############## Preguntar si es tipo_comp ####################
        cod_tipo_comprobante = SubElement(detalle_venta, 'tipoComprobante')
        cod_tipo_comprobante.text = "04"  # N/C
        cod_num_comprobantes = SubElement(detalle_venta, 'numeroComprobantes')
        cod_num_comprobantes.text = str(cliente.get_num_comprobantes_cliente_nota_credito(month, year))

        ######## Base No Objeto a Iva ################################
        cod_base_no_grava_iva = SubElement(detalle_venta, 'baseNoGraIva')
        cod_base_no_grava_iva.text = "0.00"

        ######## Base_0 ##############################################
        cod_base_imponible = SubElement(detalle_venta, 'baseImponible')
        cod_base_imponible.text = "%.2f" % (cliente.get_total_base_0_nota_credito(month, year))

        ######## Base_Iva ############################################
        cod_base_imp_grav = SubElement(detalle_venta, 'baseImpGrav')
        cod_base_imp_grav.text = "%.2f" % (cliente.get_total_base_iva_nota_credito(month, year))

        ######## MontoIva #############################################
        cod_monto_iva = SubElement(detalle_venta, 'montoIva')
        cod_monto_iva.text = "%.2f" % (cliente.get_total_monto_iva_nota_credito(month, year))

        cod_valor_ret_iva = SubElement(detalle_venta, 'valorRetIva')
        cod_valor_ret_iva.text = "0.00"
        cod_valor_ret_renta = SubElement(detalle_venta, 'valorRetRenta')
        cod_valor_ret_renta.text = "0.00"

    # Facturas y N/C
    for obj in VigenciaDocEmpresa.objects.filter(status=1, documento_sri__venta=True):
        codigo = str(obj.serie).split("-")[0]
        if codigo not in array_cod_doc_id:
            array_cod_doc_id.append(codigo)
            array_docs.append(obj)

    for ob_doc in array_docs:
        ventaEst = SubElement(ventas_establecimiento, 'ventaEst')
        codEst = SubElement(ventaEst, 'codEstab')
        codEst.text = str(ob_doc.serie).split("-")[0]
        ###### Consultar si no debe existir otro nodo ######################
        venta_x_Est = SubElement(ventaEst, 'ventasEstab')
        (total_base_iva_doc, total_base_0_doc, total_base_iva_nc, total_base_0_nc) = ob_doc.get_totales_por_mes_sin_iva(month, year)
        totales_mes_sin_iva_doc = float(total_base_iva_doc) + float(total_base_0_doc) - float(total_base_iva_nc) - float(total_base_0_nc)
        venta_x_Est.text = "%.2f" % totales_mes_sin_iva_doc


    tree = ET.ElementTree(top)
    tree.write("mixml.xml")
    return open('mixml.xml').read()

def generadorXml_marzo_2015(month, year, empresa, total_ventas_mes, lista_compras, tipo_transaccion_venta):
    """
    Función que genera la estructura del
    archivo xml ATS
    :param request:
    :param month:
    :return:
    """
    print 'atsdeinemca'
    top = Element('iva')
    tipo_id_inf = SubElement(top, 'TipoIDInformante')
    tipo_id_inf.text = "R"
    id_informante = SubElement(top, 'IdInformante')
    id_informante.text = empresa.rucci
    razon_social = SubElement(top, 'razonSocial')
    razon_social.text = empresa.razon_social.replace(".", " ")
    anio = SubElement(top, 'Anio')
    anio.text = str(year)
    mes = SubElement(top, 'Mes')
    if len(str(month)) < 2:
        month = '0'+str(month)
    mes.text = str(month)

    num_estab_ruc = SubElement(top, 'numEstabRuc')
    num_estab_ruc.text = "001"

    total_venta = SubElement(top, 'totalVentas')
    try:
        total_venta.text = "%.2f" % total_ventas_mes
    except:
        total_venta.text = "%.2f" % 0

    codigo_op = SubElement(top, 'codigoOperativo')
    codigo_op.text = 'IVA'
    compras = SubElement(top, 'compras')
    ventas = SubElement(top, 'ventas')
    ventas_establecimiento = SubElement(top, 'ventasEstablecimiento')

    #######################################
    #       Detalle de la Compra          #
    #######################################
    for obj in lista_compras:
        detalle_compra = SubElement(compras, 'detalleCompras')
        cod_sus_tri = SubElement(detalle_compra, 'codSustento')
        cod_sus_tri.text = obj.sustento_tributario_sri.codigo
        tpid_prov = SubElement(detalle_compra, 'tpIdProv')
        tpid_prov.text = Identificacion_Transaccion.objects.filter(status=1).get(transaccion=Tipo_Transaccion.objects.get(status=1, codigo=1), identificacion=obj.compra_cabecera.proveedor.tipo_identificacion).codigo
        id_prov = SubElement(detalle_compra, 'idProv')
        id_prov.text = obj.compra_cabecera.proveedor.ruc
        tipo_conp = SubElement(detalle_compra, 'tipoComprobante')
        tipo_conp.text = obj.compra_cabecera.documento.codigo_documento

        if obj.compra_cabecera.proveedor.tipo_identificacion.codigo == "P":
            tipo_prov = SubElement(detalle_compra, 'tipoProv')
            tipo_prov.text = "01"

        parte_rel = SubElement(detalle_compra, 'parteRel')
        parte_rel.text = "NO"
        fecha_regi = SubElement(detalle_compra, 'fechaRegistro')
        fecha_regi.text = obj.compra_cabecera.fecha_reg.strftime("%d/%m/%Y")
        establecimiento = SubElement(detalle_compra, 'establecimiento')
        establecimiento.text = obj.compra_cabecera.num_doc.split("-")[0]
        punto_emision = SubElement(detalle_compra, 'puntoEmision')
        punto_emision.text = obj.compra_cabecera.num_doc.split("-")[1]
        secuencial = SubElement(detalle_compra, 'secuencial')
        secuencial.text = obj.compra_cabecera.num_doc.split("-")[2]
        fecha_emision = SubElement(detalle_compra, 'fechaEmision')
        fecha_emision.text = obj.compra_cabecera.fecha_emi.strftime("%d/%m/%Y")
        autorizacion = SubElement(detalle_compra, 'autorizacion')
        autorizacion.text = obj.autorizacion
        bngiva = SubElement(detalle_compra, 'baseNoGraIva')
        bngiva.text = '0.00'
        bimponible = SubElement(detalle_compra, 'baseImponible')
        bimponible.text = str('%.2f' % obj.base0)
        bimpgrav = SubElement(detalle_compra, 'baseImpGrav')
        bimpgrav.text = str('%.2f' % obj.baseiva)
        bimpexe = SubElement(detalle_compra, 'baseImpExe')
        bimpexe.text = '0.00'
        montoice = SubElement(detalle_compra, 'montoIce')
        montoice.text = str('%.2f' % obj.monto_ice)
        montoiva = SubElement(detalle_compra, 'montoIva')
        montoiva.text = str('%.2f' % ((obj.baseiva) * 0.12))  ### Cambiar quemado iva ###

        valor_ret_bien10 = SubElement(detalle_compra, 'valRetBien10')
        valor_ret_bien10.text = '0.00'
        valor_ret_serv20 = SubElement(detalle_compra, 'valRetServ20')
        valor_ret_serv20.text = '0.00'

        valor_ret_b = SubElement(detalle_compra, 'valorRetBienes')
        valor_rets = SubElement(detalle_compra, 'valorRetServicios')
        valor_ret_serv100 = SubElement(detalle_compra, 'valRetServ100')

        totalbase_impReemb = SubElement(detalle_compra, 'totbasesImpReemb')
        totalbase_impReemb.text = '0.00'

        air = None

        #####################
        #   Retenciones     #
        #####################
        try:
            retencion = RetencionCompraCabecera.objects.get(compra_cabecera=obj.compra_cabecera, status=1)
            valor_ret_b.text = str('%.2f' % obj.compra_cabecera.getTotalRetencionxCodigo_30_x_sust(obj.sustento_tributario_sri_id))
            valor_rets.text = str('%.2f' % obj.compra_cabecera.getTotalRetencionxCodigo_70_x_sust(obj.sustento_tributario_sri_id))
            valor_ret_serv100.text = str('%.2f' % obj.compra_cabecera.getTotalRetencionxCodigo_100_x_sust(obj.sustento_tributario_sri_id))
        except RetencionCompraCabecera.DoesNotExist:
            retencion = None
            valor_ret_b.text = "0.00"
            valor_rets.text = "0.00"
            valor_ret_serv100.text = "0.00"

        ############################
        #    Pago Local_Ext        #
        ############################
        pago_ext = SubElement(detalle_compra, 'pagoExterior')
        pago_loc_ext = SubElement(pago_ext, 'pagoLocExt')
        pais = SubElement(pago_ext, 'paisEfecPago')
        aplic_convdotrib = SubElement(pago_ext, 'aplicConvDobTrib')
        pag_ext_suj_ret = SubElement(pago_ext, 'pagExtSujRetNorLeg')

        if obj.tipo_pago_sri.id == 1:
            pago_loc_ext.text = "01"
            pais.text = "NA"
            pag_ext_suj_ret.text = "NA"
            aplic_convdotrib.text = "NA"
        else:
            pago_loc_ext.text = "02"
            pais.text = str(obj.ats.pais.codigo)

            if obj.doble_tributacion:
                aplic_convdotrib.text = "SI"
            else:
                aplic_convdotrib.text = "NO"

            if obj.sujeto_retencion:
                pag_ext_suj_ret.text = "SI"
            else:
                pag_ext_suj_ret.text = "NO"
        ###########################
        #     Formas de Pago      #
        ###########################
        if obj.forma_pago_sri:
            formas_pago = SubElement(detalle_compra, 'formasDePago')
            forma_pago = SubElement(formas_pago, 'formaPago')
            forma_pago.text = obj.forma_pago_sri.codigo
        ##############################
        #   Reembolsos de Gastos     #
        ##############################
        if int(obj.documento.codigo_documento) == 41:
            reembolsos = Reembolso_gasto_compra.objects.filter(status=1, compra_cabecera=obj.compra_cabecera)
            reemb = SubElement(detalle_compra, 'reembolsos')
            for obj_reem in reembolsos:
                reembolso_xml = SubElement(reemb, 'reembolso')
                tipo_comp_reem = SubElement(reembolso_xml, 'tipoComprobanteReemb')
                tipo_comp_reem.text = obj_reem.documento.codigo_documento
                td_id_provee = SubElement(reembolso_xml, 'tpIdProvReemb')
                codigo = Identificacion_Transaccion.objects.filter(status=1).get(transaccion_id=1, identificacion=obj_reem.identificacion).codigo
                td_id_provee.text = codigo
                id_prvo_reem = SubElement(reembolso_xml, 'idProvReemb')
                id_prvo_reem.text = obj_reem.num_id
                estab_reem = SubElement(reembolso_xml, 'establecimientoReemb')
                estab_reem.text = obj_reem.num_doc.split("-")[0]
                punto_emit_reem = SubElement(reembolso_xml, 'puntoEmisionReemb')
                punto_emit_reem.text = obj_reem.num_doc.split("-")[1]
                secuencial_reem = SubElement(reembolso_xml, 'secuencialReemb')
                secuencial_reem.text = obj_reem.num_doc.split("-")[2]
                fecha_emi_reemb = SubElement(reembolso_xml, 'fechaEmisionReemb')
                fecha_emi_reemb.text = obj_reem.fecha_emi.strftime("%d/%m/%Y")
                aut_reemb = SubElement(reembolso_xml, 'autorizacionReemb')
                aut_reemb.text = obj_reem.autorizacion
                base_imp_reem = SubElement(reembolso_xml, 'baseImponibleReemb')
                base_imp_reem.text = str('%.2f' % 0.00)
                base_imp_grav_reem = SubElement(reembolso_xml, 'baseImpGravReemb')
                base_imp_grav_reem.text = str('%.2f' % obj_reem.baseiva)
                base_no_grev_reem = SubElement(reembolso_xml, 'baseNoGraIvaReemb')
                base_no_grev_reem.text = str('%.2f' % obj_reem.base0)

                baseimp_exeReemb = SubElement(reembolso_xml, 'baseImpExeReemb')
                baseimp_exeReemb.text = str('%.2f' % 0.00)

                monto_ice_reem = SubElement(reembolso_xml, 'montoIceRemb')
                monto_ice_reem.text = str('%.2f' % 0.00)
                monto_iva_reem = SubElement(reembolso_xml, 'montoIvaRemb')
                """
                ### Quemado el valor del IVA  ###
                """
                monto_iva_reem.text = str('%.2f' % (obj_reem.baseiva*0.12))

        ##################################
        #    Detalle del detalle de      #
        #       las Retenciones          #
        ##################################
        if retencion is not None:
            air = SubElement(detalle_compra, 'air')
            #escrbir nuevos nodos por aqui
            #leer de la tabla retencion_compra_cabecera uno solo donde su compra_cabecera_id= el del obj ats compraCabecera_id
            miretencioncompracabecera = RetencionCompraCabecera.objects.filter(compra_cabecera_id = obj.compra_cabecera.id)[0]
            try:
            #print str(len(miretencioncompracabecera))
                print 'el sistema del ats no se cae :o'
                arreg = str(miretencioncompracabecera.vigencia_doc_empresa.serie).split("-") #esa cosa tiene un formato xxx-xxx debe separarse
                estabret = SubElement(detalle_compra, 'estabRetencion1')
                estabret.text = str(arreg[0])#str(miretencioncompracabecera.vigencia_doc_empresa.serie)
                autorprovisional = str(miretencioncompracabecera.vigencia_doc_empresa.autorizacion)
                print str(arreg[1])
                ptoemiret = SubElement(detalle_compra, 'ptoEmiRetencion1')
                ptoemiret.text = str(arreg[1])#decian hijos de det_ret
                secret = SubElement(detalle_compra, 'secRetencion1')
                print str(miretencioncompracabecera.get_num_ret())
                secret.text = str(miretencioncompracabecera.numero_ret)
                autret = SubElement(detalle_compra, 'autRetencion1')
                autret.text = str(miretencioncompracabecera.autorizacion)
                print '111111autret'
                print str(miretencioncompracabecera.autorizacion)
                if str(miretencioncompracabecera.autorizacion) == '':
                    print 'no hay bolo'
                    autret.text = autorprovisional
                print '2222autret'
                #si esta cosa es none, debo ver en otra tabla la autorizacion 261412
                fechaemiret = SubElement(detalle_compra, 'fechaEmiRet1')
                fechaemiret.text = str(miretencioncompracabecera.fecha_emi.strftime("%d/%m/%Y"))
            except:
                print 'el sistema del ats se cae :o'
            print 'estoy en el ats de marzo para adelante :o estoy conectado a tauro :o :o'
            detalle_compra = CompraDetalle.objects.filter(compra_cabecera=obj.compra_cabecera,
                                                          sustento_tributario_id=obj.sustento_tributario_sri_id)
            lista_ret_x_sustento_id_fte = []
            lista_ret_x_sustento_fte = []

            for obj_det_compra in detalle_compra:

                if obj_det_compra.codigo_ret_fte_id not in lista_ret_x_sustento_id_fte:
                    lista_ret_x_sustento_id_fte.append(obj_det_compra.codigo_ret_fte_id)
                    lista_ret_x_sustento_fte.append([obj_det_compra.codigo_ret_fte_id,
                                                     obj_det_compra.valor,  # valor de la base de ret
                                                     obj_det_compra.monto_ret_fte])  # monto de la ret
                else:
                    for obj_lista_ret_sustento in lista_ret_x_sustento_fte:
                        if obj_lista_ret_sustento[0] == obj_det_compra.codigo_ret_fte_id:
                            obj_lista_ret_sustento[1] += obj_det_compra.valor  # valor de la base de ret
                            obj_lista_ret_sustento[2] += obj_det_compra.monto_ret_fte  # monto de la ret

            for obj_det_ret in lista_ret_x_sustento_fte:
                codigo_sri = Codigo_SRI.objects.get(id=obj_det_ret[0])
                det_ret = SubElement(air, 'detalleAir')
                cod_ret_air = SubElement(det_ret, 'codRetAir')

                cod_ret_air.text = str(codigo_sri.codigo)
                base_imp_air = SubElement(det_ret, 'baseImpAir')
                base_imp_air.text = ('%.2f' % obj_det_ret[1])
                porcent = SubElement(det_ret, 'porcentajeAir')
                porcent.text = str('%.2f' % codigo_sri.porcentaje)
                valret = SubElement(det_ret, 'valRetAir')
                valret.text = str('%.2f' % obj_det_ret[2])

                # Dividendos
                if codigo_sri.codigo == "345":
                    try:
                        dividendo = Dividendos.objects.get(compra_cabecera=obj.compra_cabecera)
                        fecha_pago_div = SubElement(det_ret, 'fechaPagoDiv')
                        fecha_pago_div.text = str(dividendo.fecha_pago.strftime("%d/%m/%Y"))
                        im_renta_soc = SubElement(det_ret, 'imRentaSoc')
                        im_renta_soc.text = str('%.2f' % dividendo.ir_asociado)
                        anio_ut_div = SubElement(det_ret, 'anioUtDiv')
                        anio_ut_div.text = str(dividendo.anio_utilidad)
                    except Dividendos.DoesNotExist:  # Si por algun motivo no puso ese codigo en la retencion
                        pass
            #escrbir nuevos nodos por aqui
            #leer de la tabla retencion_compra_cabecera uno solo donde su compra_cabecera_id= el del obj ats compraCabecera_id
            '''
            miretencioncompracabecera = RetencionCompraCabecera.objects.filter(compra_cabecera_id = obj.compra_cabecera.id)[0]
            '''
            #try:
            #print str(len(miretencioncompracabecera))
            '''
            print 'el sistema del ats no se cae :o'
            arreg = str(miretencioncompracabecera.vigencia_doc_empresa.serie).split("-") #esa cosa tiene un formato xxx-xxx debe separarse
            estabret = SubElement(detalle_compra, 'estabRetencion1')
            estabret.text = str(arreg[0])#str(miretencioncompracabecera.vigencia_doc_empresa.serie)

            print str(arreg[1])
            ptoemiret = SubElement(detalle_compra, 'ptoEmiRetencion1')
            ptoemiret.text = str(arreg[1])#decian hijos de det_ret
            secret = SubElement(detalle_compra, 'secRetencion1')
            print str(miretencioncompracabecera.get_num_ret())
            secret.text = str(miretencioncompracabecera.numero_ret)
            autret = SubElement(detalle_compra, 'autRetencion1')
            autret.text = str(miretencioncompracabecera.autorizacion)
            fechaemiret = SubElement(detalle_compra, 'fechaEmiRet1')
            fechaemiret.text = str(miretencioncompracabecera.fecha_emi)
            '''
            #except:
                #print 'el sistema del ats se cae :o'
        #################################
        #    Retencion de tipo 332      #
        #################################
        t_doc = int(obj.compra_cabecera.documento.codigo_documento)
        if air is None:
            if obj.compra_cabecera.getComprasRet0() != 0 and t_doc != 41 and t_doc != 4 and t_doc != 5:
                air = SubElement(detalle_compra, 'air')
                det_ret = SubElement(air, 'detalleAir')
                cod_ret_air = SubElement(det_ret, 'codRetAir')
                cod_ret_air.text = str(332)
                base_imp_air = SubElement(det_ret, 'baseImpAir')
                base_imp_air.text = ('%.2f' % obj.compra_cabecera.getComprasRet0())
                porcent = SubElement(det_ret, 'porcentajeAir')
                porcent.text = str('%.2f' % 0.00)
                valret = SubElement(det_ret, 'valRetAir')
                valret.text = str('%.2f' % 0.00)
        ###############################
        #   Documento que modifica    #
        ###############################
        try:
            doc_modif = Doc_Modifica.objects.get(compra_cabecera=obj.compra_cabecera)
            doc_modif_xml = SubElement(detalle_compra, 'docModificado')
            doc_modif_xml.text = str(doc_modif.tipo_documento.codigo_documento)
            estab_modif = SubElement(detalle_compra, 'estabModificado')
            estab_modif.text = str(doc_modif.numero_documento).split("-")[0]
            pto_modif = SubElement(detalle_compra, 'ptoEmiModificado')
            pto_modif.text = str(doc_modif.numero_documento).split("-")[1]
            sec_modif = SubElement(detalle_compra, 'secModificado')
            sec_modif.text = str(doc_modif.numero_documento).split("-")[2]
            aut_modif = SubElement(detalle_compra, 'autModificado')
            aut_modif.text = str(doc_modif.autorizacion)
        except:
            pass

    ############################################ VENTAS ################################################################
    array_ventas_cliente_id = []
    array_cod_doc_id = []
    array_docs = []
    array_ventas = []

    array_n_c_cliente_id = []
    array_n_c = []

    # NOTA DE CRÉDITO
    for obj in AjusteVentaCabecera.objects.filter(status=1, fecha_emi__year=year, fecha_emi__month=month).order_by("fecha_emi"):
        # Si ya se encuentra el id de ese cliente en una N/C
        if obj.cliente.id not in array_n_c_cliente_id:
            array_n_c_cliente_id.append(obj.cliente.id)
            array_n_c.append(obj.cliente)


    for obj_v in Venta_Cabecera.objects.filter(status=1, fecha__year=year, fecha__month=month).order_by("fecha"):
        # Si ya se encuentra el id de ese cliente en una venta
        if obj_v.cliente.id not in array_ventas_cliente_id:
            array_ventas_cliente_id.append(obj_v.cliente.id)
            array_ventas.append(obj_v.cliente)



    for obj_cliente in Cobro.objects.filter(status=1, fecha_reg__year=year, fecha_reg__month=month, tipo_comprobante_id=16):
        # Si ya se encuentra el id de ese cliente en una venta
        if obj_cliente.cliente.id not in array_ventas_cliente_id:
            array_ventas_cliente_id.append(obj_cliente.cliente.id)
            array_ventas.append(obj_cliente.cliente)

    for cliente in array_ventas:
        detalle_venta = SubElement(ventas, 'detalleVentas')
        cod_id_cliente = SubElement(detalle_venta, 'tpIdCliente')
        cod_id_cliente.text = codigo_identificacion_sri(tipo_transaccion_venta, cliente.tipo_identificacion)
        id_cliente = SubElement(detalle_venta, 'idCliente')
        id_cliente.text = str(cliente.ruc)

        parterel_vtas = SubElement(detalle_venta, 'parteRelVtas')
        parterel_vtas.text = "NO"

        ############## Preguntar si es tipo_comp ####################
        cod_tipo_comprobante = SubElement(detalle_venta, 'tipoComprobante')
        cod_tipo_comprobante.text = "18"  # VENTA
        cod_num_comprobantes = SubElement(detalle_venta, 'numeroComprobantes')
        cod_num_comprobantes.text = str(cliente.get_num_comprobantes_cliente(month, year))

        ######## Base No Objeto a Iva ################################
        cod_base_no_grava_iva = SubElement(detalle_venta, 'baseNoGraIva')
        cod_base_no_grava_iva.text = "0.00"

        ######## Base_0 ##############################################
        cod_base_imponible = SubElement(detalle_venta, 'baseImponible')
        cod_base_imponible.text = "%.2f" % (cliente.get_total_base_0(month, year))

        ######## Base_Iva ############################################
        cod_base_imp_grav = SubElement(detalle_venta, 'baseImpGrav')
        cod_base_imp_grav.text = "%.2f" % (cliente.get_total_base_iva(month, year))

        ######## MontoIva #############################################
        cod_monto_iva = SubElement(detalle_venta, 'montoIva')
        cod_monto_iva.text = "%.2f" % (cliente.get_total_monto_iva(month, year))

        if cliente.getRetencionVentaClienteFecha(month, year):
            (total_valor_ret_iva, total_valor_ret_renta) = cliente.get_totales_retencion_venta(month, year)
            cod_valor_ret_iva = SubElement(detalle_venta, 'valorRetIva')
            cod_valor_ret_iva.text = str(format(total_valor_ret_iva, ".2f"))
            cod_valor_ret_renta = SubElement(detalle_venta, 'valorRetRenta')
            cod_valor_ret_renta.text = str(format(total_valor_ret_renta, ".2f"))
        else:
            cod_valor_ret_iva = SubElement(detalle_venta, 'valorRetIva')
            cod_valor_ret_iva.text = "0.00"
            cod_valor_ret_renta = SubElement(detalle_venta, 'valorRetRenta')
            cod_valor_ret_renta.text = "0.00"


    for cliente in array_n_c:
        detalle_venta = SubElement(ventas, 'detalleVentas')
        cod_id_cliente = SubElement(detalle_venta, 'tpIdCliente')
        cod_id_cliente.text = codigo_identificacion_sri(tipo_transaccion_venta, cliente.tipo_identificacion)
        id_cliente = SubElement(detalle_venta, 'idCliente')
        id_cliente.text = str(cliente.ruc)

        ############## Preguntar si es tipo_comp ####################
        cod_tipo_comprobante = SubElement(detalle_venta, 'tipoComprobante')
        cod_tipo_comprobante.text = "04"  # N/C
        cod_num_comprobantes = SubElement(detalle_venta, 'numeroComprobantes')
        cod_num_comprobantes.text = str(cliente.get_num_comprobantes_cliente_nota_credito(month, year))

        ######## Base No Objeto a Iva ################################
        cod_base_no_grava_iva = SubElement(detalle_venta, 'baseNoGraIva')
        cod_base_no_grava_iva.text = "0.00"

        ######## Base_0 ##############################################
        cod_base_imponible = SubElement(detalle_venta, 'baseImponible')
        cod_base_imponible.text = "%.2f" % (cliente.get_total_base_0_nota_credito(month, year))

        ######## Base_Iva ############################################
        cod_base_imp_grav = SubElement(detalle_venta, 'baseImpGrav')
        cod_base_imp_grav.text = "%.2f" % (cliente.get_total_base_iva_nota_credito(month, year))

        ######## MontoIva #############################################
        cod_monto_iva = SubElement(detalle_venta, 'montoIva')
        cod_monto_iva.text = "%.2f" % (cliente.get_total_monto_iva_nota_credito(month, year))

        cod_valor_ret_iva = SubElement(detalle_venta, 'valorRetIva')
        cod_valor_ret_iva.text = "0.00"
        cod_valor_ret_renta = SubElement(detalle_venta, 'valorRetRenta')
        cod_valor_ret_renta.text = "0.00"

    # Facturas y N/C
    for obj in VigenciaDocEmpresa.objects.filter(status=1, documento_sri__venta=True):
        codigo = str(obj.serie).split("-")[0]
        if codigo not in array_cod_doc_id:
            array_cod_doc_id.append(codigo)
            array_docs.append(obj)

    for ob_doc in array_docs:
        ventaEst = SubElement(ventas_establecimiento, 'ventaEst')
        codEst = SubElement(ventaEst, 'codEstab')
        codEst.text = str(ob_doc.serie).split("-")[0]
        ###### Consultar si no debe existir otro nodo ######################
        venta_x_Est = SubElement(ventaEst, 'ventasEstab')
        (total_base_iva_doc, total_base_0_doc, total_base_iva_nc, total_base_0_nc) = ob_doc.get_totales_por_mes_sin_iva(month, year)
        totales_mes_sin_iva_doc = float(total_base_iva_doc) + float(total_base_0_doc) - float(total_base_iva_nc) - float(total_base_0_nc)
        venta_x_Est.text = "%.2f" % totales_mes_sin_iva_doc

    templateXml = """<?xml version="1.0" encoding="utf-8" standalone="no"?>"""
    tree1 = 'pepin'
    #tree = ET.ElementTree(templateXml)
    tree = ET.ElementTree(top)
    tree.write("mixml.xml",'UTF-8',"""<?xml version="1.0" encoding="UTF-8" standalone="no"?>""")
    #tree.write("mixml.xml", standalone=False)
    return open('mixml.xml').read()#open


@login_required(login_url='/')
@csrf_exempt
def vista_ATS(request):
    buscador = BuscadorReportes(initial={"generar_editar_ats": 1})
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        if buscador.is_valid():
            if (buscador.getMes() is not None and buscador.getMes() > 0) and \
                    (buscador.getAnio() is not None and buscador.getAnio() > 0):

                if buscador.getMes() <= 9:
                    mes = "0"+str(buscador.getMes())
                    anio = buscador.getAnio()
                else:
                    mes = str(buscador.getMes())
                    anio = buscador.getAnio()

                response = HttpResponse(generadorATS(mes, anio), content_type='text/xml')
                response['Content-Disposition'] = 'attachment; filename="ats.xml"'
                return response

            else:
                if buscador.getMes() is None or buscador.getMes() == 0:
                    errors = buscador._errors.setdefault("mes", ErrorList())
                    errors.append(u"El mes es campo requerido")
                if buscador.getAnio() is None or buscador.getAnio() == 0:
                    errors = buscador._errors.setdefault("anio", ErrorList())
                    errors.append(u"El año es campo requerido")

    return render_to_response('ats/ats.html',
                              {"buscador": buscador}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@csrf_protect
def editar_ats(request):
    formset_form = formset_factory(EditATSForm, extra=0)
    list = []
    buscador = BuscadorReportes(request.GET)
    buscador.is_valid()
    if (buscador.getMes() is not None and buscador.getMes() > 0) and \
            (buscador.getAnio() is not None and buscador.getAnio() > 0):
        if buscador.getMes() <= 9:
            mes = "0"+str(buscador.getMes())
        else:
            mes = buscador.getMes()

        anio = buscador.getAnio()
        proveedor = buscador.get_proveedor()
        num_doc = buscador.get_num_doc()

        if proveedor is not None:
            q1 = ATS.objects.filter(status=1,
                                    compra_cabecera__fecha_reg__year=anio,
                                    compra_cabecera__fecha_reg__month=mes,
                                    compra_cabecera__proveedor=proveedor,
                                    compra_cabecera__num_doc__icontains=num_doc).exclude(tipo_pago_sri__codigo="2").order_by("compra_cabecera__num_comp")
        else:
            q1 = ATS.objects.filter(status=1,
                                    compra_cabecera__fecha_reg__year=anio,
                                    compra_cabecera__fecha_reg__month=mes,
                                    compra_cabecera__num_doc__icontains=num_doc).exclude(tipo_pago_sri__codigo="2").order_by("compra_cabecera__num_comp")

        for obj in q1:
            a = {"id": obj.id,
                 "num_comp": obj.compra_cabecera.num_comp,
                 "proveedor": obj.compra_cabecera.proveedor.razon_social,
                 "ruc": obj.compra_cabecera.proveedor.ruc,
                 "num_doc": obj.compra_cabecera.num_doc,
                 "sustento_tributario": obj.sustento_tributario_sri_id,
                 "autorizacion": obj.autorizacion,
                 "forma_pago": obj.forma_pago_sri_id,
                 "tipo_pago": obj.tipo_pago_sri_id,
                 "pais": obj.pais_id,
                 "doble_tributacion": obj.doble_tributacion,
                 "sujeto_retencion": obj.sujeto_retencion}
            list.append(a)
    else:
        if buscador.getMes() is None or buscador.getMes() == 0:
            errors = buscador._errors.setdefault("mes", ErrorList())
            errors.append(u"El mes es campo requerido")
        if buscador.getAnio() is None or buscador.getAnio() == 0:
            errors = buscador._errors.setdefault("anio", ErrorList())
            errors.append(u"El año es campo requerido")

    formularios = formset_form(initial=list)
    if request.method == 'POST':
        formularios = formset_form(request.POST)
        buscador = BuscadorReportes(request.POST)
        now = datetime.datetime.now()
        if formularios.is_valid():
            for form in formularios:
                informacion = form.cleaned_data
                id = informacion.get("id")
                sustento = informacion.get("sustento_tributario")
                autorizacion = informacion.get("autorizacion")
                forma_pago = informacion.get("forma_pago")
                tipo_pago = informacion.get("tipo_pago")
                pais = informacion.get("pais")
                doble_tributacion = informacion.get("doble_tributacion")
                sujeto_retencion = informacion.get("sujeto_retencion")
                try:
                    ats = ATS.objects.get(id=id)
                    ats.sustento_tributario_sri_id = sustento
                    ats.autorizacion = autorizacion
                    ats.forma_pago_sri_id = forma_pago
                    ats.tipo_pago_sri_id = tipo_pago
                    ats.pais_id = pais
                    ats.doble_tributacion = doble_tributacion
                    ats.sujeto_retencion = sujeto_retencion
                    ats.usuario_actualizacion = request.user.username
                    ats.fecha_actualizacion = now
                    ats.save()
                except ATS.DoesNotExist:
                    pass
            messages.success(request, u"Se ha modificado exitosamente el ATS")
            return render_to_response('ats/ats.html',
                                      {"buscador": buscador,
                                       "formularios": formularios}, context_instance=RequestContext(request))
        else:
            messages.error(request, u"Existen algunos errores en el formulario por favor verifique")

    return render_to_response('ats/ats.html',
                              {"buscador": buscador,
                               "formularios": formularios}, context_instance=RequestContext(request))


def vista_101(request):
    '''
    Vista que genera el formulario 101
    :param request:
    :return:
    '''


def vista_103(request):
    '''
    Vista que genera el formulario 103
    :param request:
    :return:
    '''


def vista_104(request):
    '''
    Vista que genera el formulario 104
    :param request:
    :return:
    '''

