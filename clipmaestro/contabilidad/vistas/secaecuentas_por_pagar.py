#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from contabilidad.formularios.ComprasForm import *
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import transaction
from django.forms.util import ErrorList
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.core.paginator import *
from django import template
from administracion.models import *
from librerias.funciones.permisos import *
from contabilidad.formularios.CuentasxPagarForm import *
from django.template.loader import render_to_string
from librerias.funciones.paginacion import *
import operator
from contabilidad.funciones.cuentas_por_pagar_func import *
from librerias.funciones.funciones_vistas import *


@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def lista_pago_proveedores(request):
    """
    Vista en la cual se enlistan
    todos los pagos
    :param request:
    :return:
    """
    buscador = BuscadorPagos(request.GET)
    pago = busqueda_pagos(buscador)
    paginacion = Paginator(pago, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    reversion_pago_form = ReversionCuentasPagarForm()
    try:
        pagos = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        pagos = paginacion.page(1)
    except EmptyPage:
        pagos = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = pagos.number
    arreglo_num_paginado = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Cuentas_Pagar/pago_proveedores/lista_pagos.html',
                              {"objetos": pagos, "buscador": buscador, "total_paginas": total_paginas,
                               "numero": numero, "arreglo_paginado": arreglo_num_paginado,
                               "reversion_pago_form": reversion_pago_form,
                                "muestra_paginacion": muestra_paginacion_template(paginacion.count)},
                                context_instance=RequestContext(request))


def redireccionar_pago(request, pago):
    """
    redirecciona dependiendo de la acción que desee el usuario
    :return:
    """

    if request.GET.get("grabar_agregar", "") == "1":
        full_redirect_url = reverse("pago_proveedores")

    elif request.GET.get("grabar_imprimir", "") == "1":
        redirect_url = reverse("agregarcompras")
        extra_params = '?imp=%s' % pago.id
        full_redirect_url = '%s%s' % (redirect_url, extra_params)
    else:
        full_redirect_url = reverse("lista_pago_proveedores")

    return full_redirect_url


@transaction.commit_on_success
@csrf_exempt
@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
def pago_proveedores(request):
    """
    Vista la cual sirve para realizar los pagos
    de las deudas pendientes en cuentas por
    pagar.
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    contador = 0
    flag = False  # Variable que me dice si los datos ya han sido cargados por POST
    asiento = Cabecera_Comp_Contable()

    formset_detalle_docs = formset_factory(DetallePago, extra=0)
    detalle_docs = formset_detalle_docs()
    forma_de_pago = FormaPagoForm()

    id_prov = request.GET.get("prov", "")  # Cuando viene de Compras
    if id_prov != "":
        cabecera = CabeceraPagosForm(initial={"proveedor": id_prov})
    else:
        cabecera = CabeceraPagosForm()

    if request.method == "POST":
        detalle_docs = formset_detalle_docs(request.POST)
        cabecera = CabeceraPagosForm(request.POST)
        forma_de_pago = FormaPagoForm(request.POST)
        flag = True
        pago = Pago()
        #try:

        if cabecera.is_valid() and forma_de_pago.is_valid():
            #  Se valida la fecha de cierre de la  empresa

            print forma_de_pago.getFormaPago()
            print('la fecha---------------')
            try:
                print str(forma_de_pago.getFechaCheque())
            except:
                print('la fecha nunca le llega')

            #solo si esta cosa es pagada con cheques :o :O
            if forma_de_pago.getFormaPago() == '1': #si es un cheque tiene campo de fecha esto va a funcionar
                if forma_de_pago.getFechaCheque():
                    print 'fechacheque existe'
                else:
                    print 'la fecha cheque no existe'
                if forma_de_pago.get_fecha():
                    print 'fecha existe'
                else:
                    print 'la fecha no existe'

                if not esta_periodo_contable(forma_de_pago.getFechaCheque()):
                    parametro_empresa = get_parametros_empresa()
                    messages.error(request, u"La fecha del registro es menor a la fecha de cierre contable " +get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))
                    errors = forma_de_pago._errors.setdefault("fecha", ErrorList())
                    errors.append(u"La fecha del registro es menor a la fecha de cierre contable, por favor verifique")
                    return render_to_response('Cuentas_Pagar/pago_proveedores/pago_proveedores.html', {"cabecera": cabecera,
                                   'forms': detalle_docs, 'flag': flag, "formularios": forma_de_pago},
                                              context_instance=RequestContext(request))
                else:
                    #si entra,pero es p´rovisionado
                    #cheque_provisionado=true
                    if detalle_docs.is_valid():
                        total_pagar, total_anticipo = CalcularTotalPago(detalle_docs)
                        print 'hace el total al pagar'
                        if total_pagar <= 0 and total_anticipo <= 0:
                            messages.error(request, u"Se debe ingresar un valor mayor a cero para realizar la transacción")
                            contador += 1
                        else:
                            print 'quiere pagar'
                            if forma_de_pago.is_valid():
                                print 'cerca de caerse :o'
                                contador += GuardarPago(pago, asiento, cabecera, forma_de_pago, detalle_docs, total_pagar, total_anticipo, contador, now, request)
                                print 'si hace esto, no se cayo aqui'
                            else:
                                contador += 1
                    else:
                        messages.error(request, u"Exite un error en el detalle del pago por favor verifique")
                        contador += 1

                    if contador == 0:
                        if pago.num_cheque:
                            cuenta_banco = Cuenta_Banco.objects.get(id=int(forma_de_pago.getBanco()))
                            messages.success(request, u"Se ha grabado el pago con éxito, con numero de comprobante: " +
                                             pago.num_comp + u" y número de cheque: " + str(pago.num_cheque) + u" del banco: " +
                                             cuenta_banco.descripcion)
                        else:
                            messages.success(request, u"Se ha grabado con éxito el pago con número de comprobante: " + pago.num_comp)

                        return HttpResponseRedirect(redireccionar_pago(request, pago))

                    else:
                        transaction.rollback()
                        return render_to_response('Cuentas_Pagar/pago_proveedores/pago_proveedores.html',
                                  {"cabecera": cabecera,
                                   'forms': detalle_docs,
                                   'flag': flag,
                                   "formularios": forma_de_pago}, context_instance=RequestContext(request))
            else:
                print 'no es de cheque'
                if detalle_docs.is_valid():
                    total_pagar, total_anticipo = CalcularTotalPago(detalle_docs)

                    if total_pagar <= 0 and total_anticipo <= 0:
                        messages.error(request, u"Se debe ingresar un valor mayor a cero para realizar la transacción")
                        contador += 1
                    else:
                        if forma_de_pago.is_valid():
                            contador += GuardarPago(pago, asiento, cabecera, forma_de_pago, detalle_docs, total_pagar, total_anticipo, contador, now, request)
                        else:
                            contador += 1
                else:
                    messages.error(request, u"Exite un error en el detalle del pago por favor verifique")
                    contador += 1

                if contador == 0:
                    if pago.num_cheque:
                        cuenta_banco = Cuenta_Banco.objects.get(id=int(forma_de_pago.getBanco()))
                        messages.success(request, u"Se ha grabado el pago con éxito, con numero de comprobante: " +
                                         pago.num_comp + u" y número de cheque: " + str(pago.num_cheque) + u" del banco: " +
                                         cuenta_banco.descripcion)
                    else:
                        messages.success(request, u"Se ha grabado con éxito el pago con número de comprobante: " + pago.num_comp)

                    return HttpResponseRedirect(redireccionar_pago(request, pago))

                else:
                    transaction.rollback()
                    return render_to_response('Cuentas_Pagar/pago_proveedores/pago_proveedores.html',
                              {"cabecera": cabecera,
                               'forms': detalle_docs,
                               'flag': flag,
                               "formularios": forma_de_pago}, context_instance=RequestContext(request))


        else:
            messages.error(request, u"Por favor llene los campos obligatorios.")
            transaction.rollback()
        #except:
        #    messages.error(request, u"Error general del sistema, por favor comuníquese con el administrador del sistema")
        #    transaction.rollback()
        #    return HttpResponseRedirect(reverse('lista_pago_proveedores'))

    return render_to_response('Cuentas_Pagar/pago_proveedores/pago_proveedores.html',
                              {"cabecera": cabecera,
                               'forms': detalle_docs,
                               'flag': flag,
                               "formularios": forma_de_pago}, context_instance=RequestContext(request))



@transaction.commit_on_success
@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def pago_proveedores_16oct(request):
    """
    Vista la cual sirve para realizar los pagos
    de las deudas pendientes en cuentas por
    pagar.
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    contador = 0
    flag = False  # Variable que me dice si los datos ya han sido cargados por POST
    asiento = Cabecera_Comp_Contable()

    formset_detalle_docs = formset_factory(DetallePago, extra=0)
    detalle_docs = formset_detalle_docs()
    forma_de_pago = FormaPagoForm()

    id_prov = request.GET.get("prov", "")  # Cuando viene de Compras
    if id_prov != "":
        cabecera = CabeceraPagosForm(initial={"proveedor": id_prov})
    else:
        cabecera = CabeceraPagosForm()

    if request.method == "POST":
        detalle_docs = formset_detalle_docs(request.POST)
        cabecera = CabeceraPagosForm(request.POST)
        forma_de_pago = FormaPagoForm(request.POST)
        flag = True
        pago = Pago()
        #try:

        if cabecera.is_valid() and forma_de_pago.is_valid():
            #  Se valida la fecha de cierre de la  empresa
            if not esta_periodo_contable(forma_de_pago.get_fecha()):
                parametro_empresa = get_parametros_empresa()
                messages.error(request, u"La fecha del registro es menor a la fecha de cierre contable " +get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))
                errors = forma_de_pago._errors.setdefault("fecha", ErrorList())
                errors.append(u"La fecha del registro es menor a la fecha de cierre contable, por favor verifique")
                return render_to_response('Cuentas_Pagar/pago_proveedores/pago_proveedores.html', {"cabecera": cabecera,
                               'forms': detalle_docs, 'flag': flag, "formularios": forma_de_pago},
                                          context_instance=RequestContext(request))
            else:

                if detalle_docs.is_valid():
                    total_pagar, total_anticipo = CalcularTotalPago(detalle_docs)

                    if total_pagar <= 0 and total_anticipo <= 0:
                        messages.error(request, u"Se debe ingresar un valor mayor a cero para realizar la transacción")
                        contador += 1
                    else:
                        if forma_de_pago.is_valid():
                            contador += GuardarPago(pago, asiento, cabecera, forma_de_pago, detalle_docs, total_pagar, total_anticipo, contador, now, request)
                        else:
                            contador += 1
                else:
                    messages.error(request, u"Exite un error en el detalle del pago por favor verifique")
                    contador += 1

                if contador == 0:
                    if pago.num_cheque:
                        cuenta_banco = Cuenta_Banco.objects.get(id=int(forma_de_pago.getBanco()))
                        messages.success(request, u"Se ha grabado el pago con éxito, con numero de comprobante: " +
                                         pago.num_comp + u" y número de cheque: " + str(pago.num_cheque) + u" del banco: " +
                                         cuenta_banco.descripcion)
                    else:
                        messages.success(request, u"Se ha grabado con éxito el pago con número de comprobante: " + pago.num_comp)

                    return HttpResponseRedirect(redireccionar_pago(request, pago))

                else:
                    transaction.rollback()
                    return render_to_response('Cuentas_Pagar/pago_proveedores/pago_proveedores.html',
                              {"cabecera": cabecera,
                               'forms': detalle_docs,
                               'flag': flag,
                               "formularios": forma_de_pago}, context_instance=RequestContext(request))
        else:
            messages.error(request, u"Por favor llene los campos obligatorios.")
            transaction.rollback()
        #except:
        #    messages.error(request, u"Error general del sistema, por favor comuníquese con el administrador del sistema")
        #    transaction.rollback()
        #    return HttpResponseRedirect(reverse('lista_pago_proveedores'))

    return render_to_response('Cuentas_Pagar/pago_proveedores/pago_proveedores.html',
                              {"cabecera": cabecera,
                               'forms': detalle_docs,
                               'flag': flag,
                               "formularios": forma_de_pago}, context_instance=RequestContext(request))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def detalle_pago_proveedores(request, id):
    """
    Vista que sirve para mostrar en detalle del pago
    :param request:
    :param id:
    :return:
    """
    try:
        pago = Pago.objects.get(id=id)
        proveedor = pago.proveedor
        cuentas_pagar_detallePago = pago.getCxPpago()
        try:
            cabecera_asiento = Cabecera_Comp_Contable.objects.get(numero_comprobante=pago.num_comp)
        except Cabecera_Comp_Contable.DoesNotExist:
            cabecera_asiento = None
        detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_asiento).order_by("dbcr", "plan_cuenta__codigo")
        return render_to_response('Cuentas_Pagar/pago_proveedores/comprobante_detalle_pago_proveedores.html',
                                  {"pago": pago,
                                   "mayor_cab":cabecera_asiento,
                                   "detalle_may": detalle_mayor,
                                   "detalle_pago": cuentas_pagar_detallePago,
                                   "proveedor": proveedor}, context_instance=RequestContext(request))
    except Pago.DoesNotExist:
        raise Http404


@login_required(login_url='/')
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def anular_pago(request, id):
    """
    Anula el pago que se realizó, actualiza las tablas
    en la cual se registró el pago
    :param request:
    :param id:
    :return:
    """
    try:
        pago = Pago.objects.get(id=id)
        cxp_pago = pago.getCxPpago()
        now = datetime.datetime.now()
        #############################################
        #  Se valida la fecha de cierre de la       #
        #               empresa                     #
        #############################################
        if not esta_periodo_contable(pago.fecha_reg):
            messages.error(request, u"El pago que quiere anular se encuentra dentro del periodo del cierre contable, "
                                    u"por favor verifique")
            return HttpResponseRedirect(reverse("lista_pago_proveedores"))
        else:
            # caso cruce de documento
            if pago.forma_pago_id == 5 or pago.forma_pago_id == 6:
                for obj in cxp_pago:
                    cuentas_por_pagar = Cuentas_por_Pagar.objects.get(id=obj.cuentas_x_pagar.id)
                    cuentas_por_pagar.pagado = cuentas_por_pagar.pagado - obj.monto

                    cuentas_por_pagar.fecha_actualizacion = datetime.datetime.now()
                    cuentas_por_pagar.usuario_actualizacion = request.user.username
                    cuentas_por_pagar.save()

                    obj.status = 2
                    obj.fecha_actualizacion = now
                    obj.usuario_actualizacion = request.user.username
                    obj.save()
            else:  # Caso pagos normales
                for obj in cxp_pago:
                    cuentas_por_pagar = Cuentas_por_Pagar.objects.get(id=obj.cuentas_x_pagar.id)

                    if cuentas_por_pagar.naturaleza == 2:
                        cuentas_por_pagar.pagado = cuentas_por_pagar.pagado - obj.monto

                    # caso anticipo
                    # Si el anticipo que desea anular esta cruzado con algún documento, no se
                    # podría anular el pago, primero se necesitaría anular el cruce de documento.
                    else:
                        # Como es un anticipo anulo la cuenta por pagar ya que se anuló el pago
                        cuentas_por_pagar.status = 2
                        if cuentas_por_pagar.pagado > 0:
                            raise ErrorPagoProveedores(u"No se puede anular el pago, ya que posee un cruce de documento"
                                                       u", anule dicho cruce para que pueda anular"
                                                       u" este pago")

                    cuentas_por_pagar.fecha_actualizacion = datetime.datetime.now()
                    cuentas_por_pagar.usuario_actualizacion = request.user.username
                    cuentas_por_pagar.save()


                    obj.status = 2
                    obj.fecha_actualizacion = now
                    obj.usuario_actualizacion = request.user.username
                    obj.save()

            pago.status = 2
            pago.fecha_actualizacion = now
            pago.usuario_actualizacion = request.user.username
            pago.save()

            # Anular transacciones en banco
            try:
                mov_banco = Banco.objects.get(num_comp=pago.num_comp)
                mov_banco.status = 2
                mov_banco.fecha_actualizacion = now
                mov_banco.usuario_actualizacion = request.user.username
                mov_banco.save()
            except Banco.DoesNotExist:
                pass

            cabecera_asiento = Cabecera_Comp_Contable.objects.get(numero_comprobante=pago.num_comp)
            cabecera_asiento.status = 2
            cabecera_asiento.fecha_actualizacion = now
            cabecera_asiento.usuario_actualizacion = request.user.username
            cabecera_asiento.save()

            detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_asiento)
            for obj in detalle_mayor:
                obj.status = 2
                obj.fecha_actualizacion = now
                obj.usuario_actualizacion = request.user.username
                obj.save()
            messages.success(request, u"Se anuló el pago satisfactoriamente")
            return HttpResponseRedirect(reverse("lista_pago_proveedores"))

    except Pago.DoesNotExist:
        transaction.rollback()
        raise Http404
    except ErrorPagoProveedores, e:
        transaction.rollback()
        messages.error(request, unicode(e))
        return HttpResponseRedirect(reverse("lista_pago_proveedores"))


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def reversar_pago(request, id):
    """
    Vista que sirve para reversar un cobro
    :param request:
    :param id:
    :return:
    """
    print 'estoy tratando de reversar el pago: '
    try:
        print 'estoy reversando una cosa'
        pago = Pago.objects.get(id=id)
        now = datetime.datetime.now()
        new_pago = copiar_modelo(pago)

        if pago.tipo_comprobante_id == 27 or pago.tipo_comprobante_id == 28:
            print 'el pago es un reverso'
            respuesta = {"status": 2,
                         "mensaje": u"El pago que quiere reversar procede de una reversión, "
                                    u"por favor verifique"}
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')

        elif pago.esta_reversado():
            print 'el pago eta reversado'
            respuesta = {"status": 2,
                         "mensaje": u"El pago que quiere reversar ya se encuentra reversado, "
                                    u"por favor verifique"}
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')

        elif pago.pago_id is not None or pago.tipo_comprobante_id == 16:  # Es una reversion o una Ret en ventas
            print 'el pago es no none o ekl comoprobante 16'
            respuesta = {"status": 2,
                         "mensaje": u"No se puede reversar este comprobante, por favor verifíque"}
            resultado = json.dumps(respuesta)
            return HttpResponse(resultado, mimetype='application/json')

        else:
            print str(pago.concepto)
            cxp_pago = CXP_pago.objects.filter(pago=pago)
            if request.method == 'POST':
                reversar_form = ReversionCuentasPagarForm(request.POST)
                #reversar_form.concepto = pago.concepto
                if reversar_form.is_valid():
                    if not esta_periodo_contable(reversar_form.getFecha()):
                        respuesta = {"status": 2,
                                     "mensaje": u"No se puede reversar este pago ya que la fecha de "
                                                u"registro es menor igual al cierre contable"}
                        resultado = json.dumps(respuesta)
                        return HttpResponse(resultado, mimetype='application/json')
                    else:
                        new_pago.num_comp = get_num_comp(28, reversar_form.getFecha(), True, request)

                        if pago.fecha_reg > reversar_form.getFecha():
                            respuesta = {"status": 2,
                                         "mensaje": u"La fecha que ingresó es menor a la fecha del pago que quiere "
                                                    u"reversar por favor verifique"}
                            resultado = json.dumps(respuesta)
                            return HttpResponse(resultado, mimetype='application/json')
                        elif new_pago.num_comp == -1:
                            respuesta = {"status": 2,
                                         "mensaje": u"La fecha que ingresó es menor a la última reversión que "
                                                    u"realizó, por favor verifique"}
                            resultado = json.dumps(respuesta)
                            return HttpResponse(resultado, mimetype='application/json')

                        new_pago.tipo_comprobante_id = 28
                        new_pago.fecha_reg = reversar_form.getFecha()
                        new_pago.fecha_creacion = now
                        new_pago.pago_id = pago
                        new_pago.fecha_actualizacion = None
                        new_pago.usuario_actualizacion = ''
                        new_pago.usuario_creacion = request.user.username
                        if reversar_form.getCconcepto() == "" or reversar_form.getCconcepto().isspace():
                            new_pago.concepto = u"Reversar: " + new_pago.num_comp + u" " + pago.concepto
                        else:
                            new_pago.concepto = reversar_form.getCconcepto()
                        new_pago.save()

                        # caso cruce de documento y cruce de cuentas
                        if pago.forma_pago_id == 5 or pago.forma_pago_id == 6:
                            for obj in cxp_pago:
                                cuentas_por_pagar = Cuentas_por_Pagar.objects.get(id=obj.cuentas_x_pagar_id)
                                cuentas_por_pagar.pagado = cuentas_por_pagar.pagado - obj.monto

                                cuentas_por_pagar.fecha_actualizacion = datetime.datetime.now()
                                cuentas_por_pagar.usuario_actualizacion = request.user.username
                                cuentas_por_pagar.save()

                                new_cxp_pago = copiar_modelo(obj)
                                new_cxp_pago.pago = new_pago
                                new_cxp_pago.concepto = u"Reversado: " + new_cxp_pago.concepto
                                new_cxp_pago.naturaleza = new_cxp_pago.cuentas_x_pagar.naturaleza  # Tendra la misma naturaleza que su cta x pagar
                                new_cxp_pago.save()
                        else:
                            for obj in cxp_pago:
                                cuentas_por_pagar = Cuentas_por_Pagar.objects.get(id=obj.cuentas_x_pagar.id)
                                new_cxp_pago = copiar_modelo(obj)

                                if cuentas_por_pagar.naturaleza == 2:  # No es un anticipo o Nota de Crédito
                                    cuentas_por_pagar.pagado = cuentas_por_pagar.pagado - obj.monto
                                    new_cxp_pago.naturaleza = 2

                                # caso anticipo
                                # Si el anticipo que desea anular esta cruzado con algún documento, no se
                                # podría anular el pago, primero se necesitaría anular el cruce de documento.
                                else:
                                    # Como es un anticipo anulo la cuenta por pagar ya que se anuló el pago
                                    if cuentas_por_pagar.pagado > 0:
                                        transaction.rollback()
                                        respuesta = {"status": 2,
                                                     "mensaje": u"No se puede reversar el pago, ya que posee un cruce de documentos"
                                                                u", anule dicho cruce para que pueda anular"
                                                                u" este pago "}
                                        resultado = json.dumps(respuesta)
                                        return HttpResponse(resultado, mimetype='application/json')
                                    else:
                                        cuentas_por_pagar.pagado = cuentas_por_pagar.monto

                                    new_cxp_pago.naturaleza = 1

                                cuentas_por_pagar.fecha_actualizacion = now
                                cuentas_por_pagar.usuario_actualizacion = request.user.username
                                cuentas_por_pagar.save()


                                new_cxp_pago.pago = new_pago
                                new_cxp_pago.concepto = u"Reversado: " + new_cxp_pago.concepto
                                new_cxp_pago.save()
                        #insertar un registro en bancos
                        cabecera_asiento = Cabecera_Comp_Contable.objects.get(numero_comprobante=pago.num_comp)

                        new_cabecera_asiento = copiar_modelo(cabecera_asiento)
                        new_cabecera_asiento.concepto_comprobante = reversar_form.getCconcepto()
                        new_cabecera_asiento.fecha = reversar_form.getFecha()
                        new_cabecera_asiento.numero_comprobante = new_pago.num_comp
                        new_cabecera_asiento.tipo_comprobante_id = 28
                        new_cabecera_asiento.save()

                        detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_asiento)

                        for obj in detalle_mayor:
                            new_obj = copiar_modelo(obj)
                            new_obj.fecha_asiento = reversar_form.getFecha()
                            new_obj.cabecera_contable = new_cabecera_asiento
                            if new_obj.dbcr == 'D':
                                new_obj.dbcr = 'H'
                                new_obj.save()
                            else:
                                new_obj.dbcr = 'D'

                                new_obj.save()
                                #11 noviembre el if estaba incompletoo y me hacia None y se caia :o
                                if new_obj.plan_cuenta.tipo is not None and new_obj.plan_cuenta.tipo.id == 1:
                                    print 'es uno'
                                    mibanco = Banco()
                                    Cuentabanconumeroid = Cuenta_Banco.objects.filter(plan_cuenta_id=new_obj.plan_cuenta_id)[0]
                                    mibanco.cuenta_banco_id = Cuentabanconumeroid.id
                                    mibanco.tipo_comprobante_id = 28
                                    mibanco.num_comp = new_pago.num_comp
                                    mibanco.fecha_reg = reversar_form.getFecha()
                                    #if pago.tipo_comprobante_id == 4:
                                    if pago.forma_pago.id == 1:
                                        print 'es egreso'
                                        mibanco.num_cheque = pago.num_cheque
                                        print (str(pago.num_cheque))
                                        print 'en num de cheque'
                                    mibanco.persona = pago.proveedor.razon_social
                                    mibanco.concepto = unicode(reversar_form.getCconcepto())
                                    mibanco.valor = pago.monto
                                    mibanco.naturaleza = 1
                                    mibanco.status = 1
                                    mibanco.usuario_creacion = request.user.username
                                    mibanco.fecha_creacion = datetime.datetime.now()
                                    mibanco.conciliado = False
                                    mibanco.save()
                                    print 'el banco se ha grabado :o :o'
                                    #tabla banco
                                    #grsbar en una tabla
                                    #cuenta_banco_id
                                    #tipo_comprobante_id
                                    #num_comp
                                    #fecha_reg
                                    #num_cheque
                                    #persona
                                    #concepto
                                    #valor
                                    #naturaleza
                                    #status
                                    #usuario_creacion
                                    #fecha_creacion
                                    #conciliado
                                else:
                                    print 'una cosa es none :o el id '

                        messages.success(request, u"Se reversó el pago satisfactoriamente con "
                                                  u"número de comprobante "+new_pago.num_comp)

                        respuesta = {"status": 1, "mensaje": ""}
                        resultado = json.dumps(respuesta)
                        return HttpResponse(resultado, mimetype='application/json')
                else:
                    print 'el form es no valid'
                    respuesta = {"status": 2, "mensaje": u"Ingrese la fecha de la reversión del pago"}
                    resultado = json.dumps(respuesta)
                    return HttpResponse(resultado, mimetype='application/json')

    except Pago.DoesNotExist:
        print 'no hizo nada de lo de arriba :o'
        transaction.rollback()
        respuesta = {"status": 2, "mensaje": u"Recurso no encontrado"}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def detalle_pago_proveedores_pdf(request, id):
    """
    Vista que sirve para mostrar en detalle del pago en formato PDF
    :param request:
    :param id:
    :return:
    """
    try:
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)
        usuario = request.user.first_name+' '+request.user.last_name
        pago = Pago.objects.get(id=id)
        proveedor = pago.proveedor
        cxp_pago = pago.getCxPpago()
        cabecera_asiento = Cabecera_Comp_Contable.objects.get(numero_comprobante=pago.num_comp)
        detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_asiento).order_by("dbcr", "plan_cuenta__codigo")

        html = render_to_string('Cuentas_Pagar/pago_proveedores/comprobante_detalle_pago_proveedores_pdf.html',
                                {'pagesize': 'A4',
                                 "pago": pago,
                                 "request": request,
                                 "empresa": empresa,
                                 "usuario": usuario,
                                 "mayor_cab": cabecera_asiento,
                                 "detalle_may": detalle_mayor,
                                 "detalle_pago": cxp_pago,
                                 "fecha_actual": datetime.datetime.now(),
                                 "proveedor": proveedor}, context_instance=RequestContext(request))
        return generar_pdf_get(html)

    except Pago.DoesNotExist:
        raise Http404


def get_total_pago(detalle_doc_cobrar):
    """
    Obtiene el total del pago
    :param detalle_doc_cobrar:
    :return: total_pago
    """
    total_pago = 0.0

    if detalle_doc_cobrar.is_valid():
        for form in detalle_doc_cobrar:
            informacion = form.cleaned_data
            check = informacion.get("check")
            if check:
                total_pago += float(informacion.get("valor"))

    return total_pago


def validacion_fechas_documentos(detalle_doc_cobrar, cabecera, contador, detalle_docs, request):
    """
    Valida que las fechas de los documentos que se vayan a cruzar sean menor o iguales a la fecha de
    registro del cobro
    :param detalle_doc_cobrar:
    :param cabecera:
    :param contador:
    :param detalle_docs:
    :param request:
    :return:
    """
    if detalle_doc_cobrar.is_valid():
        for form in detalle_doc_cobrar:
            informacion = form.cleaned_data
            check = informacion.get("check")
            if check:
                try:
                    cuenta_cobrar = Cuentas_por_Pagar.objects.get(id=informacion.get("id_cuenta_cobrar"))

                    if cuenta_cobrar.fecha_reg > cabecera.getFecha():
                        contador += 1
                        messages.error(request, u"La fecha del documento a cruzar es mayor a la "
                                                u"fecha de registro del cobro, por favor verifique")
                        errors1 = form._errors.setdefault("fecha_reg", ErrorList())
                        errors1.append(u"Error")

                        errors2 = cabecera._errors.setdefault("fecha", ErrorList())
                        errors2.append(u"Error")
                except (Cuentas_por_Pagar.DoesNotExist, ValueError):
                    contador += 1
                    messages.error(request, u"Cuenta por pagar no encontrada")

    if detalle_docs.is_valid():
        for form in detalle_docs:
            informacion = form.cleaned_data
            valor = informacion.get("valor")

            if valor > 0:
                try:
                    cuenta_cobrar = Cuentas_por_Pagar.objects.get(id=informacion.get("id_cuenta_pagar"))

                    if cuenta_cobrar.fecha_reg > cabecera.getFecha():
                        contador += 1
                        messages.error(request, u"La fecha del documento a cruzar es mayor a la "
                                                u"fecha de registro del cobro, por favor verifique")
                        errors1 = form._errors.setdefault("fecha_reg", ErrorList())
                        errors1.append(u"Error")

                        errors2 = cabecera._errors.setdefault("fecha", ErrorList())
                        errors2.append(u"Error")

                except (Cuentas_por_Pagar.DoesNotExist, ValueError):
                    contador += 1
                    messages.error(request, u"Cuenta por pagar no encontrada")
    return contador


@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def cruce_documentos_cuentas(request):
    """
    Vista para cruce de documentos y cuentas
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    cursor = connection.cursor()
    contador = 0

    formset_detalle_docs = formset_factory(DetallePago, extra=0)
    formset_detalle_docs_cobrar = formset_factory(DetalleDocCruzarForm, extra=0)
    formset_detalle_ctas_cobrar = formset_factory(DetallePagoCruceCta, extra=0)
    cabecera = CabeceraCurceDocForm(initial={"fecha": now.strftime("%Y-%m-%d"),
                                                 "observeciones": "", "forma_pago_cabecera": 1})

    ###############################################################################
    # Variables que ayudan a volver a enviar los datos que ha envido el cliente,  #
    #                si es que existe algun tipo de error                         #
    ###############################################################################
    flag_cd = False
    flag_cc = False
    detalle_docs = formset_detalle_docs(prefix="form_detalle_cta_pagar")
    detalle_doc_cobrar = formset_detalle_docs_cobrar(prefix="form_detalle_cruce_doc")
    detalle_cruce_cta = formset_detalle_ctas_cobrar(prefix="form_detalle_cruce_cta")

    if request.method == "POST":
        cabecera = CabeceraCurceDocForm(request.POST)
        pago = Pago()
        total_pagar = 0.0
        asiento = Cabecera_Comp_Contable()

        #try:
        if cabecera.is_valid():
            proveedor = Proveedores.objects.get(id=cabecera.getProveedor())
            #############################################
            #  Se valida la fecha de cierre de la       #
            #               empresa                     #
            #############################################
            if not esta_periodo_contable(cabecera.getFecha()):
                parametro_empresa = get_parametros_empresa()
                messages.error(request, u"La fecha del registro es menor a la fecha de cierre contable, por favor verifique "+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))
                errors = cabecera._errors.setdefault("fecha", ErrorList())
                errors.append(u"La fecha del registro es menor a la fecha de cierre contable, por favor verifique")
                return render_to_response('Cuentas_Pagar/pago_proveedores/cruce_documentos.html',
                              {"cabecera":cabecera,
                               "flag_cd": flag_cd,
                               "flag_cc": flag_cc,
                               "detalle_docs": detalle_docs,
                               "detalle_doc_cobrar": detalle_doc_cobrar,
                               "detalle_cruce_cta": detalle_cruce_cta
                               }, context_instance=RequestContext(request))
            else:

                ###################################
                #    Caso Cruce de Documentos     #
                ###################################
                if cabecera.getFormaPagoCabecera() == "1":
                    flag_cd = True
                    detalle_docs = formset_detalle_docs(request.POST, prefix="form_detalle_cta_pagar")
                    detalle_doc_cobrar = formset_detalle_docs_cobrar(request.POST, prefix="form_detalle_cruce_doc")
                    print "----------------------------------"
                    print "detalle_doc_cobrar.errors: ", detalle_doc_cobrar.errors
                    print "----------------------------------"
                    # Para obtener el total de los pagos
                    total_pagar = get_total_pago(detalle_doc_cobrar)

                    # Guardar el pago
                    if total_pagar > 0:
                        contador += validacion_fechas_documentos(detalle_doc_cobrar, cabecera, contador, detalle_docs, request)
                        contador += GuardarPagoCruceDocs(cabecera, pago, proveedor, total_pagar, contador, asiento, now, request)
                        if detalle_docs.is_valid():
                            for form in detalle_docs:
                                informacion = form.cleaned_data
                                valor = float(informacion.get("valor"))

                                if valor > 0:
                                    cuenta_pagar = Cuentas_por_Pagar.objects.get(id=informacion.get("id_cuenta_pagar"))
                                    # Guardar Cuenta por pagar
                                    contador += GuardarCuentasPagar(cuenta_pagar, pago, total_pagar, contador, now, request)
                                    break

                        if detalle_doc_cobrar.is_valid():
                            # Guarda los detalles de los documentos que han sido cruzados
                            contador += GuardarDetalleDocCruzados(detalle_doc_cobrar, pago, proveedor, asiento, contador, now, request)
                        else:
                            contador += 1
                            messages.error(request, u"Existen errores en el detalle de cruce de documentos")
                    else:
                        contador += 1
                        messages.error(request, u"El valor a pagar debe ser mayor a $ 0.00, "
                                                u"por favor verifique los valores")

                ####################################
                #        Cruce de Cuentas          #
                ####################################
                else:
                    flag_cc = True
                    detalle_cruce_cta = formset_detalle_ctas_cobrar(request.POST, prefix="form_detalle_cruce_cta")

                    if detalle_cruce_cta.is_valid():
                        total_pagar = get_total_pago(detalle_cruce_cta)

                        if total_pagar <= 0:
                            messages.error(request, u"Se debió ingresar un valor para el cruce de cuentas")
                            contador += 1
                        else:
                            for form in detalle_cruce_cta:
                                informacion = form.cleaned_data
                                check = informacion.get("check")

                                if check:
                                    try:
                                        cuenta_pagar = Cuentas_por_Pagar.objects.get(id=informacion.get("id_cuenta_pagar"))
                                        if cuenta_pagar.fecha_reg > cabecera.getFecha():
                                            contador += 1
                                            messages.error(request, u"La fecha del documento a cruzar es mayor a la "
                                                                    u"fecha de registro del cobro, por favor verifique")
                                            errors1 = form._errors.setdefault("fecha_reg", ErrorList())
                                            errors1.append(u"Error")

                                            errors2 = cabecera._errors.setdefault("fecha", ErrorList())
                                            errors2.append(u"Error")
                                    except (Cuentas_por_Pagar.DoesNotExist, ValueError):
                                        contador += 1
                                        messages.error(request, u"Cuenta por pagar no encontrada")

                            # Guarda el pago con su respectivo asiento contable y la cabecera del asiento
                            contador += GuardarPagoCruceCtas(pago, proveedor, total_pagar, cabecera, asiento, contador, now, request)
                            # Guarda los detalles de cruce de documentos con sus respectivos asientos contalbles
                            contador += GuardardetallesCtasCruzados(detalle_cruce_cta, pago, asiento, contador, now, request)
        else:
            contador += 1
            messages.error(request, u"Por favor llene los campos obligatorios")
            if cabecera.getFormaPagoCabecera() == "1":
                flag_cd = True
                detalle_docs = formset_detalle_docs(request.POST, prefix="form_detalle_cta_pagar")
                detalle_doc_cobrar = formset_detalle_docs_cobrar(request.POST, prefix="form_detalle_cruce_doc")
            else:
                flag_cc = True
                detalle_cruce_cta = formset_detalle_ctas_cobrar(request.POST, prefix="form_detalle_cruce_cta")
        if contador == 0:
            messages.success(request, u"Se ha realizado con éxito la transacción "
                                      u"con número de comprobante: " + asiento.numero_comprobante)
            return HttpResponseRedirect(reverse("lista_pago_proveedores"))
        else:
            transaction.rollback()
        #except:
        #    transaction.rollback()
        #    messages.error(request, u"Existió un problema al generar la transacción")
        #    return HttpResponseRedirect(reverse("lista_pago_proveedores"))

    return render_to_response('Cuentas_Pagar/pago_proveedores/cruce_documentos.html',
                              {"cabecera":cabecera,
                               "flag_cd": flag_cd,
                               "flag_cc": flag_cc,
                               "detalle_docs": detalle_docs,
                               "detalle_doc_cobrar": detalle_doc_cobrar,
                               "detalle_cruce_cta": detalle_cruce_cta,
                               "fecha_actual":now.date()
                               }, context_instance=RequestContext(request))


@csrf_exempt
def cargar_cuentas_pagar(request):
    """
    Función que retorna los documentos que están pendientes
    de pago de la empresa
    :param request:
    :return:
    """
    id = request.POST.get("id")
    try:
        proveedor = Proveedores.objects.get(id=id)
        flag = True
    except:
        proveedor = None
        flag = False

    cuentas_pagar = Cuentas_por_Pagar.objects.exclude(naturaleza=1).filter(status=1, proveedor=proveedor).extra(where=["monto > pagado"])
    if cuentas_pagar:
        print("existe lista cxp")
    else:
        print("no existe lista cxp")
    l_detalle_doc = []
    formset_detalle_docs = formset_factory(DetallePago, extra=0)
    if cuentas_pagar:
        print "la longitud de ctasxpagar",str(len(cuentas_pagar))
        for obj in cuentas_pagar:
            plazo = 0
            fecha_vencimiento = ""
            if obj.plazo is not None and obj.plazo > 0:
                plazo = obj.plazo
                fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)

            l_detalle_doc.append({"id_cuenta_pagar": obj.id, "check": False,
                                  "tipo_doc": obj.documento.descripcion_documento,
                                  "numero_doc": obj.num_doc, "fecha_reg": obj.fecha_reg,
                                  "fecha_ven": fecha_vencimiento,
                                  "total": redondeo(obj.monto, 2),
                                  "saldo": redondeo((obj.monto - obj.pagado), 2),

                                  "referencia": obj.get_referencia(), "valor": 0})

    # Se agrega una línea para anticipos
    l_detalle_doc.append({"id_cuenta_pagar": -1, "check": False, "tipo_doc": "Anticipo",
                          "numero_doc": "", "fecha_reg": "","fecha_ven": "", "total": "", "saldo": "",
                          "referencia": "", "valor": 0})

    detalle_docs = formset_detalle_docs(initial=l_detalle_doc)

    return render_to_response('Cuentas_Pagar/pago_proveedores/tabla_cuentas_pagar.html',
                          {'forms': detalle_docs,
                           'flag': flag}, context_instance=RequestContext(request))

@csrf_exempt
def buscar_proveedor(request):
    """
    Función que sirve para retornar en formato json
    la lista de los proveedores activos de la empresa
    :param request:
    :return:
    """
    respuesta = []
    q = request.GET.get("term", "")
    try:
        cuentas = Proveedores.objects.filter(status=1).filter(Q(nombre__icontains=q) | Q(ruc__icontains=q) | Q(razon_social__icontains=q))[0:5]
        for p in cuentas:
            respuesta.append({"value": p.ruc+" - "+p.nombre, "key": p.ruc+" - "+p.nombre,"tipo": p.tipo_identificacion.codigo})
    except:
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@csrf_exempt
def cargar_cruce_doc(request):
    """
    Funcion que retorna los documentos pendientes de pagos ademas de los documentos que
    se pueden cruzar como por ejemplo notas de crédito o anticipos
    :param request:
    :return: HTML file
    """
    l_det_doc = []
    l_det_cr_doc = []
    formset_det_docs = formset_factory(DetallePago, extra=0)
    formset_det_docs_cob = formset_factory(DetalleDocCruzarForm, extra=0)
    id_prov = request.POST.get("id", None)
    flag = False

    if id_prov is not None:
        try:
            prov = Proveedores.objects.get(id=id_prov)
            detalle_docs = formset_det_docs(prefix="form_detalle_cta_pagar")
            detalle_doc_cobrar = formset_det_docs_cob(prefix="form_detalle_cruce_doc")

            # Documentos que no estan pagados a los proveedores
            cuentas_pagar = Cuentas_por_Pagar.objects.filter(naturaleza=2).filter(status=1, proveedor=prov).extra(where=["monto > pagado"])
            # Documentos de Naturaleza Deudora Ej: "Anticipos y Nortas de Crédito"
            doc_por_cruzar = Cuentas_por_Pagar.objects.filter(naturaleza=1).filter(status=1, proveedor=prov).extra(where=["monto > pagado"])

            i = 0
            if len(cuentas_pagar) != 0:
                print "____________________________________"
                data = {'form-TOTAL_FORMS': '2',
                        'form-INITIAL_FORMS': '0',
                        'form-MAX_NUM_FORMS': ''}
                for obj in cuentas_pagar:
                    plazo = 0
                    fecha_vencimiento = ""
                    if obj.plazo is not None and obj.plazo > 0:
                        plazo = obj.plazo
                        fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)
                    l_det_doc.append({"id_cuenta_pagar": obj.id, "tipo_doc": obj.documento.descripcion_documento,
                                      "fecha_reg": obj.fecha_reg, "numero_doc": obj.num_doc,
                                      "total": obj.monto,
                                      "fecha_ven": fecha_vencimiento,
                                      "saldo": obj.monto - obj.pagado,
                                      "referencia": obj.get_referencia(), "valor": 0})


                    """
                    data["form_detalle_cta_pagar-"+str(i)+"id_cuenta_pagar"] = obj.id
                    data["form_detalle_cta_pagar-"+str(i)+"tipo_doc"] = obj.documento.descripcion_documento
                    data["form_detalle_cta_pagar-"+str(i)+"fecha_reg"] = obj.fecha_reg
                    data["form_detalle_cta_pagar-"+str(i)+"numero_doc"] = obj.num_doc
                    data["form_detalle_cta_pagar-"+str(i)+"total"] = obj.monto
                    data["form_detalle_cta_pagar-"+str(i)+"fecha_ven"] = fecha_vencimiento
                    data["form_detalle_cta_pagar-"+str(i)+"saldo"] = obj.monto - obj.pagado
                    data["form_detalle_cta_pagar-"+str(i)+"referencia"] = obj.get_referencia()
                    data["form_detalle_cta_pagar-"+str(i)+"valor"] = 0
                    i += 1
                    """
                detalle_docs = formset_det_docs(initial=l_det_doc, prefix="form_detalle_cta_pagar")
                #detalle_docs = formset_det_docs(data)

                #print detalle_docs
                for form in detalle_docs:
                    print "XXXXXXXXXXXXXXXXXXXXXX"
                    try:
                        print "formffffffff: ", form.fields['fecha_ven'].label
                    except:
                        pass
                    """
                    print(form.cleaned_data)
                    print "XXXXXXXXXXXXXXXXXXXXXX"
                    info = form.cleaned_data
                    print 'info.get("fecha_reg"): ', info.get("fecha_reg")
                    if info.get("fecha_ven") is not None:
                        if info.get("fecha_ven") < datetime.datetime.now().date():
                            errors = form._errors.setdefault("fecha_ven", ErrorList())
                            errors.append(u"fecha invalida")
                        else:
                            print "EEEEEEEEEEEEEEE"
                            print "No"
                    else:
                            print "ZEEEEEEEEEEEEEEE"
                            print "ZNo"
                    """
            else:
                flag = True

            detalle_doc_cobrar.is_valid()

            if len(doc_por_cruzar) != 0:
                for obj in doc_por_cruzar:
                    plazo = 0
                    fecha_vencimiento = ""
                    if obj.plazo is not None and obj.plazo > 0:
                        plazo = obj.plazo
                        fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)

                    l_det_cr_doc.append({"id_cuenta_cobrar": obj.id, "check": False,
                                         "tipo_doc": obj.documento.descripcion_documento, "numero_doc": obj.num_doc,
                                         "fecha_reg": obj.fecha_reg, "total": obj.monto,
                                         "fecha_ven": fecha_vencimiento,
                                         "saldo": obj.monto - obj.pagado, "referencia": obj.get_referencia(),
                                         "valor": 0})

                detalle_doc_cobrar = formset_det_docs_cob(initial=l_det_cr_doc, prefix="form_detalle_cruce_doc")

            else:
                flag = True

            if flag:
                messages.success(request, u"No se encuentra documento que pueda ser cruzado. "
                                          u"Intente con cruce de cuentas")

        except (Proveedores.DoesNotExist, ValueError):
            prov = None
            detalle_docs = None
            detalle_doc_cobrar = None
            flag = True

    else:
        prov = None
        detalle_docs = None
        detalle_doc_cobrar = None
        flag = True

    return render_to_response('Cuentas_Pagar/pago_proveedores/tabla_cruce_doc.html',
                                  {"proveedor": prov,
                                   'formulario_facts': detalle_docs,
                                   "flag": flag,
                                   "formularios": detalle_doc_cobrar,
                                   }, context_instance=RequestContext(request))


@csrf_exempt
def cargar_cruce_cta(request):
    """
    Funcion que retorna los documentos pendientes de pagos ademas de los documentos que
    se pueden cruzar como por ejemplo notas de crédito o anticipos
    :param request:
    :return: HTML file
    """
    try:
        l_det_cruce_cta = []

        formset_det_docs = formset_factory(DetallePago, extra=0)
        formset_det_ctas_cobrar = formset_factory(DetallePagoCruceCta, extra=0)

        id_prov = request.POST.get("id")
        prov = Proveedores.objects.get(id=id_prov)
        flag = False

        detalle_docs = formset_det_docs(prefix="form_detalle_cta_pagar")
        detalle_doc_cobrar_cta = formset_det_ctas_cobrar(prefix="form_detalle_cruce_cta")

        # Todos los documentos de la tabla cuentas por pagar, para hacer cruce de cuenta
        cuentas_cruzar = Cuentas_por_Pagar.objects.filter(status=1, proveedor=prov).extra(where=["monto > pagado"], params=[])


        ###############################################
        #  Llenando las formularios de cta por cruzar #
        ###############################################
        if len(cuentas_cruzar) != 0:
            for obj in cuentas_cruzar:
                plazo = 0
                fecha_vencimiento = ""
                if obj.plazo is not None and obj.plazo > 0:
                    plazo = obj.plazo
                    fecha_vencimiento = obj.fecha_reg + datetime.timedelta(days=plazo)

                l_det_cruce_cta.append({"id_cuenta_pagar": obj.id, "check": False,
                                        "tipo_doc": obj.documento.descripcion_documento,
                                        "numero_doc": obj.num_doc, "fecha_reg": obj.fecha_reg,
                                        "fecha_ven": fecha_vencimiento,
                                        "total": obj.monto, "saldo": obj.monto - obj.pagado,
                                        "referencia": obj.get_referencia(), "cuenta": "", "valor": "0.00"})

            detalle_doc_cobrar_cta = formset_det_ctas_cobrar(initial=l_det_cruce_cta, prefix="form_detalle_cruce_cta")
        else:
            flag = True

        if flag:
            messages.success(request, u"No se encuentra documento pendiente de pago que pueda ser cruzado con cuentas")
    except:
        prov = None
        flag = True
        detalle_docs = None
        detalle_doc_cobrar_cta = None

    return render_to_response('Cuentas_Pagar/pago_proveedores/tabla_cruce_cta.html',
                              {"proveedor": prov,
                               'formulario_facts': detalle_docs,
                               "flag": flag,
                               "formularios_ctas": detalle_doc_cobrar_cta}, context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
def lista_cuentas_x_pagar_iniciales(request):
    """
    Vista en la cual se enlistan
    todos los pagos
    :param request:
    :return:
    """
    buscador = BuscadorCuentasxPagarinicialPagos(request.GET)

    cxp_lista = busqueda_cuentas_pagar_ini(buscador)
    paginacion = Paginator(cxp_lista, 10)
    numero_pagina = request.GET.get("page")

    try:
        pagos = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        pagos = paginacion.page(1)
    except EmptyPage:
        pagos = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = pagos.number
    arreglo_num_paginado = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Cuentas_Pagar/saldos_iniciales/lista_saldos_iniciales_cxp.html',
                              {"objetos": pagos, "buscador": buscador, "total_paginas": total_paginas,
                               "numero": numero, "arreglo_paginado": arreglo_num_paginado},
                              context_instance=RequestContext(request))


def tiene_doc_repetidos(cta_por_pagar):
    a = []
    for form in cta_por_pagar:
        informacion = form.cleaned_data
        tipo_doc = informacion.get("tipo_doc")
        num_doc = informacion.get("num_doc")

        if (tipo_doc, num_doc) in a:
            errors = form._errors.setdefault("num_doc", ErrorList())
            errors.append(u"El documento que ingresó ya se encuentra registrado "
                          u"en el sistema")
            return True

        a.append((tipo_doc, num_doc))

    return False


def redireccionar_cuenta_pagar_inicial(request):
    """
    redirecciona dependiendo de la acción que desee el usuario
    :return:
    """
    if request.GET.get("grabar_agregar", "") == "1":
        full_redirect_url = reverse("cuentas_x_pagar_iniciales")

    else:
        full_redirect_url = reverse("lista_cuentas_x_pagar_iniciales")

    return full_redirect_url

@login_required(login_url="/")
@csrf_exempt
@transaction.commit_on_success
def cuentas_x_pagar_iniciales(request):
    empresa = get_parametros_empresa()
    cabecera = CabeceraCXPInicialForm(initial={"fecha_reg": empresa.fecha_inicio_sistema - timedelta(days=1)})
    cta_por_pagar_formset = formset_factory(CuentasPagarInicialForm, extra=1)
    cta_por_pagar = cta_por_pagar_formset()
    now = datetime.datetime.now()
    cont_doc = 0
    contador = 0

    if request.method == "POST":
        cabecera = CabeceraCXPInicialForm(request.POST)
        cta_por_pagar = cta_por_pagar_formset(request.POST)

        if cabecera.is_valid():
            proveedor = Proveedores.objects.get(id=cabecera.getProveedor())
            if cta_por_pagar.is_valid():
                if not tiene_doc_repetidos(cta_por_pagar):  # Si no tiene documentos repetidos ingresados
                    for form in cta_por_pagar:
                        cxp = Cuentas_por_Pagar()
                        informacion = form.cleaned_data
                        documento = Documento.objects.get(id=informacion.get("tipo_doc"))
                        cxp.documento = documento
                        if Cuentas_por_Pagar.objects.filter(num_doc=informacion.get("num_doc"),
                                                            proveedor=proveedor,
                                                            documento=documento).exists():
                            errors = form._errors.setdefault("num_doc", ErrorList())
                            errors.append(u"El documento que ingresó ya se encuentra registrado "
                                          u"en el sistema, por favor verifique")
                            cont_doc += 1

                        cxp.num_doc = informacion.get("num_doc")
                        cxp.proveedor = proveedor
                        cxp.monto = informacion.get("monto")
                        cxp.pagado = 0
                        if documento.codigo_documento == "04" or documento.codigo_documento == "AN":
                            cxp.naturaleza = 1
                        else:
                            cxp.naturaleza = 2
                        cxp.plazo = informacion.get("plazo")
                        cxp.fecha_reg = empresa.fecha_inicio_sistema - timedelta(days=1)
                        cxp.fecha_creacion = now
                        cxp.usuario_creacion = request.user.username
                        cxp.save()
                else:
                    messages.error(request, u"No puede ingresar dos documentos iguales del "
                                            u"mismo proveedor, por favor verifique")
                    contador += 1

                if cont_doc > 0:
                    messages.error(request, u"El documento que ingresó ya se "
                                            u"encuentra registrado en el sistema, por favor verifique")
                    transaction.rollback()

                elif contador > 0:
                    transaction.rollback()

                else:
                    messages.success(request, u"Se ha guardado los saldos de las cuentas por pagar satisfactoriamente")
                    return HttpResponseRedirect(redireccionar_cuenta_pagar_inicial(request))
        else:
            pass

    return render_to_response('Cuentas_Pagar/saldos_iniciales/saldos_iniciales_cxp.html',
                              {"cabecera": cabecera,
                               'cta_por_pagar': cta_por_pagar,
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
def eliminar_cuentas_x_pagar_iniciales(request, id):
    """
    Vista en la cual se enlistan
    todos los pagos
    :param request:
    :return:
    """

    cxp = Cuentas_por_Pagar.objects.get(id=id)
    empresa = get_datos_empresa()
    parametros_emp = Empresa_Parametro.objects.using("base_central").get(id=empresa.id)

    if cxp.pagado == 0:
        if cxp.fecha_reg == parametros_emp.fecha_inicio_sistema - timedelta(days=1):
            cxp.status = 0
            cxp.save()
            messages.success(request, u"Se ha eliminado con éxito la cuenta por pagar " +
                             cxp.documento.descripcion_documento + u" #"
                             + cxp.num_doc + u" del proveedor: " + cxp.proveedor.razon_social)

        else:
            messages.error(request, u"Imposible realizar esta operación")
    else:
        messages.error(request, u"La cuenta por pagar que quiere eliminar tiene pagos a su favor, "
                                u"por favor verifique")

    return HttpResponseRedirect(reverse('lista_cuentas_x_pagar_iniciales'))


# AJAX
@login_required(login_url="/")
@csrf_exempt
def get_ultimo_cheque(request):
    """
    Retorna el número del último núm de cheque usado
    :return:
    """
    try:
        id_cta_banco = request.POST.get("id_cta_banco")
        cta_banco = Cuenta_Banco.objects.get(id=id_cta_banco)

        respuesta = {"status": 1,
                     "num_cheque": cta_banco.secuencia_cheque,
                     "es_cta_corriente": cta_banco.tipo_cuenta_banco_id == 2}
        resultado = json.dumps(respuesta)

    except Cuenta_Banco.DoesNotExist:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)

    return HttpResponse(resultado, mimetype='application/json')


# AJAX
@login_required(login_url="/")
@csrf_exempt
def get_fecha_actual_comprobante(request):
    """
    Retorna el número del último núm de cheque usado
    :return:
    """

    try:
        id = request.POST.get("id")
        if id == "1":  # Cheque  -  Egreso
            secuencia_tipo_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=4)
        elif id == "2":  # Efectivo  -  Pago
            secuencia_tipo_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=9)
        elif id == "5":  # Transferencia
            secuencia_tipo_comp = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=23)
        else:
            secuencia_tipo_comp = None

        if secuencia_tipo_comp is not None:
            respuesta = {"status": 1,
                         "fecha": secuencia_tipo_comp.fecha_actual.strftime("%Y-%m-%d"),
                         }
        else:
            respuesta = {"status": "",
                         "fecha": "",
                         }

        resultado = json.dumps(respuesta)

    except SecuenciaTipoComprobante.DoesNotExist:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)

    return HttpResponse(resultado, mimetype='application/json')


# AJAX
@login_required(login_url="/")
@csrf_exempt
def get_cheques_provisionados(request):
    """
    Retorna el número del último núm de cheque usado
    :return:
    """
    respuesta = []
    try:
        id_banco = request.POST.get("id")
        banco = Banco.objects.filter(cuenta_banco_id=id_banco, status=3)
        respuesta.append({"status": 1, "id": "",
                          "fecha": "", "descripcion": ""})

        for obj in banco:
            print "fecha_reg: ", obj.fecha_reg
            respuesta.append({"status": 1,
                              "id": obj.id,
                              "fecha": obj.fecha_reg.strftime("%Y-%m-%d"),
                              "descripcion": "Chq #" + obj.num_cheque + " - " + obj.num_comp
                              })
        resultado = json.dumps(respuesta)

    except Banco.DoesNotExist:
        respuesta = {"status": 2, "mensaje": "Error interno en el servidor"}
        resultado = json.dumps(respuesta)

    return HttpResponse(resultado, mimetype='application/json')