#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.core.paginator import *
from contabilidad.formularios.ProveedorForm import *
from contabilidad.formularios.ClienteForm import BuscadorFormClienteProveedor
from contabilidad.models import *
import datetime
from django.forms.util import ErrorList
from librerias.funciones.paginacion import *
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
import json
from django.db import IntegrityError, transaction
from django.db.models import Q
from django.contrib.auth.models import User
from django.template.loader import render_to_string
import time
from django.http import HttpResponse
from django import template
from librerias.funciones.validacion_rucs import *
register = template.Library()
from django.core.paginator import *
from librerias.funciones.funciones_vistas import *
from contabilidad.funciones.proveedores_func import *
import operator
from django.db import connection
from librerias.funciones.permisos import *
from contabilidad.formularios.ContactosForm import FormContacto, ContactoFormSet, ContactoFormSetOnlyRead

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def proveedores(request):
    '''
    Vista que permite listar los proveedores y realizar las búsquedas
    :param request:
    :return:
    '''
    tipo = 2                     # Para seleccionar Busqueda de Proveedor
    buscador = BuscadorFormClienteProveedor(request.GET)
    proveedores = busqueda_clientes_proveedores(buscador, tipo)
    paginacion = Paginator(proveedores, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    try:
        proveedores = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        proveedores = paginacion.page(1)
    except EmptyPage:
        proveedores = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = proveedores.number
    lista_final = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('proveedor/proveedores.html', {"objetos": proveedores, "total_paginas": total_paginas,
                               "numero": numero, "request": request,
                               "buscador": buscador,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               "arreglo_paginado": lista_final}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def registrar_proveedor(request):
    '''
    Vista que realiza el proceso de guardar la información de un proveedor al sistema
    :param request:
    :return:
    '''
    today = datetime.datetime.now()
    formulario = ProveedorForm()
    contador = 0
    tipo = 1
    form_contactos = formset_factory(FormContacto, extra=1, formset=ContactoFormSet)
    formulario_contactos = form_contactos(prefix="formset_contactos")
    now = datetime.datetime.now()

    #try:
    if request.method == "POST":
        formulario = ProveedorForm(request.POST)
        #######################################
        #  Registro de un proveedor por ajax  #
        #######################################
        if request.is_ajax():
            respuesta = []
            proveedor_cmp = Proveedores()
            #try:
            return agregar_proveedor_ajax(proveedor_cmp, formulario, request)
            #except:
            #    respuesta.append({"status": 0, "mensaje": "Error General"})
            #    resultado = json.dumps(respuesta)
            #    return HttpResponse(resultado, mimetype='application/json')
        else:
            formulario_contactos = form_contactos(request.POST, prefix="formset_contactos")

            if formulario.is_valid():
                proveedor_obj = Proveedores()
                contador = GuardarProveedor(formulario, proveedor_obj, today, request)
                if contador == 0:
                    if formulario_contactos.is_valid():
                        for form in formulario_contactos:
                            contacto = Contacto()
                            informacion = form.cleaned_data
                            contacto.tipo = 2
                            contacto.cliente_proveedor_id = proveedor_obj.id
                            contacto.nombres = informacion.get("nombres")
                            contacto.apellidos = informacion.get("apellidos")
                            contacto.telefono = informacion.get("telefono_contacto")
                            contacto.movil = informacion.get("movil")
                            contacto.correo = informacion.get("email")
                            try:
                                contacto.departamento = Departamento.objects.filter(status=1).get(id=informacion.get("departamento"))
                            except:
                                pass
                            contacto.fecha_creacion = now
                            contacto.usuario_creacion = request.user.username
                            contacto.save()
                    messages.success(request, u"El Proveedor se registró exitósamente")
                    return HttpResponseRedirect(reverse("proveedores"))
            else:
                lista_errores = "Por favor verifique los siguientes campos: "

                for i in formulario.errors:
                    lista_errores = lista_errores +(unicode(i)) + ", "
                if formulario.errors:
                    messages.error(request, unicode(lista_errores[0:-2]))
                for i in formulario:
                    if i.errors:
                        i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

            if contador > 0:
                transaction.rollback()
    #except:
    #    transaction.rollback()
    #    messages.error(request, u"Error al agregar proveedor")

    return render_to_response("proveedor/registrar_proveedor.html", {"formulario": formulario, "tipo": tipo,
                                                                     "formulario_contactos": formulario_contactos},
                                                                      context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def modificar_proveedor(request, id, numero):
    '''
    Modificar la información de un proveedor
    :param request:
    :param id:
    :param numero:
    :return:
    '''
    now_ac = datetime.datetime.now()
    contador = 0
    pago = 0
    sector = 0
    ciudad = 0
    tipo = 2
    now = datetime.datetime.now()
    contactos = Contacto.objects.filter(cliente_proveedor_id=id, tipo=2)

    try:

        nuevo = Proveedores.objects.exclude(status=0).get(id=id)

        if nuevo.forma_pago is not None:
            pago = nuevo.forma_pago.id

        try:
            if nuevo.actividad is not None:
                sector = nuevo.actividad.id
        except Sector.DoesNotExist:
            pass

        try:
            if nuevo.ciudad is not None:
                ciudad = nuevo.ciudad.id
        except:
            ciudad = 0

        try:
            if nuevo.email != "" or nuevo.email is not None:
                email = nuevo.email
            else:
                email = ""
        except:
            email = ""

        formulario = ProveedorForm(initial={"num_identificacion": nuevo.ruc, "nombre": nuevo.nombre,
                                            "direccion": nuevo.direccion, "razon_social": nuevo.razon_social,
                                            "tipo_proveedor": nuevo.tipo_proveedor.id, "telefono": nuevo.telefono,
                                            "monto_credito": nuevo.monto_credito, "plazo": nuevo.plazo,
                                            "actividad": sector, "ciudad": ciudad, "email": email,
                                            "forma_pago": pago, "tipo_identificacion": nuevo.tipo_identificacion.codigo,
                                            "grupo": nuevo.plan_cuenta.id})

        lista_contactos = []
        for obj in contactos:
            lista_contactos.append({"id": obj.id, "apellidos": obj.apellidos, "nombres": obj.nombres,
                                    "departamento": obj.departamento_id, "email": obj.correo,
                                    "telefono_contacto": obj.telefono, "movil": obj.movil})

        formset_contactos = formset_factory(FormContacto, extra=1, formset=ContactoFormSet)
        formulario_contactos = formset_contactos(initial=lista_contactos)

    except:
        messages.error(request, u"Error al obtener la información del  proveedor.")
        return HttpResponseRedirect(reverse("proveedores"))

    #try:
    if request.method == "POST":
        formulario = ProveedorForm(request.POST)
        formulario_contactos = formset_contactos(request.POST)
        if formulario.is_valid():
            contador = EditarProveedor(formulario, nuevo, now_ac, request)
            if contador == 0:
                if formulario_contactos.is_valid():
                    Contacto.objects.filter(cliente_proveedor_id=id, tipo=2).delete()  # Borran los registros anteriores
                    for form in formulario_contactos:
                        informacion = form.cleaned_data
                        contacto = Contacto()
                        contacto.tipo = 2
                        contacto.cliente_proveedor_id = id
                        contacto.nombres = informacion.get("nombres")
                        contacto.apellidos = informacion.get("apellidos")
                        contacto.telefono = informacion.get("telefono_contacto")
                        contacto.movil = informacion.get("movil")
                        contacto.correo = informacion.get("email")
                        try:
                            contacto.departamento = Departamento.objects.filter(status=1).get(id=informacion.get("departamento"))
                        except:
                            pass
                        contacto.fecha_creacion = now
                        contacto.usuario_creacion = request.user.username
                        contacto.save()

                messages.success(request, u"El Proveedor se  modificó exitosamente")
                return HttpResponseRedirect(reverse("proveedores"))

        else:

             lista_errores = "Por favor verifique los siguientes campos: "
             for i in formulario.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "

             if formulario.errors:
                messages.error(request, unicode(lista_errores[0:-2]))

             for i in formulario:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

        if contador > 0:
            transaction.rollback()
    #except:
    #    transaction.rollback()
    #    messages.error(request, "Error al modificar el proveedor")

    return render_to_response("proveedor/registrar_proveedor.html",
                              {"formulario": formulario, "obj": nuevo, "numero": numero,
                               "tipo": tipo,
                               "formulario_contactos": formulario_contactos,
                               "id": id},
                               context_instance=RequestContext(request))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def detalle_proveedor(request, id, numero):
    '''
    Vista que presenta la información del proveedor
    :param request:
    :param id:
    :param numero:
    :return:
    '''
    tipo = 3
    actividad = 0
    pago = 0
    ciudad = 0

    try:
        proveedor = Proveedores.objects.get(id=id)
        contactos = Contacto.objects.filter(cliente_proveedor_id=id, tipo=2)

        if proveedor.forma_pago is not None:
            pago = proveedor.forma_pago.id
        try:
            if proveedor.actividad is not None:
                actividad = proveedor.actividad.id
        except Sector.DoesNotExist:
            actividad = 0

        if proveedor.ciudad is not None:
            ciudad = proveedor.ciudad.id

        formulario = ProveedorFormOnlyRead(initial={"nombre": proveedor.nombre, "razon_social": proveedor.razon_social,
                                                    "num_identificacion": proveedor.ruc, "telefono": proveedor.telefono,
                                                    "direccion": proveedor.direccion, "forma_pago": pago,
                                                    "monto_credito": proveedor.monto_credito, "ciudad": ciudad,
                                                    "actividad": actividad, "tipo_proveedor": proveedor.tipo_proveedor.id,
                                                    "tipo_identificacion": proveedor.tipo_identificacion.codigo,
                                                    "grupo": proveedor.plan_cuenta.id, "plazo": proveedor.plazo,
                                                    "email": proveedor.email})
        lista_contactos = []
        for obj in contactos:
            lista_contactos.append({"apellidos": obj.apellidos, "nombres": obj.nombres,
                                    "departamento": obj.departamento_id, "email": obj.correo,
                                    "telefono_contacto": obj.telefono, "movil": obj.movil})

        formset_contactos = formset_factory(ContactoFormSetOnlyRead, extra=0)
        if lista_contactos:
            formulario_contactos = formset_contactos(initial=lista_contactos)
        else:
            formulario_contactos = None
    except:
       messages.error(request, u"Exiten problemas al obtener la información del proveedor, "
                               u"por favor comuníquese con el administrador")
       return HttpResponseRedirect(reverse("proveedores"))

    return render_to_response('proveedor/registrar_proveedor.html', {"objeto": proveedor, "id": id, "numero": numero,
                               "formulario": formulario, "tipo": tipo, "formulario_contactos": formulario_contactos,
                               "request": request}, context_instance=RequestContext(request))

@csrf_exempt
@login_required(login_url='/')
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def eliminar_proveedor(request, id):
    '''
    Cambiar el status del proveedor
    Ej:
        status = 0
    :param request:
    :param id:
    :return:
    '''
    now_ac = datetime.datetime.now()

    try:
        proveedor = Proveedores.objects.get(id=id)

        if CompraCabecera.objects.filter(status=1).filter(proveedor=proveedor).exists():
            messages.error(request, u"El proveedor no puede ser eliminado, posee transacciones.")
            return HttpResponseRedirect(reverse("proveedores"))

        elif Cuentas_por_Pagar.objects.filter(status=1).filter(proveedor=proveedor).exists():
            messages.error(request, u"El proveedor no puede ser eliminado, posee Cuentas por Pagar.")
            return HttpResponseRedirect(reverse("proveedores"))

        else:

            if proveedor.status != 0:
                proveedor.status = 0
                proveedor.usuario_actualizacion = request.user.username
                proveedor.fecha_actualizacion = now_ac
                proveedor.save()
                messages.success(request, u"El proveedor ha sido eliminado exitósamente")
                return HttpResponseRedirect(reverse("proveedores"))

    except:
        messages.error(request, u"El proveedor no se encuentra registrado.")
    return HttpResponseRedirect(reverse("proveedores"))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def cambiar_estado_proveedor(request, id):
    '''
    Vista que cambia el estado del proveedor
    Ej:
        status = 1 (Activo)
        status = 2 (Inactivo)
    :param request:
    :param id:
    :return:
    '''
    try:
        proveedor = Proveedores.objects.get(id=id)
        now_ac = datetime.datetime.now()
        if proveedor.status == 1:
            proveedor.status = 2
            proveedor.usuario_actualizacion = request.user.username
            proveedor.fecha_actualizacion = now_ac
            messages.success(request, u'Se inactivo al proveedor : "'+unicode(proveedor.razon_social) + u'" exitosamente')

        elif proveedor.status == 2:
            proveedor.status = 1
            proveedor.usuario_actualizacion = request.user.username
            proveedor.fecha_actualizacion = now_ac
            messages.success(request, u'Se activó al proveedor : "'+unicode(proveedor.razon_social) + u'" exitosamente')
        proveedor.save()
    except:
        messages.error(request, u"Exisiteron problemas al cambiar el estado.")
    return HttpResponseRedirect(reverse("proveedores"))


