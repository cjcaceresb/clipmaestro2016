#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
import datetime
from django.db.models import Q
from contabilidad.formularios.ItemForm import *
from librerias.funciones.paginacion import *
from django.forms.util import ErrorList
from django.core.paginator import *
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django import template
from administracion.models import *
from django.db import IntegrityError, transaction
import operator
from librerias.funciones.permisos import *
from librerias.funciones.funciones_vistas import *
register = template.Library()


def busqueda_items(buscador):
    """
    Funcion para buscar los items(buscador)
    """
    predicates = []
    predicates.append(('codigo__icontains', buscador.getCodigo()))

    if buscador.getNombre() != "":
        predicates.append(('nombre__icontains', buscador.getNombre()))
    if buscador.getCategoria() is not None:
        predicates.append(('categoria_item', buscador.getCategoria()))

    q_list = [Q(x) for x in predicates]
    entries = Item.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).distinct().order_by("codigo").distinct()
    return entries


@login_required(login_url='/')
def lista_items(request):
    buscador = BusquedaItemForm(request.GET)
    items = busqueda_items(buscador)
    paginacion = Paginator(items, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    try:
        items = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        items = paginacion.page(1)
    except EmptyPage:
        items = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    cantidad_items = paginacion.count
    numero = items.number

    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('Items/lista_items.html', {"objetos": items,
                                                         "total_paginas": total_paginas, "numero": numero,
                                                         "buscador": buscador,
                                                         "arreglo_paginado": lista, "request": request,
                                                         "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                                                         "cantidad_items": cantidad_items}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def buscar_categoria(request):
    respuesta = []
    q = request.GET.get("term", "")
    try:
        categorias = Categoria_Item.objects.filter(status=1).filter(Q(descripcion__icontains=q))[0:5]

        if len(categorias) == 0:
            respuesta.append({"value": "No se encuentra registrado la categoría", "key": "No se encuentra registrado la Categoría de Item"})
        for p in categorias:
            respuesta.append({"value": p.descripcion, "key": p.descripcion})
    except:
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url='/')
@csrf_exempt
def detalle_item(request, id):
    tipo = 3

    try:
        item = Item.objects.get(id=id)

        if item.iva == 12:
            cod_iva = "01"
        elif item.iva == 0:
            cod_iva = "02"
        else:
            cod_iva = "03"

        formulario = ItemFormReadOnly(initial={
            "codigo": item.codigo,
            "nombre": item.nombre,
            "categoria": item.categoria_item.id,
            "descripcion": item.descripcion,
            "unidad": item.unidad.id,
            "iva": cod_iva,
            "min_inventario": item.min_inventario,
            "ice_valor": item.porc_ice,
            "precio1": item.precio1,
            "precio2": item.precio2,
            "precio3": item.precio3,
            "costo": item.costo
        })

    except:
        messages.error(request, u"Error al obtener la información del Item.")
        return HttpResponseRedirect(reverse("lista_items"))

    return render_to_response('Items/registrar_item.html',
                              {"objetos": item,
                               "id": id,
                               "formulario": formulario,
                               "tipo": tipo}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def registrar_items(request):
    cont = 0
    tipo = 1
    today = datetime.datetime.now()
    formulario = itemForm()
    if request.method == "POST":
        formulario = itemForm(request.POST)
        if formulario.is_valid():
            item = Item()
            item.descripcion = formulario.getDescripcion()
            item.nombre = formulario.getNombre()
            item.codigo = formulario.getCodigo()
            item.categoria_item = Categoria_Item.objects.filter(status=1).get(id=formulario.getCategoria())

            if formulario.getCosto() != "":
                item.costo = formulario.getCosto()
            else:
                item.costo = 0.0

            if formulario.getPrecio1() != "":
                item.precio1 = formulario.getPrecio1()
            else:
                item.precio1 = 0.0

            if formulario.getPrecio2() != "":
                item.precio2 = formulario.getPrecio2()
            else:
                item.precio2 = 0.0

            if formulario.getPrecio3() != "":
                item.precio3 = formulario.getPrecio3()
            else:
                item.precio3 = 0.0

            if formulario.getIva() != None:
                if formulario.getIva() == "01":
                    item.iva = 12.0
                elif formulario.getIva() == "02":
                    item.iva = 0.0
                else:
                    item.iva = 0.0

            if formulario.getUnidad() != "":
                item.unidad = Unidad.objects.filter(status=1).get(id=formulario.getUnidad())
            if formulario.getMinInventario() != "":
                item.min_inventario = formulario.getMinInventario()
            if formulario.getIceValor() != "":
                item.porc_ice = formulario.getIceValor()

            ############ Validación Código ###########################################################
            if Item.objects.exclude(status=0).filter(codigo=item.codigo).exists():
                cont += 1
                errors = formulario._errors.setdefault("codigo", ErrorList())
                errors.append(u"El Código del Item ya ha sido ingresado, por favor corrija.")
                messages.error(request, u"El Código del Item ya ha sido ingresado, por favor corrija.")

            ##################################### CATEGORIA ###########################################
            if item.categoria_item.status != 1:
                cont += 1
                messages.error(request, u"Al registrar el item por favor verifique si la categoría está activa.")

            if cont == 0:
                item.usuario_creacion = request.user.username
                item.fecha_creacion = today
                item.save()
                messages.success(request, u"Se ha registado exitosamente el Item: "+str(item.codigo)+" - "+unicode(item.nombre))
                return HttpResponseRedirect(reverse("lista_items"))

            else:
                transaction.rollback()

        else:
            lista_errores = "Por favor verifique los siguientes campos: "

            for i in formulario.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "
            if formulario.errors:
                messages.error(request, unicode(lista_errores[0:-2]))
            for i in formulario:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

    return render_to_response("Items/registrar_item.html",
                              {"today": today,
                               "formulario": formulario,
                               "tipo": tipo,
                               "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def editar_items(request, id):
    now_ac = datetime.datetime.now()
    cont = 0
    tipo = 2

    try:
        nuevo_item = Item.objects.exclude(status=0).get(id=id)

        if nuevo_item.iva == 12:
            cod_iva = "01"
        elif nuevo_item.iva == 0:
            cod_iva = "02"
        else:
            cod_iva = "03"

        if nuevo_item.costo is None:
            costo = 0.0
        else:
            costo = nuevo_item.costo

        ##### VALIDACION NO PUEDE EDITAR EL ITEM SI YA HA REALIZADO VENTAS CON EL MISMO ################################
        #    if Venta_Detalle.objects.filter(item=nuevo_item).exists():                                                #
        #        messages.error(request, u"El Item no puede ser modificado, dado que posee transacciones...")          #
        #        return HttpResponseRedirect(reverse("lista_items"))                                                   #
        ################################################################################################################

        if nuevo_item.categoria_item.status != 1:
            messages.error(request, u"El Item no puede ser modificado, por favor "
                                    u"verifique si la categoría está activa...")
            return HttpResponseRedirect(reverse("lista_items"))

        formulario = itemForm(initial={
            "codigo": nuevo_item.codigo,
            "nombre": nuevo_item.nombre,
            "categoria": nuevo_item.categoria_item.id,
            "descripcion": nuevo_item.descripcion,
            "unidad": nuevo_item.unidad.id,
            "iva": cod_iva,
            "min_inventario": nuevo_item.min_inventario,
            "ice_valor": nuevo_item.porc_ice,
            "precio1": nuevo_item.precio1,
            "precio2": nuevo_item.precio2,
            "precio3": nuevo_item.precio3,
            "costo": costo
        })

    except:
        messages.error(request, u"Error, al editar el Item.")
        return HttpResponseRedirect(reverse("lista_items"))

    if request.method == "POST":
        formulario = itemForm(request.POST)
        if formulario.is_valid():
            nuevo_item.descripcion = formulario.getDescripcion()
            nuevo_item.nombre = formulario.getNombre()
            nuevo_item.codigo = formulario.getCodigo()
            nuevo_item.categoria_item = Categoria_Item.objects.exclude(status=0).get(id=formulario.getCategoria())

            nuevo_item.unidad = Unidad.objects.filter(status=1).get(id=formulario.getUnidad())
            if formulario.getMinInventario() != "":
                nuevo_item.min_inventario = formulario.getMinInventario()
            else:
                nuevo_item.min_inventario = 0.0

            if formulario.getPrecio1() != "":
                nuevo_item.precio1 = formulario.getPrecio1()
            else:
                nuevo_item.precio1 = 0.0

            if formulario.getPrecio2() != "":
                nuevo_item.precio2 = formulario.getPrecio2()
            else:
                nuevo_item.precio2 = 0.0

            if formulario.getPrecio3() != "":
                nuevo_item.precio3 = formulario.getPrecio3()
            else:
                nuevo_item.precio3 = 0.0

            if formulario.getIva() is not None:
                if formulario.getIva() == "01":
                    nuevo_item.iva = 12.0
                elif formulario.getIva() == "02":
                    nuevo_item.iva = 0.0
                else:
                    nuevo_item.iva = 0.0

            if formulario.getCosto() != "":
                nuevo_item.costo = formulario.getCosto()
            else:
                nuevo_item.costo = 0.0

            if formulario.getIceValor() != "":
                nuevo_item.porc_ice = formulario.getIceValor()
            else:
                nuevo_item.porc_ice = 0.0

             ############ VAlidación Código #######################
            if Item.objects.exclude(status=0).filter(Q(codigo=nuevo_item.codigo), ~Q(id=nuevo_item.id)).exists():
                cont += 1
                errors = formulario._errors.setdefault("codigo", ErrorList())
                errors.append(u"")
                messages.error(request, u"El Código del Item ya ha sido ingresado, por favor corrija.")

            if cont == 0:
                nuevo_item.fecha_actualizacion = now_ac
                nuevo_item.usuario_actualizacion = request.user.username
                nuevo_item.save()
                messages.success(request, u"El Item : "+str(unicode(nuevo_item.codigo))+" - "+unicode(nuevo_item.nombre)+
                                 u"      se  modificó  exitosamente")
                return HttpResponseRedirect(reverse("lista_items"))
        else:

            lista_errores = "Por favor verifique los siguientes campos: "

            for i in formulario.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "
            if formulario.errors:
                messages.error(request, unicode(lista_errores[0:-2]))
            for i in formulario:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

            if cont > 0:
                transaction.rollback()

    return render_to_response('Items/registrar_item.html',
                              {"formulario": formulario,
                               "request": request,
                               "obj": nuevo_item,
                               "id": id,
                                "tipo": tipo}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def eliminar_items(request, id):
    try:
        item = Item.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if Venta_Detalle.objects.filter(status=1).filter(item=item).exists():
            messages.error(request, u'El Item : "'+str(item.codigo)+" - "+unicode(item.nombre) + u'"  '
                                                                                                 u'no puede ser eliminado, '
                                                                                                 u'dado que posee transacciones')
            return HttpResponseRedirect(reverse("lista_items"))

        else:

            if item.status != 0:
                item.status = 0
                item.usuario_actualizacion = request.user.username
                item.fecha_actualizacion = now_ac

            item.save()
            messages.success(request, u"EL Item "+str(item.codigo)+" - "+unicode(item.nombre)+
                                      u"  ha sido eliminado exitosamente")
            return HttpResponseRedirect(reverse("lista_items"))

    except:
        transaction.rollback()
        messages.success(request, u"Existe un error al eliminar el Item.")

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def cambiar_estado_item(request, id):
    try:
        item = Item.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if item.status == 1:
            if Venta_Detalle.objects.filter(status=1).filter(item=item).exists():
                messages.error(request, u'El Item de código: "'+unicode(item.codigo)+ u'" no puede ser inactivado, dado que posee transacciones.')
                return HttpResponseRedirect(reverse("lista_items"))
            else:
                item.status = 2
                item.usuario_actualizacion = request.user.username
                item.fecha_actualizacion = now_ac
                messages.success(request, u'Se inactivó el item de código: "'+unicode(item.codigo)+" - "+unicode(item.nombre)+ u'"  exitosamente')

        elif item.status == 2:
            item.status = 1
            item.usuario_actualizacion = request.user.username
            item.fecha_actualizacion = now_ac
            messages.success(request, u'Se activó el item de código : "'+unicode(item.codigo)+" - "+unicode(item.nombre)+ u'"  exitosamente')
        item.save()
    except:
        messages.error(request, u"Existen problemas al cambiar el estado al item")
    return HttpResponseRedirect(reverse("lista_items"))




