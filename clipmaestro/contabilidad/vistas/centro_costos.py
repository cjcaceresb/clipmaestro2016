#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.core.paginator import *
from contabilidad.models import *
from contabilidad.formularios.Centro_Costo_Form import *
from clipmaestro.templatetags.formulario_base import *
from sys import modules
from sys import path
import datetime
import operator

from django.forms.util import ErrorList
from django.core.paginator import *
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.db import IntegrityError, transaction
import json
from librerias.funciones.paginacion import *
from librerias.funciones.funciones_vistas import *

@login_required(login_url='/')
def lista_centro_costos(request):
    '''
    Vista que permite enlistar los centros de costo registrados en el sistema
    :param request:
    :return:
    '''
    q = request.GET.get("q", "")
    centro_costos = Centro_Costo.objects.exclude(status=0).filter(descripcion__icontains=q).order_by("descripcion","-fecha_creacion")
    paginacion = Paginator(centro_costos, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    try:
        centro_costos = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        centro_costos = paginacion.page(1)
    except EmptyPage:
        centro_costos = paginacion.page(paginacion._num_pages)
    total_paginas = paginacion._num_pages
    numero = centro_costos.number
    lista = arreglo_paginas_template(total_paginas, numero)
    return render_to_response('centro_costos/lista_centro_costos.html',
                              {"objetos": centro_costos, "total_paginas": total_paginas, "numero": numero,
                               "arreglo_paginado": lista,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count)},
                               context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def registrar_centro_costo(request):
    today = datetime.datetime.now()
    formulario = FormCentroCosto()
    contador = 0
    tipo = 1
    try:
        if request.method == "POST":
            formulario = FormCentroCosto(request.POST)
            if formulario.is_valid():
                cc_obj = Centro_Costo()
                cc_obj.descripcion = formulario.getDescripcionCosto()
                cc_obj.fecha_creacion = today
                cc_obj.usuario_creacion = request.user.username
                if Centro_Costo.objects.filter(status=1).filter(Q(descripcion=cc_obj.descripcion),
                                                                Q(status=1) | Q(status=2)).exists():
                    errors = formulario._errors.setdefault("descripcion", ErrorList())
                    errors.append(u"El Centro de Costo ya ha sido ingresado")
                    messages.error(request, u"El Centro de Costo ya ha sido ingresado")
                    contador += 1

                if contador == 0:
                    cc_obj.save()
                    messages.success(request, u"El Centro de Costo se  agregó exitosamente")
                    return HttpResponseRedirect(reverse("lista_centro_costos"))

            else:

                lista_errores = "Por favor verifique los siguientes campos: "
                for i in formulario.errors:
                    lista_errores = lista_errores +(unicode(i)) + ", "
                if formulario.errors:
                    messages.error(request, unicode(lista_errores[0:-2]))
                for i in formulario:
                    if i.errors:
                        i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

            if contador > 0:
                transaction.rollback()

    except:
        messages.error(request, u"Existen errores al ingresar el Centro de Costo")
        transaction.rollback()
        return HttpResponseRedirect(reverse("lista_centro_costos"))

    return render_to_response("centro_costos/registrar_centro_costos.html",
                              {"formulario": formulario,
                               " request": request,
                               "tipo": tipo}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def modificar_centro_costo(request, id):
    now_ac = datetime.datetime.now()
    contador = 0
    tipo = 2

    try:
        nuevo = Centro_Costo.objects.get(id=id)
        formulario = FormCentroCosto(initial={"descripcion": nuevo.descripcion})
    except:
        messages.error(request, u"El Centro de Costo no se encuentra registrado en la base")
        return HttpResponseRedirect(reverse("lista_centro_costos"))

    try:
        if request.method == "POST":
            formulario = FormCentroCosto(request.POST)
            if formulario.is_valid():
                nuevo.descripcion = formulario.getDescripcionCosto()
                nuevo.fecha_actualizacion = now_ac
                nuevo.usuario_actualizacion = request.user.username
                bandera = Centro_Costo.objects.exclude(status=0).filter(Q(descripcion=nuevo.descripcion), ~Q(id=nuevo.id)).exists()

                if bandera:
                    errors = formulario._errors.setdefault("descripcion", ErrorList())
                    errors.append(u"El Centro de Costo ya  ha sido ingresado.")
                    contador += 1

                for i in formulario:
                    if i.errors:
                        i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

                if contador == 0:
                    nuevo.save()
                    messages.success(request, u"La información del Centro de Costo ha sido  modificada exitosamente")
                    return HttpResponseRedirect(reverse("lista_centro_costos"))
    except:
        messages.error(request, u"Existen problemas al modificar al Centro de Costo, por favor intentelo nuevamente")
        return HttpResponseRedirect(reverse("lista_centro_costos"))

    return render_to_response("centro_costos/registrar_centro_costos.html",
                               {"formulario": formulario,
                                "obj": nuevo,
                                "tipo": tipo,
                                "id": id,
                                "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def eliminar_centro_costo(request, id):
    now_ac = datetime.datetime.now()
    try:
        centro_costo = Centro_Costo.objects.get(id=id)
        if Detalle_Comp_Contable.objects.filter(status=1).filter(centro_costo=centro_costo).exists():
            messages.error(request, u"El Centro de Costo no puede ser eliminado, posee transacciones.")
            return HttpResponseRedirect(reverse("lista_centro_costos"))

        if centro_costo.status != 0:
            centro_costo.status = 0
            centro_costo.usuario_actualizacion = request.user.username
            centro_costo.fecha_actualizacion = now_ac
        centro_costo.save()
        messages.success(request, u"El Centro de Costo ha sido eliminado exitosamente.")
        return HttpResponseRedirect(reverse("lista_centro_costos"))
    except:
        messages.error(request, u"Existió un error al eliminar al centro de costo.")
        return HttpResponseRedirect(reverse("lista_centro_costos"))

@login_required(login_url='/')
@csrf_exempt
def cambiar_estado_centro_costo(request, id):
    try:
        centro_costo = Centro_Costo.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if centro_costo.status == 1:
            centro_costo.status = 2
            centro_costo.usuario_actualizacion = request.user.username
            centro_costo.fecha_actualizacion = now_ac
            messages.success(request, u'Se inactivó al Centro de Costo : "'+unicode(centro_costo.descripcion) + u'" exitosamente')

        elif centro_costo.status == 2:
            centro_costo.status = 1
            centro_costo.usuario_actualizacion = request.user.username
            centro_costo.fecha_actualizacion = now_ac
            messages.success(request, u'Se activó al Centro de Costo : "'+unicode(centro_costo.descripcion) + u'" exitosamente')
        centro_costo.save()

    except:
        messages.error(request, u"Exisiteron problemas al cambiar el estado.")
    return HttpResponseRedirect(reverse("lista_centro_costos"))



def busquedaTransaccionesCentroCosto(fecha_ini, fecha_final, num_comp, id_tipo_comp, id_cuenta, id_centro_costo, con_centro_costo):
    '''
    Función que realiza el filtro de las transacciones para la opción del centro costo
    :param fecha_ini:
    :param fecha_final:
    :param num_comp:
    :param id_tipo_comp:
    :param id_cuenta:
    :param id_centro_costo:
    :param con_centro_costo:
    :return:
    '''
    predicates = []

    if fecha_ini != "":
        predicates.append(('cabecera_contable__fecha__gte',  fecha_ini))
    predicates.append(('cabecera_contable__fecha__lte',  fecha_final))
    predicates.append(('cabecera_contable__numero_comprobante__icontains', num_comp))

    if id_tipo_comp != "":
        try:
            tipo_comp = TipoComprobante.objects.get(id=int(id_tipo_comp))
        except TipoComprobante.DoesNotExist:
            tipo_comp = None

        if tipo_comp is not None:
            predicates.append(('cabecera_contable__tipo_comprobante', tipo_comp))

    if id_cuenta != "":
        try:
            cuenta = PlanCuenta.objects.get(id=int(id_cuenta))
        except PlanCuenta.DoesNotExist:
            cuenta = None

        if cuenta is not None:
            predicates.append(('plan_cuenta', cuenta))

    if id_centro_costo != "":
        try:
            centro_costo = Centro_Costo.objects.get(id=int(id_centro_costo))
        except Centro_Costo.DoesNotExist:
            centro_costo = None

        if centro_costo is not None:
            predicates.append(('centro_costo', centro_costo))

    q_list = [Q(x) for x in predicates]

    # Sólo las transacciones que tienen centro de costo
    if int(con_centro_costo) == 1:
        entries = Detalle_Comp_Contable.objects.filter(status=1).filter(centro_costo__isnull=False).filter(reduce(operator.and_, q_list)).distinct().order_by("-cabecera_contable__fecha", "-cabecera_contable__numero_comprobante").distinct()
    else:
        # Sólo las transacciones que no tienen centro de costo
        entries = Detalle_Comp_Contable.objects.filter(status=1).filter(centro_costo__isnull=True).filter(reduce(operator.and_, q_list)).distinct().order_by("-cabecera_contable__fecha", "-cabecera_contable__numero_comprobante").distinct()

    return entries

def ReasignarCentroCostoModulo(id_modulo, centro_costo, request):
    '''
    Función que actualiza el centro de costo en las tablas de venta_detalle, compra_detalle
    :param id_modulo:
    :return:
    '''
    try:
        compra = CompraCabecera.objects.filter(status=1).get(id=id_modulo)
        detalle_compra = CompraDetalle.objects.filter(status=1).filter(compra_cabecera=compra)
        for d in detalle_compra:
            d.centro_costo = centro_costo
            d.usuario_actualizacion = request.user.username
            d.fecha_actualizacion = datetime.datetime.now()
            d.save()

    except CompraCabecera.DoesNotExist:
        pass

    try:
        venta_cabecera = Venta_Cabecera.objects.filter(status=1).get(id=id_modulo)
        detalle_venta = Venta_Detalle.objects.filter(status=1).filter(venta_cabecera=venta_cabecera)

        for d in detalle_venta:
            d.centro_costo = centro_costo
            d.usuario_actualizacion = request.user.username
            d.fecha_actualizacion = datetime.datetime.now()
            d.save()
    except Venta_Cabecera.DoesNotExist:
        pass

@login_required(login_url='/')
@csrf_exempt
def get_transacciones_centro_costo_reasignar(request):
    '''
    Función de Ajax que obtiene los registro de las transacciones de banco según
    los parámetros que recibe desde el template el id_cuenta, check_historial, fecha_corte
    :param request:
    :return tabla_html:
    '''
    # PARAMETROS DE BÚSQUEDA
    fecha_ini = request.POST.get("fecha_ini", "")
    fecha_final = request.POST.get("fecha_fin", "")
    num_comp = request.POST.get("num_comp", "")
    id_tipo_comp = request.POST.get("id_tipo_comp", "")
    id_cuenta = request.POST.get("id_cuenta", "")
    id_centro_costo = request.POST.get("id_centro_costo", "")
    check_centro_costo = request.POST.get("con_centro_costo", 0)
    id_modulo = 0
    parametro_hide = 0
    persona = ""

    query = busquedaTransaccionesCentroCosto(fecha_ini, fecha_final, num_comp, id_tipo_comp, id_cuenta, id_centro_costo, check_centro_costo)
    l_detalle_doc = []
    formset_detalle_docs = formset_factory(DetalleRectificarCentroCosto, extra=0)

    if len(query) == 0:
        parametro_hide = 1

    else:

        for obj in query:

            # COMPRA
            if obj.cabecera_contable.tipo_comprobante_id == 1:
                compra = CompraCabecera.objects.get(num_comp=obj.cabecera_contable.numero_comprobante)
                id_modulo = compra.id
                persona = compra.proveedor.razon_social

            # VENTA
            elif obj.cabecera_contable.tipo_comprobante_id == 2:
                venta = Venta_Cabecera.objects.get(numero_comprobante=obj.cabecera_contable.numero_comprobante)
                id_modulo = venta.id
                persona = venta.cliente.razon_social

            # NOTA DE CRÉDITO
            elif obj.cabecera_contable.tipo_comprobante_id == 22:
                ajuste_venta = AjusteVentaCabecera.objects.get(num_comp=obj.cabecera_contable.numero_comprobante)
                id_modulo = ajuste_venta.id
                persona = ajuste_venta.cliente.razon_social

            # BANCO
            elif obj.cabecera_contable.tipo_comprobante_id in (3, 4, 23, 24, 25):
                banco = Banco.objects.get(num_comp=obj.cabecera_contable.numero_comprobante)
                id_modulo = banco.id
                persona = banco.persona

            # ASIENTO
            elif obj.cabecera_contable.tipo_comprobante_id == 6 or obj.cabecera_contable.tipo_comprobante_id == 26:
                id_modulo = obj.cabecera_contable_id
                persona = ""


            l_detalle_doc.append({"id_modulo": id_modulo,
                                  "id_comprobante_detalle": obj.id,
                                  "tipo_comprobante": obj.cabecera_contable.tipo_comprobante.descripcion,
                                  "num_comp": obj.cabecera_contable.numero_comprobante,
                                  "fecha_reg": obj.cabecera_contable.fecha,
                                  "cuenta": obj.plan_cuenta.codigo+" - "+obj.plan_cuenta.descripcion,
                                  "valor": obj.valor,
                                  "cliente_proveedor": persona,
                                  "centro_costo": obj.centro_costo_id})

    form_transacciones = formset_detalle_docs(initial=l_detalle_doc)

    if parametro_hide == 1:
        messages.success(request, u"La consulta realizada no posee resultados")

    return render_to_response('centro_costos/tabla_reasignar_centro_costo.html',
                             {'forms': form_transacciones,
                              "parametro_hide": parametro_hide}, context_instance=RequestContext(request))



@login_required(login_url='/')
@csrf_exempt
def rectificar_centro_costo(request):
    objetos = []
    parametro_empresa = get_parametros_empresa()
    flag = False        # Variable que me dice si los datos ya han sido cargados por POST
    if parametro_empresa.fecha_cierre is not None and parametro_empresa.fecha_cierre != "":
        fecha_cierre = get_parametros_empresa().fecha_cierre
    else:
        fecha_cierre = parametro_empresa.fecha_inicio_sistema

    # fecha_inicio: Fecha Cierre + 1
    fecha_cierre_final = fecha_cierre + datetime.timedelta(days=1)
    cabecera_form = RectificarCentroCosto(initial={"fecha_ini": fecha_cierre_final,
                                                   "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    detalle_form_set = formset_factory(DetalleRectificarCentroCosto, extra=1)
    detalles_transacciones = detalle_form_set()

    if request.method == "POST":
        flag = True
        cabecera_form = RectificarCentroCosto(request.POST)
        detalles_transacciones = detalle_form_set(request.POST, request.FILES)

        if cabecera_form.is_valid():
            if detalles_transacciones.is_valid():
                for form in detalles_transacciones:
                    informacion = form.cleaned_data
                    # Proceso donde actualizo el centro de costo en detalle_comprobante_contable
                    detalle_comp = Detalle_Comp_Contable.objects.filter(status=1).get(id=informacion.get("id_comprobante_detalle"))
                    try:
                        centro_costo = Centro_Costo.objects.filter(status=1).get(id=informacion.get("centro_costo"))
                    except Centro_Costo.DoesNotExist:
                        centro_costo = None

                    detalle_comp.centro_costo = centro_costo
                    detalle_comp.usuario_actualizacion = request.user.username
                    detalle_comp.fecha_actualizacion = datetime.datetime.now()
                    detalle_comp.save()

                    # Proceso donde actualizo el centro de costo en detalles_de_módulos
                    ReasignarCentroCostoModulo(informacion.get("id_modulo"), centro_costo, request)

                messages.success(request, u"El proceso de reasignar el centro de costo ha sido realizada con éxito")
                return HttpResponseRedirect(reverse("lista_centro_costos"))

    return render_to_response("centro_costos/reasignar_centro_costo.html",
                              {"formulario": cabecera_form,
                              "objetos": objetos,
                              "flag": flag,
                              "forms": detalles_transacciones,
                              "request": request}, context_instance=RequestContext(request))