#! /usr/bin/python
# -*- coding: UTF-8-*-
from datetime import timedelta
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from contabilidad.formularios.ClienteForm import *
from contabilidad.formularios.DireccionesForm import *
from contabilidad.formularios.VentaForm import *
from contabilidad.formularios.FormGuiaRemision import *
from django.forms.util import ErrorList
from librerias.funciones.paginacion import *
from librerias.funciones.validacion_rucs import *
from django.core.paginator import *
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django import template

from contabilidad.funciones.ventas_func import *
from librerias.funciones.funciones_vistas import *

__author__ = 'Roberto'


def busqueda_guia_remision(buscador):
    """
    Función busqueda_guía_remisión(buscador)
    @ Return GuiaCabecera Object
    """
    predicates = [('fecha_emi__range',  (buscador.getFecha(), buscador.getFechaFinal()))]
    predicates.append(('num_doc__istartswith', buscador.getNumeroDocumento()))
    predicates.append(('nombre_conductor__istartswith', buscador.getCompania()))
    #predicates = [('fecha_emi__range',  (buscador.getFechaInicio(), buscador.getFechaFinal()))]

    if buscador.getNumComprobante() != "":
        predicates.append(('venta_cabecera__numero_comprobante__icontains', buscador.getNumComprobante()))
    if buscador.getEstado() != "":
        predicates.append(('status', buscador.getEstado()))
    if buscador.getEstado() != "":
        predicates.append(('status', int(buscador.getEstado())))
    if buscador.getCliente() is not None:
        predicates.append(('cliente', buscador.getCliente()))
    if buscador.getMovimiento() is not None:
        predicates.append(('tipo_mov_guia_rem', buscador.getMovimiento()))
    if buscador.getCiudad() is not None:
        predicates.append(('ciudad', buscador.getCiudad()))
    if buscador.getItem() is not None:
        predicates.append(('detalleguiaremision__item', buscador.getItem()))

    q_list = [Q(x) for x in predicates]

    entries = CabeceraGuiaRemision.objects.filter(reduce(operator.and_, q_list)).distinct().order_by("-fecha_emi", "num_doc").distinct()

    return entries

@login_required(login_url='/')
def listado_todas_guias_remision(request):
    buscador = FormBuscadorGuiaRemVenta(request.GET)
    lista_guia_remision = busqueda_guia_remision(buscador)

    paginacion = Paginator(lista_guia_remision, 50)
    numero_pagina = request.GET.get("page")
    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)
    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number
    lista = arreglo_paginas_template(total_paginas, numero)
    return render_to_response("cm_guia_remision/lista_principal_guia_rem.html", {
                                "objetos": lista_cabecera, "total_paginas": total_paginas,
                               "arreglo_paginado": lista,
                               "numero": numero, "request": request,
                               "buscador": buscador}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def anular_guia_rem(request, id):
    cabecera_guia_rem = CabeceraGuiaRemision.objects.get(status=1, id=id)
    cabecera_guia_rem.status = 2
    cabecera_guia_rem.fecha_actualizacion = datetime.datetime.now()
    cabecera_guia_rem.usuario_actualizacion = request.user.username
    cabecera_guia_rem.save()

    ############################## Changes status guia remision y detalle_guia_remision  ###############################
    lista_detalles = DetalleGuiaRemision.objects.filter(guia_rem_cabecera=cabecera_guia_rem, status=1)
    for l in lista_detalles:
        l.status = 2
        l.usuario_actualizacion = request.user.username
        l.fecha_actualizacion = datetime.datetime.now()
        l.save()

        # Updates_Items_Bodega_Cantidad_Actual
        item_bodega = Item_Bodega.objects.filter(status=1).get(item=l.item)
        cantidad_actual_bodega = item_bodega.cantidad_actual
        cantidad_entregada = float(l.cantidad)
        cantidad_actual_2 = cantidad_actual_bodega + cantidad_entregada
        item_bodega.cantidad_actual = cantidad_actual_2
        item_bodega.fecha_actualizacion = datetime.datetime.now()
        item_bodega.usuario_actualizacion = request.user.username
        item_bodega.save()
    ####################################################################################################################


    ####################################################################################################################
    # Changes status cabecera inventario y detalle_inventario                                                          #
    ####################################################################################################################
    cabecera_inventario = Inventario_Cabecera.objects.get(status=1, num_doc=cabecera_guia_rem.num_doc)  #Lista de inventario_cabecera
    cabecera_inventario.status = 2
    cabecera_inventario.usuario_actualizacion = request.user.username
    cabecera_inventario.fecha_actualizacion = datetime.datetime.now()
    cabecera_inventario.save()

    lista_detalles_inventario = Inventario_Detalle.objects.filter(status=1, inventario_cabecera=cabecera_inventario)
    for v in lista_detalles_inventario:
        v.status = 2
        v.usuario_actualizacion = request.user.username
        v.fecha_actualizacion = datetime.datetime.now()
        v.save()

    '''
    ####################################################################################################################
    # Changes status Comprobantes Contables                                                                            #
    ####################################################################################################################
    if cabecera_inventario.num_comp != "":
        if Cabecera_Comp_Contable.objects.filter(status=1, numero_comprobante=cabecera_inventario.num_comp).exists():
            cabecera_comp_contable = Cabecera_Comp_Contable.objects.get(status=1, numero_comprobante=cabecera_inventario.num_comp)
            cabecera_comp_contable.status = 2
            cabecera_comp_contable.usuario_actualizacion = request.user.username
            cabecera_comp_contable.fecha_actualizacion = datetime.datetime.now()
            #cabecera_comp_contable.save()

            detalles_comprobantes = Detalle_Comp_Contable.objects.filter(status=1, cabecera_contable=cabecera_comp_contable)
            for m in detalles_comprobantes:
                m.status = 2
                m.usuario_actualizacion = request.user.username
                m.fecha_actualizacion = datetime.datetime.now()
                #m.save()

            messages.success(request, u'"La Guía de Remisión: "'+cabecera_guia_rem.num_doc+u'"  ha sido anulada exitosamente.')
            return HttpResponseRedirect(reverse("listado_todas_guias_remision"))
    '''

    messages.success(request, u'"La Guía de Remisión: "'+cabecera_guia_rem.num_doc+u'"  ha sido anulada exitosamente.')
    return HttpResponseRedirect(reverse("listado_todas_guias_remision"))



@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def detalle_guia_remision(request, id):
    now = datetime.datetime.now()
    param = request.GET.get("param", "")
    try:
        cabecera_guia_rem = CabeceraGuiaRemision.objects.get(id=id)

        if cabecera_guia_rem.status == 1:
            detalle_guia_remision = DetalleGuiaRemision.objects.filter(status=1, guia_rem_cabecera=cabecera_guia_rem)
        else:
            detalle_guia_remision = DetalleGuiaRemision.objects.filter(guia_rem_cabecera=cabecera_guia_rem)

    except:

        messages.error(request, u"Error al obtener la información de la guía de remisión")
        transaction.rollback()
        return HttpResponseRedirect(reverse("listado_todas_guias_remision"))

    return render_to_response('cm_guia_remision/detalle_guia_rem.html',
                              {"cabecera_guia": cabecera_guia_rem,
                               "detalle_guia": detalle_guia_remision,
                                "param": param,
                                "now": now}, context_instance=RequestContext(request))


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def detalle_guia_remision_pdf(request, id):
    now = datetime.datetime.now()
    try:
        empresa = Empresa.objects.filter(status=1)[0]
        usuario = request.user.first_name+' '+request.user.last_name
        cabecera_guia_rem = CabeceraGuiaRemision.objects.get(id=id)

        if cabecera_guia_rem.status == 1:
            detalle_guia_remision = DetalleGuiaRemision.objects.filter(status=1, guia_rem_cabecera=cabecera_guia_rem)
        else:
            detalle_guia_remision = DetalleGuiaRemision.objects.filter(guia_rem_cabecera=cabecera_guia_rem)
    except:
        messages.error(request, u"Error al obtener la información de la guía de remisión")
        transaction.rollback()
        return HttpResponseRedirect(reverse("listado_todas_guias_remision"))

    return generar_pdf('cm_guia_remision/detalle_guia_rem_pdf.html',
                            {'pagesize': 'A4',
                             "empresa":empresa,
                             "usuario": usuario,
                             "cabecera_guia_rem": cabecera_guia_rem,
                             "detalle_guia_rem": detalle_guia_remision,
                             "now": now,
                             "request": request})


@csrf_exempt
@login_required(login_url='/')
def valida_documentos_guia_remision_vigentes(request):
    q = request.POST.get("fecha", "")
    print "XXXXXXXXXXXXXX"
    print q
    respuesta = []
    #try:
    fecha_split = str(q).split("-")
    fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))
    documento = Documento.objects.get(id=25)     # Guía Remisión
    vigencia = VigenciaDocEmpresa.objects.filter(status=1).filter(documento_sri=documento,
                                                                  fecha_emi__lte=fecha.strftime("%Y-%m-%d"), fecha_vencimiento__gte=fecha.strftime("%Y-%m-%d"),
                                                                  documento_sri__venta=True, sec_actual__lt=F("sec_final")).order_by("serie")

    if len(vigencia) == 0:
        respuesta.append({"status": 0, "id": 0, "descripcion": "No se encuentra definido el documento con su vigencia, favor ingrese"})
    else:
        for obj in vigencia:
            respuesta.append({"status": 1, "id": obj.id, "serie": obj.serie})
    #except:
        #pass
        #respuesta.append({"status": -1, "id": 0, "descripcion": "No se encuentra definido el documento con su vigencia, favor ingrese"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")
