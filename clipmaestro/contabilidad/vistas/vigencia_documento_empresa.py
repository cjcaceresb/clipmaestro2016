#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.core.paginator import *
from contabilidad.models import *
from contabilidad.formularios.ClienteForm import *
from clipmaestro.templatetags.formulario_base import *
from sys import modules
from sys import path
import datetime
from django.forms.util import ErrorList
from django.core.paginator import *
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from contabilidad.models import VigenciaDocEmpresa
from contabilidad.formularios.VigenciaDocumentoEmpresa_Form import *
from django.utils import formats
from django.db import transaction
import operator
from django.db import connection
from librerias.funciones.permisos import *
from librerias.funciones.paginacion import *
import json


__author__ = 'Clip Maestro'


class ErrorVigencia(Exception):
    def __init__(self, valor):
        self.valor = valor
    def __str__(self):
        return unicode(self.valor)

def busqueda_documentos_empresa(buscador):
    """
    function busqueda_roles(buscador)
    @ Return CompraCabecera Object
    """
    predicates = [('serie__icontains', (buscador.getSerie()))]
    predicates.append(('autorizacion__icontains', (buscador.getAutorizacion())))
    if buscador.getFechaEmision() is not None:
        predicates.append(('fecha_emi__gte', (buscador.getFechaEmision())))
    if buscador.getFechaVencimiento() is not None:
        predicates.append(('fecha_vencimiento__lte', (buscador.getFechaVencimiento())))
    if buscador.getTipoDoc() is not None:
        predicates.append(('documento_sri', buscador.getTipoDoc()))
    if buscador.getEstado() != "":
        predicates.append(('status', buscador.getEstado()))

    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]

    entries = VigenciaDocEmpresa.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).order_by("-fecha_creacion")
    print("longitud de filtro blocks es "+str(len(entries)))
    return entries

@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def lista_vigencia_documento_empresa(request):
    buscador = BuscadorDocEmpresa(request.GET)
    secuencias_ret = busqueda_documentos_empresa(buscador)
    paginacion = Paginator(secuencias_ret, get_num_filas_template())
    numero_pagina = request.GET.get("page")

    try:
        secuencias_ret = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        secuencias_ret = paginacion.page(1)
    except EmptyPage:
        secuencias_ret = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = secuencias_ret.number
    cad = "?page="
    pagina = cad + str(numero)
    lista = arreglo_paginas_template(total_paginas, numero)
    return render_to_response('VigenciaDocumentoEmpresa/lista_vigencia_documento_empresa.html',
                              {"objetos": secuencias_ret,
                               "arreglo_paginado": lista,
                               "buscador": buscador,
                               "total_paginas": total_paginas, "pagina": pagina,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count)},
                               context_instance=RequestContext(request))


def redireccionar_documento_empresa(param):
    if param == "1":
        return HttpResponseRedirect(reverse('agregar_ventas'))
    elif param == "2":
        return HttpResponseRedirect(reverse('provision_venta'))
    elif param == "3":
        return HttpResponseRedirect(reverse('provision_compra'))
    else:
        return HttpResponseRedirect(reverse('lista_vigencia_documento_empresa'))


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def agregar_vigencia_documento_empresa(request):
    """
    Agregar block de documentos al sistema
    :param request:
    :return:
    """
    formulario = VigenciaRetencionEmpresa_Form()
    form_elect = DocumentosElectronicosForm()
    param = request.GET.get("param", "")

    if request.method == "POST":
        formulario = VigenciaRetencionEmpresa_Form(request.POST)
        form_elect = DocumentosElectronicosForm(request.POST)
        contador = 0

        if emite_docs_electronicos():
            if form_elect.is_valid():
                documento = Documento.objects.get(id=form_elect.get_tipo_documento())
                serie = (form_elect.get_establecimiento() + "-" + form_elect.get_punto_emision())

                if VigenciaDocEmpresa.objects.filter(documento_sri=documento, serie=serie, status=1).exists():
                    errors = form_elect._errors.setdefault("tipo_documento", ErrorList())
                    errors.append(u"Ese tipo de documento ya se encuentra registrado en el sistema "
                                  u"con el mismo número de establecimiento y punto de emisión, por favor verifique")
                    messages.error(request, u"Ese tipo de documento ya se encuentra registrado en "
                                            u"el sistema con el mismo número de serie, por favor verifique")

                else:
                    documento_elect = VigenciaDocEmpresa()
                    documento_elect.serie = form_elect.get_establecimiento() + "-" + form_elect.get_punto_emision()
                    documento_elect.documento_sri = Documento.objects.get(id=form_elect.get_tipo_documento())
                    documento_elect.sec_actual = form_elect.get_sec_actual()
                    documento_elect.fecha_actual = form_elect.get_fecha_emision()
                    documento_elect.fecha_emi = form_elect.get_fecha_emision()

                    # Secuencia Electronica
                    documento_elect.fecha_vencimiento = "2099-01-01"
                    documento_elect.autorizacion = "9999999999"
                    documento_elect.sec_final = 999999999

                    documento_elect.usuario_creacion = request.user.username
                    documento_elect.fecha_creacion = datetime.datetime.now()
                    documento_elect.save()
                    return redireccionar_documento_empresa(param)
            else:
                messages.error(request, u"Por favor ingrese los campos obligatorios para ingresar el bloque de documentos")
        else:   # Documentos no electronicos
            if formulario.is_valid():
                nueva_secuencia = VigenciaDocEmpresa()
                documento = Documento.objects.get(id=formulario.getTipoDoc())
                nueva_secuencia.documento_sri = documento
                nueva_secuencia.serie = formulario.getSerie()
                nueva_secuencia.autorizacion = formulario.getAutorizacion()
                nueva_secuencia.fecha_vencimiento = formulario.getFechaVencimiento()
                nueva_secuencia.sec_inicial = formulario.getSecIni()
                nueva_secuencia.sec_final = formulario.getSec_Final()
                nueva_secuencia.sec_actual = formulario.getSec_Actual()
                nueva_secuencia.fecha_emi = formulario.getFechaEmision()

                if formulario.getFechaEmision() > formulario.getFechaVencimiento():
                    errors = formulario._errors.setdefault("fecha_emision", ErrorList())
                    errors.append(u"La fecha de emisión del bloque de documentos no puede ser "
                                            u"menor a la fecha de vencimiento, por favor corrija")
                    messages.error(request, u"La fecha de emisión del bloque de documentos no puede ser "
                                            u"menor a la fecha de vencimiento, por favor corrija")
                    contador += 1

                if formulario.getSec_Actual() < (formulario.getSecIni() - 1):
                    errors = formulario._errors.setdefault("sec_actual", ErrorList())
                    errors.append(u"El número de la secuencia actual no puede ser menor a la inicial menos uno, "
                                            u"por favor corrija")
                    messages.error(request, u"El número de la secuencia actual no puede ser menor a la inicial menos uno, "
                                            u"por favor corrija")
                    contador += 1

                if formulario.getSec_Final() < formulario.getSecIni():
                    errors = formulario._errors.setdefault("sec_final", ErrorList())
                    errors.append(u"El número de la secuencia final no puede ser menor a la inicial, "
                                            u"por favor corrija")
                    messages.error(request, u"El número de la secuencia final no puede ser menor a la inicial, "
                                            u"por favor corrija")
                    contador += 1
                if formulario.getSecIni() > formulario.getSec_Final():
                    errors = formulario._errors.setdefault("sec_ini", ErrorList())
                    errors.append(u"El número de la secuencia inicial no puede ser mayor a la final, "
                                            u"por favor corrija")
                    messages.error(request, u"El número de la secuencia final no puede ser menor a la inicial, "
                                            u"por favor corrija")
                    contador += 1

                if contador == 0:
                    if VigenciaDocEmpresa.objects.filter(serie=formulario.getSerie(), documento_sri=documento, status=1).exists():
                        # Validar el numero de documento
                        for obj in VigenciaDocEmpresa.objects.filter(serie=formulario.getSerie(), documento_sri=documento, status=1):
                            if int(obj.sec_final) >= int(nueva_secuencia.sec_inicial) >= int(obj.sec_inicial)\
                                    or int(obj.sec_final) >= int(nueva_secuencia.sec_final) >= int(obj.sec_inicial):
                                contador += 1
                                messages.error(request, u"La secuencia de bloque que ingresó ya se encuentra "
                                                        u"registrada en otro bloque")
                if contador == 0:
                    nueva_secuencia.fecha_actual = datetime.datetime.now()
                    nueva_secuencia.usuario_creacion = request.user.username
                    nueva_secuencia.usuario_actualizacion = request.user.username
                    nueva_secuencia.fecha_creacion = datetime.datetime.now()
                    nueva_secuencia.fecha_actualizacion = datetime.datetime.now()
                    nueva_secuencia.save()

                    messages.success(request, u"Se ha creado satisfactoriamente el Block de Documentos.")
                    return redireccionar_documento_empresa(param)

                else:
                    transaction.rollback()
            else:
                messages.error(request, u"Por favor ingrese los campos obligatorios para ingresar el bloque de documentos")

    return render_to_response('VigenciaDocumentoEmpresa/agregar_vigencia_documento_empresa.html',
                              {"formulario": formulario,
                               "form_elect": form_elect,
                               "param": param}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def editar_vigencia_retencion_empresa(request, id):
    try:
        contador = 0
        vigencia_documento = VigenciaDocEmpresa.objects.get(id=id)

        if RetencionCompraCabecera.objects.exclude(status=0).filter(vigencia_doc_empresa=vigencia_documento).exists()\
                    or CabeceraGuiaRemision.objects.exclude(status=0).filter(vigencia_doc_empresa=vigencia_documento).exists() or \
                Venta_Cabecera.objects.exclude(status=0).filter(vigencia_doc_empresa=vigencia_documento) or \
                AjusteVentaCabecera.objects.exclude(status=0).filter(vigencia_doc_empresa=vigencia_documento).exists():
                raise ErrorVigencia(u"No puede editar el block, ya que se ha registrado documentos de ese block en el sistema.")

        else:
            if emite_docs_electronicos():
                formulario = DocumentosElectronicosForm(initial={
                    "tipo_documento": vigencia_documento.documento_sri.id,
                    "establecimiento": vigencia_documento.serie.split("-")[0],
                    "punto_emision": vigencia_documento.serie.split("-")[1],
                    "sec_actual": vigencia_documento.sec_actual,
                    "fecha_emision_elect": vigencia_documento.fecha_emi.strftime('%Y-%m-%d'),
                })
            else:
                formulario = VigenciaRetencionEmpresaEditar_Form(initial={
                    "serie": vigencia_documento.serie,
                    "tipo_documento": vigencia_documento.documento_sri.id,
                    "sec_ini": vigencia_documento.sec_inicial,
                    "sec_final": vigencia_documento.sec_final,
                    "sec_actual": vigencia_documento.sec_actual,
                    "autorizacion": vigencia_documento.autorizacion,
                    "fecha_emision": vigencia_documento.fecha_emi.strftime('%Y-%m-%d'),
                    "fecha_vencimiento": vigencia_documento.fecha_vencimiento.strftime('%Y-%m-%d')
                })

        if request.method == "POST":
            if emite_docs_electronicos():
                formulario = DocumentosElectronicosForm(request.POST)
                if formulario.is_valid():
                    documento = Documento.objects.get(id=formulario.get_tipo_documento())
                    serie = (formulario.get_establecimiento() + "-" + formulario.get_punto_emision())

                    if VigenciaDocEmpresa.objects.filter(documento_sri=documento,
                                                         serie=serie, status=1).exclude(id=id).exists():
                        errors = formulario._errors.setdefault("tipo_documento", ErrorList())
                        errors.append(u"Ese tipo de documento ya se encuentra registrado en el sistema "
                                      u"con el mismo número de establecimiento y punto de emisión, por favor verifique")
                        messages.error(request, u"Ese tipo de documento ya se encuentra registrado en "
                                                u"el sistema con el mismo número de serie, por favor verifique")

                    else:
                        vigencia_documento.serie = formulario.get_establecimiento() + "-" + formulario.get_punto_emision()
                        vigencia_documento.documento_sri = Documento.objects.get(id=formulario.get_tipo_documento())
                        vigencia_documento.sec_actual = formulario.get_sec_actual()
                        vigencia_documento.fecha_actual = formulario.get_fecha_emision()
                        vigencia_documento.fecha_emi = formulario.get_fecha_emision()

                        # Secuencia Electronica
                        vigencia_documento.fecha_vencimiento = "2099-01-01"
                        vigencia_documento.autorizacion = "9999999999"
                        vigencia_documento.sec_final = 999999999

                        vigencia_documento.usuario_creacion = request.user.username
                        vigencia_documento.fecha_creacion = datetime.datetime.now()
                        vigencia_documento.save()

                        messages.success(request, u"Se ha editado satisfactoriamente el Block de Documentos")
                        return redireccionar_documento_empresa("")
            else:   # Documentos no electronicos
                formulario = VigenciaRetencionEmpresaEditar_Form(request.POST)
                if formulario.is_valid():
                    documento = Documento.objects.get(id=formulario.getTipoDoc())
                    vigencia_documento.documento_sri = documento
                    vigencia_documento.serie = formulario.getSerie()
                    vigencia_documento.autorizacion = formulario.getAutorizacion()
                    vigencia_documento.fecha_vencimiento = formulario.getFechaVencimiento()
                    vigencia_documento.sec_inicial = formulario.getSecIni()
                    vigencia_documento.sec_final = formulario.getSec_Final()
                    vigencia_documento.sec_actual = formulario.getSec_Actual()
                    vigencia_documento.fecha_emi = formulario.getFechaEmision()

                    if formulario.getFechaEmision() > formulario.getFechaVencimiento():
                        errors = formulario._errors.setdefault("fecha_emision", ErrorList())
                        errors.append(u"La fecha de emisión del bloque de documentos no puede ser "
                                                u"menor a la fecha de vencimiento, por favor corrija")
                        messages.error(request, u"La fecha de emisión del bloque de documentos no puede ser "
                                                u"menor a la fecha de vencimiento, por favor corrija")
                        contador += 1

                    if formulario.getSec_Actual() < formulario.getSecIni():
                        errors = formulario._errors.setdefault("sec_actual", ErrorList())
                        errors.append(u"El número de la secuencia actual no puede ser menor a la inicial, "
                                                u"por favor corrija")
                        messages.error(request, u"El número de la secuencia actual no puede ser menor a la inicial, "
                                                u"por favor corrija")
                        contador += 1

                    if formulario.getSec_Final() < formulario.getSecIni():
                        errors = formulario._errors.setdefault("sec_final", ErrorList())
                        errors.append(u"El número de la secuencia final no puede ser menor a la inicial, "
                                                u"por favor corrija")
                        messages.error(request, u"El número de la secuencia final no puede ser menor a la inicial, "
                                                u"por favor corrija")
                        contador += 1
                    if formulario.getSecIni() > formulario.getSec_Final():
                        errors = formulario._errors.setdefault("sec_ini", ErrorList())
                        errors.append(u"El número de la secuencia inicial no puede ser mayor a la final, "
                                                u"por favor corrija")
                        messages.error(request, u"El número de la secuencia final no puede ser menor a la inicial, "
                                                u"por favor corrija")
                        contador += 1

                    if contador == 0:
                        if VigenciaDocEmpresa.objects.filter(serie=formulario.getSerie(),
                                                             documento_sri=documento, status=1).exclude(id=id).exists():
                            # Validar el numero de documento
                            for obj in VigenciaDocEmpresa.objects.filter(serie=formulario.getSerie(),
                                                                         documento_sri=documento,
                                                                         status=1).exclude(id=id):
                                if int(obj.sec_final) >= int(vigencia_documento.sec_inicial) >= int(obj.sec_inicial)\
                                        or int(obj.sec_final) >= int(vigencia_documento.sec_final) >= int(obj.sec_inicial):
                                    contador += 1
                                    messages.error(request, u"La secuencia de bloque que ingresó ya se encuentra "
                                                            u"registrada en otro bloque")
                    if contador == 0:
                        vigencia_documento.fecha_actual = datetime.datetime.now()
                        vigencia_documento.usuario_creacion = request.user.username
                        vigencia_documento.usuario_actualizacion = request.user.username
                        vigencia_documento.fecha_creacion = datetime.datetime.now()
                        vigencia_documento.fecha_actualizacion = datetime.datetime.now()
                        vigencia_documento.save()

                        messages.success(request, u"Se ha editado satisfactoriamente el Block de Documentos.")
                        return redireccionar_documento_empresa("")
                    else:
                        transaction.rollback()
                else:
                    messages.error(request, u"Por favor ingrese los campos obligatorios para ingresar el bloque de documentos")
    except VigenciaDocEmpresa.DoesNotExist:
        raise Http404
    except ErrorVigencia, e:
        messages.success(request, e)
        return HttpResponseRedirect(reverse('lista_vigencia_documento_empresa'))
    return render_to_response('VigenciaDocumentoEmpresa/editar_vigencia_documento_empresa.html',
                              {'formulario': formulario,
                               "id": id},
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def detalle_vigencia_documento_empresa(request, id):
    try:
        vigencia_retencion = VigenciaDocEmpresa.objects.get(id=id)
    except VigenciaDocEmpresa.DoesNotExist:
        raise Http404
    return render_to_response('VigenciaDocumentoEmpresa/detalle_vigencia_documento_empresa.html',{'obj':vigencia_retencion,"id":id}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def eliminar_vigencia_documento_empresa(request, id):
    """
    Elimina el documento seleccionado
    :param request:
    :param id:
    :return:
    """
    try:
        vigencia_documento = VigenciaDocEmpresa.objects.get(id=id)
        now = datetime.datetime.now()
        if RetencionCompraCabecera.objects.exclude(status=0).filter(vigencia_doc_empresa=vigencia_documento,
                                                                    status=1).exists()\
                or CabeceraGuiaRemision.objects.exclude(status=0).filter(vigencia_doc_empresa=vigencia_documento).exists()\
                or Venta_Cabecera.objects.exclude(status=0).filter(vigencia_doc_empresa=vigencia_documento).exists():
            raise ErrorVigencia(u"No puede eliminar el block, ya que "
                                u"se ha registrado documentos de ese block en el sistema.")

        vigencia_documento.status = 0
        vigencia_documento.fecha_actualizacion = now
        vigencia_documento.usuario_actualizacion = request.user.username
        vigencia_documento.save()
        messages.success(request, u"Se ha eliminado con éxito el block")
    except VigenciaDocEmpresa.DoesNotExist:
        raise Http404
    except ErrorVigencia, e:
        messages.error(request, e)
    return HttpResponseRedirect(reverse('lista_vigencia_documento_empresa'))


@login_required(login_url='/')
@csrf_exempt
def get_vigencia_doc(request):
    respuesta = []

    for obj in VigenciaDocEmpresa.objects.filter(status=1).only("id", "serie", "autorizacion", "fecha_emi", "fecha_vencimiento",
                                                                "documento_sri", "sec_inicial", "sec_final", "sec_actual"):

        respuesta.append({"id": obj.id, "serie": obj.serie, "autorizacion": obj.autorizacion, "fecha_emi": str(obj.fecha_emi), "fecha_vencimiento": str(obj.fecha_vencimiento),
                          "documento_sri": obj.documento_sri, "sec_actual": obj.sec_actual,
                          "sec_inicial": obj.sec_inicial,
                          "sec_final": obj.sec_final})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def registrar_vigencia_doc(request):
    '''
     Ajax para registrar documentos vigentes desde el mismo módulo de
     transacción
    :param request:
    :return:
    '''
    resultado = []
    formulario = VigenciaRetencionEmpresa_Form(request.POST)

    try:
        if formulario.is_valid():
            nueva_secuencia = VigenciaDocEmpresa()
            documento = Documento.objects.get(id=formulario.getTipoDoc())
            nueva_secuencia.documento_sri = documento
            nueva_secuencia.serie = formulario.getSerie()
            nueva_secuencia.autorizacion = formulario.getAutorizacion()
            nueva_secuencia.fecha_vencimiento = formulario.getFechaVencimiento()
            nueva_secuencia.sec_inicial = formulario.getSecIni()
            nueva_secuencia.sec_final = formulario.getSec_Final()
            nueva_secuencia.sec_actual = formulario.getSec_Actual()
            nueva_secuencia.fecha_emi = formulario.getFechaEmision()


            if formulario.getSec_Actual() < formulario.getSecIni():
                mensaje2 = u"El número de la secuencia actual no puede ser menor a la inicial, por favor corrija"
                respuesta = {"status": 2, "mensaje": mensaje2}
                resultado = json.dumps(respuesta)
                transaction.rollback()

            elif formulario.getFechaEmision() > formulario.getFechaVencimiento():
                mensaje3 = u"La fecha de emisión del bloque de documentos no puede ser menor a la fecha de vencimiento, por favor corrija"
                respuesta = {"status": 3, "mensaje": mensaje3}
                resultado = json.dumps(respuesta)
                transaction.rollback()

            elif formulario.getSec_Final() < formulario.getSecIni():
                mensaje4 = u"El número de la secuencia final no puede ser menor a la inicial, por favor corrija"
                respuesta = {"status": 4, "mensaje": mensaje4}
                resultado = json.dumps(respuesta)
                transaction.rollback()

            elif formulario.getSecIni() > formulario.getSec_Final():
                mensaje5 = u"El número de la secuencia final no puede ser menor a la inicial, por favor corrija"
                respuesta = {"status": 5, "mensaje": mensaje5}
                resultado = json.dumps(respuesta)
                transaction.rollback()

            elif VigenciaDocEmpresa.objects.filter(serie=formulario.getSerie(), documento_sri=documento, status=1).exists():
                # Validar el numero de documento
                for obj in VigenciaDocEmpresa.objects.filter(serie=formulario.getSerie(), documento_sri=documento, status=1):
                    if int(obj.sec_final) >= int(nueva_secuencia.sec_inicial) >= int(obj.sec_inicial) or int(obj.sec_final) >= int(nueva_secuencia.sec_final) >= int(obj.sec_inicial):
                        mensaje6 = u"La secuencia de bloque que ingresó ya se encuentra registrada en otro bloque"
                        respuesta = {"status": 6, "mensaje": mensaje6}
                        resultado = json.dumps(respuesta)
                        transaction.rollback()

            else:

                nueva_secuencia.fecha_actual = datetime.datetime.now()
                nueva_secuencia.usuario_creacion = request.user.username
                nueva_secuencia.usuario_actualizacion = request.user.username
                nueva_secuencia.fecha_creacion = datetime.datetime.now()
                nueva_secuencia.fecha_actualizacion = datetime.datetime.now()
                nueva_secuencia.save()
                respuesta = {"status": 1, "mensaje": u"OK"}
                resultado = json.dumps(respuesta)


        else:
            respuesta = {"status": -1, "mensaje":  u"Llene correctamente el formulario"}
            resultado = json.dumps(respuesta)
            messages.error(request, u"Llene correctamente el formulario")
            transaction.rollback()


    except:
        respuesta = {"status": -2, "mensaje": u"Existió un error en el servidor, por favor comuniquese con el administrador"}
        resultado = json.dumps(respuesta)
        transaction.rollback()

    return HttpResponse(resultado, mimetype='application/json')