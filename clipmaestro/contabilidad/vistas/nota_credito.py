#! /usr/bin/python
# -*- coding: UTF-8-*-
from datetime import timedelta
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.forms.formsets import formset_factory
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import IntegrityError, transaction
import datetime
from django.db.models import Q
from django.db import connection
from contabilidad.formularios.NotaCreditoForm import *
from django.forms.util import ErrorList
from librerias.funciones.paginacion import *
from django.core.paginator import *
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from django import template
from contabilidad.funciones.ventas_func import *
from contabilidad.funciones.nota_credito_func import *
from librerias.funciones.funciones_vistas import *
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from librerias.funciones.comprobantes_electronicos import *
from contabilidad.formularios.VigenciaDocumentoEmpresa_Form import *
from librerias.funciones.permisos import *
from io import BytesIO
from reportlab.pdfgen import canvas
from django.http import HttpResponse
IVA = 0.14
register = template.Library()
__author__ = 'Roberto'

@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
def lista_nota_credito(request):
    '''
    Vista que lista las notas de crédito
    registradas en el sistema
    :param request:
    :return:
    '''
    buscador = FormBuscadoresNotaCredito(request.GET)
    lista_nota_credito = busqueda_nota_credito(buscador)
    paginacion = Paginator(lista_nota_credito, get_num_filas_template())
    numero_pagina = request.GET.get("page")
    form_email = FormEnvioCorreoNotaCredito()
    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number
    lista = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('nota_credito/lista_nota_credito.html',
                              {"objetos": lista_cabecera, "total_paginas": total_paginas,
                               "arreglo_paginado": lista,
                               "form_correo": form_email,
                               "muestra_paginacion": muestra_paginacion_template(paginacion.count),
                               "numero": numero, "request": request, "buscador": buscador}, context_instance=RequestContext(request))

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def agregar_nota_credito(request):
    '''
    Vista que me permite realizar una devolución de la venta y
    generar una N/C
    :param request:
    :return:
    '''
    dia_actual = datetime.datetime.now().strftime('%Y-%m-%d')
    # Tipo Comprobante NDC (Nota de Crédito)

    tipo_comprobante = TipoComprobante.objects.get(id=22)
    flag_post = 0                                   # Bandera para utilizar el Ajax y determinar cuando se hizo el post
                                                    # para manejar correctamente el rollback
    try:
        secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
        fecha_actual = secuencia_tipo.fecha_actual
    except SecuenciaTipoComprobante.DoesNotExist:
        fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")
        secuencia_tipo = SecuenciaTipoComprobante()
        secuencia_tipo.fecha_actual = fecha_actual
        secuencia_tipo.tipo_comprobante = tipo_comprobante
        secuencia_tipo.fecha_creacion = datetime.datetime.now()
        secuencia_tipo.usuario_creacion = request.user.username
        secuencia_tipo.secuencia = 0
        secuencia_tipo.save()

    # Formulario Cabecera
    formulario_dev = CabeceraVentaDevolucionForms(initial={"fecha": fecha_actual})
    # FORMSETS
    detalle_form_test = formset_factory(DetalleDevolucionManualForms, extra=1)
    formset_detalle_test = detalle_form_test(prefix="tabla_formset_venta_dev_m")
    detalle_form = formset_factory(DetalleDevolucionAutomaticaForms, extra=0, formset=BaseDetalleFormSetDevolucionAut)
    formset_detalle_aut = detalle_form()

    if request.method == "POST":
        formulario_dev = CabeceraVentaDevolucionForms(request.POST)
        # Formulario FormSet Devolucion Manual
        formset_detalle_test = detalle_form_test(request.POST, request.FILES, prefix="tabla_formset_venta_dev_m")
        # Formulario FormSet Devolucion Automática
        formset_detalle_aut = detalle_form(request.POST, request.FILES)
        flag_post = 1

        if formulario_dev.is_valid():
            if esta_periodo_contable(formulario_dev.getFecha()):
                now = datetime.datetime.now()
                try:
                    secuencia = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=22)  # Secuencia de Compras
                    fecha_comp = secuencia.fecha_actual
                except SecuenciaTipoComprobante.DoesNotExist:
                    fecha_comp = now.strftime("%Y-%m-%d")

                if fecha_comp.strftime("%Y-%m-%d") <= formulario_dev.getFecha().strftime("%Y-%m-%d"):

                    # Por Implementar N/C MANUAL - EN FASE TESTING
                    if formulario_dev.getNumVentas() == "0":    # Manual
                        if formset_detalle_test.is_valid():
                            flag_reg = 1
                            (subtotales, subtotales_cero, iva_valor, ice_valor, resultado) = \
                                CalcularTotalesDevolucionVentaManual(formset_detalle_test, request)

                            ajuste_cabecera = AjusteVentaCabecera()
                            validador_general = GuardarAjusteVentaCabecera(ajuste_cabecera, formulario_dev, None, subtotales,
                                                                           subtotales_cero, iva_valor, ice_valor, resultado,
                                                                           0, 0,  dia_actual, request)

                            if validador_general == 0:
                                # Ajuste Venta Detalle
                                validador_general, parametro_inventario = GuardarAjusteVentaDetalle(formset_detalle_test,
                                                                                                    ajuste_cabecera, flag_reg,
                                                                                                    request, dia_actual)
                                # Guardar Mayor Nota Crédito
                                cabecera_comprobante_nota_credito = Cabecera_Comp_Contable()
                                GuardarMayorNotaCredito(formulario_dev, cabecera_comprobante_nota_credito, ajuste_cabecera,
                                                                                                 dia_actual, request)

                                # Guardar Asiento Contable Nota Crédito (Cuentas de la transacción)
                                validador_general += GuardarAsientoContableNotaCredito(cabecera_comprobante_nota_credito, ajuste_cabecera, resultado,
                                                                                       iva_valor, ice_valor, 0, 0, None, request, dia_actual)

                                if parametro_inventario == 1:  # Si se mueve Inventario
                                    inventario_cabecera = Inventario_Cabecera()
                                    # Guarda en Inventario Cabecera
                                    GuardarInventarioCabeceraNotaCredito(inventario_cabecera, ajuste_cabecera,
                                                                         dia_actual, request)
                                    # Guarda en Detalle de Inventario
                                    GuardarInventarioDetalleNotaCredito(formset_detalle_test, flag_reg, inventario_cabecera,
                                                                        dia_actual, request)

                                    # Se Guarda en el Asiento Contable de la Nota Crédito si  mueven items de inventario
                                    GuardarAsientoCtasInventarioNotaCredito(cabecera_comprobante_nota_credito, flag_reg,
                                                                            formset_detalle_test, dia_actual, request)

                                    # =========== Inventario Contabilidad ============ #
                                    cabecera_comprobante = Cabecera_Comp_Contable()
                                    GuardarMayorContableInventarioNotaCredito(cabecera_comprobante, inventario_cabecera,
                                                                              dia_actual, request)
                                    # Guardar Cuentas Asiento de Inventario
                                    GuardarMayorInventarioDetalleNotaCredito(formset_detalle_test, flag_reg,
                                                                             cabecera_comprobante, dia_actual, request)
                                    # ================================================= #
                                    messages.success(request, u"Se ha realizado la N/C  exitosamente con N° "
                                                              u"de comprobante: "+str(ajuste_cabecera.num_comp)+
                                                              u" y N° de documento: "+str(ajuste_cabecera.num_doc)+
                                                              u" y con N° de comrpobante "
                                                              u" de inventario: "+str(inventario_cabecera.num_comp))

                        else:
                            messages.error(request, u"Error al ingresar la información del detalle de la N/C")

                    elif formulario_dev.getNumVentas() != "0":      # Automática (OK)
                        venta = Venta_Cabecera.objects.get(id=formulario_dev.getNumVentas(), status=1)

                        if formset_detalle_aut.is_valid():
                            flag_reg = 2
                            (subtotales, subtotales_cero, iva_valor, ice_valor, resultado, descuento_iva, descuento_cero) = CalcularTotalesDevolucionVenta(formset_detalle_aut, request)

                            # Ajuste Venta Cabecera
                            ajuste_cabecera = AjusteVentaCabecera()
                            validador_general = GuardarAjusteVentaCabecera(ajuste_cabecera, formulario_dev, venta, subtotales, subtotales_cero, iva_valor, ice_valor, resultado, descuento_iva,
                             descuento_cero, dia_actual, request)

                            if validador_general == 0:
                                # Ajuste Venta Detalle
                                validador_general, parametro_inventario = GuardarAjusteVentaDetalle(formset_detalle_aut,
                                                                                                    ajuste_cabecera, flag_reg,
                                                                                                    request, dia_actual)
                                # Guardar Mayor Nota Crédito
                                cabecera_comprobante_nota_credito = Cabecera_Comp_Contable()
                                GuardarMayorNotaCredito(formulario_dev, cabecera_comprobante_nota_credito, ajuste_cabecera,
                                                                                                 dia_actual, request)

                                # Guardar Asiento Contable Nota Crédito (Cuentas de la transacción)
                                validador_general += GuardarAsientoContableNotaCredito(cabecera_comprobante_nota_credito, ajuste_cabecera, resultado,
                                                                                       iva_valor, ice_valor, descuento_iva, descuento_cero, venta, request, dia_actual)

                                if parametro_inventario == 1:  # Si se mueve Inventario
                                    inventario_cabecera = Inventario_Cabecera()
                                    # Guarda en Inventario Cabecera
                                    GuardarInventarioCabeceraNotaCredito(inventario_cabecera, ajuste_cabecera,
                                                                         dia_actual, request)
                                    # Guarda en Detalle de Inventario
                                    GuardarInventarioDetalleNotaCredito(formset_detalle_aut, flag_reg, inventario_cabecera,
                                                                        dia_actual, request)

                                    # Se Guarda en el Asiento Contable de la Nota Crédito si  mueven items de inventario
                                    GuardarAsientoCtasInventarioNotaCredito(cabecera_comprobante_nota_credito, flag_reg,
                                                                            formset_detalle_aut, dia_actual, request)

                                    # =========== Inventario Contabilidad ============ #
                                    cabecera_comprobante = Cabecera_Comp_Contable()
                                    GuardarMayorContableInventarioNotaCredito(cabecera_comprobante, inventario_cabecera,
                                                                              dia_actual, request)
                                    # Guardar Cuentas Asiento de Inventario
                                    GuardarMayorInventarioDetalleNotaCredito(formset_detalle_aut, flag_reg,
                                                                             cabecera_comprobante, dia_actual, request)
                                    # ================================================= #

                                    messages.success(request, u"Se ha realizado la N/C  exitosamente con N° "
                                                              u"de comprobante: "+str(ajuste_cabecera.num_comp)+
                                                              u" y N° de documento: "+str(ajuste_cabecera.num_doc)+
                                                              u" proveniente de la venta  N° de documento: "+
                                                              str(venta.num_documento)+u" y N° de comrpobante de inventario: "+str(inventario_cabecera.num_comp))

                                    return HttpResponseRedirect(reverse("lista_nota_credito"))

                                else:

                                    if validador_general > 0:
                                        transaction.rollback()

                                    else:

                                        # SERVIVICIOS FASE N/C
                                        try:
                                            GuardarAsientoCtasServicioNotaCredito(cabecera_comprobante_nota_credito, ajuste_cabecera, flag_reg, formset_detalle_aut, dia_actual, request)
                                            messages.success(request, u"Se ha realizado la N/C con número: "+str(ajuste_cabecera.num_doc) +
                                                         u"  exitosamente, proveniente de la venta: "+str(venta.num_documento)+
                                                         u"   sin movimientos en inventario")
                                        except:
                                            transaction.rollback()
                                            messages.error(request, u"Existió un error al guardar la cuenta de devolución al "
                                                                    u" asiento contablde de la nota de crédito correspondiente")
                                        return HttpResponseRedirect(reverse("lista_nota_credito"))
                            else:
                                transaction.rollback()
                                messages.error(request, u"Error al ingresar la información general de la nota de crédito")
                        else:
                            transaction.rollback()
                            messages.error(request, u"Error en al llenar la información del detalle de la N/C")
                else:
                        errors = formulario_dev._errors.setdefault("fecha", ErrorList())
                        errors.append(u"Campo Requerido")
                        messages.error(request, u'Por favor verifique el campo Fecha, debe ser mayor o igual a la fecha del último comprobante: '+str(fecha_comp))
            else:

                errors = formulario_dev._errors.setdefault("fecha", ErrorList())
                errors.append(u"Campo Requerido")
                parametro_empresa = get_parametros_empresa()
                messages.error(request, u'Por favor verifique el campo Fecha, debe ser mayor a la fecha de cierre: '+get_mes(parametro_empresa.mes_cierre)+" " +str(parametro_empresa.anio_cierre))


        else:
            transaction.rollback()
            lista_errores = "Por favor verifique los siguientes campos: "
            for i in formulario_dev.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "
            if formulario_dev.errors:
                messages.error(request, unicode(lista_errores[0:-2]))
            for i in formulario_dev:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'

    if emite_docs_electronicos():

        return render_to_response('nota_credito/templates_doc_electronicos/agregar_nota_credito_electronica.html', {
                                    "formulario_cabecera": formulario_dev,
                                    "form_detalle_manual": formset_detalle_test,
                                    "forms": formset_detalle_aut,
                                    "flag_post": flag_post,
                                    "dia_actual": dia_actual}, context_instance=RequestContext(request))

    else:

        return render_to_response('nota_credito/agregar_nota_credito.html', {
                                    "formulario_cabecera": formulario_dev,
                                    "form_detalle_manual": formset_detalle_test,
                                    "forms": formset_detalle_aut,
                                    "flag_post": flag_post,
                                    "dia_actual": dia_actual}, context_instance=RequestContext(request))

@login_required(login_url='/')
def detalle_nota_credito(request, id):
    '''
    Vista que presenta la información de la nota de crédito
    :param request:
    :return:
    '''
    try:
        ajuste_cabecera = AjusteVentaCabecera.objects.get(id=id)

        if ajuste_cabecera.status == 1:
            ajuste_detalle = AjusteVentaDetalle.objects.filter(ajuste_venta_cabecera=ajuste_cabecera, status=1)
            mayor = Cabecera_Comp_Contable.objects.get(numero_comprobante=ajuste_cabecera.num_comp, status=1)
            detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=mayor, status=1).\
                order_by("dbcr", "plan_cuenta__codigo")

        elif ajuste_cabecera.status == 2:
            ajuste_detalle = AjusteVentaDetalle.objects.exclude(status=0).filter(ajuste_venta_cabecera=ajuste_cabecera)
            mayor = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=ajuste_cabecera.num_comp)
            detalle_mayor = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=mayor).\
                order_by("dbcr", "plan_cuenta__codigo")
        else:
            ajuste_detalle = None
            mayor = None
            detalle_mayor = None

    except:
        messages.error(request, u"Error al cargar el comprobante de la nota de crédito")
        transaction.rollback()
        return HttpResponseRedirect(reverse("lista_nota_credito"))

    return render_to_response('nota_credito/detalle_nota_credito.html',
                              {"ajuste_venta": ajuste_cabecera, "request": request,
                               "detalles": ajuste_detalle,
                               "mayor_cab": mayor,
                               "detalle_may": detalle_mayor}, context_instance=RequestContext(request))

@login_required(login_url='/')
def detalle_nota_credito_pdf(request, id):
    '''
    Vista que presenta la información de la nota de crédito en pdf
    :param request:
    :return:
    '''
    try:
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = EmpresaGeneral.objects.get(id=id_empresa)
        usuario = request.user.first_name+' '+request.user.last_name
        ajuste_cabecera = AjusteVentaCabecera.objects.get(id=id)

        if ajuste_cabecera.status == 1:
            ajuste_detalle = AjusteVentaDetalle.objects.filter(ajuste_venta_cabecera=ajuste_cabecera, status=1)
            mayor = Cabecera_Comp_Contable.objects.get(numero_comprobante=ajuste_cabecera.num_comp, status=1)
            detalle_mayor = Detalle_Comp_Contable.objects.filter(cabecera_contable=mayor, status=1).\
                order_by("dbcr", "plan_cuenta__codigo")

        elif ajuste_cabecera.status == 2:
            ajuste_detalle = AjusteVentaDetalle.objects.exclude(status=0).filter(ajuste_venta_cabecera=ajuste_cabecera)
            mayor = Cabecera_Comp_Contable.objects.exclude(status=0).get(numero_comprobante=ajuste_cabecera.num_comp)
            detalle_mayor = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=mayor).\
                order_by("dbcr", "plan_cuenta__codigo")

        else:
            ajuste_detalle = None
            mayor = None
            detalle_mayor = None

    except:
        messages.error(request, u"Error al cargar el comprobante de la nota de crédito")
        transaction.rollback()
        return HttpResponseRedirect(reverse("lista_nota_credito"))

    html = render_to_string('nota_credito/detalle_nota_credito_pdf.html',
                            {'pagesize': 'A4', "empresa": empresa,
                            "usuario": usuario, "ajuste_venta": ajuste_cabecera,
                            "request": request, "detalles": ajuste_detalle, "mayor_cab": mayor,
                            "detalle_may": detalle_mayor}, context_instance=RequestContext(request))

    return generar_pdf_get(html)

@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def anular_nota_credito(request, id):
    '''
    Vista para anular nota de crédito directamente completa
    :param request:
    :param id:
    :return:
    '''
    now = datetime.datetime.now()
    contador = 0
    try:
        ajuste_cabecera = AjusteVentaCabecera.objects.filter(status=1).get(id=id)
        ajuste_detalle = AjusteVentaDetalle.objects.filter(status=1, ajuste_venta_cabecera=ajuste_cabecera)

        try:
            # Anular Cuentas_x_Cobrar
            documento = Documento.objects.filter(status=1).get(id=4)    # NOTA CRÉDITO
            cuentas_x_cobrar = Cuentas_por_Cobrar.objects.filter(documento=documento).get(num_doc=ajuste_cabecera.vigencia_doc_empresa.serie+"-"+ajuste_cabecera.num_doc)
            cuentas_x_cobrar.status = 2
            cuentas_x_cobrar.fecha_actualizacion = now
            cuentas_x_cobrar.usuario_actualizacion = request.user.username
            cuentas_x_cobrar.save()

            if controla_stock():
                cuentas_x_cobrar.fecha_actualizacion = now
                cuentas_x_cobrar.usuario_actualizacion = request.user.username
                cuentas_x_cobrar.status = 2
                cuentas_x_cobrar.save()

            else:

                if cuentas_x_cobrar.cobrado > 0:
                    contador += 1
                    messages.error(request, u"No se puede anular la N/C, ya que se han hecho cobros a ella")

                else:
                    cuentas_x_cobrar.fecha_actualizacion = now
                    cuentas_x_cobrar.usuario_actualizacion = request.user.username
                    cuentas_x_cobrar.status = 2
                    cuentas_x_cobrar.save()

            ############################################################################################################
            #                           Anular C_x_Cobro                                                               #
            ############################################################################################################
            lista_cxc_cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_x_cobrar)
            for c in lista_cxc_cobro:
                c.status = 2
                c.fecha_actualizacion = now
                c.usuario_actualizacion = request.user.username
                c.save()
            ############################################################################################################

            ############################################################################################################
            #                           Anula el Cobro                                                                 #
            ############################################################################################################
            if CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_x_cobrar, status=1).exists():
                cobro = CXC_cobro.objects.filter(cuentas_x_cobrar=cuentas_x_cobrar, status=1)[0].cobro
                cobro.status = 2
                cobro.fecha_actualizacion = now
                cobro.usuario_actualizacion = request.user.username
                cobro.save()

        except Cuentas_por_Cobrar.DoesNotExist:
                contador += 1
                messages.error(request, u"Existe un problema al anular la N/C  no encuentra la cuenta por "
                                        u"cobrar asociada a la N/C seleccionada")

        # Anula Ajuste Cabecera
        ajuste_cabecera.status = 2
        ajuste_cabecera.fecha_actualizacion = now
        ajuste_cabecera.usuario_actualizacion = request.user.username

        # Si es electrónico le cambio el estado de la N/C
        if emite_docs_electronicos():
            ajuste_cabecera.estado_firma = 0

        ajuste_cabecera.save()

        # Anula Ajuste Detalle
        for det in ajuste_detalle:
            det.status = 2
            det.usuario_actualizacion = request.user.username
            det.fecha_actualizacion = now
            det.save()

        if ajuste_cabecera.venta_cabecera is not None:
            venta_detalles = Venta_Detalle.objects.filter(venta_cabecera=ajuste_cabecera.venta_cabecera)

            for v in venta_detalles:
                v.cant_devuelta -= 0 # cantidad
                v.usuario_actualizacion = request.user.username
                v.fecha_actualizacion = now
                v.save()

        # Anula Comprobantes Cabecera N/C
        comprobante_contable = Cabecera_Comp_Contable.objects.filter(status=1).get(numero_comprobante=ajuste_cabecera.num_comp)
        comprobante_contable.status = 2
        comprobante_contable.fecha_actualizacion = now
        comprobante_contable.usuario_actualizacion = request.user.username
        comprobante_contable.save()

        # Anula Comprobantes Detalle N/C
        for det_cont in Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=comprobante_contable):
            det_cont.status = 2
            det_cont.fecha_actualizacion = now
            det_cont.usuario_actualizacion = request.user.username
            det_cont.save()


        # Verifica si la venta realizó movimientos en inventario
        if Inventario_Cabecera.objects.filter(tipo_comprobante=TipoComprobante.objects.get(id=14), num_doc=ajuste_cabecera.num_doc).exists():
            Anula_Inventario_N_C(ajuste_cabecera, request, now)

        # Contrato Venta - N/C ???
        if contador == 0:
            messages.success(request, u"La N/C con # Comp: " + unicode(ajuste_cabecera.num_comp) +
                                  u" y # Doc.:"+ unicode(ajuste_cabecera.num_doc)+
                                  u" ha sido anulada exitosamente")
        else:
            transaction.rollback()

    except:
        transaction.rollback()
        messages.error(request, u"Existe un error al anular la N/C")
    return HttpResponseRedirect(reverse("lista_nota_credito"))

########################################################################################################################
#                                       VISTAS NOTA DE CRÉDITO ELECTRÓNICA                                             #
########################################################################################################################
@login_required(login_url='/')
#@permiso_accion(mensaje=mensaje_permiso)
@csrf_exempt
@transaction.commit_on_success
def enviar_nota_credito_electronica_sri(request, id):
    '''
    Vista que llama a las funciones de
    documentos electronicos de NOTA DE CRÉDITO que realiza
    el proceso según el API de Factora
    :param request:
    :param id:
    :return:
    '''
    try:
        ajuste_venta = AjusteVentaCabecera.objects.filter(status=1).get(id=id)
    except AjusteVentaCabecera.DoesNotExist:
        messages.error(request, u"Seleccione la nota de crédito con estado activo")
        ajuste_venta = None

    if ajuste_venta is not None:
        if ajuste_venta.status == 1:
            if ajuste_venta.estado_firma == 4:
                messages.success(request, u"La nota de crédito ya esta autorizada por el SRI")
                return HttpResponseRedirect(reverse("lista_nota_credito"))
            else:
                status_envio = envio_nota_credito(ajuste_venta)
                if status_envio[0] == 1:        # Se envió con éxito la nota de crédito al SRI
                    messages.success(request, u"Se ha realizado con éxito la firma y envio del documento")
                    return HttpResponseRedirect(reverse("lista_nota_credito"))
                else:
                    error = status_envio[1]
                    messages.error(request, unicode(error))
                    return HttpResponseRedirect(reverse("lista_nota_credito"))
        else:
            messages.error(request, u"La nota de crédito que seleccionó se encuentra anulada")
            return HttpResponseRedirect(reverse("lista_nota_credito"))
    else:
        messages.error(request, u"Error la nota de crédito que hizo referencia "
                                u"no puede ser enviada por favor verifique")
        return HttpResponseRedirect(reverse("lista_nota_credito"))

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
def enviar_correo_nota_credito(request):
    '''
    Vista que es llamada para enviar la información
    de la nota de crédito al cliente mediante
    su correo electrónico
    :param request:
    :return:
    '''
    form_correo = FormEnvioCorreoNotaCredito(request.POST)
    form_correo.is_valid()
    id = form_correo.get_id()
    try:
        ajuste_cabecera = AjusteVentaCabecera.objects.get(id=id)
    except AjusteVentaCabecera.DoesNotExist:
        ajuste_cabecera = None

    if request.method == "POST":
        if ajuste_cabecera.status == 1:
            if ajuste_cabecera.estado_firma >= 4:
                if enviar_correo_nota_credito_electronico(request, ajuste_cabecera):
                    respuesta = {"status": 1, "mensaje": ""}
                    resultado = json.dumps(respuesta)
                    return HttpResponse(resultado, mimetype='application/json')
                else:
                    messages.error(request, u"La Nota de Crédito no se encuentra todavía autorizada por el SRI")
        else:
            messages.error(request, u"La Nota de Crédito que seleccionó se encuentra anulada")

        respuesta = {"status": 2, "mensaje": ""}
        resultado = json.dumps(respuesta)
        return HttpResponse(resultado, mimetype='application/json')

########################################################################################################################
#                                                       AJAX                                                           #
########################################################################################################################
@csrf_exempt
@login_required(login_url='/')
def ajax_documentos_vigentes_nota_credito(request):
    '''
    Función Ajax que realiza la validación de vigencia de documento N/C según la fecha de emisión,
    la fecha de vencimiento y que la secuencia actual sea menor o igual a la secuencia final
    :param request:
    :return:
    '''
    q = request.POST.get("fecha", "")
    respuesta = []
    try:
        fecha_split = str(q).split("-")
        fecha = datetime.datetime(int(fecha_split[0]), int(fecha_split[1]), int(fecha_split[2]))
        vigencia = VigenciaDocEmpresa.objects.filter(status=1).filter(documento_sri__id=4,
                                                                      fecha_emi__lte=fecha.strftime("%Y-%m-%d"),
                                                                      fecha_vencimiento__gte=fecha.strftime("%Y-%m-%d"),
                                                                      sec_actual__lt=F("sec_final")).order_by("serie")
        if len(vigencia) == 0:
            respuesta.append({"status": 0, "id": 0, "descripcion": u"No se encuentra definido el documento "
                                                                   u"con su vigencia, favor ingrese"})
        else:
            for obj in vigencia:
                respuesta.append({"status": 1, "id": obj.id, "serie": obj.serie})
    except:
        pass
        respuesta.append({"status": -1, "id": -1, "descripcion": u"No se encuentra definido el documento "
                                                                 u"con su vigencia, favor ingrese"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@csrf_exempt
@login_required(login_url='/')
def ajax_seleccionar_nota_credito(request):
    id = request.POST.get("id", "")     # ID Serie de Documento Vigente
    try:
        documento_vigente = VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=datetime.datetime.now()).filter(documento_sri__id=4).get(id=id)
        response_data = {"id": documento_vigente.id, "serie": documento_vigente.serie,
                         "vencimiento": documento_vigente.fecha_vencimiento.strftime('%Y-%m-%d'),
                         "autorizacion": documento_vigente.autorizacion}
    except:
        pass
        response_data = {"respuesta": 'danger', "mensaje": 'Existió un error'}
    return HttpResponse(json.dumps(response_data), content_type="application/json; charset=utf-8")

@login_required(login_url='/')
@csrf_exempt
def ajax_seleccion_facturas(request):
    '''
    Función Ajax que me retorna los # de Documentos realicionados al cliente
    seleccionado
    :param request:
    :return:
    '''
    id_cliente = request.POST.get("id", "")
    dicionario = []
    try:
        cliente = Cliente.objects.get(id=id_cliente)
        q1 = Venta_Detalle.objects.filter(status=1).filter(venta_cabecera__cliente=cliente).extra(where=["cant_devuelta < cant_vendida + cant_entregada"])
        lista_ventas = Venta_Cabecera.objects.filter(venta_detalle__in=q1).order_by("num_documento").distinct()
        dicionario.append({"id": "0", "num_doc": u"DOCUMENTO NO REGISTRADO EN EL SISTEMA"})

        for obj in lista_ventas:
            dicionario.append({"id": obj.id, "num_doc": obj.num_documento})
        respuesta = {"status": 1, "documentos": dicionario}
    except:
        pass
        respuesta = {"status": 0,  "mensaje": "Error en el servidor"}
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@login_required(login_url='/')
@csrf_exempt
def get_datos_venta(request):
    '''
    Función Ajax que realiza la consulta para obtener el detalle de la venta
    :param request:
    :return html:
    '''
    id_venta = request.POST.get("id_venta")
    parametro_hide = 0
    cabecera_venta = Venta_Cabecera.objects.filter(status=1).get(id=id_venta)
    detalle_venta = Venta_Detalle.objects.filter(venta_cabecera=cabecera_venta).extra(where=["cant_vendida>cant_devuelta"])

    l_detalle_doc = []
    formset_detalle_docs = formset_factory(DetalleDevolucionAutomaticaForms, extra=0)

    if len(detalle_venta) == 0:
        parametro_hide = 1

    else:

        if detalle_venta:
            for obj in detalle_venta:
                if obj.item.TieneICE():
                    porc_ice = obj.item.porc_ice
                else:
                    porc_ice = 0.0
                l_detalle_doc.append({"id_detalle_venta": obj.id, "id_item": obj.item_id,
                                      "iva": obj.item.iva, "ice": porc_ice,
                                      "item": obj.item.nombre,
                                      "cant_vendida": obj.cantidad,
                                      "cant_ent": obj.cant_entregada,
                                      "desc_valor": obj.descuento_valor,
                                      "precio": obj.precio_unitario, "cant_dev": ""})

    form_ventas = formset_detalle_docs(initial=l_detalle_doc)
    return render_to_response('nota_credito/tabla_nota_credito_aut.html', {'forms': form_ventas,
                                                                           "parametro_hide": parametro_hide},
                                                                            context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def get_disponible_cantidad_devolver(request):
    '''
    Función AJAX que realiza la consulta para determinar si la cantidad a devolver
    es válida o no
    :param request:
    :return:
    '''
    id_detalle_venta = request.POST.get("id_detalle_venta")
    cantidad_dev = request.POST.get("cantidad_dev")
    respuesta = []
    try:
        detalle_venta = Venta_Detalle.objects.get(status=1, id=id_detalle_venta)

        if Venta_Detalle.objects.filter(status=1, id=id_detalle_venta).exists():
            cantidad_venta = float(detalle_venta.cantidad)
            r = float(cantidad_dev) + float(detalle_venta.cant_devuelta)

            if r > cantidad_venta:
                respuesta.append({"status": 0, "id": detalle_venta.id, "descripcion": u"Error la cantidad devuelta "
                                                                                      u"es mayor a la vendida"})

            elif float(detalle_venta.cant_devuelta) == cantidad_venta:
                respuesta.append({"status": 1, "id": detalle_venta.id, "descripcion": u"Items Vendidos han "
                                                                                      u"sido devueltos"})

            else:
                respuesta.append({"status": 2, "id": detalle_venta.id, "descripcion": "ok"})

    except:
        pass
        respuesta.append({"status": -1, "id": -1, "descripcion": "Error en el servidor"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")

@login_required(login_url='/')
@csrf_exempt
def get_correo_cliente_nota_credito(request):
    '''
    Función Ajax que obtiene el correo electrónico del Cliente que realizó
    la nota de crédito
    :param request:
    :return:
    '''
    id_nota_credito = request.POST.get("id", "")
    try:
        nota_credito = AjusteVentaCabecera.objects.get(id=id_nota_credito)
        if nota_credito.cliente.email:
            respuesta = {"status": 1, "correo": nota_credito.cliente.email}
        else:
            respuesta = {"status": 1, "correo": u"El cliente no tiene asignado "
                                                u"un correo electrónico por favor verifique"}
    except Venta_Cabecera.DoesNotExist:
        respuesta = {"status": 0, "mensaje": "El cliente no existe"}

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')
