#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'

import json
from contabilidad.formularios.ProveedorForm import *
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.db import connection
from django.core.paginator import *
from librerias.funciones.permisos import *
from django.contrib import messages
from contabilidad.funciones.inventario_func import *
from reportes.formularios.reportes_form import *
register = template.Library()


class ErrorInventario(Exception):
    def __init__(self, valor):
        self.valor = valor

    def __str__(self):
        return str(self.valor)


@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def lista_inventario(request):
    buscador = BuscadorInventarioForm(request.GET)
    inventarios = busqueda_inventario(buscador)
    paginacion = Paginator(inventarios, 50)
    numero_pagina = request.GET.get("page")


    try:
        lista_inventarios = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_inventarios = paginacion.page(1)
    except EmptyPage:
        lista_inventarios = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_inventarios.number

    return render_to_response('inventario/lista_inventarios.html',
                              {"objetos": lista_inventarios,
                               "buscador": buscador,
                               "total_paginas": total_paginas,
                               "numero": numero,
                              }, context_instance=RequestContext(request))


@csrf_exempt
@transaction.commit_on_success
@csrf_protect
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def registrar_inventario(request):
    now = datetime.datetime.now()
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    if empresa.fecha_ultimo_inventario is not None:
        cabecera_inv = CabeceraInvForm(initial={"fecha_reg": empresa.fecha_ultimo_inventario.strftime("%Y-%m-%d")})
    else:
        cabecera_inv = CabeceraInvForm(initial={"fecha_reg": now.strftime("%Y-%m-%d")})

    guiaform = FormGuiaRemisionCabecera(initial={"fecha_emision": now.strftime("%Y-%m-%d"),
                                                 "fecha_inicio": now.strftime("%Y-%m-%d")})

    detalle_compra_inventario_formset = formset_factory(CompraInventario, extra=1, formset=BaseDetalleInventFormSet)
    detalle_compra_inventario = detalle_compra_inventario_formset(prefix=CompraInventario().formset_prefix)

    detalle_inv_gr_form_formset = formset_factory(DetalleInventarioGRForm, extra=1, formset=BaseDetalleInventFormSet)
    detalle_inv_gr_form = detalle_inv_gr_form_formset(prefix=DetalleInventarioGRForm().formset_prefix)

    detalle_inv_aju_cant_form_formset = formset_factory(AjusteCantidadForm, extra=1, formset=BaseDetalleInventFormSet)
    detalle_inv_aju_cant = detalle_inv_aju_cant_form_formset(prefix=AjusteCantidadForm().formset_prefix)

    detalle_inv_aju_costo_form_formset = formset_factory(AjusteCostoForm, extra=1, formset=BaseDetalleInventFormSet)
    detalle_inv_aju_costo = detalle_inv_aju_costo_form_formset(prefix=AjusteCostoForm().formset_prefix)

    detalle_inv_contrato_form_formset = formset_factory(FormDetalleInventarioContrato, extra=1, formset=BaseDetalleInventFormSet)
    detalle_inv_contrato = detalle_inv_contrato_form_formset(prefix=FormDetalleInventarioContrato().formset_prefix)

    detalle_con_interno_form_formset = formset_factory(ConsumoInternoForm, extra=1, formset=BaseDetalleInventFormSet)
    detalle_con_interno = detalle_con_interno_form_formset(prefix=ConsumoInternoForm().formset_prefix)

    detalle_compra_inventario_inicial_formset = formset_factory(CompraInventarioInicial, extra=1, formset=BaseDetalleInventFormSet)
    detalle_compra_inventario_inicial = detalle_compra_inventario_inicial_formset(prefix=CompraInventarioInicial().formset_prefix)

    form_proveedor = ProveedorForm()

    contador = 0
    cont_mns_form = 0

    if request.method == "POST":
        cursor = connection.cursor()
        cabecera_inv = CabeceraInvForm(request.POST)
        guiaform = FormGuiaRemisionCabecera(request.POST)
        detalle_compra_inventario = detalle_compra_inventario_formset(request.POST, prefix=CompraInventario().formset_prefix)
        detalle_inv_gr_form = detalle_inv_gr_form_formset(request.POST, request.FILES, prefix=DetalleInventarioGRForm().formset_prefix)
        detalle_inv_aju_cant = detalle_inv_aju_cant_form_formset(request.POST, request.FILES, prefix=AjusteCantidadForm().formset_prefix)
        detalle_inv_aju_costo = detalle_inv_aju_costo_form_formset(request.POST, request.FILES, prefix=AjusteCostoForm().formset_prefix)
        detalle_inv_contrato = detalle_inv_contrato_form_formset(request.POST, request.FILES, prefix=FormDetalleInventarioContrato().formset_prefix)
        detalle_con_interno = detalle_con_interno_form_formset(request.POST, request.FILES, prefix=ConsumoInternoForm().formset_prefix)
        detalle_compra_inventario_inicial = detalle_compra_inventario_inicial_formset(request.POST, prefix=CompraInventarioInicial().formset_prefix)
        inventario_cab = Inventario_Cabecera()
        #try:
        if cabecera_inv.is_valid():
            if cabecera_inv.getTipo() == "1":  # Ingreso de Inventario
                if cabecera_inv.getMotivo() == "1":  # Compras
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioCompras(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)
                    if detalle_compra_inventario.is_valid():
                        print('tratando de guardar el detallecomprainventarro')
                        GuardarDetalleCompraInventario(detalle_compra_inventario, inventario_cab, cabecera_asiento, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle de compras verifique que están ingresados bien los valores.")

                elif cabecera_inv.getMotivo() == "2":  # Ventas
                    pass
                elif cabecera_inv.getMotivo() == "3":  # Producción - Transformación
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador += GuardarCabeceraInventarioRequisicionContrato(inventario_cab, empresa, detalle_inv_contrato, cabecera_inv, cabecera_asiento, contador,  now, request)

                elif cabecera_inv.getMotivo() == "4":  # Ajuste de cantidad
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioAjustes(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_inv_aju_cant.is_valid():
                        cont_mns_form, contador = GuardarDetalleInventarioAC(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle ajuste de cantidad revise si estan bien ingresados los valores.")

                elif cabecera_inv.getMotivo() == "5":  # Ajuste de Costo
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioAjustes(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_inv_aju_costo.is_valid():
                        cont_mns_form, contador = GuardarDetalleInventarioACosto(detalle_inv_aju_costo, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle ajuste de costo revise si estan bien ingresados los valores.")

                elif cabecera_inv.getMotivo() == "6":  # Guia de remisión Compras
                    contador = GuardarCabeceraInventarioGR(inventario_cab, cabecera_inv, empresa, cursor, contador, now, request)
                    if detalle_inv_gr_form.is_valid():
                        GuardarDetalleInventarioGR(detalle_inv_gr_form, inventario_cab, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle del inventario revise si estan bien ingresados los valores.")
                elif cabecera_inv.getMotivo() == "8":  # Consumo interno
                    asiento_cabecera = Cabecera_Comp_Contable()
                    contador += GuardarCabeceraInventarioConsumoInterno(inventario_cab, cabecera_inv, asiento_cabecera, empresa, contador, now, request)

                    if detalle_con_interno.is_valid():
                        contador += GuardarDetalleInventarioConsumoInternoI(detalle_con_interno, inventario_cab, asiento_cabecera, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Existen algunos errores en el detalle de consumo interno")

                elif cabecera_inv.getMotivo() == "9":  # Mantenimiento
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioMantenimiento(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_inv_aju_cant.is_valid():
                        cont_mns_form, contador = GuardarDetalleInventarioMantenimiento(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle de Mantenimiento revise si estan bien ingresados los valores.")

                elif cabecera_inv.getMotivo() == "10":  # Garantia
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioGarantia(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_inv_aju_cant.is_valid():
                        cont_mns_form, contador = GuardarDetalleInventarioGarantia(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle de Garantía revise si estan bien ingresados los "
                                                u"valores.")

                elif cabecera_inv.getMotivo() == "11":  # Inventario Inicial
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioInicial(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_compra_inventario_inicial.is_valid():
                        contador = GuardarDetalleCompraInventarioInicial(detalle_compra_inventario_inicial, inventario_cab, cabecera_asiento, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle de inventario inicial verifique que están ingresados bien los valores.")

                else:
                    messages.error(request, u"Error opción no válida para un ingreso de inventario")
                    contador += 1

            else:  # Egreso de inventario

                if cabecera_inv.getMotivo() == "1":  # Compras
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioComprasE(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)
                    if detalle_compra_inventario.is_valid():
                        contador += GuardarDetalleCompraInventarioE(detalle_compra_inventario, inventario_cab, cabecera_asiento, contador, cont_mns_form, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle de compras verifique que están ingresados bien los valores.")

                elif cabecera_inv.getMotivo() == "2":  # Ventas
                    pass

                elif cabecera_inv.getMotivo() == "3":  # Producción - Transformación
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador += GuardarCabeceraInventarioRequisicionContrato(inventario_cab, empresa, detalle_inv_contrato, cabecera_inv, cabecera_asiento, contador,  now, request)

                elif cabecera_inv.getMotivo() == "4":  # Ajuste de cantidad
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioAjustesE(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_inv_aju_cant.is_valid():
                        cont_mns_form, contador = GuardarDetalleInventarioAjusteCantidadE(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle ajuste de cantidad revise si estan bien ingresados los valores.")

                elif cabecera_inv.getMotivo() == "5":  # Ajuste de Costo
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraInventarioAjustesE(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_inv_aju_costo.is_valid():
                        cont_mns_form, contador = GuardarDetalleInventarioAjusteCostoE(detalle_inv_aju_costo, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle ajuste de cantidad revise si estan bien ingresados los valores.")

                elif cabecera_inv.getMotivo() == "6":  # Guia de remisión Compras
                    contador = GuardarCabeceraInventarioGRE(inventario_cab, cabecera_inv, empresa, cursor, contador, now, request)

                    if detalle_inv_gr_form.is_valid():
                        cont_mns_form, contador = GuardarDetalleInventarioGRE(detalle_inv_gr_form, inventario_cab, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle del inventario revise si estan bien ingresados los valores.")

                elif cabecera_inv.getMotivo() == "7":  # Guia de remisión Ventas
                    guia = CabeceraGuiaRemision()
                    if guiaform.is_valid():
                        contador = GuardarCabeceraInventarioVentaGR(inventario_cab, guia, guiaform, cabecera_inv, empresa, cursor, contador, now, request)

                        if detalle_inv_gr_form.is_valid():
                            contador += GuardarDetalleInventarioVentaGR(detalle_inv_gr_form, guia, inventario_cab, now, contador, request)
                        else:
                            contador += 1
                            messages.error(request, u"Error en el detalle del inventario revise si estan bien ingresados los valores.")
                    else:
                        messages.error(request, u"Existen algunos errores en la guía de remisión")
                        contador += 1
                elif cabecera_inv.getMotivo() == "8":  # Consumo interno
                    asiento_cabecera = Cabecera_Comp_Contable()
                    contador += GuardarCabeceraInventarioConsumoInterno(inventario_cab, cabecera_inv, asiento_cabecera, empresa, contador, now, request)

                    if detalle_con_interno.is_valid():
                        contador, cont_mns_form = GuardarDetalleInventarioConsumoInternoE(detalle_con_interno, inventario_cab, asiento_cabecera, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Existen algunos errores en el detalle de consumo interno")
                elif cabecera_inv.getMotivo() == "9":  # Mantenimiento
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraMantenimientoE(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_inv_aju_cant.is_valid():
                        cont_mns_form, contador = GuardarDetalleMantenimientoE(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle de Mantenimiento revise si estan bien ingresados los valores.")

                elif cabecera_inv.getMotivo() == "10":  # Garantia
                    cabecera_asiento = Cabecera_Comp_Contable()
                    contador = GuardarCabeceraGarantiaE(inventario_cab, cabecera_inv, cabecera_asiento, empresa, cursor, contador, now, request)

                    if detalle_inv_aju_cant.is_valid():
                        cont_mns_form, contador = GuardarDetalleGarantiaE(detalle_inv_aju_cant, inventario_cab, cabecera_asiento, cont_mns_form, contador, now, request)
                    else:
                        contador += 1
                        messages.error(request, u"Error en el detalle de Garantía revise si estan bien ingresados los valores.")

                # No hay inventario inicial en Egreso

                else:
                    messages.error(request, u"Error opción no válida para un egreso de inventario")
                    contador += 1

            if contador == 0:
                num_comp = inventario_cab.num_comp
                num_doc = inventario_cab.num_doc
                if None not in (num_comp, num_doc) and "" not in (num_comp, num_doc):
                    messages.success(request, u"Se registró exitosamente el inventario con número de comprobante: " +
                                              num_comp + u" y número de documento: " + num_doc)
                elif num_comp is not None and num_comp != "":
                    messages.success(request, u"Se registró exitosamente el inventario con número de comprobante: " +
                                              num_comp)
                else:
                    messages.success(request, u"Se registró exitosamente el inventario con número de documento: " + num_doc)

                return HttpResponseRedirect(reverse("lista_inventario"))
            else:
                transaction.rollback()
                if cont_mns_form > 0:
                    messages.error(request, u"El item que quiere hacer egreso no "
                                            u"existe en bodega o supera su cantidad actual")

                return render_to_response('inventario/registrar_inventario.html',
                          {"formulario": cabecera_inv,
                           "guia": guiaform,
                           "detalle_compra_inventario": detalle_compra_inventario,
                           "detalle_compra_inventario_inicial": detalle_compra_inventario_inicial,
                           "detalle_inv_gr_form": detalle_inv_gr_form,
                           "detalle_inv_aju_cant": detalle_inv_aju_cant,
                           "detalle_inv_aju_costo": detalle_inv_aju_costo,
                           "detalle_inv_contrato": detalle_inv_contrato,
                           "detalle_con_interno": detalle_con_interno,
                           "form_proveedor": form_proveedor
                          }, context_instance=RequestContext(request))
        else:
            messages.error(request, u"Algunos campos de la cabecera no son válidos, por favor revise")
        #except:
        #    messages.error(request, u"Error de conexión")
        transaction.rollback()
    return render_to_response('inventario/registrar_inventario.html',
                              {"formulario": cabecera_inv,
                               "guia": guiaform,
                               "detalle_compra_inventario": detalle_compra_inventario,
                               "detalle_compra_inventario_inicial": detalle_compra_inventario_inicial,
                               "detalle_inv_gr_form": detalle_inv_gr_form,
                               "detalle_inv_aju_cant": detalle_inv_aju_cant,
                               "detalle_inv_aju_costo": detalle_inv_aju_costo,
                               "detalle_inv_contrato": detalle_inv_contrato,
                               "detalle_con_interno": detalle_con_interno,
                               "form_proveedor": form_proveedor
                              }, context_instance=RequestContext(request))

@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def detalle_inventario(request, id):
    try:
        inventario = Inventario_Cabecera.objects.filter(~Q(status=0)).get(id=id)
        detalle_inv = Inventario_Detalle.objects.filter(inventario_cabecera=inventario).exclude(status=0)
        total_cantidad = 0.0
        total_costo = 0.0
        total = 0.0
        total_debe = 0.0
        total_haber = 0.0

        try:
            cabecera_comp = Cabecera_Comp_Contable.objects.filter(~Q(status=0)).get(numero_comprobante=inventario.num_comp)
            detalle_may = Detalle_Comp_Contable.objects.filter(cabecera_contable=cabecera_comp).exclude(status=0).order_by("dbcr", "plan_cuenta__codigo")
            for obj in detalle_inv:
                if obj.costo and obj.cantidad:
                    total_costo += obj.costo
                    total_cantidad += obj.cantidad
                    total += (obj.costo * obj.cantidad)

            for obj in detalle_may:
                if obj.dbcr == "D":
                    total_debe += obj.valor
                else:
                    total_haber += obj.valor

        except (Cabecera_Comp_Contable.DoesNotExist, Cabecera_Comp_Contable.MultipleObjectsReturned):
            detalle_may = None

        return render_to_response('inventario/detalle_inventario.html',
                                  {"inventario": inventario,
                                   "detalle_may": detalle_may,
                                   "detalle_inv": detalle_inv,
                                   "total_cantidad": total_cantidad,
                                   "total_costo": total_costo,
                                   "total": total,
                                   "total_debe": total_debe,
                                   "total_haber": total_haber
                                   }, context_instance=RequestContext(request))

    except Inventario_Cabecera.DoesNotExist:
        raise Http404

@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def detalle_inventario_pdf(request, id):
    try:
        inventario = Inventario_Cabecera.objects.filter(~Q(status=0)).get(id=id)
        detalle_inv = Inventario_Detalle.objects.filter(inventario_cabecera=inventario).exclude(status=0)
        now = datetime.datetime.now()
        id_empresa = Empresa.objects.filter(status=1)[0].empresa_general_id
        empresa = EmpresaGeneral.objects.get(id=id_empresa)
        total_cantidad = 0.0
        total_costo = 0.0
        total = 0.0
        total_debe = 0.0
        total_haber = 0.0

        try:
            cabecera_comp = Cabecera_Comp_Contable.objects.filter(~Q(status=0)).get(numero_comprobante=inventario.num_comp)
            detalle_may = Detalle_Comp_Contable.objects.filter(~Q(status=0)).filter(cabecera_contable=cabecera_comp).order_by("dbcr", "plan_cuenta__codigo")
            for obj in detalle_inv:
                if obj.costo and obj.cantidad:
                    total_costo += obj.costo
                    total_cantidad += obj.cantidad
                    total += (obj.costo * obj.cantidad)

            for obj in detalle_may:
                if obj.dbcr == "D":
                    total_debe += obj.valor
                else:
                    total_haber += obj.valor

        except (Cabecera_Comp_Contable.DoesNotExist, Cabecera_Comp_Contable.MultipleObjectsReturned):
            detalle_may = None
            cabecera_comp = None

        html = render_to_string('inventario/detalle_inventario_pdf.html',
                                {'pagesize': 'A4',
                                 "cab_inv": inventario,
                                 "mayor_cab": cabecera_comp,
                                 "empresa":empresa,
                                 "detalle_inv": detalle_inv,
                                 "fecha_actual": now,
                                 "detalle_may": detalle_may,
                                 "total_cantidad": total_cantidad,
                                 "total_costo": total_costo,
                                 "total": total,
                                 "total_debe": total_debe,
                                 "total_haber": total_haber,
                                 }, context_instance=RequestContext(request))
        return generar_pdf_get(html)

    except Inventario_Cabecera.DoesNotExist:
        raise Http404


@csrf_exempt
@transaction.commit_on_success
@csrf_protect
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def convertir_guia_compra(request):
    cabecera_form = GuiaCompraCabeceraForm()
    detalle_form_formset = formset_factory(GuiaCompraForm)
    detalle_form = detalle_form_formset()
    contador = 0
    now = datetime.datetime.now()
    if len(Proveedores.objects.filter(proveedor_item__in=Proveedor_Item.objects.filter(status=1, cantidad_provision__gt=0).distinct())) == 0:
        messages.success(request, u"Por el momento no tiene proveedores con el cual tenga una guía de "
                                  u"remisión")
    bandera = False
    if request.method == "POST":
        try:
            cursor = connection.cursor()
            inventario_cab = Inventario_Cabecera()
            cabecera_asiento = Cabecera_Comp_Contable()
            cabecera_form = GuiaCompraCabeceraForm(request.POST)
            detalle_form = detalle_form_formset(request.POST)
            bandera = True
            if cabecera_form.is_valid():
                contador = GuardarCabeceraConvGuiaCompra(inventario_cab, cabecera_form, cabecera_asiento, cursor, contador, now, request)
                if detalle_form.is_valid():
                    id = cabecera_form.getProveedor()
                    contador = GuardarDetalleConvertirGuiaCompra(id, cabecera_form, detalle_form, inventario_cab, cabecera_asiento, contador, now, request)
                else:
                    contador += 1
                    messages.error(request, u"Por favor ingrese todos los campos obligatorios del detalle")
            else:
                contador += 1
                messages.error(request, u"Por favor ingrese todos los campos obligatorios de la cabecera")
            if contador == 0:
                messages.success(request, u"Se registró exitosamente la compra de inventario")
                return HttpResponseRedirect(reverse("lista_inventario"))
            else:
                transaction.rollback()
                return render_to_response('inventario/convertir_guia_compra.html',
                              {"formulario": cabecera_form,
                               "detalle_form": detalle_form,
                               "bandera": bandera
                              }, context_instance=RequestContext(request))
        except:
            transaction.rollback()
            messages.error(request, u"El proveedor no posee productos pendientes en guia de remisión")
    return render_to_response('inventario/convertir_guia_compra.html',
                              {"formulario": cabecera_form,
                               "detalle_form": detalle_form,
                               "bandera": bandera
                              }, context_instance=RequestContext(request))

@login_required(login_url="/")
def buscar_item(request):
    q = request.GET.get("name", "")
    items = Item.objects.filter(status=1).filter(Q(descripcion__istartswith=q) | Q(codigo__istartswith=q))[0:8]
    respuesta = []
    for obj in items:
        respuesta.append({"name": obj.codigo + " - " + obj.descripcion, "id": obj.id,
                          "cuenta": obj.categoria_item.plan_cuenta_compra.id,
                          "id_cuenta":obj.categoria_item.plan_cuenta_compra.id,
                          "unidad": obj.unidad.descripcion})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@login_required(login_url="/")
def buscar_cuenta(request):
    q = request.GET.get("name", "")
    cuentas = PlanCuenta.objects.filter(status=1, nivel=5).filter(Q(descripcion__istartswith=q) | Q(codigo__istartswith=q))[0:8]
    respuesta = []
    for obj in cuentas:
        respuesta.append({"name": obj.codigo + " - " + obj.descripcion, "id": obj.id})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@csrf_exempt
@login_required(login_url="/")
def validar_fecha_inventario_ie(request):
    """
    función que valida si la fecha del inventario es
    válida que se encuentra en Empresa_Parámetro

    :param request:
    :return:
    """
    fecha_post = str(request.POST.get("fecha", "")).split("-")
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa_parametro = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)
    fecha_u_ing_inv = empresa_parametro.fecha_ultimo_inventario

    try:
        fecha = datetime.datetime(int(fecha_post[0]), int(fecha_post[1]), int(fecha_post[2])).date()
        if fecha_u_ing_inv is not None:
            if fecha >= fecha_u_ing_inv:
                respuesta = ({"status": 1})
            else:
                respuesta = ({"status": 0, "fecha": fecha_u_ing_inv.strftime("%Y-%m-%d"), "tipo": "transacción"})
        else:
            respuesta = ({"status": 1})
    except:  # Si ingresan una fecha con formato no válido
        respuesta = ({"status": 0, "fecha": fecha_u_ing_inv.strftime("%Y-%m-%d"), "tipo": "transacción"})

    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')

@csrf_exempt
@login_required(login_url="/")
def get_compras_inventario(request):
    """
    Funcion en standby
    :param request:
    :return:
    """
    try:
        id_prov = request.POST.get("id_prov")
        num_doc = request.POST.get("num_doc")
        doc = Documento.objects.get(id=request.POST.get("id_doc"))
        tipo_mov_inv = Tipo_Mov_Inventario.objects.get(id=1)
        tipo_comp = TipoComprobante.objects.get(id=14)
        tipo_comp_e = TipoComprobante.objects.get(id=15)  # Egreso de compra
        #try:
        inv_cabe = Inventario_Cabecera.objects.get(cliente_proveedor_id=id_prov,
                                                   tipo_mov_inventario=tipo_mov_inv,
                                                   tipo_comprobante=tipo_comp,
                                                   documento=doc,
                                                   num_doc=num_doc,
                                                   status=1)

        inv_cabe_egres = Inventario_Cabecera.objects.filter(cliente_proveedor_id=id_prov,
                                                            tipo_mov_inventario=tipo_mov_inv,
                                                            tipo_comprobante=tipo_comp_e,
                                                            documento=doc,
                                                            num_doc=num_doc,
                                                            status=1)

        formulario_formset = formset_factory(CompraEgresoInventario, extra=0)
        l = []
        for obj in Inventario_Detalle.objects.filter(inventario_cabecera=inv_cabe):
            l.append({"id_det_inv": obj.id,
                      "item": obj.item.codigo + obj.item.descripcion,
                      "cant_vend": obj.cantidad,
                      "cantidad": 0,
                      "costo": 0,
                      "p_unitario": ""})

        formulario = formulario_formset(initial=l)
        flag = True
        return render_to_response('inventario/tabla_egreso_compras.html',
            {"flag": flag,
             "formulario": formulario}, context_instance=RequestContext(request))

    except Inventario_Cabecera.DoesNotExist:
        flag = False
        messages.success(request, u"No se encuentra la compra que ha ingresado")
        return render_to_response('inventario/tabla_egreso_compras.html',
            {"flag": flag}, context_instance=RequestContext(request))
    #except:
    #    pass


@login_required(login_url='/')
@transaction.commit_on_success
@csrf_exempt
def modificar_inventario(request):
    lista = []
    # Parámetro que se envía desde el modal indicando que acepta realizar el cambio
    parametro = request.GET.get("reprocesar", "")
    buscador = BuscadorReportes(initial={"anio": datetime.datetime.now().year})

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)

        if buscador.is_valid():

            if buscador.getAnio() != "" and buscador.getItem() is None:
                anio_reversar = buscador.getAnio()

                guardar_detalle_reproceso_tmp(anio_reversar, request)
                messages.success(request, u"Se ha realizado los cambios exitosamente")
                #lista = reprocesar_inventario(anio_reversar, parametro, request)

                if parametro == "1":
                    #reprocesar_inventario_contabilidad(anio_reversar, request)
                    #reprocesarCostoInventario(request)
                    messages.success(request, u"Se ha realizado los cambios exitosamente")
                    return HttpResponseRedirect(reverse("modificar_inventario"))

                else:

                    if len(lista) == 0.0:
                        messages.error(request,  u"No existe movimientos de inventario para el perido: "+str(buscador.getAnio()))


            if buscador.getAnio() != "" and buscador.getItem() is not None:
                anio_reversar = buscador.getAnio()
                item = buscador.getItem()
                lista = reprocesar_inventario_item(anio_reversar, parametro, item, request)

                if parametro == "1":
                    #reprocesar_inventario_contabilidad(anio_reversar, request)
                    #reprocesarCostoInventarioItem(item, request)
                    messages.success(request, u"Se ha realizado los cambios exitosamente de su item: "+str(item.codigo)+" - "+unicode(item.nombre))
                    return HttpResponseRedirect(reverse("modificar_inventario"))


                if len(lista) == 0.0:
                    messages.error(request,  u"No existe movimientos de inventario para el item: "+
                                             str(item.codigo)+" - "+unicode(item.nombre)+
                                             u" para el perido: "+str(buscador.getAnio()))

    return render_to_response('inventario/modificar_inventario.html',
                              {"buscador": buscador,
                                "lista": lista}, context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url="/")
def recambio_inventario(request):
    lista = []
    lista_items_cont = []
    contador = 0.0
    lista.append([u"", u"", u"", u"", u"", u"REPORTE INVENTARIO"+TITULO])
    lista.append([u""])
    lista_items_cont.append([u"", u"", u"", u"", u"", u"ITEMS A REVISAR EN INVENTARIO"+TITULO])
    lista_items_cont.append([u""])

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        if buscador.is_valid():
            anio_reversar = buscador.getAnio()
            lista.append([u"Año: "+SUBTITULO, anio_reversar])
            lista.append([u""])
            lista.append([u""])

            try:
                lista_items = Item.objects.filter(status=1, categoria_item__tipo_item_id=1).order_by("codigo")
                lista_items_cont.append([u""])
                lista_items_cont.append([u"ID"+COLUMNA, u"CÓDIGO"+COLUMNA, u"NOMBRE"+COLUMNA])

                for c in lista_items:

                    if Item_Bodega_Anual.objects.filter(status=1, anio=anio_reversar, item=c).exists() and Item_Anual.objects.filter(status=1, anio=anio_reversar, item=c).exists():
                        item_anual = Item_Anual.objects.get(status=1, anio=anio_reversar, item=c)
                        item_bodega = Item_Bodega_Anual.objects.get(status=1, anio=anio_reversar, item=c)
                        ################################ DATOS INICIALES ###############################################
                        costo_inicial = item_anual.costo_actual
                        cantidad_inicial = item_bodega.cantidad_actual
                        ################################################################################################
                        cantidad_act = cantidad_inicial
                        costo_act = costo_inicial
                    else:
                        costo_inicial = 0.0
                        cantidad_inicial = 0.0
                        cantidad_act = 0.0
                        costo_act = 0.0

                    try:
                        cantidad_actual_item = Item_Bodega.objects.get(status=1, item=c).cantidad_actual
                    except:
                        cantidad_actual_item = 0.0

                    lista.append([u""])
                    lista.append([u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL,
                                  u""+G_TOTAL, u""+G_TOTAL])

                    lista.append([u"Item: "+SUBTITULO, unicode(c.codigo), unicode(c.nombre), u"", u"",
                                    u"Cantidad_Inicial: "+SUBTITULO, cantidad_inicial, u"Costo_Inicial: "+SUBTITULO, costo_inicial])

                    lista.append([u"", u"", u"", u"", u"", u"Cantidad Final: "+SUBTITULO, cantidad_actual_item,
                                  u"Costo Final:" +SUBTITULO, c.costo])


                    if Inventario_Detalle.objects.filter(status=1, item=c).exists():
                        lista.append([u""])
                        lista.append([u"Fecha_Reg"+COLUMNA, u"Fecha_Creación"+COLUMNA, u"Num_Comp"+COLUMNA, u"Movimiento"+COLUMNA,
                                      u"Cantidad_Inv"+COLUMNA, u"Costo_Inv"+COLUMNA,
                                      u"Costo_Proceso"+COLUMNA, u"Cantidad Actual"+COLUMNA,
                                      u"Costo Actual"+COLUMNA, u"ESTADO"+COLUMNA])

                        '''
                        lista.append([u"Fecha_Reg"+COLUMNA, u"Fecha_Creación"+COLUMNA, u"Num_Comp"+COLUMNA, u"Movimiento"+COLUMNA, u"Cantidad_Inv"+COLUMNA, u"Costo_Inv"+COLUMNA,
                                      u"Cantidad Actual"+COLUMNA, u"Costo Actual"+COLUMNA, u"ESTADO"+COLUMNA])
                        '''
                    else:
                        lista.append([u"No posee Movimientos de inventario"+COLUMNA])
                        lista.append([u""])

                    #for det_inv in Inventario_Detalle.objects.filter(status=1, item=c, inventario_cabecera__fecha_reg__year=anio_reversar).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__tipo_comprobante__id", "inventario_cabecera__num_comp"):

                    for det_inv in Inventario_Detalle.objects.filter(status=1, item=c, inventario_cabecera__fecha_reg__year=anio_reversar).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__fecha_creacion"):
                        linea_inv = det_inv.costo

                        #Ajuste Cantidad - Ventas - Producción - C.Interno
                        if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo:
                            det_inv.costo = costo_act

                        if det_inv.cantidad == 0.0:
                            costo_linea = float(det_inv.costo)
                        else:
                            costo_linea = float(det_inv.cantidad*det_inv.costo)

                        if det_inv.inventario_cabecera.tipo_comprobante.id == 14:   # Ingreso

                            if cantidad_act + det_inv.cantidad == 0.0:
                                costo_act = 0.0
                                cantidad_act = 0.0
                                contador += 1
                            else:
                                contador = 0.0
                                costo_act = (float(cantidad_act*costo_act) + float(costo_linea)) / (float(cantidad_act) + float(det_inv.cantidad))
                                cantidad_act = (float(cantidad_act) + float(det_inv.cantidad))
                        else:

                            if cantidad_act - det_inv.cantidad == 0.0:
                                costo_act = 0.0
                                cantidad_act = 0.0
                                contador += 1
                            else:
                                contador = 0.0
                                costo_act = (float(cantidad_act*costo_act) - float(costo_linea)) / (float(cantidad_act) - float(det_inv.cantidad))
                                cantidad_act = (float(cantidad_act) - float(det_inv.cantidad))


                        if contador > 0.0:
                            fila_item = [det_inv.item.id, det_inv.item.codigo, det_inv.item.nombre]
                            lista_items_cont.append(fila_item)


                        if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo:
                            if son_iguales(redondeo(linea_inv, 7), redondeo(costo_act, 7), 5):
                                cadena = "                 "
                            else:
                                cadena = "      ***********"
                        else:
                            cadena = ""

                        '''
                        fila = [str(det_inv.inventario_cabecera.fecha_reg), str(det_inv.inventario_cabecera.fecha_creacion),
                                det_inv.inventario_cabecera.num_comp, det_inv.inventario_cabecera.tipo_mov_inventario.descripcion,
                                det_inv.cantidad, str(linea_inv), cantidad_act, str(costo_act), cadena]
                        '''

                        fila = [det_inv.inventario_cabecera.fecha_reg, str(det_inv.inventario_cabecera.fecha_creacion),
                                det_inv.inventario_cabecera.num_comp,
                                det_inv.inventario_cabecera.tipo_mov_inventario.descripcion, det_inv.cantidad,
                                str(linea_inv), str(det_inv.costo), cantidad_act, str(costo_act), cadena]

                        '''
                        if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo:
                            # ANTERIOR
                            fila = [det_inv.inventario_cabecera.fecha_reg, det_inv.inventario_cabecera.num_comp, det_inv.inventario_cabecera.tipo_mov_inventario.descripcion,
                                    det_inv.cantidad, str(det_inv.costo), str(linea_inv), cantidad_act, str(costo_act), cadena]
                            #CAMBIO
                            fila = [str(det_inv.inventario_cabecera.fecha_reg), str(det_inv.inventario_cabecera.fecha_creacion.date()), det_inv.inventario_cabecera.num_comp, det_inv.inventario_cabecera.tipo_mov_inventario.descripcion,
                                    det_inv.cantidad, str(det_inv.costo), cantidad_act, str(costo_act), cadena]
                        else:
                            # ANTERIOR
                            fila = [det_inv.inventario_cabecera.fecha_reg, det_inv.inventario_cabecera.num_comp, det_inv.inventario_cabecera.tipo_mov_inventario.descripcion,
                                    det_inv.cantidad, str(det_inv.costo), str(linea_inv), cantidad_act, str(det_inv.costo), cadena]
                            #CAMBIO
                            fila = [str(det_inv.inventario_cabecera.fecha_reg), str(det_inv.inventario_cabecera.fecha_creacion.date()), det_inv.inventario_cabecera.num_comp, det_inv.inventario_cabecera.tipo_mov_inventario.descripcion,
                                    det_inv.cantidad, str(det_inv.costo), cantidad_act, str(det_inv.costo), cadena]
                        '''

                        lista.append(fila)
            except:
                pass
        return ExcelResponse(lista)













class Cantidad_Inicial_Bodega():

    """
    Clase que me ayuda a agrupar la
    información de la Cantidad Inicial Bodega Anual
    """
    def __init__(self):
        self.item_bodega_anual = Item_Bodega_Anual()



@csrf_exempt
@login_required(login_url="/")
def repaso_reproceso_inventario(request):
    lista = []
    lista_items_cont = []
    contador = 0.0
    lista.append([u"", u"", u"", u"", u"", u"REPORTE INVENTARIO"+TITULO])
    lista.append([u""])
    lista_items_cont.append([u"", u"", u"", u"", u"", u"ITEMS A REVISAR EN INVENTARIO"+TITULO])
    lista_items_cont.append([u""])

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        if buscador.is_valid():
            anio_reversar = buscador.getAnio()
            lista.append([u"Año: "+SUBTITULO, anio_reversar])
            lista.append([u""])
            lista.append([u""])

            try:
                lista_items = Item.objects.filter(status=1, categoria_item__tipo_item_id=1).order_by("codigo")
                lista_items_cont.append([u""])
                lista_items_cont.append([u"ID"+COLUMNA, u"CÓDIGO"+COLUMNA, u"NOMBRE"+COLUMNA])

                for c in lista_items:

                    try:
                        cantidad_inicial = Item_Bodega_Anual.objects.get(status=1, anio=anio_reversar, item=c).cantidad_incial
                    except (Item_Bodega_Anual.DoesNotExist, ValueError):
                        cantidad_inicial = 0

                    if cantidad_inicial > 0:
                        ################################ DATOS INICIALES ###############################################
                        costo_act = Item_Anual.objects.get(status=1, anio=anio_reversar, item=c).costo_actual
                        cantidad_act = cantidad_inicial
                        costo_inicial = costo_act

                    else:
                        costo_inicial = 0.0
                        cantidad_inicial = 0.0
                        cantidad_act = 0.0
                        costo_act = 0.0

                    '''
                    try:
                        cantidad_actual_item = Item_Bodega.objects.get(status=1, item=c).cantidad_actual
                    except:
                        cantidad_actual_item = 0.0
                    '''

                    lista.append([u""])
                    lista.append([u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL, u""+G_TOTAL,
                                  u""+G_TOTAL, u""+G_TOTAL])
                    lista.append([u"Item: "+SUBTITULO, unicode(c.codigo), unicode(c.nombre), u"", u"",
                                    u"Cantidad_Inicial: "+SUBTITULO, cantidad_inicial, u"Costo_Inicial: "+SUBTITULO, costo_inicial])
                    '''
                    lista.append([u"", u"", u"", u"", u"", u"Cantidad Final: "+SUBTITULO, cantidad_actual_item,
                                  u"Costo Final:" +SUBTITULO, c.costo])
                    '''

                    if Inventario_Detalle.objects.filter(status=1, item=c, inventario_cabecera__fecha_reg__year=anio_reversar).exists():
                        lista.append([u""])
                        lista.append([u"Fecha_Reg"+COLUMNA, u"Fecha_Creación"+COLUMNA, u"Num_Comp"+COLUMNA, u"Movimiento"+COLUMNA,
                                      u"Cantidad_Inv"+COLUMNA, u"Costo_Inv"+COLUMNA, u"Cantidad Actual"+COLUMNA,
                                      u"Costo Actual"+COLUMNA, u"ESTADO"+COLUMNA])
                    else:
                        lista.append([u"No posee Movimientos de inventario"+COLUMNA])
                        lista.append([u""])


                    for det_inv in Inventario_Detalle.objects.filter(status=1, item=c, inventario_cabecera__fecha_reg__year=anio_reversar).order_by("inventario_cabecera__fecha_reg", "inventario_cabecera__fecha_creacion", "inventario_cabecera__tipo_comprobante__id", "inventario_cabecera__num_comp"):
                        linea_inv = det_inv.costo

                        #Ajuste Cantidad - Ventas - Producción - C.Interno
                        if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo:
                            det_inv.costo = costo_act

                        if det_inv.cantidad == 0.0:
                            costo_linea = float(det_inv.costo)
                        else:
                            costo_linea = float(det_inv.cantidad*det_inv.costo)


                        if det_inv.inventario_cabecera.tipo_comprobante.id == 14:   # Ingreso

                            if cantidad_act + det_inv.cantidad == 0.0:
                                costo_act = 0.0
                                cantidad_act = 0.0
                            else:
                                costo_act = (float(cantidad_act*costo_act) + float(costo_linea)) / (float(cantidad_act) + float(det_inv.cantidad))
                                cantidad_act = (float(cantidad_act) + float(det_inv.cantidad))

                        else:

                            if cantidad_act - det_inv.cantidad == 0.0:
                                costo_act = 0.0
                                cantidad_act = 0.0
                            else:
                                costo_act = (float(cantidad_act*costo_act) - float(costo_linea)) / (float(cantidad_act) - float(det_inv.cantidad))
                                cantidad_act = (float(cantidad_act) - float(det_inv.cantidad))

                        '''
                        if contador > 0.0:
                            fila_item = [det_inv.item.id, det_inv.item.codigo, det_inv.item.nombre]
                            lista_items_cont.append(fila_item)
                        '''

                        #if det_inv.inventario_cabecera.tipo_mov_inventario.modifica_costo:
                        if son_iguales(redondeo(linea_inv, 9), redondeo(costo_act, 9), 9):
                            cadena = "                 "
                        else:
                            cadena = "      ***********"
                        #else:
                        #    cadena = ""

                        fila = [det_inv.inventario_cabecera.fecha_reg, str(det_inv.inventario_cabecera.fecha_creacion),
                                det_inv.inventario_cabecera.num_comp,
                                det_inv.inventario_cabecera.tipo_mov_inventario.descripcion, det_inv.cantidad,
                                str(linea_inv),  cantidad_act, str(costo_act), cadena]

                        lista.append(fila)
            except:
                pass
        return ExcelResponse(lista)





@login_required(login_url="/")
#@permiso_accion(mensaje=mensaje_permiso)
@csrf_exempt
def cierre_anual_inventario(request):
    lista = []
    buscador = CierreAnualForm(initial={"anio": datetime.datetime.now().year})
    cursor = connection.cursor()

    if request.method == "POST":
        buscador = CierreAnualForm(request.POST)
        if buscador.is_valid():

            if buscador.getAnio() > 0:
                buscador = CierreAnualForm(request.POST)
                if realiza_cierre():
                    cursor.execute('select proceso_cierre_inventario_anual(%s)', [str(buscador.getAnio())])
                    messages.success(request, u"Se ha realizado el cierre anual de "
                                              u"inventario del año "+str(buscador.getAnio()))
                else:
                    transaction.rollback()
                    messages.error(request, u"Error al realizar el cierre anual de "
                                            u"inventario del año "+str(buscador.getAnio()))
            else:
                messages.error(request, u"Por favor ingrese un año para realizar el proceso")

    return render_to_response('inventario/cierreanual.html',
                              {"buscador": buscador, "lista": lista}, context_instance=RequestContext(request))

def realiza_cierre():
    '''
    Función Validadora que indica que
    puedo realizar el cierre de inventario
    :return:
    '''
    fecha_actual = datetime.datetime.now().date()
    anio_actual = fecha_actual.year
    fecha_cierre_anual = get_parametros_empresa().anio_cierre
    return anio_actual > fecha_cierre_anual


#############################NUEVAS VISTAS PARA LA CONVERSION DE MEDIDA################################################
from librerias.util import ResponseAngular
import itertools
from operator import itemgetter

@login_required(login_url='/')
@csrf_exempt
def conversion_items(request):
    return render_to_response("Items/conversion_items.html", context_instance=RequestContext(request))


@csrf_exempt
def get_datos_conversion(request):
    bodegas = Bodega.objects.filter(status=1)
    secuencia = 0
    fecha = ""
    try:
        tipo = TipoComprobante.objects.get(status=1,codigo="CIN")
        secuenciatipo = SecuenciaTipoComprobante.objects.get(status=1,tipo_comprobante__id=int(tipo.id))
        secuencia = secuenciatipo.secuencia
        fecha = secuenciatipo.fecha_actual
    except:
        tipo = TipoComprobante.objects.get(status=1,codigo="CIN")
        st = SecuenciaTipoComprobante()
        st.tipo_comprobante_id= int(tipo.id)
        st.secuencia = 1
        st.fecha_actual = datetime.datetime.now()
        st.status = 1
        st.fecha_creacion = datetime.datetime.now()
        st.fecha_actualizacion = datetime.datetime.now()
        st.usuario_creacion = request.user.username
        st.usuario_actualizacion = request.user.username
        st.save()
        secuencia = st.secuencia
        fecha = st.fecha_actual

    if len(bodegas) == 0:
        data = dict(
            success=False,
            bodegas=None,
        )
    else:

        bodega_vacia = dict(id='',label='Seleccione la bodega')
        bodegas_arreglo = []
        bodegas_arreglo.append(bodega_vacia)

        data = dict(
            success = True,
            bodegas = bodegas_arreglo + map(lambda i: dict(id=i.id,
                                     label=unicode(i.descripcion)
                                     ), bodegas),
            fecha = str(fecha),
            secuencia = str(secuencia)
        )

    return HttpResponse(json.dumps(data), content_type="application/json")


@csrf_exempt
def buscar_item_conversion(request):
    q = request.GET.get("q","")

    items = Item.objects.filter(Q(status=1), Q(codigo__icontains=q)| Q(nombre__icontains=q))[0:10]
    if len(items) == 0:
        data = dict(
            success=False,
            items=None,
        )
    else:
        data = dict(
            success=True,
            items=map(lambda i: dict(id=i.id,
                                     label=unicode(i.codigo) + " - " + unicode(i.nombre),
                                     codigo=unicode(i.codigo),
                                     unidad=i.unidad.descripcion,
                                     unidad_id=i.unidad.id,
                                     tipo_unidad=i.unidad.tipo_unidad.descripcion,
                                     costo_item=i.costo
                                     ), items),
        )

    return HttpResponse(json.dumps(data), content_type="application/json")



@csrf_exempt
def get_factor_conversion(request):
    response = ResponseAngular(request.body)
    id_unidad_d = int(response.get("unidad_desde"))
    id_unidad_h = int(response.get("unidad_hasta"))

    resultado = FactorConversion.objects.filter(status=1, unidad_de_id=id_unidad_d, unidad_a_id=id_unidad_h)

    if len(resultado) > 0:
        data = dict(success=True, factor_conversion=resultado[0].factor)
    else:
        data = dict(success=False)
    return HttpResponse(json.dumps(data), content_type="application/json")


@csrf_exempt
def validar_cabecera(request):

    response = ResponseAngular(request.body)
    fecha_tmp = response.get("fecha")
    respuesta = []

    if fecha_tmp == "":
        respuesta.append({"id": 0, "status": 2, "descripcion": u"Por favor seleccione la fecha para continuar."})
    else:
        fecha_v = datetime.datetime.strptime(str(fecha_tmp), "%Y-%m-%d")
        fecha_actual = datetime.datetime.now()

        if len(fecha_tmp) == 10 and fecha_v <= fecha_actual:
            tipo = TipoComprobante.objects.get(status=1, codigo="CIN")
            secuenciatipo = SecuenciaTipoComprobante.objects.get(status=1, tipo_comprobante__id=int(tipo.id))
            secuencia = secuenciatipo.secuencia
            fecha = secuenciatipo.fecha_actual.strftime("%Y-%m-%d")

            if fecha_v >= datetime.datetime.strptime(fecha, "%Y-%m-%d"):
                respuesta.append({"id": 0, "status": 1})
            else:
                respuesta.append({"id": 0, "status": 2,
                                  "descripcion": u"Por favor seleccione una fecha mayor o igual al último comprobante "+str(fecha)})
        else:
            respuesta.append({"id": 0, "status": 2,
                              "descripcion": u"Por favor seleccione una fecha menor o igual a la fecha de hoy."})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, content_type="application/json; charset=utf-8")


def total_stock_formulario(items, id_item, indice_actual):
    total_stock = 0
    disponible = 0
    for idx, i in enumerate(items):
        if i['inicio_id'] == id_item:
            total_stock += float(i['inicio_cantidad'])
            if idx != indice_actual:
                disponible += float(i['inicio_cantidad'])
    return total_stock, disponible

@csrf_exempt
def actualizar_stock_item(request):
    response = ResponseAngular(request.body)
    id_bodega = int(response.get("id_bodega"))
    id_item = response.get("id_item")
    items = response.get("items")
    indice_actual = response.get("indice_actual")
    stock_ingresado, stock_disponible = total_stock_formulario(items, id_item, indice_actual)
    print str(stock_ingresado), str(stock_disponible)
    #try:
    ib = Item_Bodega.objects.get(status=1, item__id=int(id_item), bodega_id=id_bodega)
    if stock_ingresado <= ib.cantidad_actual:
        data = dict(success=True, cantidad_actual_bodega=ib.cantidad_actual if ib.cantidad_actual >= 0 else 0)
    else:
        disponible = ib.cantidad_actual - stock_disponible if ib.cantidad_actual - stock_disponible > 0 else 0
        print "disponible  -->"+str(disponible)
        data = dict(success=False, codigo=1, cantidad_actual=disponible, mensaje=u"Ha superado el stock disponible del item. Stock disponible: "+str(disponible))
    #except:
    #    data = dict(success=False, codigo=2, mensaje=u"El item no pertenece a la bodega seleccionada.")
    return HttpResponse(json.dumps(data), content_type="application/json")


@csrf_exempt
def validar_detalle(request):
    response = ResponseAngular(request.body)
    id_bodega = int(response.get("id_bodega"))
    items = response.get("items")
    # VALIDACION DETALLE
    lista_mensajes = []
    items.sort(key=itemgetter("inicio_id"))
    for key, group in itertools.groupby(items, lambda item: item["inicio_id"]):
        if key is not None:
            #print key, sum([float(item["inicio_cantidad"]) for item in group])
            try:
                ib = Item_Bodega.objects.get(status=1, item__id=int(key), bodega_id=id_bodega)
                item = Item.objects.get(status=1, id=int(key))
            except:
                lista_mensajes.append(dict(indice=-1,
                                           cantidad_actual=0,
                                           mensaje=u"El item " + item.nombre + u" no pertenece a la bodega seleccionada."))

            if sum([float(item["inicio_cantidad"]) for item in group]) > ib.cantidad_actual:
                lista_mensajes.append(dict(indice=-1,
                                           cantidad_actual=0,
                                           mensaje=u"Ha superado el stock del item " + item.nombre+ u". Cantidad Actual: " + str(
                                               ib.cantidad_actual)))
    # VALIDACION DETALLE
    # lista_mensajes = []
    # items_validos = 0
    # for idx, fila in enumerate(items):
    #     if not fila['disabled'] and (fila['factor_conversion']is not None or fila['factor_conversion'] != "") and \
    #             (fila['fin_cantidad'] is not None or fila['fin_cantidad'] != ""):
    #         items_validos += 1
    #
    #         try:
    #             ib = Item_Bodega.objects.get(status=1, item__id=int(fila['inicio_id']), bodega_id=id_bodega)
    #         except:
    #             lista_mensajes.append(dict(indice=idx,
    #                                        cantidad_actual=0,
    #                                        mensaje=u"El item " + fila['inicio_descripcion'] + u" no pertenece a la bodega seleccionada."))
    #         if ib.cantidad_actual < 1:
    #             lista_mensajes.append(dict(indice=idx,
    #                                        cantidad_actual=ib.cantidad_actual,
    #                                        mensaje=u"No hay stock del item " + fila['inicio_descripcion'] +
    #                                                u". Cantidad Actual: " + str(ib.cantidad_actual)))
    #         if int(fila['cantidad']) > ib.cantidad_actual:
    #             lista_mensajes.append(dict(indice=idx,
    #                                        cantidad_actual=ib.cantidad_actual,
    #                                        mensaje=u"Ha superado el stock del item " + fila[
    #                                            'inicio_descripcion'] + u". Cantidad Actual: " + str(
    #                                            ib.cantidad_actual)))
    #
    # if items_validos == 0:
    #     lista_mensajes.append(dict(indice=-1,
    #                                mensaje=u"El detalle debe contener por lo menos un item para proceder a grabar la venta."))
    #     data = dict(
    #         success=False,
    #         lista_mensajes=lista_mensajes
    #     )
    #     return HttpResponse(json.dumps(data), content_type="application/json")
    #
    # if len(lista_mensajes) != 0:
    #     data = dict(
    #         success=False,
    #         lista_mensajes=lista_mensajes
    #     )
    #     return HttpResponse(json.dumps(data), content_type="application/json")
    data = dict(
            success=True,

        )
    return HttpResponse(json.dumps(data), content_type="application/json")

def get_format_serie(num_secuencia):
    f_n = str(num_secuencia)
    for i in range(6-len(f_n)):
        f_n = "0" + f_n
    return f_n


def get_num_comprobante(nueva_Secuencia):
    return "CIN" + get_format_serie(nueva_Secuencia)


def get_fecha_orden_comp(fecha):
    fecha_emi = fecha.split("-")
    time_now = datetime.datetime.now()
    fecha_emision_documento = fecha_emi[0] + "-" + fecha_emi[1] + "-" + fecha_emi[2] + "T" + str(time_now.hour) + ":" + \
                              str(time_now.minute) + ":" + str(time_now.second) + "-05:00"

    return fecha_emision_documento


@csrf_exempt
def grabar_conversion(request):
    response = ResponseAngular(request.body)
    cabecera = response.get("cabecera")
    items = response.get("items")
    lista_mensajes = []
    tipo = TipoComprobante.objects.get(status=1, codigo="CIN")
    secuenciatipo = SecuenciaTipoComprobante.objects.get(status=1, tipo_comprobante__id=int(tipo.id))
    nueva_secuencia = secuenciatipo.secuencia + 1
    fecha = secuenciatipo.fecha_actual.strftime("%Y-%m-%d")
    fecha_formulario = cabecera['fecha']
    fecha_cabecera = datetime.datetime.strptime(str(cabecera['fecha']), "%Y-%m-%d")

    if fecha_cabecera < datetime.datetime.strptime(fecha, "%Y-%m-%d"):
        data = dict(success=False, codigo=1, mensaje=u"La fecha del último comprobante es mayor a la fecha seleccionada.")
        return HttpResponse(json.dumps(data), content_type="application/json")

    #INVENTARIO CABECERA
    inv_cabecera_0 = Inventario_Cabecera()
    inv_cabecera_0.num_comp = get_num_comprobante(nueva_secuencia)
    inv_cabecera_0.fecha_reg = fecha_formulario
    inv_cabecera_0.tipo_id = 2
    inv_cabecera_0.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=13)
    inv_cabecera_0.detalle = u"CONVERSIÓN DE MEDIDA"
    inv_cabecera_0.orden_comp = get_fecha_orden_comp(str(cabecera['fecha']))
    inv_cabecera_0.tipo_comprobante = TipoComprobante.objects.get(id=13)
    inv_cabecera_0.status = 1
    inv_cabecera_0.usuario_creacion = request.user.username
    inv_cabecera_0.save()


    inv_cabecera_1 = Inventario_Cabecera()
    inv_cabecera_1.num_comp = get_num_comprobante(nueva_secuencia)
    inv_cabecera_1.fecha_reg = fecha_formulario
    inv_cabecera_1.tipo_id = 1
    inv_cabecera_1.tipo_mov_inventario = Tipo_Mov_Inventario.objects.get(id=14)
    inv_cabecera_1.detalle = u"CONVERSIÓN DE MEDIDA"
    inv_cabecera_1.orden_comp = get_fecha_orden_comp(str(cabecera['fecha']))
    inv_cabecera_1.tipo_comprobante = TipoComprobante.objects.get(id=13)
    inv_cabecera_1.status = 1
    inv_cabecera_1.usuario_creacion = request.user.username
    inv_cabecera_1.save()

    #SECUENCIA TIPO COMPROBANTE
    #SE ACTUALIZA EL NUMERO DE COMPROBANTE Y LA FECHA ACTUAL
    secuenciatipo.fecha_actual = fecha_formulario
    secuenciatipo.secuencia = nueva_secuencia
    secuenciatipo.save()

    #DETALLE
    for idx, item in enumerate(items):
        ib = Item_Bodega.objects.get(status=1, item__id=int(item['inicio_id']), bodega_id=int(cabecera['bodega']))
        cantidad_actual = ib.cantidad_actual
        if cantidad_actual >= float(item['inicio_cantidad']):
            obj_item = Item.objects.get(id=int(item['inicio_id']))
            inv_detalle_0 = Inventario_Detalle()
            inv_detalle_0.inventario_cabecera = inv_cabecera_0
            inv_detalle_0.bodega = Bodega.objects.get(id=int(cabecera['bodega']))
            inv_detalle_0.item = obj_item
            inv_detalle_0.cantidad = float(item['inicio_cantidad'])
            inv_detalle_0.costo = float(obj_item.costo)
            inv_detalle_0.status = 1
            inv_detalle_0.usuario_creacion = request.user.username
            inv_detalle_0.save()

            obj_item_ingreso = Item.objects.get(id=int(item['fin_id']))
            inv_detalle_1 = Inventario_Detalle()
            inv_detalle_1.inventario_cabecera = inv_cabecera_1
            inv_detalle_1.bodega = Bodega.objects.get(id=int(cabecera['bodega']))
            inv_detalle_1.item = obj_item_ingreso
            inv_detalle_1.cantidad = float(item['fin_cantidad'])
            inv_detalle_1.costo = float(obj_item.costo) * float(item['factor_conversion'])
            inv_detalle_1.status = 1
            inv_detalle_1.usuario_creacion = request.user.username
            inv_detalle_1.save()

            ib.cantidad_actual = ib.cantidad_actual - float(item['inicio_cantidad'])
            ib.save()

            costo_actual_ingreso = 0
            try:
                ib_ingreso = Item_Bodega.objects.get(status=1, item__id=int(item['fin_id']), bodega_id=int(cabecera['bodega']))
                costo_actual_ingreso = ((ib_ingreso.cantidad_actual * obj_item_ingreso.costo) + (float(item['fin_Cantidad']) * inv_detalle_1.costo)) / \
                                       (ib_ingreso.cantidad_actual + float(item['fin_cantidad']))

                ib_ingreso.cantidad_actual = ib_ingreso.cantidad_actual + float(item['fin_cantidad'])
                ib_ingreso.save()
            except:
                ib_nuevo = Item_Bodega()
                ib_nuevo.cantidad_actual = float(item['fin_cantidad'])
                ib_nuevo.item = obj_item_ingreso
                ib_nuevo.bodega = Bodega.objects.get(id=int(cabecera['bodega']))
                ib_nuevo.status = 1
                ib_nuevo.usuario_creacion = request.user.username
                ib_nuevo.save()
                costo_actual_ingreso = inv_detalle_1.costo

            obj_item_ingreso.costo = costo_actual_ingreso
            obj_item_ingreso.save()

        else:
            lista_mensajes.append(dict(indice=idx,
                                       mensaje=u"Ha superado el stock del item " + item['descripcion'] + u". Cantidad Actual: " + str(cantidad_actual)))
    if len(lista_mensajes) > 0:
        lista_mensajes.append(dict(indice=-1,
                                   mensaje=u"El detalle debe contener por lo menos un item para proceder a grabar la venta."))

        data = dict(
            success=False,
            lista_mensajes=lista_mensajes
        )
        return HttpResponse(json.dumps(data), content_type="application/json")

    data = dict(success=True, mensaje=u"Se ha realizado la conversión exitosamente")
    return HttpResponse(json.dumps(data), content_type="application/json")
