#! /usr/bin/python
# -*- coding: UTF-8-*-

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.core.paginator import *
from contabilidad.models import *
import datetime
from django.forms.util import ErrorList
from contabilidad.formularios.TarjetaForm import *

@login_required(login_url='/')
def lista_tarjetas(request):
    q = request.GET.get("q", "")
    tarjetas = Tarjeta_Credito.objects.filter(Q(descripcion__icontains=q) | Q(numero_tarjeta=q), Q(status=1) | Q(status=2)).order_by("descripcion")
    paginacion = Paginator(tarjetas, 10)
    numero_pagina = request.GET.get("page")

    try:
        tarjetas = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        tarjetas = paginacion.page(1)
    except EmptyPage:
        tarjetas = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = tarjetas.number

    return render_to_response('banco/lista_banco.html', {"objetos": tarjetas, "total_paginas": total_paginas, "numero": numero, "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
def detalle_tarjeta(request, id, numero):
    try:
        tarjeta = Tarjeta_Credito.objects.get(id=id)
    except:
        messages.error(request, u"Error recurso no encontrado")
        return HttpResponseRedirect(reverse("lista_tarjetas"))
    return render_to_response('banco/detalle_banco.html', {"objeto": tarjeta, "id": id,"numero": numero}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def transaccion_tarjeta(request):

    return render_to_response()

@login_required(login_url='/')
@csrf_exempt
def registrar_tarjeta(request):
    today = datetime.datetime.now()
    secuencia = 1
    formulario = TarjetaForm()

    if request.method == "POST":
        formulario = TarjetaForm(request.POST)
        if formulario.is_valid():
            tarjeta_credito = Tarjeta_Credito()
            plan_cuenta = PlanCuenta()
            tarjeta_credito.descripcion = formulario.getDescripcion()
            tarjeta_credito.numero_tarjeta = formulario.getNumeroTarjeta()
            tarjeta_credito.tipo = formulario.getTipo()
            tarjeta_credito.fecha_ultimo_cierre = formulario.getFechaUltimoCierre()
            tarjeta_credito.fecha_ultimo_conciliacion = formulario.getFechaUltimoConciliacion()

            tarjeta_credito.fecha_creacion = today
            tarjeta_credito.usuario_creacion = request.user.username

            padre_cuenta = PlanCuenta.objects.get(tipo_id=1, nivel=4)
            plan_cuenta.clase_cuenta = "M"

            plan_cuenta.tipo = padre_cuenta.tipo
            plan_cuenta.descripcion = formulario.getDescripcion()
            plan_cuenta.naturaleza = formulario.getNaturaleza()
            plan_cuenta.nivel = padre_cuenta.nivel+1
            plan_cuenta.cod_relacion_super_cia = formulario.getCodigoSuperCia()
            plan_cuenta.fecha_creacion = today
            plan_cuenta.status = 1
            plan_cuenta.id_cta_grupo = padre_cuenta
            plan_cuenta.codigo = plan_cuenta.ObtenerCodigo()

            banco_obj.codigo = plan_cuenta.codigo
            plan_cuenta.save()

            banco_obj.plan_cuenta_id = plan_cuenta.id
            banco_obj.save()

            messages.success(request, "El Banco se  registró exitosamente")
            return HttpResponseRedirect(reverse("lista_bancos"))
    return render_to_response("banco/registrar_banco.html",
                              {"formulario": formulario, "request": request},
                                context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def guardar_banco_cuenta(request):
    today = datetime.datetime.now()
    secuencia = 1
    es_cuenta = True
    a = str(request.GET.get("descripcion", ""))
    id_cuenta = request.GET.get("id", "")
    descripcion_cuenta = a.replace("-", " ")
    naturaleza_cuenta = request.GET.get("naturaleza", "")
    codigo_cuenta_super_cia = request.GET.get("cod_super_cia", "")

    formulario = TarjetaForm(initial={"descripcion": descripcion_cuenta,
                                    "naturaleza": naturaleza_cuenta, "cod_relacion_super_cia": codigo_cuenta_super_cia})
    if request.method == "POST":
        formulario = TarjetaForm(request.POST)
        plan_cuenta = PlanCuenta()
        if formulario.is_valid():
            banco_obj = Banco()
            banco_obj.descripcion = formulario.getDescripcion()
            banco_obj.numero_cuenta = formulario.getNumeroCuenta()
            banco_obj.tipo = formulario.getTipo()
            banco_obj.fecha_ultimo_cierre = formulario.getFechaUltimoCierre()
            banco_obj.fecha_ultimo_conciliacion = formulario.getFechaUltimoConciliacion()
            banco_obj.fecha_creacion = today
            banco_obj.usuario_creacion = request.user.username

            #try:
            objetos = Banco.objects.all()
            for obj in objetos:
                if len(objetos) != 0 and obj.status != 0:
                    secuencia = secuencia+1
                if obj.secuencia_cheque == secuencia:
                    secuencia = secuencia+1

            banco_obj.secuencia_cheque = secuencia
            padre_cuenta = PlanCuenta.objects.get(id=int(id_cuenta))
            plan_cuenta.id_cta_grupo = padre_cuenta

            if padre_cuenta.nivel != 5:
                plan_cuenta.clase_cuenta = "G"
            else:
                plan_cuenta.clase_cuenta = "M"

            plan_cuenta.tipo = padre_cuenta.tipo

            plan_cuenta.descripcion = formulario.getDescripcion()
            plan_cuenta.naturaleza = formulario.getNaturaleza()
            plan_cuenta.cod_relacion_super_cia = codigo_cuenta_super_cia
            plan_cuenta.nivel = padre_cuenta.nivel+1
            plan_cuenta.usuario_creacion = request.user.username
            plan_cuenta.usuario_actualizacion = request.user.username
            plan_cuenta.fecha_actualizacion = today
            plan_cuenta.fecha_creacion = today
            plan_cuenta.status = 1
            plan_cuenta.codigo = plan_cuenta.ObtenerCodigo()

            banco_obj.codigo = plan_cuenta.codigo
            plan_cuenta.save()

            banco_obj.plan_cuenta_id = plan_cuenta.id
            banco_obj.save()
            messages.success(request, "El Banco se  registró exitosamente")
            return HttpResponseRedirect(reverse("lista_plan_cuenta"))
            #except:
            #    messages.error("Existen problemas de conectividad, por favor intentelo nuevamente.")
            #    return HttpResponseRedirect(reverse("lista_plan_cuenta"))

    return render_to_response("banco/registrar_banco.html", {"formulario": formulario, "bandera": es_cuenta, "id": id_cuenta, "descripcion": descripcion_cuenta,
    "naturaleza": naturaleza_cuenta, "codgio_super_cia": codigo_cuenta_super_cia, "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def modificar_banco_cuenta(request, codigo_cuenta, descripcion):
    now_ac = datetime.datetime.now()
    es_cuenta = True

    try:
        nuevo = Banco.objects.get(codigo=codigo_cuenta)
        plan_cuenta = PlanCuenta.objects.get(codigo=nuevo.codigo)
        nuevo.descripcion = descripcion.replace("-", " ")
    except:
        messages.error(request, "El Banco no existe. ")
        return HttpResponseRedirect(reverse("lista_bancos"))

    formulario = EditFormBancoDiff(initial={
        "descripcion": nuevo.descripcion,
        "numero_cuenta": nuevo.numero_cuenta,
        "tipo": nuevo.tipo,
        "fecha_ultimo_conciliacion": nuevo.fecha_ultimo_conciliacion.strftime("%Y-%m-%d"),
        "fecha_ultimo_cierre": nuevo.fecha_ultimo_cierre.strftime("%Y-%m-%d"),

    })

    if request.method == "POST":
        formulario = EditFormBancoDiff(request.POST)
        if formulario.is_valid():
            nuevo.descripcion = formulario.getDescripcion()
            nuevo.numero_cuenta = formulario.getNumeroCuenta()
            nuevo.fecha_ultimo_conciliacion = formulario.getFechaUltimoConciliacion()
            nuevo.fecha_ultimo_cierre = formulario.getFechaUltimoCierre()
            nuevo.tipo = formulario.getTipo()

            objetos = Banco.objects.all()
            bandera = False

            for obj in objetos:
                if nuevo.id is not obj.id:
                    if nuevo.numero_cuenta == obj.numero_cuenta and (nuevo.status == 1 or nuevo.status == 2):
                        errors = formulario._errors.setdefault("numero_cuenta", ErrorList())
                        errors.append(u"Nº de Cuenta ya ha sido ingresado")
                        bandera = True

            if bandera == True:
                return render_to_response("banco/modificar_banco.html",{"formulario": formulario, "obj": nuevo, "bandera": es_cuenta, "codigo_cuenta":codigo_cuenta, "descripcion": descripcion}, context_instance=RequestContext(request))

            else:
                nuevo.descripcion = formulario.getDescripcion()
                nuevo.numero_cuenta = formulario.getNumeroCuenta()
                nuevo.tipo = formulario.getTipo()
                nuevo.fecha_ultimo_conciliacion = formulario.getFechaUltimoConciliacion()
                nuevo.fecha_ultimo_cierre = formulario.getFechaUltimoCierre()
                nuevo.fecha_actualizacion = now_ac
                nuevo.usuario_actualizacion = request.user.username

                plan_cuenta.descripcion = nuevo.descripcion

                try:
                    nuevo.save()
                    plan_cuenta.save()
                    messages.success(request, "La información del Banco ha sido  modificada exitosamente")

                    if es_cuenta:
                        return HttpResponseRedirect(reverse("lista_plan_cuenta"))
                    else:
                        return HttpResponseRedirect(reverse("lista_bancos"))
                except:
                    messages.error(request, "Existen problemas de conectividad, por favor intentelo nuevamente.")
                    return HttpResponseRedirect(reverse("lista_bancos"))
    return render_to_response("banco/modificar_banco.html",
        {"formulario": formulario, "obj":nuevo, "bandera": es_cuenta, "codigo_cuenta": codigo_cuenta, "descripcion": descripcion, "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def modificar_banco(request, id, numero):
    now_ac = datetime.datetime.now()
    try:
        nuevo = Banco.objects.get(id=id)
        plan_cuenta = PlanCuenta.objects.get(id=nuevo.plan_cuenta_id)
    except:
        messages.error(request, "El Banco no se encuentra registrado en la base. ")
        return HttpResponseRedirect(reverse("lista_bancos"))

    formulario = EditFormBanco(initial={
        "descripcion": nuevo.descripcion,
        "numero_cuenta": nuevo.numero_cuenta,
        "tipo": nuevo.tipo,
        "naturaleza": plan_cuenta.naturaleza,
        "cod_relacion_super_cia": plan_cuenta.cod_relacion_super_cia

    })

    if request.method == "POST":
        formulario = EditFormBanco(request.POST)
        if formulario.is_valid():
            nuevo.descripcion = formulario.getDescripcion()
            nuevo.numero_cuenta = formulario.getNumeroCuenta()
            nuevo.tipo = formulario.getTipo()

            plan_cuenta.descripcion = nuevo.descripcion
            plan_cuenta.naturaleza = formulario.getNaturaleza()
            plan_cuenta.cod_relacion_super_cia = formulario.getCodigoSuperCia()
            objetos = Banco.objects.all()
            bandera = False

            for obj in objetos:
                if nuevo.id is not obj.id:
                    if nuevo.numero_cuenta == obj.numero_cuenta and (nuevo.status == 1 or nuevo.status == 2):
                        errors = formulario._errors.setdefault("numero_cuenta", ErrorList())
                        errors.append(u"Nº de Cuenta ya ha sido ingresado")
                        bandera = True

            if bandera == True:
                return render_to_response("banco/modificar_banco.html",{"formulario": formulario, "obj": nuevo, "numero":numero}, context_instance=RequestContext(request))

            else:
                nuevo.descripcion = formulario.getDescripcion()
                nuevo.numero_cuenta = formulario.getNumeroCuenta()
                nuevo.tipo = formulario.getTipo()

                plan_cuenta.descripcion = nuevo.descripcion
                plan_cuenta.naturaleza = formulario.getNaturaleza()
                plan_cuenta.cod_relacion_super_cia = formulario.getCodigoSuperCia()

                nuevo.fecha_actualizacion = now_ac
                nuevo.usuario_actualizacion = request.user.username

                try:
                    nuevo.save()
                    plan_cuenta.save()
                    messages.success(request, "La información del Banco ha sido  modificada exitosamente")
                    return HttpResponseRedirect(reverse("lista_bancos"))

                except:

                    messages.error("Existen problemas de conectividad, por favor intentelo nuevamente.")
                    return HttpResponseRedirect(reverse("lista_bancos"))

    return render_to_response("banco/modificar_banco.html",
        {"formulario": formulario, "obj": nuevo, "numero": numero, "request": request}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def eliminar_banco(request, id):
    banco = Banco.objects.get(id=id)
    now_ac = datetime.datetime.now()

    if banco.status != 0:
        banco.status = 0
        banco.usuario_actualizacion = request.user.username
        banco.fecha_actualizacion = now_ac

    banco.save()
    messages.success(request,"El Banco ha sido eliminado exitosamente")
    return HttpResponseRedirect(reverse("lista_bancos"))

@login_required(login_url='/')
@csrf_exempt
def activar_banco(request, id):
    try:
        banco = Banco.objects.get(id=id)
        now_ac = datetime.datetime.now()

        if banco.status == 0 or banco.status == 2:
            banco.status = 1
            banco.usuario_actualizacion = request.user.username
            banco.fecha_actualizacion = now_ac

        banco.save()
        messages.success(request, "El Banco fue activado")
        return HttpResponseRedirect(reverse("lista_bancos"))
    except:
        messages.error(request, "No encontrado")

    return HttpResponseRedirect(reverse("lista_bancos"))

@login_required(login_url='/')
@csrf_exempt
def inactivar_banco(request, id):
        banco = Banco.objects.get(id=id)
        now_ac = datetime.datetime.now()
        if banco.status == 1:
            banco.status = 2
            banco.usuario_actualizacion = request.user.username
            banco.fecha_actualizacion = now_ac
        banco.save()
        messages.success(request, "El Banco fue inactivado")
        return HttpResponseRedirect(reverse("lista_bancos"))

@login_required(login_url='/')
def cierre_banco_mes(request, id):

    banco = Banco.objects.get(id=id)
    plan_cuenta = PlanCuenta.objects.get(id=banco.plan_cuenta_id)
    saldo = Saldo_Banco.objects.get(plan_cuenta_id=plan_cuenta.id)


    print "RElacion", saldo.anio
    print "Plan Cuenta:", plan_cuenta.id
    print "Plan Cuenta Codigo:", plan_cuenta.codigo
    print "Banco codigo:", banco.codigo
    print "Saldo BAnco:", saldo.id

    anio = banco.fecha_ultimo_cierre.strftime("%Y")
    mes = banco.fecha_ultimo_cierre.strftime("%m")
    saldo.fecha_ultimo_cierre = banco.fecha_ultimo_cierre
    saldo.anio = anio
    saldo.mes = mes
    saldo.save()


    d = banco.fecha_ultimo_cierre + datetime.timedelta(days=1)
    f = banco.fecha_ultimo_cierre + datetime.timedelta(days=30)

    #fecha_inicio = d.strftime("%Y-%m-%d")
    #fecha_fin = f.strftime("%Y-%m-%d")

    return render_to_response('banco/cierrre_banco_mes.html', {"objetos": banco, "fecha_inicio": d, "fecha_fin": f, "saldo": saldo, "request": request}, context_instance=RequestContext(request))

