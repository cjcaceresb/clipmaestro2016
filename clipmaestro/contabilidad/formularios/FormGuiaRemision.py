#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *
from datetime import date, timedelta
from django.forms.formsets import BaseFormSet

__author__ = 'Roberto'

class FormGuiaRemision(forms.Form):
    serie_guia = forms.ChoiceField(choices=[], label="Serie")
    #num = forms.CharField(max_length=125, label="#", required=False)
    fecha_inicio = forms.DateField(label=u'F. Inicio', widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_final = forms.DateField(label=u'F. Final ', widget=forms.DateInput(format=("%Y-%m-%d")))
    ####################################### Información Transportista #######################################
    compania = forms.CharField(max_length=220, label="Compañía")
    ruc_remision = forms.CharField(max_length=100, label="RUC")
    ciudad = forms.ChoiceField(choices=[], label="Ciudad")
    direccion_guia = forms.ChoiceField(choices=[], label="Dirección")
    direccion_conductor = forms.CharField(label="Dirección")

    def getSerie(self):
        return self.data["serie_guia"]
    def getFechaIni(self):
        return self.cleaned_data["fecha_inicio"]
    def getFechaFin(self):
        return self.cleaned_data["fecha_final"]
    def getCompania(self):
        return self.data["compania"]
    def getCiudad(self):
        return self.data["ciudad"]
    def getDireccion(self):
        return self.data["direccion_guia"]
    def getDireccionConductor(self):
        return self.data["direccion_conductor"]
    def getRuc(self):
        return self.data["ruc_remision"]

    def __init__(self, *args, **kwargs):
        super(FormGuiaRemision, self).__init__(*args, **kwargs)
        self.fields['serie_guia'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=datetime.datetime.now(), fecha_vencimiento__gte=datetime.datetime.now()).filter(documento_sri=25).order_by("serie")]
        self.fields['serie_guia'].widget.attrs['class'] = "selectpicker show-tick serie_guia_rem"
        self.fields['serie_guia'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['serie_guia'].widget.attrs['data-width'] = "100%"

        self.fields['fecha_inicio'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_inicio'].widget.attrs['readonly'] = "readonly"
        self.fields['fecha_inicio'].widget.attrs['class'] = "control-fecha"
        self.fields['fecha_inicio'].widget.attrs['placeholder'] = "aaaa-mm-dd"

        self.fields['fecha_final'].widget.attrs['readonly'] = "readonly"
        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['class'] = "control-fecha"
        self.fields['fecha_final'].widget.attrs['placeholder'] = "aaaa-mm-dd"

        self.fields['ciudad'].choices = [("", "")]+[(x.id, x.descripcion) for x in Ciudad.objects.filter(status=1).order_by("descripcion")]
        self.fields['ciudad'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['ciudad'].widget.attrs['data-style'] = "slc-primary"
        self.fields['ciudad'].widget.attrs['data-size'] = "auto"
        self.fields['ciudad'].widget.attrs['data-live-search'] = True

        self.fields['ruc_remision'].widget.attrs['maxlength'] = "13"
        self.fields['ruc_remision'].widget.attrs['class'] = "numerico2"

        self.fields['direccion_guia'].choices = [("", "")]+[(x.id, x.descripcion+u" - "+x.direccion1) for x in Cliente_Direccion.objects.filter(status=1, descripcion__isnull=False, direccion1__isnull=False)]
        self.fields['direccion_guia'].widget.attrs['class'] = "selectpicker show-tick direccion_guia"
        self.fields['direccion_guia'].widget.attrs['data-style'] = "slc-primary"
        self.fields['direccion_guia'].widget.attrs['style'] = "margin-top: 5px;"
        self.fields['direccion_guia'].widget.attrs['data-size'] = "auto"
        self.fields['direccion_guia'].widget.attrs['data-live-search'] = True

        self.fields['direccion_conductor'].widget.attrs['maxlength'] = "100"

class FormGuiaRemisionCabecera(forms.Form):
    serie = forms.ChoiceField(choices=[], label="Serie")
    num = forms.CharField(max_length=125, label="#")
    fecha_inicio = forms.DateTimeField(label=u'F. Inicio')
    fecha_final = forms.DateTimeField(label=u'F. Final ')
    fecha_emision = forms.DateTimeField(label="Fecha de Emisión", required=False)
    cliente = forms.ChoiceField(label="Cliente", required=False)
    motivo = forms.ChoiceField(label="Motivo", required=False)
    ####################################### Información Transportista #######################################
    compania = forms.CharField(max_length=220, label="Compañía")
    ruc_remision = forms.CharField(max_length=100, label="RUC")
    ciudad = forms.ChoiceField(choices=[], label="Ciudad")
    direccion_guia = forms.ChoiceField(choices=[], label="Dirección")
    direccion_conductor = forms.CharField(label="Dirección")


    def __init__(self, *args, **kwargs):
        super(FormGuiaRemisionCabecera, self).__init__(*args, **kwargs)
        self.fields['serie'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=datetime.datetime.now(), fecha_vencimiento__gte=datetime.datetime.now()).filter(documento_sri=25).order_by("serie")]
        self.fields['serie'].widget.attrs['class'] = "selectpicker show-tick serie_guia_rem"
        self.fields['serie'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['serie'].widget.attrs['data-width'] = "100%"

        self.fields['cliente'].widget.attrs['placeholder'] = "Búsqueda Cod - CI - Nombre"
        self.fields['cliente'].widget.attrs['class']="selectpicker show-tick"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].choices = [("", "")]+[(x.id, x.ruc+u" - "+x.razon_social) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-size'] = "auto"

class FormGuiaRemisionDetalle(forms.Form):
    cantidad = forms.FloatField()
    item = forms.CharField()
    serie = forms.CharField()

    def __init__(self, *args, **kwargs):
        super(FormGuiaRemisionDetalle, self).__init__(*args, **kwargs)

class FormCabeceraguiaRemVenta(forms.Form):
    fecha_reg = forms.DateField(label=u"Fecha", widget=forms.DateInput(format=("%Y-%m-%d")))
    num_doc = forms.CharField(max_length=100, label=u"# Doc. Venta")
    cliente = forms.CharField(max_length=250, label=u"Cliente")


    def getFechaReg(self):
        return self.cleaned_data["fecha_reg"]
    def getNumDoc(self):
        return self.data["num_doc"]
    def getCliente(self):
       return self.data["cliente"]


    def __init__(self, *args, **kwargs):
        super(FormCabeceraguiaRemVenta, self).__init__(*args, **kwargs)

        self.fields['fecha_reg'].widget.attrs['class'] = "control-fecha calendario-gris"
        self.fields['fecha_reg'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_reg'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['num_doc'].widget.attrs['maxlength'] = "9"
        self.fields['num_doc'].widget.attrs['data-width'] = "100%"

        self.fields['num_doc'].widget.attrs['readonly'] = True
        self.fields['cliente'].widget.attrs['readonly'] = True

class FormDetalleGuiaRemVenta(forms.Form):
    id_detalle_venta = forms.IntegerField(label=u"id_detalle")
    id_item = forms.IntegerField(label=u"id_item")
    item = forms.CharField(label=u"Item")
    cantidad = forms.CharField(max_length=100, label=u"Cant. Venta")
    unidad = forms.CharField(label=u"Unidad")
    cant_entregada1 = forms.CharField(max_length=100, label=u"Cant.Entregada")
    cant_entregada = forms.CharField(max_length=100, label=u"Cant.Entregada")

    def __init__(self, *args, **kwargs):
        super(FormDetalleGuiaRemVenta, self).__init__(*args, **kwargs)

        self.fields['id_item'].widget.attrs['class'] = "id_item"
        self.fields['id_item'].widget.attrs['style'] = "width: 100%"

        self.fields['item'].widget.attrs['readonly'] = True
        self.fields['cantidad'].widget.attrs['readonly'] = True
        self.fields['cant_entregada1'].widget.attrs['readonly'] = True
        self.fields['unidad'].widget.attrs['readonly'] = True


        self.fields['item'].choices = [(x.id, unicode(x.codigo[0:15])+ "-"+unicode(x.nombre[0:15]))
                                                      for x in Item.objects.filter(status=1).order_by("nombre")]

        self.fields['item'].widget.attrs['style'] = "width: 100%"
        self.fields['item'].widget.attrs['class'] = "item selectpicker show-tick item"
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"

        self.fields['cant_entregada'].widget.attrs['class'] = "numerico cant_entregada"
        self.fields['cantidad'].widget.attrs['class'] = "cantidad"
        self.fields['cant_entregada1'].widget.attrs['class'] = "cant_entregada1"

# Clase que me ayuda a validar los formset vacios
class RequiredFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False


class FormBuscadorGuiaRemVenta(forms.Form):
    selec_estado = (('', ''), ('1', 'Activo'), ('2', 'Anulado'))
    fecha = forms.DateTimeField(label=u'Fecha Inicial: ', widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_final = forms.DateTimeField(label=u'Fecha Final: ', widget=forms.DateInput(format=("%Y-%m-%d")))
    num_comp = forms.CharField(label="# de Comprobante:")
    #tiene_venta = forms.BooleanField(label=u"Venta:")
    ndocumento = forms.CharField(label="# Documento: ", required=True)
    cliente = forms.ChoiceField(choices=[], label="Cliente: ")
    movimiento = forms.ChoiceField(choices=[], label=u"Movimiento:")

    fecha_inicio = forms.DateField(label=u"Fecha de Inicio:", widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_traslado = forms.DateField(label=u"Fecha de Traslado:", widget=forms.DateInput(format=("%Y-%m-%d")))
    ciudad = forms.ChoiceField(label=u"Ciudad:", choices=[])
    compania = forms.CharField(label=u"Compañía")
    item = forms.ChoiceField(label="Item:", choices=[])
    estado = forms.ChoiceField(choices=selec_estado, label="Estado")
    page = forms.IntegerField()

    def getCompania(self):
        try:
            value = self.data['compania']
            return value
        except:
            return ""

    def getFecha(self):
        try:
            value = str(self.data['fecha']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return date.today()-timedelta(days=30)

    def getFechaFinal(self):
        try:
            value = str(self.data['fecha_final']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return datetime.datetime.now()

    def getNumComprobante(self):
        try:
            value = self.data['num_comp']
            return value
        except:
            return ""

    def getCliente(self):
        try:
            value = self.data['cliente']
            return Cliente.objects.get(id=value)
        except:
            return None

    def getCiudad(self):
        try:
            value = self.data['ciudad']
            return Ciudad.objects.get(id=value)
        except:
            return None

    def getItem(self):
        try:
            value = self.data['item']
            return Item.objects.get(id=value)
        except:
            return None

    def getMovimiento(self):
        try:
            value = self.data['movimiento']
            return TipoMovGuiaRemision.objects.get(id=value)
        except:
            return None

    def getNumeroDocumento(self):
        try:
            value = self.data['ndocumento']
            return value
        except:
            return ""
    def getEstado(self):
        try:
            value = self.data['estado']
            return value
        except:
            return ""

        ###########################################################################################

    def __init__(self, *args, **kwargs):
        super(FormBuscadorGuiaRemVenta, self).__init__(*args, **kwargs)

        self.fields['item'].choices = [("", "")] + [(x.id, x.codigo+" - "+x.nombre) for x in Item.objects.filter(status=1, nombre__isnull=False, descripcion__isnull=False).order_by("codigo")]
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['item'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['item'].widget.attrs['data-width'] = "100%"

        self.fields['fecha'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['readonly']="readonly"
        self.fields['fecha'].widget.attrs['class']="control-fecha"

        self.fields['fecha_final'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['readonly']="readonly"
        self.fields['fecha_final'].widget.attrs['class']="control-fecha"

        self.fields['estado'].widget.attrs['class']="selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style']="slc-b-s"
        self.fields['estado'].widget.attrs['data-width']="100%"

        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].choices = [('', '')] + [(x.id, x.razon_social[0:30]) for x in Cliente.objects.exclude(status=0)]

        self.fields['ciudad'].choices = [("", "")]+[(x.id, x.descripcion) for x in Ciudad.objects.filter(status=1)]
        self.fields['ciudad'].widget.attrs['class'] = "selectpicker show-tick ciudad"
        self.fields['ciudad'].widget.attrs['data-style'] = "slc-primary"
        self.fields['ciudad'].widget.attrs['data-size'] = "15"
        self.fields['ciudad'].widget.attrs['data-live-search'] = True
        self.fields['ciudad'].widget.attrs['data-name'] = "ciudad"

        self.fields['movimiento'].choices = [("", "")]+[(x.id, x.descripcion) for x in TipoMovGuiaRemision.objects.filter(status=1)]
        self.fields['movimiento'].widget.attrs['class'] = "selectpicker show-tick ciudad"
        self.fields['movimiento'].widget.attrs['data-style'] = "slc-primary"
        self.fields['movimiento'].widget.attrs['data-size'] = "15"
        self.fields['movimiento'].widget.attrs['data-live-search'] = True
        self.fields['movimiento'].widget.attrs['data-name'] = "ciudad"

        self.fields['ndocumento'].widget.attrs['placeholder'] = "001-001-000000"
        self.fields['num_comp'].widget.attrs['placeholder'] = "vta20XX000000"

        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"

