#! /usr/bin/python
# -*- coding: UTF-8 -*-

from django import forms
from  contabilidad.models import *
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *
from datetime import date, timedelta
__author__ = 'Clip Maestro'


class CabeceraPagosForm(forms.Form):
    proveedor = forms.ChoiceField(choices=[], label="Proveedor")
    observaciones = forms.CharField(max_length=255, widget=forms.Textarea, required=False)

    def getProveedor(self):
        return self.data["proveedor"]

    def getObservacion(self):
        return self.data["observaciones"]

    def __init__(self, *args, **kwargs):
        super(CabeceraPagosForm, self).__init__(*args, **kwargs)
        self.fields['observaciones'].widget.attrs['rows'] = 3
        self.fields['observaciones'].widget.attrs['max_length'] = 255
        self.fields['proveedor'].choices = [("", "")] + [(x.id, x.razon_social) for x in Proveedores.objects.filter(status=1).order_by("razon_social")]
        self.fields['proveedor'].widget.attrs['class'] = "selectpicker dropup show-tick"
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un proveedor"
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['data-width'] = "100%"
        self.fields['proveedor'].widget.attrs['data-live-search'] = True


class DetallePago(forms.Form):
    id_cuenta_pagar = forms.IntegerField(label="id_cuenta_pagar")
    check = forms.BooleanField(label="check", required=False)
    tipo_doc = forms.CharField(max_length=100, required=False)
    numero_doc = forms.CharField(required=False)
    fecha_reg = forms.CharField(required=False)
    fecha_ven = forms.CharField(required=False)
    total = forms.FloatField(label="total", required=False)
    saldo = forms.FloatField(label="saldo", required=False)
    referencia = forms.CharField(required=False)
    valor = forms.FloatField(label="valor", required=False)

    def getIdCuentaPagar(self):
        return self.data["id_cuenta_pagar"]
    def getCheck(self):
        return self.data["check"]
    def getValor(self):
        return self.data["valor"]

    def __init__(self, *args, **kwargs):
        super(DetallePago, self).__init__(*args, **kwargs)
        self.fields['check'].widget.attrs['class']="checks"
        self.fields['valor'].widget.attrs['class']="valor numerico"
        self.fields['numero_doc'].widget.attrs['class']="numero_doc"
        self.fields['tipo_doc'].widget.attrs['class']="tipo_doc"
        self.fields['tipo_doc'].widget.attrs['readonly']= True
        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['total'].widget.attrs['class']= "total numerico"
        self.fields['fecha_reg'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['class']= "fecha_vencimiento"
        self.fields['saldo'].widget.attrs['readonly'] = True
        self.fields['numero_doc'].widget.attrs['readonly'] = True
        self.fields['saldo'].widget.attrs['class']= "saldo numerico"
        self.fields['referencia'].widget.attrs['readonly'] = True
        self.fields['referencia'].widget.attrs['class']="referencia"
        self.fields['id_cuenta_pagar'].widget.attrs['class'] = "tipo"


class FormaPagoForm(forms.Form):

    terminos = (('1','Corriente'),('2','Diferido'))
    plazos = (('3','3'),('6','6'),('9','9'),('12','12'),('18','18'),('24','24'),('36','36'))

    forma_pago = forms.ChoiceField(choices=[], label="forma_pago")
    cheque_provisionado = forms.BooleanField(required=False)
    fecha = forms.DateField(label="Fecha", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    bancos = forms.ChoiceField(choices=[], required=False)
    tarjetas = forms.ChoiceField(choices=[], required=False)
    termino = forms.ChoiceField(choices=terminos, required=False)
    caja = forms.ChoiceField(choices=[], required=False)
    plazo = forms.ChoiceField(choices=plazos, required=False)
    interes = forms.FloatField(required=False)
    cheque = forms.CharField(max_length=20, required=False)
    lista_cheques_prov = forms.ChoiceField(choices=[], required=False)
    fecha_cheque = forms.DateField(required=False, widget=forms.DateInput(format=("%Y-%m-%d")))
    referencia = forms.CharField(max_length=50, label="referencia", required=False)

    def clean(self):
        """
            Función para validar el formulario
        """
        fecha = self.cleaned_data.get('fecha', "")
        cheque_provisionado = self.cleaned_data.get('cheque_provisionado', False)
        forma_pago = self.cleaned_data.get('forma_pago', "")
        bancos = self.cleaned_data.get('bancos', "")
        lista_cheques_prov = self.cleaned_data.get('lista_cheques_prov', "")
        fecha_cheque = self.cleaned_data.get('fecha_cheque', "")

        #Validación de la forma de pago
        if forma_pago == "1":  # cheque
            if not cheque_provisionado:
                if fecha == "":
                    self._errors["fecha"] = "Campo obligatorio"

            if cheque_provisionado:
                if lista_cheques_prov == "":
                    self._errors["lista_cheques_prov"] = "Campo obligatorio"
                else:
                    try:
                        Banco.objects.get(id=lista_cheques_prov, status=3)
                    except Banco.DoesNotExist:
                        self._errors["lista_cheques_prov"] = "Campo obligatorio"

            if fecha_cheque == "":
                self._errors["fecha_cheque"] = "Campo obligatorio"
        else:
            if fecha == "":
                self._errors["fecha"] = "Campo obligatorio"

        if forma_pago in ("1", "4"):
            if bancos == "":
                self._errors["bancos"] = "Campo obligatorio"
            else:
                try:
                    Cuenta_Banco.objects.get(id=bancos, status=1)
                except Cuenta_Banco.DoesNotExist:
                    self._errors["bancos"] = "Campo obligatorio"

        return self.cleaned_data

    def getFormaPago(self):
        return self.data["forma_pago"]

    def getBanco(self):
        return self.data["bancos"]

    def getTarjetas(self):
        return self.data["tarjetas"]

    def getTermino(self):
        return self.data["termino"]

    def getPlazo(self):
        return self.data["plazo"]

    def es_provisionado(self):
        try:
            return self.data["cheque_provisionado"]
        except:
            return False

    def get_lista_cheques_prov(self):
        return self.data["lista_cheques_prov"]

    def getInteres(self):
        return self.data["interes"]

    def getCheque(self):
        return self.data["cheque"]

    def get_fecha(self):
        return self.cleaned_data["fecha"]

    def getFechaCheque(self):
        return self.data["fecha_cheque"]

    def getReferencia(self):
        return self.data["referencia"]

    def getCajaChica(self):
        return self.data["caja"]

    def __init__(self, *args, **kwargs):
        super(FormaPagoForm, self).__init__(*args, **kwargs)
        self.fields['forma_pago'].widget.attrs['class']="selectpicker show-tick"
        self.fields['forma_pago'].widget.attrs['data-name']="forma_de_pago"
        self.fields['forma_pago'].widget.attrs['data-style']="slc-primary"
        self.fields['forma_pago'].widget.attrs['data-width']="100%"

        self.fields['forma_pago'].choices = [(x.id, x.descripcion) for x in FormaPago.objects.filter(banco_modulo=True,
                                                                                                     status=1)]

        self.fields['lista_cheques_prov'].choices = [(x.id, "Chq #"+x.num_cheque + " - "+ x.num_comp) for x in Banco.objects.filter(status=3)]
        self.fields['lista_cheques_prov'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['lista_cheques_prov'].widget.attrs['data-name'] = "forma_de_pago"
        self.fields['lista_cheques_prov'].widget.attrs['data-style'] = "slc-primary"
        self.fields['lista_cheques_prov'].widget.attrs['data-width'] = "100%"
        self.fields['lista_cheques_prov'].widget.attrs['title'] = "Seleccione Chq."

        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "control-fecha input-calendario"

        self.fields['bancos'].widget.attrs['class']="selectpicker show-tick"
        self.fields['bancos'].widget.attrs['data-style']="slc-primary"
        self.fields['bancos'].widget.attrs['data-width']="100%"
        self.fields['bancos'].widget.attrs['title']="Seleccione una Cuenta Bancaria"

        self.fields['cheque_provisionado'].widget.attrs['title'] = "Cheques provisionados"

        self.fields['caja'].widget.attrs['class']="selectpicker show-tick"
        self.fields['caja'].widget.attrs['data-style']="slc-primary"
        self.fields['caja'].widget.attrs['data-width']="100%"
        self.fields['caja'].widget.attrs['title']="Seleccione una Caja Chica"
        self.fields['tarjetas'].widget.attrs['class']="selectpicker show-tick"
        self.fields['tarjetas'].widget.attrs['data-style']="slc-primary"
        self.fields['tarjetas'].widget.attrs['data-width']="100%"
        self.fields['bancos'].choices = [("", "")] + [(x.id, x.descripcion + " " + x.tipo_cuenta_banco.descripcion + " " + x.numero_cuenta) for x in Cuenta_Banco.objects.filter(status=1)]
        self.fields['caja'].choices = [("", "")] + [(x.id, x.descripcion+ " - " + x.codigo) for x in PlanCuenta.objects.filter(status=1, nivel=5, tipo=TipoCuenta.objects.get(alias=12))]
        self.fields['tarjetas'].choices = [("", "")] + [(x.id, x.descripcion + " " + x.numero_tarjeta) for x in Tarjeta_Credito.objects.filter(status=1)]
        self.fields['termino'].widget.attrs['class']="selectpicker show-tick"
        self.fields['termino'].widget.attrs['data-style']="slc-primary"
        self.fields['termino'].widget.attrs['data-width']="100%"
        self.fields['plazo'].widget.attrs['class']="selectpicker show-tick numerico"
        self.fields['plazo'].widget.attrs['data-style']="slc-primary"
        self.fields['plazo'].widget.attrs['data-width']="100%"
        self.fields['interes'].widget.attrs['class']="numerico"
        self.fields['cheque'].widget.attrs['class']="numero_cheque"
        self.fields['cheque'].widget.attrs['style']="background: white"
        self.fields['fecha_cheque'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_cheque'].widget.attrs['class']="fecha_cheque input-calendario"
        self.fields['fecha_cheque'].widget.attrs['placeholder']="aaaa-mm-dd"
        self.fields['fecha_cheque'].widget.attrs['style']="width: 100%"
        self.fields['referencia'].widget.attrs['class']="referencia txt-100pc"


forma_pago_cruce = ((1, "Cruce de Documentos"), (2, "Cruce de Cuentas"))


class CabeceraCurceDocForm(forms.Form):
    fecha = forms.DateField(label="Fecha", widget=forms.DateInput(format=("%Y-%m-%d")))
    observaciones = forms.CharField(max_length=750, widget=forms.Textarea, required=False)
    proveedor = forms.ChoiceField(choices=[], label=u"Proveedor")
    forma_pago_cabecera = forms.ChoiceField(choices=forma_pago_cruce, label="Forma de Pago")

    def getFecha(self):
        return self.cleaned_data['fecha']

    def getObservacion(self):
        return self.data["observaciones"]

    def getFormaPagoCabecera(self):
        return self.data["forma_pago_cabecera"]

    def getProveedor(self):
        return self.data["proveedor"]

    def __init__(self, *args, **kwargs):
        super(CabeceraCurceDocForm, self).__init__(*args, **kwargs)
        self.fields['observaciones'].widget.attrs['rows'] = 3
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['forma_pago_cabecera'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['forma_pago_cabecera'].widget.attrs['data-style']="slc-primary"

        q1 = Cuentas_por_Pagar.objects.filter(status=1).extra(where=["monto > pagado"])
        self.fields['proveedor'].choices = [("", "")] + [(x.id, x.razon_social) for x in Proveedores.objects.filter(status=1, cuentas_por_pagar__in=q1).distinct().order_by("razon_social")]
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un proveedor"
        self.fields['proveedor'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['data-live-search'] = True


class DetalleDocCruzarForm(forms.Form):
    id_cuenta_cobrar = forms.IntegerField(label="id_cuenta_cobrar")
    check = forms.BooleanField(label="check", required=False)
    tipo_doc = forms.CharField(max_length=50, required=False)
    numero_doc = forms.CharField(required=False)
    fecha_reg = forms.CharField(required=False)
    total = forms.FloatField(label="total", required=False)
    saldo = forms.FloatField(label="saldo", required=False)
    referencia = forms.CharField(required=False)
    valor = forms.FloatField(label="valor")

    def __init__(self, *args, **kwargs):
        super(DetalleDocCruzarForm, self).__init__(*args, **kwargs)
        self.fields['check'].widget.attrs['class']="checks"
        self.fields['valor'].widget.attrs['class']="valor numerico"
        self.fields['saldo'].widget.attrs['class']="saldo"
        self.fields['tipo_doc'].widget.attrs['readonly']= True
        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['fecha_reg'].widget.attrs['readonly'] = True
        self.fields['total'].widget.attrs['class'] = "total"
        self.fields['numero_doc'].widget.attrs['readonly'] = True
        self.fields['referencia'].widget.attrs['readonly'] = True
        self.fields['saldo'].widget.attrs['readonly'] = True
        self.fields['id_cuenta_cobrar'].widget.attrs['class'] = "tipo"


class DetallePagoCruceCta(forms.Form):
    id_cuenta_pagar = forms.IntegerField(label="id_cuenta_pagar")
    check = forms.BooleanField(label="check", required=False)
    tipo_doc = forms.CharField(max_length=50, required=False)
    numero_doc = forms.CharField(required=False)
    fecha_reg = forms.CharField(required=False)
    fecha_ven = forms.CharField(required=False)

    total = forms.FloatField(label="total", required=False)
    saldo = forms.FloatField(label="saldo", required=False)
    referencia = forms.CharField(required=False)
    cuenta = forms.ChoiceField(choices=[], required=False)
    valor = forms.FloatField(label="valor", required=False)

    def __init__(self, *args, **kwargs):
        super(DetallePagoCruceCta, self).__init__(*args, **kwargs)
        self.fields['valor'].widget.attrs['class']="valor numerico"

        self.fields['tipo_doc'].widget.attrs['readonly'] = True

        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['total'].widget.attrs['class'] = "total"

        self.fields['saldo'].widget.attrs['readonly'] = True
        self.fields['saldo'].widget.attrs['class'] = "saldo"

        self.fields['fecha_reg'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['readonly'] = True
        self.fields['fecha_ven'].widget.attrs['class'] = "fecha_vencimiento"

        self.fields['numero_doc'].widget.attrs['readonly'] = True

        self.fields['referencia'].widget.attrs['readonly'] = True

        self.fields['id_cuenta_pagar'].widget.attrs['class'] = "tipo"

        self.fields['check'].widget.attrs['class'] = "checks"

        self.fields['cuenta'].widget.attrs['class'] = "cuenta selectpicker show-tick"
        # Filtro excluye cuentas por tipo... # Revisar q cuentas es posible presentar
        self.fields['cuenta'].choices = [("", "")] + [(x.id, x.descripcion + " - " + x.codigo) for x in PlanCuenta.objects.filter(status=1, nivel=5).exclude(tipo=TipoCuenta.objects.get(alias=1)).exclude(tipo=TipoCuenta.objects.get(id=2)).exclude(tipo=TipoCuenta.objects.get(id=14))] ## Excluir cts
        self.fields['cuenta'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta'].widget.attrs['title'] = "Seleccione una Cuentas"
        self.fields['cuenta'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta'].widget.attrs['data-live-search'] = True


class BuscadorPagos(forms.Form):
    estados = (("", ""), (1, "Activo"), (2, "Anulado"))

    fecha_ini = forms.DateField(label=u"Fecha Inicial",
                                widget=forms.DateInput(format=("%Y-%m-%d")))

    fecha_final = forms.DateField(label=u"Fecha Final",
                                  widget=forms.DateInput(format=("%Y-%m-%d")))

    num_comp = forms.CharField(max_length=25, label=u"# Comprobante")
    num_doc = forms.CharField(max_length=25, label=u"# Documento")
    proveedor = forms.ChoiceField(choices=[], label=u"Proveedor")
    valor_ini = forms.FloatField(label=u"Rango de monto")
    valor_final = forms.FloatField(label=u"Hasta")
    estado = forms.ChoiceField(choices=estados)
    page = forms.IntegerField()

    def getFechaIni(self):
        try:
            value = self.data["fecha_ini"]
            str_fecha = str(value).split("-")
            return datetime.date(int(str_fecha[0]), int(str_fecha[1]), int(str_fecha[2]))
        except:
            return None

    def getFechaFinal(self):
        try:
            value = self.data["fecha_final"]
            str_fecha = str(value).split("-")
            return datetime.date(int(str_fecha[0]), int(str_fecha[1]), int(str_fecha[2]))
        except:
            return datetime.date.today()

    def getNumComp(self):
        try:
            value = self.data["num_comp"]
            return str(value).strip(' \t\n\r')
        except:
            return ""

    def getNumDoc(self):
        try:
            value = self.data["num_doc"]
            return value
        except:
            return ""
    def getProveedor(self):
        try:
            value = self.data["proveedor"]
            return Proveedores.objects.get(id=value)
        except:
            return None

    def getValorIni(self):
        try:
            value = float((self.data["valor_ini"]))
            return value
        except:
            return 0
    def getValorFinal(self):
        try:
            value = float((self.data["valor_final"]))
            return value
        except:
            return 1000000000
    def getEstado(self):
        try:
            value = int(self.data["estado"])
            return value
        except:
            return ""
    def getPage(self):
        try:
            value = int(self.data["page"])
            return value
        except:
            return 1

    def __init__(self, *args, **kwargs):
        super(BuscadorPagos, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_ini'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_ini'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_final'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['valor_ini'].widget.attrs['class'] = "numerico"
        self.fields['valor_final'].widget.attrs['class'] = "numerico"

        self.fields['estado'].widget.attrs['class'] = "cuenta selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado'].widget.attrs['data-width'] = "100%"
        self.fields['estado'].widget.attrs['title'] = "Seleccione un estado"

        self.fields['proveedor'].widget.attrs['class'] = "cuenta selectpicker show-tick"
        self.fields['proveedor'].choices = [("", "")] + [(x.id, x.razon_social[0:30]) for x in Proveedores.objects.filter(status=1).order_by("razon_social")]
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un proveedor"
        self.fields['proveedor'].widget.attrs['data-width'] = "100%"
        self.fields['proveedor'].widget.attrs['data-live-search'] = True


class CabeceraCXPInicialForm(forms.Form):
    proveedor = forms.ChoiceField(choices=[], label="Proveedor")
    fecha_reg = forms.CharField(label=u"Fecha de Registro", required=False)

    def getProveedor(self):
        return self.data["proveedor"]

    def __init__(self, *args, **kwargs):
        super(CabeceraCXPInicialForm, self).__init__(*args, **kwargs)
        self.fields['proveedor'].choices = [("", "")] + [(x.id, x.razon_social) for x in Proveedores.objects.filter(status=1).order_by("razon_social")]
        self.fields['proveedor'].widget.attrs['class'] = "selectpicker dropup show-tick"
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un proveedor"
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['data-width'] = "100%"
        self.fields['proveedor'].widget.attrs['data-live-search'] = True


class CuentasPagarInicialForm(forms.Form):
    tipo_doc = forms.ChoiceField(choices=[], label=u"Tipo Documento")
    num_doc = forms.CharField(max_length=20, label=u"# Documento")
    fecha = forms.CharField(max_length=100, label=u"Fecha", required=False)
    monto = forms.FloatField(label="Monto")
    plazo = forms.IntegerField(required=False, initial=0)

    def clean(self):
        monto = self.cleaned_data.get('monto', 0)

        if monto <= 0:
            self._errors["monto"] = "Campo obligatorio"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(CuentasPagarInicialForm, self).__init__(*args, **kwargs)
        q = Documento.objects.filter(status=1).order_by("codigo_documento")
        self.fields['tipo_doc'].choices = [("", "")] + [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in q]
        self.fields['tipo_doc'].widget.attrs['class'] = "selectpicker dropup show-tick tipo_doc"
        self.fields['tipo_doc'].widget.attrs['title'] = "Seleccione el tipo de documento"
        self.fields['tipo_doc'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_doc'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_doc'].widget.attrs['data-live-search'] = True

        self.fields['monto'].widget.attrs['class'] = "numerico monto"
        self.fields['num_doc'].widget.attrs['class'] = "numerico num_doc"
        self.fields['plazo'].widget.attrs['class'] = "numero_natural plazo"


class BuscadorCuentasxPagarinicialPagos(forms.Form):
    tipo_doc = forms.ChoiceField(choices=[], label=u"Tipo Documento")
    num_doc = forms.CharField(max_length=25, label=u"# Documento")
    proveedor = forms.ChoiceField(choices=[], label=u"Proveedor")
    monto_ini = forms.FloatField(label=u"Monto desde")
    monto_final = forms.FloatField(label=u"Hasta")

    pagado_ini = forms.FloatField(label=u"Pagado desde")
    pagado_final = forms.FloatField(label=u"Hasta")

    plazo = forms.IntegerField(label=u"Plazo")
    page = forms.IntegerField()

    def get_tipo_doc(self):
        try:
            value = self.data["tipo_doc"]
            return int(value)
        except:
            return ""

    def get_num_doc(self):
        try:
            value = self.data["num_doc"]
            return value
        except:
            return ""

    def get_proveedor(self):
        try:
            value = self.data["proveedor"]
            return value
        except:
            return ""

    def get_monto_ini(self):
        try:
            value = self.data["monto_ini"]
            return float(value)
        except:
            return 0

    def get_monto_final(self):
        try:
            value = self.data["monto_final"]
            return float(value)
        except:
            return 999999999

    def get_pagado_ini(self):
        try:
            value = self.data["pagado_ini"]
            return float(value)
        except:
            return 0

    def get_pagado_final(self):
        try:
            value = self.data["pagado_final"]
            return float(value)
        except:
            return 999999999

    def get_plazo(self):
        try:
            value = self.data["plazo"]
            return int(value)
        except:
            return 0

    def __init__(self, *args, **kwargs):
        super(BuscadorCuentasxPagarinicialPagos, self).__init__(*args, **kwargs)
        q = Documento.objects.filter(status=1).order_by("codigo_documento")
        self.fields['tipo_doc'].choices = [("", "")] + [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in q]
        self.fields['tipo_doc'].widget.attrs['class'] = "selectpicker dropup show-tick tipo_doc"
        self.fields['tipo_doc'].widget.attrs['title'] = "Seleccione el tipo de documento"
        self.fields['tipo_doc'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_doc'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_doc'].widget.attrs['data-live-search'] = True

        q = Proveedores.objects.filter(status=1).order_by("razon_social")
        self.fields['proveedor'].choices = [("", "")] + [(x.id, x.razon_social+" - "+x.ruc) for x in q]
        self.fields['proveedor'].widget.attrs['class'] = "selectpicker dropup show-tick tipo_doc"
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un Proveedor"
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['data-width'] = "100%"
        self.fields['proveedor'].widget.attrs['data-live-search'] = True

        self.fields['monto_ini'].widget.attrs['class'] = "numerico"
        self.fields['monto_ini'].widget.attrs['placeholder'] = "0.00"

        self.fields['monto_final'].widget.attrs['class'] = "numerico"
        self.fields['monto_final'].widget.attrs['placeholder'] = "0.00"

        self.fields['pagado_ini'].widget.attrs['class'] = "numerico"
        self.fields['pagado_ini'].widget.attrs['placeholder'] = "0.00"

        self.fields['pagado_final'].widget.attrs['class'] = "numerico"
        self.fields['pagado_final'].widget.attrs['placeholder'] = "0.00"

        self.fields['plazo'].widget.attrs['class'] = "numero_natural"
        self.fields['plazo'].widget.attrs['placeholder'] = "0"


class ReversionCuentasPagarForm(forms.Form):
    fecha = forms.DateField(label="Fecha Registro:", widget=forms.DateInput(format=("%Y-%m-%d")))
    concepto = forms.CharField(max_length=255, widget=forms.Textarea, required=False, label="Concepto:")

    def getFecha(self):
        return self.cleaned_data["fecha"]

    def getCconcepto(self):
        return self.data["concepto"]

    def __init__(self, *args, **kwargs):
        super(ReversionCuentasPagarForm, self).__init__(*args, **kwargs)
        self.fields['concepto'].widget.attrs['rows'] = 3
        self.fields['concepto'].widget.attrs['maxlength'] = 255
        self.fields['concepto'].widget.attrs['class'] = 'concepto_reversar'
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "numerico control-fecha"