#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from django.db import connection
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from datetime import date, timedelta
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from librerias.funciones.funciones_vistas import *
from django.db.models import Q
from django.utils.html import *
from django.forms.formsets import BaseFormSet

__author__ = 'Clip Maestro'


class EditATSForm(forms.Form):
    id = forms.IntegerField()
    num_comp = forms.CharField(max_length=37, required=False)
    proveedor = forms.CharField(max_length=200, required=False)
    ruc = forms.CharField(max_length=200, required=False)
    num_doc = forms.CharField(max_length=200, required=False)
    sustento_tributario = forms.ChoiceField(choices=[])
    autorizacion = forms.CharField(max_length=37)
    forma_pago = forms.ChoiceField(choices=[], required=False)
    tipo_pago = forms.ChoiceField(choices=[])
    pais = forms.ChoiceField(choices=[], required=False)
    doble_tributacion = forms.BooleanField(required=False)
    sujeto_retencion = forms.BooleanField(required=False)

    def clean(self):
        """
        Función para validar el formulario
        """
        id_tipo_pago = self.cleaned_data.get('tipo_pago')
        id_pais = self.cleaned_data.get('pais')
        try:
            tipo_pago = Tipo_Pago_SRI.objects.get(id=id_tipo_pago)
            if tipo_pago.codigo == "2":
                try:
                    pais = Pais.objects.get(id=id_pais)
                except (Pais.DoesNotExist, ValueError):
                    self._errors["pais"] = u"Debe de ingresar un pais si el tipo de " \
                                           u"pago es Exterior"
        except Tipo_Pago_SRI.DoesNotExist:
            pass

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(EditATSForm, self).__init__(*args, **kwargs)
        q1 = Sustento_Tributario.objects.filter(status=1)
        q2 = Forma_Pago_SRI.objects.filter(status=1)
        q3 = Tipo_Pago_SRI.objects.exclude(status=0).order_by("codigo")
        q4 = Pais.objects.filter(status=1).order_by("descripcion")
        self.fields['num_comp'].widget.attrs['readonly'] = "readonly"
        self.fields['sustento_tributario'].choices = [(x.id, x.codigo+u" - "+x.descripcion) for x in q1]
        self.fields['sustento_tributario'].widget.attrs['class']="selectpicker"
        self.fields['sustento_tributario'].widget.attrs['data-style'] = "slc-primary"
        self.fields['sustento_tributario'].widget.attrs['data-width'] = "100%"
        self.fields['sustento_tributario'].widget.attrs['data-size'] = "auto"

        self.fields['forma_pago'].choices = [('', '')] + [(x.id, x.descripcion) for x in q2]
        self.fields['forma_pago'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['forma_pago'].widget.attrs['data-style'] = "slc-primary"
        self.fields['forma_pago'].widget.attrs['data-width'] = "100%"
        self.fields['forma_pago'].widget.attrs['data-size'] = "auto"

        self.fields['tipo_pago'].choices = [(x.codigo, x.descripcion) for x in q3]
        self.fields['tipo_pago'].widget.attrs['class']="selectpicker"
        self.fields['tipo_pago'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_pago'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_pago'].widget.attrs['data-size'] = "auto"

        self.fields['pais'].choices = [('', '')] + [(x.id, x.descripcion) for x in q4]
        self.fields['pais'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['pais'].widget.attrs['data-style'] = "slc-primary"
        self.fields['pais'].widget.attrs['data-width'] = "100%"
        self.fields['pais'].widget.attrs['data-size'] = "auto"
        self.fields['pais'].widget.attrs['data-live-search'] = True