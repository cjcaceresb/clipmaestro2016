#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django import forms
from django.utils.html import *
from contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from datetime import date, timedelta
from django.utils.encoding import smart_str, smart_unicode
from librerias.funciones.validacion_formularios import validador_espacios
from django.contrib import messages
__author__ = 'user3'

solo_letras = u'^[A-Za-zÑñáéíóúÁÉÍÓÚ ]+$'
solo_numeros = u'^[0-9]+$'

tipo_plan_cuenta = (('', ''), ('D', 'D'), ('C', 'C'))
del_sel_tran = "%/&$"

def validate_numero_cuenta(value):
    '''
    Función validadora del # de cuenta que no este vacía y que no se repita.
    :param value:
    :return:
    '''
    try:
        if(value.index(' ')!=None):
            raise ValidationError(u'%s No es permitido porque tiene espacios en blanco' % value)
    except ValueError:
        pass

    if Cuenta_Banco.objects.filter(Q(descripcion=value), Q(numero_cuenta=value), Q(status=1) | Q(status=2)).exists():
        raise ValidationError(u' El Nº de cuenta ya ha sido ingresado')
    else:
        return True

class FormBancoOnlyRead(forms.Form):
    '''
    Formulario utilizado para tenerlo en modo sólo lectura llamado para la opción "VER CUENTA BANCARIA"
    '''
    cuentas = forms.ChoiceField(label=u"Cuenta", choices=[], required=False)
    descripcion = forms.CharField(max_length=75,label=u"Nombre Cuenta", validators=[validador_espacios])
    numero_cuenta = forms.CharField(label=u"Nº de Cuenta", validators=[validador_espacios])
    tipo = forms.ChoiceField(label=u"Tipo de Cuenta", choices=[])
    secuencia_cheque = forms.CharField(label=u"Última Secuencia Cheque", required=False)
    fecha_ultimo_cierre = forms.CharField(label=u"Fecha Último Cierre")
    fecha_conciliacion = forms.CharField(label=u"Fecha Última Conciliación")

    def __init__(self, *args, **kwargs):
        super(FormBancoOnlyRead, self).__init__(*args, **kwargs)
        self.fields['numero_cuenta'].widget.attrs['maxlength'] = "10"
        self.fields['descripcion'].widget.attrs['maxlength'] = "75"
        self.fields['tipo'].choices = [(x.id, x.descripcion) for x in Tipo_Cuenta_Banco.objects.filter(status=1)]
        self.fields['tipo'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo'].widget.attrs['data-style'] = "slc-primary"

        q1 = PlanCuenta.objects.filter(tipo_id=1, nivel=5)
        self.fields['cuentas'].choices = [("", "")]+[(x.id, x.codigo+" - "+unicode(x.descripcion)) for x in q1]
        self.fields['cuentas'].widget.attrs['title'] = "Seleccione una Cuenta"
        self.fields['cuentas'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuentas'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuentas'].widget.attrs['data-width'] = "100%"
        self.fields['cuentas'].widget.attrs['data-size'] = "auto"
        self.fields['fecha_ultimo_cierre'].widget.attrs['data-date-format'] = "dd-mm-yyyy"
        self.fields['fecha_ultimo_cierre'].widget.attrs['class'] = "numerico calendario-gris datepicker input-calendario"
        self.fields['fecha_ultimo_cierre'].widget.attrs['readonly'] = "readonly"
        self.fields['fecha_conciliacion'].widget.attrs['data-date-format'] = "dd-mm-yyyy"
        self.fields['fecha_conciliacion'].widget.attrs['class'] = "numerico calendario-gris datepicker input-calendario"
        self.fields['fecha_conciliacion'].widget.attrs['readonly'] = "readonly"
        self.fields['secuencia_cheque'].widget.attrs['placeholder'] = "9999999999"
        self.fields['secuencia_cheque'].widget.attrs['maxlength'] = "10"
        self.fields['tipo'].widget.attrs['readonly'] = True
        self.fields['tipo'].widget.attrs['disabled'] = True
        self.fields['descripcion'].widget.attrs['readonly'] = True
        self.fields['numero_cuenta'].widget.attrs['readonly'] = True
        self.fields['secuencia_cheque'].widget.attrs['readonly'] = True
        self.fields['fecha_ultimo_cierre'].widget.attrs['readonly'] = True
        self.fields['fecha_ultimo_cierre'].widget.attrs['disabled'] = True
        self.fields['fecha_conciliacion'].widget.attrs['readonly'] = True
        self.fields['fecha_conciliacion'].widget.attrs['disabled'] = True
        self.fields['cuentas'].widget.attrs['readonly'] = True
        self.fields['cuentas'].widget.attrs['disabled'] = True

class FormBanco(forms.Form):
    cuentas = forms.ChoiceField(label=u"Cuenta", choices=[], required=False)
    descripcion = forms.CharField(max_length=50, label=u"Nombre Banco", validators=[validador_espacios], required=False)
    numero_cuenta = forms.CharField(label=u"Nº Cta. Bancaria", validators=[validador_espacios])
    tipo = forms.ChoiceField(label=u"Tipo de Cuenta", choices=[])
    secuencia_cheque = forms.CharField(label=u"Última Secuencia Cheque", required=False)
    fecha_ultimo_cierre = forms.CharField(label=u"Fecha Último Cierre", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_conciliacion = forms.CharField(label=u"Fecha Última Conciliación", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)

    def getDescripcion(self):
        return self.data["descripcion"]
    def getNumeroCuenta(self):
        return self.data["numero_cuenta"]
    def getSecuenciaCheque(self):
        return self.data["secuencia_cheque"]
    def getTipo(self):
        return self.data['tipo']
    def getFechaUltimoCierre(self):
        return self.data["fecha_ultimo_cierre"]
    def getFechaUltimoConciliacion(self):
        return self.data["fecha_conciliacion"]
    def getCuentas(self):
        try:
            value = self.data["cuentas"]
            return PlanCuenta.objects.get(id=value)
        except:
            return None

    def clean(self):
        tipo = self.cleaned_data.get("tipo", None)
        cuentas = self.cleaned_data.get("cuentas", None)
        descripcion = self.cleaned_data.get("descripcion", None)
        secuencia_cheque = self.cleaned_data.get("secuencia_cheque", None)

        if tipo == "2":
            if secuencia_cheque is None or secuencia_cheque == "":
                self._errors["secuencia_cheque"] = u"Dato Requerido"

        if cuentas is None or cuentas == "":
            if descripcion is None or descripcion == "":
                self._errors["descripcion"] = u"Dato Requerido"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(FormBanco, self).__init__(*args, **kwargs)
        self.fields['numero_cuenta'].widget.attrs['maxlength'] = "10"
        self.fields['descripcion'].widget.attrs['maxlength'] = "50"

        self.fields['tipo'].choices = [(x.id, x.descripcion) for x in Tipo_Cuenta_Banco.objects.filter(status=1)]
        self.fields['tipo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo'].widget.attrs['data-width'] = "100%"
        self.fields['tipo'].widget.attrs['data-size'] = "auto"

        q1 = PlanCuenta.objects.filter(tipo_id=1, nivel=5, status=1)
        self.fields['cuentas'].choices = [("", "")]+[(x.id, x.codigo+" - "+unicode(x.descripcion)) for x in q1]
        self.fields['cuentas'].widget.attrs['title'] = "Seleccione una Cuenta"
        self.fields['cuentas'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuentas'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuentas'].widget.attrs['data-width'] = "100%"
        self.fields['cuentas'].widget.attrs['data-size'] = "auto"

        self.fields['fecha_ultimo_cierre'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_ultimo_cierre'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_ultimo_cierre'].widget.attrs['class'] = "numerico calendario-gris datepicker input-calendario"
        self.fields['fecha_ultimo_cierre'].widget.attrs['readonly'] = "readonly"
        self.fields['fecha_conciliacion'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_conciliacion'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_conciliacion'].widget.attrs['class'] = "numerico calendario-gris datepicker input-calendario"
        self.fields['fecha_conciliacion'].widget.attrs['readonly'] = "readonly"
        self.fields['secuencia_cheque'].widget.attrs['placeholder'] = "9999999999"
        self.fields['secuencia_cheque'].widget.attrs['maxlength'] = "10"

solo_alfanumericas = u'[a-zA-Z0-9_]+$'

def validate_valor(value):
    valor = float(value)
    if valor <= 0:
        raise ValidationError(u'Ingrese un valor superior a $0.00')
    else:
        return True


class SelectCuentaBancaria(forms.Select):
    allow_multiple_selected = False
    def __init__(self, attrs=None, choices=()):
        super(SelectCuentaBancaria, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            try:
                tipo_cuenta = Cuenta_Banco.objects.get(id=option_value).tipo_cuenta_banco_id
                id_plan_cuenta = Cuenta_Banco.objects.get(id=option_value).plan_cuenta_id
            except:
                tipo_cuenta = ""
                id_plan_cuenta = ""

            return format_html(u'<option value="{0}" {1} data-tipo={2} data-id={3}> {4} </option>',
                               option_value,
                               selected_html,
                               tipo_cuenta,
                               id_plan_cuenta,
                               unicode(force_text(option_label)))
        else:
            return format_html('<option value="{0}" {1}>{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class CabeceraTransaccionForm(forms.Form):
    tipo_movimiento = forms.ChoiceField(choices=[], label=u"Tipo Movimiento")
    check_provision = forms.BooleanField(label=u"Cheque Provisionado", required=False)
    fecha = forms.DateField(label=u"F. de Registro", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_cheque = forms.DateField(label=u"F. del Cheque", required=False, widget=forms.DateInput(format=("%Y-%m-%d")))
    cuenta_bancaria = forms.ChoiceField(choices=[], label=u"Cuenta Bancaria", widget=SelectCuentaBancaria())
    cheques_provisionados = forms.ChoiceField(choices=[], label=u"Ch./ provisionados", required=False)
    numero_cheque = forms.CharField(label=u"# del Cheque", required=False)
    # BENEFICIARIO
    paguese = forms.CharField(max_length=150, label=u"Páguese a", required=False)
    referencia = forms.CharField(max_length=150, required=False)
    concepto = forms.CharField(widget=forms.Textarea, label=u"Concepto", required=False)

    def getTipoMov(self):
        return self.data["tipo_movimiento"]

    def getFecha(self):
        return self.cleaned_data['fecha']

    def getFechaCheque(self):
        return self.cleaned_data["fecha_cheque"]

    def getCuentaBanc(self):
        return self.data["cuenta_bancaria"]

    def getNumeroCheque(self):
        return self.data["numero_cheque"]

    def getPaguese(self):
        return self.data["paguese"]

    def getReferencia(self):
        return self.data["referencia"]

    def getConcepto(self):
        return self.data["concepto"]

    def es_cheque_provisionado(self):
        try:
            return self.data["check_provision"]
        except:
            return False

    def get_cheque_provisionado(self):
        return self.data["cheques_provisionados"]

    def clean(self):
        """
            Función para validar el formulario
        """
        tipo_movimiento = self.cleaned_data.get('tipo_movimiento')
        paguese = self.cleaned_data.get('paguese')
        es_provisionado = self.cleaned_data.get('check_provision')
        cheque_provisionado = self.cleaned_data.get("cheques_provisionados")
        fecha_reg = self.cleaned_data.get("fecha")

        # Validación de Pago (local-exterior)
        if tipo_movimiento == '1' or tipo_movimiento == '2' or tipo_movimiento == '3':
            if paguese == "":
                self._errors["paguese"] = u"Se debe de ingresar la información del beneficiario"
            if es_provisionado:
                try:
                    Banco.objects.get(id=cheque_provisionado, status=3)
                except (Banco.DoesNotExist, ValueError):
                    self._errors["cheques_provisionados"] = u"Debe de ingresar el cheque provisionado"
            else:
                if fecha_reg == "":
                    self._errors["fecha"] = u"Campo obligatorio"
        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(CabeceraTransaccionForm, self).__init__(*args, **kwargs)
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "numerico calendario-gris datepicker"
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['text-align'] = "left"
        self.fields['fecha'].widget.attrs['style'] = "width: 100%"
        self.fields['concepto'].widget.attrs['maxlength'] = "200"
        self.fields['paguese'].widget.attrs['maxlength'] = "150"
        self.fields['referencia'].widget.attrs['maxlength'] = "150"
        self.fields['check_provision'].widget.attrs['title'] = "Cheques Provisionados"
        self.fields['fecha_cheque'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_cheque'].widget.attrs['class'] = "numerico calendario-gris datepicker input-calendario"
        self.fields['fecha_cheque'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_cheque'].widget.attrs['text-align'] = "left"
        self.fields['fecha_cheque'].widget.attrs['style'] = "width: 100%"
        self.fields['tipo_movimiento'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_movimiento'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_movimiento'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_movimiento'].widget.attrs['data-size'] = "auto"
        self.fields['tipo_movimiento'].choices = [(x.id, x.descripcion)
                                                  for x in TipoMovimientoBancario.objects.filter(status=1).order_by("id")]

        self.fields['cuenta_bancaria'].choices = [("", "")] + \
                                                 [(x.id, x.descripcion+" - " + x.tipo_cuenta_banco.descripcion
                                                   + " - " + x.numero_cuenta)
                                                  for x in Cuenta_Banco.objects.filter(status=1).order_by("descripcion")]

        self.fields['cuenta_bancaria'].widget.attrs['title'] = "Seleccione una Cuenta"
        self.fields['cuenta_bancaria'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_bancaria'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta_bancaria'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta_bancaria'].widget.attrs['data-size'] = "auto"

        query_provisionados = Banco.objects.filter(status=3)
        self.fields['cheques_provisionados'].choices = [("", "")] + [(x.id, x.num_cheque+" - "+unicode(x.cuenta_banco.descripcion))
                                                                     for x in query_provisionados]

        self.fields['cheques_provisionados'].widget.attrs['title'] = "Seleccione una Cheque Provisionado"
        self.fields['cheques_provisionados'].widget.attrs['class'] = "selectpicker show-tick cheque_provision"
        self.fields['cheques_provisionados'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cheques_provisionados'].widget.attrs['data-width'] = "100%"
        self.fields['cheques_provisionados'].widget.attrs['data-size'] = "auto"
        self.fields['concepto'].widget.attrs['maxlength'] = "250"
        self.fields['concepto'].widget.attrs['style'] = "width: 100%"
        self.fields['referencia'].widget.attrs['style'] = "width: 100%"
        self.fields['numero_cheque'].widget.attrs['style'] = "width: 100%"
        self.fields['numero_cheque'].widget.attrs['readonly'] = True
        self.fields['paguese'].widget.attrs['style'] = "width: 100%"

from django.forms.formsets import BaseFormSet
class BaseDetalleTransFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseDetalleTransFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

    def clean(self):
        """Checks that no two articles have the same title."""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return

        for form in self.forms:
            id_cta = form.cleaned_data.get("cuenta_id")
            if not PlanCuenta.objects.filter(id=id_cta, status=1, nivel=5).exists():
                errors = form._errors.setdefault("cuenta_id", ErrorList())
                errors.append(u"Ingrese una cuenta válida")
            try:
                debe = redondeo(float(form.cleaned_data.get("debe", 0)))
            except:
                debe = 0
            try:
                haber = redondeo(float(form.cleaned_data.get("haber", 0)))
            except:
                haber = 0

            if debe == 0 and haber == 0:
                errors = form._errors.setdefault("debe", ErrorList())
                errors.append(u"Ingrese un valor")
                errors = form._errors.setdefault("haber", ErrorList())
                errors.append(u"Ingrese un valor")
            else:
                if debe > 0 and haber > 0:
                    errors = form._errors.setdefault("debe", ErrorList())
                    errors.append(u"valor no válido")
                    errors = form._errors.setdefault("haber", ErrorList())
                    errors.append(u"Ingrese un valor")


class SelectTransacciones(forms.Select):
    allow_multiple_selected = False
    def __init__(self, attrs=None, choices=()):
        super(SelectTransacciones, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            detalle = force_text(option_label).split(del_sel_tran)[0] + " - " + \
                      force_text(option_label).split(del_sel_tran)[1]

            return format_html(u'<option value="{0}" {1} data-tipo={2}>{3}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label).split(del_sel_tran)[2],
                               smart_unicode(detalle)
                               )
        else:
            return format_html('<option value="{0}"{1} data-porcentaje=0>{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class FormDetalleTransaccion(forms.Form):
    cuenta_id = forms.ChoiceField(choices=[], label="Cuenta", widget=SelectTransacciones())
    concepto = forms.CharField(label="Concepto", max_length=200, required=False)
    debe = forms.FloatField(label="debe", required=False)
    haber = forms.FloatField(label="haber", required=False)
    centro_costo = forms.ChoiceField(choices=[], label=u"Centro de Costo", required=False)

    def __init__(self, *args, **kwargs):
        super(FormDetalleTransaccion, self).__init__(*args, **kwargs)
        self.fields['centro_costo'].choices = [("", "")]+[(x.id, x.descripcion) for x in Centro_Costo.objects.filter(status=1)]
        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker centro_costo"
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['centro_costo'].widget.attrs['data-name'] = "combo_centro_costo"
        self.fields['centro_costo'].widget.attrs['style'] = "text-align: right;"
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True

        self.fields['concepto'].widget.attrs['class'] = "concepto"
        self.fields['concepto'].widget.attrs['style'] = "width: 100%; height: 32px !important;"
        self.fields['concepto'].widget.attrs['maxlength'] = "200"

        self.fields['debe'].widget.attrs['placeholder'] = "0.00"
        self.fields['debe'].widget.attrs['style'] = "text-align: right; width: 140px; height: 32px !important;"
        self.fields['debe'].widget.attrs['class'] = "numerico debe"

        self.fields['haber'].widget.attrs['placeholder'] = "0.00"
        self.fields['haber'].widget.attrs['style'] = "text-align: right; width: 140px; height: 32px !important;"
        self.fields['haber'].widget.attrs['class'] = "numerico haber"
        q1 = PlanCuenta.objects.filter(status=1, nivel=5).filter(tipo__in=TipoCuenta.objects.exclude().filter(id=1, status=1)).order_by("codigo")
        q2 = PlanCuenta.objects.filter(status=1, nivel=5).exclude(tipo__in=TipoCuenta.objects.filter(status=1).exclude(id=8).exclude(id=12).exclude(id=22).exclude(id=2)).order_by("codigo")
        #q = PlanCuenta.objects.filter(status=1, nivel=5).exclude(tipo__in=TipoCuenta.objects.filter(status=1).exclude(id=11).exclude(id=15).exclude(id=22).exclude(id=20).exclude(id=2).exclude(id=6).exclude(id=8).exclude(id=19).exclude(id=21).exclude(id=17).order_by("codigo"))
        self.fields['cuenta_id'].choices = [("", "")] + [(x.id, x.codigo + del_sel_tran + x.descripcion + del_sel_tran + str(x.tipo_id)) for x in q1 ] + \
                                           [(x.id, x.codigo + del_sel_tran + x.descripcion + del_sel_tran + "0") for x in q2]
        # 1 diciembre el 12 es para que salgan las cajas chicas
        self.fields['cuenta_id'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_id'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta_id'].widget.attrs['data-name'] = "combo_codigo_cta"
        self.fields['cuenta_id'].widget.attrs['title'] = "Seleccione una cuenta"
        self.fields['cuenta_id'].widget.attrs['style'] = "text-align: right;"
        self.fields['cuenta_id'].widget.attrs['data-live-search'] = True

class FormBuscadorCuentaBanco(forms.Form):
    '''
    Formulario utilizado para el buscador de las Cuentas Bancarias
    '''
    descripcion = forms.CharField(label=u"Descripcion", max_length=120)
    numero_cuenta = forms.CharField(label=u"# Cuenta", max_length=120)
    tipo_cuenta = forms.ChoiceField(choices=[])
    page = forms.IntegerField()

    def getDescripcion(self):
        try:
            value = self.data['descripcion']
            return value
        except:
            return ""

    def getNumeroCuenta(self):
        try:
            value = self.data['numero_cuenta']
            return value
        except:
            return ""

    def getTipoCuentaBanco(self):
        try:
            value = self.data['tipo_cuenta']
            return Tipo_Cuenta_Banco.objects.get(id=value)
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(FormBuscadorCuentaBanco, self).__init__(*args, **kwargs)
        self.fields['tipo_cuenta'].choices = [("", "")] + [(x.id, x.descripcion) for x in Tipo_Cuenta_Banco.objects.filter(status=1).order_by("id")]
        self.fields['tipo_cuenta'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo_cuenta'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_cuenta'].widget.attrs['data-width'] = "100%"

estado = (("", ""), (1, "Activo"), (2, "Anulado"))
class FormBuscadorTransaccionBanco(forms.Form):
    '''
    Formulario utilizado para el buscador de las Transacciones Bancarias
    '''
    num_comp = forms.CharField(label=u"# de Comprobante", max_length=120)
    fecha_inicio = forms.DateField(label=u'Fecha Inicio', widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_fin = forms.DateField(label=u'Fecha Fin', widget=forms.DateInput(format=("%Y-%m-%d")))
    tipo_comprobante = forms.ChoiceField(choices=[], label="Tipo Comprobante")
    cuenta_banco = forms.ChoiceField(choices=[], label="Cuenta Bancaria")
    concepto = forms.CharField(label=u"Concepto", max_length=220, widget=forms.Textarea)
    estado = forms.ChoiceField(label=u"Estado", choices=estado)
    page = forms.IntegerField()

    def get_num_comprobante(self):
        try:
            return self.data['num_comp']
        except:
            return ""

    def get_fecha_ini(self):
        try:
            value = str(self.data['fecha_inicio']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return None

    def get_fecha_final(self):
        try:
            value = str(self.data['fecha_fin']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return datetime.datetime.now().date()

    def get_tipo_comp(self):
        try:
            return self.data['tipo_comprobante']
        except:
            return ""

    def get_cuenta_banco(self):
        try:
            return self.data['cuenta_banco']
        except:
            return ""

    def get_concepto(self):
        try:
            return self.data['concepto']
        except:
            return ""

    def get_estado(self):
        try:
            return self.data['estado']
        except:
            return ""

    def __init__(self, *args, **kwargs):
        super(FormBuscadorTransaccionBanco, self).__init__(*args, **kwargs)
        self.fields['fecha_inicio'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha_inicio'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_inicio'].widget.attrs['class'] = "control-fecha"

        self.fields['fecha_fin'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_fin'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_fin'].widget.attrs['class'] = "control-fecha"
        q1 = TipoComprobante.objects.filter(status=1).filter(Q(id=3) | Q(id=4) | Q(id=23) | Q(id=24) | Q(id=25)).order_by("id")
        self.fields['tipo_comprobante'].choices = [("", "")] + [(x.id, x.descripcion) for x in q1]
        self.fields['tipo_comprobante'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_comprobante'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_comprobante'].widget.attrs['data-live-search'] = True
        self.fields['tipo_comprobante'].widget.attrs['data-width'] = "100%"

        qcta = Cuenta_Banco.objects.filter(status=1).order_by("id")
        self.fields['cuenta_banco'].choices = [("", "")] + [(x.id, x.descripcion + x.numero_cuenta) for x in qcta]
        self.fields['cuenta_banco'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_banco'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta_banco'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_banco'].widget.attrs['data-width'] = "100%"

        self.fields['estado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado'].widget.attrs['data-live-search'] = True
        self.fields['estado'].widget.attrs['data-width'] = "100%"

        self.fields['concepto'].widget.attrs['placeholder'] = "Concepto"
        self.fields['concepto'].widget.attrs['rows'] = 3
        self.fields['concepto'].widget.attrs['maxlength'] = "220"

class FormAnularChequeBanco(forms.Form):
    fecha_reg = forms.DateField(label=u'Fecha Registro: ', widget=forms.DateInput(format=("%Y-%m-%d")))
    cuenta_banco = forms.ChoiceField(choices=[], label=u'Cuenta Bancaria: ')
    numero_cheque = forms.CharField(max_length=9, label=u'Número de Cheque: ', required=False)

    def getCuentaBanco(self):
        return self.data['cuenta_banco']

    def getFechaReg(self):
        return self.cleaned_data["fecha_reg"]

    def getNumeroCheque(self):
        return self.data['numero_cheque']

    def __init__(self, *args, **kwargs):
        super(FormAnularChequeBanco, self).__init__(*args, **kwargs)

        self.fields['cuenta_banco'].choices = [(x.id, x.plan_cuenta.codigo+"-"+x.plan_cuenta.descripcion)
                                               for x in Cuenta_Banco.objects.filter(status=1, tipo_cuenta_banco__id=2).order_by("plan_cuenta__codigo")]

        self.fields['cuenta_banco'].widget.attrs['class'] = "selectpicker show-tick cuenta_banco"
        self.fields['cuenta_banco'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['cuenta_banco'].widget.attrs['data-width'] = "100%"
        self.fields['numero_cheque'].widget.attrs['class'] = "numerico txt-100pc"
        self.fields['fecha_reg'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_reg'].widget.attrs['data-width'] = "100%"
        self.fields['fecha_reg'].widget.attrs['style'] = "width: 100%;"
        self.fields['fecha_reg'].widget.attrs['class'] = "calendario-gris input-calendario"

class FormConciliacionBanco(forms.Form):
    fecha_cierre = forms.DateField(label=u"Fecha de Corte", widget=forms.DateInput(format=("%Y-%m-%d")))
    cuenta_banco = forms.ChoiceField(label=u"Cuenta Bancaria")
    ver_historial = forms.BooleanField(required=False)

    def getFechaCierre(self):
        return self.cleaned_data['fecha_cierre']

    def getCuentaBanco(self):
        try:
            value = self.data['cuenta_banco']
            return Cuenta_Banco.objects.get(id=value)
        except:
            return None

    def getVerHistorial(self):
        try:
            return self.data["ver_historial"]
        except:
            return False


    def __init__(self, *args, **kwargs):
        super(FormConciliacionBanco, self).__init__(*args, **kwargs)
        self.fields['fecha_cierre'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha_cierre'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_cierre'].widget.attrs['class'] = "control-fecha"
        self.fields['fecha_cierre'].widget.attrs['style'] = "width: 100%;"
        self.fields['fecha_cierre'].widget.attrs['data-width'] = "100%"

        self.fields['cuenta_banco'].choices = [("", "")] + [(x.id, x.numero_cuenta+" - "+x.descripcion+" - "+x.tipo_cuenta_banco.descripcion)
                                                            for x in Cuenta_Banco.objects.filter(status=1).order_by("id")]
        self.fields['cuenta_banco'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_banco'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta_banco'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_banco'].widget.attrs['data-width'] = "100%"


class FormTransaccionesConciliar(forms.Form):
    '''
    Formulario utilizado como FormSet para obtener todas las transacciones realizadas en base a una "CUENTA BANCARIA"
    '''
    id_banco = forms.IntegerField(label=u"id_banco")
    conciliado = forms.BooleanField(label=u"conciliado", required=False)
    check = forms.BooleanField(label=u"check", required=False)
    tipo_comprobante = forms.CharField(label=u"Tipo Comprobante", required=False)
    num_comp = forms.CharField(label=u"# de Comprobante", max_length=120, required=False)
    fecha_reg = forms.DateField(label=u'Fecha Inicio', widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    num_cheque = forms.CharField(label=u'# Cheque', required=False)
    beneficiario = forms.CharField(label=u'Beneficiario', required=False)
    detalle = forms.CharField(label=u"Detalle", required=False)
    valor = forms.FloatField(label=u"Valor",  required=False)

    def getNumComprobante(self):
        try:
            value = self.data['num_comp']
            return value
        except:
            return ""

    def getNumCheque(self):
        try:
            value = self.data['num_cheque']
            return value
        except:
            return ""

    def getFechaReg(self):
        return self.cleaned_data['fecha_reg']

    def getCheck(self):
        return self.data["check"]

    def getValor(self):
        return self.data["valor"]

    def __init__(self, *args, **kwargs):
        super(FormTransaccionesConciliar, self).__init__(*args, **kwargs)
        self.fields['fecha_reg'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_reg'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_reg'].widget.attrs['class'] = "control-fecha"
        self.fields['beneficiario'].widget.attrs['class'] = 'info_beneficiario'
        self.fields['detalle'].widget.attrs['class'] = 'info detalle'
        self.fields['check'].widget.attrs['class'] = 'check'
        self.fields['conciliado'].widget.attrs['class'] = 'conciliado'

