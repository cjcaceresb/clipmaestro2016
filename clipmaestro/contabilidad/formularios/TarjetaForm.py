#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q

__author__ = 'user3'

solo_letras = u'^[A-Za-zÑñáéíóúÁÉÍÓÚ ]+$'
solo_numeros = u'^[0-9]+$'

origen = ((1, 'Nacional'), (2, 'Internacional'))

tipo_tarjeta = ((1, 'Visa'), (2, 'Mastercard'), (3, 'Diners'), (4, 'American Express'))

def validate_numero_cuenta(value):
    try:
        if(value.index(' ')!=None):
            raise ValidationError(u'%s No es permitido porque tiene espacios en blanco' % value)
    except ValueError:
        pass
    if Banco.objects.filter(Q(numero_cuenta=value), Q(status=1) | Q(status=2)).exists():
        raise ValidationError(u' El Nº de cuenta ya ha sido ingresado')
    else:
        return True

class TarjetaForm(forms.Form):
    descripcion = forms.CharField(label="Descripción", max_length=220)
    numero_tarjeta = forms.RegexField(validators=[validate_numero_cuenta], label="Nº de Cuenta", regex=solo_numeros, error_message = (u"Este campo sólo acepta números."),)
    tipo = forms.ChoiceField(label="Tipo", choices=tipo_tarjeta)
    origen_tarjeta = forms.ChoiceField(label="Tipo", choices=origen)
    banco_emisor = forms.CharField(max_length=200)
    dia_corte = forms.IntegerField()
    fecha_expiracion = forms.DateTimeField()

    def __init__(self, *args, **kwargs):
        super(TarjetaForm, self).__init__(*args, **kwargs)
        self.fields['numero_tarjeta'].widget.attrs['maxlength'] = "50"
        self.fields['tipo'].widget.attrs['class']="selectpicker"
        self.fields['tipo'].widget.attrs['data-style']="slc-primary"
        self.fields['origen_tarjeta'].widget.attrs['class']="selectpicker"
        self.fields['origen_tarjeta'].widget.attrs['data-style']="slc-primary"
        self.fields['fecha_expiracion'].widget.attrs['class']="calendario-gris"

    def getDescripcion(self):
        return self.data["descripcion"]

    def getNumeroTarjeta(self):
        return self.data["numero_tarjeta"]

    def getTipo(self):
        return self.data["tipo"]

    def getOrigenTarjeta(self):
        return self.data["origen_tarjeta"]

    def getBancoEmisor(self):
        return self.data["banco_emisor"]

    def getDiaCorte(self):
        return self.data["dia_corte"]

    def getFechaExpiracion(self):
        return self.data["fecha_expiracion"]
