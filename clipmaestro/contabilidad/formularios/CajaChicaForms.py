#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q


class FormCabeceraCaja(forms.Form):
    fecha = forms.DateTimeField(label="Fecha")
    responsable = forms.ChoiceField(label="Responsable", choices=[])
    concepto = forms.CharField(max_length=200, widget=forms.Textarea, label="Concepto")

    def getFecha(self):
        return self.data["fecha"]

    def getResponsable(self):
        return self.data["responsable"]

    def getConcepto(self):
       return self.data["concepto"]

    def __init__(self, *args, **kwargs):
        super(FormCabeceraCaja, self).__init__(*args, **kwargs)
        self.fields['concepto'].widget.attrs['placeholder'] = "Concepto General"
        self.fields['fecha'].widget.attrs['class'] = "calendario-gris"
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['responsable'].choices = [(x.id, x.codigo+"-"+x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(status=1, tipo__alias=12, nivel=5).order_by("codigo")]
        self.fields['responsable'].widget.attrs['class'] = "selectpicker"
        self.fields['responsable'].widget.attrs['data-style'] = "slc-primary"

tipo_localidad = (('',''),
        ("01", '01-PAGO LOCAL'),
        ("02", '02-PAGO EXTERIOR'),)

tipo = (('1', 'B'), ('2', 'S'))

costo_gasto = (('1', 'Costo'),
               ('2', 'Gasto'))

### Formulario que posee los datos de Detalle de Caja Chica ###
class FormDetalleCajaChica(forms.Form):
    cuenta = forms.CharField(label="CUENTA")
    tipo_identificacion = forms.ChoiceField(choices=[], label="Tipo de Identificación Proveedor")
    proveedor = forms.CharField(max_length=20, label="Proveedor")
    sustento_tributario = forms.ChoiceField(choices=[], label="Sustento Tributario")
    tipo_documento = forms.ChoiceField(choices=[], label="Tipo Documento")
    serie = forms.CharField(label="Serie")
    fecha_emision = forms.DateTimeField(label="Fecha de Emisión")
    num_autorizacion = forms.CharField(label="Nº Autorización")
    fecha_vencimiento = forms.DateTimeField(label="Fecha de Vencimiento")
    porc_iva = forms.ChoiceField(choices=[], required=False, label="IVA")
    #bs = forms.ChoiceField(choices=tipo, label="B/S")
    cto_gto = forms.ChoiceField(choices=costo_gasto)
    valor = forms.FloatField()
    '''
    ################### Se debe Quitar #################################
    id_info = forms.ChoiceField(label="Id_info_gasto", required=False)
    pago = forms.ChoiceField(choices=[])
    pais = forms.ChoiceField(choices=[], required=False)
    conv_doble_tributacion = forms.BooleanField(required=False)
    sujeto_reten = forms.BooleanField(required=False)
    '''

    def getCuenta(self):
        return self.data["cuenta"]
    def getBS(self):
        return self.data["bs"]
    def getValor(self):
        return self.data["valor"]
    def getIVA(self):
        return self.data["porc_iva"]
    def getBS(self):
        return self.data["bs"]

    def __init__(self, *args, **kwargs):
        super(FormDetalleCajaChica, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].choices = [(x.codigo, x.codigo+"-"+x.descripcion) for x in Identificacion.objects.exclude(status=0).exclude(codigo='F')]
        self.fields['tipo_identificacion'].widget.attrs['class'] = "selectpicker tipo_identificacion"
        self.fields['tipo_identificacion'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['tipo_identificacion'].widget.attrs['data-name'] = "combo_tipo_id"
        self.fields['tipo_identificacion'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_identificacion'].widget.attrs["style"] = "min-width: 140px;"


        self.fields['sustento_tributario'].choices = [(x.id, x.codigo+" - "+x.descripcion) for x in Sustento_Tributario.objects.filter(status=1).order_by("codigo")]
        self.fields['sustento_tributario'].widget.attrs['class'] = "selectpicker sustento_tributario"
        self.fields['sustento_tributario'].widget.attrs['data-style'] = "slc-c-c"
        self.fields['sustento_tributario'].widget.attrs['data-id'] = "sustent_tri"
        self.fields['sustento_tributario'].widget.attrs['data-size'] = "5"
        self.fields['sustento_tributario'].widget.attrs['data-name'] = "combo_sust_trib"

        self.fields['tipo_documento'].choices = [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in Documento.objects.exclude(status=0).order_by("codigo_documento")]
        self.fields['tipo_documento'].widget.attrs['class'] = "selectpicker tipo_documento"
        self.fields['tipo_documento'].widget.attrs['data-id'] = "data_tipo_doc"
        self.fields['tipo_documento'].widget.attrs['data-style'] = "slc-c-c"
        self.fields['tipo_documento'].widget.attrs['data-size'] = "5"
        self.fields['tipo_documento'].widget.attrs['data-name'] = "combo_tipo_doc"

        self.fields['proveedor'].widget.attrs['class'] = "proveedor numerico"
        self.fields['proveedor'].widget.attrs['placeholder'] = "RUC..."
        self.fields['num_autorizacion'].widget.attrs['maxlength'] = "37"
        self.fields['num_autorizacion'].widget.attrs['class'] = "num_autorizacion numerico"
        self.fields['fecha_emision'].widget.attrs['class'] = "fecha_emision"
        self.fields['fecha_vencimiento'].widget.attrs['class'] = "fecha_vencimiento"
        self.fields['serie'].widget.attrs['class'] = "serie numerico"
        self.fields['serie'].widget.attrs["style"] = "width: 154px;"


        # Ancho de controles tabla detalle compra asiento
        self.fields['sustento_tributario'].widget.attrs['data-width'] = "100%"
        self.fields['sustento_tributario'].widget.attrs["style"] = "width: 100px;"

        self.fields['proveedor'].widget.attrs["style"] = "width: 100%;"
        self.fields['proveedor'].widget.attrs["style"] = "min-width:120px;"
        self.fields['proveedor'].widget.attrs['maxlength'] = "13"


        self.fields['tipo_documento'].widget.attrs['data-width'] = "100%"


        self.fields['fecha_emision'].widget.attrs["style"] = "width: 100px;"
        self.fields['fecha_vencimiento'].widget.attrs["style"] = "width: 100px;"


        self.fields['fecha_emision'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_vencimiento'].widget.attrs['data-date-format'] = "yyyy-mm-dd"




        self.fields['num_autorizacion'].widget.attrs["style"] = "width:100px;"

        self.fields['cuenta'].widget.attrs['class'] = "cuenta"
        self.fields['cuenta'].widget.attrs['placeholder'] = "0.0.00.00.000 - Cuenta"
        self.fields['cuenta'].widget.attrs['style'] = "width: 230px;"

        #self.fields['bs'].widget.attrs['class'] = "b_s"
        #self.fields['bs'].widget.attrs['class']="selectpicker b_s"
        #self.fields['bs'].widget.attrs['data-style']="slc-b-s"
        #self.fields['bs'].widget.attrs['data-width'] = "80px"

        self.fields['cto_gto'].widget.attrs['class'] = "b_s"
        self.fields['cto_gto'].widget.attrs['class'] = "selectpicker b_s cto_gto"
        self.fields['cto_gto'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['cto_gto'].widget.attrs['data-width'] = "102px"

        self.fields['valor'].widget.attrs['class'] = "numerico valor"
        self.fields['valor'].widget.attrs['placeholder']="     0.00"
        self.fields['valor'].widget.attrs['style'] = "width: 80px"

        self.fields['porc_iva'].choices = [(x.codigo + "-" + str(x.porcentaje) + "-" + str(x.id), str(x.porcentaje) + "% - " +x.codigo+" - "+ x.descripcion) for x in Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion=VigenciaRetencionSRI.objects.filter(tipo_codigo_sri=Tipos_Retencion.objects.filter(status=1).get(id=2)).get(fecha_final__year=9999).id).order_by("codigo")]
        self.fields['porc_iva'].widget.attrs['data-size'] = "5"
        self.fields['porc_iva'].widget.attrs["style"] = " width: 100px;"
        self.fields['porc_iva'].widget.attrs['data-width'] = "100px"
        self.fields['porc_iva'].widget.attrs['class'] = "selectpicker cod_porc_iva"
        self.fields['porc_iva'].widget.attrs['data-style'] = "slc-b-s combo_porc_iva"
        self.fields['porc_iva'].widget.attrs['data-name'] = "combo_porc_iva"


