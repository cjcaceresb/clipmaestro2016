#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from contabilidad.models import *
from django.utils.html import *




class LoginForm(forms.Form):
    username = forms.CharField( min_length=3, max_length=100,label="Usuario")
    password = forms.CharField(widget=forms.PasswordInput(render_value=False), label="Clave")
    next = forms.CharField(min_length=3, max_length=100, required=False)

    def getUsername(self):
        return self.data["username"]

    def getPassword(self):
        return self.data["password"]

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['next'].label = ""
        self.fields['next'].widget.attrs['style'] = "display: none"


