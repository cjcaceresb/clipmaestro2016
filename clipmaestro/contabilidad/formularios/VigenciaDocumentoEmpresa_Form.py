#! /usr/bin/python
# -*- coding: UTF-8 -*-

from django.forms.util import ErrorList
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
from administracion.models import *
from django.forms import *
from django.db.models import Q
from contabilidad.models import *
from librerias.funciones.funciones_vistas import emite_docs_electronicos

__author__ = 'Clip Maestro'

mensaje_ayuda_sec_actual = u"La secuencia actual es el número en que se " \
                           u"encuentra en uso el block de documento. Ej: si " \
                           u"el # de factura que le toca es la 000000001, la " \
                           u"actual sería 0, si es 000000021, la actual sería " \
                           u"20"

mensaje_ayuda_sec_ini = u"La secuencia inicial es el número en que inicia " \
                        u" el block de documento. Ej: si " \
                        u"el # de factura que inicia es 10, la " \
                        u"secuencia inicial sería 10. A excepción que comience en 1, " \
                        u"ahí la secuencia inicial sería 0"

def validate_serie(value):
    try:
        if(value.index(' ') != None):
            raise ValidationError(u'No es permitido porque tiene espacios en blanco')
    except ValueError:
        return True

def validate_autorizacion(value):
    try:
        if(value.index(' ')!=None):
            raise ValidationError(u'No es permitido porque tiene espacios en blanco')
    except ValueError:
        pass


class VigenciaRetencionEmpresa_Form(forms.Form):
    serie = CharField(max_length=7, validators=[validate_serie], label=u"# Serie")
    tipo_documento = ChoiceField(choices=[], label=u"Tipo de Doc.")
    sec_ini = IntegerField(max_value=99999999, label=u"Sec. Inicial")
    sec_final = IntegerField(max_value=999999999, label=u"Sec. Final")
    sec_actual = IntegerField(max_value=999999999, label=u"Sec. Actual")
    autorizacion = CharField(max_length=37, validators=[validate_autorizacion], label=u"Autorización")
    fecha_emision = DateField(label=u"F. Emisión", widget=DateInput(format=("%Y-%m-%d")))
    fecha_vencimiento = DateField(label=u"F. Vencimiento", widget=DateInput(format=("%Y-%m-%d")))

    def getSerie(self):
        return self.data['serie']

    def getTipoDoc(self):
        return self.data['tipo_documento']

    def getSecIni(self):
        return self.cleaned_data['sec_ini']

    def getAutorizacion(self):
        return self.data['autorizacion']

    def getSec_Final(self):
        return self.cleaned_data['sec_final']

    def getSec_Actual(self):
        return self.cleaned_data['sec_actual']

    def getFechaVencimiento(self):
        return self.cleaned_data['fecha_vencimiento']

    def getFechaEmision(self):
        return self.cleaned_data['fecha_emision']

    def __init__(self, *args, **kwargs):
        super(VigenciaRetencionEmpresa_Form, self).__init__(*args, **kwargs)
        self.fields['sec_ini'].widget.attrs['class'] = "numerico"
        self.fields['serie'].widget.attrs['class'] = "numerico"
        self.fields['serie'].widget.attrs['placeholder'] = "00X-00X"
        self.fields['autorizacion'].widget.attrs['class'] = "numerico"
        self.fields['autorizacion'].widget.attrs['style'] = "width: 100%"

        self.fields['sec_ini'].widget.attrs['class'] = "numerico"
        self.fields['sec_ini'].widget.attrs['data-toggle'] = "popover"
        self.fields['sec_ini'].widget.attrs['title'] = "Ayuda"
        self.fields['sec_ini'].widget.attrs['data-content'] = mensaje_ayuda_sec_ini

        self.fields['sec_final'].widget.attrs['class'] = "numerico"
        self.fields['sec_final'].widget.attrs['style'] = "width: 100%"
        self.fields['sec_actual'].widget.attrs['class'] = "numerico"
        self.fields['sec_actual'].widget.attrs['style'] = "width: 100%"

        self.fields['fecha_vencimiento'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_vencimiento'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_vencimiento'].widget.attrs['text-align'] = "left"
        self.fields['fecha_vencimiento'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"
        self.fields['fecha_vencimiento'].widget.attrs['style'] = "width: 100%"

        self.fields['sec_actual'].widget.attrs['style'] = "width: 100%"
        self.fields['sec_actual'].widget.attrs['data-toggle'] = "popover"
        self.fields['sec_actual'].widget.attrs['title'] = "Ayuda"
        self.fields['sec_actual'].widget.attrs['data-content'] = mensaje_ayuda_sec_actual

        self.fields['sec_actual'].widget.attrs['data-placement'] = "bottom"

        self.fields['fecha_emision'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_emision'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_emision'].widget.attrs['text-align'] = "left"
        self.fields['fecha_emision'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"

        self.fields['tipo_documento'].choices = [(x.id, x.codigo_documento + " - " + x.descripcion_documento) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]
        self.fields['tipo_documento'].widget.attrs['data-live-search'] = True
        self.fields['tipo_documento'].widget.attrs['class'] = "selectpicker show-tick numerico"
        self.fields['tipo_documento'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_documento'].widget.attrs['data-width'] = "100%"


class DocumentosElectronicosForm(forms.Form):
    tipo_documento = ChoiceField(choices=[], label=u"Tipo de Doc.")
    establecimiento = CharField(max_length=3, label=u"Establecimiento")
    punto_emision = CharField(max_length=3, label=u"Punto de Emisión")
    sec_actual = IntegerField(max_value=99999999, label=u"Sec. Actual")
    fecha_emision_elect = DateField(label=u"F. Emisión", widget=DateInput(format=("%Y-%m-%d")))

    def get_establecimiento(self):
        return self.cleaned_data['establecimiento']

    def get_punto_emision(self):
        return self.cleaned_data['punto_emision']

    def get_tipo_documento(self):
        return self.cleaned_data['tipo_documento']

    def get_sec_actual(self):
        return self.cleaned_data['sec_actual']

    def get_fecha_emision(self):
        return self.cleaned_data['fecha_emision_elect']

    def clean(self):
        """
            Función para validar el formulario
        """
        establecimiento = self.cleaned_data.get('establecimiento')
        punto_emision = self.cleaned_data.get('punto_emision')
        sec_actual = self.cleaned_data.get('sec_actual')
        fecha_emision_elect = self.cleaned_data.get('fecha_emision_elect')

        # Valida si el usuario ingresó un # establecimiento válido
        try:
           est_num = int(establecimiento)
        except (ValueError, TypeError):
            est_num = 0

        try:
           pto_num = int(punto_emision)
        except (ValueError, TypeError):
            pto_num = 0

        if est_num <= 0:
            self._errors["establecimiento"] = u"Por favor verifíque este campo"

        if pto_num <= 0:
            self._errors["punto_emision"] = u"Por favor verifíque este campo"

        if sec_actual < 0:
            self._errors["sec_actual"] = u"Por favor verifíque este campo"
        if fecha_emision_elect is not None:
            if fecha_emision_elect > datetime.datetime.now().date():
                self._errors["fecha_emision_elect"] = u"Por favor verifíque este campo"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(DocumentosElectronicosForm, self).__init__(*args, **kwargs)
        self.fields['sec_actual'].widget.attrs['class'] = "numerico"
        self.fields['sec_actual'].widget.attrs['data-toggle'] = "popover"
        self.fields['sec_actual'].widget.attrs['title'] = "Ayuda"
        self.fields['sec_actual'].widget.attrs['data-content'] = mensaje_ayuda_sec_actual

        self.fields['sec_actual'].widget.attrs['data-placement'] = "bottom"

        self.fields['establecimiento'].widget.attrs['class'] = "numerico"
        self.fields['establecimiento'].widget.attrs['placeholder'] = "00X"

        self.fields['punto_emision'].widget.attrs['class'] = "numerico"
        self.fields['punto_emision'].widget.attrs['placeholder'] = "00X"

        self.fields['sec_actual'].widget.attrs['class'] = "numerico"

        self.fields['fecha_emision_elect'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_emision_elect'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_emision_elect'].widget.attrs['text-align'] = "left"
        self.fields['fecha_emision_elect'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"

        self.fields['tipo_documento'].choices = [(x.id, x.codigo_documento + " - " + x.descripcion_documento) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]
        self.fields['tipo_documento'].widget.attrs['data-live-search'] = True
        self.fields['tipo_documento'].widget.attrs['class'] = "selectpicker show-tick numerico"
        self.fields['tipo_documento'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_documento'].widget.attrs['data-width'] = "100%"


class VigenciaRetencionEmpresaEditar_Form(forms.Form):
    serie = CharField(max_length=7, label="Serie")
    tipo_documento = ChoiceField(choices=[], label=u"Tipo de Doc.")
    sec_ini = IntegerField(max_value=99999999, label=u"Sec. Inicial")
    sec_final = IntegerField(max_value=999999999, label=u"Sec. Final")
    sec_actual = IntegerField(max_value=999999999, label=u"Sec. Actual")
    autorizacion = CharField(max_length=37, label=u"Autorización")
    fecha_emision = DateTimeField(label=u"Fecha de Emi.")
    fecha_vencimiento = DateTimeField(label=u"Fecha de Ven.")

    def getSerie(self):
        return self.data['serie']

    def getTipoDoc(self):
        return self.data['tipo_documento']

    def getSecIni(self):
        return self.cleaned_data['sec_ini']

    def getAutorizacion(self):
        return self.data['autorizacion']

    def getSec_Final(self):
        return self.cleaned_data['sec_final']

    def getSec_Actual(self):
        return self.cleaned_data['sec_actual']

    def getFechaVencimiento(self):
        return self.data['fecha_vencimiento']

    def getFechaEmision(self):
        return self.data['fecha_emision']

    def __init__(self, *args, **kwargs):
        super(VigenciaRetencionEmpresaEditar_Form, self).__init__(*args, **kwargs)
        self.fields['serie'].widget.attrs['class'] = "numerico"
        self.fields['serie'].widget.attrs['placeholder'] = "00X-00X"
        self.fields['autorizacion'].widget.attrs['class'] = "numerico"

        self.fields['sec_ini'].widget.attrs['class'] = "numerico"
        self.fields['sec_ini'].widget.attrs['data-toggle'] = "popover"
        self.fields['sec_ini'].widget.attrs['title'] = "Ayuda"
        self.fields['sec_ini'].widget.attrs['data-content'] = mensaje_ayuda_sec_ini

        self.fields['sec_final'].widget.attrs['class'] = "numerico"
        self.fields['sec_actual'].widget.attrs['class'] = "numerico"
        self.fields['sec_actual'].widget.attrs['style'] = "width: 100%"
        self.fields['sec_actual'].widget.attrs['data-toggle'] = "popover"
        self.fields['sec_actual'].widget.attrs['title'] = "Ayuda"
        self.fields['sec_actual'].widget.attrs['data-content'] = mensaje_ayuda_sec_actual

        self.fields['sec_actual'].widget.attrs['data-placement'] = "bottom"

        self.fields['fecha_vencimiento'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_vencimiento'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_vencimiento'].widget.attrs['text-align'] = "left"
        self.fields['fecha_vencimiento'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"

        self.fields['fecha_emision'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_emision'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_emision'].widget.attrs['text-align'] = "left"
        self.fields['fecha_emision'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"

        self.fields['tipo_documento'].choices = [(x.id, x.codigo_documento + " - " + x.descripcion_documento) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]
        self.fields['tipo_documento'].widget.attrs['data-live-search'] = True
        self.fields['tipo_documento'].widget.attrs['class'] = "selectpicker show-tick numerico"
        self.fields['tipo_documento'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_documento'].widget.attrs['data-width'] = "100%"


class BuscadorDocEmpresa(forms.Form):
    tipo_doc = ChoiceField(choices=[], required=False, label=u"Tipo de Doc.")
    serie = CharField(max_length=7, required=False, label=u"Serie")
    fecha_emision = DateTimeField(required=False, label=u"F. Emisión")
    fecha_vencimiento = DateTimeField(required=False, label=u"F. Vencimiento")
    autorizacion = CharField(max_length=37, required=False, label=u"Autorización")
    estado = ChoiceField(choices=[], label=u"Estado", required=False)
    page = IntegerField()

    def getTipoDoc(self):
        try:
            value = self.data["tipo_doc"]
            return Documento.objects.get(id=value)
        except:
            return None

    def getSerie(self):
        try:
            value = self.data["serie"]
            return value
        except:
            return ""

    def getEstado(self):
        try:
            value = self.data["estado"]
            return value
        except:
            return ""

    def getFechaEmision(self):
        try:
            value = self.data["fecha_emision"]
            split_fecha = str(value).split("-")
            return datetime.datetime(int(split_fecha[0]), int(split_fecha[1]), int(split_fecha[2]))
        except:
            return None

    def getFechaVencimiento(self):
        try:
            value = self.data["fecha_vencimiento"]
            split_fecha = str(value).split("-")
            return datetime.datetime(int(split_fecha[0]), int(split_fecha[1]), int(split_fecha[2]))
        except:
            return None

    def getAutorizacion(self):
        try:
            value = self.data["autorizacion"]
            return value
        except:
            return ""


    def __init__(self, *args, **kwargs):
        super(BuscadorDocEmpresa, self).__init__(*args, **kwargs)
        self.fields['tipo_doc'].choices = [('', '')] + [(x.id,x.codigo_documento+' - '+ x.descripcion_documento[0:60]) for x in Documento.objects.filter(status=1)]
        #self.fields['tipo_doc'].choices.widget.attrs['placeholder'] = [('', '')] + [(x.id, x.descripcion_documento[0:60]) for x in Documento.objects.filter(status=1)]

        self.fields['tipo_doc'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_doc'].widget.attrs['data-live-search'] = True
        self.fields['tipo_doc'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_doc'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_doc'].widget.attrs['title'] = "Seleccione un Documento"

        self.fields['serie'].widget.attrs['placeholder'] = "00X-00X"
        self.fields['serie'].widget.attrs['style'] = "width:100%"
        self.fields['autorizacion'].widget.attrs['style'] = "width:100%"

        self.fields['fecha_emision'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha_emision'].widget.attrs['placeholder']="aaaa-mm-dd"
        self.fields['fecha_emision'].widget.attrs['class'] = " datepicker control-fecha txt-100pc input-calendario numerico"

        self.fields['fecha_vencimiento'].widget.attrs['data-date-format']="yyyy-mm-dd"
        if emite_docs_electronicos():
            self.fields['fecha_vencimiento'].widget.attrs['disabled'] = "disabled"

        self.fields['fecha_vencimiento'].widget.attrs['placeholder']="aaaa-mm-dd"
        self.fields['fecha_vencimiento'].widget.attrs['class'] = "datepicker control-fecha txt-100pc " \
                                                                 "input-calendario numerico deshabilitado"

        self.fields['estado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado'].choices = [('', '')] + [(x.id, x.descripcion[0:30]) for x in Estados.objects.filter(status=1)]
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado'].widget.attrs['data-width'] = "100%"
        self.fields['estado'].widget.attrs['title'] = "Seleccione un estado"

        self.fields['page'].widget.attrs['style'] = "display: none"
