from django.core.exceptions import ValidationError
from django import forms
from contabilidad.models import *
__author__ = 'Clip Maestro'

class RetencionCuentaFormulario(forms.Form):
    id = forms.IntegerField()
    nombre_cta = forms.CharField(max_length=50, required=False)
    tipo = forms.ChoiceField(choices=[])
    porcentaje = forms.FloatField(required=False, max_value=100)

    def getId(self):
        return self.data["id"]
    def getNombreCta(self):
        return self.data["nombre_cta"]
    def getPorcentaje(self):
        return self.data["porcentaje"]
    def getTipo(self):
        return self.data["tipo"]

    def __init__(self, *args, **kwargs):
        super(RetencionCuentaFormulario, self).__init__(*args, **kwargs)
        self.fields['tipo'].choices = [(x.id, x.descripcion) for x in Tipos_Retencion.objects.exclude(Q(id=2) | Q(id=3)).filter(status=1).order_by("descripcion")]
        self.fields['id'].widget.attrs['style']="display:none"
        self.fields['nombre_cta'].widget.attrs['disabled'] = True
        self.fields['nombre_cta'].widget.attrs['class'] = "nom_cta"
        self.fields['porcentaje'].widget.attrs['class'] = "numerico"
        self.fields['tipo'].widget.attrs['class']= "selectpicker"
        self.fields['tipo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo'].widget.attrs['data-width'] = "250px"