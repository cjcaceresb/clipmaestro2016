#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from django.db import connection
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from datetime import date, timedelta
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from django.utils.html import *
from django.forms.formsets import BaseFormSet

solo_numeros = u'^[0-9]+$'

def validate_valor(value):
    valor = float(value)
    if valor <= 0:
        raise ValidationError(u'Ingrese un valor superior a 0.00')
    else:
        return True

class FormProvisionVenta(forms.Form):
    tipo_documento = forms.ChoiceField(label=u"Tipo Doc.")
    serie = forms.ChoiceField(choices=[], label=u"Serie")
    fecha = forms.DateField(label=u"Fecha")
    utlimo_doc_emititdo = forms.IntegerField(label=u"Último Doc. Emitido")
    utlimo_comp_emititdo = forms.IntegerField(label=u"Último Comprobante")
    cantidad_prov = forms.FloatField(label=u"# Doc. a provisionar", validators=[validate_valor])
    num_inicio_doc = forms.IntegerField(label=u"Desde", validators=[validate_valor])
    num_fin_doc = forms.IntegerField(label=u"# Hasta", validators=[validate_valor])
    num_inicio_comp = forms.IntegerField(label=u"Desde", validators=[validate_valor])
    num_fin_comp = forms.IntegerField(label=u"# Hasta", validators=[validate_valor])

    def getSerie(self):
       return self.data["serie"]

    def getTipoDoc(self):
       return self.data["tipo_documento"]

    def getNumIniDoc(self):
       return self.data["num_inicio_doc"]

    def getNumFinDoc(self):
       return self.data["num_fin_doc"]

    def getNumIniComp(self):
       return self.data["num_inicio_comp"]

    def getNumFinComp(self):
       return self.data["num_fin_comp"]

    def getFecha(self):
       return self.cleaned_data["fecha"]

    def getUltimoDocEmi(self):
       return self.data["utlimo_doc_emititdo"]

    def getUltimoCompEmi(self):
       return self.data["utlimo_comp_emititdo"]

    def getCantidadDocPRov(self):
       return self.data["cantidad_prov"]


    def __init__(self, *args, **kwargs):
        super(FormProvisionVenta, self).__init__(*args, **kwargs)
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"

        self.fields['serie'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1, documento_sri__id=1).distinct().order_by("serie")]
        self.fields['serie'].widget.attrs['class'] = "selectpicker show-tick serie serieprovisionventa"
        self.fields['serie'].widget.attrs['data-style'] = "slc-b-s combo_serie"
        self.fields['serie'].widget.attrs['data-name'] = "combo_serie"
        self.fields['serie'].widget.attrs['data-size'] = "8"

        self.fields['tipo_documento'].choices = [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in Documento.objects.filter(status=1).filter(vigenciadocempresa__fecha_emi__lte=datetime.datetime.now(), vigenciadocempresa__fecha_vencimiento__gte=datetime.datetime.now(), venta=True).filter(Q(codigo_documento="01")| Q(codigo_documento="02")| Q(codigo_documento="04")).order_by("codigo_documento").distinct()]
        self.fields['tipo_documento'].widget.attrs['class'] = "selectpicker show-tick tipo_doc"
        self.fields['tipo_documento'].widget.attrs['data-style'] = "slc-b-s tipo_doc"
        self.fields['tipo_documento'].widget.attrs['data-name'] = "combo_tipo_doc"
        self.fields['tipo_documento'].widget.attrs['data-size'] = "8"

        self.fields['cantidad_prov'].widget.attrs['class'] = "numerico cantidad_prov"
        self.fields['cantidad_prov'].widget.attrs['placeholder'] = "Cantidad de número de documento a provisionar, Ej: 5"
        self.fields['cantidad_prov'].widget.attrs['maxlength'] = "6"

        self.fields['utlimo_doc_emititdo'].widget.attrs['class'] = "numerico utlimo_doc_emititdo"
        self.fields['utlimo_doc_emititdo'].widget.attrs['readonly'] = True
        self.fields['utlimo_doc_emititdo'].widget.attrs['maxlength'] = "6"
        self.fields['utlimo_comp_emititdo'].widget.attrs['class'] = "numerico utlimo_comp_emititdo"
        self.fields['utlimo_comp_emititdo'].widget.attrs['readonly'] = True
        self.fields['utlimo_comp_emititdo'].widget.attrs['maxlength'] = "6"
        self.fields['num_inicio_doc'].widget.attrs['class'] = "numerico num_inicio_doc"
        self.fields['num_inicio_doc'].widget.attrs['readonly'] = True
        self.fields['num_inicio_doc'].widget.attrs['maxlength'] = "6"
        self.fields['num_fin_doc'].widget.attrs['maxlength'] = "6"
        self.fields['num_fin_doc'].widget.attrs['readonly'] = True
        self.fields['num_fin_doc'].widget.attrs['class'] = "numerico num_fin_doc"
        self.fields['num_inicio_comp'].widget.attrs['class'] = "numerico num_inicio_comp"
        self.fields['num_inicio_comp'].widget.attrs['readonly'] = True
        self.fields['num_inicio_comp'].widget.attrs['maxlength'] = "6"
        self.fields['num_fin_comp'].widget.attrs['maxlength'] = "6"
        self.fields['num_fin_comp'].widget.attrs['readonly'] = True
        self.fields['num_fin_comp'].widget.attrs['class'] = "numerico num_fin_comp"

class FormProvisionCompra(forms.Form):
    serie = forms.ChoiceField(choices=[], label=u"Serie", required=False)
    fecha = forms.DateField(label=u"Fecha:")
    utlimo_doc_emititdo = forms.IntegerField(label=u"Último Doc. Ret. Emitido", required=False)
    utlimo_comp_emititdo = forms.IntegerField(label=u"Último Comprobante")
    cantidad_prov = forms.FloatField(label=u"# Doc. Ret. a provisionar", validators=[validate_valor])

    num_inicio_doc = forms.IntegerField(label=u"Desde", validators=[validate_valor], required=False)
    num_fin_doc = forms.IntegerField(label=u"# Hasta", validators=[validate_valor], required=False)
    num_inicio_comp = forms.IntegerField(label=u"Desde", validators=[validate_valor])
    num_fin_comp = forms.IntegerField(label=u"# Hasta", validators=[validate_valor])

    def getSerie(self):
       return self.data["serie"]

    def getNumIniDoc(self):
       return self.data["num_inicio_doc"]

    def getNumFinDoc(self):
       return self.data["num_fin_doc"]

    def getNumIniComp(self):
       return self.data["num_inicio_comp"]

    def getNumFinComp(self):
       return self.data["num_fin_comp"]

    def getFecha(self):
       return self.cleaned_data["fecha"]

    def getUltimoDocEmi(self):
       return self.data["utlimo_doc_emititdo"]

    def getUltimoCompEmi(self):
       return self.data["utlimo_comp_emititdo"]

    def getCantidadDocPRov(self):
       return self.data["cantidad_prov"]


    def __init__(self, *args, **kwargs):
        super(FormProvisionCompra, self).__init__(*args, **kwargs)
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['style'] = "width: 100%;"

        self.fields['serie'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1, documento_sri__id=24, sec_actual__lt=F("sec_final")).order_by("serie")]
        self.fields['serie'].widget.attrs['class'] = "selectpicker show-tick serie serieprovisionventa"
        self.fields['serie'].widget.attrs['data-style'] = "slc-b-s combo_serie"
        self.fields['serie'].widget.attrs['data-name'] = "combo_serie"
        self.fields['serie'].widget.attrs['data-size'] = "8"

        self.fields['cantidad_prov'].widget.attrs['class'] = "numerico cantidad_prov"
        self.fields['cantidad_prov'].widget.attrs['maxlength'] = "6"

        self.fields['utlimo_doc_emititdo'].widget.attrs['class'] = "numerico utlimo_doc_emititdo"
        self.fields['utlimo_doc_emititdo'].widget.attrs['readonly'] = True
        self.fields['utlimo_doc_emititdo'].widget.attrs['maxlength'] = "6"

        self.fields['utlimo_comp_emititdo'].widget.attrs['class'] = "numerico utlimo_comp_emititdo"
        self.fields['utlimo_comp_emititdo'].widget.attrs['readonly'] = True
        self.fields['utlimo_comp_emititdo'].widget.attrs['maxlength'] = "6"

        self.fields['num_inicio_doc'].widget.attrs['class'] = "numerico num_inicio_doc"
        self.fields['num_inicio_doc'].widget.attrs['readonly'] = True
        self.fields['num_inicio_doc'].widget.attrs['maxlength'] = "6"

        self.fields['num_fin_doc'].widget.attrs['maxlength'] = "6"
        self.fields['num_fin_doc'].widget.attrs['readonly'] = True
        self.fields['num_fin_doc'].widget.attrs['class'] = "numerico num_fin_doc"

        self.fields['num_inicio_comp'].widget.attrs['class'] = "numerico num_inicio_comp"
        self.fields['num_inicio_comp'].widget.attrs['readonly'] = True
        self.fields['num_inicio_comp'].widget.attrs['maxlength'] = "6"

        self.fields['num_fin_comp'].widget.attrs['maxlength'] = "6"
        self.fields['num_fin_comp'].widget.attrs['readonly'] = True
        self.fields['num_fin_comp'].widget.attrs['class'] = "numerico num_fin_comp"


class FormProvisionCheque(forms.Form):
    cuenta_banco = forms.ChoiceField(choices=[], label=u"Cuenta Bancaria")
    fecha = forms.DateField(label=u"Fecha:")
    cantidad_prov = forms.FloatField(label=u"# de Cheques a Provisionar", validators=[validate_valor])
    utlimo_doc_emititdo = forms.IntegerField(label=u"Último Cheque Emitido", required=False)
    utlimo_comp_emititdo = forms.IntegerField(label=u"Último Comprobante")

    num_inicio_doc = forms.IntegerField(label=u"Desde", validators=[validate_valor], required=False)
    num_fin_doc = forms.IntegerField(label=u"# Hasta", validators=[validate_valor], required=False)
    num_inicio_comp = forms.IntegerField(label=u"Desde", validators=[validate_valor])
    num_fin_comp = forms.IntegerField(label=u"# Hasta", validators=[validate_valor])


    def getCuentaBanco(self):
       return self.data["cuenta_banco"]

    def getNumIniDoc(self):
       return self.data["num_inicio_doc"]

    def getNumFinDoc(self):
       return self.data["num_fin_doc"]

    def getNumIniComp(self):
       return self.data["num_inicio_comp"]

    def getNumFinComp(self):
       return self.data["num_fin_comp"]

    def getFecha(self):
       return self.cleaned_data["fecha"]

    def getUltimoDocEmi(self):
       return self.data["utlimo_doc_emititdo"]

    def getUltimoCompEmi(self):
       return self.data["utlimo_comp_emititdo"]

    def getCantidadDocPRov(self):
       return self.data["cantidad_prov"]

    def __init__(self, *args, **kwargs):
        super(FormProvisionCheque, self).__init__(*args, **kwargs)

        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['style'] = "width: 100%;"

        self.fields['cuenta_banco'].choices = [(x.id, x.plan_cuenta.codigo+"-"+x.descripcion) for x in Cuenta_Banco.objects.filter(status=1).filter(tipo_cuenta_banco__id=2).order_by("descripcion")]
        self.fields['cuenta_banco'].widget.attrs['class'] = "selectpicker show-tick "
        self.fields['cuenta_banco'].widget.attrs['data-style'] = "slc-b-s c"

        self.fields['cantidad_prov'].widget.attrs['class'] = "numerico cantidad_prov"
        self.fields['cantidad_prov'].widget.attrs['placeholder'] = "Cantidad de número de cheques a provisionar, Ej: 5"
        self.fields['cantidad_prov'].widget.attrs['maxlength'] = "6"

        self.fields['utlimo_doc_emititdo'].widget.attrs['class'] = "numerico utlimo_doc_emititdo"
        self.fields['utlimo_doc_emititdo'].widget.attrs['readonly'] = True
        self.fields['utlimo_doc_emititdo'].widget.attrs['maxlength'] = "6"

        self.fields['utlimo_comp_emititdo'].widget.attrs['class'] = "numerico utlimo_comp_emititdo"
        self.fields['utlimo_comp_emititdo'].widget.attrs['readonly'] = True
        self.fields['utlimo_comp_emititdo'].widget.attrs['maxlength'] = "6"

        self.fields['num_inicio_doc'].widget.attrs['class'] = "numerico num_inicio_doc"
        self.fields['num_inicio_doc'].widget.attrs['readonly'] = True
        self.fields['num_inicio_doc'].widget.attrs['maxlength'] = "6"

        self.fields['num_fin_doc'].widget.attrs['maxlength'] = "6"
        self.fields['num_fin_doc'].widget.attrs['readonly'] = True
        self.fields['num_fin_doc'].widget.attrs['class'] = "numerico num_fin_doc"

        self.fields['num_inicio_comp'].widget.attrs['class'] = "numerico num_inicio_comp"
        self.fields['num_inicio_comp'].widget.attrs['readonly'] = True
        self.fields['num_inicio_comp'].widget.attrs['maxlength'] = "6"

        self.fields['num_fin_comp'].widget.attrs['maxlength'] = "6"
        self.fields['num_fin_comp'].widget.attrs['readonly'] = True
        self.fields['num_fin_comp'].widget.attrs['class'] = "numerico num_fin_comp"

