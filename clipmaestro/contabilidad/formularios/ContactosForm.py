#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *
from django.forms.formsets import BaseFormSet
from django.core.validators import validate_email


__author__ = 'Roberto'

class ContactoFormSetOnlyRead(forms.Form):
    nombres = forms.CharField(label="Nombres", required=False)
    apellidos = forms.CharField(label="Apellidos", required=False)
    email = forms.EmailField(label="Email", required=False)
    telefono_contacto = forms.CharField(label=u"Teléfono", required=False)
    movil = forms.CharField(label=u"Móvil", required=False)
    departamento = forms.ChoiceField(label="Departamento", required=False)

    def __init__(self, *args, **kwargs):
        super(ContactoFormSetOnlyRead, self).__init__(*args, **kwargs)
        self.fields['departamento'].choices = [("", "")]+[(x.id, x.descripcion) for x in Departamento.objects.filter(status=1)]
        self.fields['departamento'].widget.attrs['class'] = "selectpicker departamento"
        self.fields['departamento'].widget.attrs['data-style'] = "slc-primary"
        self.fields['nombres'].widget.attrs['class'] = "nombres"
        self.fields['apellidos'].widget.attrs['class'] = "apellidos"
        self.fields['email'].widget.attrs['class'] = "email"
        self.fields['telefono_contacto'].widget.attrs['class'] = "numerico telefono_contacto"
        self.fields['movil'].widget.attrs['class'] = "movil"
        self.fields['telefono_contacto'].widget.attrs['maxlength'] = "15"
        self.fields['movil'].widget.attrs['maxlength'] = "15"
        self.fields['nombres'].widget.attrs['maxlength'] = "60"
        self.fields['apellidos'].widget.attrs['maxlength'] = "60"
        self.fields['email'].widget.attrs['maxlength'] = "30"
        self.fields['email'].widget.attrs['placeholder'] = "email@ejemplo.com"
        self.fields['movil'].widget.attrs['placeholder'] = "(99)-9999-9999"
        self.fields['telefono_contacto'].widget.attrs['readonly'] = True
        self.fields['departamento'].widget.attrs['readonly'] = True
        self.fields['departamento'].widget.attrs['disabled'] = True
        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['movil'].widget.attrs['readonly'] = True
        self.fields['apellidos'].widget.attrs['readonly'] = True
        self.fields['nombres'].widget.attrs['readonly'] = True


# Clase que me ayuda a validar los formset vacios
class RequiredFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(RequiredFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

class ContactoFormSet(BaseFormSet):
    def clean(self):
        """Checks that no two articles have the same title."""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return
        for form in self.forms:
            if form.cleaned_data.get("nombres", "") != "" and form.cleaned_data.get("apellidos", "") != "":
                nombres = (unicode(form.cleaned_data.get("nombres", ""))).encode("utf-8")
                apellidos = (unicode(form.cleaned_data.get("apellidos", ""))).encode("utf-8")

                if not ((nombres.isspace()) and (apellidos.isspace())):
                    if form.cleaned_data.get("email", "") == "":
                        errors = form._errors.setdefault("email", ErrorList())
                        errors.append(u"Ingrese el correo")
                else:
                    errors = form._errors.setdefault("nombres", ErrorList())
                    errors.append(u"Ingrese un nombre de contacto")
                    errors = form._errors.setdefault("apellidos", ErrorList())
                    errors.append(u"Ingrese un apellido de contacto")

class FormContacto(forms.Form):
    nombres = forms.CharField(label="Nombres", required=False)
    apellidos = forms.CharField(label="Apellidos", required=False)
    email = forms.EmailField(label="Email", required=False)
    telefono_contacto = forms.CharField(label=u"Teléfono", required=False)
    movil = forms.CharField(label=u"Móvil", required=False)
    departamento = forms.ChoiceField(label="Departamento", required=False)


    def getNombres(self):
        return self.data["nombre"]
    def getApellidos(self):
        return self.data["apellidos"]
    def getEmail(self):
        return self.data["email"]
    def getTelefonoContacto(self):
        return self.data["telefono_contacto"]
    def getMovil(self):
        return self.data["movil"]
    def getDepartamento(self):
        return self.data["departamento"]

    def __init__(self, *args, **kwargs):
        super(FormContacto, self).__init__(*args, **kwargs)
        self.fields['departamento'].choices = [(x.id, x.descripcion) for x in Departamento.objects.filter(status=1)]
        self.fields['departamento'].widget.attrs['class'] = "selectpicker departamento"
        self.fields['departamento'].widget.attrs['data-style'] = "slc-primary"
        self.fields['nombres'].widget.attrs['class'] = "nombres"
        self.fields['apellidos'].widget.attrs['class'] = "apellidos"
        self.fields['email'].widget.attrs['class'] = "email"
        self.fields['telefono_contacto'].widget.attrs['class'] = "numerico telefono_contacto"
        self.fields['movil'].widget.attrs['class'] = "numerico movil"
        self.fields['telefono_contacto'].widget.attrs['maxlength'] = "15"
        self.fields['movil'].widget.attrs['maxlength'] = "15"
        self.fields['nombres'].widget.attrs['maxlength'] = "60"
        self.fields['apellidos'].widget.attrs['maxlength'] = "60"
        self.fields['email'].widget.attrs['maxlength'] = "100"
        self.fields['email'].widget.attrs['placeholder'] = "email@ejemplo.com"
        self.fields['movil'].widget.attrs['placeholder'] = "(99)-9999-9999"
