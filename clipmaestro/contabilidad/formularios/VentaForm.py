#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from django.db import connection
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from datetime import date, timedelta
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from librerias.funciones.funciones_vistas import *
from django.db.models import Q
from django.utils.html import *
from django.forms.formsets import BaseFormSet

tipo_descuento = (('', ''), ('1', '% - Porcentaje '), ('2', '$ - Monto'))
tipo_desc = (('', ''), ('1', 'Por Venta'), ('2', 'Por Producto'))
solo_alfanumericas = u'[a-zA-Z0-9_]+$'
def validate_cliente(value):
    try:
        cliente = unicode(value).encode("iso-8859-2", "replace")
        ruc_prov = str(cliente).split('-')[0].replace(" ", "")
        Cliente.objects.get(status=1, ruc=ruc_prov)
    except:
        raise ValidationError(u'El cliente que ingresó se encuentra inactivo o no existe')

class SelectItemParametros(forms.Select):
    allow_multiple_selected = False
    def __init__(self, attrs=None, choices=()):
        super(SelectItemParametros, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            try:
                item = Item.objects.get(id=option_value)
                id_item = item.id
                tipo_item = item.TipoItem()
                costo = Item.objects.get(id=option_value).costo
                iva = Item.objects.get(id=option_value).iva
                precio = Item.objects.get(id=option_value).precio1
                ice = Item.objects.get(id=option_value).porc_ice
                unidad = item.unidad.descripcion

                if costo is None:
                    costo = 0
                if iva is None:
                    iva = 0
                if precio is None:
                    precio = 0.0
                if ice is None:
                    ice = 0.0
            except:
                costo = 0.0
                iva = 0.0
                ice = 0.0
                precio = 0.0
                tipo_item = 0.0
                unidad = None
                id_item = None

            return format_html(u'<option value="{0}" {1} data-iva={3} data-precio={4} data-ice={5} '
                               u'data-unidad={6} data-tipo={7}> {8} </option>',
                               option_value,
                               selected_html,
                               costo,
                               iva,
                               precio,
                               ice,
                               unidad,
                               tipo_item,
                               unicode(force_text(option_label)))
        else:
            return format_html('<option value="{0}" {1}> {2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))

class FormVentasCabecera(forms.Form):
    fecha = forms.DateField(label=u"Fecha", widget=forms.DateInput(format=("%Y-%m-%d")))
    reembolso_gasto = forms.BooleanField(required=False, label=u"Reemb. Gasto")
    tipo_documento = forms.ChoiceField(choices=[], label=u"Tipo Doc.", required=False)
    serie = forms.ChoiceField(choices=[], required=False)
    numero_documento = forms.CharField(max_length=150, label=u"# Doc.", required=False)
    num_autorizacion = forms.CharField(label=u"# Autorización", required=False)
    cliente = forms.ChoiceField(choices=[], label=u"Cliente")
    vendedor = forms.ChoiceField(choices=[], label=u"Vendedor", required=False)
    direccion = forms.ChoiceField(choices=[], label=u"Dirección")
    nota = forms.CharField(label=u"Nota", required=False)
    select_desc = forms.BooleanField(required=False, label=u"Desc. Venta")
    descuento_porc = forms.ChoiceField(choices=tipo_descuento, label=u"Descuento", required=False)
    tipo_descuento = forms.ChoiceField(choices=tipo_desc, required=False)
    monto_descuento = forms.FloatField(label=u"Ingrese su Porc.Desc. ", required=False)
    contrato = forms.ChoiceField(label=u"Contrato", required=False)
    monto_descuento_doce = forms.FloatField(label=u"Ingrese su Desc", required=False)
    monto_descuento_cero = forms.FloatField(label=u"Ingrese su Desc", required=False)
    concepto = forms.CharField(label=u"Concepto", max_length=200, widget=forms.Textarea, required=False)
    guia_remision = forms.BooleanField(required=False, label=u"Generar Guía de Remisión")

    forma_pago = forms.ChoiceField(choices=[], required=False)
    plazo = forms.CharField(label=u"Plazo", required=False)
    fecha_vencimiento = forms.DateTimeField(label=u"Fecha V.", required=False)
    ################################### DESCUENTOS CABECERA #####################################
    def getDescuentoPorcentaje(self):
        return self.data["descuento_porc"]
    def getTipoDescuento(self):
        return self.data["tipo_descuento"]
    def getMontoDescuento(self):
        return self.data["monto_descuento"]
    def getMontoDescuentoCero(self):
        return self.data["monto_descuento_cero"]
    def getMontoDescuentoDoce(self):
        return self.data["monto_descuento_doce"]
    #############################################################################################

    ############################# Check Cabcerea #################################################
    def getReembolso(self):
        try:
            return self.data["reembolso_gasto"]
        except:
            return False
    def getAnularCheck(self):
        try:
            return self.data["anular_check"]
        except:
            return False
    def getGuiaRemision(self):
        try:
            value = self.data["guia_remision"]
            return value
        except:
            return False
    def getCheckDescuento(self):
        try:
            return self.data["select_desc"]
        except:
            return False
    ##############################################################################################

    def getDireccion(self):
        return self.data["direccion"]
    def getFecha(self):
        return self.cleaned_data["fecha"]
    def getTipoDoc(self):
        return self.data["tipo_documento"]
    def getSerie(self):
       return self.data["serie"]
    def getNumeroDoc(self):
        return self.data["numero_documento"]
    def getNumAutoriza(self):
       return self.data["num_autorizacion"]
    def getFechaVencimiento(self):
        return self.data["fecha_vencimiento"]
    def getCliente(self):
       return self.data["cliente"]
    def getFormaPago(self):
        return self.data["forma_pago"]
    def getConcepto(self):
       return self.data["concepto"]
    def getVendedor(self):
       return self.data["vendedor"]
    def getNota(self):
       return self.data["nota"]
    def getPlazo(self):
        return self.data["plazo"]
    def getContrato(self):
        try:
            value = self.cleaned_data["contrato"]
            return ContratoCabecera.objects.get(id=value)
        except (ContratoCabecera.DoesNotExist, ValueError):
            return None

    def clean(self):
        """
        Función para validar el formulario
        """
        if not emite_docs_electronicos():
            serie_vigente_doc = self.cleaned_data.get('serie', None)

            if serie_vigente_doc is None:
                self._errors["serie"] = u"Dato Requerido"
        else:
            pass
        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(FormVentasCabecera, self).__init__(*args, **kwargs)
        self.fields['tipo_documento'].choices = [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in Documento.objects.filter(status=1).
                                                                                                                filter(vigenciadocempresa__fecha_emi__lte=datetime.datetime.now(),
                                                                                                                       vigenciadocempresa__fecha_vencimiento__gte=datetime.datetime.now(),
                                                                                                                       venta=True).order_by("codigo_documento").distinct()]

        self.fields['tipo_documento'].widget.attrs['class'] = "selectpicker show-tick tipo_documento"
        self.fields['tipo_documento'].widget.attrs['data-style'] = "slc-b-s combo_tipo_doc"
        self.fields['tipo_documento'].widget.attrs['data-name'] = "combo_tipo_documento"
        self.fields['tipo_documento'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_documento'].widget.attrs['data-size'] = "8"

        self.fields['serie'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1, documento_sri__id=1).order_by("serie")]
        self.fields['serie'].widget.attrs['class'] = "selectpicker show-tick serie"
        self.fields['serie'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['serie'].widget.attrs['data-name'] = "combo_serie"
        self.fields['serie'].widget.attrs['data-width'] = "100%"

        self.fields['forma_pago'].choices = [(x.id, x.descripcion) for x in TipoPago.objects.filter(status=1).order_by("id")]
        self.fields['forma_pago'].widget.attrs['class']="selectpicker show-tick forma_pago"
        self.fields['forma_pago'].widget.attrs['data-style'] = "slc-b-s combo_forma_pago width: 102px;"
        self.fields['forma_pago'].widget.attrs['data-name'] = "combo_forma_pago"

        self.fields['descuento_porc'].widget.attrs['class']="selectpicker show-tick descuento_porc"
        self.fields['descuento_porc'].widget.attrs['data-style']="slc-b-s combo_descuento_porc"
        self.fields['descuento_porc'].widget.attrs['data-name']="combo_descuento_porc"
        self.fields['descuento_porc'].widget.attrs['data-width'] = "100%"

        self.fields['tipo_descuento'].widget.attrs['class']="selectpicker show-tick tipo_descuento"
        self.fields['tipo_descuento'].widget.attrs['data-style']="slc-b-s combo_tipo_descuento"
        self.fields['tipo_descuento'].widget.attrs['data-name']="combo_tipo_descuento"
        self.fields['tipo_descuento'].widget.attrs['data-width'] = "100%"

        self.fields['vendedor'].choices = [("", "")]+[(x.id, x.nombres_completos+" "+x.apellidos_completos)
                                                      for x in Colaboradores.objects.filter(status=1).order_by("apellidos_completos")]

        self.fields['vendedor'].widget.attrs['class'] = "selectpicker show-tick vendedor  "
        self.fields['vendedor'].widget.attrs['data-style'] = "slc-b-s combo_vendedor"
        self.fields['vendedor'].widget.attrs['data-name'] = "combo_vendedor"
        self.fields['vendedor'].widget.attrs['data-live-search'] = True

        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "calendario-gris"
        self.fields['fecha'].widget.attrs['data-width'] = "100%"
        self.fields['fecha'].widget.attrs['style'] = "width: 100%"

        self.fields['fecha_vencimiento'].widget.attrs['class'] = "calendario-gris"
        self.fields['num_autorizacion'].widget.attrs['class'] = "numerico"
        self.fields['monto_descuento'].widget.attrs['style'] = ""
        self.fields['monto_descuento_doce'].widget.attrs['style'] = ""
        self.fields['monto_descuento_cero'].widget.attrs['style'] = ""

        self.fields['numero_documento'].widget.attrs['class'] = "numerico"
        self.fields['numero_documento'].widget.attrs['placeholder'] = "000000000"
        self.fields['numero_documento'].widget.attrs['maxlength'] = "9"
        self.fields['numero_documento'].widget.attrs['data-width'] = "100%"

        self.fields['monto_descuento'].widget.attrs['class'] = "numerico"
        self.fields['monto_descuento'].widget.attrs['style'] = "text-align: left;"
        self.fields['monto_descuento'].widget.attrs['placeholder'] = "%       "

        self.fields['monto_descuento_doce'].widget.attrs['class'] = "numerico"
        self.fields['monto_descuento_doce'].widget.attrs['style'] = "text-align: left;"
        self.fields['monto_descuento_doce'].widget.attrs['placeholder'] = "$        "

        self.fields['monto_descuento_cero'].widget.attrs['class'] = "numerico"
        self.fields['monto_descuento_cero'].widget.attrs['style'] = "text-align: left;"
        self.fields['monto_descuento_cero'].widget.attrs['placeholder'] = "$           "

        self.fields['plazo'].widget.attrs['class'] = "numerico"
        self.fields['plazo'].widget.attrs['placeholder'] = "# días"
        self.fields['concepto'].widget.attrs['placeholder'] = "Concepto General"
        self.fields['concepto'].widget.attrs['maxlength'] = "200"
        self.fields['nota'].widget.attrs['maxlength'] = "15"
        self.fields['plazo'].widget.attrs['maxlength'] = "3"

        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick cliente"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].choices = [("", "")]+[(x.id, x.ruc+u" - "+x.razon_social) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-size'] = "auto"

        self.fields['direccion'].widget.attrs['class']="selectpicker show-tick"
        self.fields['direccion'].widget.attrs['title'] = "Seleccione primero un Cliente"
        self.fields['direccion'].widget.attrs['data-live-search'] = True
        qcliente_direccion = Cliente_Direccion.objects.filter(status=1, descripcion__isnull=False,
                                                              direccion1__isnull=False)
        self.fields['direccion'].choices = [("", "")]+[(x.id, x.descripcion+u" - "+x.direccion1) for x in qcliente_direccion]
        self.fields['direccion'].widget.attrs['data-style'] = "slc-primary"
        self.fields['direccion'].widget.attrs['data-width'] = "100%"
        self.fields['direccion'].widget.attrs['data-size'] = "auto"

        self.fields['contrato'].widget.attrs['class'] = "selectpicker show-tick contrato"
        self.fields['contrato'].widget.attrs['data-live-search'] = True
        self.fields['contrato'].choices = [("", "")] + [(str(x.id), str(x.num_cont) +u" - " + x.cliente.razon_social[0:30])
                                                        for x in ContratoCabecera.objects.filter(status=1).order_by("fecha_reg")]
        self.fields['contrato'].widget.attrs['data-style'] = "slc-primary"
        self.fields['contrato'].widget.attrs['data-width'] = "100%"
        self.fields['contrato'].widget.attrs['data-size'] = "auto"
        self.fields['numero_documento'].widget.attrs['readonly'] = True

class BaseDetalleVentaFormSet(BaseFormSet):
    """
    Clase que ayuda a validar los formset de los items
    """
    def __init__(self, *args, **kwargs):
        super(BaseDetalleVentaFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False
    def clean(self):
        """Checks that no two articles have the same title."""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return

        for form in self.forms:
            item = form.cleaned_data.get("item")
            if item is not None:
                if not Item.objects.filter(id=item, status=1).exists():
                    errors = form._errors.setdefault("item", ErrorList())
                    errors.append(u"Ingrese una item")

            precio_u = form.cleaned_data.get("precio_u", None)
            if precio_u is not None:
                try:
                    if float(precio_u) <= 0:
                        errors = form._errors.setdefault("precio_u", ErrorList())
                        errors.append(u"precio_u no válido")
                except:
                    errors = form._errors.setdefault("precio_u", ErrorList())
                    errors.append(u"precio_u no válido")

            cantidad = form.cleaned_data.get("cantidad")
            if cantidad is not None:
                try:
                    if float(cantidad) <= 0:
                        errors = form._errors.setdefault("cantidad", ErrorList())
                        errors.append(u"valor no válido")
                except:
                    errors = form._errors.setdefault("cantidad", ErrorList())
                    errors.append(u"valor no válido")

class FormDetalleVentaTest(forms.Form):
    id_item = forms.IntegerField(label=u"id_item", required=False)
    tipo_item = forms.IntegerField(label=u"tipo_item", required=False)
    item = forms.ChoiceField(label=u"Item", choices=[], widget=SelectItemParametros)
    concepto = forms.CharField(required=False, label=u"concepto")
    centro_costo = forms.ChoiceField(choices=[], required=False)
    cantidad = forms.FloatField()
    precio_u = forms.FloatField(label=u"precio_u", validators=[validar_valor])
    iva = forms.FloatField()
    ice = forms.FloatField(required=False)
    porc_descuento = forms.FloatField(required=False)
    subtotal = forms.FloatField(label=u"valor", validators=[validar_valor])
    cant_entregada = forms.FloatField(required=False)

    def getCantidad(self):
        return self.data["cantidad"]
    def getItem(self):
       return self.data["item"]
    def getCentroCosto(self):
       return self.data["centro_costo"]
    def getPrecioU(self):
        return self.data["precio_u"]
    def getDescuento(self):
        return self.data["porc_descuento"]
    def getSubtotal(self):
       return self.data["subtotal"]
    def getFormaPago(self):
        return self.data["forma_pago"]
    def getConceptoVenta(self):
       return self.data["concepto"]

    def __init__(self, *args, **kwargs):
        super(FormDetalleVentaTest, self).__init__(*args, **kwargs)
        self.fields['centro_costo'].choices = [("", "")]+[(x.id, x.descripcion) for x in Centro_Costo.objects.filter(status=1)]
        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker centro_costo"
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-c-c"
        self.fields['centro_costo'].widget.attrs['data-width'] = "100%"
        self.fields['centro_costo'].widget.attrs['data-name'] = "combo_centro_costo"

        self.fields['iva'].widget.attrs['class']="numerico iva "
        self.fields['iva'].widget.attrs['style'] = "width: 50px"
        self.fields['iva'].widget.attrs['readonly'] = True

        self.fields['ice'].widget.attrs['class'] = "numerico2 ice "
        self.fields['ice'].widget.attrs['style'] = "width: 50px"
        self.fields['ice'].widget.attrs['readonly'] = True

        self.fields['id_item'].widget.attrs['class'] = "id_item"
        self.fields['id_item'].widget.attrs['style'] = "width: 100%"

        self.fields['tipo_item'].widget.attrs['class'] = "tipo_item"
        self.fields['tipo_item'].widget.attrs['style'] = "width: 100%"

        #self.fields['item'].widget.attrs['class'] = "item"
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Item"
        self.fields['item'].widget.attrs['style'] = "width: 195px;"
        self.fields['item'].choices = [("", "")] + [(x.id, unicode(x.codigo)+"-"+unicode(x.nombre[0:40]))
                                                    for x in Item.objects.filter(status=1)]

        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick item"

        self.fields['concepto'].widget.attrs['style'] = " width: 292px"
        #self.fields['concepto'].widget.attrs['maxlength'] = "50"

        self.fields['cantidad'].widget.attrs['class'] = "numerico cantidad"
        self.fields['cantidad'].widget.attrs['style'] = "width: 90px"
        self.fields['cantidad'].widget.attrs['maxlength'] = "6"

        self.fields['cant_entregada'].widget.attrs['style'] = "width: 80px;"
        self.fields['cant_entregada'].widget.attrs['class'] = "numerico cant_entregada"
        self.fields['cant_entregada'].widget.attrs['maxlength'] = "6"

        self.fields['precio_u'].widget.attrs['class'] = "numerico precio_u"
        self.fields['precio_u'].widget.attrs['style'] = "width: 75px"
        self.fields['precio_u'].widget.attrs['data-name'] = "precio_unidad"
        self.fields['precio_u'].widget.attrs['maxlength'] = "10"
        if not modifica_precio_venta():
            self.fields['precio_u'].widget.attrs['readonly'] = True

        self.fields['porc_descuento'].widget.attrs['class'] = "numerico descuento"
        self.fields['porc_descuento'].widget.attrs['style'] = "width:75px"
        self.fields['subtotal'].widget.attrs['class'] = "numerico subtotal"
        self.fields['subtotal'].widget.attrs['readonly'] = True

        self.fields['concepto'].widget.attrs['class'] = "txt-oculto concepto_item"

class FormDetalleVentaStock(forms.Form):
    id_item = forms.IntegerField(label=u"id_item", required=False)
    tipo_item = forms.IntegerField(label=u"tipo_item", required=False)
    item = forms.ChoiceField(label=u"Item", choices=[], widget=SelectItemParametros)
    #concepto = forms.CharField(required=False, label=u"concepto")
    centro_costo = forms.ChoiceField(choices=[], required=False)
    cantidad = forms.FloatField()
    unidad = forms.CharField(label=u"Unidad", required=False)

    precio_u = forms.FloatField(label=u"precio_u", validators=[validar_valor])
    iva = forms.FloatField()
    ice = forms.FloatField(required=False)
    porc_descuento = forms.FloatField(required=False)
    subtotal = forms.FloatField(label=u"valor", validators=[validar_valor])

    def __init__(self, *args, **kwargs):
        super(FormDetalleVentaStock, self).__init__(*args, **kwargs)
        self.fields['centro_costo'].choices = [("", "")]+[(x.id, x.descripcion) for x in Centro_Costo.objects.filter(status=1)]
        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker centro_costo"
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-c-c"
        self.fields['centro_costo'].widget.attrs['data-width'] = "100%"
        self.fields['centro_costo'].widget.attrs['data-name'] = "combo_centro_costo"

        self.fields['iva'].widget.attrs['class'] = "numerico iva_stock"
        self.fields['iva'].widget.attrs['style'] = "width: 50px"
        self.fields['iva'].widget.attrs['readonly'] = True

        self.fields['ice'].widget.attrs['class'] = "numerico ice_stock"
        self.fields['ice'].widget.attrs['style'] = "width: 50px"
        self.fields['ice'].widget.attrs['readonly'] = True

        self.fields['id_item'].widget.attrs['class'] = "id_item_stock"
        self.fields['id_item'].widget.attrs['style'] = "width: 100%"

        self.fields['tipo_item'].widget.attrs['class'] = "tipo_item"
        self.fields['tipo_item'].widget.attrs['style'] = "width: 100%"

        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Item"
        self.fields['item'].widget.attrs['style'] = "width: 195px;"
        self.fields['item'].choices = [("", "")] + [(x.id, unicode(x.codigo)+"-"+unicode(x.nombre[0:15])) for x in Item.objects.filter(status=1)]
        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick item_stock"
        self.fields['item'].widget.attrs['style'] = "width: 100%"

        self.fields['unidad'].widget.attrs['class'] = "unidad_stock"
        self.fields['unidad'].widget.attrs['readonly'] = True

        self.fields['cantidad'].widget.attrs['class'] = "numerico cantidad_stock"
        self.fields['cantidad'].widget.attrs['maxlength'] = "6"

        self.fields['precio_u'].widget.attrs['class'] = "numerico precio_u_stock"
        self.fields['precio_u'].widget.attrs['style'] = "width: 75px"
        self.fields['precio_u'].widget.attrs['data-name'] = "precio_unidad"
        self.fields['precio_u'].widget.attrs['maxlength'] = "10"

        if not modifica_precio_venta():
            self.fields['precio_u'].widget.attrs['readonly'] = True

        self.fields['porc_descuento'].widget.attrs['class'] = "numerico descuento"
        self.fields['porc_descuento'].widget.attrs['style'] = "width:75px"

        self.fields['subtotal'].widget.attrs['class'] = "numerico subtotal_stock"
        self.fields['subtotal'].widget.attrs['readonly'] = True

class FormBuscadoresVenta(forms.Form):
    fecha = forms.DateTimeField(label=u'Fecha Inicial: ', widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_final = forms.DateTimeField(label=u'Fecha Final: ', widget=forms.DateInput(format=("%Y-%m-%d")))
    num_comp = forms.CharField(label=u"# de Comprobante:")
    ndocumento = forms.CharField(label=u"# Documento: ", required=True)
    #if not emite_docs_electronicos():
    vigencia_doc_empresa = forms.ChoiceField(choices=[])
    cliente = forms.ChoiceField(choices=[], label="Cliente: ")
    referencia = forms.CharField(max_length=250, label="Referencia:")
    item = forms.ChoiceField(label=u"Item:", choices=[])
    fpago = forms.ChoiceField(choices=[], label=u"Forma de Pago: ")
    rango_monto_ini = forms.FloatField(label=u"Rango de Monto: ", required=False, validators=[validar_valor])
    rango_monto_hasta = forms.FloatField(label=u"Hasta: ", required=False, validators=[validar_valor])
    venta_iva = forms.BooleanField(label=u"Venta con IVA:")
    estado = forms.ChoiceField(choices=[], label=u"Estado", required=False)
    venta_sin_iva = forms.BooleanField(label=u"Venta sin IVA:")
    tipo_documento = forms.ChoiceField(choices=[], label=u"Tipo de Documento: ")
    reembolso = forms.BooleanField(label=u"Reembolso de Gasto:")
    vendedor = forms.ChoiceField(label=u"Vendedor", choices=[])
    estado_doc_elect = forms.ChoiceField(choices=[], label=u"Estado Documento Electrónico", required=False)
    page = forms.IntegerField()

    def getNumComprobante(self):
        try:
            value = self.data['num_comp']
            return value
        except:
            return ""

    def getFecha(self):
        try:
            value = str(self.data['fecha']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return None

    def getFechaFinal(self):
        try:
            value = str(self.data['fecha_final']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return datetime.datetime.now()


    def getCliente(self):
        try:
            value = self.data['cliente']
            return Cliente.objects.get(id=value)
        except:
            return None

    def getTipoDoc(self):
        try:
            value = self.data['tipo_documento']
            return Documento.objects.get(id=value)
        except:
            return None

    def getVigenciaDocEmp(self):
        try:
            value = self.data['vigencia_doc_empresa']
            return VigenciaDocEmpresa.objects.get(id=value)
        except:
            return None

    def getVendedorBuscador(self):
        try:
            value = self.data['vendedor']
            return Colaboradores.objects.get(id=value)
        except:
            return None

    def getItem(self):
        try:
            value = self.data['item']
            return Item.objects.get(id=value)
        except:
            return None

    def getNumeroDocumento(self):
        try:
            value = self.data['ndocumento']
            return value
        except:
            return ""

    def getRangoMontoIni(self):
        try:
            value = self.data['rango_monto_ini']
            return float(value)
        except:
            return 0

    def getRangoMontoHasta(self):
        try:
            value = self.data['rango_monto_hasta']
            return float(value)
        except:
            return 100000000000

    def getReferencia(self):
        try:
            value = self.data['referencia']
            return value
        except:
            return ""

    def getFormaPago(self):
        try:
            value = self.data['fpago']
            return TipoPago.objects.get(id=value)
        except:
            return None
    def getReembolsoGasto(self):
        try:
            value = self.data['reembolso']
            return value
        except:
            return False

    def getVentaIVA(self):
        try:
            value = self.data['venta_iva']
            return value
        except:
            return False

    def getVentaSinIVA(self):
        try:
            value = self.data['venta_sin_iva']
            return value

        except:
            return False

    def getEstado(self):
        try:
            value = self.data['estado']
            return value
        except:
            return ""

    def getEstadoDocElect(self):
        try:
            value = self.data['estado_doc_elect']
            return value
        except:
            return ""

    ###########################################################################################

    def __init__(self, *args, **kwargs):
        super(FormBuscadoresVenta, self).__init__(*args, **kwargs)
        self.fields['tipo_documento'].choices = [("", "")] + [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]
        self.fields['tipo_documento'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_documento'].widget.attrs['data-style'] = "slc-primary txt-150"
        self.fields['tipo_documento'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_documento'].widget.attrs['data-live-search'] = True

        #if not emite_docs_electronicos():
        self.fields['vigencia_doc_empresa'].choices = [("", "")] + [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1).order_by("serie").distinct("serie")]
        self.fields['vigencia_doc_empresa'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['vigencia_doc_empresa'].widget.attrs['data-style'] = "slc-primary txt-150"
        self.fields['vigencia_doc_empresa'].widget.attrs['data-width'] = "100%"

        self.fields['item'].choices = [("", "")] + [(x.id, x.codigo+" - "+x.nombre) for x in Item.objects.filter(status=1, nombre__isnull=False, descripcion__isnull=False).order_by("codigo")]
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['item'].widget.attrs['data-style'] = "slc-b-s"

        self.fields['item'].widget.attrs['data-width'] = "100%"

        self.fields['rango_monto_ini'].widget.attrs['data-style'] = "width: 100%;"
        self.fields['rango_monto_hasta'].widget.attrs['data-style'] = "width: 100%;"

        self.fields['fecha'].widget.attrs['data-date-format']="yyyy-mm-dd"

        self.fields['fecha'].widget.attrs['class'] = "control-fecha"

        self.fields['fecha_final'].widget.attrs['data-date-format']="yyyy-mm-dd"

        self.fields['fecha_final'].widget.attrs['class']="control-fecha"

        self.fields['fpago'].choices = [("", "")] + [(x.id, x.descripcion) for x in TipoPago.objects.filter(status=1)]
        self.fields['fpago'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['fpago'].widget.attrs['data-style'] = "slc-primary txt-150"
        self.fields['fpago'].widget.attrs['data-width'] = "100%"

        self.fields['vendedor'].choices = [("", "")] + [(x.id, x.nombres_completos+" "+x.apellidos_completos) for x in Colaboradores.objects.filter(status=1)]
        self.fields['vendedor'].widget.attrs['class']="selectpicker show-tick"
        self.fields['vendedor'].widget.attrs['data-live-search'] = True
        self.fields['vendedor'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['vendedor'].widget.attrs['data-width'] = "100%"

        self.fields['estado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['estado'].widget.attrs['data-width'] = "100%"
        self.fields['rango_monto_ini'].widget.attrs['class'] = "numerico"
        self.fields['rango_monto_hasta'].widget.attrs['class'] = "numerico"
        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].choices = [('', '')] + [(x.id, x.razon_social[0:30]) for x in Cliente.objects.exclude(status=0)]
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['referencia'].widget.attrs['class'] = "txt-100pc"

        self.fields['ndocumento'].widget.attrs['placeholder'] = "0000000XX"
        self.fields['num_comp'].widget.attrs['placeholder'] = "VTA0000000XX"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_final'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['rango_monto_ini'].widget.attrs['placeholder'] = "$ 0.00"
        self.fields['rango_monto_hasta'].widget.attrs['placeholder'] = "$ 0.00"

        self.fields['estado'].choices = [("", "")] + [(x.id, x.descripcion) for x in Estados.objects.filter(status=1).exclude(codigo=0)]
        self.fields['estado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado'].widget.attrs['data-width'] = "100%"


        self.fields['estado_doc_elect'].choices = [('', '')] + [(x.codigo, x.descripcion) for x in EstadoDocElectronico.objects.using("base_central").filter(status=1).exclude(status=0)]
        self.fields['estado_doc_elect'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado_doc_elect'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado_doc_elect'].widget.attrs['data-width'] = "100%"

class RetencionVentaCabForm(forms.Form):
    fecha_ret = forms.DateField(label=u"Fecha de Registro", widget=forms.DateInput(format=("%Y-%m-%d")))

    def getFecha(self):
        value = self.cleaned_data['fecha_ret']
        return value

    def __init__(self, *args, **kwargs):
        super(RetencionVentaCabForm, self).__init__(*args, **kwargs)
        self.fields['fecha_ret'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_ret'].widget.attrs['class'] = "numerico datepicker"
        self.fields['fecha_ret'].widget.attrs['text-align'] = "left"
        self.fields['fecha_ret'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

class BaseDetalleRetenFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseDetalleRetenFormSet, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False
    def clean(self):
        """Checks that no two articles have the same title."""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return

        for form in self.forms:
            id_cta = form.cleaned_data.get("cuenta")
            if not PlanCuenta.objects.filter(id=id_cta, status=1, nivel=5).exists():
                errors = form._errors.setdefault("cuenta", ErrorList())
                errors.append(u"Ingrese una cuenta válida")

            valor = form.cleaned_data.get("valor_retenido")
            if valor is None or valor == "":
                errors = form._errors.setdefault("valor_retenido", ErrorList())
                errors.append(u"Valor no válido")
            else:
                try:
                    if round(float(valor), 2) <= 0:
                        errors = form._errors.setdefault("valor_retenido", ErrorList())
                        errors.append(u"valor no válido")
                except:
                    errors = form._errors.setdefault("valor_retenido", ErrorList())
                    errors.append(u"valor no válido")

class RetencionVentaForm(forms.Form):
    cuenta = forms.ChoiceField(choices=[])
    valor_retenido = forms.FloatField(validators=[validar_valor])

    def __init__(self, *args, **kwargs):
        super(RetencionVentaForm, self).__init__(*args, **kwargs)
        self.fields['cuenta'].choices = [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(tipo_id__in=(15, 22))]
        self.fields['cuenta'].widget.attrs['class'] = "cuenta selectpicker show-tick"
        self.fields['cuenta'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta'].widget.attrs['data-name'] = "cuenta_ret"

        self.fields['valor_retenido'].widget.attrs['class'] = "numerico valor_retenido txt-100pc txt-float-right"

class AnularVentaFacturaForm(forms.Form):
    fecha_reg = forms.DateField(label=u"Fecha Registro: ")
    documento = forms.ChoiceField(choices=[], label=u'Serie Documento: ')
    numero_doc = forms.CharField(max_length=9, label=u'Número de Factura: ', required=False)
    numero_comp = forms.CharField(max_length=9, required=False)

    def getDocumento(self):
        return self.data['documento']

    def getNumeroFactura(self):
        return self.data['numero_doc']

    def getFechaREg(self):
        return self.cleaned_data['fecha_reg']

    def __init__(self, *args, **kwargs):
        super(AnularVentaFacturaForm, self).__init__(*args, **kwargs)
        self.fields['documento'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1, documento_sri=Documento.objects.get(id=1)).order_by("serie")]
        self.fields['documento'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['documento'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['documento'].widget.attrs['data-width'] = "100%"
        self.fields['numero_doc'].widget.attrs['class'] = "numerico txt-100pc"

        self.fields['fecha_reg'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_reg'].widget.attrs['data-width'] = "100%"
        self.fields['fecha_reg'].widget.attrs['style'] = "width: 100%;"
        self.fields['fecha_reg'].widget.attrs['class'] = "calendario-gris input-calendario"

        self.fields['numero_comp'].widget.attrs['readonly'] = "readonly"
        self.fields['numero_comp'].widget.attrs['style'] = "width: 100%;"

class FormDetalleFormaPago(forms.Form):
    tipo_pago = forms.ChoiceField(label=u"Tipo Pago")
    valor = forms.FloatField(label=u"Valor", validators=[validar_valor])
    institucion = forms.ChoiceField(required=False)

    def getTipoPago(self):
        return self.data['tipo_pago']

    def getInstitucion(self):
        return self.data['institucion']


    def __init__(self, *args, **kwargs):
        super(FormDetalleFormaPago, self).__init__(*args, **kwargs)

        self.fields['valor'].widget.attrs['class'] = "numerico valor"
        self.fields['valor'].widget.attrs['placeholder'] = "0.00"
        self.fields['valor'].widget.attrs['style'] = "width: 100%;"

        self.fields['institucion'].widget.attrs['style'] = "width: 100%;"
        self.fields['institucion'].widget.attrs['class'] = "institucion"

        self.fields['tipo_pago'].widget.attrs['class'] = "selectpicker show-tick tipo_pago"
        self.fields['tipo_pago'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_pago'].widget.attrs['data-live-search'] = True
        self.fields['tipo_pago'].widget.attrs['data-name'] = "tipo_pago_name"
        self.fields['tipo_pago'].widget.attrs['style'] = "width: 200px;"
        self.fields['tipo_pago'].choices = [("", "")] + [(x.id, unicode(x.descripcion)) for x in FormaPago.objects.filter(status=1).filter(Q(id=1)| Q(id=2)| Q(id=3))]


        self.fields['institucion'].widget.attrs['data-style'] = "slc-primary"
        self.fields['institucion'].widget.attrs['data-live-search'] = True
        self.fields['institucion'].widget.attrs['data-name'] = "institucion"
        self.fields['institucion'].widget.attrs['style'] = "width: 200px;"
        self.fields['institucion'].widget.attrs['class'] = "selectpicker show-tick institucion"

class FormEnvioCorreoFactura(forms.Form):
    id_venta = forms.IntegerField()
    file = forms.FileField(label=u"Archivo Adjunto", required=False)
    cc = forms.EmailField(max_length=500, required=False)


    def get_id(self):
        return self.cleaned_data["id_venta"]

    def get_file(self):
        return self.cleaned_data["file"]

    def get_cc(self):
        return self.cleaned_data["cc"]

    def __init__(self, *args, **kwargs):
        super(FormEnvioCorreoFactura, self).__init__(*args, **kwargs)
        self.fields['file'].widget.attrs['class'] = "txt-100pc"
        self.fields['cc'].widget.attrs['class'] = "txt-100pc"
        self.fields['cc'].widget.attrs['placeholder'] = "cc1@dominio.com; cc2@dominio.com"

