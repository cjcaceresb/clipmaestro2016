#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *
from librerias.funciones.funciones_vistas import *

solo_alfanumericas = u'[a-zA-Z0-9_ \\ s]+$'
solo_numeros = u'^[0-9]+$'
solo_decimal = u'^\d+(\.\d{1,2})?$'

class FormClienteOnlyRead(forms.Form):
    grupo = forms.ChoiceField(choices=[], label=u"Cuenta Asignada")
    ############################# DATOS BÁSICOS #############################################
    tipo_identificacion = forms.ChoiceField(choices=[], label=u"Tipo de Identificación")
    Num_Identificacion = forms.CharField(label=u"# Identificación")
    tipo_cliente = forms.ChoiceField(choices=[], label=u"Tipo Cliente", required=False)
    razon_social = forms.CharField(widget=forms.Textarea, label=u"Razón Social", max_length=220, validators=[validador_espacios])
    nombre = forms.CharField(widget=forms.Textarea, label=u"Nombre Comercial", max_length=220, required=False)
    actividad = forms.ChoiceField(choices=[], label=u"Actividad", required=False)
    ##########################################################################################
    if emite_docs_electronicos():
        email = forms.EmailField(max_length=65, required=True, label=u"Email")
    else:
        email = forms.EmailField(max_length=65, required=False, label=u"Email")
    ############################## INFORMACIÓN COMERCIAL #####################################
    margen_utilidad = forms.RegexField(label="Margen de Utilidad", regex=solo_decimal)
    forma_pago = forms.ChoiceField(choices=[], label="Forma de Pago", required=False)
    plazo = forms.RegexField(label="Plazo", required=False, regex=solo_numeros)
    monto_credito = forms.RegexField(label="Monto Crédito", required=False, regex=solo_decimal)

    def __init__(self, *args, **kwargs):
        super(FormClienteOnlyRead, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].choices = [(x.codigo, x.descripcion) for x in Identificacion.objects.exclude(status=0).exclude(codigo='F')]
        self.fields['tipo_identificacion'].widget.attrs['class']="selectpicker"
        self.fields['tipo_identificacion'].widget.attrs['data-style']="slc-primary"

        self.fields['grupo'].choices = [(x.id, x.codigo+"-"+x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(status=1, tipo__alias=7, nivel=5)]
        self.fields['grupo'].widget.attrs['class'] = "selectpicker"
        self.fields['grupo'].widget.attrs['data-style'] = "slc-primary"

        self.fields['tipo_cliente'].choices = [(x.id, x.descripcion) for x in TipoPersona.objects.filter(status=1)]
        self.fields['tipo_cliente'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo_cliente'].widget.attrs['data-style'] = "slc-primary"

        self.fields['Num_Identificacion'].widget.attrs['maxlength'] = "13"
        self.fields['plazo'].widget.attrs['maxlength'] = "6"
        self.fields['monto_credito'].widget.attrs['maxlength'] = "12"
        self.fields['margen_utilidad'].widget.attrs['maxlength'] = "5"

        self.fields['actividad'].choices = [("", "")]+[(x.id, x.descripcion) for x in Sector.objects.filter(status=1)]
        self.fields['actividad'].widget.attrs['class']="selectpicker"
        self.fields['actividad'].widget.attrs['data-style']="slc-primary"

        self.fields['forma_pago'].choices = [("", "")]+[(x.id, x.descripcion) for x in TipoPago.objects.filter(status=1)]
        self.fields['forma_pago'].widget.attrs['class']="selectpicker"
        self.fields['forma_pago'].widget.attrs['data-style']="slc-primary"

        self.fields['monto_credito'].widget.attrs['style'] = "text-align: right;"
        self.fields['margen_utilidad'].widget.attrs['style'] = "text-align: right;"
        self.fields['plazo'].widget.attrs['style'] = "text-align: right;"

        self.fields['monto_credito'].widget.attrs['placeholder'] = "0.00"
        self.fields['margen_utilidad'].widget.attrs['placeholder'] = "0.00"

        self.fields['Num_Identificacion'].widget.attrs['placeholder'] = ""
        self.fields['plazo'].widget.attrs['placeholder'] = "Número de días, Ej: 15"

        self.fields['grupo'].widget.attrs['readonly'] = True
        self.fields['grupo'].widget.attrs['disabled'] = True

        self.fields['tipo_identificacion'].widget.attrs['readonly'] = True
        self.fields['tipo_identificacion'].widget.attrs['disabled'] = True

        self.fields['tipo_cliente'].widget.attrs['readonly'] = True
        self.fields['tipo_cliente'].widget.attrs['disabled'] = True

        self.fields['razon_social'].widget.attrs['style'] = "height: 110px;"
        self.fields['nombre'].widget.attrs['style'] = "height: 110px;"
        #self.fields['razon_social'].widget.attrs['style'] = "margin-bottom: 4px;"

        self.fields['razon_social'].widget.attrs['readonly'] = True
        self.fields['nombre'].widget.attrs['readonly'] = True
        self.fields['margen_utilidad'].widget.attrs['readonly'] = True

        self.fields['actividad'].widget.attrs['readonly'] = True
        self.fields['actividad'].widget.attrs['disabled'] = True

        self.fields['forma_pago'].widget.attrs['readonly'] = True
        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['email'].widget.attrs['style'] = "width: 468px;"
        self.fields['forma_pago'].widget.attrs['disabled'] = True

        self.fields['plazo'].widget.attrs['readonly'] = True
        self.fields['monto_credito'].widget.attrs['readonly'] = True
        self.fields['Num_Identificacion'].widget.attrs['readonly'] = True

class FormClientes(forms.Form):
    grupo = forms.ChoiceField(choices=[], label="Cuenta Asignada")
    ############################# DATOS BÁSICOS #############################################
    tipo_identificacion = forms.ChoiceField(choices=[], label=u"Tipo de Identificación")
    Num_Identificacion = forms.CharField(label="# Identificación")
    tipo_cliente = forms.ChoiceField(choices=[], label="Tipo Cliente", required=False)
    razon_social = forms.CharField(label="Razón Social", validators=[validador_espacios])
    nombre = forms.CharField(label="Nombre Comercial", required=False)
    actividad = forms.ChoiceField(choices=[], label=u"Sector", required=False)
    if emite_docs_electronicos():
        email = forms.EmailField(max_length=65, required=True, label=u"Correo Electrónico")
    else:
        email = forms.EmailField(max_length=65, required=False, label=u"Correo Electrónico")
    ##########################################################################################
    ############################## INFORMACIÓN COMERCIAL #####################################
    margen_utilidad = forms.RegexField(label="Margen de Utilidad", regex=solo_decimal, required=False)
    forma_pago = forms.ChoiceField(choices=[], label="Forma de Pago", required=False)
    plazo = forms.RegexField(label="Plazo", required=False, regex=solo_numeros)
    monto_credito = forms.RegexField(label="Monto Crédito", required=False, regex=solo_decimal)

    ################################## DATOS BÁSICOS ##########################################
    def getNombres(self):
        try:
            value = self.data['nombre']
            return value
        except:
            return ""
    def getTipoCliente(self):
        return self.data["tipo_cliente"]
    def getTipoIdentificacion(self):
        return self.data["tipo_identificacion"]
    def getRazonSocial(self):
        return self.data["razon_social"]
    def getRuc(self):
        return self.data["Num_Identificacion"]
    def getActividad(self):
        return self.data["actividad"]
    ####################################################################################

    ################################## INFORMACIÓN COMERCIAL #############################
    def getGrupoClientes(self):
        return self.data["grupo"]
    def getFormaPago(self):
        return self.data["forma_pago"]
    def getMontoCredito(self):
        return self.data["monto_credito"]
    def getPlazo(self):
        return self.data["plazo"]
    def getMargenUtilidad(self):
        return self.data["margen_utilidad"]
    ########################################################################################

    def get_email(self):
        try:
            return self.data["email"]
        except:
            return ""

    def clean(self):
        """
            Función para validar el formulario
        """
        from django.core.validators import validate_email
        from django.core.exceptions import ValidationError

        email = self.cleaned_data.get('email')

        if emite_docs_electronicos():
            try:
                validate_email(email)
            except ValidationError:
                self._errors["email"] = u"El email es una dato obligatorio"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(FormClientes, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].choices = [(x.codigo, x.descripcion) for x in Identificacion.objects.exclude(status=0).exclude(codigo='F')]
        self.fields['tipo_identificacion'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo_identificacion'].widget.attrs['data-style'] = "slc-primary"

        self.fields['grupo'].choices = [(x.id, x.codigo+"-"+x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(status=1, tipo__alias=7, nivel=5)]
        self.fields['grupo'].widget.attrs['class'] = "selectpicker"
        self.fields['grupo'].widget.attrs['data-style'] = "slc-primary"

        self.fields['tipo_cliente'].choices = [(x.id, x.descripcion) for x in TipoPersona.objects.filter(status=1)]
        self.fields['tipo_cliente'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo_cliente'].widget.attrs['data-style'] = "slc-primary"

        self.fields['Num_Identificacion'].widget.attrs['maxlength'] = "13"
        self.fields['razon_social'].widget.attrs['maxlength'] = "150"
        self.fields['nombre'].widget.attrs['maxlength'] = "150"
        self.fields['plazo'].widget.attrs['maxlength'] = "6"
        self.fields['monto_credito'].widget.attrs['maxlength'] = "12"
        self.fields['margen_utilidad'].widget.attrs['maxlength'] = "5"

        self.fields['email'].widget.attrs['placeholder'] = "info@dominio.com"

        self.fields['actividad'].choices = [("", "")]+[(x.id, x.descripcion) for x in Sector.objects.filter(status=1)]
        self.fields['actividad'].widget.attrs['class']="selectpicker"
        self.fields['actividad'].widget.attrs['data-style']="slc-primary"

        self.fields['forma_pago'].choices = [("", "")]+[(x.id, x.descripcion) for x in TipoPago.objects.filter(status=1)]
        self.fields['forma_pago'].widget.attrs['class']="selectpicker"
        self.fields['forma_pago'].widget.attrs['data-style']="slc-primary"

        self.fields['monto_credito'].widget.attrs['style'] = "text-align: right;"
        self.fields['margen_utilidad'].widget.attrs['style'] = "text-align: right;"
        self.fields['plazo'].widget.attrs['style'] = "text-align: right;"

        self.fields['monto_credito'].widget.attrs['placeholder'] = "0.00"
        self.fields['margen_utilidad'].widget.attrs['placeholder'] = "0.00"

        self.fields['Num_Identificacion'].widget.attrs['placeholder'] = ""
        self.fields['plazo'].widget.attrs['placeholder'] = "Número de días, Ej: 15"

class BuscadorFormClienteProveedor(forms.Form):
    tipo_identificacion = forms.ChoiceField(choices=[], label=u"Tipo de Identificación")
    num_id = forms.CharField(label=u"# Identificación")
    razon_social = forms.CharField(label=u"Razón Social")
    nombre_comercial = forms.CharField(label=u"Nombre Comercial")
    page = forms.IntegerField()

    def getTipoIdentificacion(self):
        try:
            value = self.data["tipo_identificacion"]
            return Identificacion.objects.get(id=value)
        except:
            return None

    def getNumID(self):
        try:
            value = self.data["num_id"]
            return str(value).strip(' \t\n\r')
        except:
            return ""

    def getRazonSocial(self):
        try:
            value = self.data["razon_social"]
            return str(value).strip(' \t\n\r')
        except:
            return ""

    def getNombreComercial(self):
        try:
            value = self.data['nombre_comercial']
            return value
        except:
            return ""


    def __init__(self, *args, **kwargs):
        super(BuscadorFormClienteProveedor, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].choices = [("", "")]+[(x.id, x.descripcion) for x in Identificacion.objects.filter(status=1).exclude(id=4)]
        self.fields['tipo_identificacion'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo_identificacion'].widget.attrs['data-style'] = "slc-primary"

        self.fields['num_id'].widget.attrs['maxlength'] = "13"
        self.fields['razon_social'].widget.attrs['maxlength'] = "65"
        self.fields['nombre_comercial'].widget.attrs['maxlength'] = "75"

class FormClienteEditarVenta(forms.Form):
    tipo_identificacion = forms.ChoiceField(choices=[], label=u"Tipo de Identificación")
    num_id = forms.CharField(label="# Identificación")
    tipo_cliente = forms.ChoiceField(choices=[], label="Tipo Cliente", required=False)
    razon_social = forms.CharField(label="Razón Social", validators=[validador_espacios])
    nombre = forms.CharField(label="Nombre Comercial", required=False)
    if emite_docs_electronicos():
        email = forms.EmailField(max_length=65, required=True, label=u"Correo Electrónico")
    else:
        email = forms.EmailField(max_length=65, required=False, label=u"Correo Electrónico")

    def getNombres(self):
        try:
            value = self.data['nombre']
            return value
        except:
            return ""

    def getTipoCliente(self):
        return self.data["tipo_cliente"]

    def getTipoIdentificacion(self):
        return self.data["tipo_identificacion"]

    def getRazonSocial(self):
        return self.data["razon_social"]

    def getRuc(self):
        return self.data["num_id"]

    def get_email(self):
        try:
            return self.data["email"]
        except:
            return ""

    def clean(self):
        """
            Función para validar el formulario
        """
        from django.core.validators import validate_email
        from django.core.exceptions import ValidationError

        email = self.cleaned_data.get('email')

        if emite_docs_electronicos():
            try:
                validate_email(email)
            except ValidationError:
                self._errors["email"] = u"El email es una dato obligatorio"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(FormClienteEditarVenta, self).__init__(*args, **kwargs)
        self.fields['tipo_identificacion'].choices = [(x.codigo, x.descripcion) for x in Identificacion.objects.exclude(status=0).exclude(codigo='F')]
        self.fields['tipo_identificacion'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo_identificacion'].widget.attrs['data-style'] = "slc-primary"

        self.fields['tipo_cliente'].choices = [(x.id, x.descripcion) for x in TipoPersona.objects.filter(status=1)]
        self.fields['tipo_cliente'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo_cliente'].widget.attrs['data-style'] = "slc-primary"

        self.fields['num_id'].widget.attrs['maxlength'] = "13"
        self.fields['razon_social'].widget.attrs['maxlength'] = "150"
        self.fields['nombre'].widget.attrs['maxlength'] = "150"
        self.fields['email'].widget.attrs['placeholder'] = "info@dominio.com"


