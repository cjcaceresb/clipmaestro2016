#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *
from django.forms.formsets import BaseFormSet
from django.core.validators import validate_email


__author__ = 'Clipmaestro'

def validate_valor(value):
    valor = float(value)
    if valor <= 0:
        raise ValidationError(u'Ingrese un valor superior a $0.00')
    else:
        return True

class ContratoForm(forms.Form):
    tipo = (('01', '12%'), ("02", '0%'))
    fecha_reg = forms.DateField(label=u"Fecha de Registro", widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_ent = forms.DateField(label=u"Fecha de Entrega", widget=forms.DateInput(format=("%Y-%m-%d")))
    secuencia = forms.IntegerField(label=u"# Último Contrato", required=False)
    cliente = forms.ChoiceField(choices=[], label=u"Cliente")
    tipo_pago = forms.ChoiceField(choices=[], label=u"Forma/Pago")
    plazo = forms.IntegerField(label=u"Plazo/Días", required=False)
    vendedor = forms.ChoiceField(choices=[], label=u"Vendedor")
    responsable = forms.CharField(max_length=100, label=u"Responsable")
    descripcion = forms.CharField(max_length=255, label=u"Descripción", widget=forms.Textarea, required=False)
    valor = forms.FloatField(label=u"Valor", validators=[validar_valor])
    #iva = forms.FloatField(label=u"Impuesto", validators=[validar_valor])
    iva = forms.ChoiceField(label=u"Impuesto", choices=tipo)
    monto_iva = forms.FloatField(label=u"Monto Iva")

    total = forms.FloatField(label=u"Total")
    costo_estimado = forms.FloatField(label=u"Costo Estimado", validators=[validar_valor])
    utilidad = forms.FloatField(label=u"Utilidad")

    def getFechaReg(self):
        return self.cleaned_data["fecha_reg"]

    def getFechaEnt(self):
        return self.cleaned_data["fecha_ent"]

    def getCliente(self):
        try:
            return Cliente.objects.get(id=self.cleaned_data["cliente"])
        except Cliente.DoesNotExist:
            return None

    def getTipoPago(self):
        try:
            return TipoPago.objects.get(id=self.cleaned_data["tipo_pago"])
        except TipoPago.DoesNotExist:
            return None

    def getPlazo(self):
        return self.cleaned_data["plazo"]

    def getVendedor(self):
        try:
            return Colaboradores.objects.get(id=self.cleaned_data["vendedor"])
        except Colaboradores.DoesNotExist:
            return None

    def getResponsable(self):
        return self.cleaned_data["responsable"]

    def getDescripcion(self):
        return self.cleaned_data["descripcion"]

    def getValor(self):
        return self.cleaned_data["valor"]

    def getIva(self):
        return self.cleaned_data["iva"]

    def getMontoIva(self):
        return self.cleaned_data["monto_iva"]

    def getTotal(self):
        return self.cleaned_data["total"]

    def getCostoEst(self):
        return self.cleaned_data["costo_estimado"]

    def getUtilidad(self):
        return self.cleaned_data["utilidad"]

    def getSecuencia(self):
        return self.cleaned_data["secuencia"]

    def __init__(self, *args, **kwargs):
        super(ContratoForm, self).__init__(*args, **kwargs)
        self.fields['fecha_reg'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"
        self.fields['fecha_reg'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_reg'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['fecha_ent'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"
        self.fields['fecha_ent'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_ent'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['cliente'].choices = [("", "")]+[(x.id, unicode(x.razon_social)) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-size'] = "auto"

        self.fields['tipo_pago'].choices = [(x.id, unicode(x.descripcion)) for x in TipoPago.objects.filter(status=1)]
        self.fields['tipo_pago'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_pago'].widget.attrs['data-style'] = "slc-primary"

        self.fields['vendedor'].choices = [("", "")]+[(x.id, unicode(x.nombres_completos+" "+x.apellidos_completos)) for x in Colaboradores.objects.filter(status=1)]
        self.fields['vendedor'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['vendedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['vendedor'].widget.attrs['title'] = "Seleccione un vendedor"
        self.fields['vendedor'].widget.attrs['data-live-search'] = True
        self.fields['responsable'].widget.attrs['data-style'] = "slc-primary"
        self.fields['vendedor'].widget.attrs['data-width'] = "100%"
        self.fields['vendedor'].widget.attrs['data-size'] = "auto"

        self.fields['plazo'].widget.attrs['class'] = "numero_n"

        self.fields['descripcion'].widget.attrs['rows'] = 8
        self.fields['descripcion'].widget.attrs['maxlength'] = "250"#con esto corregi lo de la descripcion que no dejaba grabar pero no decia por que en el contrato forms
        #self.fields['descripcion'].widget.attrs['placeholder'] = "La longitud del campo es de max. 200 caracteres"
        self.fields['descripcion'].widget.attrs['style'] = "width:100%"

        self.fields['valor'].widget.attrs['style'] = "width:100%"
        self.fields['valor'].widget.attrs['class'] = "numerico"

        self.fields['iva'].widget.attrs['class'] = "selectpicker iva"
        self.fields['iva'].widget.attrs['data-style'] = "slc-primary"
        self.fields['iva'].widget.attrs['data-size'] = "auto"
        self.fields['iva'].widget.attrs['style'] = "text-align :right;"
        #self.fields['iva'].widget.attrs['class'] = "numerico"

        self.fields['total'].widget.attrs['style'] = "width:100%"
        self.fields['total'].widget.attrs['readonly'] = True
        self.fields['total'].widget.attrs['class'] = "numerico"

        self.fields['monto_iva'].widget.attrs['style'] = "width:100%"
        self.fields['monto_iva'].widget.attrs['readonly'] = True
        self.fields['monto_iva'].widget.attrs['class'] = "numerico"

        self.fields['costo_estimado'].widget.attrs['style'] = "width:100%"
        self.fields['costo_estimado'].widget.attrs['class'] = "numerico"
        self.fields['utilidad'].widget.attrs['style'] = "width:100%"
        self.fields['utilidad'].widget.attrs['class'] = "numerico"
        self.fields['utilidad'].widget.attrs['readonly'] = True
        self.fields['secuencia'].widget.attrs['readonly'] = True
        self.fields['secuencia'].widget.attrs['style'] = "text-align :right;"
        self.fields['responsable'].widget.attrs['style'] = "width:100%"

class BuscadorContrato(forms.Form):
    fecha_reg_ini = forms.DateField(label=u"Fecha de Registro", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_reg_fin = forms.DateField(label=u"Fecha de Registro", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_ent_ini = forms.DateField(label=u"Fecha de Entrega", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_ent_fin = forms.DateField(label=u"Fecha de Entrega", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    cliente = forms.ChoiceField(choices=[], label=u"Cliente", required=False)
    tipo_pago = forms.ChoiceField(choices=[], label=u"Forma/Pago", required=False)
    vendedor = forms.ChoiceField(choices=[], label=u"Vendedor", required=False)
    responsable = forms.CharField(max_length=100, label=u"Responsable", required=False)
    page = forms.IntegerField(required=False)

    def getFechaRegIni(self):
        try:
            return self.cleaned_data["fecha_reg_ini"]
        except:
            return None

    def getFechaRegFin(self):
        try:
            return self.cleaned_data["fecha_reg_fin"]
        except:
            return None

    def getFechaEntIni(self):
        try:
            return self.cleaned_data["fecha_ent_ini"]
        except:
            return None

    def getFechaEntFin(self):
        try:
            return self.cleaned_data["fecha_ent_fin"]
        except:
            return None

    def getCliente(self):
        try:
            return Cliente.objects.get(id=self.cleaned_data["cliente"])
        except (Cliente.DoesNotExist, ValueError):
            return None

    def getTipoPago(self):
        try:
            return TipoPago.objects.get(id=self.cleaned_data["tipo_pago"])
        except (FormaPago.DoesNotExist, ValueError):
            return None

    def getPlazo(self):
        return self.cleaned_data["plazo"]

    def getVendedor(self):
        try:
            return Colaboradores.objects.get(id=self.cleaned_data["vendedor"])
        except (Colaboradores.DoesNotExist, ValueError):
            return None

    def getResponsable(self):
        return self.cleaned_data["responsable"]

    def __init__(self, *args, **kwargs):
        super(BuscadorContrato, self).__init__(*args, **kwargs)
        self.fields['fecha_reg_ini'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"
        self.fields['fecha_reg_ini'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_reg_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['fecha_reg_fin'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"
        self.fields['fecha_reg_fin'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_reg_fin'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['fecha_ent_ini'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"
        self.fields['fecha_ent_ini'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_ent_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['fecha_ent_fin'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"
        self.fields['fecha_ent_fin'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_ent_fin'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['cliente'].choices = [("", "")]+[(x.id, unicode(x.razon_social)) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-size'] = "auto"

        self.fields['tipo_pago'].choices = [("", "")] + [(x.id, unicode(x.descripcion)) for x in TipoPago.objects.filter(status=1)]
        self.fields['tipo_pago'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_pago'].widget.attrs['title'] = "Seleccione una forma de pago"
        self.fields['tipo_pago'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_pago'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_pago'].widget.attrs['data-size'] = "auto"

        self.fields['vendedor'].choices = [("", "")]+[(x.id, unicode(x.nombres_completos+" "+x.apellidos_completos)) for x in Colaboradores.objects.filter(status=1)]
        self.fields['vendedor'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['vendedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['vendedor'].widget.attrs['title'] = "Seleccione un vendedor"
        self.fields['vendedor'].widget.attrs['data-live-search'] = True
        self.fields['vendedor'].widget.attrs['data-width'] = "100%"
        self.fields['vendedor'].widget.attrs['data-size'] = "auto"

        self.fields['responsable'].widget.attrs['style'] = "width: 100%"

        self.fields['page'].widget.attrs['style'] = "display: none"

class SelectSueldoEmpleado(forms.Select):
    allow_multiple_selected = False
    def __init__(self, attrs=None, choices=()):
        super(SelectSueldoEmpleado, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":

            try:

                sueldo = Empleados.objects.get(id=option_value).sueldo
                if sueldo is None:
                    sueldo = 0.0

            except:
                sueldo = 0.0

            return format_html(u'<option value="{0}" {1} data-sueldo={2} > {3} </option>',
                                option_value,
                                selected_html,
                                sueldo, unicode(force_text(option_label)))

        else:

            return format_html('<option value="{0}" {1}>{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class CostoContratoForm(forms.Form):
    modalidad = (('01', 'Externa'), ("02", 'Interna'))
    fecha_reg = forms.DateField(label=u"Fecha de Registro", widget=forms.DateInput(format=("%Y-%m-%d")))
    tipo = forms.ChoiceField(choices=modalidad, label=u"Mano de Obra")
    proveedor = forms.ChoiceField(choices=[], label=u"Proveedor", required=False)
    empleado = forms.ChoiceField(choices=[], label=u"Empleado", required=False, widget=SelectSueldoEmpleado())
    factura = forms.ChoiceField(choices=[], label=u"Factura", required=False)
    referencia = forms.CharField(label=u"Referencia", required=False)
    sueldo = forms.CharField(label=u"Sueldo", required=False)
    valor_cmp = forms.CharField(label=u"Valor Compra", required=False)
    concepto = forms.CharField(max_length=250, label="Concepto", widget=forms.Textarea, required=False)

    def getFactura(self):
        try:
            return self.data['factura']
        except:
            return None

    def getValorCmp(self):
        try:
            value = self.data['valor_cmp']
            return value
        except:
            return None

    def getProveedor(self):
        try:
            value = self.data['proveedor']
            return Proveedores.objects.get(id=value)
        except:
            return None

    def getEmpleado(self):
        try:
            value = self.data['empleado']
            return Empleados.objects.get(id=value)
        except:
            return None

    def getFechaReg(self):
        return self.cleaned_data["fecha_reg"]

    def getTipo(self):
        return self.data['tipo']

    def getConcepto(self):
        return self.data['concepto']

    def __init__(self, *args, **kwargs):
        super(CostoContratoForm, self).__init__(*args, **kwargs)
        self.fields['fecha_reg'].widget.attrs['class'] = "datepicker calendario-gris input-calendario"
        self.fields['fecha_reg'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_reg'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_reg'].widget.attrs['style'] = "width:100%"

        self.fields['tipo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo'].widget.attrs['data-width'] = "100%"
        self.fields['tipo'].widget.attrs['data-size'] = "auto"

        self.fields['factura'].choices = [("", "")]+[(x.id, unicode(x.num_doc)) for x in CompraCabecera.objects.filter(status=1)]
        self.fields['factura'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['factura'].widget.attrs['title'] = "Seleccione una Factura"
        self.fields['factura'].widget.attrs['data-live-search'] = True
        self.fields['factura'].widget.attrs['data-style'] = "slc-primary"
        self.fields['factura'].widget.attrs['data-width'] = "100%"
        self.fields['factura'].widget.attrs['data-size'] = "auto"

        q1 = CostoDistribuirCompras.objects.filter(monto__gt=F("distribuido"), status=1).distinct("compra_cabecera__proveedor")
        self.fields['proveedor'].choices = [("", "")]+[(x.compra_cabecera.proveedor_id, unicode(x.compra_cabecera.proveedor.razon_social)) for x in q1]
        self.fields['proveedor'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un Proveedor"
        self.fields['proveedor'].widget.attrs['data-live-search'] = True
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['data-width'] = "100%"
        self.fields['proveedor'].widget.attrs['data-size'] = "auto"

        self.fields['empleado'].choices = [("", "")]+[(x.id, unicode(x.nombres_completos)+" "+unicode(x.apellidos_completos)) for x in Empleados.objects.filter(status=1)]
        self.fields['empleado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['empleado'].widget.attrs['title'] = "Seleccione un Empleado"
        self.fields['empleado'].widget.attrs['data-live-search'] = True
        self.fields['empleado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['empleado'].widget.attrs['data-width'] = "100%"
        self.fields['empleado'].widget.attrs['data-size'] = "auto"

        self.fields['sueldo'].widget.attrs['readonly'] = True
        self.fields['valor_cmp'].widget.attrs['readonly'] = True


class CostoContratoDetalleProveedorForm(forms.Form):
    contrato = forms.ChoiceField(choices=[], label=u"Contrato")
    valor = forms.CharField(label=u"Valor", validators=[validate_valor])

    def getValor(self):
        return self.data['valor']

    def __init__(self, *args, **kwargs):
        super(CostoContratoDetalleProveedorForm, self).__init__(*args, **kwargs)
        self.fields['contrato'].choices = [("", "")]+[(x.id, unicode(x.num_cont+"-"+x.cliente.razon_social)) for x in ContratoCabecera.objects.filter(status=1)]
        self.fields['contrato'].widget.attrs['class'] = "selectpicker show-tick contrato"
        self.fields['contrato'].widget.attrs['title'] = "Seleccione un Contrato"
        self.fields['contrato'].widget.attrs['data-live-search'] = True
        self.fields['contrato'].widget.attrs['data-style'] = "slc-primary"
        self.fields['contrato'].widget.attrs['data-width'] = "100%"
        self.fields['contrato'].widget.attrs['data-size'] = "auto"

        self.fields['valor'].widget.attrs['style'] = "width: 100%"
        self.fields['valor'].widget.attrs['class'] = "numerico valor"

class CostoContratoDetalleEmpleadoForm(forms.Form):
    contrato = forms.ChoiceField(choices=[], label=u"Contrato")
    horas = forms.CharField(label=u"Horas", required=False)
    valor = forms.CharField(label=u"Valor")

    def __init__(self, *args, **kwargs):
        super(CostoContratoDetalleEmpleadoForm, self).__init__(*args, **kwargs)

        self.fields['contrato'].choices = [("", "")]+[(x.id, unicode(x.num_cont+"-"+x.cliente.razon_social)) for x in ContratoCabecera.objects.filter(status=1)]
        self.fields['contrato'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['contrato'].widget.attrs['title'] = "Seleccione un Contrato"
        self.fields['contrato'].widget.attrs['data-live-search'] = True
        self.fields['contrato'].widget.attrs['data-style'] = "slc-primary"
        self.fields['contrato'].widget.attrs['data-width'] = "100%"
        self.fields['contrato'].widget.attrs['data-size'] = "auto"

        self.fields['horas'].widget.attrs['class'] = "numerico"
        self.fields['horas'].widget.attrs['style'] = "width: 100%"

        self.fields['valor'].widget.attrs['readonly'] = True
        self.fields['valor'].widget.attrs['style'] = "width: 100%"
        self.fields['valor'].widget.attrs['class'] = "numerico valor"

        self.fields['horas'].widget.attrs['class'] = "horas"
