#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from datetime import date, timedelta
from librerias.funciones.validacion_formularios import *
from django.contrib import messages
from librerias.funciones.validacion_rucs import *

__author__ = 'Roberto'

solo_letras = u'^[A-Za-zÑñáéíóúÁÉÍÓÚ_\\s]+$'
solo_numeros = u'^[0-9]+$'
solo_alfanumericas = u'[a-zA-Z0-9_\\s]+$'


class FormColaboradores(forms.Form):
    sexo = (('', ''), ('Masculino', 'Masculino'), ('Femenino','Femenino'))
    estado_civil = (('', ''), ('Soltero(a)', 'Soltero(a)'), ('Casado(a)', 'Casado(a)'))
    num_id = forms.CharField(max_length=20, label=u"Cédula")
    nombres = forms.RegexField(max_length=70, label=u"Nombres", regex=solo_letras, validators=[validador_espacios])
    apellidos = forms.RegexField(max_length=70, label=u"Apellidos", regex=solo_letras, validators=[validador_espacios])
    fecha_Nac = forms.DateField(label=u"Fecha Nacimiento", widget=forms.DateInput(format=("%Y-%m-%d")))
    telefono = forms.CharField(label=u"Teléfono", validators=[validador_espacios], required=False)
    direccion = forms.RegexField(label=u"Dirección", validators=[validador_espacios], regex=solo_alfanumericas)
    email = forms.EmailField(label=u"Email", max_length=50)
    estado_civil = forms.ChoiceField(choices=estado_civil, label=u"Estado Cívil")
    sexo = forms.ChoiceField(choices=sexo, label=u"Sexo")

    def getNombres(self):
        return self.data["nombres"]

    def getApellidos(self):
        return self.data["apellidos"]

    def getNum_ID(self):
        return self.data["num_id"]

    def getSexo(self):
        return self.data["sexo"]

    def getFecha_Nac(self):
        return self.cleaned_data["fecha_Nac"]

    def getTelefono(self):
        try:
            return self.data["telefono"]
        except:
            return ""

    def getEstado_Civil(self):
        return self.data["estado_civil"]

    def getDireccion(self):
        return self.data["direccion"]

    def getCorreo(self):
        return self.data["email"]


    def clean(self):
        fecha_nac = self.cleaned_data.get('fecha_Nac')

        if fecha_nac != "" and fecha_nac is not None:
            if fecha_nac.year > (datetime.datetime.now().year - 18):
                self._errors["fecha_Nac"] = u"Ingrese una fecha de nacimiento válida , el vendedor debe ser mayor de 18 años"
                self.fields['fecha_Nac'].widget.attrs['title'] = self._errors["fecha_Nac"]

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(FormColaboradores, self).__init__(*args, **kwargs)
        self.fields['sexo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['sexo'].widget.attrs['data-style'] = "slc-primary show-tick"
        self.fields['estado_civil'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado_civil'].widget.attrs['data-style'] = "slc-primary"
        self.fields['fecha_Nac'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_Nac'].widget.attrs['class'] = "calendario-gris input-calendario"
        self.fields['email'].widget.attrs['placeholder'] = "email@ejemplo.com"
        self.fields['fecha_Nac'].widget.attrs['placeholder'] = "0000-00-00"
        self.fields['telefono'].widget.attrs['placeholder'] = "(99) 9999-9999"
        self.fields['telefono'].widget.attrs['class'] = "numerico"
        self.fields['num_id'].widget.attrs['class'] = "numerico"
        self.fields['num_id'].widget.attrs['placeholder'] = ""
        self.fields['num_id'].widget.attrs['maxlength'] = "10"
        self.fields['telefono'].widget.attrs['maxlength'] = "15"
        self.fields['direccion'].widget.attrs['maxlength'] = "125"
        self.fields['fecha_Nac'].widget.attrs['maxlength'] = "10"

class FormColaboradoresOnly(forms.Form):
    sexo = (('', ''), ('Masculino', 'Masculino'), ('Femenino','Femenino'))
    estado_civil = (('', ''), ('Soltero(a)', 'Soltero(a)'), ('Casado(a)', 'Casado(a)'))
    num_id = forms.CharField(max_length=20, label=u"Cédula")
    nombres = forms.RegexField(max_length=70, label=u"Nombres", regex=solo_letras, validators=[validador_espacios])
    apellidos = forms.RegexField(max_length=70, label=u"Apellidos", regex=solo_letras, validators=[validador_espacios])
    fecha_Nac = forms.CharField(label=u"Fecha Nacimiento")
    telefono = forms.CharField(label=u"Teléfono", validators=[validador_espacios], required=False)
    direccion = forms.CharField(label=u"Dirección", widget=forms.Textarea)
    email = forms.EmailField(label=u"Email")
    estado_civil = forms.ChoiceField(choices=estado_civil, label=u"Estado Cívil")
    sexo = forms.ChoiceField(choices=sexo, label=u"Sexo")

    def __init__(self, *args, **kwargs):
        super(FormColaboradoresOnly, self).__init__(*args, **kwargs)
        self.fields['sexo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['sexo'].widget.attrs['data-style'] = "slc-primary show-tick"
        self.fields['estado_civil'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado_civil'].widget.attrs['data-style'] = "slc-primary"
        self.fields['fecha_Nac'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_Nac'].widget.attrs['class'] = "calendario-gris input-calendario"
        self.fields['email'].widget.attrs['placeholder'] = "email@ejemplo.com"
        self.fields['fecha_Nac'].widget.attrs['placeholder'] = "0000-00-00"
        self.fields['telefono'].widget.attrs['placeholder'] = "(99) 9999-9999"
        self.fields['telefono'].widget.attrs['class'] = "numerico"
        self.fields['telefono'].widget.attrs['data-style'] = "text-align: right"
        self.fields['num_id'].widget.attrs['placeholder'] = ""
        self.fields['num_id'].widget.attrs['maxlength'] = "13"
        self.fields['direccion'].widget.attrs['maxlength'] = "125"
        self.fields['fecha_Nac'].widget.attrs['maxlength'] = "10"
        self.fields['sexo'].widget.attrs['readonly'] = True
        self.fields['sexo'].widget.attrs['disabled'] = True
        self.fields['estado_civil'].widget.attrs['readonly'] = True
        self.fields['estado_civil'].widget.attrs['disabled'] = True
        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['direccion'].widget.attrs['readonly'] = True
        self.fields['telefono'].widget.attrs['readonly'] = True
        self.fields['fecha_Nac'].widget.attrs['readonly'] = True
        self.fields['fecha_Nac'].widget.attrs['disabled'] = True
        self.fields['apellidos'].widget.attrs['readonly'] = True
        self.fields['nombres'].widget.attrs['readonly'] = True
        self.fields['num_id'].widget.attrs['readonly'] = True

class FormBuscadorColaboradores(forms.Form):
    sexo_t = (('', ''), ('Masculino', 'Masculino'), ('Femenino','Femenino'))
    estado_civil_t = (('', ''), ('Soltero(a)', 'Soltero(a)'), ('Casado(a)', 'Casado(a)'))
    nombres = forms.CharField(label=u"Nombres", max_length=120)
    apellidos = forms.CharField(label=u"Apellidos", max_length=120)
    num_id = forms.CharField(label=u"# Identificación")
    telefono = forms.CharField(label=u"Teléfono")
    direccion = forms.CharField(label=u"Dirección")
    fecha_nacimiento = forms.DateField()
    email = forms.EmailField()
    sexo = forms.ChoiceField(choices=sexo_t, label=u"Sexo")
    estado_civil = forms.ChoiceField(choices=estado_civil_t, label=u"Estado Cívil")
    page = forms.IntegerField()

    def getNombres(self):
        try:
            value = self.data['nombres']
            return value
        except:
            return ""

    def getApellidos(self):
        try:
            value = self.data['apellidos']
            return value
        except:
            return ""

    def getDireccion(self):
        try:
            value = self.data['direccion']
            return value
        except:
            return ""

    def getNumID(self):
        try:
            value = self.data['num_id']
            return value
        except:
            return ""

    def getFechaNacimiento(self):
        try:
            return self.cleaned_data['fecha_nacimiento']
        except:
            return None

    def getEmail(self):
        try:
            value = self.data['email']
            return value
        except:
            return ""
