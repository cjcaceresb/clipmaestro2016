#! /usr/bin/python
# -*- coding: UTF-8 -*-

from librerias.funciones.funciones_vistas import *
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from contabilidad.models import *
import datetime
from django.forms import ModelForm
from django.db.models import Q
from datetime import date, timedelta
from django.utils.encoding import smart_str, smart_unicode
from administracion.models import Estados
__author__ = 'Clip Maestro'

#################
#  Constantes   #
#################


del_det_comp = "%@&/"  # Delimitador en Detalle de Compras

def validate_proveedor(value):
    try:
        proveedor = unicode(value).encode("iso-8859-2", "replace")
        ruc_prov = str(proveedor).split('-')[0].replace(" ","")
        Proveedores.objects.get(status=1, ruc=ruc_prov)
    except:
        raise ValidationError(u'El proveedor que ingresó se encuentra inactivo o no existe')


class CabeceraCompForm(forms.Form):

    fecha_asiento = forms.DateField(label=u"Fecha Asiento", widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha = forms.DateField(label=u"Fecha Emisión", widget=forms.DateInput(format=("%Y-%m-%d")))
    proveedor = forms.IntegerField(label=u"Proveedor")
    sust_tribut = forms.ChoiceField(choices=[], label=u"Sust. Tributario")
    tipo_de_doc = forms.ChoiceField(choices=[], label=u"Tipo de Doc.")
    serie = forms.CharField(max_length=20, label=u"# Doc")
    autorizacion = forms.CharField(max_length=37, label=u"Autorización")
    vence = forms.DateField(label=u"Vence", widget=forms.DateInput(format=("%Y-%m-%d")))
    forma_pago = forms.ChoiceField(choices=[], label=u"Forma de Pago")
    plazo = forms.IntegerField(required=False, label=u"Plazo")
    pago = forms.ChoiceField(choices=[], label=u"Origen")
    pais = forms.IntegerField(required=False)
    convenio_doble_tr = forms.BooleanField(required=False, label=u"Doble Trib.")
    sujeto_retencion = forms.BooleanField(required=False, label=u"Sujeto Ret.")
    contrato = forms.IntegerField(label=u"Contrato", required=False)

    tipo_de_doc_modif = forms.ChoiceField(choices=[], label="Tipo de Doc.", required=False)
    serie_modif = forms.CharField(max_length=20, label="Serie", required=False)
    autorizacion_modif = forms.CharField(max_length=37, required=False, label=u"Autorización")

    concepto = forms.CharField(max_length=600, label="Concepto", widget=forms.Textarea, required=False)

    def getConcepto(self):
        return self.data["concepto"]

    def getProveedor(self):
        return self.data["proveedor"]

    def getAutorizacion(self):
        return self.data["autorizacion"]

    def getPago(self):
        return self.data["pago"]

    def getPais(self):
        return self.data["pais"]

    def getConvenioDobleT(self):
        return self.data["convenio_doble_tr"]

    def getSujeto_Retencion(self):
        return self.data["sujeto_retencion"]

    def getFecha_asiento(self):
        return self.cleaned_data["fecha_asiento"]

    def getFecha(self):
        return self.cleaned_data["fecha"]

    def getSustento_tributario(self):
        return self.data["sust_tribut"]

    def getSerie(self):
        return self.data["serie"]

    def getTipo_de_documento(self):
        return str(unicode(self.data["tipo_de_doc"]))

    def getVence(self):
        return self.cleaned_data["vence"]

    def getForma_pago(self):
        return self.data["forma_pago"]

    def getContrato(self):
        try:
            value = self.cleaned_data["contrato"]
            return ContratoCabecera.objects.get(id=value)
        except:
            return None

    def getPlazo(self):
        return self.data["plazo"]

    def getTipoDocMod(self):
        return self.data["tipo_de_doc_modif"]

    def getSerieMod(self):
        return self.data["serie_modif"]

    def getAutorMod(self):
        return self.data["autorizacion_modif"]

    def clean(self):
        """
            Función para validar el formulario
        """

        forma_pago = self.cleaned_data.get('forma_pago')
        plazo = self.cleaned_data.get('plazo')
        pago = self.cleaned_data.get('pago')
        pais = self.cleaned_data.get('pais')
        tipo_de_doc = self.cleaned_data.get('tipo_de_doc')
        serie_modif = self.cleaned_data.get('serie_modif')
        autorizacion_modif = self.cleaned_data.get('autorizacion_modif')

        #Validación de la forma de pago
        if forma_pago == '2':
            if plazo is not None or plazo != "":
                if plazo <= 0:
                    self._errors["plazo"] = u"Usted debe ingresar un plazo si es pago a crédito"
            else:
                self._errors["plazo"] = u"Usted debe ingresar un plazo si es pago a crédito"

        # Validación de Pago (local-exterior)
        if pago == '2' or pago is None:
            if pais == "" or pais is None:
                self._errors["pais"] = u"Usted debe ingresar el país desde donde vino la compra"

        # Validación de N/D N/C
        if es_nota_credito(tipo_de_doc) or es_nota_debito(tipo_de_doc):
            if serie_modif == '' or serie_modif is None:
                self._errors["serie_modif"] = u"Usted debe ingresar el número de documento que modifica"

            if autorizacion_modif == '' or autorizacion_modif is None:
                self._errors["autorizacion_modif"] = u"Usted debe ingresar el número de autorización del documento que modifica"

        try:
            Documento.objects.get(id=tipo_de_doc, status=1)
        except (Documento.DoesNotExist, ValueError):
            self._errors["tipo_de_doc"] = u"Docuemento invalido"
            self.fields['tipo_de_doc'].widget.attrs['title'] = "Docuemento invalido"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):

        super(CabeceraCompForm, self).__init__(*args, **kwargs)

        self.fields['tipo_de_doc'].choices = [(x.id , x.codigo_documento+" - "+x.descripcion_documento) for x in Documento.objects.exclude(status=0).order_by("codigo_documento")]
        self.fields['tipo_de_doc'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_de_doc'].widget.attrs['data-id'] = "data_tipo_doc"
        self.fields['tipo_de_doc'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_de_doc'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_de_doc'].widget.attrs['data-size'] = "auto"
        self.fields['tipo_de_doc'].widget.attrs['data-live-search'] = True

        self.fields['sust_tribut'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['sust_tribut'].widget.attrs['data-style'] = "slc-primary"
        self.fields['sust_tribut'].widget.attrs['data-name'] = "sust_tribut"
        self.fields['sust_tribut'].widget.attrs['data-width'] = "100%"
        self.fields['sust_tribut'].widget.attrs['data-size'] = "auto"
        self.fields['sust_tribut'].choices = [(x.codigo, x.codigo+u" - "+x.descripcion) for x in Sustento_Tributario.objects.filter(status=1)]

        self.fields['forma_pago'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['forma_pago'].widget.attrs['data-style'] = "slc-primary"
        self.fields['forma_pago'].widget.attrs['data-width'] = "100%"
        self.fields['forma_pago'].widget.attrs['data-size'] = "auto"
        self.fields['forma_pago'].widget.attrs['style'] = "32px"
        self.fields['forma_pago'].choices = [(x.id, x.descripcion) for x in TipoPago.objects.filter(status=1)]

        self.fields['proveedor'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['proveedor'].widget.attrs['data-url'] = reverse("buscar_proveedor")

        self.fields['contrato'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['contrato'].widget.attrs['data-url'] = reverse("buscar_contratos")

        self.fields['serie'].widget.attrs['placeholder'] = "000-000-000000000"
        self.fields['serie'].widget.attrs['class'] = "serie"

        self.fields['fecha_asiento'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_asiento'].widget.attrs['class'] = "calendario-gris"
        self.fields['fecha_asiento'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_asiento'].widget.attrs['readonly'] = True

        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "calendario-gris"
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['readonly'] = True

        self.fields['vence'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['vence'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['vence'].widget.attrs['readonly'] = True

        self.fields['concepto'].widget.attrs['placeholder'] = "Concepto"
        self.fields['concepto'].widget.attrs['rows'] = 3
        self.fields['concepto'].widget.attrs['maxlength'] = "250"

        self.fields['serie_modif'].widget.attrs['placeholder'] = "000-000-000000000"

        self.fields['autorizacion_modif'].widget.attrs['maxlength'] = "37"
        self.fields['autorizacion_modif'].widget.attrs['class'] = "numero_natural"

        self.fields['autorizacion'].widget.attrs['placeholder'] = "000000000"
        self.fields['autorizacion'].widget.attrs['class'] = "numero_natural"
        self.fields['autorizacion_modif'].widget.attrs['maxlength'] = "37"

        self.fields['plazo'].widget.attrs['placeholder'] = "# dias"
        self.fields['plazo'].widget.attrs['class'] = "numerico"

        self.fields['pago'].widget.attrs['data-style'] = "slc-primary"
        self.fields['pago'].widget.attrs['data-width'] = "100%"
        self.fields['pago'].widget.attrs['class'] = "selectpicker show-tick slc-primary pago_cont_cred"
        self.fields['pago'].widget.attrs['data-size'] = "auto"
        self.fields['pago'].widget.attrs['style'] = "32px"
        self.fields['pago'].choices = [(x.codigo, x.descripcion) for x in Tipo_Pago_SRI.objects.exclude(status=0).order_by("codigo")]

        self.fields['pais'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['pais'].widget.attrs['data-url'] = reverse("buscar_paises")

        self.fields['tipo_de_doc_modif'].widget.attrs['class']="selectpicker"
        self.fields['tipo_de_doc_modif'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_de_doc_modif'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_de_doc_modif'].widget.attrs['data-size'] = "auto"
        self.fields['tipo_de_doc_modif'].widget.attrs['data-live-search'] = True
        self.fields['tipo_de_doc_modif'].choices = [(x.codigo_documento, x.codigo_documento + " - " + x.descripcion_documento) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]

        #self.fields['tipo_de_doc_modif'].widget.attrs['class'] = "ajaxcombobox"
        #self.fields['tipo_de_doc_modif'].widget.attrs['data-url'] = reverse("buscar_tipo_documento")


tipo = (('1', 'B'), ('2', 'S'))


################
## Utilizar para validar la cuenta del combo
def validate_cuenta(value):
    try:
        PlanCuenta.objects.get(id=value, status=1, nivel=5)
        return True
    except:
        raise ValidationError(u'La cuenta ingresada ha sido eliminada o no existe, por favor intente con otra cuenta')

def validate_valor(value):
    valor = float(value)
    if valor <= 0:
        raise ValidationError(u'Ingrese un valor superior a $0.00')
    else:
        return True


class SelectCompras(forms.Select):
    allow_multiple_selected = False

    def __init__(self, attrs=None, choices=()):
        super(SelectCompras, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            detalle = force_text(option_label).split(del_det_comp)[0] + "-" + \
                      force_text(option_label).split(del_det_comp)[1] + "-" + \
                      force_text(option_label).split(del_det_comp)[2]
            es_variable = "false" if force_text(option_label).split(del_det_comp)[5] == "None" else "true"

            return format_html(u'<option value="{0}" {1} data-porcentaje={2} data-codigo={3} data-esvariable={5} '
                               u'data-porc_max={6} data-porc_min={7}>{4}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label).split(del_det_comp)[3],
                               force_text(option_label).split(del_det_comp)[4],
                               smart_unicode(detalle),
                               es_variable,
                               force_text(option_label).split(del_det_comp)[6],
                               force_text(option_label).split(del_det_comp)[7]
                               )
        else:
            return format_html('<option value="{0}"{1} data-porcentaje="" data-codigo="">{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class WidgetDetalleCompra(forms.Select):
    allow_multiple_selected = False

    def __init__(self, attrs=None, choices=()):
        super(WidgetDetalleCompra, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            try:
                plan_cta = PlanCuenta.objects.get(id=option_value)
                tipo = plan_cta.tipo.id
            except:
                tipo = 0

            return format_html(u'<option value="{0}" {1} data-tipo={2}>{3}</option>',
                               option_value, selected_html, tipo,
                               smart_unicode(option_label)
                               )
        else:
            return format_html('<option value="{0}"{1} data-tipo="">{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class FormDetalleCompCompra(forms.Form):
    cuenta = forms.IntegerField()
    centro_costo = forms.IntegerField(required=False)
    sust_tribut = forms.ChoiceField(choices=[], required=False)
    cambio_sust = forms.BooleanField(required=False)
    valor_iva = forms.ChoiceField(choices=[], widget=SelectCompras)
    bs = forms.ChoiceField(choices=tipo, label="B/S")
    fte = forms.IntegerField(label="FTE", required=False)
    porc_ret_fte = forms.FloatField(label="% Ret Fte.", required=False)
    iva = forms.IntegerField(required=False, label="IVA")
    porc_ret_iva = forms.FloatField(label="% Ret IVA.", required=False)

    valor = forms.FloatField(label="Valor", validators=[validate_valor])

    def clean(self):
        """
            Función para validar el formulario
        """

        ret_fte = self.cleaned_data.get('fte')
        ret_iva = self.cleaned_data.get('iva')

        porc_fte = self.cleaned_data.get('porc_ret_fte')
        porc_iva = self.cleaned_data.get('porc_ret_iva')

        #Retencion Fte
        try:
            cod_ret_fte = Codigo_SRI.objects.get(id=ret_fte)
            if cod_ret_fte.porc_variable:
                if porc_fte in (None, ""):
                    self._errors["porc_ret_fte"] = u"Dato obligatorio"
        except (Codigo_SRI.DoesNotExist, ValueError):
            pass

        #Retencion IVA
        try:
            cod_ret_iva = Codigo_SRI.objects.get(id=ret_iva)
            if cod_ret_iva.porc_variable:
                if porc_iva in (None, ""):
                    self._errors["porc_ret_iva"] = u"Dato obligatorio"
        except (Codigo_SRI.DoesNotExist, ValueError):
            pass

        return self.cleaned_data

    def getCuenta(self):
        return self.data["cuenta"]

    def getCentroCosto(self):
        return self.data["centro_costo"]

    def getValorIva(self):
        return self.data["valor_iva"]

    def getFte(self):
        return self.data["fte"]

    def getIva(self):
        return self.data["iva"]

    def getBS(self):
        return self.data["bs"]

    def getValor(self):
        return self.data["valor"]

    def __init__(self, *args, **kwargs):
        super(FormDetalleCompCompra, self).__init__(*args, **kwargs)
        try:
            fecha_codigos_sri = SecuenciaTipoComprobante.objects.get(tipo_comprobante_id=1).fecha_actual
        except:
            fecha_codigos_sri = get_parametros_empresa().fecha_inicio_sistema
            if fecha_codigos_sri is None:
                fecha_codigos_sri = datetime.date.today()

        self.fields['cuenta'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['cuenta'].widget.attrs['data-url'] = reverse("buscar_cuenta_codigo")

        self.fields['sust_tribut'].widget.attrs['class'] = "selectpicker show-tick sust_tribut"
        self.fields['sust_tribut'].widget.attrs['data-style'] = "slc-primary"
        self.fields['sust_tribut'].widget.attrs['data-name'] = "sust_tribut"
        self.fields['sust_tribut'].widget.attrs['data-width'] = "100%"
        self.fields['sust_tribut'].widget.attrs['data-size'] = "auto"
        self.fields['sust_tribut'].widget.attrs['style'] = "32px"
        self.fields['sust_tribut'].choices = [(x.codigo, x.codigo+u" - "+x.descripcion) for x in Sustento_Tributario.objects.filter(status=1)]

        self.fields['cambio_sust'].widget.attrs['title'] = "Marcar para cambiar el sustento tributario de este registro"
        self.fields['cambio_sust'].widget.attrs['class'] = "check_sust"

        self.fields['centro_costo'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['centro_costo'].widget.attrs['data-url'] = reverse("buscar_centro_costo")

        self.fields['bs'].widget.attrs['class'] = "b_s selectpicker show-tick"
        self.fields['bs'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['bs'].widget.attrs['data-name'] = "bien_serv"
        self.fields['bs'].widget.attrs["style"] = "min-width:100%;"
        self.fields['bs'].widget.attrs["data-width"] = "70px"

        self.fields['valor'].widget.attrs['class'] = "numerico txt-cantidad valor"
        self.fields['valor'].widget.attrs['maxlength']="10"
        # self.fields['valor'].widget.attrs['placeholder']="pepin"

        self.fields['fte'].widget.attrs['class'] = "ret_fte campo_retencion"
        self.fields['fte'].widget.attrs['data-url'] = reverse("buscar_codigo_sri_fte")

        q1 = VigenciaRetencionSRI.objects.get(tipo_codigo_sri=Tipos_Retencion.objects.get(id=1),
                                              fecha_inicio__lte=fecha_codigos_sri,
                                              fecha_final__gte=fecha_codigos_sri)

        query = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion=q1).order_by("orden", "codigo")[0]

        self.fields['fte'].widget.attrs['value'] = query.id

        self.fields['porc_ret_fte'].widget.attrs['value'] = query.porcentaje

        self.fields['iva'].widget.attrs['class'] = "ret_iva campo_retencion"
        self.fields['iva'].widget.attrs['data-url'] = reverse("buscar_codigo_sri_iva")

        self.fields['valor_iva'].widget.attrs['class'] = "selectpicker cod_ret_com iva show-tick"
        q1 = VigenciaRetencionSRI.objects.get(tipo_codigo_sri=Tipos_Retencion.objects.get(id=2),
                                              fecha_inicio__lte=fecha_codigos_sri,
                                              fecha_final__gte=fecha_codigos_sri)
        query = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion=q1).order_by("orden", "codigo")

        self.fields['valor_iva'].choices = [(x.id, str(x.porcentaje) + "%" + del_det_comp + x.codigo + del_det_comp
                                             + x.descripcion + del_det_comp + str(x.porcentaje) + del_det_comp
                                             + x.codigo + del_det_comp + str(x.porc_variable) + del_det_comp
                                             + str(convert_cero_none(x.porc_max)) + del_det_comp
                                             + str(convert_cero_none(x.porc_min))) for x in query]
        self.fields['valor_iva'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['valor_iva'].widget.attrs['data-name'] = "combo_iva"

        self.fields['porc_ret_fte'].widget.attrs['class'] = "porc_ret_fte numerico campo_retencion"
        self.fields['porc_ret_fte'].widget.attrs['readonly'] = "readonly"

        self.fields['porc_ret_iva'].widget.attrs['class'] = "porc_ret_iva numerico campo_retencion"
        self.fields['porc_ret_iva'].widget.attrs['readonly'] = "readonly"



def categories_as_choices():
    categories = []
    all_cuentas = []
    cuentas_inventario = []
    mano_obra = []
    mano_obra_transitoria = []

    for category in PlanCuenta.objects.filter(status=1, nivel=5).exclude(tipo__in=TipoCuenta.objects.filter(status=1)).order_by("codigo"):
        all_cuentas.append([category.id, category.codigo + " - " + category.descripcion])
    new_category = ["Cuentas", all_cuentas]

    for category in PlanCuenta.objects.filter(status=1, nivel=5, tipo__in=TipoCuenta.objects.filter(id=16)).order_by("codigo"):
        cuentas_inventario.append([category.id, category.codigo + " - " + category.descripcion])
    inventario_categoria = ["Cuentas de Inventario", cuentas_inventario]

    for category in PlanCuenta.objects.filter(status=1, nivel=5, tipo__in=TipoCuenta.objects.filter(id=17)).order_by("codigo"):
        mano_obra.append([category.id, category.codigo + " - " + category.descripcion])
    inventario_categoria_mano_obra = [u"Cuentas Mano de Obra - Producción", mano_obra]

    for category in PlanCuenta.objects.filter(status=1, nivel=5, tipo__in=TipoCuenta.objects.filter(id=19)).order_by("codigo"):
        mano_obra_transitoria.append([category.id, category.codigo + " - " + category.descripcion])
    inventario_categoria_mano_obra_transitoria = [u"Cuentas Mano de Obra Transitoria- Producción", mano_obra_transitoria]

    categories.append(inventario_categoria)
    categories.append(inventario_categoria_mano_obra)
    categories.append(inventario_categoria_mano_obra_transitoria)
    categories.append(new_category)

    return [("", "")] + categories


def validar_num_reten(value):
    valor = int(value)
    if valor <= 0:
        raise ValidationError(u"Número de retención no válido")
    else:
        return True


class SelectRetencionCompra(forms.Select):
    allow_multiple_selected = False

    def __init__(self, attrs=None, choices=()):
        super(SelectRetencionCompra, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)

    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        if option_value != "":
            ult_reten = VigenciaDocEmpresa.objects.get(id=option_value).sec_actual
            return format_html('<option value="{0}" {1} data-num="{2}">{3}</option>',
                               option_value,
                               selected_html,
                               ult_reten,
                               force_text(option_label))
        else:
            return format_html('<option value="{0}" {1} data-num="">{2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))


class FormRetencionCompra(forms.Form):
    serie_ret = forms.IntegerField(label="Serie", required=False)
    numero_retencion = forms.IntegerField(label=u"# Retención", validators=[validar_num_reten], required=False)
    fecha_emision_retencion = forms.DateField(label=u"F_emisión", widget=forms.DateInput(format=("%Y-%m-%d")))
    autorizacion_retencion = forms.CharField(max_length=37, label=u"Autorización", required=False)

    def getSerie(self):
        return self.data["serie_ret"]

    def getNumeroRetencion(self):
        return self.data["numero_retencion"]

    def getFechaEmisionRetencion(self):
        try:
            return self.cleaned_data["fecha_emision_retencion"]
        except:
            return None

    def getAutorizacionRetencion(self):
        return self.data["autorizacion_retencion"]

    def __init__(self, *args, **kwargs):
        super(FormRetencionCompra, self).__init__(*args, **kwargs)
        self.fields['fecha_emision_retencion'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_emision_retencion'].widget.attrs['maxlength']="10"
        self.fields['fecha_emision_retencion'].widget.attrs['data-date-format']="yyyy-mm-dd"
        #self.fields['fecha_emision_retencion'].widget.attrs['readonly'] = True

        self.fields['autorizacion_retencion'].widget.attrs['readonly'] = True
        self.fields['autorizacion_retencion'].widget.attrs['class'] = "txt-desactivado"

        #self.fields['serie_ret'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['serie_ret'].widget.attrs['data-url'] = reverse("buscar_block_retenciones")


class DetalleRetencionForm(forms.Form):
    tipo = forms.CharField(max_length=20, required=True, label="tipo")
    cod_sri = forms.IntegerField(label="cod_sri")
    codigo_ret = forms.CharField(max_length=20, required=True, label="cod_ret")
    cod_cta = forms.IntegerField(required=True)
    base = forms.FloatField(label="", required=True)
    porcentaje = forms.FloatField(label="", required=True)
    total = forms.FloatField(label="", required=True)

    def getCodigo(self):
        return self.data["codigo_ret"]
    def __init__(self, *args, **kwargs):
        super(DetalleRetencionForm, self).__init__(*args, **kwargs)
        self.fields['codigo_ret'].widget.attrs['class'] = "codigo_ret"

        self.fields['tipo'].widget.attrs['class'] = "tipo"
        self.fields['tipo'].widget.attrs['readonly'] = True

        self.fields['base'].widget.attrs['readonly'] = True
        self.fields['base'].widget.attrs['class'] = "base  txt-desactivado"

        self.fields['porcentaje'].widget.attrs['readonly'] = True
        self.fields['porcentaje'].widget.attrs['class'] = "porcentaje  txt-desactivado"

        self.fields['total'].widget.attrs['readonly'] = "readonly"
        self.fields['total'].widget.attrs['class'] = "total  txt-desactivado"

        """
        self.fields['cod_cta'].widget.attrs['data-name'] = "cod_cta"
        self.fields['cod_cta'].widget.attrs['class'] = "show-tick"
        self.fields['cod_cta'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cod_cta'].choices = [(x.id, x.plan_cuenta.descripcion) for x in Cuenta_Retencion.objects.filter(status=1)]
        self.fields['cod_sri'].widget.attrs['class'] = "cod_sri"
        """
        self.fields['cod_cta'].widget.attrs['class'] = "ajaxcombobox"
        self.fields['cod_cta'].widget.attrs['data-url'] = reverse("buscar_cuentas_retencion")


class FormReembolsoGasto(forms.Form):
    tipo_identificacion = forms.ChoiceField(choices=[], label="Tipo de Identificación Proveedor")
    proveedor = forms.CharField(max_length=100, label="Proveedor")
    tipo_documento = forms.ChoiceField(choices=[], label="Tipo Documento")
    serie = forms.CharField(label="# Documento")
    fecha_emision = forms.DateField(label="Fecha de Emisión", widget=forms.DateInput(format=("%Y-%m-%d")))
    num_autorizacion = forms.CharField(label="Nº Autorización")
    base_cero = forms.CharField(label="Base 0%", required=False)
    base_iva = forms.CharField(label="Base IVA", required=False)

    def getTipo_Identificacion(self):
        return self.data["tipo_identificacion"]

    def getProveedor(self):
        return self.data["proveedor"]

    def __init__(self, *args, **kwargs):
        super(FormReembolsoGasto, self).__init__(*args, **kwargs)
        self.fields['tipo_documento'].choices = [(x.id , x.codigo_documento+" - "+x.descripcion_documento) for x in Documento.objects.exclude(status=0).order_by("codigo_documento")]
        self.fields['tipo_documento'].widget.attrs['class'] = "selectpicker tipo_documento"
        self.fields['tipo_documento'].widget.attrs['data-id'] = "data_tipo_doc"
        self.fields['tipo_documento'].widget.attrs['data-style'] = "slc-c-c"
        self.fields['tipo_documento'].widget.attrs['data-size'] = "8"
        self.fields['tipo_documento'].widget.attrs['data-name'] = "combo_tipo_doc"

        self.fields['tipo_identificacion'].choices = [(x.codigo, x.codigo+"-"+x.descripcion) for x in Identificacion.objects.exclude(status=0).exclude(codigo='F')]
        self.fields['tipo_identificacion'].widget.attrs['class'] = "selectpicker tipo_identificacion combo"
        self.fields['tipo_identificacion'].widget.attrs['data-style']="slc-c-c"
        self.fields['tipo_identificacion'].widget.attrs['data-name']="combo_tipo_id"
        self.fields['tipo_identificacion'].widget.attrs['data-width']="100%"

        self.fields['proveedor'].widget.attrs['class'] = "proveedor numerico"
        self.fields['proveedor'].widget.attrs['placeholder'] = "Número de Documento"

        self.fields['num_autorizacion'].widget.attrs['maxlength'] = "37"
        self.fields['num_autorizacion'].widget.attrs['class'] = "num_autorizacion numero_natural"

        self.fields['fecha_emision'].widget.attrs['class'] = "fecha_emision"
        self.fields['fecha_emision'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_emision'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_emision'].widget.attrs['placeholder'] = "aaaa-mm-dd"

        self.fields['serie'].widget.attrs['class'] = "serie numerico"

        self.fields['base_cero'].widget.attrs['class'] = "base_cero numerico"
        self.fields['base_iva'].widget.attrs['class'] = "base_iva numerico"

        # Ancho de controles tabla detalle compra asiento

        self.fields['tipo_identificacion'].widget.attrs['data-width'] = "100%"
        self.fields['proveedor'].widget.attrs["style"] = "min-width:115px;"
        self.fields['proveedor'].widget.attrs['maxlength'] = "13"


#estados = (("", ""), (1, "Activo"), (2, "Anulado"), (3, "Pendiente"))


class FormBuscador(forms.Form):
    fecha = forms.DateTimeField(label='Fecha Inicial: ', required=False)
    fecha_final = forms.DateTimeField(label='Fecha Final: ', required=False)
    prov = forms.ChoiceField(label="Proveedor: ", required=False)
    tipo_documento = forms.ChoiceField(choices=[], label="Tipo de Documento: ", required=False)
    ndocumento = forms.CharField(label="# Documento: ", required=False)
    rango_monto_ini = forms.FloatField(label="Rango de Monto: ", required=False)
    rango_monto_hasta = forms.FloatField(label="Hasta: ", required=False)
    num_comprobante = forms.CharField(max_length=30, label="# de Comprobante:", required=False)
    num_retencion = forms.CharField(max_length=20, label=u"# de Retención:", required=False)

    cuenta = forms.ChoiceField(choices=[], label="Cuenta Contable:", required=False)
    codigo_iva = forms.ChoiceField(choices=[], label=u"Código IVA:", required=False)
    codigo_ret_fte = forms.ChoiceField(choices=[], label=u"Código Ret. Fte.:", required=False)
    codigo_ret_iva = forms.ChoiceField(choices=[], label=u"Código Ret. IVA:", required=False)
    fpago = forms.ChoiceField(choices=[], label="Forma de Pago: ", required=False)
    concepto = forms.CharField(max_length=250, label="Concepto", required=False)
    estado = forms.ChoiceField(choices=[], label="Estado", required=False)
    estado_doc_elect = forms.ChoiceField(choices=[], label=u"Estado Documento Electrónico", required=False)
    page = forms.IntegerField(required=False)

    def getFecha(self):
        try:
            value = str(self.data['fecha']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return None

    def getFechaFinal(self):
        try:
            value = str(self.data['fecha_final']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()

        except:
            return datetime.datetime.now().date()

    def getProveedor(self):
        try:
            value = self.data['prov']
            return Proveedores.objects.get(id=value)
        except:
            return None

    def getTipoDoc(self):
        try:
            value = self.data['tipo_documento']
            return Documento.objects.get(id=value)
        except:
            return None

    def getNumeroDocumento(self):
        try:
            value = self.data['ndocumento']
            return value
        except:
            return ""

    def getRangoMontoIni(self):
        try:
            value = self.data['rango_monto_ini']
            return float(value)
        except:
            return 0

    def getRangoMontoHasta(self):
        try:
            value = self.data['rango_monto_hasta']
            return float(value)
        except:
            return 100000000000

    def getNumComprobante(self):
        try:
            value = self.data['num_comprobante']
            return value
        except:
            return ""

    def getNumRetencion(self):
        try:
            value = self.data['num_retencion']
            return value
        except:
            return ""

    def getCuenta(self):
        try:
            value = self.data['cuenta']
            return PlanCuenta.objects.get(id=value)
        except:
            return None

    def getCodigoIva(self):
        try:
            value = self.data['codigo_iva']
            return Codigo_SRI.objects.get(id=value)
        except:
            return None

    def getCodigoRetFte(self):
        try:
            value = self.data['codigo_ret_fte']
            return Codigo_SRI.objects.get(id=value)
        except:
            return None

    def getCodigoRetIVA(self):
        try:
            value = self.data['codigo_ret_iva']
            return Codigo_SRI.objects.get(id=value)
        except:
            return None

    def getFormaPago(self):
        try:
            value = self.data['fpago']
            return TipoPago.objects.get(id=value)
        except:
            return None

    def getConcepto(self):
        try:
            value = self.data['concepto']
            return value
        except:
            return ""

    def getEstado(self):
        try:
            value = self.data['estado']
            return value
        except:
            return ""

    def getEstadoDocElect(self):
        try:
            value = self.data['estado_doc_elect']
            return value
        except:
            return ""


    def __init__(self, *args, **kwargs):
        super(FormBuscador, self).__init__(*args, **kwargs)
        self.fields['fecha'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['placeholder']="aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class']="control-fecha numerico datepicker"

        self.fields['fecha_final'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_final'].widget.attrs['class'] = "control-fecha numerico datepicker"

        self.fields['prov'].widget.attrs['class']="selectpicker show-tick"
        self.fields['prov'].widget.attrs['data-style'] = "slc-primary"
        self.fields['prov'].widget.attrs['data-width'] = "100%"
        self.fields['prov'].widget.attrs['data-live-search'] = True
        self.fields['prov'].choices = [('', '')] + [(x.id, x.razon_social[0:30]) for x in Proveedores.objects.exclude(status=0).order_by("razon_social")]

        self.fields['tipo_documento'].choices = [("", "")] + [(x.id, x.codigo_documento+" - "+x.descripcion_documento) for x in Documento.objects.filter(status=1).order_by("codigo_documento")]
        self.fields['tipo_documento'].widget.attrs['class']="selectpicker show-tick"
        self.fields['tipo_documento'].widget.attrs['data-style']="slc-primary txt-150"
        self.fields['tipo_documento'].widget.attrs['data-width']="100%"

        self.fields['ndocumento'].widget.attrs['placeholder']="001-001-000000000"

        self.fields['rango_monto_ini'].widget.attrs['placeholder'] = "0.00"
        self.fields['rango_monto_ini'].widget.attrs['class'] = "numerico"

        self.fields['rango_monto_hasta'].widget.attrs['placeholder'] = "0.00"
        self.fields['rango_monto_hasta'].widget.attrs['class'] = "numerico"

        self.fields['num_comprobante'].widget.attrs['placeholder'] = "cmp20XX000000"

        self.fields['num_retencion'].widget.attrs['placeholder']="001-001-000000000"

        self.fields['cuenta'].choices = [("","")] + [(x.id,x.codigo+' - '+ x.descripcion) for x in PlanCuenta.objects.filter(status=1)]
        self.fields['cuenta'].widget.attrs['class']="selectpicker show-tick"
        self.fields['cuenta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta'].widget.attrs['data-style']="slc-primary"
        self.fields['cuenta'].widget.attrs['data-width']="100%"

        self.fields['codigo_iva'].choices = [("","")] + [(x.id, x.codigo + " - " + str(x.porcentaje) + "% - " + x.descripcion) for x in Codigo_SRI.objects.filter(status=1, vigencia_retencion__in=VigenciaRetencionSRI.objects.filter(tipo_codigo_sri=Tipos_Retencion.objects.get(id=2))).distinct("codigo")]
        self.fields['codigo_iva'].widget.attrs['class']="selectpicker show-tick"
        self.fields['codigo_iva'].widget.attrs['data-style']="slc-primary"
        self.fields['codigo_iva'].widget.attrs['data-width']="100%"

        self.fields['codigo_ret_fte'].choices = [("","")] + [(x.id, x.codigo + " - " + str(x.porcentaje) + "% - " + x.descripcion) for x in Codigo_SRI.objects.filter(status=1, vigencia_retencion__in=VigenciaRetencionSRI.objects.filter(tipo_codigo_sri=Tipos_Retencion.objects.get(id=1))).distinct("codigo")]
        self.fields['codigo_ret_fte'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['codigo_ret_fte'].widget.attrs['data-style'] = "slc-primary"
        self.fields['codigo_ret_fte'].widget.attrs['data-width'] = "100%"

        self.fields['codigo_ret_iva'].choices = [("","")] + [(x.id, x.codigo + "-" + str(x.porcentaje) + "% - " + x.descripcion) for x in Codigo_SRI.objects.filter(status=1, vigencia_retencion__in=VigenciaRetencionSRI.objects.filter(tipo_codigo_sri=Tipos_Retencion.objects.get(id=4))).distinct("codigo")]
        self.fields['codigo_ret_iva'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['codigo_ret_iva'].widget.attrs['data-style']="slc-primary"
        self.fields['codigo_ret_iva'].widget.attrs['data-width']="100%"

        self.fields['fpago'].choices = [("","")] + [(x.id, x.descripcion) for x in TipoPago.objects.filter(status=1)]
        self.fields['fpago'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['fpago'].widget.attrs['data-style'] = "slc-primary"
        self.fields['fpago'].widget.attrs['data-width'] = "100%"

        self.fields['estado'].choices = [("", "")] + [(x.id, x.descripcion) for x in Estados.objects.filter(status=1).exclude(codigo=0)]
        self.fields['estado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado'].widget.attrs['data-width'] = "100%"

        self.fields['estado_doc_elect'].choices = [('', '')] + [(x.codigo, x.descripcion) for x in EstadoDocElectronico.objects.using("base_central").filter(status=1).exclude(status=0)]
        self.fields['estado_doc_elect'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado_doc_elect'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado_doc_elect'].widget.attrs['data-width'] = "100%"

        self.fields['concepto'].widget.attrs['style'] = "width: 100%"


class FormCambiarRetencion(forms.Form):
    id_ret = forms.IntegerField()
    serie = forms.ChoiceField(choices=[], label=u'Serie: ')
    numero_ret = forms.CharField(max_length=9, label=u'Número de retención: ')
    nuevo_numero_ret = forms.CharField(max_length=9, label=u'Nuevo número de retención: ')
    fecha_emision = forms.DateTimeField(label=u'Fecha Emisión: ')

    def getId(self):
        return self.data['id_ret']

    def getSerie(self):
        return self.data['serie']

    def getNumeroRetencion(self):
        return self.data['numero_ret']

    def getNuevoNumeroRetencion(self):
        return self.data['nuevo_numero_ret']

    def getFechaEmi(self):
        value = self.data['fecha_emision']
        try:
            a = str(value).split("-")
            return datetime.datetime(int(a[0]), int(a[1]), int(a[2]))
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(FormCambiarRetencion, self).__init__(*args, **kwargs)
        self.fields['serie'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1, documento_sri=Documento.objects.get(id=24)).order_by("serie")]
        self.fields['serie'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['serie'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['serie'].widget.attrs['data-width'] = "100%"
        self.fields['numero_ret'].widget.attrs['readonly'] = True
        self.fields['fecha_emision'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_emision'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_emision'].widget.attrs['class'] = "input-calendario numerico txt-100pc"
        self.fields['numero_ret'].widget.attrs['class'] = "numerico txt-100pc"
        self.fields['nuevo_numero_ret'].widget.attrs['class'] = "numerico txt-100pc"


class FormAnularRetencion(forms.Form):
    documento = forms.ChoiceField(choices=[], label=u'Serie Documento: ')
    fecha_retencion = forms.DateField(label="Fecha", widget=forms.DateInput(format=("%Y-%m-%d")))
    numero_ret = forms.CharField(max_length=9, label=u'Número de retención: ', required=False)

    def getDocumento(self):
        return self.data['documento']

    def getNumeroRetencion(self):
        return self.data['numero_ret']

    def get_fecha(self):
        return self.cleaned_data['fecha_retencion']

    def __init__(self, *args, **kwargs):
        super(FormAnularRetencion, self).__init__(*args, **kwargs)
        self.fields['documento'].choices = [(x.id, x.serie) for x in VigenciaDocEmpresa.objects.filter(status=1,
                                                                                                       documento_sri=Documento.objects.get(id=24),
                                                                                                       sec_actual__lt=F('sec_final'),
                                                                                                       fecha_vencimiento__gte=datetime.datetime.now()).order_by("serie")]
        self.fields['documento'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['documento'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['documento'].widget.attrs['data-width'] = "100%"
        self.fields['numero_ret'].widget.attrs['class'] = "numerico txt-100pc"
        self.fields['fecha_retencion'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_retencion'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_retencion'].widget.attrs['class'] = "input-calendario numerico datepicker txt-100pc"


class FormEnvioCorreo(forms.Form):
    id_compra = forms.IntegerField()
    file = forms.FileField(label=u"Archivo Adjunto", required=False)
    cc = forms.EmailField(max_length=500, required=False)

    def get_id(self):
        return self.data["id_compra"]

    def get_file(self):
        return self.data["file"]

    def get_cc(self):
        return self.data["cc"]

    def __init__(self, *args, **kwargs):
        super(FormEnvioCorreo, self).__init__(*args, **kwargs)
        self.fields['file'].widget.attrs['class'] = "txt-100pc"
        self.fields['cc'].widget.attrs['class'] = "txt-100pc"
        self.fields['cc'].widget.attrs['placeholder'] = "cc1@dominio.com; cc2@dominio.com"