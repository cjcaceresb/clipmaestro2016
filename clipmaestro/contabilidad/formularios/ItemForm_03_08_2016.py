#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *


tipo = (('01', '12%'),
        ("02", '0%'),
        ("03", 'No objeto'),)

class ItemFormReadOnly(forms.Form):
    codigo = forms.CharField(max_length=50)
    categoria = forms.ChoiceField(choices=[])
    nombre = forms.CharField(widget=forms.Textarea,max_length=250, validators=[validador_espacios])
    descripcion = forms.CharField(max_length=250, widget=forms.Textarea, label="Descripción", validators=[validador_espacios])
    min_inventario = forms.IntegerField(required=False)
    unidad = forms.ChoiceField(choices=[])
    iva = forms.ChoiceField(choices=tipo)
    precio1 = forms.FloatField(label="PvP1")
    precio2 = forms.FloatField(label="PvP2")
    precio3 = forms.FloatField(label="PvP3")
    costo = forms.FloatField(label="Costo", required=False)
    ice_valor = forms.FloatField(required=False, label="ICE")

    def __init__(self, *args, **kwargs):
        super(ItemFormReadOnly, self).__init__(*args, **kwargs)
        self.fields['codigo'].widget.attrs['maxlength'] = "50"
        self.fields['categoria'].choices = [("","")]+[(x.id, x.descripcion) for x in Categoria_Item.objects.filter(status=1)]
        self.fields['categoria'].widget.attrs['class']="selectpicker show-tick "
        self.fields['categoria'].widget.attrs['data-style']="slc-primary"
        self.fields['unidad'].choices = [("","")]+[(x.id, x.descripcion) for x in Unidad.objects.filter(status=1)]
        self.fields['unidad'].widget.attrs['class']="selectpicker show-tick "
        self.fields['unidad'].widget.attrs['data-style']="slc-primary"
        self.fields['unidad'].widget.attrs['data-live-search'] = True
        self.fields['unidad'].widget.attrs['data-size'] = 18
        self.fields['iva'].widget.attrs['class']="selectpicker show-tick "
        self.fields['iva'].widget.attrs['data-style']="slc-primary"
        self.fields['ice_valor'].widget.attrs['class'] = "numerico valor_ice"
        self.fields['ice_valor'].widget.attrs['style'] = "text-align:right"
        self.fields['ice_valor'].widget.attrs['placeholder'] = "ICE  %"
        self.fields['min_inventario'].widget.attrs['style'] = "text-align:right"
        self.fields['min_inventario'].widget.attrs['class'] = "numerico2"
        self.fields['precio1'].widget.attrs['style'] = "text-align:right"
        self.fields['precio2'].widget.attrs['style'] = "text-align:right"
        self.fields['precio3'].widget.attrs['style'] = "text-align:right"
        self.fields['precio1'].widget.attrs['class'] = "numerico p1"
        self.fields['precio2'].widget.attrs['class'] = "numerico p2"
        self.fields['precio3'].widget.attrs['class'] = "numerico p3"
        self.fields['costo'].widget.attrs['class'] = "numerico"
        self.fields['costo'].widget.attrs['style'] = "text-align:right"

        self.fields['categoria'].widget.attrs['readonly'] = True
        self.fields['categoria'].widget.attrs['disabled'] = True

        self.fields['unidad'].widget.attrs['readonly'] = True
        self.fields['unidad'].widget.attrs['disabled'] = True

        self.fields['min_inventario'].widget.attrs['readonly'] = True
        self.fields['nombre'].widget.attrs['readonly'] = True
        self.fields['nombre'].widget.attrs['style'] = "height: 110px;"
        self.fields['codigo'].widget.attrs['readonly'] = True
        self.fields['descripcion'].widget.attrs['readonly'] = True

        self.fields['iva'].widget.attrs['readonly'] = True
        self.fields['iva'].widget.attrs['disabled'] = True


        self.fields['costo'].widget.attrs['readonly'] = True
        self.fields['precio1'].widget.attrs['readonly'] = True
        self.fields['precio2'].widget.attrs['readonly'] = True
        self.fields['precio3'].widget.attrs['readonly'] = True
        self.fields['ice_valor'].widget.attrs['readonly'] = True

class BusquedaItemForm(forms.Form):
    categoria = forms.ChoiceField(label=u"Categoría", choices=[])
    codigo = forms.CharField(label=u"Código")
    nombre = forms.CharField(label=u"Nombre")
    estado = forms.ChoiceField(choices=[], label=u"Estado")
    page = forms.IntegerField()

    def getCodigo(self):
        try:
            value = self.data['codigo']
            return value
        except:
            return ""

    def getNombre(self):
        try:
            value = self.data['nombre']
            return value
        except:
            return ""

    def getCategoria(self):
        try:
            value = self.data['categoria']
            return Categoria_Item.objects.get(id=value)
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(BusquedaItemForm, self).__init__(*args, **kwargs)
        self.fields['categoria'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['categoria'].choices = [("", "")] + [(x.id, x.descripcion) for x in Categoria_Item.objects.filter(status=1)]
        self.fields['categoria'].widget.attrs['data-live-search'] = True
        self.fields['categoria'].widget.attrs['title'] = "Seleccione una Categoría"
        self.fields['categoria'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['categoria'].widget.attrs['data-width'] = "100%"
        self.fields['categoria'].widget.attrs['data-size'] = "auto"

class itemForm(forms.Form):
    codigo = forms.CharField(max_length=50)
    categoria = forms.ChoiceField(choices=[])
    nombre = forms.CharField(max_length=200, validators=[validador_espacios])
    descripcion = forms.CharField(widget=forms.Textarea, label=u"Descripción", validators=[validador_espacios], required=False)
    min_inventario = forms.IntegerField(required=False)
    unidad = forms.ChoiceField(choices=[])
    iva = forms.ChoiceField(choices=tipo)
    precio1 = forms.FloatField(label="PvP1", required=False)
    precio2 = forms.FloatField(label="PvP2", required=False)
    precio3 = forms.FloatField(label="PvP3", required=False)
    costo = forms.FloatField(label="Costo", required=False)
    ice_valor = forms.FloatField(required=False, label="ICE")

    def getNombre(self):
        return self.data["nombre"]
    def getDescripcion(self):
        return self.data["descripcion"]
    def getCodigo(self):
        return self.data["codigo"]
    def getCategoria(self):
        return self.data["categoria"]
    def getUnidad(self):
        return self.data["unidad"]
    def getMinInventario(self):
        return self.data["min_inventario"]
    def getIceValor(self):
        return self.data["ice_valor"]
    def getIva(self):
        return self.data["iva"]
    def getPrecio1(self):
        return self.data["precio1"]
    def getPrecio2(self):
        return self.data["precio2"]
    def getPrecio3(self):
        return self.data["precio3"]

    def getCosto(self):
        return self.data["costo"]

    def __init__(self, *args, **kwargs):
        super(itemForm, self).__init__(*args, **kwargs)
        self.fields['codigo'].widget.attrs['maxlength'] = "50"
        self.fields['nombre'].widget.attrs['maxlength'] = "100"
        self.fields['nombre'].widget.attrs['placeholder'] = "NOTA: La longitud Máxima para el" \
                                                                 " nombre del item es de 100 carácteres."

        self.fields['descripcion'].widget.attrs['maxlength'] = "200"
        self.fields['descripcion'].widget.attrs['placeholder'] = "NOTA: La longitud Máxima para la" \
                                                                 " descripción del item es de 200 carácteres."

        self.fields['categoria'].choices = [("", "")]+[(x.id, x.descripcion) for x in Categoria_Item.objects.filter(status=1)]
        self.fields['categoria'].widget.attrs['class'] = "selectpicker show-tick "
        self.fields['categoria'].widget.attrs['data-style'] = "slc-primary"

        self.fields['unidad'].choices = [("","")]+[(x.id, x.descripcion) for x in Unidad.objects.filter(status=1)]
        self.fields['unidad'].widget.attrs['class']="selectpicker show-tick "
        self.fields['unidad'].widget.attrs['data-style']="slc-primary"
        self.fields['unidad'].widget.attrs['data-live-search'] = True
        self.fields['unidad'].widget.attrs['data-size'] = 18

        self.fields['iva'].widget.attrs['class']="selectpicker show-tick "
        self.fields['iva'].widget.attrs['data-style']="slc-primary"

        self.fields['ice_valor'].widget.attrs['class'] = "numerico valor_ice"
        self.fields['ice_valor'].widget.attrs['style'] = "text-align:right"
        self.fields['ice_valor'].widget.attrs['placeholder'] = "ICE  %"

        self.fields['min_inventario'].widget.attrs['style'] = "text-align:right"
        self.fields['min_inventario'].widget.attrs['class'] = "numerico2"
        self.fields['min_inventario'].widget.attrs['maxlength'] = "8"

        self.fields['precio1'].widget.attrs['style'] = "text-align:right"
        self.fields['precio2'].widget.attrs['style'] = "text-align:right"
        self.fields['precio3'].widget.attrs['style'] = "text-align:right"

        self.fields['precio1'].widget.attrs['class'] = "numerico p1"
        self.fields['precio2'].widget.attrs['class'] = "numerico p2"
        self.fields['precio3'].widget.attrs['class'] = "numerico p3"

        self.fields['costo'].widget.attrs['class'] = "numerico"
        self.fields['costo'].widget.attrs['style'] = "text-align:right"
        self.fields['costo'].widget.attrs['readonly'] = True




