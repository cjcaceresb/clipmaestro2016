#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from django.db import connection
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from datetime import date, timedelta
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from librerias.funciones.funciones_vistas import *
from django.db.models import Q
from django.utils.html import *
from django.forms.formsets import BaseFormSet
from contabilidad.formularios.VentaForm import *

__author__ = 'Roberto'

class FormBuscadoresNotaCredito(forms.Form):
    #selec_estado = (('', ''), ('1', 'Activo'), ('2', 'Anulado'), ('3', 'Pendiente'))
    fecha = forms.DateTimeField(label=u'Fecha Inicial: ', widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_final = forms.DateTimeField(label=u'Fecha Final: ', widget=forms.DateInput(format=("%Y-%m-%d")))
    num_comp = forms.CharField(label=u"# de Comprobante:")
    ndocumento = forms.CharField(label=u"# Documento: ", required=True)
    cliente = forms.ChoiceField(choices=[], label="Cliente: ")
    referencia = forms.CharField(max_length=250, label="Referencia:")
    item = forms.ChoiceField(label=u"Item:", choices=[])
    rango_monto_ini = forms.FloatField(label=u"Rango de Monto: ", required=False, validators=[validar_valor])
    rango_monto_hasta = forms.FloatField(label=u"Hasta: ", required=False, validators=[validar_valor])
    estado = forms.ChoiceField(choices=[], label=u"Estado")
    vendedor = forms.ChoiceField(label=u"Vendedor", choices=[])
    page = forms.IntegerField()

    def getNumComprobante(self):
        try:
            value = self.data['num_comp']
            return value
        except:
            return ""

    def getFecha(self):
        try:
            value = str(self.data['fecha']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return None

    def getFechaFinal(self):
        try:
            value = str(self.data['fecha_final']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return datetime.datetime.now().date()

    def getVendedor(self):
        try:
            value = self.data['vendedor']
            return Colaboradores.objects.get(id=value)
        except:
            return None

    def getCliente(self):
        try:
            value = self.data['cliente']
            return Cliente.objects.get(id=value)
        except:
            return None

    def getItem(self):
        try:
            value = self.data['item']
            return Item.objects.get(id=value)
        except:
            return None

    def getNumeroDocumento(self):
        try:
            value = self.data['ndocumento']
            return value
        except:
            return ""

    def getRangoMontoIni(self):
        try:
            value = self.data['rango_monto_ini']
            return float(value)
        except:
            return 0

    def getRangoMontoHasta(self):
        try:
            value = self.data['rango_monto_hasta']
            return float(value)
        except:
            return 100000000000

    def getReferencia(self):
        try:
            value = self.data['referencia']
            return value
        except:
            return ""

    def getEstado(self):
        try:
            value = self.data['estado']
            return value
        except:
            return ""

    def __init__(self, *args, **kwargs):
        super(FormBuscadoresNotaCredito, self).__init__(*args, **kwargs)
        qitem = Item.objects.filter(status=1, nombre__isnull=False, descripcion__isnull=False).order_by("codigo")
        self.fields['item'].choices = [("", "")] + [(x.id, x.codigo+" - "+x.nombre) for x in qitem]
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['item'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['item'].widget.attrs['data-width'] = "100%"

        self.fields['vendedor'].choices = [("", "")] + [(x.id, x.nombres_completos+" "+x.apellidos_completos) for x in Colaboradores.objects.filter(status=1)]
        self.fields['vendedor'].widget.attrs['class']="selectpicker show-tick"
        self.fields['vendedor'].widget.attrs['data-live-search'] = True
        self.fields['vendedor'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['vendedor'].widget.attrs['data-width'] = "100%"

        self.fields['fecha'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "control-fecha"
        self.fields['fecha_final'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['class']="control-fecha"

        self.fields['estado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['estado'].widget.attrs['data-width'] = "100%"
        self.fields['rango_monto_ini'].widget.attrs['class'] = "numerico"
        self.fields['rango_monto_hasta'].widget.attrs['class'] = "numerico"
        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].choices = [('', '')] + [(x.id, x.razon_social[0:30]) for x in Cliente.objects.exclude(status=0)]
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['referencia'].widget.attrs['class'] = "txt-100pc"

        self.fields['estado'].choices = [("", "")] + [(x.id, x.descripcion) for x in Estados.objects.filter(status=1).exclude(codigo=0)]
        self.fields['estado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado'].widget.attrs['data-width'] = "100%"

        self.fields['ndocumento'].widget.attrs['placeholder'] = "0000000XX"
        self.fields['num_comp'].widget.attrs['placeholder'] = "NDC0000000XX"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_final'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['rango_monto_ini'].widget.attrs['placeholder'] = "$ 0.00"
        self.fields['rango_monto_hasta'].widget.attrs['placeholder'] = "$ 0.00"

class CabeceraVentaDevolucionForms(forms.Form):
    fecha = forms.DateField(label=u"Fecha", widget=forms.DateInput(format=("%Y-%m-%d")))
    cliente = forms.ChoiceField(choices=[], label=u"Cliente")
    concepto = forms.CharField(label=u"Concepto", max_length=250, widget=forms.Textarea, required=False)
    razon_modifica = forms.CharField(label=u"Razón de Modificación", max_length=220,
                                     widget=forms.Textarea, required=False)
    serie = forms.ChoiceField(label=u"Serie", choices=[], required=False)
    num_documentos_venta = forms.ChoiceField(required=False, choices=[], label=u"# Documentos")
    num_documento_modifica = forms.CharField(required=False, label=u"# Doc. a Modificar")
    num_autorizacion = forms.CharField(label=u"# Autorización", required=False)
    fecha_vencimiento = forms.DateTimeField(label=u"Fecha V.", required=False)

    def clean(self):
        """
            Función para validar el formulario
        """
        if not emite_docs_electronicos():
            serie_doc = self.cleaned_data.get('serie', None)

            if serie_doc is None:
                self._errors["serie"] = u"Dato Requerido"
        else:
            pass
        return self.cleaned_data

    def getFecha(self):
        return self.cleaned_data["fecha"]

    def getCliente(self):
       return self.data["cliente"]

    def getRazonModificar(self):
       return self.data["razon_modifica"]

    def getConcepto(self):
       return self.data["concepto"]

    def getSerie(self):
       return self.data["serie"]

    def getNumVentas(self):
        try:
            value = self.data['num_documentos_venta']
            return value
        except:
            return ""

    def getNumDocumentoModifica(self):
        try:
            value = self.data['num_documento_modifica']
            return value
        except:
            return ""

    def __init__(self, *args, **kwargs):
        super(CabeceraVentaDevolucionForms, self).__init__(*args, **kwargs)
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick cliente"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].choices = [("", "")]+[(x.id, x.ruc+u" - "+x.razon_social) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-size'] = "auto"

        self.fields['fecha'].widget.attrs['class'] = "calendario-gris"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "control-fecha"

        # Query que filtra el Documento que tenga cantidad de block vigente
        # y vigencia según la fecha actual del tipo de comprobante de Nota de Crédito
        tipo_comprobante = TipoComprobante.objects.get(id=22)   # Tipo Comprobante NDC (Nota de Crédito)
        try:
            secuencia_tipo = SecuenciaTipoComprobante.objects.get(tipo_comprobante=tipo_comprobante)
            fecha_actual = secuencia_tipo.fecha_actual
        except SecuenciaTipoComprobante.DoesNotExist:
            fecha_actual = datetime.datetime.now().strftime("%Y-%m-%d")

        q1 = VigenciaDocEmpresa.objects.filter(status=1).filter(fecha_emi__lte=fecha_actual, fecha_vencimiento__gte=fecha_actual, documento_sri__id=4,  sec_actual__lt=F("sec_final")).order_by("serie")
        self.fields['serie'].choices = [(x.id, x.serie) for x in q1]
        self.fields['serie'].widget.attrs['class'] = "selectpicker show-tick serie"
        self.fields['serie'].widget.attrs['data-style'] = "slc-b-s combo_serie"
        self.fields['serie'].widget.attrs['data-name'] = "combo_serie"
        self.fields['serie'].widget.attrs['data-size'] = "8"

        self.fields['num_documentos_venta'].choices = [("", ""), ("0", "DOCUMENTO NO REGISTRADO EN EL SISTEMA")]+[(x.id, x.num_documento) for x in Venta_Cabecera.objects.filter(status=1)]
        self.fields['num_documentos_venta'].widget.attrs['title'] = "Seleccione un Número de Documento"
        self.fields['num_documentos_venta'].widget.attrs['class'] = "selectpicker show-tick num_documentos_venta"
        self.fields['num_documentos_venta'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['num_documentos_venta'].widget.attrs['data-live-search'] = True
        self.fields['num_documentos_venta'].widget.attrs['data-width'] = "100%"
        self.fields['num_documentos_venta'].widget.attrs['data-size'] = "8"
        self.fields['num_documento_modifica'].widget.attrs['placeholder'] = "001-00X-0000000000"
        self.fields['razon_modifica'].widget.attrs['maxlength'] = "200"
        self.fields['fecha_vencimiento'].widget.attrs['class'] = "calendario-gris"
        self.fields['num_autorizacion'].widget.attrs['class'] = "numerico"

class BaseDetalleFormSetDevolucionAut(BaseFormSet):
    """
    Clase que ayuda a validar los formset de los items
    """
    def __init__(self, *args, **kwargs):
        super(BaseDetalleFormSetDevolucionAut, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

    def clean(self):
        """Checks that no two articles have the same title."""
        if any(self.errors):
            # Don't bother validating the formset unless each form is valid on its own
            return
        for form in self.forms:
            subtotal = form.cleaned_data.get("subtotal", None)
            cantidad_dev = form.cleaned_data.get("cant_dev", None)

            if subtotal is not None:
                try:
                    if float(subtotal) <= 0:
                        errors = form._errors.setdefault("subtotal", ErrorList())
                        errors.append(u"subtotal no válido")
                except:
                    errors = form._errors.setdefault("subtotal", ErrorList())
                    errors.append(u"subtotal no válido")

            if cantidad_dev is not None:
                try:
                    if float(subtotal) <= 0:
                        errors = form._errors.setdefault("cant_dev", ErrorList())
                        errors.append(u"cantidad a devolver no válida")
                except:
                    errors = form._errors.setdefault("cant_dev", ErrorList())
                    errors.append(u"cantidad a devolver no válida")

            if cantidad_dev is None and subtotal is None:
                errors = form._errors.setdefault("cant_dev", ErrorList())
                form._errors["cant_dev"] = u"Dato Requerido"
                errors.append(u"Ingreses una cantidad a devolver o un costo")

                errors = form._errors.setdefault("subtotal", ErrorList())
                form._errors["subtotal"] = u"Dato Requerido"
                errors.append(u"Ingrerese un costo en el subtotal o una cantidad a devolver")

class DetalleDevolucionAutomaticaForms(forms.Form):
    cantidad_costo = (('1', 'Cantidad'), ('2', 'Costo'))
    id_detalle_venta = forms.IntegerField(label=u"id_detalle")
    id_item = forms.IntegerField(label=u"id_item")
    tipo = forms.ChoiceField(choices=cantidad_costo, required=False)
    item = forms.CharField(max_length=150, label=u"Item")
    iva = forms.FloatField(label=u"iva")
    ice = forms.FloatField(label=u"ice")
    cant_vendida = forms.CharField(max_length=15, label=u"Cant. V.")
    cant_ent = forms.CharField(max_length=15, label=u"Cant. Ent.")
    precio = forms.CharField(max_length=15, label=u"Precio U.")
    desc_porc = forms.FloatField(label=u"desc_porc", required=False)
    desc_valor = forms.FloatField()
    cant_dev = forms.CharField(required=False, max_length=15, label=u"Cant. Dev.")
    subtotal = forms.FloatField(required=False)
    tipo_desc = forms.IntegerField(required=False, label=u"tipo_desc")

    def __init__(self, *args, **kwargs):
        super(DetalleDevolucionAutomaticaForms, self).__init__(*args, **kwargs)

        self.fields['tipo'].widget.attrs['class'] = "selectpicker tipo"
        self.fields['tipo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo'].widget.attrs['title'] = "Seleccione un Tipo"

        self.fields['id_item'].widget.attrs['class'] = "id_item"
        self.fields['cant_dev'].widget.attrs['class'] = "numerico cant_dev_a text-right"
        self.fields['id_detalle_venta'].widget.attrs['class'] = "id_detalle_venta_a"
        self.fields['item'].widget.attrs['class'] = "item"
        self.fields['desc_valor'].widget.attrs['class'] = "desc_valor text-right"
        self.fields['desc_porc'].widget.attrs['class'] = "desc_porc"

        self.fields['cant_vendida'].widget.attrs['class'] = "cant_vendida  text-right"
        self.fields['precio'].widget.attrs['class'] = "precio  text-right"

        self.fields['cant_vendida'].widget.attrs['style'] = "width: 100px;"
        self.fields['precio'].widget.attrs['style'] = "width: 100px;"
        self.fields['cant_ent'].widget.attrs['style'] = "width: 100px;"
        self.fields['desc_valor'].widget.attrs['style'] = "width: 100px;"
        self.fields['cant_dev'].widget.attrs['style'] = "width: 100px;"

        self.fields['item'].widget.attrs['readonly'] = True
        self.fields['cant_vendida'].widget.attrs['readonly'] = True
        self.fields['precio'].widget.attrs['readonly'] = True
        self.fields['cant_ent'].widget.attrs['readonly'] = True
        self.fields['desc_valor'].widget.attrs['readonly'] = True

        self.fields['subtotal'].widget.attrs['class'] = 'subtotal numerico text-right'
        self.fields['cant_ent'].widget.attrs['class'] = "text-right"
        self.fields['iva'].widget.attrs['class'] = "iva"
        self.fields['ice'].widget.attrs['class'] = "ice"
        self.fields['tipo_desc'].widget.attrs['class'] = "tipo_desc"
        self.fields['cant_dev'].widget.attrs['placeholder'] = "0.00"

def ValidatorItem(value):
    if value is None:
        raise ValidationError(u'Ingrese el item')
    else:
        return

class SelectItemParametrosManual(forms.Select):
    allow_multiple_selected = False
    def __init__(self, attrs=None, choices=()):
        super(SelectItemParametrosManual, self).__init__(attrs)
        # choices can be any iterable, but we may need to render this widget
        # multiple times. Thus, collapse it into a list so it can be consumed
        # more than once.
        self.choices = list(choices)
    def render_option(self, selected_choices, option_value, option_label):
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''

        if option_value != "":
            try:
                item = Item.objects.get(id=option_value)
                tipo_item = item.TipoItem()
                costo = Item.objects.get(id=option_value).costo
                iva = Item.objects.get(id=option_value).iva
                precio = Item.objects.get(id=option_value).precio1
                ice = Item.objects.get(id=option_value).porc_ice

                if costo is None:
                    costo = 0
                if iva is None:
                    iva = 0
                if precio is None:
                    precio = 0.0
                if ice is None:
                    ice = 0.0
            except:
                costo = 0.0
                iva = 0.0
                ice = 0.0
                precio = 0.0
                tipo_item = 0.0

            return format_html(u'<option value="{0}" {1} data-iva={3} data-precio={4} data-ice={5} '
                               u'data-tipo={6}> {7} </option>', option_value, selected_html,
                               costo, iva, precio, ice, tipo_item, unicode(force_text(option_label)))
        else:
            return format_html('<option value="{0}" {1}> {2}</option>',
                               option_value,
                               selected_html,
                               force_text(option_label))

class DetalleDevolucionManualForms(forms.Form):
    CHOICES = (('1', ''), ('2', ''))
    rd_cant_costo = forms.TypedChoiceField(choices=CHOICES, widget=forms.RadioSelect(), required=False)
    item = forms.ChoiceField(choices=[], label=u"Item", widget=SelectItemParametrosManual, validators=[ValidatorItem])
    precio = forms.CharField(max_length=15, label=u"Precio U.", required=False)
    cant_dev = forms.CharField(max_length=15, label=u"Cant. V.")
    subtotal = forms.CharField(max_length=15, label=u"subtotal")
    act_inven = forms.BooleanField(required=False)

    def getActInv(self):
        value = self.data['act_inven']
        return value

    def __init__(self, *args, **kwargs):
        super(DetalleDevolucionManualForms, self).__init__(*args, **kwargs)
        self.fields['cant_dev'].widget.attrs['class'] = "numerico cant_dev"
        self.fields['cant_dev'].widget.attrs['style'] = "width: 110px;"

        self.fields['precio'].widget.attrs['class'] = "numerico precio"
        self.fields['precio'].widget.attrs['style'] = "width: 120px;"

        self.fields['subtotal'].widget.attrs['class'] = "numerico subtotal"
        self.fields['subtotal'].widget.attrs['style'] = "width: 150px;"

        self.fields['item'].widget.attrs['class'] = "item"
        self.fields['act_inven'].widget.attrs['class'] = "act_inven"

        self.fields['item'].widget.attrs['class'] = "item selectpicker show-tick item"
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Item"
        self.fields['item'].choices = [("", "")] + [(x.id, unicode(x.codigo[0:15])+ "-"+unicode(x.nombre[0:15])) for x in Item.objects.filter(status=1)]
        self.fields['cant_dev'].widget.attrs['placeholder'] = "0.00"
        self.fields['precio'].widget.attrs['placeholder'] = "0.00"

class FormEnvioCorreoNotaCredito(forms.Form):
    id_nota_credito = forms.IntegerField()
    file = forms.FileField(label=u"Archivo Adjunto", required=False)
    cc = forms.EmailField(max_length=500, required=False)

    def get_id(self):
        return self.cleaned_data["id_nota_credito"]

    def get_file(self):
        return self.cleaned_data["file"]

    def get_cc(self):
        return self.cleaned_data["cc"]

    def __init__(self, *args, **kwargs):
        super(FormEnvioCorreoNotaCredito, self).__init__(*args, **kwargs)
        self.fields['file'].widget.attrs['class'] = "txt-100pc"
        self.fields['cc'].widget.attrs['class'] = "txt-100pc"
        self.fields['cc'].widget.attrs['placeholder'] = "cc1@dominio.com; cc2@dominio.com"