#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *

class BusquedaCategoriaItemForm(forms.Form):
    tipo = forms.ChoiceField(label=u"Tipo Item", choices=[])
    cuenta_inventario = forms.ChoiceField(label=u"Cuenta", choices=[])
    cuenta_venta = forms.ChoiceField(label=u"Cuenta", choices=[])
    cuenta_costo_venta = forms.ChoiceField(label=u"Cuenta", choices=[])
    descripcion = forms.CharField(label=u"Nombre")
    estado = forms.ChoiceField(choices=[], label=u"Estado")
    page = forms.IntegerField()

    def getDescripcion(self):
        try:
            value = self.data['descripcion']
            return value
        except:
            return ""

    def getTipoItem(self):
        try:
            value = self.data['tipo']
            return Tipo_Item.objects.get(id=value)
        except:
            return None

    def getCuentaInventario(self):
        try:
            value = self.data['cuenta_inventario']
            return PlanCuenta.objects.get(id=value)
        except:
            return None

    def getCuentaVenta(self):
        try:
            value = self.data['cuenta_venta']
            return PlanCuenta.objects.get(id=value)
        except:
            return None

    def getCuentaCostoVenta(self):
        try:
            value = self.data['cuenta_costo_venta']
            return PlanCuenta.objects.get(id=value)
        except:
            return None



    def __init__(self, *args, **kwargs):
        super(BusquedaCategoriaItemForm, self).__init__(*args, **kwargs)
        self.fields['tipo'].choices = [("","")]+[(x.id, x.descripcion) for x in Tipo_Item.objects.filter(status=1)]
        self.fields['tipo'].widget.attrs['class'] = "selectpicker"
        self.fields['tipo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo'].widget.attrs['title'] = "Seleccione una Categoria de Item"

        self.fields['cuenta_inventario'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_inventario'].choices = [("", "")] + [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=4)]
        self.fields['cuenta_inventario'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_inventario'].widget.attrs['title'] = "Seleccione una Cuenta de Inventario"
        self.fields['cuenta_inventario'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['cuenta_inventario'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta_inventario'].widget.attrs['data-size'] = "auto"

        self.fields['cuenta_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_venta'].choices = [("", "")] + [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=5)]
        self.fields['cuenta_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_venta'].widget.attrs['title'] = "Seleccione una Cuenta de Venta"
        self.fields['cuenta_venta'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['cuenta_venta'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta_venta'].widget.attrs['data-size'] = "auto"

        self.fields['cuenta_costo_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_costo_venta'].choices = [("", "")] + [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=6)]
        self.fields['cuenta_costo_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_costo_venta'].widget.attrs['title'] = "Seleccione una Cuenta de Costo de Venta"
        self.fields['cuenta_costo_venta'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['cuenta_costo_venta'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta_costo_venta'].widget.attrs['data-size'] = "auto"

        self.fields['descripcion'].widget.attrs['style'] = "width: 100%;"


class CategoriaItemFormReadOnly(forms.Form):
    nombre = forms.CharField(max_length=250, validators=[validador_espacios])
    tipo = forms.ChoiceField(choices=[], label=u"Tipo")
    cuenta_compra = forms.ChoiceField(choices=[], label=u"Cuenta Inventario")
    cuenta_venta = forms.ChoiceField(choices=[], label=u"Cuenta para Venta")
    cuenta_costo_venta = forms.ChoiceField(choices=[], label=u"Cuenta Costo/Venta")
    cuenta_inv_proceso = forms.ChoiceField(choices=[], label=u"Cuenta Inv. Proceso")

    cuenta_producto_terminado = forms.ChoiceField(choices=[], label=u"Cuenta Producto Terminado")
    cuenta_devolucion_venta = forms.ChoiceField(choices=[], label=u"Cuenta Devolución Venta")
    cuenta_descuento_venta = forms.ChoiceField(choices=[], label=u"Cuenta Descuento Venta")

    def __init__(self, *args, **kwargs):
        super(CategoriaItemFormReadOnly, self).__init__(*args, **kwargs)
        self.fields['tipo'].choices = [("", "")]+[(x.id, x.descripcion) for x in Tipo_Item.objects.filter(status=1)]
        self.fields['tipo'].widget.attrs['class']="selectpicker show-tick "
        self.fields['tipo'].widget.attrs['data-style']="slc-primary"

        self.fields['cuenta_compra'].choices = [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=4)]
        self.fields['cuenta_venta'].choices = [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=5)]
        self.fields['cuenta_descuento_venta'].choices = [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=6)]
        self.fields['cuenta_devolucion_venta'].choices = [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=6)]

        self.fields['cuenta_inv_proceso'].choices = [("", "")]+[(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=20)]
        self.fields['cuenta_producto_terminado'].choices = [("", "")]+[(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=21)]
        self.fields['cuenta_devolucion_venta'].choices = [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=18)]
        self.fields['cuenta_descuento_venta'].choices = [(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=10)]

        self.fields['cuenta_compra'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_compra'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_compra'].widget.attrs['data-size']= "5"
        self.fields['cuenta_compra'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_venta'].widget.attrs['data-size']= "5"
        self.fields['cuenta_venta'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_costo_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_costo_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_costo_venta'].widget.attrs['data-size']= "5"
        self.fields['cuenta_costo_venta'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_inv_proceso'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_inv_proceso'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_inv_proceso'].widget.attrs['data-size']= "5"
        self.fields['cuenta_inv_proceso'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_producto_terminado'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_producto_terminado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_producto_terminado'].widget.attrs['data-size']= "5"
        self.fields['cuenta_producto_terminado'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_devolucion_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_devolucion_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_devolucion_venta'].widget.attrs['data-size']= "5"
        self.fields['cuenta_devolucion_venta'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_descuento_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_descuento_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_descuento_venta'].widget.attrs['data-size']= "5"
        self.fields['cuenta_descuento_venta'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_compra'].widget.attrs['readonly'] = True
        self.fields['cuenta_compra'].widget.attrs['disabled'] = True

        self.fields['nombre'].widget.attrs['readonly'] = True

        self.fields['tipo'].widget.attrs['readonly'] = True
        self.fields['tipo'].widget.attrs['disabled'] = True

        self.fields['cuenta_venta'].widget.attrs['readonly'] = True
        self.fields['cuenta_venta'].widget.attrs['disabled'] = True

        self.fields['cuenta_descuento_venta'].widget.attrs['readonly'] = True
        self.fields['cuenta_descuento_venta'].widget.attrs['disabled'] = True
        self.fields['cuenta_devolucion_venta'].widget.attrs['readonly'] = True
        self.fields['cuenta_devolucion_venta'].widget.attrs['disabled'] = True
        self.fields['cuenta_producto_terminado'].widget.attrs['readonly'] = True
        self.fields['cuenta_producto_terminado'].widget.attrs['disabled'] = True
        self.fields['cuenta_inv_proceso'].widget.attrs['readonly'] = True
        self.fields['cuenta_inv_proceso'].widget.attrs['disabled'] = True
        self.fields['cuenta_costo_venta'].widget.attrs['readonly'] = True
        self.fields['cuenta_costo_venta'].widget.attrs['disabled'] = True

class categoriaItemForm(forms.Form):
    nombre = forms.CharField(max_length=250, validators=[validador_espacios])
    tipo = forms.ChoiceField(choices=[], label=u"Tipo")
    cuenta_compra = forms.ChoiceField(choices=[], label=u"Cuenta Inventario", required=False)
    cuenta_venta = forms.ChoiceField(choices=[], label=u"Cuenta para Venta")
    cuenta_costo_venta = forms.ChoiceField(choices=[], label=u"Cuenta Costo/Venta", required=False)
    cuenta_inv_proceso = forms.ChoiceField(choices=[], label=u"Cuenta Inv. Proceso", required=False)
    cuenta_producto_terminado = forms.ChoiceField(choices=[], label=u"Cuenta Producto Terminado", required=False)
    cuenta_devolucion_venta = forms.ChoiceField(choices=[], label=u"Cuenta Devolución Venta")
    cuenta_descuento_venta = forms.ChoiceField(choices=[], label=u"Cuenta Descuento Venta")

    def getNombre(self):
        return self.data["nombre"]

    def geTipo(self):
       return self.data["tipo"]

    def getCuentaCompra(self):
        try:
            return self.data["cuenta_compra"]
        except:
            return None

    def getCuentaVenta(self):
        try:
            return self.data["cuenta_venta"]
        except:
            return None

    def getCuentaCostoVenta(self):
        try:
            return self.data["cuenta_costo_venta"]
        except:
            return None

    def getCuentaInventarioProceso(self):
        try:
            return self.data["cuenta_inv_proceso"]
        except:
            return None

    def getCuentaPRoductoTerminado(self):
        try:
            return self.data["cuenta_producto_terminado"]
        except:
            return None

    def getCuentaDevolucionVenta(self):
        try:
            return self.data["cuenta_devolucion_venta"]
        except:
            return None

    def getCuentaDescuentoVenta(self):
        try:
            return self.data["cuenta_descuento_venta"]
        except:
            return None

    def clean(self):
        tipo = self.cleaned_data.get("tipo", "")
        cuenta_compra = self.cleaned_data.get("cuenta_compra", "")
        #cuenta_inv_proceso = self.cleaned_data.get("cuenta_inv_proceso", "")
        #cuenta_producto_terminado = self.cleaned_data.get("cuenta_producto_terminado", "")

        if tipo == "1":
            if cuenta_compra == "":
                self._errors["cuenta_compra"] = "Dato obligatorio"
            '''
            if cuenta_inv_proceso == "":
                self._errors["cuenta_inv_proceso"] = "Dato obligatorio"
            if cuenta_producto_terminado == "":
                self._errors["cuenta_producto_terminado"] = "Dato obligatorio"
            '''

    def __init__(self, *args, **kwargs):
        super(categoriaItemForm, self).__init__(*args, **kwargs)
        self.fields['tipo'].choices = [(x.id, x.descripcion) for x in Tipo_Item.objects.filter(status=1)]
        self.fields['tipo'].widget.attrs['class']="selectpicker show-tick "
        self.fields['tipo'].widget.attrs['data-style']="slc-primary"

        self.fields['cuenta_compra'].choices = [(x.id, x.descripcion+" - "+x.codigo) for x in PlanCuenta.objects.filter(status=1,nivel=5).filter(tipo=4)]
        self.fields['cuenta_venta'].choices = [(x.id, x.descripcion+" - "+x.codigo) for x in PlanCuenta.objects.filter(status=1,nivel=5).filter(tipo=5)]
        self.fields['cuenta_costo_venta'].choices = [(x.id, x.descripcion+" - "+x.codigo) for x in PlanCuenta.objects.filter(status=1,nivel=5).filter(tipo=6)]

        self.fields['cuenta_inv_proceso'].choices = [("", "")]+[(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=20)]
        self.fields['cuenta_producto_terminado'].choices = [("", "")]+[(x.id, x.descripcion) for x in PlanCuenta.objects.filter(status=1).filter(tipo=21)]
        self.fields['cuenta_devolucion_venta'].choices = [(x.id, x.descripcion+" - "+x.codigo) for x in PlanCuenta.objects.filter(status=1,nivel=5).filter(tipo=18)]
        self.fields['cuenta_descuento_venta'].choices = [(x.id, x.descripcion+" - "+x.codigo) for x in PlanCuenta.objects.filter(status=1,nivel=5).filter(tipo=10)]

        self.fields['cuenta_compra'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_compra'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_compra'].widget.attrs['data-size'] = "5"
        self.fields['cuenta_compra'].widget.attrs['data-style'] = "slc-primary"

        self.fields['cuenta_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_venta'].widget.attrs['data-size'] = "5"
        self.fields['cuenta_venta'].widget.attrs['data-style'] = "slc-primary"

        self.fields['cuenta_costo_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_costo_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_costo_venta'].widget.attrs['data-size'] = "5"
        self.fields['cuenta_costo_venta'].widget.attrs['data-style'] = "slc-primary"

        self.fields['cuenta_inv_proceso'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_inv_proceso'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_inv_proceso'].widget.attrs['data-size'] = "5"
        self.fields['cuenta_inv_proceso'].widget.attrs['data-style'] = "slc-primary"

        self.fields['cuenta_producto_terminado'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_producto_terminado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_producto_terminado'].widget.attrs['data-size']= "5"
        self.fields['cuenta_producto_terminado'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_devolucion_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_devolucion_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_devolucion_venta'].widget.attrs['data-size']= "5"
        self.fields['cuenta_devolucion_venta'].widget.attrs['data-style']= "slc-primary"

        self.fields['cuenta_descuento_venta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta_descuento_venta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_descuento_venta'].widget.attrs['data-size']= "5"
        self.fields['cuenta_descuento_venta'].widget.attrs['data-style']= "slc-primary"
        self.fields['nombre'].widget.attrs['maxlength'] = "65"


