#! /usr/bin/python
# -*- coding: UTF-8 -*-

from django.forms.util import ErrorList
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
from contabilidad.models import *
from django.forms import *


class PlanCuentaForm (forms.Form):
    id_cta = IntegerField(required=False)
    descripcion = CharField(max_length=50, widget=forms.Textarea, label=u"Descripción")
    naturaleza = ChoiceField(choices=(("D", "Deudora"), ("C", "Acreedora")), label="Naturaleza")
    tipo_cuenta = ChoiceField(choices=[], label=u"Tipo de Cuenta", required=False)

    def getDescripcion(self):
        return self.data["descripcion"]

    def getidCta(self):
        return self.cleaned_data["id_cta"]

    def getNaturaleza(self):
        return self.data["naturaleza"]

    def get_tipo_cuenta(self):
        try:
            return TipoCuenta.objects.get(id=self.data["tipo_cuenta"])
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(PlanCuentaForm, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs['style'] = "height: 60px"
        self.fields['descripcion'].widget.attrs['maxlength'] = "50"

        self.fields['naturaleza'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['naturaleza'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['naturaleza'].widget.attrs['data-width'] = "100%"

        self.fields['id_cta'].widget.attrs['style'] = "display: none"

        self.fields['tipo_cuenta'].choices = [("", "")] + [(x.id, x.descripcion) for x in TipoCuenta.objects.filter(status=1)]
        self.fields['tipo_cuenta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_cuenta'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['tipo_cuenta'].widget.attrs['data-width'] = "100%"


