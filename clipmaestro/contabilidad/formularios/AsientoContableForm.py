#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django import forms
from django.utils.html import *
from contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from datetime import date, timedelta

solo_alfanumericas = u'[a-zA-Z0-9_]+$'

def validate_cuenta(value):
    '''
    Función Validadora que valida si se ingreso una
    cuenta en el campo del formulario
    :param value:
    :return:
    '''
    try:
        PlanCuenta.objects.get(id=value, status=1, nivel=5)
        return True
    except:
        raise ValidationError(u'La cuenta ingresada ha sido eliminada o no existe, por favor intente con otra cuenta')

class CabeceraAsientoForm(forms.Form):
    fecha = forms.DateField(label=u"Fecha", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    utlimo_asiento = forms.CharField(label=u"Último # Comprob.", required=False)
    concepto = forms.CharField(widget=forms.Textarea, label=u"Concepto")

    def getFecha(self):
        try:
            return self.cleaned_data["fecha"]
        except:
            return None

    def getConceptoComprobante(self):
        return self.data["concepto"]

    def __init__(self, *args, **kwargs):
        super(CabeceraAsientoForm, self).__init__(*args, **kwargs)
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['class'] = "calendario-gris"
        self.fields['fecha'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['utlimo_asiento'].widget.attrs['style'] = "text-align: right;"
        self.fields['utlimo_asiento'].widget.attrs['readonly'] = "True"
        self.fields['concepto'].widget.attrs['maxlength'] = "150"

class FormDetalleComprobanteAsiento(forms.Form):
    codigo_cta = forms.ChoiceField(label=u"Cuenta", validators=[validate_cuenta])
    concepto = forms.CharField(label="Concepto", max_length=200, required=False)
    debe = forms.FloatField(required=False, label="Debe")
    haber = forms.FloatField(required=False, label="Haber")
    centro_costo = forms.ChoiceField(choices=[], label="Centro de Costo", required=False)

    def getCodigo(self):
        return self.data["codigo_cta"]
    def getDescripcion(self):
        return self.data["descripcion"]
    def getDebe(self):
        return self.data["debe"]
    def getHaber(self):
        return self.data["haber"]
    def getCentroCosto(self):
        return Centro_Costo.objects.get(id=self.data["centro_costo"])

    def clean(self):
        debe = self.cleaned_data.get('debe')
        haber = self.cleaned_data.get('haber')

        #Validación del debe y haber
        if debe <= 0 and haber <= 0:
            self._errors["debe"] = u"Usted debe ingresar un valor mayor a $ 0.0"
            self._errors["haber"] = u"Usted debe ingresar un valor mayor a $ 0.0"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(FormDetalleComprobanteAsiento, self).__init__(*args, **kwargs)
        self.fields['centro_costo'].choices = [("", "")]+[(x.id, x.descripcion) for x in Centro_Costo.objects.exclude(status=0)]
        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker centro_costo"
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['centro_costo'].widget.attrs['data-name'] = "combo_centro_costo"
        self.fields['centro_costo'].widget.attrs['style'] = "text-align: right;"
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True


        # Query que bloquea las cuentas de módulo según el "TIPO CUENTA".
        '''
        q = PlanCuenta.objects.filter(status=1, nivel=5).exclude(
            tipo__in=TipoCuenta.objects.filter(status=1).filter(Q(id=11) | Q(id=15) | Q(id=22) |
                                                                Q(id=20) | Q(id=2) | Q(id=8) |
                                                                Q(id=19) | Q(id=21) | Q(id=17)).order_by("codigo"))
        '''

        # Query que selecciona todas las cuentas de nivel 5 sin restricción, a pedido de los clientes
        q = PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo")

        self.fields['codigo_cta'].choices = [("", "")]+[(x.id, x.codigo+"-"+x.descripcion) for x in q]
        self.fields['codigo_cta'].widget.attrs['class'] = "selectpicker show-tick codigo_cta"
        self.fields['codigo_cta'].widget.attrs['data-style'] = "slc-primary"
        self.fields['codigo_cta'].widget.attrs['data-width'] = "100%"
        self.fields['codigo_cta'].widget.attrs['data-live-search'] = True
        self.fields['codigo_cta'].widget.attrs['title'] = "Seleccione una Cuenta"

        self.fields['concepto'].widget.attrs['class'] = "concepto"
        self.fields['concepto'].widget.attrs['style'] = "width: 100%; height: 32px !important;"
        self.fields['concepto'].widget.attrs['maxlength'] = "75"

        self.fields['debe'].widget.attrs['placeholder'] = "0.00"
        self.fields['haber'].widget.attrs['placeholder'] = "0.00"

        self.fields['debe'].widget.attrs['style'] = "text-align: right;  width: 100%; height: 32px !important;"
        self.fields['haber'].widget.attrs['style'] = "text-align: right; width: 100%; height: 32px !important;"

        self.fields['debe'].widget.attrs['class'] = "numerico debe"
        self.fields['haber'].widget.attrs['class'] = "numerico haber"

class FormDetalleComprobanteAsientoInicial(forms.Form):
    codigo_cta = forms.ChoiceField(label=u"Cuenta", validators=[validate_cuenta])
    concepto = forms.CharField(label=u"Concepto", max_length=200, required=False)
    debe = forms.FloatField(required=False, label=u"Debe")
    haber = forms.FloatField(required=False, label=u"Haber")
    centro_costo = forms.ChoiceField(choices=[], label=u"Centro de Costo", required=False)

    def getCodigo(self):
        return self.data["codigo_cta"]
    def getDescripcion(self):
        return self.data["descripcion"]
    def getDebe(self):
        return self.data["debe"]
    def getHaber(self):
        return self.data["haber"]
    def getCentroCosto(self):
        return Centro_Costo.objects.get(id=self.data["centro_costo"])

    def clean(self):
        debe = self.cleaned_data.get('debe')
        haber = self.cleaned_data.get('haber')

        #Validación del debe y haber
        if debe <= 0 and haber <= 0:
            self._errors["debe"] = u"Usted debe ingresar un valor mayor a $ 0.0"
            self._errors["haber"] = u"Usted debe ingresar un valor mayor a $ 0.0"

        return self.cleaned_data

    def __init__(self, *args, **kwargs):
        super(FormDetalleComprobanteAsientoInicial, self).__init__(*args, **kwargs)
        self.fields['centro_costo'].choices = [("", "")]+[(x.id, x.descripcion) for x in Centro_Costo.objects.exclude(status=0)]
        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker centro_costo"
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['centro_costo'].widget.attrs['data-name'] = "combo_centro_costo"
        self.fields['centro_costo'].widget.attrs['style'] = "text-align: right;"
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True

        # TODAS LAS CUENTAS DISPONIBLES
        q = PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo")
        self.fields['codigo_cta'].choices = [("", "")]+[(x.id, x.codigo+"-"+x.descripcion) for x in q]
        self.fields['codigo_cta'].widget.attrs['class'] = "selectpicker show-tick codigo_cta"
        self.fields['codigo_cta'].widget.attrs['data-style'] = "slc-primary"
        self.fields['codigo_cta'].widget.attrs['data-width'] = "100%"
        self.fields['codigo_cta'].widget.attrs['data-live-search'] = True
        self.fields['codigo_cta'].widget.attrs['title'] = "Seleccione una Cuenta"

        self.fields['concepto'].widget.attrs['class'] = "concepto"
        self.fields['concepto'].widget.attrs['maxlength'] = "75"
        self.fields['concepto'].widget.attrs['style'] = "width: 100%; height: 32px !important;"
        self.fields['debe'].widget.attrs['placeholder'] = "0.00"
        self.fields['haber'].widget.attrs['placeholder'] = "0.00"
        self.fields['debe'].widget.attrs['style'] = "text-align: right;  width: 100%; height: 32px !important;"
        self.fields['haber'].widget.attrs['style'] = "text-align: right; width: 100%; height: 32px !important;"
        self.fields['debe'].widget.attrs['class'] = "numerico debe"
        self.fields['haber'].widget.attrs['class'] = "numerico haber"

class FormBuscadoresAsientoContable(forms.Form):
    num_comp = forms.CharField(label=u'# Comp.',)
    fecha_inicio = forms.DateField(label=u'Fecha Inicio', widget=forms.DateInput(format=("%Y-%m-%d")))
    tipo_comprobante = forms.ChoiceField(choices=[], label=u"Tipo Comprobante")
    fecha_fin = forms.DateField(label=u'Fecha Fin', widget=forms.DateInput(format=("%Y-%m-%d")))
    cuenta = forms.ChoiceField(choices=[], label=u"Cuenta")
    page = forms.IntegerField()

    def getFechaInicio(self):
        try:
            value = str(self.data['fecha_inicio']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return None

    def getFechaFin(self):
        try:
            value = str(self.data['fecha_fin']).split("-")
            return datetime.datetime(int(value[0]), int(value[1]), int(value[2])).date()
        except:
            return datetime.datetime.now().date()

    def getTipoComprobante(self):
        try:
            tipo_comprobante = self.data['tipo_comprobante']
            return TipoComprobante.objects.get(id=tipo_comprobante)
        except:
            return None

    def getCuenta(self):
        try:
            cuenta = self.data['cuenta']
            return PlanCuenta.objects.get(id=cuenta)
        except:
            return None

    def getNumComprobante(self):
        try:
            value = self.data["num_comp"]
            return str(value).strip(' \t\n\r')
        except:
            return ""


    def __init__(self, *args, **kwargs):
        super(FormBuscadoresAsientoContable, self).__init__(*args, **kwargs)
        self.fields['tipo_comprobante'].choices = [("", "")] + [(x.id,x.codigo+' - '+ x.descripcion) for x in TipoComprobante.objects.filter(status=1).order_by("codigo")]
        self.fields['tipo_comprobante'].widget.attrs['class'] = "selectpicker show-tick "
        self.fields['tipo_comprobante'].widget.attrs['data-live-search'] = True
        self.fields['tipo_comprobante'].widget.attrs['data-size'] = 25
        self.fields['tipo_comprobante'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['tipo_comprobante'].widget.attrs['data-width'] = "100%"

        q = PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo")
        self.fields['cuenta'].choices = [("", "")] + [(x.id,x.codigo+' - '+ x.descripcion) for x in q]
        self.fields['cuenta'].widget.attrs['class'] = "selectpicker show-tick "
        self.fields['cuenta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta'].widget.attrs['data-size'] = 25
        self.fields['cuenta'].widget.attrs['data-style'] = "slc-b-s"
        self.fields['cuenta'].widget.attrs['data-width'] = "100%"

        self.fields['fecha_inicio'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_inicio'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_inicio'].widget.attrs['class'] = "control-fecha"
        self.fields['fecha_fin'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_fin'].widget.attrs['placeholder'] = "aaaa-mm-dd"
        self.fields['fecha_fin'].widget.attrs['class'] = "control-fecha"



