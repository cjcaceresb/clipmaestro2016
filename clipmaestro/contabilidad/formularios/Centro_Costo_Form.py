#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
import datetime
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from librerias.funciones.validacion_formularios import *

__author__ = 'user3'
solo_alfanumericas = u'[a-zA-Z0-9_ \\ s]+$'

class FormCentroCosto(forms.Form):
    descripcion = forms.CharField(label=u"Descripción", max_length=150, widget=forms.Textarea, validators=[validador_espacios])

    def getDescripcionCosto(self):
        return self.data["descripcion"]

    def __init__(self, *args, **kwargs):
        super(FormCentroCosto, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs['maxlength'] = "150"


class RectificarCentroCosto(forms.Form):
    fecha_ini = forms.DateField(label=u"Fecha inicial", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_final = forms.DateField(label=u"Fecha final", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    num_comp = forms.CharField(label=u"# Comp.", required=False)
    tipo_comp = forms.ChoiceField(label=u"Tipo Comprobante", required=False, choices=[])
    cuentas = forms.ChoiceField(label=u"Cuentas", required=False, choices=[])
    centro_costo = forms.ChoiceField(label=u"Centro Costo", required=False, choices=[])
    con_centro_costo = forms.BooleanField(required=False)

    def getFechaIni(self):
        try:
            value = self.cleaned_data["fecha_ini"]
            return value
        except:
            return None

    def getFechaFinal(self):
        try:
            value = self.cleaned_data["fecha_final"]
            return value
        except:
            return None

    def getNumComp(self):
        try:
            value = self.cleaned_data["num_comp"]
            return value
        except:
            return ""

    def getTipoComp(self):
        try:
            value = self.cleaned_data["tipo_comp"]
            return TipoComprobante.objects.get(id=value)
        except:
            return None

    def getCuentas(self):
        try:
            value = self.cleaned_data["cuentas"]
            return PlanCuenta.objects.get(id=value)
        except:
            return None

    def getCentroCosto(self):
        try:
            value = self.cleaned_data["centro_costo"]
            return Centro_Costo.objects.get(id=value)
        except:
            return None


    def __init__(self, *args, **kwargs):
        super(RectificarCentroCosto, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_ini'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_ini'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['data-mask'] = "9999-99-99"
        self.fields['fecha_final'].widget.attrs['class'] = "datepicker"

        self.fields['tipo_comp'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_comp'].widget.attrs['data-live-search'] = True
        self.fields['tipo_comp'].choices = [("", "")] + [(x.id, unicode(x.descripcion)) for x in TipoComprobante.objects.filter(status=1).order_by("descripcion")]
        self.fields['tipo_comp'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_comp'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_comp'].widget.attrs['data-size'] = "auto"

        self.fields['cuentas'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuentas'].choices = [('', '')] + [(x.id, x.codigo+" - "+unicode(x.descripcion)) for x in PlanCuenta.objects.filter(status=1)]
        self.fields['cuentas'].widget.attrs['data-live-search'] = True
        self.fields['cuentas'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuentas'].widget.attrs['data-width'] = "100%"
        self.fields['cuentas'].widget.attrs['data-size'] = "auto"

        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['centro_costo'].choices = [('', '')] + [(x.id, x.descripcion) for x in Centro_Costo.objects.filter(status=1)]
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['centro_costo'].widget.attrs['data-width'] = "100%"
        self.fields['centro_costo'].widget.attrs['data-size'] = "auto"
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True



class DetalleRectificarCentroCosto(forms.Form):
    '''
    FormSet que presenta las transacciones
    '''
    id_modulo = forms.IntegerField(label=u"id_modulo", required=False)
    id_comprobante_detalle = forms.IntegerField(label=u"id_comprobante_detalle", required=False)
    num_comp = forms.CharField(label=u"# Comp.", required=False)
    fecha_reg = forms.DateField(label=u"Fecha Resgistro", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    cliente_proveedor = forms.CharField(label=u"Cliente/Proveedor", required=False)
    cuenta = forms.CharField(label=u"Cuenta", required=False)
    valor = forms.CharField(label=u"Valor", required=False)
    centro_costo = forms.ChoiceField(label=u"Centro Costo")


    def getCentroCosto(self):
        try:
            value = self.cleaned_data["centro_costo"]
            return Centro_Costo.objects.get(id=value)
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(DetalleRectificarCentroCosto, self).__init__(*args, **kwargs)


        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['centro_costo'].choices = [('', '')] + [(x.id, x.descripcion) for x in Centro_Costo.objects.filter(status=1)]
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['centro_costo'].widget.attrs['data-width'] = "100%"
        self.fields['centro_costo'].widget.attrs['data-size'] = "auto"
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True
