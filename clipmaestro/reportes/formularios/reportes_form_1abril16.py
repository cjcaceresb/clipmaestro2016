#! /usr/bin/python
# -*- coding: UTF-8 -*-

from django import forms
from django.utils.html import *
from contabilidad.models import *
from django.template.defaultfilters import default
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from librerias.funciones.funciones_vistas import *

import datetime
meses = (
    ("", ""),
    (1, "Enero"),
    (2, "Febrero"),
    (3, "Marzo"),
    (4, "Abril"),
    (5, "Mayo"),
    (6, "Junio"),
    (7, "Julio"),
    (8, "Agosto"),
    (9, "Septiembre"),
    (10, "Octubre"),
    (11, "Noviembre"),
    (12, "Diciembre")
)


estados_reporte = (
    (1, "Vencido"),
    (2, "Por vencer")
)

tipos_reporte = (
    (1, "Resumido"),
    (2, "Detallado")
)

tipos_reportePIG = (
    (1, "Balance General"),
    (2, "Pérdidas y Ganancias")
)



c_tipo_persona = (("1", "persona Juridica"),("2", "Persona Natural"))

opt_ats = [(1, 'Generar'),
           (2, 'Editar')]

class BuscadorReportes(forms.Form):
    fecha_ini = forms.DateField(label=u"Fecha inicial", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_final = forms.DateField(label=u"Fecha final", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_modificar = forms.DateField(label=u"Fecha modificar", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    mes = forms.ChoiceField(choices=meses, label=u"Mes", required=False)
    anio = forms.IntegerField(label=u"Años", required=False)
    cliente = forms.ChoiceField(label=u"Clientes", choices=[], required=False)
    proveedor = forms.ChoiceField(label=u"Proveedor", choices=[], required=False)
    item = forms.ChoiceField(label=u"Item", choices=[], required=False)
    centro_costo = forms.ChoiceField(label=u"Centro de Costo", required=False, choices=[])
    estado_reporte = forms.ChoiceField(label=u"Estado", required=False, choices=estados_reporte)
    tipo_reporte = forms.ChoiceField(label=u"Tipo", required=False, choices=tipos_reporte)
    tipo_reportePIG = forms.ChoiceField(label=u"Tipo PIG", required=False, choices=tipos_reportePIG)
    num_doc = forms.CharField(max_length=50, label=u"# Docuemento", required=False)
    generar_editar_ats = forms.ChoiceField(choices=opt_ats, widget=forms.RadioSelect(), required=False)
    sel_all = forms.BooleanField(required=False)
    all_prov = forms.BooleanField(label=u"Todos los proveedores", required=False)
    all_client = forms.BooleanField(label=u"Todos los clientes", required=False)
    all_items = forms.BooleanField(label=u"Todos los Items", required=False)
    all_centro_costo = forms.BooleanField(required=False)

    def getFechaIni(self):
        try:
            value = self.cleaned_data["fecha_ini"]
            return value
        except:
            return None

    def getFechaFinal(self):
        try:
            value = self.cleaned_data["fecha_final"]
            return value
        except:
            return None

    def getFechaModificar(self):
        try:
            value = self.cleaned_data["fecha_modificar"]
            return value
        except:
            return None

    def getMes(self):
        value = self.cleaned_data["mes"]
        try:
            return int(value)
        except:
            return 0

    def getAnio(self):
        try:
            value = self.cleaned_data["anio"]
            return int(value)
        except:
            return 0

    def getItem(self):
        try:
            value = self.cleaned_data["item"]
            return Item.objects.get(id=value)
        except:
            return None

    def getAllProv(self):
        try:
            value = self.cleaned_data["all_prov"]
            return value
        except:
            return False

    def getSelAll(self):
        try:
            value = self.cleaned_data["sel_all"]
            return value
        except:
            return False

    def getAllClient(self):
        try:
            value = self.cleaned_data["all_client"]
            return value
        except:
            return False

    def getAllItems(self):
        try:
            value = self.cleaned_data["all_items"]
            return value
        except:
            return False

    def getCliente(self):
        try:
            value = self.data['cliente']
            return Cliente.objects.get(id=value)
        except:
            return None

    def get_proveedor(self):
        try:
            value = self.data['proveedor']
            return Proveedores.objects.get(id=value)
        except:
            return None

    def get_num_doc(self):
        try:
            value = self.cleaned_data["num_doc"]
            return value
        except:
            return ""

    def getCentroCosto(self):
        try:
            value = self.data['centro_costo']
            return Centro_Costo.objects.get(id=value)
        except:
            return None

    def get_generar_editar_ats(self):
        try:
            value = self.cleaned_data["generar_editar_ats"]
            return value
        except:
            return ""

    def por_centro_costo(self):
        try:
            value = self.cleaned_data["all_centro_costo"]
            return value
        except:
            return False

    def getEstadoReporte(self):
        value = self.cleaned_data["estado_reporte"]
        try:
            return int(value)
        except:
            return 0

    def getTipoReporte(self):
        value = self.cleaned_data["tipo_reporte"]
        try:
            return int(value)
        except:
            return 0

    def getTipoReportePIG(self):
        value = self.cleaned_data["tipo_reportePIG"]
        try:
            return int(value)
        except:
            return 0

    def __init__(self, *args, **kwargs):
        super(BuscadorReportes, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_ini'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_ini'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['data-mask'] = "9999-99-99"
        self.fields['fecha_final'].widget.attrs['class'] = "datepicker"

        self.fields['fecha_modificar'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_modificar'].widget.attrs['placeholder'] = "aaaa-mm-dd"

        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Item"
        self.fields['item'].choices = [("", "")] + [(x.id, unicode(x.codigo) + " - " + unicode(x.nombre)) for x in Item.objects.filter(status=1).order_by("nombre")]
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-width'] = "100%"
        self.fields['item'].widget.attrs['data-size'] = "auto"

        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].choices = [('', '')] + [(x.id, x.razon_social[0:30]) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-size'] = "auto"

        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['centro_costo'].choices = [('', '')] + [(x.id, x.descripcion[0:30]) for x in Centro_Costo.objects.filter(status=1)]
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True
        self.fields['centro_costo'].widget.attrs['title'] = "Seleccione un Centro Costo"
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['centro_costo'].widget.attrs['data-width'] = "100%"
        self.fields['centro_costo'].widget.attrs['data-size'] = "auto"

        self.fields['proveedor'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['proveedor'].choices = [('', '')] + [(x.id, x.razon_social[0:30] + " - " + x.ruc ) for x in Proveedores.objects.filter(status=1).order_by("razon_social")]
        self.fields['proveedor'].widget.attrs['data-live-search'] = True
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un Proveedor"
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['data-width'] = "100%"
        self.fields['proveedor'].widget.attrs['data-size'] = "auto"
        self.fields['proveedor'].widget.attrs['data-live-search'] = True

        self.fields['mes'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['mes'].widget.attrs['data-live-search'] = True
        self.fields['mes'].widget.attrs['title'] = "Seleccione un Mes"
        self.fields['mes'].widget.attrs['data-style'] = "slc-primary"
        self.fields['mes'].widget.attrs['data-width'] = "100%"
        self.fields['mes'].widget.attrs['data-size'] = "auto"

        self.fields['estado_reporte'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado_reporte'].widget.attrs['data-live-search'] = True
        self.fields['estado_reporte'].widget.attrs['title'] = "Seleccione un Mes"
        self.fields['estado_reporte'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado_reporte'].widget.attrs['data-width'] = "100%"
        self.fields['estado_reporte'].widget.attrs['data-size'] = "auto"


        self.fields['tipo_reporte'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_reporte'].widget.attrs['data-live-search'] = True
        self.fields['tipo_reporte'].widget.attrs['title'] = "Seleccione un Mes"
        self.fields['tipo_reporte'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_reporte'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_reporte'].widget.attrs['data-size'] = "auto"



class BuscadorReportes_fecha_inicial_final(forms.Form):

    fecha_ini = forms.DateField(label=u"Fecha inicial", required=False,widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_final = forms.DateField(label=u"Fecha Final", required=False,widget=forms.DateInput(format=("%Y-%m-%d")))
    def getFechaIni(self):
        try:
            value = self.cleaned_data["fecha_ini"]
            return value
        except:
            return None

    def getFechaFinal(self):
        try:
            value = self.cleaned_data["fecha_final"]
            return value
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(BuscadorReportes_fecha_inicial_final, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_ini'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_ini'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['data-mask'] = "9999-99-99"
        self.fields['fecha_final'].widget.attrs['class'] = "datepicker"


class BuscadorReportes_balance_comprobacion(forms.Form):
    mes = forms.ChoiceField(choices=meses, label=u"Mes", required=False)
    anio = forms.IntegerField(label=u"Años", required=False)
    sel_all = forms.BooleanField(required=False)

    def getMes(self):
        value = self.cleaned_data["mes"]
        try:
            return int(value)
        except:
            return 0

    def getAnio(self):
        try:
            value = self.cleaned_data["anio"]
            return int(value)
        except:
            return 0

    def getSelAll(self):
       try:
           value = self.cleaned_data["sel_all"]
           return value
       except:
           return False

    def __init__(self, *args, **kwargs):
        super(BuscadorReportes_balance_comprobacion, self).__init__(*args, **kwargs)
        self.fields['mes'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['mes'].widget.attrs['data-live-search'] = True
        self.fields['mes'].widget.attrs['title'] = "Seleccione un Mes"
        self.fields['mes'].widget.attrs['data-style'] = "slc-primary"
        self.fields['mes'].widget.attrs['data-width'] = "100%"
        self.fields['mes'].widget.attrs['data-size'] = "auto"


class BuscadorReportes_libro_mayor(forms.Form):
    fecha_ini = forms.DateField(label=u"Fecha inicial", required=False,widget=forms.DateInput(format=("%Y-%m-%d")))
    fecha_final = forms.DateField(label=u"Fecha Final", required=False,widget=forms.DateInput(format=("%Y-%m-%d")))
    sel_all = forms.BooleanField(required=False)

    def getFechaIni(self):
        try:
            value = self.cleaned_data["fecha_ini"]
            return value
        except:
            return None

    def getFechaFinal(self):
        try:
            value = self.cleaned_data["fecha_final"]
            return value
        except:
            return None

    def getSelAll(self):
       try:
           value = self.cleaned_data["sel_all"]
           return value
       except:
           return False

    def __init__(self, *args, **kwargs):
        super(BuscadorReportes_libro_mayor, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_ini'].widget.attrs['class'] = "datepicker"
        self.fields['fecha_ini'].widget.attrs['data-mask'] = "9999-99-99"

        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['data-mask'] = "9999-99-99"
        self.fields['fecha_final'].widget.attrs['class'] = "datepicker"

class ReporteSaldosForm(forms.Form):
    proveedor = forms.ChoiceField(choices=[], label=u"Proveedor", required=False)
    cliente = forms.ChoiceField(choices=[], label=u"Cliente", required=False)
    cuenta = forms.ChoiceField(choices=[], label=u"Cuentas", required=False)
    banco = forms.ChoiceField(choices=[], label=u"Bancos", required=False)
    item = forms.ChoiceField(choices=[], label=u"Items", required=False)
    tipo_mov_inv = forms.ChoiceField(choices=[], label=u"Tipo Movimiento Inventario", required=False)
    centro_costo = forms.ChoiceField(choices=[], label=u"Centro de Costos", required=False)

    fecha_corte = forms.DateField(label=u"Fecha inicial", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    saldo_banco = forms.FloatField(required=False, label=u"Saldo Banco")

    def getProveedor(self):
        try:
            value = self.cleaned_data["proveedor"]
            return Proveedores.objects.get(id=value)
        except:
            return None

    def getCliente(self):
        try:
            value = self.cleaned_data["cliente"]
            return Cliente.objects.get(id=value)
        except:
            return None

    def getCuenta(self):
        try:
            value = self.cleaned_data["cuenta"]
            return PlanCuenta.objects.get(id=value)
        except:
            return None

    # modifio la función getBanco para obtener el objeto Cuenta_Banco
    def getBanco(self):
        try:
            value = self.cleaned_data["banco"]
            return Cuenta_Banco.objects.get(id=value)
        except:
            return None

    def getCentroCosto(self):
        try:
            value = self.cleaned_data["centro_costo"]
            return Centro_Costo.objects.get(id=value)
        except Centro_Costo.DoesNotExist:
            return None

    def getItem(self):
        try:
            value = self.cleaned_data["item"]
            return Item.objects.get(id=value)
        except:
            return None


    def getSaldoBanco(self):
        return self.data['saldo_banco']

    def getFechaCorte(self):
        try:
            value = self.cleaned_data["fecha_corte"]
            return value
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(ReporteSaldosForm, self).__init__(*args, **kwargs)

        self.fields['proveedor'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['proveedor'].widget.attrs['data-live-search'] = True
        self.fields['proveedor'].widget.attrs['title'] = "Seleccione un Proveedor"
        self.fields['proveedor'].choices = [("", "")] + [(x.id, x.ruc+u" - "+x.razon_social) for x in Proveedores.objects.filter(status=1).order_by("razon_social")]
        self.fields['proveedor'].widget.attrs['data-style'] = "slc-primary"
        self.fields['proveedor'].widget.attrs['data-width'] = "100%"
        self.fields['proveedor'].widget.attrs['data-size'] = "auto"

        self.fields['centro_costo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['centro_costo'].widget.attrs['data-live-search'] = True
        self.fields['centro_costo'].widget.attrs['title'] = "Seleccione centro costo"
        self.fields['centro_costo'].choices = [("", "")] + [(x.id, x.descripcion) for x in Centro_Costo.objects.filter(status=1).order_by("descripcion")]
        self.fields['centro_costo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['centro_costo'].widget.attrs['data-width'] = "100%"
        self.fields['centro_costo'].widget.attrs['data-size'] = "auto"

        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].choices = [("", "")] + [(x.id, x.razon_social) for x in Cliente.objects.filter(status=1).order_by("razon_social")]
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-size'] = "auto"

        self.fields['cuenta'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta'].widget.attrs['data-live-search'] = True
        self.fields['cuenta'].widget.attrs['title'] = "Seleccione una Cuenta"
        self.fields['cuenta'].choices = [("", "")] + [(x.id,
                                                       x.codigo + " - " + x.descripcion) for x in PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo")]
        self.fields['cuenta'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta'].widget.attrs['data-size'] = "auto"

        self.fields['banco'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['banco'].widget.attrs['data-live-search'] = True
        self.fields['banco'].widget.attrs['title'] = "Seleccione un Banco"
        self.fields['banco'].choices = [("", "")] + [(x.id, unicode(x.descripcion) +" - "+x.tipo_cuenta_banco.descripcion +" - " + x.numero_cuenta) for x in Cuenta_Banco.objects.filter(status=1).order_by("descripcion")]
        self.fields['banco'].widget.attrs['data-style'] = "slc-primary"
        self.fields['banco'].widget.attrs['data-width'] = "100%"
        self.fields['banco'].widget.attrs['data-size'] = "auto"

        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Item"
        self.fields['item'].choices = [("", "")] + [(x.id, unicode(x.codigo) + " - " + unicode(x.nombre)) for x in Item.objects.filter(status=1).order_by("nombre")]
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-width'] = "100%"
        self.fields['item'].widget.attrs['data-size'] = "auto"

        self.fields['tipo_mov_inv'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['tipo_mov_inv'].widget.attrs['data-live-search'] = True
        self.fields['tipo_mov_inv'].widget.attrs['title'] = "Seleccione un Motivo"
        self.fields['tipo_mov_inv'].choices = [("", "")] + [(x.id, unicode(x.descripcion)) for x in Tipo_Mov_Inventario.objects.filter(status=1).order_by("descripcion")]
        self.fields['tipo_mov_inv'].widget.attrs['data-style'] = "slc-primary"
        self.fields['tipo_mov_inv'].widget.attrs['data-width'] = "100%"
        self.fields['tipo_mov_inv'].widget.attrs['data-size'] = "auto"

        self.fields['fecha_corte'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_corte'].widget.attrs['style'] = " width: 390px;"
        self.fields['fecha_corte'].widget.attrs['data-width'] = "100%"
        self.fields['fecha_corte'].widget.attrs['placeholder'] = "aaaa-mm-dd"

        self.fields['saldo_banco'].widget.attrs['style'] = " text-align: right;"
        self.fields['saldo_banco'].widget.attrs['placeholder'] = "$ 0.00"
        self.fields['saldo_banco'].widget.attrs['class'] = "numerico"

class BuscadorReportesVentaPendiente(forms.Form):
    selec_ordenamiento = (('', ''),  ('1', 'Clientes'), ('2', 'Items'))
    fecha_ini = forms.DateField(label=u"Fecha inicial", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    fecha_final = forms.DateField(label=u"Fecha final", widget=forms.DateInput(format=("%Y-%m-%d")), required=False)
    cliente = forms.ChoiceField(label=u"Cliente", choices=[], required=False)
    item = forms.ChoiceField(label=u"Item", choices=[], required=False)
    ordenamiento = forms.ChoiceField(label=u"Ordenado por:", choices=selec_ordenamiento, required=False)

    def getFechaIni(self):
        try:
            value = self.cleaned_data["fecha_ini"]
            return value
        except:
            return None

    def getFechaFinal(self):
        try:
            value = self.cleaned_data["fecha_final"]
            return value
        except:
            return None

    def getItem(self):
        try:
            value = self.data['item']
            return Item.objects.get(id=value)
        except:
            return None

    def getCliente(self):
        try:
            value = self.data['cliente']
            return Cliente.objects.get(id=value)
        except:
            return None


    def getOrdenamiento(self):
        try:
            value = self.data['ordenamiento']
            return value
        except:
            return ""





    def __init__(self, *args, **kwargs):
        super(BuscadorReportesVentaPendiente, self).__init__(*args, **kwargs)
        self.fields['fecha_ini'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"

        self.fields['cliente'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cliente'].choices = [('', '')] + [(x.id, x.razon_social[0:30]) for x in Cliente.objects.filter(status=1)]
        self.fields['cliente'].widget.attrs['data-live-search'] = True
        self.fields['cliente'].widget.attrs['title'] = "Seleccione un Cliente"
        self.fields['cliente'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cliente'].widget.attrs['data-width'] = "100%"
        self.fields['cliente'].widget.attrs['data-size'] = "auto"

        self.fields['item'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['item'].choices = [("", "")] + [(x.id, x.codigo+" - "+x.nombre) for x in Item.objects.filter(status=1, nombre__isnull=False, descripcion__isnull=False).order_by("codigo")]
        self.fields['item'].widget.attrs['data-live-search'] = True
        self.fields['item'].widget.attrs['title'] = "Seleccione un Item"
        self.fields['item'].widget.attrs['data-style'] = "slc-primary"
        self.fields['item'].widget.attrs['data-width'] = "100%"
        self.fields['item'].widget.attrs['data-size'] = "auto"

        self.fields['ordenamiento'].widget.attrs['class'] = "selectpicker"
        self.fields['ordenamiento'].widget.attrs['data-live-search'] = True
        self.fields['ordenamiento'].widget.attrs['data-style'] = "slc-primary"
        self.fields['ordenamiento'].widget.attrs['data-width'] = "100%"
        self.fields['ordenamiento'].widget.attrs['data-size'] = "auto"



class ReporteSaldosPendienteForm(forms.Form):
    fecha_final = forms.DateField(label=u"Fecha final", widget=forms.DateInput(format=("%Y-%m-%d")))

    def getFechaFinal(self):
        try:
            value = self.cleaned_data["fecha_final"]
            return value
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(ReporteSaldosPendienteForm, self).__init__(*args, **kwargs)
        self.fields['fecha_final'].widget.attrs['data-date-format'] = "yyyy-mm-dd"


class FormAnio(forms.Form):
    anio = forms.ChoiceField(label=u"Año", choices=anios)

    def getAnios(self):
        try:
            value = self.data["anio"]
            return value
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(FormAnio, self).__init__(*args, **kwargs)
        self.fields['anio'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['anio'].widget.attrs['data-live-search'] = True
        self.fields['anio'].widget.attrs['title'] = u"Seleccione un Año"
        self.fields['anio'].widget.attrs['data-style'] = "slc-primary"
        self.fields['anio'].widget.attrs['data-width'] = "100%"
        self.fields['anio'].widget.attrs['data-size'] = "auto"