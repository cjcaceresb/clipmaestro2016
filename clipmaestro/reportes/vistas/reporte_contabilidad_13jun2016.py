#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import connection
from django.core.paginator import *
from django import forms, template
from django.utils.html import *
from  contabilidad.models import *
from librerias.funciones.excel_response_mod import *
import xlwt
from django.db.models import Sum
import operator
from reportes.formularios.reportes_form import *
from django.forms.formsets import formset_factory
from reportes.models import *
from funciones import *
from librerias.funciones.funciones_vistas import *
import calendar
from django.forms.util import ErrorList
from contabilidad.funciones.contabilidad_func import *
from librerias.funciones.paginacion import *
from librerias.funciones.permisos import *
import requests
from django.db import connection


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def libro_diario(request):
    buscador = FormBuscadoresAsientoContable(request.GET)
    cabeceras = busqueda_contabilidad(buscador, 0)
    paginacion = Paginator(cabeceras, get_num_filas_template())
    numero_pagina = request.GET.get("page")

    try:
        cabeceras = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        cabeceras = paginacion.page(1)
    except EmptyPage:
        cabeceras = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = cabeceras.number
    lista_pag = arreglo_paginas_template(total_paginas, numero)

    return render_to_response('asiento_contable/libro_diario.html', {"buscador": buscador, "objetos": cabeceras,
                                "total_paginas": total_paginas, "numero": numero,
                                "mostrar_paginacion": muestra_paginacion_template(paginacion.count),
                                "arreglo_paginado": lista_pag}, context_instance=RequestContext(request))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def libro_diario_pdf(request, id):
    total_debe = 0.00
    total_haber = 0.00
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    try:
        cabecera = Cabecera_Comp_Contable.objects.get(id=id)

        if cabecera.status == 1:
            detalles_debe = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera.id, dbcr="D").order_by("plan_cuenta__codigo")
            detalles_haber = Detalle_Comp_Contable.objects.filter(status=1).filter(cabecera_contable=cabecera.id, dbcr="H").order_by("plan_cuenta__codigo")
            for obj in detalles_debe:
                total_debe += float(obj.valor)
            for obj in detalles_haber:
                total_haber += float(obj.valor)
        else:
            detalles_debe = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera.id, dbcr="D").order_by("plan_cuenta__codigo")
            detalles_haber = Detalle_Comp_Contable.objects.exclude(status=0).filter(cabecera_contable=cabecera.id, dbcr="H").order_by("plan_cuenta__codigo")
            for obj in detalles_debe:
                total_debe += float(obj.valor)
            for obj in detalles_haber:
                total_haber += float(obj.valor)

        html = render_to_string('asiento_contable/detalle_asiento_contable_pdf.html',
                                {'pagesize': 'A4',
                                 "empresa":empresa,
                                 "usuario": usuario,
                                 "fecha_actual": datetime.datetime.now(),
                                 "request": request,
                                 "total_debe": total_debe,
                                 "total_haber": total_haber,
                                 "objeto_cabecera": cabecera,
                                 "objeto_detalle_debe": detalles_debe,
                                 "objeto_detalle_haber": detalles_haber
                                 }, context_instance=RequestContext(request))

        return generar_pdf_get(html)

    except Cabecera_Comp_Contable.DoesNotExist:
        return Http404


class Reporte_103():
    def __init__(self):
        self.codigo = Codigo_SRI()
        self.lista = []
        self.total_base = 0.0
        self.total_ret = 0.0


def generar_reporte_103(lista, fecha_ini, fecha_fin):
    """
    Funcion donde se ejecutan las consultas a la base y las sumatorias
    para el reporte 103
    :param lista:
    :param fecha_ini:
    :param fecha_fin:
    :return:
    """
    obj_vigencia_retencion = VigenciaRetencionSRI()
    total_todas_compras = 0
    total_todas_ret = 0
    #obj_vigencia_retencion = VigenciaRetencionSRI.objects.filter(status= 1, fecha_inicio__lte__fecha_ini, fecha_final__gte_fecha_fin )
    #print len(obj_vigencia_retencion)
    query = Codigo_SRI.objects.values('id').distinct().order_by("codigo")#saca todos los codigo de sri ordenados por coigo
    print 'la longitud es',len(query), query[0]
    for obj in query:
        print obj["id"]
        datos_reporte = Reporte_103()
        datos_reporte.codigo = Codigo_SRI.objects.filter(id=obj["id"])[0]#el primero que encuenrtede hacer el query
        #print datos_reporte.codigo
        q_lista = CompraDetalle.objects.filter(status=1,
                                               compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                               codigo_ret_fte__id=obj["id"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")

        datos_reporte.lista = q_lista  # Lista del detalle de compra que tiene ese código de retención

        q_tcompra = CompraDetalle.objects.filter(status=1,
                                                 compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                 codigo_ret_fte__id=obj["id"])\
            .exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor',
                                                                                      field="valor"))["valor__sum"]
        total_compras = cero_if_none(q_tcompra)  # 'q_tcompra' sumariza el total de las compras ret de ese codigo

        q_tcompras_nc = CompraDetalle.objects.filter(status=1,
                                                     compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                     codigo_ret_fte__id=obj["id"],
                                                     compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('valor', field="valor"))["valor__sum"]

        total_compras_n_c = cero_if_none(q_tcompras_nc)  # 'q_tcompras_nc' sumariza el total de las compras
                                                         # con notas de credito o débito en esa fecha

        q_tret = CompraDetalle.objects.filter(status=1,
                                              compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                              codigo_ret_fte__id=obj["id"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('monto_ret_fte',
                           field="monto_ret_fte"))["monto_ret_fte__sum"]

        total_ret = cero_if_none(q_tret)  # 'q_tret' sumariza el total de las retenciones a la fte de ese codigo
                                          # en esa fecha

        datos_reporte.total_base = total_compras - total_compras_n_c
        datos_reporte.total_ret = total_ret

        if len(datos_reporte.lista) > 0:
            lista.append(datos_reporte)
            total_todas_compras += datos_reporte.total_base
            total_todas_ret += total_ret
    print 'la longitud salida es',len(query), query[0]
    return total_todas_compras, total_todas_ret




def generar_reporte_103_hasta_21_julio(lista, fecha_ini, fecha_fin):
    """
    Funcion donde se ejecutan las consultas a la base y las sumatorias
    para el reporte 103
    :param lista:
    :param fecha_ini:
    :param fecha_fin:
    :return:
    """
    total_todas_compras = 0
    total_todas_ret = 0

    query = Codigo_SRI.objects.values('codigo').distinct().order_by("codigo")#saca todos los codigo de sri ordenados por coigo
    for obj in query:
        datos_reporte = Reporte_103()
        datos_reporte.codigo = Codigo_SRI.objects.filter(codigo=obj["codigo"])[0]#el primero que encuenrtede hacer el query

        q_lista = CompraDetalle.objects.filter(status=1,
                                               compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                               codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")

        datos_reporte.lista = q_lista  # Lista del detalle de compra que tiene ese código de retención

        q_tcompra = CompraDetalle.objects.filter(status=1,
                                                 compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                 codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor',
                                                                                      field="valor"))["valor__sum"]
        total_compras = cero_if_none(q_tcompra)  # 'q_tcompra' sumariza el total de las compras ret de ese codigo

        q_tcompras_nc = CompraDetalle.objects.filter(status=1,
                                                     compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                     codigo_ret_fte__codigo=obj["codigo"],
                                                     compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('valor', field="valor"))["valor__sum"]

        total_compras_n_c = cero_if_none(q_tcompras_nc)  # 'q_tcompras_nc' sumariza el total de las compras
                                                         # con notas de credito o débito en esa fecha

        q_tret = CompraDetalle.objects.filter(status=1,
                                              compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                              codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('monto_ret_fte',
                           field="monto_ret_fte"))["monto_ret_fte__sum"]

        total_ret = cero_if_none(q_tret)  # 'q_tret' sumariza el total de las retenciones a la fte de ese codigo
                                          # en esa fecha

        datos_reporte.total_base = total_compras - total_compras_n_c
        datos_reporte.total_ret = total_ret

        if len(datos_reporte.lista) > 0:
            lista.append(datos_reporte)
            total_todas_compras += datos_reporte.total_base
            total_todas_ret += total_ret

    return total_todas_compras, total_todas_ret

def generar_reporte_103_anio_mes(lista, anio, mes):
    """
    Funcion donde se ejecutan las consultas a la base y las sumatorias
    para el reporte 103
    :param lista:
    :param fecha_ini:
    :param fecha_fin:
    :return:
    """
    total_todas_compras = 0
    total_todas_ret = 0
    fecha_ini = 0
    fecha_fin = 0
    primerdia = str(anio),'-',str(mes),'-',str(01)
    print('el primer dia es')
    print primerdia
    ultimodia = 31

    query = Codigo_SRI.objects.values('codigo').distinct().order_by("codigo")#saca todos los codigo de sri ordenados por coigo
    for obj in query:
        datos_reporte = Reporte_103()
        datos_reporte.codigo = Codigo_SRI.objects.filter(codigo=obj["codigo"])[0]#el primero que encuenrtede hacer el query

        q_lista = CompraDetalle.objects.filter(status=1,
                                               compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                               codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")

        datos_reporte.lista = q_lista  # Lista del detalle de compra que tiene ese código de retención

        q_tcompra = CompraDetalle.objects.filter(status=1,
                                                 compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                 codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor',
                                                                                      field="valor"))["valor__sum"]
        total_compras = cero_if_none(q_tcompra)  # 'q_tcompra' sumariza el total de las compras ret de ese codigo

        q_tcompras_nc = CompraDetalle.objects.filter(status=1,
                                                     compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                     codigo_ret_fte__codigo=obj["codigo"],
                                                     compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('valor', field="valor"))["valor__sum"]

        total_compras_n_c = cero_if_none(q_tcompras_nc)  # 'q_tcompras_nc' sumariza el total de las compras
                                                         # con notas de credito o débito en esa fecha

        q_tret = CompraDetalle.objects.filter(status=1,
                                              compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                              codigo_ret_fte__codigo=obj["codigo"])\
            .exclude(compra_cabecera__documento__codigo_documento="04")\
            .aggregate(Sum('monto_ret_fte',
                           field="monto_ret_fte"))["monto_ret_fte__sum"]

        total_ret = cero_if_none(q_tret)  # 'q_tret' sumariza el total de las retenciones a la fte de ese codigo
                                          # en esa fecha

        datos_reporte.total_base = total_compras - total_compras_n_c
        datos_reporte.total_ret = total_ret

        if len(datos_reporte.lista) > 0:
            lista.append(datos_reporte)
            total_todas_compras += datos_reporte.total_base
            total_todas_ret += total_ret

    return total_todas_compras, total_todas_ret





@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                #report = get_nombre_reporte('CMP_COMPRA') + '.html'
                report = get_nombre_reporte('CONT_103') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reporte_contabilidad/reporte_103.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_104(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                #report = get_nombre_reporte('CMP_COMPRA') + '.html'
                report = get_nombre_reporte('CONT_104') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reporte_contabilidad/reporte_104.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))



@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_104_impreso(request, id):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                #report = get_nombre_reporte('CMP_COMPRA') + '.html'
                if int(id) ==1:
                    report = get_nombre_reporte('CONT_104') + '.xls'
                if int(id) ==2:
                    report = get_nombre_reporte('CONT_104') + '.pdf'
                #report = get_nombre_reporte('CONT_103') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    if int(id)== 1:
        response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
        nombre = "rpt_104_"+str(fecha_ini)+"_al_"+str(fecha_final)
        response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    if int(id)== 2:
        response = HttpResponse(r.content, mimetype='application/pdf')
        nombre = "rpt_104_"+str(fecha_ini)+"_al_"+str(fecha_final)
        response['Content-Disposition'] = 'attachment;filename="'+nombre+'".pdf"'
    return response


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_impreso(request, id):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                #report = get_nombre_reporte('CMP_COMPRA') + '.html'
                if int(id) ==1:
                    report = get_nombre_reporte('CONT_103') + '.xls'
                if int(id) ==2:
                    report = get_nombre_reporte('CONT_103') + '.pdf'
                #report = get_nombre_reporte('CONT_103') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    if int(id)== 1:
        response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
        nombre = "rpt_103_"+str(fecha_ini)+"_al_"+str(fecha_final)
        response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    if int(id)== 2:
        response = HttpResponse(r.content, mimetype='application/pdf')
        nombre = "rpt_103_"+str(fecha_ini)+"_al_"+str(fecha_final)
        response['Content-Disposition'] = 'attachment;filename="'+nombre+'".pdf"'
    return response
    '''
    return render_to_response('reporte_contabilidad/reporte_103.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))
    '''



@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_pdf(request):
    """
    Función que me devuelve un html con el reporte
    103
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    lista = []
    total_todas_compras = 0.0
    total_todas_ret = 0.0
    buscador = BuscadorReportes()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_fin):
            total_todas_compras, total_todas_ret = generar_reporte_103(lista, fecha_ini, fecha_fin)
            html = render_to_string('reporte_contabilidad/reporte_103_pdf.html',
                            {'pagesize': 'A4',
                             "lista": lista,
                             "total_todas_compras": total_todas_compras,
                             "total_todas_ret": total_todas_ret,
                             "empresa":empresa,
                             "request": request
                             }, context_instance=RequestContext(request))

            nombre = ( 'Reporte 103 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))#cambios
            return generar_pdf_nombre(html, nombre)
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 103
    return render_to_response('reporte_contabilidad/reporte_103.html',
                      {"buscador": buscador,
                       "total_todas_compras": total_todas_compras,
                       "total_todas_ret": total_todas_ret,
                       "lista": lista,
                       }, context_instance=RequestContext(request))



@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_pdf_offline_ok(request):
    """
    Función que me devuelve un html con el reporte
    103
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    lista = []
    total_todas_compras = 0.0
    total_todas_ret = 0.0
    buscador = BuscadorReportes()

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_fin):
            total_todas_compras, total_todas_ret = generar_reporte_103(lista, fecha_ini, fecha_fin)
            html = render_to_string('reporte_contabilidad/reporte_103_pdf.html',
                            {'pagesize': 'A4',
                             "lista": lista,
                             "total_todas_compras": total_todas_compras,
                             "total_todas_ret": total_todas_ret,
                             "empresa":empresa,
                             "request": request
                             }, context_instance=RequestContext(request))

            nombre = ( 'Reporte 103 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))#cambios
            return generar_pdf_nombre(html, nombre)
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 103
    return render_to_response('reporte_contabilidad/reporte_103.html',
                      {"buscador": buscador,
                       "total_todas_compras": total_todas_compras,
                       "total_todas_ret": total_todas_ret,
                       "lista": lista,
                       }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_excel(request):
    """
    Función que me devuelve el reporte
    103 en excel
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    lista_clase = []
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        lista = [[u"", u"", u"", u"REPORTE 103"+TITULO]]

        if None not in (fecha_ini, fecha_fin):
            total_todas_compras, total_todas_ret = generar_reporte_103(lista_clase, fecha_ini, fecha_fin)
            ###############  Cabecera del EXCEL  ###############
            lista.append([u""])
            lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini.strftime("%Y-%m-%d")])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_fin.strftime("%Y-%m-%d")])
            lista.append([u""])
            ####################################################
            for obj in lista_clase:
                lista.append([u"Código:"+SUBTITULO, obj.codigo.codigo])
                lista.append([u"Descripción:"+SUBTITULO, obj.codigo.descripcion])
                lista.append([u""])
                lista.append([u"Código"+COLUMNA, u"Fecha"+COLUMNA, u"Tipo Doc."+COLUMNA, u"# Documento"+COLUMNA,
                              u"Concepto"+COLUMNA, u"Base"+COLUMNA, u" % "+COLUMNA, u"Valor Retenido"+COLUMNA])
                for det in obj.lista:
                    obj_list = [det.codigo_ret_fte.codigo,
                                det.compra_cabecera.fecha_reg.strftime("%Y-%m-%d"),
                                det.compra_cabecera.documento.descripcion_documento,
                                det.compra_cabecera.num_doc,
                                det.compra_cabecera.concepto,
                                det.valor,
                                obj.codigo.porcentaje,
                                #det.porc_ret_fte,# 20 julio 2015 el mismo porcentaje que el del jefe del ciclo
                                det.monto_ret_fte]
                    lista.append(obj_list)
                lista.append([u"Totales"+SUBTITULO, u"", u"", u"", u"", "$ " + str(obj.total_base)+G_TOTAL, u"",
                              "$ " + str(obj.total_ret)+G_TOTAL])
                lista.append([u""])
            lista.append([u"",u"",u"",u"",u"",u"TOTAL BASE"+SUBTITULO,U"TOTAL VALOR RETENIDO"+SUBTITULO])
            lista.append([u"",u"",u"",u"",u"","$ " + str(total_todas_compras)+G_TOTAL, "$ " + str(total_todas_ret)+G_TOTAL])
            return ExcelResponse(lista, 'Reporte 103 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))

        else:  # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 103
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/reporte_103.html',
                              {"buscador": buscador,
                               "lista": lista_clase,
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_103_excel(request):
    """
    Función que me devuelve el reporte
    103 en excel
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    lista_clase = []
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        lista = [[u"", u"", u"", u"REPORTE 103"+TITULO]]

        if None not in (fecha_ini, fecha_fin):
            total_todas_compras, total_todas_ret = generar_reporte_103(lista_clase, fecha_ini, fecha_fin)
            ###############  Cabecera del EXCEL  ###############
            lista.append([u""])
            lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini.strftime("%Y-%m-%d")])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_fin.strftime("%Y-%m-%d")])
            lista.append([u""])
            ####################################################
            for obj in lista_clase:
                lista.append([u"Código:"+SUBTITULO, obj.codigo.codigo])
                lista.append([u"Descripción:"+SUBTITULO, obj.codigo.descripcion])
                lista.append([u""])
                lista.append([u"Código"+COLUMNA, u"Fecha"+COLUMNA, u"Tipo Doc."+COLUMNA, u"# Documento"+COLUMNA,
                              u"Concepto"+COLUMNA, u"Base"+COLUMNA, u" % "+COLUMNA, u"Valor Retenido"+COLUMNA])
                for det in obj.lista:
                    obj_list = [det.codigo_ret_fte.codigo,
                                det.compra_cabecera.fecha_reg.strftime("%Y-%m-%d"),
                                det.compra_cabecera.documento.descripcion_documento,
                                det.compra_cabecera.num_doc,
                                det.compra_cabecera.concepto,
                                det.valor,
                                obj.codigo.porcentaje,
                                #det.porc_ret_fte,# 20 julio 2015 el mismo porcentaje que el del jefe del ciclo
                                det.monto_ret_fte]
                    lista.append(obj_list)
                lista.append([u"Totales"+SUBTITULO, u"", u"", u"", u"", "$ " + str(obj.total_base)+G_TOTAL, u"",
                              "$ " + str(obj.total_ret)+G_TOTAL])
                lista.append([u""])
            lista.append([u"",u"",u"",u"",u"",u"TOTAL BASE"+SUBTITULO,U"TOTAL VALOR RETENIDO"+SUBTITULO])
            lista.append([u"",u"",u"",u"",u"","$ " + str(total_todas_compras)+G_TOTAL, "$ " + str(total_todas_ret)+G_TOTAL])
            return ExcelResponse(lista, 'Reporte 103 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))

        else:  # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 103
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/reporte_103.html',
                              {"buscador": buscador,
                               "lista": lista_clase,
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url='/')
def reporte_comprobantes_anulados(request):
    '''
    Vista que obtiene desde jasperServer el reporte de
    comprobante anulados
    :param request:
    :return:
    '''
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    c = None
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        match = resolve(request.path)
        permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))

        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports' + empresa.path_jasper + '/'

                # Report to process:
                report = permiso.nombre_reporte + '.html'
                #report = 'libro_mayor.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")}

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)
                print "XXXXXXXXXXXXXXXXXXXXXXXXXXXX"
                print r
                print r.content[0:1500]
                print "XXXXXXXXXXXXXXXXXXXXXXXXXXXX"

    return render_to_response('reporte_contabilidad/reporte_comprobantes_anulados.html',
                              {"buscador": buscador, "objetos": r, "c": c}, context_instance=RequestContext(request))



@csrf_exempt
@login_required(login_url='/')
def reporte_comprobantes_anulados_pdf(request):
    '''
    Vista que obtiene desde jasperServer el reporte de
    comprobante anulados
    :param request:
    :return:
    '''
    buscador = BuscadorReportes(request.GET)
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    match = resolve(request.path)
    permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))

    r = None
    c = None

    if buscador.is_valid():
        if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
            fecha_ini = buscador.getFechaIni()
            fecha_final = buscador.getFechaFinal()
            # Path to resource rest service:
            url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports' + empresa.path_jasper + '/'

            # Report to process:
            report = permiso.nombre_reporte + '.pdf'
            #report = 'libro_mayor.html'
            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            # Params
            params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_final.strftime("%Y-%m-%d")}

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "XXXXXXXXXXXXXXXXXXXXXXXXXXXX"
            print r
            print r.content[0:1500]
            print "XXXXXXXXXXXXXXXXXXXXXXXXXXXX"

            response = HttpResponse(r.content, content_type='application/pdf')
            return response

    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    return render_to_response('reporte_contabilidad/reporte_comprobantes_anulados.html',
                              {"buscador": buscador, "objetos": r}, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_comprobantes_anulados_por_fecha_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(request.GET)
    buscador.is_valid()  # Para que funcione el cleaned data
    fecha_ini = buscador.getFechaIni()
    fecha_final = buscador.getFechaFinal()

    match = resolve(request.path)
    permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))

    if None not in (fecha_ini, fecha_final):
        # Path to resource rest service:
        url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports' + empresa.path_jasper + '/'

        # Report to process:
        report = permiso.nombre_reporte + '.xls'
        # Authorisation credentials:
        auth = (empresa.user_jasper, empresa.pass_jasper)
        # Params
        params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"), 'fecha_final': fecha_final.strftime("%Y-%m-%d")}
        # Init session so we have no need to auth again and again:
        s = requests.Session()
        r = s.get(url=url+report, auth=auth, params=params)
        response = HttpResponse(r.content, content_type='application/excel')
        response['Content-Disposition'] = 'attachment;filename="comprobantes_anulados_por_fecha.xls"'
        return response


class Reporte_104():
    def __init__(self):
        self.codigo = Codigo_SRI()
        self.lista = []
        self.total_base_0 = 0.0
        self.total_base_iva = 0.0
        self.total_monto_iva = 0.0
        self.total_ret_iva_30 = 0.0
        self.total_ret_iva_70 = 0.0
        self.total_ret_iva_100 = 0.0


def listar_reporte_104(obj, fecha_ini, fecha_fin, lista):
    """
    Lista el Objeto Reporte_104
    :param obj:
    :param fecha_ini:
    :param fecha_fin:
    :param lista:
    :return:
    """
    sumador1 = 0
    sumador2 = 0
    sumador3 = 0
    sumador4 = 0
    sumador5 = 0
    sumador6 = 0
    datos_reporte = Reporte_104()
    datos_reporte.codigo = obj
    datos_reporte.lista = CompraDetalle.objects.filter(status=1,
                                                       compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                       codigo_iva__id=obj.id,
                                                       ).order_by("-compra_cabecera__num_comp", "-compra_cabecera__fecha_reg")

    total_base_0 = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                  compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                  codigo_iva__id=obj.id,
                                                                  codigo_iva__porcentaje=0).exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor', field="valor"))["valor__sum"])

    total_base_0_nota_credito = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                               compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                               codigo_iva__id=obj.id,
                                                                               codigo_iva__porcentaje=0,
                                                                               compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor', field="valor"))["valor__sum"])

    total_base_iva = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                    compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                    codigo_iva__id=obj.id,
                                                                    codigo_iva__porcentaje__gt=0).exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor', field="valor"))["valor__sum"])

    total_base_iva_nota_credito = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                                 compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                                 codigo_iva__id=obj.id,
                                                                                 codigo_iva__porcentaje__gt=0,
                                                                                 compra_cabecera__documento__codigo_documento="04").aggregate(Sum('valor', field="valor"))["valor__sum"])

    total_monto_iva = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                     compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                     codigo_iva__id=obj.id,
                                                                     ).exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('monto_iva'))["monto_iva__sum"])

    total_monto_iva_nota_credito = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                                  compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                                  codigo_iva__id=obj.id,
                                                                                  compra_cabecera__documento__codigo_documento="04"
                                                                                  ).aggregate(Sum('monto_iva'))["monto_iva__sum"])

    total_ret_30 = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                  compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                  codigo_iva__id=obj.id,
                                                                  codigo_ret_iva__porcentaje=30).exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('monto_ret_iva'))["monto_ret_iva__sum"])

    total_ret_70 = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                  compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                  codigo_iva__id=obj.id,
                                                                  codigo_ret_iva__porcentaje=70).exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('monto_ret_iva'))["monto_ret_iva__sum"])

    total_ret_100 = convert_cero_none(CompraDetalle.objects.filter(status=1,
                                                                   compra_cabecera__fecha_reg__range=(fecha_ini, fecha_fin),
                                                                   codigo_iva__id=obj.id,
                                                                   codigo_ret_iva__porcentaje=100).exclude(compra_cabecera__documento__codigo_documento="04").aggregate(Sum('monto_ret_iva'))["monto_ret_iva__sum"])

    datos_reporte.total_base_0 = total_base_0 - total_base_0_nota_credito
    datos_reporte.total_base_iva = total_base_iva - total_base_iva_nota_credito
    datos_reporte.total_monto_iva = total_monto_iva - total_monto_iva_nota_credito
    datos_reporte.total_ret_iva_30 = total_ret_30
    datos_reporte.total_ret_iva_70 = total_ret_70
    datos_reporte.total_ret_iva_100 = total_ret_100
    if len(datos_reporte.lista) > 0:
        lista.append(datos_reporte)
        return datos_reporte.total_base_0, datos_reporte.total_base_iva, datos_reporte.total_monto_iva, \
               datos_reporte.total_ret_iva_30, datos_reporte.total_ret_iva_70, datos_reporte.total_ret_iva_100
    else:
        return 0, 0, 0, 0, 0, 0

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_104_10jun(request):
    """
    Función que me devuelve un html con el reporte
    104
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    lista = []
    buscador = BuscadorReportes(initial={'fecha_ini': get_fecha_rango_ant(now)[0].strftime("%Y-%m-%d"),
                                         'fecha_final': get_fecha_rango_ant(now)[1].strftime("%Y-%m-%d")})
    total_base_0 = 0
    total_base_iva = 0
    total_monto_iva = 0
    total_ret_iva_30 = 0
    total_ret_iva_70 = 0
    total_ret_iva_100 = 0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data de rept
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        if None not in (fecha_ini, fecha_fin):
            for obj in Codigo_SRI.objects.filter(vigencia_retencion__tipo_codigo_sri_id=2).order_by("codigo"):
                total_base_0i, total_base_ivai, total_monto_ivai, total_ret_iva_30i, total_ret_iva_70i, total_ret_iva_100i = listar_reporte_104(obj, fecha_ini, fecha_fin, lista)
                total_base_0 += total_base_0i
                total_base_iva += total_base_ivai
                total_monto_iva += total_monto_ivai
                total_ret_iva_30 += total_ret_iva_30i
                total_ret_iva_70 += total_ret_iva_70i
                total_ret_iva_100 += total_ret_iva_100i
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/reporte_104.html',
                              {"buscador": buscador,
                               "total_base_0": total_base_0,
                               "total_base_iva": total_base_iva,
                               "total_monto_iva": total_monto_iva,
                               "total_ret_iva_30": total_ret_iva_30,
                               "total_ret_iva_70": total_ret_iva_70,
                               "total_ret_iva_100": total_ret_iva_100,
                               "lista": lista
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_104_pdf(request):
    """
    Función que me devuelve un html con el reporte
    104
    :param request:
    :return:
    """
    totalfinal1 = 0
    totalfinal2 = 0
    totalfinal3 = 0
    totalfinal4 = 0
    totalfinal5 = 0
    totalfinal6 = 0
    empresa = Empresa.objects.filter(status=1)[0]
    lista = []
    lista_rep = []
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data de rept
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        if None not in (fecha_ini, fecha_fin):
            for obj in Codigo_SRI.objects.filter(vigencia_retencion__tipo_codigo_sri_id=2).order_by("codigo"):
                listar_reporte_104(obj, fecha_ini, fecha_fin, lista)
            for objl in lista:#la funcion de arriba le cambia los valores a un alista llamada lista
                totalfinal1 = totalfinal1+objl.total_base_0
                totalfinal2 = totalfinal2+objl.total_base_iva
                totalfinal3 = totalfinal3+objl.total_monto_iva
                totalfinal4 = totalfinal4+objl.total_ret_iva_30
                totalfinal5 = totalfinal5+objl.total_ret_iva_70
                totalfinal6 = totalfinal6+objl.total_ret_iva_100

        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

            # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 104
            return render_to_response('reporte_contabilidad/reporte_104.html',
                              {"buscador": buscador,
                               "totalfinal1": totalfinal1,
                               "totalfinal2": totalfinal2,
                               "totalfinal3": totalfinal3,
                               "totalfinal4": totalfinal4,
                               "totalfinal5": totalfinal5,
                               "totalfinal6": totalfinal6,
                               "lista": lista
                               }, context_instance=RequestContext(request))

    html = render_to_string('reporte_contabilidad/reporte_104_pdf.html',
                            {'pagesize': 'A4',
                             "lista": lista,
                             "totalfinal1": totalfinal1,
                             "totalfinal2": totalfinal2,
                             "totalfinal3": totalfinal3,
                             "totalfinal4": totalfinal4,
                             "totalfinal5": totalfinal5,
                             "totalfinal6": totalfinal6,
                             "empresa": empresa,
                             "request": request
                             }, context_instance=RequestContext(request))
    nombre = ('Reporte 104 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))
    return generar_pdf_nombre(html, nombre)


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_104_excel(request):
    """
    Función que me devuelve el reporte
    104 en formato excel
    :param request:
    :return:
    """
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        lista = [[u"", u"", u"", u"", u"", u"REPORTE 104"+TITULO]]
        lista_rep = []
        totalfinal1 = 0
        totalfinal2 = 0#ESTOS ACUMULADORES ME AYUDAN PARA LOS FESULTADOS FINALES DEL EXCEL :)
        totalfinal3 = 0
        totalfinal4 = 0
        totalfinal5 = 0
        totalfinal6 = 0
        if None not in (fecha_ini, fecha_fin):
            lista.append([u""])
            lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini.strftime("%Y-%m-%d")])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_fin.strftime("%Y-%m-%d")])
            lista.append([u""])

            for obj in Codigo_SRI.objects.filter(vigencia_retencion__tipo_codigo_sri_id=2).order_by("codigo"):
                listar_reporte_104(obj, fecha_ini, fecha_fin, lista_rep)

            for obj_cab in lista_rep:
                lista.append([u"Código:"+SUBTITULO, obj_cab.codigo.codigo])
                lista.append([u"descripción:"+SUBTITULO, obj_cab.codigo.descripcion])
                lista.append([u""])
                lista.append([u"Código"+COLUMNA, u"Fecha"+COLUMNA, u"Tipo Doc."+COLUMNA, u"# Documento"+COLUMNA,
                              u"Concepto"+COLUMNA, u"# Ret"+COLUMNA, u"Base0"+COLUMNA, u"BaseIva"+COLUMNA,
                              u"Monto IVA"+COLUMNA, u"30%"+COLUMNA, u"70%"+COLUMNA, u"100%"+COLUMNA])
                for obj in obj_cab.lista:
                    if obj.codigo_iva.porcentaje == 0:
                        base0 = obj.valor
                        baseiva = 0.00
                    else:
                        base0 = 0.00
                        baseiva = obj.valor
                    try:
                        if obj.codigo_ret_iva.porcentaje == 30:
                            obj_list = [obj.codigo_iva.codigo,
                                        obj.compra_cabecera.fecha_reg.strftime("%Y-%m-%d"),
                                        obj.compra_cabecera.documento.descripcion_documento,
                                        obj.compra_cabecera.num_doc,
                                        obj.compra_cabecera.concepto,
                                        obj.compra_cabecera.get_num_retencion(),
                                        base0,
                                        baseiva,
                                        obj.monto_iva,
                                        obj.monto_ret_iva,
                                        0.0,
                                        0.0]
                        elif obj.codigo_ret_iva.porcentaje == 70:
                            obj_list = [obj.codigo_iva.codigo,
                                        obj.compra_cabecera.fecha_reg.strftime("%Y-%m-%d"),
                                        obj.compra_cabecera.documento.descripcion_documento,
                                        obj.compra_cabecera.num_doc,
                                        obj.compra_cabecera.concepto,
                                        obj.compra_cabecera.get_num_retencion(),
                                        base0,
                                        baseiva,
                                        obj.monto_iva,
                                        0.0,
                                        obj.monto_ret_iva,
                                        0.0]
                        else:
                            obj_list = [obj.codigo_iva.codigo,
                                        obj.compra_cabecera.fecha_reg.strftime("%Y-%m-%d"),
                                        obj.compra_cabecera.documento.descripcion_documento,
                                        obj.compra_cabecera.num_doc,
                                        obj.compra_cabecera.concepto,
                                        obj.compra_cabecera.get_num_retencion(),
                                        base0,
                                        baseiva,
                                        obj.monto_iva,
                                        0.0,
                                        0.0,
                                        obj.monto_ret_iva]
                    except:
                        obj_list = [obj.codigo_iva.codigo,
                                    obj.compra_cabecera.fecha_reg.strftime("%Y-%m-%d"),
                                    obj.compra_cabecera.documento.descripcion_documento,
                                    obj.compra_cabecera.num_doc,
                                    obj.compra_cabecera.concepto,
                                    obj.compra_cabecera.get_num_retencion(),
                                    base0,
                                    baseiva,
                                    obj.monto_iva,
                                    0.0,
                                    0.0,
                                    0.0]
                    lista.append(obj_list)
                # Totales
                lista.append([u"Totales:"+SUBTITULO, u"", u"", u"", u"", u"", str(obj_cab.total_base_0)+G_TOTAL,
                              str(obj_cab.total_base_iva)+G_TOTAL, str(obj_cab.total_monto_iva)+G_TOTAL,
                              str(obj_cab.total_ret_iva_30)+G_TOTAL, str(obj_cab.total_ret_iva_70)+G_TOTAL,
                              str(obj_cab.total_ret_iva_100)+G_TOTAL])
                lista.append([u""])
                totalfinal1 = totalfinal1+obj_cab.total_base_0
                totalfinal2 = totalfinal2+obj_cab.total_base_iva
                totalfinal3 = totalfinal3+obj_cab.total_monto_iva
                totalfinal4 = totalfinal4+obj_cab.total_ret_iva_30
                totalfinal5 = totalfinal5+obj_cab.total_ret_iva_70
                totalfinal6 = totalfinal6+obj_cab.total_ret_iva_100
            lista.append([u"", u"", u"", u"", u"", u"", u"TOTAL BASE 0"+SUBTITULO,u"TOTAL BASE IVA"+SUBTITULO,u"TOTAL MONTO IVA"+SUBTITULO,u"TOTAL RET 30"+SUBTITULO,U"TOTAL RET 70"+SUBTITULO,u"TOTAL RET 100"+SUBTITULO])#estas cosas son la informacion que no se mostraba, son los acumuladores agregados arriba
            lista.append([u"", u"", u"", u"", u"", u"",str(totalfinal1),str(totalfinal2),str(totalfinal3),str(totalfinal4),str(totalfinal5),str(totalfinal6)])

            return ExcelResponse(lista, 'Reporte 104 desde ' + fecha_ini.strftime("%Y-%m-%d") + ' hasta: ' + fecha_fin.strftime("%Y-%m-%d"))
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

            # Si tiene errores no le devuelve un pdf si no que se le retorna a la pagina de reporte 104
            return render_to_response('reporte_contabilidad/reporte_104.html',
                              {"buscador": buscador,
                               "lista": lista_rep
                               }, context_instance=RequestContext(request))


class SaldoCtaMayor():
    """
    Clase que me ayuda a agrupar la cuenta con el saldo
    en el libro mayor
    """
    def __init__(self, saldo):
        self.detalle = Detalle_Comp_Contable()
        self.saldo = saldo


class LibroMayor():
    def __init__(self, saldo, lista):
        self.saldo = saldo
        self.saldo_debe = 0.0
        self.saldo_haber = 0.0
        self.lista = lista
        self.cuenta = PlanCuenta()


def listar_libro_mayor(buscador, fecha_ini, fecha_fin, cuentas, lista, request):
    """
    Lista en un arreglo las cuentas con sus moviemientos para el libro mayor
    :param buscador:
    :param fecha_ini:
    :param fecha_fin:
    :param cuentas:
    :param lista:
    :return:
    """

    lista_id = []
    if buscador.getSelAll():
        for obj_cta in PlanCuenta.objects.filter(status=1,
                                                 nivel=5).order_by("codigo"):
            total_debe = 0.0
            total_haber = 0.0
            saldo_inicial = obj_cta.get_saldo_cuenta_antes_fecha(fecha_ini)
            saldo_actual = saldo_inicial
            lista_cuenta = []
            for obj_det in Detalle_Comp_Contable.objects.filter(
                    plan_cuenta=obj_cta,
                    fecha_asiento__range=(fecha_ini, fecha_fin),
                    status=1).order_by("fecha_asiento", "cabecera_contable__numero_comprobante"):
                if obj_det.dbcr == "D":
                    total_debe += redondeo(obj_det.valor)
                else:
                    total_haber += redondeo(obj_det.valor)

                if obj_cta.naturaleza == "D" and obj_det.dbcr == "D"\
                        or obj_cta.naturaleza == "C" and obj_det.dbcr == "H":
                    saldo_actual += obj_det.valor
                else:
                    saldo_actual -= obj_det.valor

                cuenta_saldo = SaldoCtaMayor(redondeo(saldo_actual))
                cuenta_saldo.detalle = obj_det
                lista_cuenta.append(cuenta_saldo)

            if not (saldo_inicial == 0 and len(lista_cuenta) == 0):
                libro = LibroMayor(saldo_inicial, lista_cuenta)
                libro.cuenta = obj_cta
                libro.saldo_debe = redondeo(total_debe)
                libro.saldo_haber = redondeo(total_haber)
                lista.append(libro)
    else:
        if cuentas.is_valid():
            for obj in cuentas:
                informacion = obj.cleaned_data
                try:
                    cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"))
                    total_debe = 0.0
                    total_haber = 0.0

                    # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                    if cuenta.id not in lista_id:
                        lista_id.append(cuenta.id)
                        saldo_inicial = cuenta.get_saldo_cuenta_antes_fecha(fecha_ini)
                        saldo_actual = saldo_inicial
                        lista_cuenta = []
                        for obj_det in Detalle_Comp_Contable.objects.filter(
                                plan_cuenta=cuenta,
                                fecha_asiento__range=(fecha_ini, fecha_fin),
                                status=1).order_by("fecha_asiento", "cabecera_contable__numero_comprobante"):
                            if obj_det.dbcr == "D":
                                total_debe += obj_det.valor
                            else:
                                total_haber += obj_det.valor

                            if cuenta.naturaleza == "D" and obj_det.dbcr == "D"\
                                or cuenta.naturaleza == "C" and obj_det.dbcr == "H":
                                saldo_actual += obj_det.valor
                            else:
                                saldo_actual -= obj_det.valor

                            cuenta_saldo = SaldoCtaMayor(redondeo(saldo_actual))
                            cuenta_saldo.detalle = obj_det
                            lista_cuenta.append(cuenta_saldo)

                        if not (saldo_inicial == 0 and len(lista_cuenta) == 0):
                            libro = LibroMayor(saldo_inicial, lista_cuenta)
                            libro.cuenta = cuenta
                            libro.saldo_debe = redondeo(total_debe)
                            libro.saldo_haber = redondeo(total_haber)
                            lista.append(libro)
                except PlanCuenta.DoesNotExist:
                    pass
        if len(lista_id) == 0:
            messages.error(request, u"Debe selecionar al menos una cuenta")
            return 1
    return 0

################################
#  FALTA LOS SALDOS INICIALES  #
################################

@login_required()
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_libro_mayor(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="cuentas")
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        cuentas = cuentas_formset(request.POST, prefix="cuentas")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""
        urljasper = get_url_jasper()
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:
            report = get_nombre_reporte('CONT_LIBROMAYOR') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    try:
                        cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"), status=1)
                        id_cuentas += str(cuenta.id) + ","
                    except PlanCuenta.DoesNotExist:
                        pass

            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0

            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'todos': todos,
                      'cuenta': id_cuentas[0:-1]
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            if r.status_code != 200:
                r = None
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/libro_mayor.html',
                              {"buscador": buscador,
                               "lista": r,
                               "cuentas": cuentas
                               }, context_instance=RequestContext(request))


"""
@login_required()
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_libro_mayor(request):

    lista = []
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})
    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="cuentas")
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        cuentas = cuentas_formset(request.POST, prefix="cuentas")

        if None not in(fecha_ini, fecha_fin):
            listar_libro_mayor(buscador, fecha_ini, fecha_fin, cuentas, lista, request)
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/libro_mayor.html',
                              {"buscador": buscador,
                               "lista": lista,
                               "cuentas": cuentas
                               }, context_instance=RequestContext(request))

"""


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_libro_mayor_pdf(request):
    """
        Función que me retorna el un template con el reporte de libro mayor
        :param request:
        :return:
    """
    cuentas_formset = formset_factory(ReporteSaldosForm)
    r = None
    buscador = BuscadorReportes_libro_mayor()
    cuentas = cuentas_formset(prefix="cuentas")#creo que esto faltaba de poner 1 julio porque decia que no existia una variable y se asignaba por abajo
    if request.method == "POST":
        buscador = BuscadorReportes_libro_mayor(request.POST)
        if buscador.is_valid():
            fecha_ini = buscador.getFechaIni()
            fecha_fin = buscador.getFechaFinal()
            cuentas = cuentas_formset(request.POST, prefix="cuentas")
            id_cuentas = ""
            urljasper = get_url_jasper()
            if None not in(fecha_ini, fecha_fin):
                empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
                url = urljasper + empresa.path_jasper + '/'
                # Report to process:
                report = get_nombre_reporte('CONT_LIBROMAYOR') + '.pdf'

                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                if cuentas.is_valid():
                    for obj in cuentas:
                        informacion = obj.cleaned_data
                        try:
                            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"), status=1)
                            id_cuentas += str(cuenta.id) + ","
                        except PlanCuenta.DoesNotExist:
                            pass

                if buscador.getSelAll():
                    todos = 1
                else:
                    todos = 0

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                          'todos': todos,
                          'cuenta': id_cuentas[0:-1]
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

                response = HttpResponse(r.content, content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="LibroMayor.pdf"'
                return response
            else:
                if fecha_ini is None:
                    errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                    errors.append(u"La fecha inicial es requerido")
                if fecha_fin is None:
                    errors = buscador._errors.setdefault("fecha_final", ErrorList())
                    errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/libro_mayor.html',
                      {"buscador": buscador,
                       "lista": r,
                       "cuentas": cuentas
                       }, context_instance=RequestContext(request))

@login_required()
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_libro_mayor_excel(request):
    """
    Función que me retorna el un hoja excel con el reporte de libro mayor
    :param request:
    :return:
    """

    cuentas_formset = formset_factory(ReporteSaldosForm)
    r = None
    buscador = BuscadorReportes_libro_mayor()
    cuentas = cuentas_formset(prefix="cuentas")
    if request.method == "POST":
        buscador = BuscadorReportes_libro_mayor(request.POST)
        if buscador.is_valid():
            fecha_ini = buscador.getFechaIni()
            fecha_fin = buscador.getFechaFinal()
            cuentas = cuentas_formset(request.POST, prefix="cuentas")
            id_cuentas = ""
            urljasper = get_url_jasper()
            if None not in(fecha_ini, fecha_fin):
                empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
                url = urljasper + empresa.path_jasper + '/'

                # Report to process:
                report = get_nombre_reporte('CONT_LIBROMAYOR') + '.xls'

                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                if cuentas.is_valid():
                    for obj in cuentas:
                        informacion = obj.cleaned_data
                        try:
                            cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"), status=1)
                            id_cuentas += str(cuenta.id) + ","
                        except PlanCuenta.DoesNotExist:
                            pass

                if buscador.getSelAll():
                    todos = 1
                else:
                    todos = 0

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                          'todos': todos,
                          'cuenta': id_cuentas[0:-1]
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

                response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                nombre = "Reporte Libro Mayor del "+str(fecha_ini)+" al "+str(fecha_fin)
                response['Content-Disposition'] = 'attachment;filename="' + nombre + '".xls"'
                return response
            else:
                if fecha_ini is None:
                    errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                    errors.append(u"La fecha inicial es requerido")
                if fecha_fin is None:
                    errors = buscador._errors.setdefault("fecha_final", ErrorList())
                    errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/libro_mayor.html',
                      {"buscador": buscador,
                       "lista": r,
                       "cuentas": cuentas
                       }, context_instance=RequestContext(request))


@login_required()
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_libro_mayor_centro_costo(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="cuentas")
    r = None
    urljasper = get_url_jasper()
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        centro_costo = buscador.getCentroCosto()
        cuentas = cuentas_formset(request.POST, prefix="cuentas")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""
        if None not in(fecha_ini, fecha_fin, centro_costo):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:
            #CONT_LIBROMAYORCENTROCOSTO
            report = get_nombre_reporte('CONT_LIBROMAYORCENTROCOSTO') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    try:
                        cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"), status=1)
                        id_cuentas += str(cuenta.id) + ","
                    except PlanCuenta.DoesNotExist:
                        pass

            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0

            # Params
            params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'todos': todos,
                      'centro_costo_id': centro_costo.id,
                      'cuentas': id_cuentas[0:-1]
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)

            if r.status_code != 200:
                r = None
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
            if centro_costo is None:
                errors = buscador._errors.setdefault("centro_costo", ErrorList())
                errors.append(u"El centro de costo es requerido")

    return render_to_response('reporte_contabilidad/libro_mayor_centro_costo.html',
                              {"buscador": buscador,
                               "lista": r,
                               "cuentas": cuentas
                               }, context_instance=RequestContext(request))


@login_required()
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_libro_mayor_centro_costo_excel(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="cuentas")
    r = None
    urljasper = get_url_jasper()
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        centro_costo = buscador.getCentroCosto()

        cuentas = cuentas_formset(request.POST, prefix="cuentas")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""

        if None not in(fecha_ini, fecha_fin, centro_costo):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:
            report = get_nombre_reporte('CONT_LIBROMAYORCENTROCOSTO') + '.xls'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    try:
                        cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"), status=1)
                        id_cuentas += str(cuenta.id) + ","
                    except PlanCuenta.DoesNotExist:
                        pass

            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0

            # Params
            params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'todos': todos,
                      'centro_costo_id': centro_costo.id,
                      'cuentas': id_cuentas[0:-1]
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)

            if r.status_code != 200:
                r = None
            else:
                response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                nombre = "Rpt_Libro_Mayor_x_Ctr_ Costo_"+unicode(centro_costo)+"_"+str(fecha_ini)+"_"+str(fecha_fin)
                response['Content-Disposition'] = 'attachment;filename="' + nombre + '".xls"'
                return response
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/libro_mayor_centro_costo.html',
                              {"buscador": buscador,
                               "lista": r,
                               "cuentas": cuentas
                               }, context_instance=RequestContext(request))


@login_required()
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_libro_mayor_centro_costo_pdf(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="cuentas")
    r = None
    urljasper = get_url_jasper()
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        centro_costo = buscador.getCentroCosto()
        cuentas = cuentas_formset(request.POST, prefix="cuentas")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""
        if None not in(fecha_ini, fecha_fin, centro_costo):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:
            report = get_nombre_reporte('CONT_LIBROMAYORCENTROCOSTO') + '.pdf'
            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    try:
                        cuenta = PlanCuenta.objects.get(id=informacion.get("cuenta"), status=1)
                        id_cuentas += str(cuenta.id) + ","
                    except PlanCuenta.DoesNotExist:
                        pass

            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0

            # Params
            params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'todos': todos,
                      'centro_costo_id': centro_costo.id,
                      'cuentas': id_cuentas[0:-1]
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)

            if r.status_code != 200:
                r = None
            else:
                nombrereporte = "Rpt_Libro_Mayor_x_Ctr_ Costo_"+unicode(centro_costo)+"_"+str(fecha_ini)+"_"+str(fecha_fin)
                response = HttpResponse(r.content, content_type='application/pdf')
                response['Content-Disposition'] = 'attachment; filename="'+nombrereporte+'.pdf"'
                return response
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/libro_mayor_centro_costo.html',
                              {"buscador": buscador,
                               "lista": r,
                               "cuentas": cuentas
                               }, context_instance=RequestContext(request))


class BalanceComprobacion():
    def __init__(self):
        self.saldo_inicial = 0.0
        self.saldo_acreedor = 0.0
        self.saldo_deudor = 0.0
        self.saldo_final = 0.0
        self.plan_cuenta = PlanCuenta()


def lista_balance_comprobacion(acum, total_debe, total_haber, lista_balance, tipo, mes, anio):
    """
    Función que me lista los movimientos de las cuentas del balance de comprobación y los guarda
    en un arreglo

    :param acum: Si es reporte acumulado
    :param total_debe:  guarda los totales del debe
    :param total_haber: guarda los totales del haber
    :param lista_balance:  Lista donde se guardan el detalle del reporte
    :param tipo: Me especifica como se llenará la lista_balance, si es para HTML, PDF o para archivo EXCEl
    :param mes:  El mes que se desea tener el reporte

    :return:
    """
    # Para los totales
    total_saldo_ini_debe = 0.0
    total_saldo_ini_haber = 0.0
    total_saldo_final_debe = 0.0
    total_saldo_final_haber = 0.0

    if acum:  # Acumulado
        for obj in PlanCuenta.objects.filter(status=1).order_by("codigo"):
            try:
                saldo_inicial_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=obj,
                                                                               anio=anio).saldo_inicial)

                saldo_deudor_acum = convert_cero_none(SaldoCuenta.objects.get(anio=anio,
                                                                              plan_cuenta=obj).get_saldo_debe_acum(mes))

                saldo_acreedor_acum = convert_cero_none(SaldoCuenta.objects.get(anio=anio,
                                                                                plan_cuenta=obj).get_saldo_haber_acum(mes))

            except SaldoCuenta.DoesNotExist:
                saldo_inicial_anio = 0.0
                saldo_deudor_acum = 0.0
                saldo_acreedor_acum = 0.0
            except:
                print 'except mas general de los plancuentas'+str(obj.codigo)+'nooo :o'
                saldo_inicial_anio = 0.0
                saldo_deudor_acum = 0.0
                saldo_acreedor_acum = 0.0

            # Solo se presenta las cuentas que hayan tenido algun movimiento

            if (saldo_inicial_anio, saldo_deudor_acum, saldo_acreedor_acum) != (0.0, 0.0, 0.0):
                balance = BalanceComprobacion()
                balance.saldo_deudor = redondeo(saldo_deudor_acum)
                balance.saldo_acreedor = redondeo(saldo_acreedor_acum)
                balance.plan_cuenta = obj
                balance.saldo_inicial = saldo_inicial_anio

                if balance.plan_cuenta.naturaleza == "D":
                    balance.saldo_final = redondeo(balance.saldo_inicial + saldo_deudor_acum - saldo_acreedor_acum)
                else:
                    balance.saldo_final = redondeo(balance.saldo_inicial - saldo_deudor_acum + saldo_acreedor_acum)

                # Sumatoria de totales debito y crédito y saldos
                total_debe += balance.saldo_deudor
                total_haber += balance.saldo_acreedor

                if obj.naturaleza == "D":
                    total_saldo_ini_debe += balance.saldo_inicial
                    total_saldo_final_debe += balance.saldo_final
                else:
                    total_saldo_ini_haber += balance.saldo_inicial
                    total_saldo_final_haber += balance.saldo_final

                if tipo == 1:  # HTML y PDF
                    lista_balance.append(balance)
                elif tipo == 2:  # EXCEL
                    # Llenar la lista para el excel
                    lista_balance.append([balance.plan_cuenta.codigo, unicode(balance.plan_cuenta.descripcion),
                                          balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor,
                                          balance.saldo_final])
                else:
                    pass

    else:  # No acumulado
        for obj in PlanCuenta.objects.filter(status=1).order_by("codigo"):
            ## Saldos
            try:
                saldo_cuenta = SaldoCuenta.objects.get(plan_cuenta=obj, anio=anio)
                saldo_inicial_anio = convert_cero_none(saldo_cuenta.saldo_inicial)
                saldo_deudor = convert_cero_none(saldo_cuenta.get_saldo_debe_mes(mes))
                saldo_acreedor = convert_cero_none(saldo_cuenta.get_saldo_haber_mes(mes))

                if obj.naturaleza == "D":
                    saldo_acum = saldo_cuenta.get_saldo_debe_acum(mes-1) - saldo_cuenta.get_saldo_haber_acum(mes-1)
                else:
                    saldo_acum = saldo_cuenta.get_saldo_haber_acum(mes-1) - saldo_cuenta.get_saldo_debe_acum(mes-1)

            except:
                saldo_inicial_anio = 0.0
                saldo_deudor = 0.0
                saldo_acreedor = 0.0
                saldo_acum = 0.0

            saldo_inicial = saldo_inicial_anio + saldo_acum

            if (saldo_inicial, saldo_deudor, saldo_acreedor) != (0, 0, 0):
                balance = BalanceComprobacion()
                balance.saldo_inicial = redondeo(saldo_inicial)
                balance.saldo_deudor = redondeo(saldo_deudor)
                balance.saldo_acreedor = redondeo(saldo_acreedor)
                balance.plan_cuenta = obj

                if balance.plan_cuenta.naturaleza == "D":
                    balance.saldo_final = redondeo(saldo_inicial + saldo_deudor - saldo_acreedor)
                else:
                    balance.saldo_final = redondeo(saldo_inicial - saldo_deudor + saldo_acreedor)

                # Sumatoria de totales debito y crédito y saldos
                total_debe += balance.saldo_deudor
                total_haber += balance.saldo_acreedor

                if obj.naturaleza == "D":
                    total_saldo_ini_debe += balance.saldo_inicial
                    total_saldo_final_debe += balance.saldo_final
                else:
                    total_saldo_ini_haber += balance.saldo_inicial
                    total_saldo_final_haber += balance.saldo_final

                if tipo == 1:  # HTML y PDF
                    lista_balance.append(balance)
                elif tipo == 2:  # EXCEL
                    # Llenar la lista para el excel
                    lista_balance.append([balance.plan_cuenta.codigo, unicode(balance.plan_cuenta.descripcion),
                                          balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor,
                                          balance.saldo_final])
                else:
                    pass
    total_saldo_ini = total_saldo_ini_debe - total_saldo_ini_haber
    total_saldo_final = total_saldo_final_debe - total_saldo_final_haber

    return redondeo(total_saldo_ini), redondeo(total_saldo_final), redondeo(total_debe), redondeo(total_haber)


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_balance_comprobacion(request):
    """
    Función que retorna el template del balance de comprobación
    :param request:
    :return:
    """
    buscador = BuscadorReportes_balance_comprobacion()
    total_debe = 0.0
    total_haber = 0.0
    lista_balance = []
    total_saldo_ini = 0.0
    total_saldo_final = 0.0
    if request.method == "POST":
        buscador = BuscadorReportes_balance_comprobacion(request.POST)

        if buscador.is_valid() and  buscador.getMes() != 0 and buscador.getAnio() != 0:
            acum = buscador.getSelAll()
            mes = buscador.getMes()
            anio = buscador.getAnio()
            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            # Función que me enlista el detalle del balance de comprobación
            total_saldo_ini, total_saldo_final, total_debe, total_haber = \
                lista_balance_comprobacion(acum, total_debe, total_haber, lista_balance, 1, mes, anio)
        else:

            if buscador.getMes() == 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Por favor seleccione un mes en el formulario")

            if buscador.getAnio() == 0:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Por favor seleccione un año en el formulario")

    return render_to_response('reporte_contabilidad/balance_comprobacion.html',
                              {"buscador": buscador,
                               "lista": lista_balance,
                               "total_debe": total_debe,
                               "total_haber": total_haber,
                               "total_saldo_ini": total_saldo_ini,
                               "total_saldo_final": total_saldo_final
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_balance_comprobacion_pdf(request):
    """
    Función que retorna el template del balance de comprobación
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    total_debe = 0.0
    total_haber = 0.0
    total_saldo_ini = 0.0
    total_saldo_final = 0.0
    lista_balance = []
    buscador = BuscadorReportes()
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        if buscador.is_valid() and buscador.getMes() != 0 and buscador.getAnio() != 0:
            acum = buscador.getSelAll()
            mes = buscador.getMes()
            anio = buscador.getAnio()
            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()

            # Función que me enlista el detalle del balance de comprobación
            total_saldo_ini, total_saldo_final, total_debe, total_haber = lista_balance_comprobacion(acum,
                                                                                 total_debe,
                                                                                 total_haber,
                                                                                 lista_balance,
                                                                                 1,
                                                                                 mes, anio)

            html = render_to_string('reporte_contabilidad/balance_comprobacion_pdf.html',
                                    {'pagesize': 'A4',
                                     "lista": lista_balance,
                                     "total_debe": total_debe,
                                     "total_haber": total_haber,
                                     "total_saldo_ini": total_saldo_ini,
                                     "empresa": empresa,
                                     "total_saldo_final": total_saldo_final,
                                     "request": request}, context_instance=RequestContext(request))
            nombre = "Bal_comprobacion_"+str(get_mes(mes))+"-"+str(buscador.getAnio())
            return generar_pdf_nombre(html,nombre)

        else:

            if buscador.getMes() == 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Por favor seleccione un mes en el formulario")

            if buscador.getAnio() == 0:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Por favor seleccione un año en el formulario")

            return render_to_response('reporte_contabilidad/balance_comprobacion.html',
                              {"buscador": buscador,
                               "lista": lista_balance,
                               "total_debe": total_debe,
                               "total_haber": total_haber,
                               "total_saldo_ini": total_saldo_ini,
                               "total_saldo_final": total_saldo_final
                               }, context_instance=RequestContext(request))
    else:
        return render_to_response('reporte_contabilidad/balance_comprobacion.html',
                              {"buscador": buscador,
                               "lista": lista_balance,
                               "total_debe": total_debe,
                               "total_haber": total_haber,
                               "total_saldo_ini": total_saldo_ini,
                               "total_saldo_final": total_saldo_final
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_balance_comprobacion_excel(request):
    """
    Función que retorna el excel del balance de comprobación
    :param request:
    :return:
    """
    total_debe = 0.0
    total_haber = 0.0
    total_saldo_ini = 0.0
    total_saldo_final = 0.0
    lista_balance = []
    buscador = BuscadorReportes()
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        if buscador.is_valid() and buscador.getMes() != 0 and buscador.getAnio() != 0:
            lista_balance.append([u"", u"", u"", u"BALANCE DE COMPROBACIÓN%/%1"])
            acum = buscador.getSelAll()
            mes = buscador.getMes()
            anio = buscador.getAnio()

            #  Actualiza la tabla de saldo de cuentas
            cursor = connection.cursor()
            try:
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                row = cursor.fetchone()
            except:
                pass
            finally:
                cursor.close()
            lista_balance.append([u"MES: %/%2", get_mes(mes)])
            lista_balance.append([u"AÑO: %/%2", str(anio)])

            if acum:
                lista_balance.append([u"Acumulado:%/%2", u"SI"])
            else:
                lista_balance.append([u"Acumulado:%/%2", u"NO"])
            lista_balance.append([u""])
            lista_balance.append([u"Cuenta%/%2", u"Descripción%/%2", u"Saldo inicial%/%2",
                                  u"Total Débito%/%2", u"Total Crédito%/%2", u"Saldo Final%/%2"])

            # Función que me enlista el detalle del balance de comprobación
            total_saldo_ini, total_saldo_final, total_debe, total_haber = lista_balance_comprobacion(acum, total_debe, total_haber, lista_balance, 2, mes, anio)

            lista_balance.append([u"Totales:%/%2", u"", total_saldo_ini, total_debe,
                                  total_haber, total_saldo_final])

            nombre = "Bal_comprobacion_"+str(get_mes(mes))+"-"+str(buscador.getAnio())
            #return generar_pdf_nombre(html,nombre)
            return ExcelResponse(lista_balance, unicode(nombre))
        else:
            if buscador.getMes() == 0:
                errors = buscador._errors.setdefault("mes", ErrorList())
                errors.append(u"Por favor seleccione un mes en el formulario")
            if buscador.getAnio() == 0:
                errors = buscador._errors.setdefault("anio", ErrorList())
                errors.append(u"Por favor seleccione un año en el formulario")
            return render_to_response('reporte_contabilidad/balance_comprobacion.html',
                              {"buscador": buscador,
                               "lista": lista_balance,
                               "total_debe": total_debe,
                               "total_haber": total_haber,
                               "total_saldo_ini": total_saldo_ini,
                               "total_saldo_final": total_saldo_final
                               }, context_instance=RequestContext(request))
    else:
        return render_to_response('reporte_contabilidad/balance_comprobacion.html',
                              {"buscador": buscador,
                               "lista": lista_balance,
                               "total_debe": total_debe,
                               "total_haber": total_haber,
                               "total_saldo_ini": total_saldo_ini,
                               "total_saldo_final": total_saldo_final
                               }, context_instance=RequestContext(request))

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_balance_comprobacion2(request):
    """
    Vista que saca el reporte de balance de comprobación cuando no existe la
    tabla saldo_cuenta
    Función que retorna el template del balance de comprobación
    :param request:
    :return:
    """
    buscador = BuscadorReportes()
    total_debe = 0.0
    total_haber = 0.0
    total_saldo_ini = 0.0
    total_saldo_final = 0.0
    lista_balance = []

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        acum = buscador.getSelAll()
        mes = buscador.getMes()

        # Para los totales
        total_saldo_ini_debe = 0.0
        total_saldo_ini_haber = 0.0
        total_saldo_final_debe = 0.0
        total_saldo_final_haber = 0.0

        if acum:  # Acumulado
            for obj in PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo"):
                now = datetime.datetime.now()
                try:
                    saldo_inicial_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=obj, anio=now.year).saldo_inicial)
                except:
                    saldo_inicial_anio = 0.0

                ini_fin_mes = calendar.monthrange(now.year, mes)

                saldo_deudor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                      fecha_asiento__range=(str(now.year)+'-01-01',
                                                                                                            str(now.year)+"-"'0'+str(mes)+"-"+str(ini_fin_mes[1])),
                                                                                      status=1,
                                                                                      dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])
                saldo_acreedor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                        fecha_asiento__range=(str(now.year)+'-01-01',
                                                                                                              str(now.year)+"-"'0'+str(mes)+"-"+str(ini_fin_mes[1])),
                                                                                        status=1,
                                                                                        dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])
                balance = BalanceComprobacion()
                balance.saldo_inicial = saldo_inicial_anio
                balance.saldo_deudor = saldo_deudor
                balance.saldo_acreedor = saldo_acreedor
                balance.plan_cuenta = obj

                if balance.plan_cuenta.naturaleza == "D":
                    balance.saldo_final = saldo_inicial_anio + saldo_deudor - saldo_acreedor
                else:
                    balance.saldo_final = saldo_inicial_anio - saldo_deudor + saldo_acreedor

                # Sumatoria de totales debito y crédito y saldos
                total_debe += balance.saldo_deudor
                total_haber += balance.saldo_acreedor
                if obj.naturaleza == "D":
                    total_saldo_ini_debe += balance.saldo_inicial
                    total_saldo_final_debe += balance.saldo_final
                else:
                    total_saldo_ini_haber += balance.saldo_inicial
                    total_saldo_final_haber += balance.saldo_final

                # Solo se presenta las cuentas que hayan tenido algun movimiento
                if (balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor) != (0, 0, 0):
                    lista_balance.append(balance)

        else:  # No acumulado
            for obj in PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo"):
                now = datetime.datetime.now()
                ## Saldo Inicial del año
                try:
                    saldo_inicial_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=obj, anio=now.year).saldo_inicial)
                except:
                    saldo_inicial_anio = 0.0

                ## Se valida si el mes que ingreso es mayor a enero para sumarizar el saldo inicial
                if mes > 1:
                    saldo_acum_debe = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                               fecha_asiento__lt=str(now.year)+"-0"+str(mes)+"-01",
                                                               fecha_asiento__year=now.year,
                                                               status=1,
                                                               dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])

                    saldo_acum_haber = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                               fecha_asiento__lt=str(now.year)+"-0"+str(mes)+"-01",
                                                               fecha_asiento__year=now.year,
                                                               status=1,
                                                               dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])

                    if obj.naturaleza == "D":
                        saldo_acum = saldo_acum_debe - saldo_acum_haber
                    else:
                        saldo_acum = saldo_acum_haber - saldo_acum_debe
                else:
                    saldo_acum = 0.0

                saldo_inicial = saldo_inicial_anio + saldo_acum

                saldo_deudor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                      fecha_asiento__month=mes,
                                                                                      status=1,
                                                                                      dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])
                saldo_acreedor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                        fecha_asiento__month=mes,
                                                                                        status=1,
                                                                                        dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])


                balance = BalanceComprobacion()
                balance.saldo_inicial = saldo_inicial
                balance.saldo_deudor = saldo_deudor
                balance.saldo_acreedor = saldo_acreedor
                balance.plan_cuenta = obj
                if balance.plan_cuenta.naturaleza == "D":
                    balance.saldo_final = saldo_inicial + saldo_deudor - saldo_acreedor
                else:
                    balance.saldo_final = saldo_inicial - saldo_deudor + saldo_acreedor

                # Sumatoria de totales debito y crédito y saldos
                total_debe += balance.saldo_deudor
                total_haber += balance.saldo_acreedor
                if obj.naturaleza == "D":
                    total_saldo_ini_debe += balance.saldo_inicial
                    total_saldo_final_debe += balance.saldo_final
                else:
                    total_saldo_ini_haber += balance.saldo_inicial
                    total_saldo_final_haber += balance.saldo_final

                # Solo se presenta las cuentas que hayan tenido algun movimiento
                if (balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor) != (0, 0, 0):
                    lista_balance.append(balance)
        total_saldo_ini = total_saldo_ini_debe - total_saldo_ini_haber
        total_saldo_final = total_saldo_final_debe - total_saldo_final_haber
    return render_to_response('reporte_contabilidad/balance_comprobacion.html',
                              {"buscador": buscador,
                               "lista": lista_balance,
                               "total_debe": total_debe,
                               "total_haber": total_haber,
                               "total_saldo_ini": total_saldo_ini,
                               "total_saldo_final": total_saldo_final
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_balance_comprobacion_pdf2(request):
    """
    Vista que saca el reporte de balance de comprobación cuando no existe la
    tabla saldo_cuenta
    Función que retorna el template del balance de comprobación
    :param request:
    :return:
    """
    total_debe = 0.0
    total_haber = 0.0
    total_saldo_ini = 0.0
    total_saldo_final = 0.0
    lista_balance = []

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        acum = buscador.getSelAll()
        mes = buscador.getMes()

        # Para los totales
        total_saldo_ini_debe = 0.0
        total_saldo_ini_haber = 0.0
        total_saldo_final_debe = 0.0
        total_saldo_final_haber = 0.0

        if acum:  # Acumulado
            for obj in PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo"):
                now = datetime.datetime.now()
                try:
                    saldo_inicial_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=obj, anio=now.year).saldo_inicial)
                except:
                    saldo_inicial_anio = 0.0

                ini_fin_mes = calendar.monthrange(now.year, mes)
                saldo_deudor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                      fecha_asiento__range=(str(now.year)+'-01-01',
                                                                                                            str(now.year)+"-"'0'+str(mes)+"-"+str(ini_fin_mes[1])),
                                                                                      status=1,
                                                                                      dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])
                saldo_acreedor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                        fecha_asiento__range=(str(now.year)+'-01-01',
                                                                                                              str(now.year)+"-"'0'+str(mes)+"-"+str(ini_fin_mes[1])),
                                                                                        status=1,
                                                                                        dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])

                balance = BalanceComprobacion()
                balance.saldo_inicial = saldo_inicial_anio
                balance.saldo_deudor = saldo_deudor
                balance.saldo_acreedor = saldo_acreedor
                balance.plan_cuenta = obj

                if balance.plan_cuenta.naturaleza == "D":
                    balance.saldo_final = saldo_inicial_anio + saldo_deudor - saldo_acreedor
                else:
                    balance.saldo_final = saldo_inicial_anio - saldo_deudor + saldo_acreedor

                # Sumatoria de totales debito y crédito y saldos
                total_debe += balance.saldo_deudor
                total_haber += balance.saldo_acreedor
                if obj.naturaleza == "D":
                    total_saldo_ini_debe += balance.saldo_inicial
                    total_saldo_final_debe += balance.saldo_final
                else:
                    total_saldo_ini_haber += balance.saldo_inicial
                    total_saldo_final_haber += balance.saldo_final



                # Solo se presenta las cuentas que hayan tenido algun movimiento
                if (balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor) != (0, 0, 0):
                    lista_balance.append(balance)

        else:  # No acumulado
            for obj in PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo"):
                now = datetime.datetime.now()
                ## Saldo Inicial del año
                try:
                    saldo_inicial_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=obj, anio=now.year).saldo_inicial)
                except:
                    saldo_inicial_anio = 0.0

                ## Se valida si el mes que ingreso es mayor a enero para sumarizar el saldo inicial
                if mes > 1:
                    saldo_acum_debe = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                               fecha_asiento__lt=str(now.year)+"-0"+str(mes)+"-01",
                                                               fecha_asiento__year=now.year,
                                                               status=1,
                                                               dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])

                    saldo_acum_haber = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                               fecha_asiento__lt=str(now.year)+"-0"+str(mes)+"-01",
                                                               fecha_asiento__year=now.year,
                                                               status=1,
                                                               dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])

                    if obj.naturaleza == "D":
                        saldo_acum = saldo_acum_debe - saldo_acum_haber
                    else:
                        saldo_acum = saldo_acum_haber - saldo_acum_debe
                else:
                    saldo_acum = 0.0

                saldo_inicial = saldo_inicial_anio + saldo_acum

                saldo_deudor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                      fecha_asiento__month=mes,
                                                                                      status=1,
                                                                                      dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])
                saldo_acreedor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                        fecha_asiento__month=mes,
                                                                                        status=1,
                                                                                        dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])

                balance = BalanceComprobacion()
                balance.saldo_inicial = saldo_inicial
                balance.saldo_deudor = saldo_deudor
                balance.saldo_acreedor = saldo_acreedor
                balance.plan_cuenta = obj
                if balance.plan_cuenta.naturaleza == "D":
                    balance.saldo_final = saldo_inicial + saldo_deudor - saldo_acreedor
                else:
                    balance.saldo_final = saldo_inicial - saldo_deudor + saldo_acreedor

                # Sumatoria de totales debito y crédito y saldos
                total_debe += balance.saldo_deudor
                total_haber += balance.saldo_acreedor
                if obj.naturaleza == "D":
                    total_saldo_ini_debe += balance.saldo_inicial
                    total_saldo_final_debe += balance.saldo_final
                else:
                    total_saldo_ini_haber += balance.saldo_inicial
                    total_saldo_final_haber += balance.saldo_final

                # Solo se presenta las cuentas que hayan tenido algun movimiento
                if (balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor) != (0, 0, 0):
                    lista_balance.append(balance)
        total_saldo_ini = total_saldo_ini_debe - total_saldo_ini_haber
        total_saldo_final = total_saldo_final_debe - total_saldo_final_haber

    html = render_to_string('reporte_contabilidad/balance_comprobacion_pdf.html',
                            {'pagesize': 'A4',
                             "lista": lista_balance,
                             "total_debe": total_debe,
                             "total_haber": total_haber,
                             "total_saldo_ini": total_saldo_ini,
                             "total_saldo_final": total_saldo_final,
                             "request": request}, context_instance=RequestContext(request))
    return generar_pdf(html)


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_balance_comprobacion_excel2(request):
    """
    Vista que saca el reporte de balance de comprobación cuando no existe la
    tabla saldo_cuenta
    Función que retorna el excel del balance de comprobación
    :param request:
    :return:
    """
    total_debe = 0.0
    total_haber = 0.0
    total_saldo_ini = 0.0
    total_saldo_final = 0.0
    lista_balance = []
    lista_balance.append([u"", u"", u"", u"BALANCE DE COMPROBACIÓN"])
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        acum = buscador.getSelAll()
        mes = buscador.getMes()

        lista_balance.append([u"MES: ", get_mes(mes)])
        if acum:
            lista_balance.append([u"Acumulado:", u"SI"])
        else:
            lista_balance.append([u"Acumulado:", u"NO"])
        lista_balance.append([u""])
        lista_balance.append([u"Cuenta", u"Descripción", u"Saldo inicial",
                              u"Total Débito", u"Total Crédito", u"Saldo Final"])

        # Para los totales
        total_saldo_ini_debe = 0.0
        total_saldo_ini_haber = 0.0
        total_saldo_final_debe = 0.0
        total_saldo_final_haber = 0.0

        if acum:  # Acumulado
            for obj in PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo"):
                now = datetime.datetime.now()
                try:
                    saldo_inicial_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=obj, anio=now.year).saldo_inicial)
                except:
                    saldo_inicial_anio = 0.0

                ini_fin_mes = calendar.monthrange(now.year, mes)
                saldo_deudor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                      fecha_asiento__range=(str(now.year)+'-01-01',
                                                                                                            str(now.year)+"-"'0'+str(mes)+"-"+str(ini_fin_mes[1])),
                                                                                      status=1,
                                                                                      dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])
                saldo_acreedor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                        fecha_asiento__range=(str(now.year)+'-01-01',
                                                                                                              str(now.year)+"-"'0'+str(mes)+"-"+str(ini_fin_mes[1])),
                                                                                        status=1,
                                                                                        dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])

                balance = BalanceComprobacion()
                balance.saldo_inicial = saldo_inicial_anio
                balance.saldo_deudor = saldo_deudor
                balance.saldo_acreedor = saldo_acreedor
                balance.plan_cuenta = obj

                if balance.plan_cuenta.naturaleza == "D":
                    balance.saldo_final = saldo_inicial_anio + saldo_deudor - saldo_acreedor
                else:
                    balance.saldo_final = saldo_inicial_anio - saldo_deudor + saldo_acreedor


                # Sumatoria de totales debito y crédito y saldos
                total_debe += balance.saldo_deudor
                total_haber += balance.saldo_acreedor
                if obj.naturaleza == "D":
                    total_saldo_ini_debe += balance.saldo_inicial
                    total_saldo_final_debe += balance.saldo_final
                else:
                    total_saldo_ini_haber += balance.saldo_inicial
                    total_saldo_final_haber += balance.saldo_final

                # Solo se presenta las cuentas que hayan tenido algun movimiento
                if (balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor) != (0, 0, 0):
                    # Llenar la lista para el excel
                    lista_balance.append([balance.plan_cuenta.codigo, unicode(balance.plan_cuenta.descripcion),
                                          balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor,
                                          balance.saldo_final])

        else:  # No acumulado
            for obj in PlanCuenta.objects.filter(status=1, nivel=5).order_by("codigo"):
                now = datetime.datetime.now()
                ## Saldo Inicial del año
                try:
                    saldo_inicial_anio = convert_cero_none(SaldoCuenta.objects.get(plan_cuenta=obj, anio=now.year).saldo_inicial)
                except:
                    saldo_inicial_anio = 0.0

                ## Se valida si el mes que ingreso es mayor a enero para sumarizar el saldo inicial
                if mes > 1:
                    saldo_acum_debe = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                               fecha_asiento__lt=str(now.year)+"-0"+str(mes)+"-01",
                                                               fecha_asiento__year=now.year,
                                                               status=1,
                                                               dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])

                    saldo_acum_haber = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                               fecha_asiento__lt=str(now.year)+"-0"+str(mes)+"-01",
                                                               fecha_asiento__year=now.year,
                                                               status=1,
                                                               dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])

                    if obj.naturaleza == "D":
                        saldo_acum = saldo_acum_debe - saldo_acum_haber
                    else:
                        saldo_acum = saldo_acum_haber - saldo_acum_debe
                else:
                    saldo_acum = 0.0

                saldo_inicial = saldo_inicial_anio + saldo_acum

                saldo_deudor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                      fecha_asiento__month=mes,
                                                                                      status=1,
                                                                                      dbcr="D").aggregate(Sum('valor', field="valor"))["valor__sum"])
                saldo_acreedor = convert_cero_none(Detalle_Comp_Contable.objects.filter(plan_cuenta=obj,
                                                                                        fecha_asiento__month=mes,
                                                                                        status=1,
                                                                                        dbcr="H").aggregate(Sum('valor', field="valor"))["valor__sum"])


                balance = BalanceComprobacion()
                balance.saldo_inicial = saldo_inicial
                balance.saldo_deudor = saldo_deudor
                balance.saldo_acreedor = saldo_acreedor
                balance.plan_cuenta = obj
                if balance.plan_cuenta.naturaleza == "D":
                    balance.saldo_final = saldo_inicial + saldo_deudor - saldo_acreedor
                else:
                    balance.saldo_final = saldo_inicial - saldo_deudor + saldo_acreedor

                # Sumatoria de totales debito y crédito y saldos
                total_debe += balance.saldo_deudor
                total_haber += balance.saldo_acreedor
                if obj.naturaleza == "D":
                    total_saldo_ini_debe += balance.saldo_inicial
                    total_saldo_final_debe += balance.saldo_final
                else:
                    total_saldo_ini_haber += balance.saldo_inicial
                    total_saldo_final_haber += balance.saldo_final

                # Solo se presenta las cuentas que hayan tenido algun movimiento
                if (balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor) != (0, 0, 0):
                    lista_balance.append([balance.plan_cuenta.codigo, unicode(balance.plan_cuenta.descripcion),
                                          balance.saldo_inicial, balance.saldo_deudor, balance.saldo_acreedor,
                                          balance.saldo_final])

        total_saldo_ini = total_saldo_ini_debe - total_saldo_ini_haber
        total_saldo_final = total_saldo_final_debe - total_saldo_final_haber
        lista_balance.append([u"Totales:", u"", total_saldo_ini, total_debe,
                              total_haber, total_saldo_final])

    return ExcelResponse(lista_balance, u'Balance de Comprobación')


meses = (
    ("", ""),
    (1, "Enero"),
    (2, "Febrero"),
    (3, "Marzo"),
    (4, "Abril"),
    (5, "Mayo"),
    (6, "Junio"),
    (7, "Julio"),
    (8, "Agosto"),
    (9, "Septiembre"),
    (10, "Octubre"),
    (11, "Noviembre"),
    (12, "Diciembre")
)


class BuscadorSaldoCuentas(forms.Form):

    mes = forms.ChoiceField(choices=meses, label=u"Mes", required=True)
    anio=  forms.IntegerField(required=True, label=u'Año')
    acumulado = forms.BooleanField(required=False,label=u"Acumulado")

    def getMes(self):
        value = self.data["mes"]
        try:
            return int(value)
        except:
            return 0

    def getAcumulado(self):
        try:
            value = self.cleaned_data["acumulado"]
            return value
        except:
            return False

    def getAnio(self):
        try:
            value = self.cleaned_data["anio"]
            return value
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(BuscadorSaldoCuentas, self).__init__(*args, **kwargs)
        self.fields['anio'].widget.attrs['class'] = "txt-100pc numerico"
        self.fields['anio'].widget.attrs['maxlength'] = 4
        self.fields['mes'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['mes'].widget.attrs['data-live-search'] = True
        self.fields['mes'].widget.attrs['title'] = "Seleccione un Mes"
        self.fields['mes'].widget.attrs['data-style'] = "slc-primary"
        self.fields['mes'].widget.attrs['data-width'] = "100%"
        self.fields['mes'].widget.attrs['data-size'] = "auto"


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldos_cuentas(request):
    buscador = BuscadorSaldoCuentas(request.POST)
    if request.method == "POST":
        if buscador.is_valid():
            acumulado = buscador.getAcumulado()
            mes = buscador.getMes()
            anio = buscador.getAnio()
            saldos = SaldoCuenta.objects.filter(anio=anio).order_by('plan_cuenta__codigo')

            if len(saldos) > 0:
                lista = []
                for saldo in saldos:
                    if not acumulado:

                        if saldo.plan_cuenta.naturaleza == 'D':
                            total = redondeo(saldo.get_saldo_debe_mes(mes) - saldo.get_saldo_haber_mes(mes))
                            item = {'codigo': saldo.plan_cuenta.codigo,'descripcion':saldo.plan_cuenta.descripcion,
                                    'total': total}
                            lista.append(item)
                        else:
                            total = redondeo(saldo.get_saldo_haber_mes(mes) - saldo.get_saldo_debe_mes(mes))
                            item = {'codigo': saldo.plan_cuenta.codigo,'descripcion':saldo.plan_cuenta.descripcion,
                                    'total': total}
                            lista.append(item)
                    else:

                        if saldo.plan_cuenta.naturaleza == 'D':
                            total = redondeo(convert_cero_none(saldo.saldo_inicial) + (saldo.get_saldo_acumulado_debe_plan_cuentas(mes) - saldo.get_saldo_acumulado_haber_plan_cuentas(mes)))
                            item = {'codigo': saldo.plan_cuenta.codigo, 'descripcion':saldo.plan_cuenta.descripcion,
                                    'total': total}
                            lista.append(item)
                        else:
                            total = redondeo(convert_cero_none(saldo.saldo_inicial) + (saldo.get_saldo_acumulado_haber_plan_cuentas(mes) - saldo.get_saldo_acumulado_debe_plan_cuentas(mes)))
                            item = {'codigo': saldo.plan_cuenta.codigo, 'descripcion': saldo.plan_cuenta.descripcion,
                                    'total': total}
                            lista.append(item)

                datos = {"buscador": buscador, 'saldos': lista, 'acumulado': acumulado, 'mes': mes, 'anio': anio}

            else:
                messages.success(request, u"No se encontraron resultados")
                datos = {"buscador": buscador}
        else:

            datos = {"buscador": buscador}

            lista_errores = "Por favor verifique los siguientes campos: "
            for i in buscador.errors:
                lista_errores = lista_errores +(unicode(i)) + ", "
            if buscador.errors:
                messages.error(request, unicode(lista_errores[0:-2]))
            for i in buscador:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
    else:
        datos = {"buscador": buscador}

    return render_to_response('reporte_contabilidad/reporte_saldos_de_cuentas.html', datos,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldos_cuentas_pdf(request):

    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorSaldoCuentas(request.POST)
    if request.method == "POST":
        if buscador.is_valid():
            acumulado = buscador.getAcumulado()
            mes = buscador.getMes()
            anio = buscador.getAnio()
            acu = 'No'
            if acumulado:
                acu = 'Si'

            saldos = SaldoCuenta.objects.filter(anio=anio).order_by('plan_cuenta__codigo')

            if len(saldos) > 0:
                lista = []
                for saldo in saldos:
                    if not acumulado:

                        if saldo.plan_cuenta.naturaleza == 'D':
                            total = redondeo(saldo.get_saldo_debe_mes(mes) - saldo.get_saldo_haber_mes(mes))
                            item = {'codigo': saldo.plan_cuenta.codigo,'descripcion':saldo.plan_cuenta.descripcion,'total':total}
                            lista.append(item)
                        else:
                            total = redondeo(saldo.get_saldo_haber_mes(mes) - saldo.get_saldo_debe_mes(mes))
                            item = {'codigo': saldo.plan_cuenta.codigo,'descripcion':saldo.plan_cuenta.descripcion,'total':total}
                            lista.append(item)
                    else:

                        if saldo.plan_cuenta.naturaleza == 'D':
                            total = redondeo(convert_cero_none(saldo.saldo_inicial) + (saldo.get_saldo_acumulado_debe_plan_cuentas(mes) - saldo.get_saldo_acumulado_haber_plan_cuentas(mes)))
                            item = {'codigo': saldo.plan_cuenta.codigo,'descripcion':saldo.plan_cuenta.descripcion,'total':total}
                            lista.append(item)
                        else:
                            total = redondeo(convert_cero_none(saldo.saldo_inicial) + (saldo.get_saldo_acumulado_haber_plan_cuentas(mes) - saldo.get_saldo_acumulado_debe_plan_cuentas(mes)))
                            item = {'codigo': saldo.plan_cuenta.codigo,'descripcion':saldo.plan_cuenta.descripcion,'total':total}
                            lista.append(item)


                datos = {
                    "buscador": buscador,
                    'saldos':lista,
                    'acumulado':acumulado,
                    'mes':mes,
                    'anio':anio,
                    'empresa':empresa,
                    'usuario':usuario,
                    'acu':acu,
                    'mes':get_mes(mes),
                    'anio':anio
                }
            else:
                messages.success(request,u"No se encontraron resultados")
                datos = {
                    "buscador": buscador

                }
                return render_to_response('reporte_contabilidad/reporte_saldos_de_cuentas.html',datos,context_instance=RequestContext(request))


        else:
            messages.error(request,u"Por favor ingrese el Mes y el Año, son campos requeridos (*).")
            datos = {
                "buscador": buscador
            }
            return render_to_response('reporte_contabilidad/reporte_saldos_de_cuentas.html',datos,context_instance=RequestContext(request))
    else:

        datos = {
            "buscador": buscador
        }
        return render_to_response('reporte_contabilidad/reporte_saldos_de_cuentas.html',datos,context_instance=RequestContext(request))

    html = render_to_string('reporte_contabilidad/reporte_saldo_de_cuentas_pdf.html', datos,
                            context_instance=RequestContext(request))
    nombre = "Rept_saldo_cuentas "+str(get_mes(mes))+"-"+str(anio)
    return generar_pdf_nombre(html,nombre)
    #return generar_pdf(html)


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldos_cuentas_excel(request):

    lista = []
    buscador = BuscadorSaldoCuentas(request.POST)
    if buscador.is_valid():
        acumulado = buscador.getAcumulado()
        mes = buscador.getMes()
        anio = buscador.getAnio()
        acu = 'No'

        if acumulado:
            acu = 'Si'

        lista.append(['', u'Reporte Saldos de Cuentas'+TITULO,''])
        lista.append([''])
        lista.append([u'Mes: ' + get_mes(mes)+SUBTITULO, u'Año: '+str(anio)+SUBTITULO,u'Acumulado: '+acu+SUBTITULO])
        lista.append([''])

        cabecera = [u'Código de Cuenta'+COLUMNA,u'Descripción de Cuenta'+COLUMNA,'Total'+COLUMNA]
        lista.append(cabecera)

        saldos = SaldoCuenta.objects.filter(anio=anio).order_by('plan_cuenta__codigo')

        for saldo in saldos:
            if not acumulado:

                if saldo.plan_cuenta.naturaleza == 'D':
                    total = redondeo(saldo.get_saldo_debe_mes(mes) - saldo.get_saldo_haber_mes(mes))
                    item = [str(saldo.plan_cuenta.codigo),unicode(saldo.plan_cuenta.descripcion),total]
                    lista.append(item)
                else:
                    total = redondeo(saldo.get_saldo_haber_mes(mes) - saldo.get_saldo_debe_mes(mes))
                    item = [str(saldo.plan_cuenta.codigo),unicode(saldo.plan_cuenta.descripcion),total]
                    lista.append(item)
            else:

                if saldo.plan_cuenta.naturaleza == 'D':
                    total = redondeo(convert_cero_none(saldo.saldo_inicial) + (saldo.get_saldo_acumulado_debe_plan_cuentas(mes) - saldo.get_saldo_acumulado_haber_plan_cuentas(mes)))
                    item = [str(saldo.plan_cuenta.codigo),unicode(saldo.plan_cuenta.descripcion),total]
                    lista.append(item)
                else:
                    total = redondeo(convert_cero_none(saldo.saldo_inicial) + (saldo.get_saldo_acumulado_haber_plan_cuentas(mes) - saldo.get_saldo_acumulado_debe_plan_cuentas(mes)))
                    item = [str(saldo.plan_cuenta.codigo),unicode(saldo.plan_cuenta.descripcion),total]
                    lista.append(item)
        nombre = "Rept_saldo_cuentas "+str(get_mes(mes))+"-"+str(anio)
        return ExcelResponse(lista, nombre)
    else:
        datos = {
                "buscador": buscador,
                "mensaje":'Por favor ingrese el Mes y el Año, son campos requeridos (*).'
            }
        return render_to_response('reporte_contabilidad/reporte_saldos_de_cuentas.html',datos,context_instance=RequestContext(request))


class BuscadorCodigoFormularioSRI(forms.Form):


    codigo= forms.ChoiceField(choices=[], label=u"Código", required=True)
    fecha = forms.DateField(required=True,label=u"Fecha")


    def getFecha(self):
        try:
            value = self.data["fecha"]
            x = str(value).split("-")
            return datetime.datetime(int(x[0]), int(x[1]), int(x[2]))
        except:
            return datetime.datetime.now()

    def getCodigo(self):
        try:
            value = self.data["codigo"]
            return Tipos_Retencion.objects.get(id=value)
        except:
            return None

    def __init__(self, *args, **kwargs):
        super(BuscadorCodigoFormularioSRI, self).__init__(*args, **kwargs)
        self.fields['fecha'].widget.attrs['class'] = "calendario-gris input-calendario"
        self.fields['fecha'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['codigo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['codigo'].widget.attrs['data-live-search'] = True
        self.fields['codigo'].widget.attrs['title'] = u"Seleccione un Código"
        self.fields['codigo'].choices = [("", "")] +  [(1,'103'),(2,'104')]
        self.fields['codigo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['codigo'].widget.attrs['data-width'] = "100%"
        self.fields['codigo'].widget.attrs['data-size'] = "auto"


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_codigo_formulario_sri(request):

    buscador = BuscadorCodigoFormularioSRI(request.POST)
    if request.method == "POST":
        if buscador.is_valid():

            id_codigo = buscador.getCodigo().id
            fecha = buscador.getFecha().strftime("%Y-%m-%d")
            codigos = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion__status=1,vigencia_retencion__fecha_inicio__lte=fecha,vigencia_retencion__fecha_final__gte=fecha).filter(status=1,vigencia_retencion__tipo_codigo_sri__id=id_codigo).order_by('codigo')

            if len(codigos) > 0:
                datos = {
                    "buscador": buscador,
                    "codigos":codigos
                }
            else:
                 datos = {
                    "buscador": buscador,
                    "mensaje":u'No se encontraron resuldos para su busqueda.'
                }

        else:
            datos = {
                "buscador": buscador,
                "mensaje":u'Por favor ingrese el Código y la Fecha, son campos requeridos (*).'
            }
    else:

        datos = {
            "buscador": buscador
        }

    return render_to_response('reporte_contabilidad/reporte_codigo_formulario_sri.html',datos,context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_codigo_formulario_sri_pdf(request):

    buscador = BuscadorCodigoFormularioSRI(request.POST)
    if request.method == "POST":
        if buscador.is_valid():

            id_codigo = buscador.getCodigo().id
            fecha = buscador.getFecha().strftime("%Y-%m-%d")
            codigos = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion__status=1,vigencia_retencion__fecha_inicio__lte=fecha,vigencia_retencion__fecha_final__gte=fecha).filter(status=1,vigencia_retencion__tipo_codigo_sri__id=id_codigo).order_by('codigo')
            empresa = Empresa.objects.filter(status=1)[0]
            usuario = request.user.first_name+' '+request.user.last_name

            if len(codigos) > 0:
                datos = {
                    "buscador": buscador,
                    "codigos":codigos,
                    "codigo":buscador.getCodigo().descripcion,
                    "fecha":buscador.getFecha().strftime("%d-%m-%Y"),
                    'empresa':empresa,
                    'usuario':usuario

                }
            else:
                datos = {
                    "buscador": buscador,
                    "mensaje":u'No se encontraron resuldos para su busqueda.'
                }
                return render_to_response('reporte_contabilidad/reporte_codigo_formulario_sri.html',datos,context_instance=RequestContext(request))


        else:
            datos = {
                "buscador": buscador,
                "mensaje":u'Por favor ingrese el Código y la Fecha, son campos requeridos (*).'
            }
            return render_to_response('reporte_contabilidad/reporte_codigo_formulario_sri.html',datos,context_instance=RequestContext(request))
    else:

        datos = {
            "buscador": buscador
        }
        return render_to_response('reporte_contabilidad/reporte_codigo_formulario_sri.html',datos,context_instance=RequestContext(request))

    html = render_to_string('reporte_contabilidad/reporte_codigo_formulario_sri_pdf.html', datos,
                            context_instance=RequestContext(request))
    return generar_pdf(html)


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_codigo_formulario_sri_excel(request):
    lista = []
    print("dentro para impresion")
    buscador = BuscadorCodigoFormularioSRI(request.POST)
    if request.method == "POST":
        print("dentro para impresion siendo post")
        if buscador.is_valid():

            id_codigo = buscador.getCodigo().id
            fecha = buscador.getFecha().strftime("%Y-%m-%d")
            codigos = Codigo_SRI.objects.filter(status=1).filter(vigencia_retencion__status=1,vigencia_retencion__fecha_inicio__lte=fecha,vigencia_retencion__fecha_final__gte=fecha).filter(status=1,vigencia_retencion__tipo_codigo_sri__id=id_codigo).order_by('codigo')

            if len(codigos) > 0:
                print("dentro para impresion")
                lista.append(['',u'Reporte Código Formulario SRI Código '+ buscador.getCodigo().codigo+TITULO,''])
                lista.append([''])
                lista.append([u'Código: '+buscador.getCodigo().codigo+SUBTITULO,u'Descripción: '+buscador.getCodigo().descripcion+SUBTITULO,u'Fecha: '+str(fecha)+SUBTITULO])
                lista.append([''])

                cabecera = [u'Código de Cuenta'+COLUMNA,u'Descripción de Cuenta'+COLUMNA,'Porcentaje (%)'+COLUMNA]
                lista.append(cabecera)

                for obj in codigos:
                    porcentaje = 0.00
                    if obj.porcentaje:
                        porcentaje = redondeo(obj.porcentaje)

                    item = [obj.codigo, obj.descripcion, porcentaje]
                    lista.append(item)

                return ExcelResponse(lista, u'Reporte Código Formulario SRI - '+buscador.getCodigo().codigo)

            else:
                 datos = {
                    "buscador": buscador,
                    "mensaje":u'No se encontraron resuldos para su busqueda.'
                }

        else:
            print("dentro para impresion NO siendo post VVVVVVV")
            datos = {
                "buscador": buscador,
                "mensaje":u'Por favor ingrese el Código y la Fecha, son campos requeridos (*).'
            }
    else:
        print("dentro para impresion NO siendo post")
        datos = {
            "buscador": buscador
        }

    return render_to_response('reporte_contabilidad/reporte_codigo_formulario_sri.html',datos,context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
def reporte_verificacion_modulo_contabilidad(request):
    '''
    Vista para presentar el reporte de verificación
    :param request:
    :return:
    '''
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes()
    r = None
    c = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        match = resolve(request.path)
        permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))

        if buscador.is_valid():
            mes = buscador.getMes()
            anio = buscador.getAnio()
            if mes > 0 and anio > 1980:
                # Path to resource rest service:
                url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports' + empresa.path_jasper + '/'
                # Report to process:
                report = permiso.nombre_reporte + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)
                # Params
                params = {'mes': mes, 'anio': anio}

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reporte_contabilidad/reporte_comprobantes_verificacion_modulo_contabilidad.html',
                              {"buscador": buscador, "objetos": r, "c": c}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_proyectado_vs_real(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                report = get_nombre_reporte('CONT_PROYECTADO_VS_REAL') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reporte_contabilidad/reporte_proyectado_vs_real.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))

#proyectado vs real pero en excel 28 marzo 2016

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_proyectado_vs_real_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "GET":
        print'el rpt proyectado vs real de contab excel 28 marzo 2016'
        buscador = BuscadorReportes_fecha_inicial_final(request.GET)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                report = get_nombre_reporte('CONT_PROYECTADO_VS_REAL') + '.xls'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    response = HttpResponse(r.content, content_type='application/excel')
    response['Content-Disposition'] = 'attachment;filename="rpt_proyectado_vs_real.xls"'
    return response

#proyectado vs real pero en PDF 28 marzo 2016

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_proyectado_vs_real_pdf(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "GET":
        print'el rpt proyectado vs real de contab pdf 28 marzo 2016'
        buscador = BuscadorReportes_fecha_inicial_final(request.GET)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                report = get_nombre_reporte('CONT_PROYECTADO_VS_REAL') + '.pdf'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    response = HttpResponse(r.content, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="rpt_cont_proy_real.pdf"'
    return response
    '''
    return render_to_response('reporte_contabilidad/reporte_proyectado_vs_real.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))
    '''


#reporte puesto el 20 d enero
@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_plan_cuenta(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    urljasper = get_url_jasper()
    url = urljasper + empresa.path_jasper + '/'
    report = get_nombre_reporte('CONTABILIDAD_PLAN_CUENTA') + '.html'
    # Authorisation credentials:
    auth = (empresa.user_jasper, empresa.pass_jasper)
    s = requests.Session()
    r = s.get(url=url+report, auth=auth)

    match = resolve(request.path)
    print 'el path es', str(request.path)
    #url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports' + empresa.path_jasper + '/'
    print str(url),"url del sistema zzzzzzzzzzzzz"
    #permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))

    return render_to_response('reporte_contabilidad/reporte_plan_cuenta.html',
                              {"objetos": r
                               }, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_plan_cuenta_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    urljasper = get_url_jasper()
    url = urljasper + empresa.path_jasper + '/'
    report = get_nombre_reporte('CONTABILIDAD_PLAN_CUENTA') + '.xls'
    # Authorisation credentials:
    auth = (empresa.user_jasper, empresa.pass_jasper)
    s = requests.Session()
    r = s.get(url=url+report, auth=auth)
    response = HttpResponse(r.content, content_type='application/excel')
    response['Content-Disposition'] = 'attachment;filename="Plan_cuenta.xls"'
    return response

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_plan_cuenta_pdf(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    urljasper = get_url_jasper()
    url = urljasper + empresa.path_jasper + '/'
    report = get_nombre_reporte('CONTABILIDAD_PLAN_CUENTA') + '.pdf'
    # Authorisation credentials:
    auth = (empresa.user_jasper, empresa.pass_jasper)
    s = requests.Session()
    r = s.get(url=url+report, auth=auth)
    response = HttpResponse(r.content, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="Rpt_plancuentas.pdf"'
    return response


#implementado el 30 de marzo del 2016 esto es reporte de comprbantes por tipo

@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_comprobantes_x_tipo(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="cuentas")
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        cuentas = cuentas_formset(request.POST, prefix="cuentas")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""
        tipo = ""
        urljasper = get_url_jasper()
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:


            report = get_nombre_reporte('CONT_COMP_X_TIPO') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0

            if buscador.getITipoComprobante() is not None:
                print str(buscador.getITipoComprobante().descripcion)+'el cbox del tipo de cprobante :o'+str(buscador.getITipoComprobante().id)
                tipo = str(buscador.getITipoComprobante().id)
                if int(tipo)== 6:
                    print "el tipo es seis es un asst"
                    report = get_nombre_reporte('comprobantes_ast') + '.html'
            else:
                print 'no ha seleccionado un objeto correcto en el cbox'
            # Params
            params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'tipo': tipo
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print str(r.status_code)+'el statuscode del reoprte de tipos comprobantes'
            if r.status_code != 200:
                r = None
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_contabilidad/comprobantes_x_tipo.html',
                              {"buscador": buscador,
                               "lista": r,
                               "cuentas": cuentas
                               }, context_instance=RequestContext(request))



#lo mismo que lo de arriba pero en excel 1 de abril

@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_comprobantes_x_tipo_excel(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="cuentas")
    r = None
    nombre = ""
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        cuentas = cuentas_formset(request.POST, prefix="cuentas")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""
        tipo = ""

        urljasper = get_url_jasper()
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:
            report = get_nombre_reporte('CONT_COMP_X_TIPO') + '.xls'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0

            if buscador.getITipoComprobante() is not None:
                print str(buscador.getITipoComprobante().descripcion)+'el cbox del tipo de cprobante :o'+str(buscador.getITipoComprobante().id)
                tipo = str(buscador.getITipoComprobante().id)
                nombre = str(buscador.getITipoComprobante().descripcion)
                if int(tipo)== 6:
                    print "el tipo es seis es un asst"
                    report = get_nombre_reporte('comprobantes_ast') + '.xls'
            else:
                print 'no ha seleccionado un objeto correcto en el cbox'
            # Params
            params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'tipo': tipo
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print str(r.status_code)+'el statuscode del reoprte de tipos comprobantes'
            if r.status_code != 200:
                r = None
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    else:
        print 'en el metoo de ex cel no es metodo post :o'
    #r = s.get(url=url+report, auth=auth)
    #nombre = "rpt_x_tipo_"+nombre+".xls"
    response = HttpResponse(r.content, content_type='application/excel')
    #response['Content-Disposition'] = 'attachment;filename="rpt_x_tipo_.xls"'
    nombre = "rpt_trans_x_tipo_"+str(nombre)+"_"+str(fecha_ini)+"_"+str(fecha_fin)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    return response

#lo mismo que lo de arriba pero en excel 1 de abril

@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_comprobantes_x_tipo_pdf(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="cuentas")
    r = None
    nombre = ""
    if request.method == "POST":
        print 'el reporte contable x tipo en pdf'
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        cuentas = cuentas_formset(request.POST, prefix="cuentas")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""
        tipo = ""

        urljasper = get_url_jasper()
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:
            report = get_nombre_reporte('CONT_COMP_X_TIPO') + '.pdf'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0

            if buscador.getITipoComprobante() is not None:
                print str(buscador.getITipoComprobante().descripcion)+'el cbox del tipo de cprobante :o'+str(buscador.getITipoComprobante().id)
                tipo = str(buscador.getITipoComprobante().id)
                nombre = str(buscador.getITipoComprobante().descripcion)
                if int(tipo)== 6:
                    print "el tipo es seis es un asst"
                    report = get_nombre_reporte('comprobantes_ast') + '.pdf'
            else:
                print 'no ha seleccionado un objeto correcto en el cbox'
            # Params
            params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'tipo': tipo
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print str(r.status_code)+'el statuscode del reoprte de tipos comprobantes'
            if r.status_code != 200:
                r = None
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    else:
        print 'en el metoo de pdf no es metodo post :o'
    #r = s.get(url=url+report, auth=auth)
    #nombre = "rpt_x_tipo_"+nombre+".xls"
    response = HttpResponse(r.content, content_type='application/pdf')
    #response['Content-Disposition'] = 'attachment;filename="rpt_x_tipo_.xls"'
    nombre = "rpt_trans_x_tipo_"+str(nombre)+"_"+str(fecha_ini)+"_"+str(fecha_fin)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".pdf"'
    return response