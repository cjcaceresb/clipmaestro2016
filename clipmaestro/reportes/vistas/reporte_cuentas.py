#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from contabilidad.formularios.ComprasForm import *
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import transaction
from django.forms.util import ErrorList
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.core.paginator import *
from django import template
from administracion.models import *
from librerias.funciones.permisos import *
from excel_response import ExcelResponse

@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuentas_lista(request):
    q = request.GET.get("q", "")

    if "-" in q and q != "":
        codigo_cuenta = q.split("-")[0].replace(" ","")
        nombre_cuenta = q.split("-")[1].replace(" ","")
    else:
        nombre_cuenta = q
        codigo_cuenta = q
    cuentas = PlanCuenta.objects.filter(status=1,nivel=5).filter(Q(codigo__istartswith=codigo_cuenta) | Q(descripcion__istartswith=nombre_cuenta))

    paginacion = Paginator(cuentas, 10)
    numero_pagina = request.GET.get("page")

    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number
    return render_to_response('reportes_cuentas/reporte_cuentas_lista.html',{"objetos":lista_cabecera, "total_paginas": total_paginas, "numero" : numero, "request": request}, context_instance=RequestContext(request))


@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def buscar_cuenta(request):
    respuesta = []
    q = request.GET.get("term", "")
    try:
        cuentas = PlanCuenta.objects.filter(status=1).filter(Q(codigo__icontains=q) | Q(descripcion__icontains=q))[0:5]
        for p in cuentas:
            respuesta.append({"value": p.codigo+" - "+p.descripcion, "key": p.codigo+" - "+p.descripcion})
    except:
        respuesta.append({"value": "error base", "key": "error"})
    resultado = json.dumps(respuesta)
    return HttpResponse(resultado, mimetype='application/json')


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuenta_fechas(request,id):
    fecha_inicio = request.GET.get("fecha_inicio","")
    fecha_fin = request.GET.get("fecha_fin","")

    if fecha_inicio == "":
        fecha_inicio = "0001-01-01"

    else:
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)


    if fecha_fin == "":
        fecha_fin = datetime.datetime.now()
    else:
        year = int(str(fecha_fin).split("-")[0])
        month = int(str(fecha_fin).split("-")[1])
        day = int(str(fecha_fin).split("-")[2])
        fecha_fin = datetime.date(year,month,day)



    cuentas = Detalle_Comp_Contable.objects.filter(plan_cuenta_id=id,status=1).filter(fecha_creacion__range=(fecha_inicio,fecha_fin)).order_by('fecha_creacion')


    try:
        cuenta_id = PlanCuenta.objects.get(id=id)
    except PlanCuenta.DoesNotExist:
        cuenta_id = None

    total_registros= len(cuentas)
    total_cuenta = 0.0

    for cuenta in cuentas:

        total_cuenta += float(cuenta.valor)

    #Agregar nueva columna
    #cuenta_tmp = cuentatmp

    cuenta_tmp = {}
    lista = []
    total = 0.0
    if cuentas:
        for obj in cuentas:

            if obj.dbcr == "D":
                total += float(obj.valor)
                cuenta_tmp = {"dbcr":obj.dbcr,"fecha_creacion":obj.fecha_creacion,"concepto":obj.cabecera_contable.concepto_comprobante,"valor":obj.valor,"total":float(total)}

            if obj.dbcr == "H":
                total -= float(obj.valor)
                cuenta_tmp = {"dbcr":obj.dbcr,"fecha_creacion":obj.fecha_creacion,"concepto":obj.cabecera_contable.concepto_comprobante,"valor":obj.valor,"total":float(total)}
            lista.append(cuenta_tmp)


    paginacion = Paginator(lista, 10)
    numero_pagina = request.GET.get("page")

    try:
        lista_cabecera = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        lista_cabecera = paginacion.page(1)
    except EmptyPage:
        lista_cabecera = paginacion.page(paginacion._num_pages)

    total_paginas = paginacion._num_pages
    numero = lista_cabecera.number

    return render_to_response('reportes_cuentas/reporte_cuenta_fechas.html', {"objetos": lista_cabecera,"cuenta_id":cuenta_id,"fecha_inicio":fecha_inicio,"fecha_fin":fecha_fin,"request":request,"total_paginas": total_paginas, "numero" : numero,"total_cuenta":total_cuenta,"total_registros":total_registros,"id_cuenta":id}, context_instance=RequestContext(request))


@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuenta_fechas_excel(request):
    id = request.GET.get("id_cuenta", "")
    fecha_inicio = request.GET.get("fecha_inicio","")
    fecha_fin = request.GET.get("fecha_fin","")

    if fecha_inicio == "":
        fecha_inicio = "0001-01-01"

    else:
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)


    if fecha_fin == "":
        fecha_fin = datetime.datetime.now()
    else:
        year = int(str(fecha_fin).split("-")[0])
        month = int(str(fecha_fin).split("-")[1])
        day = int(str(fecha_fin).split("-")[2])
        fecha_fin = datetime.date(year,month,day)

    cuentas = Detalle_Comp_Contable.objects.filter(plan_cuenta_id=id,status=1).filter(fecha_creacion__range=(fecha_inicio,fecha_fin)).order_by('fecha_creacion')
    datos = []
    datos.append("Fecha")
    datos.append("Glosa")
    datos.append("Debe")
    datos.append("Haber")
    datos.append("Total")

    total = 0.0
    total_debe=0.0
    total_haber=0.0

    data1 = []
    data1.append(datos)
    for obj in cuentas:

        datos = []
        datos.append(obj.fecha_creacion.strftime('%d-%m-%Y'))
        datos.append(obj.cabecera_contable.concepto_comprobante)

        if obj.dbcr == "D":
           datos.append(("%.2f"%float(obj.valor)))
           total += float(obj.valor)
           total_debe += float(obj.valor)
        else:
            datos.append(("%.2f"%float(0.00)))

        if obj.dbcr == "H":
           datos.append(("%.2f"%float(obj.valor)))
           total -= float(obj.valor)
           total_haber += float(obj.valor)
        else:
            datos.append(("%.2f"%float(0.00)))

        datos.append(("%.2f"%float(total)))

        data1.append(datos)

    return ExcelResponse(data1, 'Cuentas')


def generar_cuenta_fechas_pdf(template_src, context_dict):
    template = get_template(template_src)
    context = Context(context_dict)
    html = template.render(context)
    result = StringIO.StringIO()

    pdf = pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), result)
    if not pdf.err:
        return HttpResponse(result.getvalue(), mimetype='application/pdf')
    return HttpResponse('Tenemos algunos errores<pre>%s</pre>' % escape(html))


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuenta_fechas_pdf(request):
    id = request.GET.get("id_cuenta")
    fecha_inicio = request.GET.get("fecha_inicio","0001-01-01")
    fecha_fin = request.GET.get("fecha_fin",datetime.datetime.now())

    if fecha_inicio == "":
        fecha_inicio = "0001-01-01"
    else:
        year = int(str(fecha_inicio).split("-")[0])
        month = int(str(fecha_inicio).split("-")[1])
        day = int(str(fecha_inicio).split("-")[2])
        fecha_inicio = datetime.date(year,month,day)

    if fecha_fin == "":
        fecha_fin = datetime.datetime.now()
    else:
        year = int(str(fecha_fin).split("-")[0])
        month = int(str(fecha_fin).split("-")[1])
        day = int(str(fecha_fin).split("-")[2])

        fecha_fin = datetime.date(year,month,day)

    cuentas = Detalle_Comp_Contable.objects.filter(plan_cuenta_id=id,status=1).filter(fecha_creacion__range=(fecha_inicio,fecha_fin)).order_by('fecha_creacion')

    try:
        cuenta_id = PlanCuenta.objects.get(id=id)
    except PlanCuenta.DoesNotExist:
        cuenta_id = None


    fecha_actual = datetime.datetime.now()
    total_registros= len(cuentas)
    total_cuenta = 0.0

    for cuenta in cuentas:
        total_cuenta += float(cuenta.valor)


    return generar_cuenta_fechas_pdf('reportes_cuentas/reporte_cuenta_fechas_pdf.html',
                              {'pagesize': 'A4',
                               "cuentas": cuentas,
                               "cuenta_id":cuenta_id,
                               "request":request,
                               "fecha_actual":fecha_actual,
                               "fecha_inicio":fecha_inicio,
                               "fecha_fin":fecha_fin,
                               "total_cuenta":total_cuenta,
                               "total_registros":total_registros})