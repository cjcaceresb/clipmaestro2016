#! /usr/bin/python
# -*- coding: UTF-8-*-
from administracion.models import Empresa

__author__ = 'Clip Maestro'

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from librerias.funciones.excel_response_mod import ExcelResponse
from reportes.formularios.reportes_form import *

from django.db.models import Sum
import operator
from django.db.models import F
from django.forms.formsets import formset_factory
from librerias.funciones.funciones_vistas import generar_pdf
from librerias.funciones.excel_response_mod import *
from librerias.funciones.funciones_vistas import *
from librerias.funciones.permisos import *


@csrf_exempt
@login_required()
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_listado_contratos_fechas(request):
    """
    Reporte de contratos por intervalo de fechas
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01"})
    contratos = []

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)

        if buscador.is_valid():
            fecha_inicio = buscador.getFechaIni()
            if fecha_inicio is not None:

                contratos = ContratoCabecera.objects.filter(status=1,
                                                            fecha_reg__gte=fecha_inicio).order_by("fecha_reg",
                                                                                                  "num_cont")

    return render_to_response("reporte_contratos/reporte_listado_contratos_fechas.html",{"buscador": buscador,
                                                                                         "contratos": contratos},
                              context_instance=RequestContext(request))



@csrf_exempt
@login_required()
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_listado_contratos_fechas_excel(request):
    """
    Reporte de contratos por intervalo de fechas
    :param request:
    :return:
    """
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01"})
    contratos = []
    lista = []
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)

        if buscador.is_valid():
            fecha_inicio = buscador.getFechaIni()
            if fecha_inicio is not None:
                contratos = ContratoCabecera.objects.filter(status=1,
                                                            fecha_reg__gte=fecha_inicio).order_by("fecha_reg","num_cont")
                if contratos:

                    lista.append([u"", u"", u"", u"", u"Reporte  Listado de Contratos por Fechas"+TITULO])
                    lista.append([u""])
                    lista.append([u"", u"", u"Fecha Inicial: "+SUBTITULO, fecha_inicio])
                    lista.append([u""])
                    lista.append([u"Fecha"+COLUMNA,u"Contrato"+COLUMNA,u"Cod. Cliente"+COLUMNA,u"Nombre"+COLUMNA,u"Fecha Entrega"+COLUMNA,u"Fecha Cierre"+COLUMNA,u"Valor"+COLUMNA,u"Iva"+COLUMNA,u"Total"+COLUMNA,u"Costo"+COLUMNA,u"N° Doc."+COLUMNA])

                    for contrato in contratos:
                        row = []
                        total = 0.00
                        valor = redondeo(convert_cero_none(contrato.base0) + convert_cero_none(contrato.baseiva))
                        total = redondeo(convert_cero_none(contrato.base0) + convert_cero_none(contrato.baseiva) + convert_cero_none(contrato.monto_iva))
                        row = [str(contrato.fecha_reg),str(contrato.num_cont),str(contrato.cliente.ruc),unicode(contrato.cliente.razon_social),contrato.fecha_entrega,contrato.fecha_cierre,valor,contrato.monto_iva,total,contrato.getTotalDetallesCabecera(),str(contrato.getNumerosDocumentosVentasContrato())]
                        lista.append(row)

                    return ExcelResponse(lista, 'Rpt_Lista_Contratos_desde_'+str(fecha_inicio))


    return render_to_response("reporte_contratos/reporte_listado_contratos_fechas.html",{"buscador": buscador,"contratos": contratos},context_instance=RequestContext(request))


@csrf_exempt
@login_required()
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_listado_contratos_fechas_pdf(request):
    """
    Reporte de contratos por intervalo de fechas
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0:1]
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01"})
    contratos = []
    fecha =""

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)

        if buscador.is_valid():
            fecha_inicio = buscador.getFechaIni()
            if fecha_inicio is not None:
                fecha = fecha_inicio
                contratos = ContratoCabecera.objects.filter(status=1,
                                                            fecha_reg__gte=fecha_inicio).order_by("fecha_reg","num_cont")
                datos = {"buscador": buscador,
                         "contratos": contratos,
                         'empresa':empresa,
                         'usuario':request.user.username,
                         'fecha':fecha
                        }

                html = render_to_string('reporte_contratos/reporte_listado_contratos_fechas_pdf.html', datos,
                                        context_instance=RequestContext(request))
                nombre = 'Rpt_Lista_Contratos_desde_'+str(fecha_inicio)
                return generar_pdf_nombre(html, nombre)

    datos = {"buscador": buscador,
             "contratos": contratos,
             'empresa':empresa,
             'usuario':request.user.username,
             'fecha':fecha
            }

    return render_to_response("reporte_contratos/reporte_listado_contratos_fechas.html",{"buscador": buscador,"contratos": contratos},context_instance=RequestContext(request))

@csrf_exempt
@login_required()
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_contrato_detalle(request,id):
    """
    Reporte contrato detalle
    :param request:
    :return:
    """
    contrato = None
    detalles = None
    try:
        contrato = ContratoCabecera.objects.get(status=1,id=id)
        detalles = ContratoDetalle.objects.filter(status=1,contrato_cabecera_id=id)
    except:
        detalles = None
        contrato = None


    datos = {
        'detalles':detalles,
        'contrato':contrato,
        'id':id
    }
    return render_to_response("reporte_contratos/reporte_contrato_detalle.html",datos,
                              context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def print_contrato_detalle(request,id):
    """
    Reporte contrato detalle
    :param request:
    :return:
    """
    contrato = None
    detalles = None
    try:
        contrato = ContratoCabecera.objects.get(status=1,id=id)
        detalles = ContratoDetalle.objects.filter(status=1,contrato_cabecera_id=id)

        f = open('workfile.txt', 'w')
        f.write('0123456789abcdef')
        f.seek(5)     # Go to the 6th byte in the file
        f.read(1)
        f.seek(-3,'test') # Go to the 3rd byte before the end

        f.read(1)



    except:
        detalles = None
        contrato = None


    datos = {
        'detalles':detalles,
        'contrato':contrato,
        'id':id
    }
    return render_to_response("reporte_contratos/print_contrato_detalle.html",datos,
                              context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_contrato_detalle_pdf(request,id):
    """
    Reporte contrato detalle pdf
    :param request:
    :return:
    """
    contrato = None
    detalles = None
    try:
        contrato = ContratoCabecera.objects.get(status=1,id=id)
        detalles = ContratoDetalle.objects.filter(status=1,contrato_cabecera_id=id)
    except:
        detalles = None
        contrato = None

    empresa = Empresa.objects.filter(status=1)[0]
    datos = {
        'detalles':detalles,
        'contrato':contrato,
        'id':id,
        'empresa':empresa,
        'usuario':request.user.username
    }
    html = render_to_string('reporte_contratos/reporte_contrato_detalle_pdf.html', datos,
                            context_instance=RequestContext(request))
    return generar_pdf_nombre(html,'Detalle_contrato_'+str(contrato.num_cont) )


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_contrato_detalle_excel(request,id):
    """
    Reporte contrato detalle pdf
    :param request:
    :return:
    """
    lista = []
    row = []

    lista.append([u"", u"Reporte  Contrato Detalle"+TITULO])
    lista.append([u""])

    contrato = None
    detalles = None
    try:
        contrato = ContratoCabecera.objects.get(status=1,id=id)
        detalles = ContratoDetalle.objects.filter(status=1,contrato_cabecera_id=id)

        #detalle = ContratoDetalle.objects.get(status=1,id=id)

        lista.append([ u"N° de Contrato:"+SUBTITULO, contrato.num_cont,u"Valor:",redondeo(convert_cero_none(contrato.base0) + convert_cero_none(contrato.baseiva))])
        lista.append([u"Fecha de Registro:"+SUBTITULO, str(contrato.fecha_reg),u"Iva:",redondeo(convert_cero_none(contrato.monto_iva))])
        lista.append([ u"Código del Cliente:"+SUBTITULO, contrato.cliente.ruc,u"Total:",redondeo(convert_cero_none(contrato.base0) + convert_cero_none(contrato.baseiva) + convert_cero_none(contrato.monto_iva))])
        lista.append([ u"Razón Social del Cliente:"+SUBTITULO, contrato.cliente.razon_social,u"Costo:",redondeo(convert_cero_none(contrato.getTotalDetallesCabecera()))])
        lista.append([u""])
        for detalle in detalles:
            if detalle.tipo_comprobante_id == 15 or detalle.tipo_comprobante_id == 14:
                inventario = Inventario_Cabecera.objects.get(status=1,id=detalle.modulo_id)
                lista.append([u"Inventario"])
                lista.append([u"N° Comp:"+SUBTITULO,inventario.num_comp,u"N° Doc:"+SUBTITULO,inventario.num_doc])
                lista.append([u"Fecha Registro:"+SUBTITULO,str(inventario.fecha_reg),u"Detalle:"+SUBTITULO,inventario.detalle])
                lista.append([u"Item"+COLUMNA,u"Cantidad"+COLUMNA,u"Costo"+COLUMNA,u"Total"+COLUMNA])

                for detalle_inv in inventario.getInventarioDetalles():
                    lista.append([unicode(detalle_inv.item.nombre),detalle_inv.cantidad,detalle_inv.costo,redondeo(convert_cero_none(detalle_inv.cantidad) * convert_cero_none(detalle_inv.costo))])

                lista.append([u""])

            elif detalle.tipo_comprobante_id == 2:
                venta = Venta_Cabecera.objects.get(status=1,id=detalle.modulo_id)
                lista.append([u"Venta"])
                lista.append([u"N° Comp: "+SUBTITULO,venta.numero_comprobante,u"N° Doc: "+SUBTITULO,venta.num_documento])
                lista.append([u"Fecha Registro: "+SUBTITULO,str(venta.fecha),u"Concepto: "+SUBTITULO,venta.concepto])



                lista.append([u""])

            elif detalle.tipo_comprobante_id == 1:
                compra = CompraCabecera.objects.get(status=1,id=detalle.modulo_id)
                lista.append([u"Compra"])
                lista.append([u"N° Comp: "+SUBTITULO,compra.num_comp,u"Código Proveedor: "+SUBTITULO,compra.proveedor.ruc])
                lista.append([u"N° Doc: "+SUBTITULO,compra.num_doc,u"Base 0: "+SUBTITULO,compra.base0])
                lista.append([u"Fecha Registro: "+SUBTITULO,str(compra.fecha_reg),u"Base Iva: "+SUBTITULO,compra.baseiva])
                lista.append([u"Razón Social Proveedor: "+SUBTITULO,compra.proveedor.razon_social,u"Monto Iva: "+SUBTITULO,compra.monto_iva])
                lista.append([u"Concepto: "+SUBTITULO,compra.concepto])
                lista.append([u""])

            else:
                pass

    except:
        detalles = None
        contrato = None

    return ExcelResponse(lista, 'Detalle_contrato_'+str(contrato.num_cont))
    #return generar_pdf('reporte_contratos/reporte_contrato_detalle_pdf.html',datos)
