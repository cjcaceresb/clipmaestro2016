#! /usr/bin/python
# -*- coding: UTF-8-*-
import os, sys
sys.stdout = sys.stderr

import site
site.addsitedir('/var/www/clientes/clipmaestro_condiecsa/clipmaestro/clipmaestro')

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
if '/var/www/clientes/clipmaestro_condiecsa/clipmaestro/clipmaestro' not in sys.path:
    sys.path.append('/var/www/clientes/clipmaestro_condiecsa/clipmaestro/clipmaestro')
os.environ['DJANGO_SETTINGS_MODULE'] = 'clipmaestro.settings'

