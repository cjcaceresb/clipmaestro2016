#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.http import *
from librerias.funciones.funciones_vistas import *

def contabilidad_menu(request):
    return render_to_response('contabilidad_menu.html', context_instance=RequestContext(request))

def ventas_menu(request):
    return render_to_response('ventas_menu.html', context_instance=RequestContext(request))

def compras_menu(request):
    return render_to_response('compras_menu.html', context_instance=RequestContext(request))

def cm_login(request):
    return render_to_response('cm_login.html', context_instance=RequestContext(request))

@login_required(login_url='/')
def administracion(request):
    return render_to_response('administracion.html', context_instance=RequestContext(request))






