#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.core.context_processors import request
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.core.urlresolvers import reverse
import datetime
from contabilidad.formularios.LoginForm import *
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from administracion.models import *

def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

@csrf_exempt
def login_view(request):
    '''
    Vista que realiza el proceso de login
    :param request:
    :return:
    '''
    if not request.user.is_authenticated():
        siguiente = request.GET.get("next", "")
        if request.method == "POST":
            form = LoginForm(request.POST)
            if form.is_valid():
                username = form.cleaned_data["username"]
                password = form.cleaned_data["password"]
                siguiente = form.cleaned_data["next"]
                user = authenticate(username=username, password=password, is_active=True)
                if user is not None:
                    if user.is_active:
                        login(request, user)
                        persona = Persona.objects.get(id=user.id)
                        persona.last_ip = get_client_ip(request)
                        persona.save()
                        if siguiente != "":
                            return HttpResponseRedirect(siguiente)
                        else:
                            return HttpResponseRedirect(reverse("administracion"))
                    else:
                        messages.info(request, u"El usuario está inactivo comunicarse con el administrador del sistema")
                        return HttpResponseRedirect(reverse("login"))
                else:
                    messages.error(request, u"Ha ingresado un usuario o contraseña incorrecta")
                    return render_to_response("login/login.html", {"form": form}, context_instance=RequestContext(request))
            else:
                form = LoginForm(initial={"next": siguiente})
                messages.error(request, u"Por favor para iniciar sesión debe ingresar su usuario y su clave.")
                return render_to_response("login/login.html", {"form": form}, context_instance=RequestContext(request))
        else:
            form = LoginForm(initial={"next": siguiente})
            return render_to_response("login/login.html", {"form": form}, context_instance=RequestContext(request))
    else:
        return HttpResponseRedirect(reverse("administracion"))

def salir(request):
    logout(request)
    return HttpResponseRedirect(reverse("login"))


