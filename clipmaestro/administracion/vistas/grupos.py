#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from librerias.funciones.paginacion import *
from django.core.paginator import *
from administracion.models import *
from administracion.formularios.RolesForm import *
import datetime
from django.forms.formsets import formset_factory
from django.db import IntegrityError,transaction
from django.forms.util import ErrorList
from librerias.funciones.permisos import *
from administracion.funciones.grupos_func import *


__author__ = 'Clip Maestro'


class ErrorRoles(Exception):
    def __init__(self, valor):
        self.valor = valor

    def __str__(self):
        return unicode(self.valor)


@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def lista_grupos(request):
    buscador = BuscadorRol(request.GET)
    roles = busqueda_roles(buscador)
    paginacion = Paginator(roles, 10)
    numero_pagina = request.GET.get("page")

    try:
        roles = paginacion.page(numero_pagina)
    except PageNotAnInteger:
        roles = paginacion.page(1)
    except EmptyPage:
        roles = paginacion.page(paginacion._num_pages)
    total_paginas = paginacion._num_pages
    numero = roles.number
    lista_pepa = arreglo_paginas_template(total_paginas, numero)

    return render_to_response("grupo/grupos.html", {"objetos": roles,"buscador": buscador, "total_paginas": total_paginas,"numero": numero,"arreglo_paginado": lista_pepa, "request": request}, context_instance=RequestContext(request))



@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def agregar_grupo(request):
    now = datetime.datetime.now()
    cabecera = FormRoles()
    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = EmpresaGeneral.objects.get(id=id_empresa)
    contador = 0
    lista_id = []
    #try:
    if request.method == "POST":
        llenar_lista_id_post(empresa, lista_id, request)
        cabecera = FormRoles(request.POST)

        if cabecera.is_valid():
            grupo = Grupo()
            grupo.descripcion = cabecera.getDescripcion()
            grupo.nombre = cabecera.getNombre()
            grupo.fecha_creacion = now
            grupo.usuario_creacion = request.user.username
            grupo.save()

            for g_modulos in empresa.get_modulos_empresa():
                for app in g_modulos.get_app():
                    for permisos in app.get_permisos():
                        if request.POST.get(str(permisos.id) + "-" + str(app.id)):
                            permisos.grupo_permisos.add(grupo)
                            contador += 1

            if contador > 0:
                messages.success(request, u'El Grupo de Permisos "' + grupo.nombre +u'" se agregó exitosamente')
                return HttpResponseRedirect(reverse("lista_grupos"))
            else:
                messages.error(request, u'El Grupo de Permisos debe tener por lo menos un permiso')
                transaction.rollback()

        else:
            messages.error(request, u"Por favor verifique los campos obligatorios: ")

        return render_to_response("grupo/registrar_grupo.html", {"gmodulos": empresa,
                                                               "cabecera": cabecera,
                                                               "lista": lista_id},
                                  context_instance=RequestContext(request))
    #except:
    #    transaction.rollback()
    #    messages.error(request, u"Error al guardar la información")
    #    return HttpResponseRedirect(reverse("lista_roles"))

    return render_to_response("grupo/registrar_grupo.html", {"gmodulos": empresa,
                                                           "cabecera": cabecera,
                                                           "lista": lista_id},
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def detalle_grupo(request, id):
    try:
        grupo = Grupo.objects.get(id=id)
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = EmpresaGeneral.objects.get(id=id_empresa)
        lista_permisos = grupo.get_lista_permisos_id()
        cabecera = FormRolesEditar(initial={
            "id": grupo.id,"nombre": grupo.nombre, "descripcion": grupo.descripcion
        })

        return render_to_response("grupo/detalle_grupo.html", {"gmodulos": empresa,
                                                               "cabecera": cabecera,
                                                               "id_rol": id,
                                                               "lista_permisos": lista_permisos},
                                  context_instance=RequestContext(request))
    except Grupo.DoesNotExist:
        raise Http404

def validar_campos_editar(cabecera, id, request):
    contador = 0
    try:
        validador_espacios(cabecera.getNombre())
    except:
        messages.error(request, u'El nombre del Grupo de Permisos no es permitido porque solo tiene espacios en blanco')
        contador += 1
    if Grupo.objects.exclude(id=id).filter(status=1).filter(nombre=cabecera.getNombre()).exists():
        messages.error(request, u'El Grupo de Permisos "'+cabecera.getNombre()+u'" ya ha sido ingresado al sistema, por favor verifique')
        contador += 1
    try:
        validador_espacios(cabecera.getDescripcion())
    except:
        messages.error(request, u'La descripción del Grupo de Permisos no es permitido porque solo tiene espacios en blanco')
        contador += 1
    if contador > 0:
        return False
    else:
        return True


@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def editar_grupo(request, id):
    try:
        grupo = Grupo.objects.get(id=id)
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = EmpresaGeneral.objects.get(id=id_empresa)
        cabecera = FormRolesEditar(initial={"nombre": grupo.nombre, "descripcion": grupo.descripcion})
        now = datetime.datetime.now()
        contador = 0
        empresa.get_modulos_empresa()
        grupo = Grupo.objects.get(id=id)
        lista_id = grupo.get_lista_permisos_id()
        #try:
        if request.method == "POST":
            llenar_lista_id_post(empresa, lista_id, request)
            cabecera = FormRolesEditar(request.POST)
            if validar_campos_editar(cabecera, id, request) and cabecera.is_valid():
                grupo.descripcion = cabecera.getDescripcion()
                grupo.nombre = cabecera.getNombre()
                grupo.fecha_actualizacion = now
                grupo.usuario_actualizacion = request.user.username
                grupo.save()

                for g_modulos in empresa.get_modulos_empresa():
                    for aplicaciones in g_modulos.get_app():
                        for permisos in aplicaciones.get_permisos():
                            permisos.grupo_permisos.remove(grupo)
                            if request.POST.get(str(permisos.id) + "-" + str(aplicaciones.id), "") != "":
                                permisos.grupo_permisos.add(grupo)
                                contador += 1
                if contador > 0:
                    messages.success(request, u'El Grupo de Permisos "'+grupo.nombre +u'" se ha modificado exitosamente')
                    return HttpResponseRedirect(reverse("lista_grupos"))
                else:
                    messages.error(request, u'El Grupo de Permisos debe tener por lo menos un permiso')
                    return render_to_response("grupo/modificar_grupo.html", {"gmodulos": empresa,
                                                                           "cabecera": cabecera,
                                                                           "lista": lista_id}, context_instance=RequestContext(request))
            else:
                lista_errores = u"Por favor verifique los siguientes campos obligatorios: "
                for i in cabecera.errors:
                    lista_errores = lista_errores +(unicode(i)).upper() + ", "
                if cabecera.errors:
                    messages.error(request, unicode(lista_errores[0:-2]))
                return render_to_response("grupo/modificar_grupo.html", {"gmodulos": empresa,
                                                                       "cabecera": cabecera,
                                                                       "lista": lista_id}, context_instance=RequestContext(request))
        #except:
        #    transaction.rollback()
        #    messages.error(request,u"Error al guardar la información")
    except Grupo.DoesNotExist:
        raise Http404
    return render_to_response("grupo/modificar_grupo.html", {"gmodulos": empresa,
                                                           "cabecera": cabecera,
                                                           "lista": lista_id}, context_instance=RequestContext(request))


@login_required(login_url='/')
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def eliminar_grupo(request, id):
    try:
        grupo = Grupo.objects.get(id=id)
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = EmpresaGeneral.objects.get(id=id_empresa)
        now = datetime.datetime.now()
        grupo.status = 0
        grupo.usuario_actualizacion = request.user.username
        grupo.fecha_actualizacion = now

        if Persona.objects.filter(grupo=grupo, estado=1).exists():
            raise ErrorRoles(u'No puede eliminar el Grupo de Permisos "' + unicode(grupo.nombre) + u'", ya que está siendo utilizado por usuarios del sistema, '
                             u"intente modificarlo")

        for g_modulos in empresa.get_modulos_empresa():
            for aplicaciones in g_modulos.get_app():
                for permisos in aplicaciones.get_permisos():
                    permisos.grupo_permisos.remove(grupo)

        grupo.save()
        messages.success(request, u'Se eliminó con exito el Grupo de Permisos "'+grupo.nombre+u'"')

    except Grupo.DoesNotExist:
        transaction.rollback()
        raise Http404
    except ErrorRoles, e:
        transaction.rollback()
        messages.error(request, unicode(e))
    except:
        transaction.rollback()
        messages.error(request, u"Existió un problema en el servidor")

    return HttpResponseRedirect(reverse("lista_grupos"))
