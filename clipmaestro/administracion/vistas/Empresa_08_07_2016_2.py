#! /usr/bin/python
# -*- coding: UTF-8-*-
from django.template import RequestContext
from administracion.formularios.EmpresaForm import UtilidadPerdidaForm
from librerias.funciones.permisos import *
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from administracion.models import *
from administracion.formularios.personaForm import *
from django.core.paginator import *
import datetime
from django.core.files import File
from administracion.formularios.EmpresaForm import *
from clipmaestro.settings import *
from librerias.funciones.funciones_vistas import *
from administracion.models import *
import os

__author__ = 'Clip Maestro'

EMPRESA = 'empresa'     # Carpeta donde se guarda la imagen de la empresa

def save_file(file, path=''):
    ''' Little helper to save a file
    '''
    dir = str('%s/%s' % (MEDIA_ROOT, EMPRESA))
    if not os.path.isdir(dir):
        os.mkdir(dir)
        os.chmod(dir, 01777)
    filename = file._get_name()
    fd = open('%s/%s/%s' % (MEDIA_ROOT, EMPRESA, str(path) + str(filename)), 'wb')
    for chunk in file.chunks():
        fd.write(chunk)
    fd.close()


@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso, es_admin=True)
def configuracion_empresa(request):
    now = datetime.datetime.now()
    parametro_empresa = get_parametros_empresa()

    try:
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = EmpresaGeneral.objects.using('base_central').get(id=id_empresa)
        empresa_form = EmpresaForm(initial={'rucci': empresa.rucci, 'razon_social': empresa.razon_social,
                                            'tipo_empresa': empresa.tipo_empresa, 'nombre_comercial':empresa.nombre_comercial,
                                            'fecha_inicio_actividades': empresa.fecha_inicio_actividades.strftime("%Y-%m-%d"),
                                            'contribuyente_especial': empresa.contribuyente_especial,
                                            'representante_legal': empresa.representante_legal,
                                            "num_resolucion": empresa.num_resolucion,
                                            'tipo_identificacion': empresa.tipo_identificacion.codigo,
                                            'direccion': empresa.direccion, 'correo': empresa.correo,
                                            'telefono': empresa.telefono, 'actividad': empresa.actividad_id,
                                            'logotipo': empresa.logotipo})

        datos_contador = ContadorForm(initial={'nombre_contador': empresa.nombre_contador,
                                               'ruc_contador': empresa.ruc_contador, 'telefono_contador': empresa.telefono_contador,
                                               'correo_contador': empresa.correo_contador,
                                               'direccion_contador': empresa.direccion_contador})
    except:
        id_empresa = Empresa.objects.all()[0].empresa_general_id
        empresa = EmpresaGeneral.objects.get(id=id_empresa)
        empresa_form = EmpresaForm()
        datos_contador = ContadorForm()

    try:
        utilidad_perdida_form = UtilidadPerdidaForm(initial=
                                                    {"cuenta_utilidad_ejercicio": parametro_empresa.plan_cuenta_utilidad_id,
                                                     "cuenta_perdida_ejercicio": parametro_empresa.plan_cuenta_perdida_id})
        bandera = 1
    except:
        utilidad_perdida_form = UtilidadPerdidaForm()
        bandera = 0

    if request.method == 'POST':
        empresa_form = EmpresaForm(request.POST, request.FILES)
        datos_contador = ContadorForm(request.POST)
        form_utilidad_perdida = UtilidadPerdidaForm(request.POST)

        if empresa_form.is_valid() and datos_contador.is_valid():
            empresa = EmpresaGeneral.objects.using('base_central').get(id=id_empresa)
            empresa.correo = empresa_form.getCorreo()
            empresa.contribuyente_especial = empresa_form.getContribEspec()
            empresa.num_resolucion = empresa_form.get_num_resolucion()
            empresa.direccion = empresa_form.getDireccion()
            empresa.nombre_comercial = empresa_form.getNombreComercial()
            empresa.actividad = ActividadEmpresa.objects.using("base_central").get(id=empresa_form.getActividad())
            try:
                empresa.logotipo = 'empresa/'+request.FILES['logotipo'].name
                save_file(request.FILES['logotipo'])
            except:
                pass
            empresa.fecha_inicio_actividades = empresa_form.getFechaIniAct()
            empresa.nombre_contador = datos_contador.getNombreContador()
            empresa.ruc_contador = datos_contador.getRucContador()
            empresa.correo_contador = datos_contador.getCorreoContador()
            empresa.direccion_contador = datos_contador.getDireccionContador()
            empresa.telefono_contador = datos_contador.getTelefonoContador()
            empresa.razon_social = empresa_form.getRazonSocial()
            empresa.representante_legal = empresa_form.getRepresentLegal()
            empresa.rucci = empresa_form.getRucci()
            empresa.telefono = empresa_form.getTelefono()
            empresa.tipo_empresa = empresa_form.getTipoEmpresa()
            empresa.tipo_identificacion = Identificacion.objects.using("base_central").get(codigo=empresa_form.getTipoIdentific())
            empresa.fecha_creacion = now
            empresa.fecha_actualizacion = now
            empresa.usuario_creacion = request.user.username
            empresa.usuario_actualizacion = request.user.username
            empresa.save()

            #UPDATE
            parametro_empresa = get_parametros_empresa()
            if form_utilidad_perdida.getPerdidaEjercicio() is not None:
                parametro_empresa.plan_cuenta_perdida_id = form_utilidad_perdida.getPerdidaEjercicio().id
            else:
                parametro_empresa.plan_cuenta_perdida_id = None

            if form_utilidad_perdida.getUtilidadEjericio() is not None:
                parametro_empresa.plan_cuenta_utilidad_id = form_utilidad_perdida.getUtilidadEjericio().id
            else:
                parametro_empresa.plan_cuenta_utilidad_id = None

            if bandera == 1:
                parametro_empresa.usuario_actualizacion = request.user.username
                parametro_empresa.fecha_actualizacion = now
            else:
                parametro_empresa.usuario_creacion = request.user.username
                parametro_empresa.fecha_creacion = now

            parametro_empresa.save()

            messages.success(request, u"Se han grabado con éxito los cambios "
                                      u"en la empresa "+ unicode(empresa.razon_social))

            return HttpResponseRedirect(reverse("administracion"))

        else:
            for i in empresa_form:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
            for i in datos_contador:
                if i.errors:
                    i.field.widget.attrs['class'] = 'selectpicker campo_requerido'
            messages.error(request, u"Por favor, llene los campos obligatorios")

    return render_to_response("Empresa/configuracion_empresa.html", {'formulario': empresa_form,
                                                                     'contadorform': datos_contador,
                                                                     "empresa": empresa,
                                                                     "form_utilidad_perdida": utilidad_perdida_form}, context_instance=RequestContext(request))


def cm_404(request):
    return render_to_response("Empresa/cm_404.html", context_instance=RequestContext(request))

def cm_500(request):
    return render_to_response("Empresa/cm_500.html", context_instance=RequestContext(request))


from rexec import FileWrapper
import os
from django.http import HttpResponse


def test(request):
    #try:
    filename = "/home/server_sftp/backup_clientes_clipmaestro/2016-07-07/demo_pv.sql.gz"
    wrapper = FileWrapper(file(filename))
    response = HttpResponse(wrapper, content_type='application/javascript')
    response['Content-Disposition'] = 'attachment; filename=%s' % os.path.basename(filename)
    response['Content-Length'] = os.path.getsize(filename)
    #except:
    #    return render_to_response("Empresa/cm_500.html", context_instance=RequestContext(request))
    return response
