#! /usr/bin/python
# -*- coding: UTF-8 -*-
__author__ = 'Clipmaestro'

from django.contrib import admin
from administracion.models import *
import datetime
from django.contrib import messages
from django import forms
from django.forms.models import BaseInlineFormSet
from django.forms.util import ErrorList
from clipmaestro.settings import *

def user_unicode(self):
    return u'%s' % self.nombre


def sucuencia_tipo_comprobante_unicode(self):
    return u'%s - %s' % (self.tipo_comprobante.descripcion, self.secuencia)


def descripcion_unicode(self):
    return u"%s" % self.descripcion


def documento_sri_unicode(self):
    return u"%s - %s" % (self.codigo_documento, self.descripcion_documento)


def codigo_descripcion_unicode(self):
    return u"%s - %s" % (self.codigo, self.descripcion)


def compra_unicode(self):
    try:
        return u"%s - %s" % (self.num_comp, self.proveedor.razon_social)
    except:
        return u"%s" % self.num_comp


def venta_unicode(self):
    try:
        return u"%s - %s" % (self.numero_comprobante, self.cliente.razon_social)
    except:
        return u"%s" % self.numero_comprobante

def proveedor_cliente_unicode(self):
    return u"%s - %s" % (self.ruc, self.razon_social)


def codigo_sri_unicode(self):
    return u"%s - %s -  %s %%" % (self.codigo, self.descripcion, self.porcentaje)


def vigencia_doc_empresa_unicode(self):
    return u"%s %s" % (self.documento_sri.descripcion_documento, self.serie)


def colaboradores_unicode(self):
    return u"%s %s" % (self.apellidos_completos, self.nombres_completos)


def cliente_direccion_unicode(self):
    return u"%s %s" % (self.descripcion, self.direccion1)


def item_unicode(self):
    return u"%s - %s" % (self.codigo, self.nombre)


def cuenta_banco_unicode(self):
    return u"%s - %s %s" % (self.descripcion, self.numero_cuenta, self.tipo_cuenta_banco.descripcion)


def banco_unicode(self):
    return u"%s %s" % (self.cuenta_banco, self.num_comp)


def sustento_unicode(self):
    return u"%s - %s" % (self.codigo, self.descripcion)

def categoria_item_unicode(self):
    return u"%s" % (self.descripcion)

SecuenciaTipoComprobante.__unicode__ = sucuencia_tipo_comprobante_unicode
TipoComprobante.__unicode__ = descripcion_unicode
Documento.__unicode__ = documento_sri_unicode
Item.__unicode__ = item_unicode
Bodega.__unicode__ = descripcion_unicode
PlanCuenta.__unicode__ = codigo_descripcion_unicode
Tipo_Cuenta_Banco.__unicode__ = descripcion_unicode
CompraCabecera.__unicode__ = compra_unicode
Proveedores.__unicode__ = proveedor_cliente_unicode
Categoria_Item.__unicode__ = categoria_item_unicode
Cliente.__unicode__ = proveedor_cliente_unicode
TipoPago.__unicode__ = descripcion_unicode
Codigo_SRI.__unicode__ = codigo_sri_unicode
VigenciaDocEmpresa.__unicode__ = vigencia_doc_empresa_unicode
Cliente_Direccion.__unicode__ = cliente_direccion_unicode
Cuenta_Banco.__unicode__ = cuenta_banco_unicode
FormaPago.__unicode__ = descripcion_unicode
Venta_Cabecera.__unicode__ = venta_unicode
Banco.__unicode__ = banco_unicode
TipoCuenta.__unicode__ = descripcion_unicode
Tipos_Retencion.__unicode__ = descripcion_unicode
Identificacion.__unicode__ = descripcion_unicode
Categoria_Item.__unicode__ = descripcion_unicode
Unidad.__unicode__ = descripcion_unicode
Tipo_Mov_Inventario.__unicode__ = descripcion_unicode
Sustento_Tributario.__unicode__ = sustento_unicode
Forma_Pago_SRI.__unicode__ = descripcion_unicode
Tipo_Pago_SRI.__unicode__ = descripcion_unicode
Centro_Costo.__unicode__ = descripcion_unicode
#cosas jmunoz ver campos de cosas que son foreignkeys en base y objetos de comboboxes
# MiClase.__unicode__ = funcionparaverunatributo
Persona.__unicode__ = colaboradores_unicode
Ciudad.__unicode__ = descripcion_unicode
TipoPersona.__unicode__ = descripcion_unicode
Sector.__unicode__ = descripcion_unicode
Permisos.__unicode__ = user_unicode
Reportes_Jasper.__unicode__ = user_unicode


class SecTipoComprobanteAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    search_fields = ['tipo_comprobante__descripcion', 'secuencia']
    list_display = ["id", "tipo_comprobante", "secuencia", "fecha_actual", "status","fecha_actualizacion"]
    list_filter = ["tipo_comprobante", "fecha_actual", "status", "fecha_actualizacion"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class SecComprobanteContableAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    search_fields = ['tipo_comp__descripcion', 'secuencia']
    list_display = ["id", "tipo_comp", "anio", "mes", "secuencia", "fecha_actual", "status", "fecha_actualizacion"]
    list_filter = ["tipo_comp", "anio", "mes", "fecha_actual", "status"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class VigenciaDocEmpresaAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    search_fields = ['id', 'documento_sri__descripcion_documento', 'serie', 'autorizacion', 'fecha_emi']
    list_display = ['id', 'documento_sri', 'serie', 'autorizacion', 'fecha_emi']
    list_filter = ["documento_sri", "serie", "autorizacion"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class ItemBodegaAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    search_fields = ['id', 'item__codigo', 'item__nombre', 'bodega__descripcion', 'cantidad_actual']
    list_display = ['id', 'get_item_bodega', 'cantidad_actual']
    list_filter = ["status"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class CuentaBancoAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    search_fields = ['id', 'descripcion', 'plan_cuenta_descripcion', 'numero_cuenta', 'secuencia_cheque']
    list_display = ['id', 'descripcion', 'plan_cuenta', 'numero_cuenta', 'secuencia_cheque']
    list_filter = ["tipo_cuenta_banco"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class PlanCuentaAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'codigo', 'descripcion', 'id_cta_grupo', 'nivel', 'naturaleza', 'tipo']
    search_fields = ['id', 'descripcion', 'id_cta_grupo__descripcion', 'nivel', 'naturaleza']
    list_filter = ["naturaleza", "nivel"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class Cuenta_RetencionAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'plan_cuenta', 'tipo_codigo_sri', 'porcentaje', 'status']
    search_fields = ['id', 'plan_cuenta__descripcion', 'tipo_codigo_sri', 'porcentaje', 'status']
    list_filter = ["tipo_codigo_sri", "status"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class DocModificaAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'compra_cabecera', 'tipo_documento', 'numero_documento', 'autorizacion', 'status']
    search_fields = ['id', 'compra_cabecera__num_comp', 'tipo_documento__descripcion', 'numero_documento', 'autorizacion', 'status']
    list_filter = ["status"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class ItemAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'codigo', 'nombre', 'descripcion']
    search_fields = ['id', 'codigo', 'nombre', 'descripcion']
    list_filter = ["status", "categoria_item"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class ProveedorAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'razon_social', 'tipo_identificacion', 'ruc']
    search_fields = ['id', 'razon_social', 'ruc']
    list_filter = ["status"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class CategoriaItemAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'descripcion', 'tipo_item', 'getCuentaVenta', 'getCuentaCompInv', 'getCuentaCostoVenta']
    search_fields = ['id', 'descripcion', 'tipo_item']
    list_filter = ["status"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class CuentasPagarAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'fecha_reg', 'proveedor', 'compra_cabecera', 'documento', 'num_doc', 'naturaleza',
                    'status']
    search_fields = ['id', 'fecha_reg', 'proveedor__razon_social', 'compra_cabecera__num_comp',
                     'documento__descripcion_documento', 'num_doc', 'naturaleza',
                     'monto', 'pagado', 'status']

    list_filter = ["status", "naturaleza"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class CuentasCobrarAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'fecha_reg', 'cliente', 'venta_cabecera', 'documento', 'num_doc', 'naturaleza',
                    'status']
    search_fields = ['id', 'fecha_reg', 'venta_cabecera__razon_social', 'venta_cabecera__num_comp',
                     'documento__descripcion_documento', 'num_doc', 'naturaleza',
                     'monto', 'pagado', 'status']

    list_filter = ["status", "naturaleza"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class ReembolsoGastoCompraAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'compra_cabecera', 'identificacion', 'num_id', 'documento',
                    'num_doc', 'fecha_emi', 'status']
    search_fields = ['id', 'compra_cabecera__num_comp', 'identificacion__descripcion', 'num_id',
                     'documento__descripcion_documento', 'num_doc', 'fecha_emi', 'status']
    list_filter = ["status"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class AjusteVentaDetalleInline(admin.TabularInline):
    model = AjusteVentaDetalle
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]

class AjusteVentaCabeceraAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'num_comp', 'fecha_emi', 'cliente', 'venta_cabecera', 'num_doc', 'num_doc_modifica', 'status']
    search_fields = ['id', 'num_comp', 'fecha_emi', 'cliente__razon_social', 'num_doc', 'num_doc_modifica', 'status']
    list_filter = ['status']
    inlines = (AjusteVentaDetalleInline,)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class ContratoDetalleInline(admin.TabularInline):
    model = ContratoDetalle
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class ContratoCabeceraAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'fecha_reg', 'num_cont', 'concepto', 'status']
    search_fields = ['id', 'fecha_reg', 'num_cont', 'status']
    list_filter = ['status']
    inlines = (ContratoDetalleInline,)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class CompraDetalleInline(admin.TabularInline):
    model = CompraDetalle
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class ATSDetInline(admin.TabularInline):
    model = ATS
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class CompraCabeceraAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'num_comp', 'fecha_reg', 'proveedor', 'documento', 'num_doc', 'status']
    search_fields = ['id', 'num_comp', 'fecha_reg', 'proveedor__razon_social', 'proveedor__ruc', 'documento__descripcion_documento', 'num_doc']
    list_filter = ["proveedor", 'documento', 'status']
    inlines = (CompraDetalleInline, ATSDetInline)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class VentaMedia:
    js = (
        ('http://code.jquery.com/jquery-1.11.0.min.js',
         STATIC_URL + 'js/js_administracion/ventas_admin.js'
         )
    )


class VentaDetalleInline(admin.TabularInline):
    model = Venta_Detalle
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class VentaCabeceraAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'numero_comprobante', 'fecha', 'cliente', 'documento', 'num_documento', 'status']
    search_fields = ['id', 'numero_comprobante', 'fecha', 'cliente__razon_social', 'cliente__ruc', 'documento__descripcion_documento', 'num_documento']
    list_filter = ["cliente", 'documento', 'status']
    inlines = (VentaDetalleInline,)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class DetalleGuiaRemisionInline(admin.TabularInline):
    model = DetalleGuiaRemision
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class CabeceraGuiaRemisionAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'venta_cabecera', 'fecha_emi', 'vigencia_doc_empresa', 'num_doc', 'cliente', 'status']
    search_fields = ['id', 'venta_cabecera__num_comp', 'fecha_emi', 'vigencia_doc_empresa', 'num_doc', 'cliente',
                     'status']
    list_filter = ['status']
    inlines = (DetalleGuiaRemisionInline,)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class CompContableDetInline(admin.TabularInline):
    model = Detalle_Comp_Contable
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class CompContableCabeceraAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'numero_comprobante', 'fecha', 'concepto_comprobante', 'status']
    search_fields = ['id', 'numero_comprobante', 'fecha', 'concepto_comprobante', 'status']
    list_filter = ["tipo_comprobante", 'status']
    inlines = (CompContableDetInline, )

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class RetencionDetalleDetInline(admin.TabularInline):
    model = RetencionCompraDetalle
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class RetencionCabeceraAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'numero_ret', 'compra_cabecera', 'fecha_emi', 'status']
    search_fields = ['id', 'numero_ret', 'fecha_emi', 'status', 'compra_cabecera__num_comp']
    list_filter = ["vigencia_doc_empresa", 'status']
    inlines = (RetencionDetalleDetInline,)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class InventarioDetalleInline(admin.TabularInline):
    model = Inventario_Detalle
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class Inventario_CabeceraAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'fecha_reg', 'num_comp', 'tipo_mov_inventario', 'status']
    search_fields = ['id', 'fecha_reg', 'num_comp', 'status']
    list_filter = ['status']
    inlines = (InventarioDetalleInline,)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class TransaccionBancoAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'cuenta_banco', 'tipo_comprobante', 'num_comp', 'fecha_reg', 'num_cheque', 'status']
    search_fields = ['id', 'cuenta_banco_descripcion', 'tipo_comprobante_descripcion', 'num_comp', 'fecha_reg', 'num_cheque', 'status']
    list_filter = ["cuenta_banco", 'tipo_comprobante', 'status']

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class CXP_PagoDetInline(admin.TabularInline):
    model = CXP_pago
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class PagoAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']

    list_display = ['id', 'proveedor', 'fecha_reg', 'tipo_comprobante', 'num_comp', 'forma_pago', 'monto',
                    'num_cheque', 'referencia', 'fecha_pago', 'plazo_diferido', 'interes', 'concepto', 'status']

    search_fields = ['id', 'proveedor__razon_social', 'fecha_reg', 'num_comp', 'num_cheque']

    list_filter = ["tipo_comprobante", 'status']

    inlines = (CXP_PagoDetInline,)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


#cosas creadas por jmunoz para mejorar el llenado desde la ventana web
class ClienteAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    search_fields = ['nombre', 'ruc']
    list_display = ["id", "ruc", "razon_social", "nombre", "status","fecha_actualizacion"]
    list_filter = ["nombre", "status", "fecha_actualizacion"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class VendedorAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    search_fields = ['nombres_completos', 'num_id', 'apellidos_completos']
    list_display = ["id", "num_id", "apellidos_completos", "nombres_completos", "status","fecha_actualizacion"]
    list_filter = ["nombres_completos", "status", "fecha_actualizacion"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class EmpleadoAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    search_fields = ['nombres_completos', 'apellidos_completos']
    list_display = ["id", "apellidos_completos", "nombres_completos", "status","fecha_actualizacion"]
    list_filter = ["nombres_completos", "apellidos_completos","status", "fecha_actualizacion"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class CXC_CobroDetInline(admin.TabularInline):
    model = CXC_cobro
    extra = 1
    exclude = ["fecha_actualizacion", "usuario_actualizacion", "fecha_creacion", "usuario_creacion"]


class CobroAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']

    list_display = ['id', 'cliente', 'fecha_reg', 'tipo_comprobante', 'num_comp', 'forma_pago', 'monto',
                    'referencia', 'fecha_pago', 'concepto', 'status']

    search_fields = ['cliente__razon_social', 'num_comp', 'referencia']

    list_filter = ["tipo_comprobante", 'status']

    inlines = (CXC_CobroDetInline,)

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()

class GrupoAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'nombre', 'descripcion']
    search_fields = ['id', 'descripcion', 'nombre']
    list_filter = ["nombre", "status"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class PersonaAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'nombres_completos', 'apellidos_completos']
    search_fields = ['id', 'nombres_completos', 'apellidos_completos']
    list_filter = ["nombres_completos", "apellidos_completos"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()



class ReporteJasperAdmin(admin.ModelAdmin):
    exclude = ['usuario_creacion', 'usuario_actualizacion', 'fecha_creacion', 'fecha_actualizacion']
    list_display = ['id', 'codigo', 'nombre']
    search_fields = ['id', 'codigo', 'nombre']
    list_filter = ["codigo", "nombre"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()


class ConfigJasperAdmin(admin.ModelAdmin):

    list_display = ['id', 'url']
    search_fields = ['id', 'url']
    list_filter = ["url"]

    def save_model(self, request, obj, form, change):
        if not obj.usuario_creacion:
            obj.usuario_creacion = request.user.username
        obj.usuario_actualizacion = request.user.username
        if not obj.fecha_creacion:
            obj.fecha_creacion = datetime.datetime.now()
        obj.fecha_actualizacion = datetime.datetime.now()
        obj.save()




admin.site.register(Colaboradores, VendedorAdmin)
admin.site.register(Cliente, ClienteAdmin)
admin.site.register(Empleados, EmpleadoAdmin)
admin.site.register(Cobro, CobroAdmin)
admin.site.register(Persona, PersonaAdmin)
admin.site.register(Grupo, GrupoAdmin)
admin.site.register(Reportes_Jasper, ReporteJasperAdmin)
#fin cosas jmunoz
admin.site.register(SecuenciaTipoComprobante, SecTipoComprobanteAdmin)
admin.site.register(Seq_Comp_Contable, SecComprobanteContableAdmin)
admin.site.register(VigenciaDocEmpresa, VigenciaDocEmpresaAdmin)
admin.site.register(Item_Bodega, ItemBodegaAdmin)
admin.site.register(Cuenta_Banco, CuentaBancoAdmin)
admin.site.register(CompraCabecera, CompraCabeceraAdmin)
admin.site.register(Venta_Cabecera, VentaCabeceraAdmin, Media=VentaMedia)
admin.site.register(Cabecera_Comp_Contable, CompContableCabeceraAdmin)
admin.site.register(RetencionCompraCabecera, RetencionCabeceraAdmin)
admin.site.register(Banco, TransaccionBancoAdmin)
admin.site.register(Pago, PagoAdmin)
admin.site.register(PlanCuenta, PlanCuentaAdmin)
admin.site.register(Cuenta_Retencion, Cuenta_RetencionAdmin)
admin.site.register(Doc_Modifica, DocModificaAdmin)
admin.site.register(Reembolso_gasto_compra, ReembolsoGastoCompraAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Cuentas_por_Pagar, CuentasPagarAdmin)
admin.site.register(AjusteVentaCabecera, AjusteVentaCabeceraAdmin)
admin.site.register(Cuentas_por_Cobrar, CuentasCobrarAdmin)
admin.site.register(CabeceraGuiaRemision, CabeceraGuiaRemisionAdmin)
admin.site.register(Inventario_Cabecera, Inventario_CabeceraAdmin)
admin.site.register(ContratoCabecera, ContratoCabeceraAdmin)
admin.site.register(Proveedores, ProveedorAdmin)
admin.site.register(Categoria_Item, CategoriaItemAdmin)
#cosas jmunoz

#fin cosas jmunoz
