#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.core.exceptions import ValidationError
import datetime
import time
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.forms import ModelForm
from django.db.models import Q
from administracion.models import *
from librerias.funciones.validacion_formularios import *
from django.contrib import messages
from librerias.funciones.validacion_rucs import *

solo_letras = u'^[A-Za-zÑñáéíóúÁÉÍÓÚ_\\s]+$'
solo_numeros = u'^[0-9]+$'
solo_alfanumericas = u'[a-zA-Z0-9_\\s]+$'

sexo = (('', ''), ('Masculino', 'Masculino'), ('Femenino','Femenino'))
estado_civil = (('', ''), ('Soltero(a)', 'Soltero(a)'), ('Casado(a)', 'Casado(a)'))

def validate_cedula_unica(value):
    try:
        if(value.index(' ')!= None):
            raise ValidationError(u'%s No es permitido porque tiene espacios en blanco' % value)
    except ValueError:
        pass

    if Persona.objects.exclude(estado=0).filter(cedula=value).exists():
        raise ValidationError(u'%s El Nº de identificación ya ha sido ingresado' % value)
    else:
        return True

def validate_usuario(value):
    try:
        if(value.index(' ')!= None):
            raise ValidationError(u'%s No es permitido ya que tiene espacios en blanco' % value)
    except ValueError:
        pass

def validate_clave(value):
    try:
        if(value.index(' ')!= None):
            raise ValidationError(u'%s No es permitido ya que tiene espacios en blanco' % value)
    except ValueError:
        pass

def validate_fecha(value):
    try:
        if(value.index(' ')!= None):
            raise ValidationError(u'%s No es permitido los espacioes en blanco en la fecha' % value)
    except ValueError:
        pass

def correo_unico(value):
    if Persona.objects.filter(email=value, is_active=True).exists():
         raise ValidationError(u'%s Correo ya ha sido ingresado' % value)
    else:
        return True

def ruc_is_cedula(value):
    if int(value[2]) < 6 and len(value) == 10 and (int(value[0:1]) < 25):
        valores = [int(value[x]) * (2 - x % 2) for x in range(9)]
        suma = sum(map(lambda x: x > 9 and x - 9 or x, valores))
        num = int(str(suma)[-1:])
        if num == 0:
            dig_veri = 0
            if int(value[9]) == dig_veri or int(value[9]) == 0:
                return True
            else:
                raise ValidationError("El Nº de Identificación no  es válido")
        else:
            dig_veri = 10-num

            if int(value[9]) == dig_veri or int(value[9]) == 0:
                return True
            else:
                raise ValidationError("El Nº de Identificaciòn no  es válido")

    else:
        raise ValidationError("cedula no válido")


class FormBuscadorPersona(forms.Form):
    cedula = forms.CharField(max_length=13, label=u"Cédula")
    nombre = forms.CharField(max_length=50, label=u"Nombre")
    usuario = forms.CharField(max_length=50, label=u"Usuario")
    page = forms.IntegerField()

    def get_cedula(self):
        try:
            return self.data["cedula"]
        except KeyError:
            return ""

    def get_nombre(self):
        try:
            return self.data["nombre"]
        except KeyError:
            return ""

    def get_usuario(self):
        try:
            return self.data["usuario"]
        except KeyError:
            return ""


# Clase que me ayuda a validar los formset vacios
class FormUsuariosOnlyRead(forms.Form):
    cedula = forms.CharField(max_length=20, label=u"Cédula")
    nombres = forms.RegexField(max_length=60, label=u"Nombres", regex=solo_letras, validators=[validador_espacios])
    apellidos = forms.RegexField(max_length=60, label=u"Apellidos", regex=solo_letras, validators=[validador_espacios])
    usuario = forms.CharField(max_length=30, label=u"Usuario", validators=[validador_espacios])
    fecha_Nac = forms.CharField(label=u"Fecha Nacimiento")
    telefono = forms.CharField(label=u"Teléfono", validators=[validador_espacios])
    direccion = forms.RegexField(label=u"Dirección", validators=[validador_espacios], regex=solo_alfanumericas)
    email = forms.EmailField(label=u"Email")
    estado_civil = forms.ChoiceField(choices=estado_civil, label=u"Estado Cívil")
    sexo = forms.ChoiceField(choices=sexo, label=u"Sexo")
    facebook = forms.CharField(required=False, max_length=100, label="Facebook")
    twitter = forms.CharField(required=False, max_length=100, label="Twitter")
    grupo = forms.ChoiceField(choices=[], label=u"Grupo")

    def __init__(self, *args, **kwargs):
        super(FormUsuariosOnlyRead, self).__init__(*args, **kwargs)
        self.fields['grupo'].choices = [(x.id, x.nombre) for x in Grupo.objects.exclude(status=0)]
        self.fields['grupo'].widget.attrs['class'] = "selectpicker"
        self.fields['sexo'].widget.attrs['class'] = "selectpicker"
        self.fields['sexo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado_civil'].widget.attrs['class'] = "selectpicker"
        self.fields['estado_civil'].widget.attrs['data-style'] = "slc-primary"
        self.fields['grupo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['fecha_Nac'].widget.attrs['data-date-format'] = "dd-mm-yyyy"
        self.fields['fecha_Nac'].widget.attrs['class'] = "calendario-gris"
        self.fields['fecha_Nac'].widget.attrs['readonly'] = "True"

        self.fields['email'].widget.attrs['placeholder'] = "email@ejemplo.com"
        self.fields['fecha_Nac'].widget.attrs['placeholder'] = "00-00-0000"
        self.fields['telefono'].widget.attrs['placeholder'] = "(99) 9999-9999"
        self.fields['cedula'].widget.attrs['placeholder'] = ""
        self.fields['cedula'].widget.attrs['maxlength'] = "10"
        self.fields['direccion'].widget.attrs['maxlength'] = "125"

        self.fields['usuario'].widget.attrs['maxlength'] = "16"
        self.fields['fecha_Nac'].widget.attrs['maxlength'] = "10"

        self.fields['cedula'].widget.attrs['readonly'] = True
        self.fields['nombres'].widget.attrs['readonly'] = True
        self.fields['apellidos'].widget.attrs['readonly'] = True
        self.fields['usuario'].widget.attrs['readonly'] = True
        self.fields['grupo'].widget.attrs['readonly'] = True
        self.fields['grupo'].widget.attrs['disabled'] = True

        self.fields['email'].widget.attrs['readonly'] = True
        self.fields['telefono'].widget.attrs['readonly'] = True
        self.fields['direccion'].widget.attrs['readonly'] = True
        self.fields['facebook'].widget.attrs['readonly'] = True
        self.fields['twitter'].widget.attrs['readonly'] = True

        self.fields['estado_civil'].widget.attrs['readonly'] = True
        self.fields['estado_civil'].widget.attrs['disabled'] = True
        self.fields['sexo'].widget.attrs['readonly'] = True
        self.fields['sexo'].widget.attrs['disabled'] = True

        self.fields['fecha_Nac'].widget.attrs['readonly'] = True
        self.fields['fecha_Nac'].widget.attrs['disabled'] = True

class FormPersona(forms.Form):
    cedula = forms.CharField(max_length=20, label=u"Cédula")
    nombres = forms.RegexField(max_length=60, label=u"Nombres", regex=solo_letras, validators=[validador_espacios])
    apellidos = forms.RegexField(max_length=60, label=u"Apellidos", regex=solo_letras, validators=[validador_espacios])
    usuario = forms.CharField(min_length=4, max_length=30, label=u"Usuario", validators=[validador_espacios, validate_usuario])
    contrasenia = forms.RegexField( widget=forms.PasswordInput(render_value=False), label=u"Contraseña", validators=[validador_espacios], regex=solo_alfanumericas)
    comfirma_contrasenia = forms.CharField( widget=forms.PasswordInput(render_value=False), label= u"Confirmar Contraseña")
    fecha_nac = forms.DateField(label=u"Fecha Nacimiento", widget=forms.DateInput(format=("%Y-%m-%d")))
    telefono = forms.CharField(label=u"Teléfono", validators=[validador_espacios], required=False)
    direccion = forms.CharField(label=u"Dirección", validators=[validador_espacios], required=False)
    email = forms.EmailField(label=u"Email")
    estado_civil = forms.ChoiceField(choices=estado_civil, label=u"Estado Cívil", required=False)
    sexo = forms.ChoiceField(choices=sexo, label=u"Sexo")
    foto = forms.ImageField(required=False, label=u"Foto")
    facebook = forms.CharField(required=False, max_length=100, label="Facebook")
    twitter = forms.CharField(required=False, max_length=100, label="Twitter")

    def getNombres(self):
        return self.data["nombres"]

    def getFoto(self):
        return self.data["foto"]

    def getPrimerNombre(self):
        return self.data["nombres"].split(" ")[0]

    def getSegundoNombre(self):
        try:
            return self.data["nombres"].split(" ")[1]
        except:
            return ''

    def getApellidos(self):
        return self.data["apellidos"]

    def getPrimerAp(self):
        return self.data["apellidos"].split(" ")[0]

    def getSegundoAp(self):
        try:
            return self.data["apellidos"].split(" ")[1]
        except:
            return ''

    def getCedula(self):
        return self.data["cedula"]

    def getSexo(self):
        return self.data["sexo"]

    def getFecha_Nac(self):
        return self.cleaned_data["fecha_nac"]

    def getTelefono(self):
        return self.data["telefono"]

    def getEstado_Civil(self):
        return self.data["estado_civil"]

    def getFacebook(self):
        return self.data["facebook"]

    def getTwitter(self):
        return self.data["twitter"]

    def getDireccion(self):
        return self.data["direccion"]

    def getCorreo(self):
        return self.data["email"]

    def getUsuario(self):
        return self.data["usuario"]

    def getPassword(self):
        return self.data["contrasenia"]

    def getPassword2(self):
        return self.data["comfirma_contrasenia"]

    def __init__(self, *args, **kwargs):
        super(FormPersona, self).__init__(*args, **kwargs)
        self.fields['sexo'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['sexo'].widget.attrs['data-style'] = "slc-primary show-tick"
        self.fields['estado_civil'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado_civil'].widget.attrs['data-style'] = "slc-primary"
        self.fields['foto'].widget.attrs['class'] = "file"
        self.fields['fecha_nac'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_nac'].widget.attrs['class'] = "calendario-gris datepicker"
        self.fields['fecha_nac'].widget.attrs['readonly'] = "True"
        self.fields['fecha_nac'].widget.attrs['placeholder'] = "0000-00-00"
        self.fields['fecha_nac'].widget.attrs['maxlength'] = "10"
        self.fields['email'].widget.attrs['placeholder'] = "email@ejemplo.com"
        self.fields['telefono'].widget.attrs['placeholder'] = "(99) 9999-9999"
        self.fields['telefono'].widget.attrs['maxlength'] = "15"
        self.fields['cedula'].widget.attrs['placeholder'] = ""
        self.fields['cedula'].widget.attrs['maxlength'] = "10"
        self.fields['direccion'].widget.attrs['maxlength'] = "125"
        self.fields['usuario'].widget.attrs['maxlength'] = "16"
        self.fields['contrasenia'].widget.attrs['maxlength'] = "24"
        self.fields['comfirma_contrasenia'].widget.attrs['maxlength'] = "24"

    def clean(self):
        password1 = self.cleaned_data.get('contrasenia')
        password2 = self.cleaned_data.get('comfirma_contrasenia')
        fecha_nac = self.cleaned_data.get('fecha_nac')
        cedula = self.cleaned_data.get('cedula', "")
        email = self.cleaned_data.get('email', "")
        usuario = self.cleaned_data.get('usuario', "")

        if password1 != "" and password1 is not None:
            if len(password1) < 5:
                self._errors["contrasenia"] = u"Error: la contraseña ingresada debe ser mayor a 5 carácteres"
                self.fields['contrasenia'].widget.attrs['title'] = self._errors["contrasenia"]

        elif password1 != password2:
            self._errors["contrasenia"] = u"Contraseñas diferentes"
            self._errors["comfirma_contrasenia"] = u"Contraseñas diferentes"
            self.fields['contrasenia'].widget.attrs['title'] = self._errors["contrasenia"]
            self.fields['comfirma_contrasenia'].widget.attrs['title'] = self._errors["contrasenia2"]

        if fecha_nac != "" and fecha_nac is not None:
            if fecha_nac.year > (datetime.datetime.now().year - 18):
                self._errors["fecha_nac"] = u"El usuario debe ser mayor de 18 años"
                self.fields['fecha_nac'].widget.attrs['title'] = self._errors["fecha_nac"]

        if cedula != "":
            if Persona.objects.exclude(Q(cedula="9999999999"), Q(estado=1) | Q(estado=2)).filter(Q(cedula=cedula),
                                                                                                 Q(estado=1) |
                                                                                                         Q(estado=2)).exists():
                self._errors["cedula"] = u"El Nº de Identificación ya ha sido ingresado"
                self.fields['cedula'].widget.attrs['title'] = self._errors["cedula"]

            if not isCedula(cedula):
                self._errors["cedula"] = u"Error: El Nº de Identificación es incorrecto."
                self.fields['cedula'].widget.attrs['title'] = self._errors["cedula"]

        if User.objects.filter(is_active=True).filter(email=email).exists():
            self._errors["email"] = u"Error: El Email ya esta asignado a otro usuario."
            self.fields['email'].widget.attrs['title'] = self._errors["email"]

        if User.objects.filter(username=usuario, is_active=True).exists():
            self._errors["usuario"] = u"Error: El usuario ya ha sido ingresado."
            self.fields['usuario'].widget.attrs['title'] = self._errors["usuario"]

        return self.cleaned_data


class FormPersonaEditar(forms.Form):
    cedula = forms.CharField(max_length=20, label=u"Cédula")
    nombres = forms.RegexField(max_length=60, label=u"Nombres", regex=solo_letras, error_message=("Este campo sólo acepta letras"), validators=[validador_espacios])
    apellidos = forms.RegexField(max_length=60, label=u"Apellidos", regex=solo_letras, error_message=("Este campo sólo acepta letras"), validators=[validador_espacios])
    fecha_nac = forms.DateField(label=u"Fecha Nacimiento", widget=forms.DateInput(format=("%Y-%m-%d")))
    telefono = forms.CharField(label=u"Teléfono", validators=[validador_espacios], required=False)
    direccion = forms.CharField(label=u"Dirección", validators=[validador_espacios], required=False)
    email = forms.EmailField(label=u"Email")
    estado_civil = forms.ChoiceField(choices=estado_civil, label=u"Estado Cívil", required=False)
    sexo = forms.ChoiceField(choices=sexo, label=u"Sexo")
    foto = forms.ImageField(required=False, label=u"Foto")
    facebook = forms.CharField(required=False, max_length=100, label=u"Facebook")
    twitter = forms.CharField(required=False, max_length=100, label=u"Twitter")

    def getNombres(self):
        return self.data["nombres"]
    def getFoto(self):
        return self.data["foto"]
    def getPrimerNombre(self):
        return self.data["nombres"].split(" ")[0]
    def getSegundoNombre(self):
        try:
            return self.data["nombres"].split(" ")[1]
        except:
            return ''

    def getApellidos(self):
        return self.data["apellidos"]
    def getPrimerAp(self):
        return self.data["apellidos"].split(" ")[0]
    def getSegundoAp(self):
        try:
            return self.data["apellidos"].split(" ")[1]
        except:
            return ''

    def getCedula(self):
        return self.data["cedula"]
    def getSexo(self):
        return self.data["sexo"]

    def getFecha_Nac(self):
        return self.data["fecha_nac"]

    def getTelefono(self):
        return self.data["telefono"]
    def getEstado_Civil(self):
        return self.data["estado_civil"]
    def getFacebook(self):
        return self.data["facebook"]
    def getTwitter(self):
        return self.data["twitter"]
    def getDireccion(self):
        return self.data["direccion"]
    def getCorreo(self):
        return self.data["email"]

    def clean(self):
        fecha_nac = self.cleaned_data.get('fecha_nac')
        if fecha_nac != "" and fecha_nac is not None:
            if fecha_nac.year > (datetime.datetime.now().year - 18):
                self._errors["fecha_nac"] = u"El usuario debe ser mayor de 18 años"
                self.fields['fecha_nac'].widget.attrs['title'] = self._errors["fecha_nac"]

    def __init__(self, *args, **kwargs):
        super(FormPersonaEditar, self).__init__(*args, **kwargs)
        self.fields['sexo'].widget.attrs['class'] = "selectpicker"
        self.fields['sexo'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado_civil'].widget.attrs['class'] = "selectpicker"
        self.fields['estado_civil'].widget.attrs['data-style'] = "slc-primary"
        self.fields['foto'].widget.attrs['class'] = "file"

        self.fields['fecha_nac'].widget.attrs['data-date-format'] = "yyyy-mm-dd"
        self.fields['fecha_nac'].widget.attrs['class'] = "calendario-gris input-calendario"
        self.fields['fecha_nac'].widget.attrs['placeholder'] = "0000-00-00"
        self.fields['fecha_nac'].widget.attrs['maxlength'] = "10"

        self.fields['email'].widget.attrs['placeholder'] = "email@ejemplo.com"
        self.fields['telefono'].widget.attrs['placeholder'] = "(99) 9999-9999"
        self.fields['telefono'].widget.attrs['maxlength'] = "15"
        self.fields['cedula'].widget.attrs['placeholder'] = "99-99999999"
        self.fields['cedula'].widget.attrs['maxlength'] = "10"
        self.fields['cedula'].widget.attrs['class'] = "numerico"


class FormEditUser(forms.Form):
    '''
    Formulario FormEditUser
    '''
    clave_actual = forms.CharField(widget=forms.PasswordInput(render_value=False), validators=[validador_espacios, validate_usuario], label=u"Contraseña Actual")
    password1 = forms.RegexField(widget=forms.PasswordInput(render_value=False), label=u"Nueva Contraseña", validators=[validador_espacios], regex=solo_alfanumericas)
    password2 = forms.RegexField(widget=forms.PasswordInput(render_value=False), label=u"Confirmar Contraseña", validators=[validador_espacios], regex=solo_alfanumericas)

    def clean(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 != "":
            if password1 and password1 != password2:
                raise forms.ValidationError("Contraseñas no coinciden")

        return self.cleaned_data

    def getClaveActual(self):
        return self.data["clave_actual"]

    def getPassword1(self):
        return self.data["password1"]

    def getPassword2(self):
        return self.data["password2"]

    def __init__(self, *args, **kwargs):
        super(FormEditUser, self).__init__(*args, **kwargs)
        self.fields['clave_actual'].widget.attrs['maxlength'] = "16"
        self.fields['password1'].widget.attrs['maxlength'] = "16"
        self.fields['password2'].widget.attrs['maxlength'] = "16"

