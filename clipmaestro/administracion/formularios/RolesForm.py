#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from django import forms
from administracion.models import *
from django.utils.html import *
from  contabilidad.models import *
from django.template.defaultfilters import default
from django.forms import ModelForm
from django.db.models import Q


class FormRoles(forms.Form):
    nombre = forms.CharField(max_length=200, required=True, label=u"Nombre")
    descripcion = forms.CharField(max_length=750, widget=forms.Textarea, required=True, label=u"Descripción")

    def clean(self):
        """
            Función para validar el formulario
        """
        nombre = self.data["nombre"]
        descripcion = self.data["descripcion"]

        # validar nombre
        if Grupo.objects.filter(nombre=nombre, status=1).exists():
            self._errors["nombre"] = u"El nombre del grupo " \
                                    u"ya se encuetra registrado"

        # validar
        elif unicode(nombre).isspace():
            self._errors["nombre"] = u"Por favor ingrese un dato válido"

        if unicode(descripcion).isspace():
            self._errors["descripcion"] = u"Por favor ingrese un dato válido"

        return self.cleaned_data

    def getNombre(self):
        return self.data["nombre"]

    def getDescripcion(self):
        return self.data["descripcion"]

    def __init__(self, *args, **kwargs):
        super(FormRoles, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs['rows'] = 3
        self.fields['descripcion'].widget.attrs['style'] = 'width: 100%'
        self.fields['descripcion'].widget.attrs['maxlength'] = 200
        self.fields['nombre'].widget.attrs['maxlength'] = 100
        self.fields['nombre'].widget.attrs['style'] = 'width: 100%'

class FormRolesEditar(forms.Form):
    id = forms.IntegerField(required=False)
    nombre = forms.CharField(max_length=220, required=True)
    descripcion = forms.CharField(max_length=1000, widget=forms.Textarea, required=True)

    def getId(self):
        return self.data["id"]
    def getNombre(self):
        return self.data["nombre"]
    def getDescripcion(self):
        return self.data["descripcion"]

    def __init__(self, *args, **kwargs):
        super(FormRolesEditar, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs['rows'] = 3
        self.fields['descripcion'].widget.attrs['maxlength'] = 200
        self.fields['descripcion'].widget.attrs['style'] = 'width: 100%'
        #self.fields['descripcion'].widget.attrs['readonly'] = "readonly"

        self.fields['nombre'].widget.attrs['maxlength'] = 100
        #self.fields['nombre'].widget.attrs['readonly'] = "readonly" cambio para quitarle la caracteristica de solo lectura en el formulario editable
        self.fields['nombre'].widget.attrs['style'] = 'width: 100%'


estados = (("", ""), (1, "Activo"), (2, "Inactivo"))
class BuscadorRol(forms.Form):
    nombre = forms.CharField(max_length=200, required=True, label=u"Nombre")
    descripcion = forms.CharField(max_length=1000, widget=forms.Textarea, required=True, label=u"Descripción")
    estado = forms.ChoiceField(choices=estados, label=u"Estado")
    page = forms.IntegerField()

    def getNombre(self):
        try:
            value = self.data["nombre"]
            return value
        except:
            return ""
    def getDescripcion(self):
        try:
            value = self.data["descripcion"]
            return value
        except:
            return ""
    def getEstado(self):
        try:
            value = self.data["estado"]
            return value
        except:
            return ""

    def __init__(self, *args, **kwargs):
        super(BuscadorRol, self).__init__(*args, **kwargs)
        self.fields['descripcion'].widget.attrs['rows'] = 3
        self.fields['descripcion'].widget.attrs['max_length'] = 1000
        self.fields['descripcion'].widget.attrs['style'] = "width: 100%"
        self.fields['nombre'].widget.attrs['style'] = "width: 100%"

        self.fields['estado'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['estado'].widget.attrs['data-style'] = "slc-primary"
        self.fields['estado'].widget.attrs['data-width'] = "100%"
        self.fields['estado'].widget.attrs['title'] = "Seleccione un estado"

        self.fields['page'].widget.attrs['style'] = "display: none"
