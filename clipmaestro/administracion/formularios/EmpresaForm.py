#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django.core.context_processors import request
from django.forms.util import ErrorList
import re
from django.core.exceptions import ValidationError
from contabilidad.models import *
from django import forms
from administracion.models import *
from librerias.funciones.validacion_rucs import *
from librerias.funciones.validacion_formularios import *

def validarRuc(value):
    if isEmpresaPublica(value)or isPersonaJuridica(value) or isPersonaNatural(value):
        return True
    else:
        raise ValidationError(u'Ruc o Cédula de la empresa inválida')

class ContadorForm(forms.Form):
    nombre_contador = forms.CharField(max_length=100, label=u"Nombre del Contador", validators=[validador_espacios])
    ruc_contador = forms.CharField(max_length=200, label=u"Ruc Contador", validators=[validador_espacios, validarRuc])
    telefono_contador = forms.CharField(max_length=220, label=u"Teléfono", validators=[validador_espacios])
    correo_contador = forms.EmailField(max_length=220, label=u"Correo", validators=[validador_espacios], required=False)
    direccion_contador = forms.CharField(max_length=220, label=u"Dirección", required=False)

    def getNombreContador(self):
        return self.data['nombre_contador']
    def getRucContador(self):
        return self.data['ruc_contador']
    def getTelefonoContador(self):
        return self.data['telefono_contador']
    def getCorreoContador(self):
        return self.data['correo_contador']
    def getDireccionContador(self):
        return self.data['direccion_contador']

    def __init__(self, *args, **kwargs):
        super(ContadorForm, self).__init__(*args, **kwargs)
        self.fields['telefono_contador'].widget.attrs['class'] = "numericos"
        self.fields['ruc_contador'].widget.attrs['class'] = "numericos"


class EmpresaForm(forms.Form):
    tipo_empresa_choice = ((1, 'Natural'), (2, 'Juridico'))
    rucci = forms.CharField(max_length=13, label=u"RUC/CI", validators=[validador_espacios, validarRuc])
    razon_social = forms.CharField(max_length=220, label=u"Razón Social", validators=[validador_espacios])
    tipo_empresa = forms.ChoiceField(choices=tipo_empresa_choice, label=u"Tipo de Empresa")
    nombre_comercial = forms.CharField(max_length=220, label=u"Nombre Comercial", validators=[validador_espacios])
    fecha_inicio_actividades = forms.DateTimeField(label=u"Fecha Inicio Actividad")
    contribuyente_especial = forms.BooleanField(label=u"Contribuyente Especial", required=False)
    num_resolucion = forms.CharField(max_length=100, label=u"# Resolución", required=False)
    representante_legal = forms.CharField(max_length=220, label=u"Representante Legal", validators=[validador_espacios])
    tipo_identificacion = forms.ChoiceField(choices=[], label=u"Tipo de Identificación")
    direccion = forms.CharField(max_length=220, label=u"Dirección", required=False)
    correo = forms.EmailField(max_length=220, label="Correo", required=False)
    telefono = forms.CharField(max_length=220, label=u"Teléfono", validators=[validador_espacios])
    actividad = forms.ChoiceField(choices=[], label=u"Actividad")
    logotipo = forms.ImageField(label=u"Logotipo", required=False)

    def __init__(self, *args, **kwargs):
        super(EmpresaForm, self).__init__(*args, **kwargs)
        self.fields['tipo_empresa'].widget.attrs['class']="selectpicker show-tick"
        self.fields['tipo_empresa'].widget.attrs['data-style']="slc-primary"
        self.fields['tipo_empresa'].widget.attrs['data-width']="100%"
        self.fields['tipo_empresa'].widget.attrs['data-height']="100%"

        self.fields['fecha_inicio_actividades'].widget.attrs['data-date-format']="yyyy-mm-dd"
        self.fields['fecha_inicio_actividades'].widget.attrs['class']="aaaa-mm-dd"

        self.fields['tipo_identificacion'].widget.attrs['class']="selectpicker show-tick"
        self.fields['tipo_identificacion'].widget.attrs['data-style']="slc-primary"
        self.fields['tipo_identificacion'].widget.attrs['data-width']="100%"
        self.fields['tipo_identificacion'].widget.attrs['data-height']="100%"
        self.fields['tipo_identificacion'].choices = [(x.codigo, x.codigo + " - " + x.descripcion) for x in Identificacion.objects.filter(status=1)]

        self.fields['contribuyente_especial'].widget.attrs['class'] = "checks"

        self.fields['actividad'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['actividad'].widget.attrs['data-style'] = "slc-primary"
        self.fields['actividad'].widget.attrs['data-width'] = "100%"
        self.fields['actividad'].widget.attrs['data-height'] = "100%"
        self.fields['actividad'].choices = [(x.id, x.descripcion) for x in ActividadEmpresa.objects.filter(status=1)]

        self.fields['telefono'].widget.attrs['placeholder'] = "042-000000"

        self.fields['rucci'].widget.attrs['class'] = "numericos"
        self.fields['rucci'].widget.attrs['readonly'] = True
        self.fields['razon_social'].widget.attrs['readonly'] = True

        self.fields['num_resolucion'].widget.attrs['class'] = "txt-100pc"
        self.fields['correo'].widget.attrs['placeholder'] = "empresa@dominio.com"

    def getRucci(self):
        return self.data['rucci']

    def getRazonSocial(self):
        return self.data['razon_social']

    def getTipoEmpresa(self):
        return self.data['tipo_empresa']

    def getNombreComercial(self):
        return self.data['nombre_comercial']

    def getFechaIniAct(self):
        return self.data['fecha_inicio_actividades']

    def getContribEspec(self):
        return self.cleaned_data['contribuyente_especial']

    def get_num_resolucion(self):
        return self.cleaned_data['num_resolucion']

    def getRepresentLegal(self):
        return self.data['representante_legal']

    def getTipoIdentific(self):
        return self.data['tipo_identificacion']

    def getDireccion(self):
        return self.data['direccion']

    def getCorreo(self):
        return self.data['correo']

    def getTelefono(self):
        return self.data['telefono']

    def getActividad(self):
        return self.data['actividad']

    def getLogotipo(self):
        return self.data['logotipo']


class UtilidadPerdidaForm(forms.Form):
    cuenta_utilidad_ejercicio = forms.ChoiceField(choices=[], label=u"Cuenta Utilidad del Ejercicio:", required=False)
    cuenta_perdida_ejercicio = forms.ChoiceField(choices=[], label=u"Cuenta Pérdida del Ejercicio:", required=False)

    def getPerdidaEjercicio(self):
        try:
            value = self.data['cuenta_perdida_ejercicio']
            return PlanCuenta.objects.get(id=value)
        except (PlanCuenta.DoesNotExist, ValueError):
            return None

    def getUtilidadEjericio(self):
        try:
            value = self.data['cuenta_utilidad_ejercicio']
            return PlanCuenta.objects.get(id=value)
        except (PlanCuenta.DoesNotExist, ValueError):
            return None

    def __init__(self, *args, **kwargs):
        super(UtilidadPerdidaForm, self).__init__(*args, **kwargs)
        # Tipo Cuenta Patrimonio
        q = PlanCuenta.objects.get(status=1, tipo_id=26)
        query = []

        for c in q.get_Hijos_nivel_5():
            query.append(c)

        self.fields['cuenta_utilidad_ejercicio'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_utilidad_ejercicio'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta_utilidad_ejercicio'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta_utilidad_ejercicio'].widget.attrs['data-height'] = "100%"
        self.fields['cuenta_utilidad_ejercicio'].choices = [("", "")]+[(x.id, x.codigo + " - " + x.descripcion) for x in query]
        self.fields['cuenta_perdida_ejercicio'].widget.attrs['class'] = "selectpicker show-tick"
        self.fields['cuenta_perdida_ejercicio'].widget.attrs['data-style'] = "slc-primary"
        self.fields['cuenta_perdida_ejercicio'].widget.attrs['data-width'] = "100%"
        self.fields['cuenta_perdida_ejercicio'].widget.attrs['data-height'] = "100%"
        self.fields['cuenta_perdida_ejercicio'].choices = [("", "")]+[(x.id, x.codigo + " - " + x.descripcion) for x in query]
