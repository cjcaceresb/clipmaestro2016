from django.conf import settings


def global_vars(request):
    return {'URL_MEDIA': settings.URL_MEDIA,
            'URL': settings.URL}