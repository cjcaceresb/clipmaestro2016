#! /usr/bin/python
# -*- coding: UTF-8 -*-
from django import template
from django.db.models import Count
from django.core.urlresolvers import reverse, resolve
from django.core.urlresolvers import RegexURLResolver, RegexURLPattern, Resolver404, get_resolver
from administracion.models import *
register = template.Library()
__author__ = 'Clip Maestro'


@register.simple_tag(name='verificar_rol_permiso')
def verificar_rol_permiso(rol_id, app_id, permisos):
    rol = Rol.objects.get(id=rol_id)
    app = Aplicaciones.objects.get(id=app_id)
    try:
        permiso = Rol_Aplicaciones.objects.get(aplicacion_id=app.id, rol_id=rol.id)
        if permisos == "agregar":
            if permiso.puede_agregar:
                return {"respuesta": True, "app": app, "permiso":permisos}
            else:
                return {"respuesta": False, "app": app, "permiso":permisos}
        elif permisos == "editar":
            if permiso.puede_editar:
                return {"respuesta": True, "app": app, "permiso":permisos}
            else:
                return {"respuesta": False, "app": app, "permiso":permisos}
        elif permisos == "eliminar":
            if permiso.puede_borrar:
                return {"respuesta": True, "app": app, "permiso":permisos}
            else:
                return {"respuesta": False, "app": app, "permiso":permisos}
    except:
        return {"respuesta": False, "app": app, "permiso": permisos}

register.inclusion_tag('tags/verificar_rol_permiso.html')(verificar_rol_permiso)
