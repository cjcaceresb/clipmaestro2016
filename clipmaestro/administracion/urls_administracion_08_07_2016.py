# -*- coding: UTF-8 -*-
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
)

#Administración Clip Maestro
urlpatterns += patterns('administracion.vistas.Empresa',
url('^configuracion_empresa/$', 'configuracion_empresa', name="configuracion_empresa"),
url('^404/$', 'cm_404', name="404"),
url('^500/$', 'cm_500', name="500")
)