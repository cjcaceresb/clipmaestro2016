# -*- coding: UTF-8 -*-

from librerias.funciones.validacion_formularios import *
from django.contrib import messages
from django.db.models import Q
import operator
from administracion.models import *
__author__ = 'Clip Maestro'


def busqueda_roles(buscador):
    """
    function busqueda_roles(buscador)
    @ Return CompraCabecera Object
    """
    predicates = [('nombre__icontains', (buscador.getNombre()))]
    predicates.append(('descripcion__icontains', buscador.getDescripcion()))

    if buscador.getEstado() != "":
        predicates.append(('status', buscador.getEstado()))

    # reate the list of Q objects and run the queries as above..
    q_list = [Q(x) for x in predicates]

    entries = Grupo.objects.exclude(status=0).filter(reduce(operator.and_, q_list)).order_by("-fecha_creacion")
    return entries


def llenar_lista_id_post(empresa, lista_id, request):
    """
    Llena la lista de todos los ids de los permisos que el usuario haya escojido
    :param empresa:
    :param grupo:
    :param lista_id:
    :param request:
    :return:
    """
    for g_modulos in empresa.get_modulos_empresa():
        for aplicaciones in g_modulos.get_app():
            for permisos in aplicaciones.get_permisos():
                if request.POST.get(str(permisos.id) + "-" + str(aplicaciones.id), "") != "":
                    lista_id.append(permisos.id)