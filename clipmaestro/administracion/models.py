# -*- coding: UTF-8 -*-
from django.db import models
import datetime
from django.contrib.auth.models import User
from contabilidad.models import *

# Create your models here.

class Metodo_Costo(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=200)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(default=datetime.datetime.now())
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "metodo_costo"

class ActividadEmpresa(models.Model):
    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=220)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(default=datetime.datetime.now())
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "empresa_actividad"

class EmpresaGeneral(models.Model):
    def url(self, filename):
        ruta	= str("empresa/%s" % filename)
        return ruta

    id = models.AutoField(primary_key=True)
    rucci = models.CharField(max_length=200)
    razon_social = models.CharField(max_length=220)
    tipo_empresa = models.IntegerField()
    nombre_comercial = models.CharField(max_length=220)
    fecha_inicio_actividades = models.DateField()
    contribuyente_especial = models.BooleanField(default=False)
    num_resolucion = models.CharField(max_length=100)

    representante_legal = models.CharField(max_length=220)
    tipo_identificacion = models.ForeignKey(Identificacion)
    direccion = models.CharField(max_length=220)
    correo = models.CharField(max_length=220)
    telefono = models.CharField(max_length=220)
    actividad = models.ForeignKey(ActividadEmpresa)

    nombre_contador = models.CharField(max_length=100)
    ruc_contador = models.CharField(max_length=200)
    telefono_contador = models.CharField(max_length=220, null=True)
    correo_contador = models.CharField(max_length=220, null=True)
    direccion_contador = models.CharField(max_length=220, null=True)

    logotipo = models.ImageField(upload_to=url, null=True, blank=True)

    nombre_bd = models.CharField(max_length=100)
    host_bd = models.CharField(max_length=100)
    port_bd = models.CharField(max_length=100)
    usuario_bd = models.CharField(max_length=100)
    pass_bd = models.CharField(max_length=100)
    subdominio = models.CharField(max_length=100)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(default=datetime.datetime.now())
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "empresa_general"

    def get_modulos_empresa(self):
        """
            Funcion que retorna los módulos
        """
        modulos = Modulo.objects.filter(moduloempresa__empresa=self, status=1)
        return modulos

class Empresa_Parametro(EmpresaGeneral):
    fecha_inicio_sistema = models.DateField()
    tipo_secuencia_diario = models.IntegerField()
    tipo_secuencia_ingresos = models.IntegerField()
    tipo_secuencia_egresos = models.IntegerField()
    tipo_secuencia_compra = models.IntegerField()
    tipo_secuencia_venta = models.IntegerField()
    tipo_secuencia_inventario = models.IntegerField()
    tipo_secuencia_retencion = models.IntegerField()
    tipo_secuencia_cheque = models.IntegerField()
    tipo_secuencia_documento_venta = models.IntegerField()
    tipo_secuencia_liq_compra = models.IntegerField()
    metodo_costo = models.ForeignKey(Metodo_Costo)
    fecha_ultimo_ingreso = models.DateField()
    fecha_ultimo_egreso = models.DateField()
    fecha_ultimo_diario = models.DateField()
    fecha_ultimo_compra = models.DateField()
    fecha_ultimo_inventario = models.DateField()
    fecha_cierre = models.DateField()
    mes_cierre = models.IntegerField()
    anio_cierre = models.IntegerField()
    stock = models.BooleanField()
    doc_electronico = models.BooleanField()
    apikey = models.CharField(max_length=100)
    clave_api = models.CharField(max_length=100)
    height_logo = models.IntegerField(default=100)
    width_logo = models.IntegerField(default=300)
    num_filas_template = models.IntegerField(blank=True, null=True)

    # DATOS DEL SERVIDOR JASPER
    path_jasper = models.CharField(max_length=255)
    user_jasper = models.CharField(max_length=255)
    pass_jasper = models.CharField(max_length=255)

    ################ Parametros Adicionales de la venta #####################
    tipo_venta = models.IntegerField()                             # Modalidad o tipo de venta que se realiza
    modifica_precio_venta = models.BooleanField(default=True)      # Si se le permite al vendedor modificar el precio durante la venta

    plan_cuenta_utilidad = models.ForeignKey(PlanCuenta, related_name="plan_cuenta_utilidad_id", db_column="plan_cuenta_utilidad_id", blank=True)
    plan_cuenta_perdida = models.ForeignKey(PlanCuenta, related_name="plan_cuenta_perdida_id", db_column="plan_cuenta_perdida_id", blank=True)


    class Meta:
        db_table = "empresa_parametro"


class Empresa(models.Model):
    id = models.AutoField(primary_key=True)
    empresa_general = models.ForeignKey(EmpresaGeneral)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(default=datetime.datetime.now())
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "empresa"

    def get_empresa_general(self):
        return EmpresaGeneral.objects.get(id=self.empresa_general_id)

class Modulo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=220)
    descripcion = models.TextField()
    status = models.IntegerField(default=1)

    fecha_creacion = models.DateField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateField(default=datetime.datetime.now())
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    def get_app(self):
        return Aplicacion.objects.using("base_central").filter(modulo=self)

    class Meta:
        db_table = "modulo"

class Aplicacion(models.Model):
    id = models.AutoField(primary_key=True)
    modulo = models.ForeignKey(Modulo)
    nombre = models.CharField(max_length=220)
    descripcion = models.TextField()
    codigo = models.CharField(max_length=80)
    imagen = models.CharField(max_length=400, null=True, blank=True)
    url = models.CharField(max_length=400, null=True, blank=True)
    status = models.IntegerField(default=1)

    fecha_creacion = models.DateField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateField(default=datetime.datetime.now(), null=True)
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    def get_permisos(self):
        return Permisos.objects.filter(aplicacion=self)

    class Meta:
        db_table = "aplicacion"

class ModuloEmpresa(models.Model):
    id = models.AutoField(primary_key=True)
    empresa = models.ForeignKey(Empresa)
    modulo = models.ForeignKey(Modulo)
    status = models.IntegerField(default=1)

    fecha_creacion = models.DateField(default=datetime.datetime.now(), null=True)
    fecha_actualizacion = models.DateField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "modulo_empresa"


class Permisos(models.Model):
    id = models.AutoField(primary_key=True)
    aplicacion = models.ForeignKey(Aplicacion)
    nombre = models.CharField(max_length=50)
    etiqueta = models.CharField(max_length=100)
    nombre_reporte = models.CharField(max_length=100)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateTimeField(default=datetime.datetime.now(), null=True)
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "permisos"

class Grupo(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=220)
    descripcion = models.CharField(max_length=400)
    permisos = models.ManyToManyField(Permisos, related_name="grupo_permisos")

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateField(default=datetime.datetime.now())
    fecha_actualizacion = models.DateField(default=datetime.datetime.now())
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "grupo"

    def get_lista_permisos_id(self):
        lista = []
        for obj in self.permisos.values_list('id'):
            lista.append(int(obj[0]))
        return lista


class Persona(User):
    def url(self, filename):
        ruta = str("usuarios/%s" % filename)
        return ruta
    empresa = models.ForeignKey(Empresa)
    grupo = models.ManyToManyField(Grupo, related_name="persona_grupo")
    permisos = models.ManyToManyField(Permisos, related_name="persona_permisos")

    cedula              = models.CharField(max_length=20, null=True)
    nombres_completos   = models.TextField()
    apellidos_completos = models.TextField(null=True)
    segundo_nombre      = models.CharField(max_length=220)
    segundo_apellido    = models.CharField(max_length=220)
    sexo                = models.CharField(max_length=220)
    fotografia          = models.ImageField(upload_to=url, verbose_name="Imagen", null=True)
    fecha_nacimiento    = models.DateField(null=True)
    estado_civil        = models.CharField(max_length=220, null=True)
    facebook            = models.CharField(max_length=220, null=True)
    sitio_web            = models.CharField(max_length=220, null=True)
    twitter             = models.CharField(max_length=220, null=True)
    direccion           = models.CharField(max_length=220, null=True)
    telefono            = models.CharField(max_length=220, null=True)
    es_admin            = models.BooleanField(default=False)
    last_ip             = models.GenericIPAddressField(null=True)

    estado              = models.IntegerField(default=1)
    fecha_creacion  =   models.DateTimeField(default=datetime.datetime.now())
    fecha_actualizacion  =   models.DateTimeField(default=datetime.datetime.now())
    usuario_creacion = models.CharField(max_length=50)
    usuario_actualizacion = models.CharField(max_length=50)

    class Meta:
        db_table = "persona"
        verbose_name = "Persona"
        verbose_name_plural = "Personas"

    def get_grupos(self):
        return self.grupo.filter(status=1)


class Cheque(models.Model):
    '''
        Modelo que se encarga de estructurar el cheque
        a imprimir y los atributos informativos del documento
        deben ser guardados con "," para representar las coordenadas
        (x,y) donde se pintarán en el documento.
        ej:fecha = (15,25.5)
    '''
    id = models.AutoField(primary_key=True)
    empresa = models.ForeignKey(EmpresaGeneral)
    cuenta_banco = models.ForeignKey(Cuenta_Banco)
    alto = models.FloatField()
    ancho = models.FloatField()
    ####### DATOS INFORMATIVOS ######################
    persona = models.CharField(max_length=15)
    fecha = models.CharField(max_length=15)
    total = models.CharField(max_length=15)
    total_palabras = models.CharField(max_length=15)
    lugar = models.CharField(max_length=15, null=True)
    fecha_emitido = models.CharField(max_length=15, null=True)
    #################################################

    class Meta:
        db_table = "cheque"

class Grupos_Contables(models.Model):

    id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=255)

    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now(), null=True)
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "grupos_contables"


class Reportes_Jasper(models.Model):

    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=200)
    nombre = models.CharField(max_length=100)
    ruta = models.CharField(max_length=200)


    status = models.IntegerField(default=1)
    fecha_creacion = models.DateTimeField(default=datetime.datetime.now(), null=True)
    fecha_actualizacion = models.DateTimeField(null=True)
    usuario_creacion = models.CharField(max_length=50, null=True)
    usuario_actualizacion = models.CharField(max_length=50, null=True)

    class Meta:
        db_table = "reportes_jasper"
        verbose_name = "Reporte Jasper"
        verbose_name_plural = "Reportes Jasper"


class Config_Jasper(models.Model):

    id = models.AutoField(primary_key=True)
    url = models.CharField(max_length=100)

    class Meta:
        db_table = "config_jasper"
        verbose_name = "Config Jasper"
        verbose_name_plural = "Configs Jasper"