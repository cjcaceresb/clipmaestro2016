# -*- coding: UTF-8 -*-
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin


admin.autodiscover()

urlpatterns = patterns('',
)

#Reportes Compras
urlpatterns += patterns('reportes.vistas.reporte_compras',
url('^reportes/compras/reporte_compras/$', 'reporte_compras_por_fecha', name="reporte_compras_por_fecha"),
url('^reportes/reporte_compras_pdf/$', 'reporte_compras_por_fecha_pdf', name="reporte_compras_por_fecha_pdf"),
url('^reportes/compras/reporte_compras_excel$', 'reporte_compras_por_fecha_excel', name="reporte_compras_por_fecha_excel"),
url('^reportes/compras/saldo_cuentas_pagar/$', 'reporte_saldo_ctas_x_pagar', name="reporte_saldo_ctas_x_pagar"),
url('^reportes/compras/reporte_compras_proveedor/$', 'reporte_compras_por_fecha_proveedor', name="reporte_compras_por_fecha_proveedor"),
url('^reportes/compras/reporte_cartera_compras/$', 'reporte_cartera_compras', name="reporte_cartera_compras"),
url('^reportes/compras/reporte_cartera_compras_excel/$', 'reporte_cartera_compras_excel', name="reporte_cartera_compras_excel"),

url('^reportes/compras/reporte_compras_x_proveedor$', 'reporte_compras_x_proveedor', name="reporte_compras_x_proveedor"),
url('^reportes/compras/reporte_compras_x_proveedor_excel', 'reporte_compras_x_proveedor_excel', name="reporte_compras_x_proveedor_excel"),
url('^reportes/compras/reporte_compras_x_cuenta$', 'reporte_compras_x_cuenta', name="reporte_compras_x_cuenta"),
url('^reportes/compras/reporte_compras_x_cuenta_excel', 'reporte_compras_x_cuenta_excel', name="reporte_compras_x_cuenta_excel"),
url('^reportes/compras/reporte_estad_compras_x_proveedor$', 'reporte_estadistica_compras_x_proveedor', name="reporte_estadistica_compras_x_proveedor"),
url('^reportes/compras/reporte_estad_compras_x_proveedor_excel', 'reporte_estadistica_compras_x_proveedor_excel', name="reporte_estadistica_compras_x_proveedor_excel"),

)

#Reportes Ventas
urlpatterns += patterns('reportes.vistas.reporte_ventas',
url('^reportes/reporte_ventas/$', 'reporte_venta_por_fecha', name="reporte_venta_por_fecha"),
url('^reportes/reporte_ventas_excel/$', 'reporte_venta_por_fecha_excel', name="reporte_venta_por_fecha_excel"),
url('^reportes/reporte_ventas_pdf/$', 'reporte_venta_por_fecha_pdf', name="reporte_venta_por_fecha_pdf"),

url('^reportes/reporte_ventas_cliente/$', 'reporte_ventas_cliente', name="reporte_ventas_cliente"),
url('^reportes/reporte_ventas_cliente/pdf/$', 'reporte_ventas_cliente_pdf', name="reporte_ventas_cliente_pdf"),
url('^reportes/reporte_ventas_clientes/excel/$', 'reporte_ventas_clientes_excel', name="reporte_ventas_clientes_excel"),

url('^reportes/reporte_ventas_item/$', 'reporte_ventas_item', name="reporte_ventas_item"),
url('^reportes/reporte_ventas_item/pdf/$', 'reporte_ventas_item_pdf', name="reporte_ventas_item_pdf"),
url('^reportes/reporte_ventas_item/excel/$', 'reporte_ventas_item_excel', name="reporte_ventas_item_excel"),

url('^reportes/reporte_ventas/pendientes/$', 'reporte_ventas_pendientes', name="reporte_ventas_pendientes"),
url('^reportes/reporte_ventas/pendientes/excel/$', 'reporte_ventas_pendientes_excel', name="reporte_ventas_pendientes_excel"),
url('^reportes/reporte_ventas/pendientes/pdf/$', 'reporte_ventas_pendientes_pdf', name="reporte_ventas_pendientes_pdf"),

url('^reportes/reporte_ventas/contado_credito/$', 'reporte_ventas_contado_credito_fecha', name="reporte_ventas_contado_credito_fecha"),
url('^reportes/reporte_ventas/contado_credito/excel/$', 'reporte_ventas_contado_credito_fecha_excel', name="reporte_ventas_contado_credito_fecha_excel"),
url('^reportes/reporte_ventas/contado_credito/pdf/$', 'reporte_ventas_contado_credito_fecha_pdf', name="reporte_ventas_contado_credito_fecha_pdf"),
url('^reportes/reporte_ventas/reporte_cartera_ventas/$', 'reporte_cartera_ventas', name="reporte_cartera_ventas"),
url('^reportes/reporte_ventas/reporte_comisiones_excel/$', 'reporte_comisiones_excel', name="reporte_comisiones_excel"),
url('^reportes/reporte_ventas/reporte_venta_retencion_control/$', 'reporte_venta_retencion_control', name="reporte_venta_retencion_control"),
url('^reportes/reporte_ventas/reporte_venta_retencion_control_excel/$', 'reporte_venta_retencion_control_excel', name="reporte_venta_retencion_control_excel"),
url('^reportes/reporte_ventas/reporte_cartera_ventas_excel/$', 'reporte_cartera_ventas_excel', name="reporte_cartera_ventas_excel"),

url('^reportes/reporte_ventas/reporte_comisiones/$', 'reporte_comisiones', name="reporte_comisiones"),
url('^reportes/reporte_ventas/reporte_estad_ventas_x_cliente$', 'reporte_estadistica_ventas_x_cliente', name="reporte_estadistica_ventas_x_cliente")


)

# Reporte Proveedores
urlpatterns += patterns('reportes.vistas.reporte_proveedores',
url('^reportes/proveedores/reporte_monto_proveedor$', 'reporte_monto_proveedor_por_fecha', name="reporte_monto_proveedor_por_fecha"),
url('^reportes/proveedores/reporte_monto_proveedor_pdf$', 'reporte_monto_proveedor_por_fecha_pdf', name="reporte_monto_proveedor_por_fecha_pdf"),
url('^reportes/proveedores/reporte_monto_proveedor_excel$', 'reporte_monto_proveedor_por_fecha_excel', name="reporte_monto_proveedor_por_fecha_excel"),
url('^reportes/proveedores/reporte_proveedores', 'reporte_proveedores', name="reporte_proveedores"),
url('^reportes/proveedores/reporte_proveedor_excel$', 'reporte_proveedores_excel', name="reporte_proveedores_excel"),
url('^reportes/proveedores/reporte_proveedor_pdf$', 'reporte_proveedores_pdf', name="reporte_proveedores_pdf"),
url('^reportes/proveedores/reporte_cuentas_por_pagar_pendientes$', 'reporte_cuentas_por_pagar_pendientes', name="reporte_cuentas_por_pagar_pendientes"),
url('^reportes/proveedores/reporte_cuentas_por_pagar_pendientes_pdf$', 'reporte_cuentas_por_pagar_pendientes_pdf', name="reporte_cuentas_por_pagar_pendientes_pdf"),
url('^reportes/proveedores/reporte_cuentas_por_pagar_pendientes_excel$', 'reporte_cuentas_por_pagar_pendientes_excel', name="reporte_cuentas_por_pagar_pendientes_excel"),
url('^reportes/proveedores/reporte_saldos_por_proveedores$', 'reporte_saldos_por_proveedores', name="reporte_saldos_por_proveedores"),
url('^reportes/proveedores/reporte_saldos_por_proveedores_pdf$', 'reporte_saldos_por_proveedores_pdf', name="reporte_saldos_por_proveedores_pdf"),
url('^reportes/proveedores/reporte_saldos_por_proveedores_excel$', 'reporte_saldos_por_proveedores_excel', name="reporte_saldos_por_proveedores_excel")
)

# Reporte Cliente
urlpatterns += patterns('reportes.vistas.reporte_clientes',
url('^reportes/clientes/reporte_monto_cliente$', 'reporte_monto_cliente_por_fecha', name="reporte_monto_cliente_por_fecha"),
url('^reportes/clientes/reporte_monto_cliente_pdf$', 'reporte_monto_cliente_por_fecha_pdf', name="reporte_monto_cliente_por_fecha_pdf"),
url('^reportes/clientes/reporte_monto_cliente_excel$', 'reporte_monto_cliente_por_fecha_excel', name="reporte_monto_cliente_por_fecha_excel"),
url('^reportes/clientes/reporte_clientes$', 'reporte_clientes', name="reporte_clientes"),
url('^reportes/clientes/reporte_cliente_pdf$', 'reporte_clientes_pdf', name="reporte_clientes_pdf"),
url('^reportes/clientes/reporte_cliente_excel$', 'reporte_clientes_excel', name="reporte_clientes_excel"),
url('^reportes/clientes/reporte_cuentas_por_cobrar_pendientes$', 'reporte_cuentas_por_cobrar_pendientes', name="reporte_cuentas_por_cobrar_pendientes"),
url('^reportes/clientes/reporte_cuentas_por_cobrar_pendientes_pdf$', 'reporte_cuentas_por_cobrar_pendientes_pdf', name="reporte_cuentas_por_cobrar_pendientes_pdf"),
url('^reportes/clientes/reporte_cuentas_por_cobrar_pendientes_excel$', 'reporte_cuentas_por_cobrar_pendientes_excel', name="reporte_cuentas_por_cobrar_pendientes_excel"),
url('^reportes/clientes/reporte_saldos_por_clientes$', 'reporte_saldos_por_clientes', name="reporte_saldos_por_clientes"),
url('^reportes/clientes/reporte_saldos_por_clientes_pdf$', 'reporte_saldos_por_clientes_pdf', name="reporte_saldos_por_clientes_pdf"),
url('^reportes/clientes/reporte_saldos_por_clientes_excel$', 'reporte_saldos_por_clientes_excel', name="reporte_saldos_por_clientes_excel"),
)

# Reporte Bancos
urlpatterns += patterns('reportes.vistas.reporte_bancos',
url('^reportes/bancos/reporte_saldo_bancos/$', 'reporte_saldo_bancos_por_fecha', name="reporte_saldo_bancos_por_fecha"),
url('^reportes/bancos/reporte_saldo_bancos_pdf/$', 'reporte_saldo_bancos_por_fecha_pdf', name="reporte_saldo_bancos_por_fecha_pdf"),
url('^reportes/bancos/reporte_saldo_bancos_excel/$', 'reporte_saldo_bancos_por_fecha_excel', name="reporte_saldo_bancos_por_fecha_excel"),
url('^reportes/bancos/reporte_lista_cheques_girados/$', 'reporte_listado_cheques_girados', name="reporte_listado_cheques_girados"),
url('^reportes/bancos/reporte_listado_cheques_girados_pdf/$', 'reporte_listado_cheques_girados_pdf', name="reporte_listado_cheques_girados_pdf"),
url('^reportes/bancos/reporte_lista_cheques_girados_excel/$', 'reporte_listado_cheques_girados_excel', name="reporte_listado_cheques_girados_excel"),
url('^reportes/bancos/reporte_lista_movimientos_bancarios/$', 'reporte_movimientos_bancarios', name="reporte_movimientos_bancarios"),
url('^reportes/bancos/reporte_lista_movimientos_bancarios_pdf/$', 'reporte_movimientos_bancarios_pdf', name="reporte_movimientos_bancarios_pdf"),
url('^reportes/bancos/reporte_lista_movimientos_bancarios_excel/$', 'reporte_movimientos_bancarios_excel', name="reporte_movimientos_bancarios_excel"),
url('^reportes/bancos/reporte_transacciones_bancarias_por_conciliar/$', 'reporte_transacciones_bancarias_por_conciliar', name="reporte_transacciones_bancarias_por_conciliar"),
url('^reportes/bancos/reporte_transacciones_bancarias_por_conciliar_pdf/$', 'reporte_transacciones_bancarias_por_conciliar_pdf', name="reporte_transacciones_bancarias_por_conciliar_pdf"),
url('^reportes/bancos/reporte_transacciones_bancarias_por_conciliar_excel/$', 'reporte_transacciones_bancarias_por_conciliar_excel', name="reporte_transacciones_bancarias_por_conciliar_excel")

)

# Reportes Inventario
urlpatterns += patterns('reportes.vistas.reporte_inventario',
url('^reportes/$', 'reporte_inicio', name="reporte_inicio"),

url('^reportes/inventario/saldo_item/$', 'reporte_saldo_por_item', name="reporte_saldo_por_item"),
url('^reportes/inventario/saldo_item_pdf/$', 'reporte_saldo_por_item_pdf', name="reporte_saldo_por_item_pdf"),
url('^reportes/inventario/saldo_item_excel/$', 'reporte_saldo_por_item_excel', name="reporte_saldo_por_item_excel"),
url('^reportes/inventario/kardex_item/$', 'reporte_kardex_por_item', name="reporte_kardex_por_item"),
url('^reportes/inventario/kardex_item_pdf/$', 'reporte_kardex_item_pdf', name="reporte_kardex_item_pdf"),
url('^reportes/inventario/kardex_item_excel/$', 'reporte_kardex_por_item_excel', name="reporte_kardex_por_item_excel"),
url('^reportes/inventario/movim_invent_motivo/$', 'reporte_movim_inventario_motivo', name="reporte_movim_inventario_motivo"),
url('^reportes/inventario/movim_invent_motivo_excel/$', 'reporte_movim_inventario_motivo_excel', name="reporte_movim_inventario_motivo_excel"),
url('^reportes/inventario/reporte_items/$', 'reporte_items', name="reporte_items"),
url('^reportes/inventario/reporte_items_pdf/$', 'reporte_items_pdf', name="reporte_items_pdf"),
url('^reportes/inventario/reporte_items_excel/$', 'reporte_items_excel', name="reporte_items_excel"),
)


#Reportes Financieros
urlpatterns += patterns('reportes.vistas.reporte_estados_financieros',
    url('^reportes/financiero/balance_general$', 'balance_general', name="balance_general"),
    url('^reportes/financiero/balance_general_pdf$', 'balance_general_pdf', name="balance_general_pdf"),
    url('^reportes/financiero/balance_general_excel$', 'balance_general_excel', name="balance_general_excel"),
    url('^reportes/financiero/balance_general_detallado$', 'balance_general_detallado', name="balance_general_detallado"),
    url('^reportes/financiero/balance_general_detallado_pdf$', 'balance_general_detallado_pdf', name="balance_general_detallado_pdf"),
    url('^reportes/financiero/balance_general_detallado_excel$', 'balance_general_detallado_excel', name="balance_general_detallado_excel"),
    url('^reportes/financiero/perdidas_ganancias$', 'reporte_perdidas_ganacias', name="reporte_perdidas_ganacias"),
    url('^reportes/financiero/perdidas_ganancias_pdf$', 'reporte_perdidas_ganacias_pdf', name="reporte_perdidas_ganacias_pdf"),
    url('^reportes/financiero/perdidas_ganancias_excel$', 'reporte_perdidas_ganancias_excel', name="reporte_perdidas_ganancias_excel"),
    url('^reportes/financiero/perdidas_ganancias_detallado$', 'reporte_perdidas_ganacias_detallado', name="reporte_perdidas_ganacias_detallado"),
    url('^reportes/financiero/perdidas_ganancias_detallado_pdf$', 'reporte_perdidas_ganacias_detallado_pdf', name="reporte_perdidas_ganacias_detallado_pdf"),
    url('^reportes/financiero/perdidas_ganancias_detallado_excel$', 'reporte_perdidas_ganacias_detallado_excel', name="reporte_perdidas_ganacias_detallado_excel"),
    url('^reportes/financiero/comparativo_estado_resultado$', 'comparativo_estado_resultado', name="comparativo_estado_resultado"),
    url('^reportes/financiero/balance_general_o_PIG$', 'balance_general_o_PIG', name="balance_general_o_PIG"),
    url('^reportes/financiero/balance_general_o_PIG_excel$', 'balance_general_o_PIG_excel', name="balance_general_o_PIG_excel"),
    url('^reportes/financiero/balance_general_o_PIG_pdf$', 'balance_general_o_PIG_pdf', name="balance_general_o_PIG_pdf")

)

# Reportes de Contabilidad
urlpatterns += patterns('reportes.vistas.reporte_contabilidad',
    url('^reportes/contabilidad/libro_diario/$', 'libro_diario', name="libro_diario"),
    url('^reportes/contabilidad/libro_diario/(?P<id>\d+)/$', 'libro_diario_pdf', name="libro_diario_pdf"),
    url('^reportes/contabilidad/104/$', 'reporte_104', name="reporte_104"),
    url('^reportes/contabilidad/104_pdf/$', 'reporte_104_pdf', name="reporte_104_pdf"),
    url('^reportes/contabilidad/104_excel/$', 'reporte_104_excel', name="reporte_104_excel"),
    url('^reportes/contabilidad/103/$', 'reporte_103', name="reporte_103"),
    url('^reportes/contabilidad/103_pdf/$', 'reporte_103_pdf', name="reporte_103_pdf"),
    url('^reportes/contabilidad/103_excel/$', 'reporte_103_excel', name="reporte_103_excel"),
    url('^reportes/contabilidad/libro_mayor/$', 'reporte_libro_mayor', name="reporte_libro_mayor"),
    url('^reportes/contabilidad/libro_mayor_pdf/$', 'reporte_libro_mayor_pdf', name="reporte_libro_mayor_pdf"),
    url('^reportes/contabilidad/libro_mayor_excel/$', 'reporte_libro_mayor_excel', name="reporte_libro_mayor_excel"),

    url('^reportes/contabilidad/libro_mayor_centro_costo/$', 'reporte_libro_mayor_centro_costo', name="reporte_libro_mayor_centro_costo"),
    url('^reportes/contabilidad/libro_mayor_centro_costo_excel/$', 'reporte_libro_mayor_centro_costo_excel', name="reporte_libro_mayor_centro_costo_excel"),
    url('^reportes/contabilidad/libro_mayor_centro_costo_pdf/$', 'reporte_libro_mayor_centro_costo_pdf', name="reporte_libro_mayor_centro_costo_pdf"),

    url('^reportes/contabilidad/balance_comprobacion/$', 'reporte_balance_comprobacion', name="reporte_balance_comprobacion"),
    url('^reportes/contabilidad/balance_comprobacion_pdf/$', 'reporte_balance_comprobacion_pdf', name="reporte_balance_comprobacion_pdf"),
    url('^reportes/contabilidad/balance_comprobacion_excel/$', 'reporte_balance_comprobacion_excel', name="reporte_balance_comprobacion_excel"),
    url('^reportes/contabilidad/reporte_saldos_cuentas/$', 'reporte_saldos_cuentas', name="reporte_saldos_cuentas"),
    url('^reportes/contabilidad/reporte_saldos_cuentas_pdf/$', 'reporte_saldos_cuentas_pdf', name="reporte_saldos_cuentas_pdf"),
    url('^reportes/contabilidad/reporte_saldos_cuentas_excel/$', 'reporte_saldos_cuentas_excel', name="reporte_saldos_cuentas_excel"),
    url('^reportes/contabilidad/reporte_codigo_formulario_sri/$', 'reporte_codigo_formulario_sri', name="reporte_codigo_formulario_sri"),
    url('^reportes/contabilidad/reporte_codigo_formulario_sri_pdf/$', 'reporte_codigo_formulario_sri_pdf', name="reporte_codigo_formulario_sri_pdf"),
    url('^reportes/contabilidad/reporte_codigo_formulario_sri_excel/$', 'reporte_codigo_formulario_sri_excel', name="reporte_codigo_formulario_sri_excel"),

    url('^reportes/contabilidad/reporte_comprobantes_anulados/$', 'reporte_comprobantes_anulados', name="reporte_comprobantes_anulados"),
    url('^reportes/contabilidad/reporte_comprobantes_anulados/pdf/$', 'reporte_comprobantes_anulados_pdf', name="reporte_comprobantes_anulados_pdf"),
    url('^reportes/contabilidad/reporte_comprobantes_anulados/excel/$', 'reporte_comprobantes_anulados_por_fecha_excel', name="reporte_comprobantes_anulados_por_fecha_excel"),

    url('^reportes/contabilidad/reporte_proyectado_vs_real/$', 'reporte_proyectado_vs_real', name="reporte_proyectado_vs_real"),

    url('^reportes/contabilidad/reporte_verificacion_modulo_contabilidad/$', 'reporte_verificacion_modulo_contabilidad', name="reporte_verificacion_modulo_contabilidad"),

)


urlpatterns += patterns('reportes.vistas.reporte_cuentas',
url('^reportes/reporte_cuentas_lista/$', 'reporte_cuentas_lista', name="reporte_cuentas_lista"),
url('^reportes/reporte_cuentas_lista/buscar_cuenta/$', 'buscar_cuenta', name="buscar_cuenta"),
url('^reportes/reporte_cuenta_fechas/(?P<id>\d+)/$', 'reporte_cuenta_fechas', name="reporte_cuenta_fechas"),
url('^reportes/reporte_cuenta_fechas_pdf/$', 'reporte_cuenta_fechas_pdf', name="reporte_cuenta_fechas_pdf"),
url('^reportes/reporte_cuenta_fechas_excel/$', 'reporte_cuenta_fechas_excel', name="reporte_cuenta_fechas_excel"),

)

urlpatterns += patterns('reportes.vistas.reporte_retencion',
url('^reportes/reportes_retencion_fuente$', 'reporte_retencion_fuente', name="reporte_retencion_fuente"),
url('^reportes/reportes_retenciones/buscar_proveedor_retencion/$', 'buscar_proveedor_retencion', name="buscar_proveedor_retencion"),
url('^reportes/reporte_retencion_fuente_proveedor_fechas_excel/$', 'reporte_retencion_fuente_proveedor_fechas_excel', name="reporte_retencion_fuente_proveedor_fechas_excel"),
url('^reportes/reporte_retencion_fuente_pdf/$', 'reporte_retencion_fuente_pdf', name="reporte_retencion_fuente_pdf"),

)

#Proyectos
urlpatterns += patterns('reportes.vistas.reporte_contratos',
url('^reportes/reporte_lista_contratos_fechas/$', 'reporte_listado_contratos_fechas', name="reporte_listado_contratos_fechas"),
url('^reportes/reporte_listado_contratos_fechas_excel/$', 'reporte_listado_contratos_fechas_excel', name="reporte_listado_contratos_fechas_excel"),
url('^reportes/reporte_listado_contratos_fechas_pdf/$', 'reporte_listado_contratos_fechas_pdf', name="reporte_listado_contratos_fechas_pdf"),
url('^reportes/reporte_contrato_detalle/(?P<id>\d+)/$', 'reporte_contrato_detalle', name="reporte_contrato_detalle"),
url('^reportes/print_contrato_detalle/(?P<id>\d+)/$', 'print_contrato_detalle', name="print_contrato_detalle"),
url('^reportes/reporte_contrato_detalle_pdf/(?P<id>\d+)/$', 'reporte_contrato_detalle_pdf', name="reporte_contrato_detalle_pdf"),
url('^reportes/reporte_contrato_detalle_excel/(?P<id>\d+)/$', 'reporte_contrato_detalle_excel', name="reporte_contrato_detalle_excel"),

)