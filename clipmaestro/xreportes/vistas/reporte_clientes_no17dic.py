#! /usr/bin/python
# -*- coding: UTF-8-*-
import requests
from administracion.models import Empresa

__author__ = 'Clip Maestro'

from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from reportes.formularios.reportes_form import *
from django.db import connection
from django.db.models import Sum
import operator
from django.db.models import F
from django.forms.formsets import formset_factory
from librerias.funciones.funciones_vistas import generar_pdf
from librerias.funciones.excel_response_mod import *
from librerias.funciones.permisos import *
from reportes.vistas.funciones import get_monto_cobrado_fecha_func, get_monto_saldo_cxc_fecha_func


def get_saldo_client(cliente, fecha_ini):
    """
    Función que retorna el saldo del proveedor dependiendo de una fecha
    específica
    :param cliente:
    :param fecha_ini:
    :return: float
    """

    monto_deudor1 = Cuentas_por_Cobrar.objects.filter(cliente=cliente, naturaleza=1,
                                                      fecha_reg__lt=fecha_ini,
                                                      status=1).aggregate(Sum('monto', field="monto"))["monto__sum"]
    monto_acreedor1 = Cuentas_por_Cobrar.objects.filter(cliente=cliente, naturaleza=2,
                                                        fecha_reg__lt=fecha_ini,
                                                        status=1).aggregate(Sum('monto', field="monto"))["monto__sum"]

    monto_deudor2 = CXC_cobro.objects.exclude(cobro__num_comp=F("cuentas_x_cobrar__num_doc")).filter(
        cobro__cliente=cliente,
        cuentas_x_cobrar__naturaleza=2,
        cobro__fecha_reg__lt=fecha_ini,
        status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"]

    monto_acreedor2 = CXC_cobro.objects.exclude(cobro__num_comp=F("cuentas_x_cobrar__num_doc")).filter(
        cobro__cliente=cliente, cuentas_x_cobrar__naturaleza=1,
        cobro__fecha_reg__lt=fecha_ini,
        status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"]

    if monto_deudor1 is None:
        monto_deudor1 = 0.0
    else:
        monto_deudor1 = float(monto_deudor1)

    if monto_deudor2 is None:
        monto_deudor2 = 0.0
    else:
        monto_deudor2 = float(monto_deudor2)

    if monto_acreedor1 is None:
        monto_acreedor1 = 0.0
    else:
        monto_acreedor1 = float(monto_acreedor1)

    if monto_acreedor2 is None:
        monto_acreedor2 = 0.0
    else:
        monto_acreedor2 = float(monto_acreedor2)

    return round(((monto_deudor1 + monto_deudor2) - (monto_acreedor1 + monto_acreedor2)), 2)


class ListRepClient():
    """
    Clase que me ayuda a agrupar las cuentas por pagar
    y los pagos, esto para el reporte de saldo de
    proveedores
    """
    def __init__(self, tipo, fecha_reg, num_doc):
        self.tipo = tipo
        self.fecha_reg = fecha_reg
        self.num_doc = num_doc
        self.cuentas_x_cobrar = Cuentas_por_Cobrar()
        self.cxc_cobro = CXC_cobro()
        self.saldo = 0.0


class DatosClientSaldos():
    """
    Clase que me ayuda a agrupar el saldo de cada cliente
    ademas de los datos del cliente y la lista de las transacciones
    """
    def __init__(self, saldo):
        self.saldo = saldo
        self.lista_saldo = []
        self.cliente = Cliente()
        self.saldo_deudor = 0.0
        self.saldo_acreedor = 0.0
        self.total_saldo_pendiente = 0.0


def ord_list_fecha(cliente, fecha_ini, fecha_final, list_rep, saldo_ini):
    """
    Función que ordena la clase ListRepProv, por un
    rango de fechas
    :param cliente:
    :param fecha_ini:
    :param fecha_final:
    :param list:
    :return:
    """

    saldo_deudor = 0.0
    saldo_acreedor = 0.0
    cta_cob = Cuentas_por_Cobrar.objects.filter(cliente=cliente,
                                                fecha_reg__range=(fecha_ini, fecha_final),
                                                status=1).order_by("fecha_reg")

    cobros = Cobro.objects.filter(cliente=cliente,
                                  fecha_reg__range=(fecha_ini, fecha_final),
                                  status=1).order_by("fecha_reg")

    for obj in cta_cob:
        list_rep_client = ListRepClient(1, obj.fecha_reg, obj.num_doc)
        list_rep_client.cuentas_x_cobrar = obj
        list_rep.append(list_rep_client)

    for obj in cobros:
        for obj_cxc_cobro in obj.getCxPcobro():
            # Se valida que el pago no sea el del anticipo, si no hubiera duplicados
            if obj_cxc_cobro.cuentas_x_cobrar.num_doc != obj_cxc_cobro.cobro.num_comp:
                list_rep_client = ListRepClient(2, obj.fecha_reg, obj_cxc_cobro.cuentas_x_cobrar.num_doc)
                list_rep_client.cxc_cobro = obj_cxc_cobro
                list_rep.append(list_rep_client)

    list_rep.sort(key=operator.attrgetter('num_doc', 'fecha_reg'))
    saldo = saldo_ini

    for obj_list in list_rep:
        if obj_list.tipo == 1:
            if obj_list.cuentas_x_cobrar.naturaleza == 1:
                saldo += obj_list.cuentas_x_cobrar.monto
                saldo_deudor += obj_list.cuentas_x_cobrar.monto
            else:
                saldo -= obj_list.cuentas_x_cobrar.monto
                saldo_acreedor += obj_list.cuentas_x_cobrar.monto
        else:
            if obj_list.cxc_cobro.cuentas_x_cobrar.naturaleza == 1:
                saldo -= obj_list.cxc_cobro.monto
                saldo_acreedor += obj_list.cxc_cobro.monto
            else:
                saldo += obj_list.cxc_cobro.monto
                saldo_deudor += obj_list.cxc_cobro.monto
        obj_list.saldo = saldo

    return saldo_deudor, saldo_acreedor


def listar_saldo_clientes(buscador, lista_id_client, fecha_ini, fecha_final, clientes, lista_saldo_client):
    buscador.is_valid()  # Se llama a la función para usar cleaned_data
    if buscador.getAllClient():
        all_client = Cliente.objects.filter(status=1).order_by("razon_social")
        for cliente in all_client:

            list_rep = []
            lista_id_client.append(cliente.id)
            saldo_cliente = get_saldo_client(cliente, fecha_ini)
            saldo_deudor, saldo_acreedor = ord_list_fecha(cliente, fecha_ini, fecha_final, list_rep, saldo_cliente)
            if saldo_cliente != 0 or len(list_rep) != 0:
                datos_client = DatosClientSaldos(saldo_cliente)
                datos_client.cliente = cliente
                datos_client.lista_saldo = list_rep
                datos_client.saldo_deudor = saldo_deudor
                datos_client.saldo_acreedor = saldo_acreedor

                lista_saldo_client.append(datos_client)
    else:
        if clientes.is_valid():
            for obj in clientes:
                informacion = obj.cleaned_data
                try:
                    list_rep = []
                    cliente = Cliente.objects.get(id=informacion.get("cliente"))
                    # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                    if cliente.id not in lista_id_client:
                        lista_id_client.append(cliente.id)
                        saldo_cliente = get_saldo_client(cliente, fecha_ini)
                        saldo_deudor, saldo_acreedor = ord_list_fecha(cliente, fecha_ini, fecha_final, list_rep, saldo_cliente)

                        if saldo_cliente != 0 or len(list_rep) != 0:
                            datos_client = DatosClientSaldos(saldo_cliente)
                            datos_client.cliente = cliente
                            datos_client.lista_saldo = list_rep
                            datos_client.saldo_deudor = saldo_deudor
                            datos_client.saldo_acreedor = saldo_acreedor

                            lista_saldo_client.append(datos_client)
                except Cliente.DoesNotExist:
                    pass

def guiareporte_compras_x_cuenta(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    cuentas = proveed_formset(prefix='cuenta')
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        #cuentas = cuentas_formset(request.POST, prefix="cuentas")
        cuentas = proveed_formset(request.POST, prefix='cuenta')
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('COMPRA_COMPRAS_X_CUENTA') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    print('esto es informacion')
                    print str(informacion)
                    try:
                        proveedor = PlanCuenta.objects.get(id=informacion.get("cuenta"))
                        id_proveedores += str(proveedor.id) + ","
                    except Proveedores.DoesNotExist:
                        pass
            else:
                print('no hay cuentassss')
            print(str(id_proveedores))
            if buscador.getAllProv():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'cuenta': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    return render_to_response('reportes_compras/reporte_compras_x_cuenta.html',
                              {"buscador": buscador,
                               "lista": r,#"lista": r,
                               "proveedores": cuentas
                               }, context_instance=RequestContext(request))



@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_monto_cliente_por_fecha(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="clientes")
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        cuentas = cuentas_formset(request.POST, prefix="clientes")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""
        urljasper = get_url_jasper()
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:
            report = get_nombre_reporte('CLIENTE_ESTADO_CUENTA_CLIENTE') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)
            print('el user')
            print(empresa.pass_jasper)

            print('el pass')
            print('+++++++++++')
            print 'la ruta de reopirtr'+str(url)
            print('************************')
            print(empresa.user_jasper)
            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    try:
                        cuenta = PlanCuenta.objects.get(id=informacion.get("cliente"), status=1)
                        id_cuentas += str(cuenta.id) + ","
                    except PlanCuenta.DoesNotExist:
                        pass

            if buscador.getAllClient():
                todos = 1
            else:
                todos = 0
            print str(id_cuentas)+'las cuentas o la cuebte'
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'todos': todos,
                      'cliente': id_cuentas[0:-1]
                      }
            print (str(params['todos']))+'estodos ouno'
            print (str(params['cliente']))
            print('si es todos')
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            print(s)
            r = s.get(url=url+report, auth=auth, params=params)
            print(url+report)
            print(params)
            print('params lm')
            if r.status_code != 200:
                r = None
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    return render_to_response('reporte_clientes/reporte_monto_clientes.html',
                              {"buscador": buscador,
                               "lista": r,
                               "clientes": cuentas
                               }, context_instance=RequestContext(request))
#fin de la actualizacion del reporte
#lo mismo de arriba pero en excel

@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_monto_cliente_por_fecha_excel(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    cuentas_formset = formset_factory(ReporteSaldosForm)
    cuentas = cuentas_formset(prefix="clientes")
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        cuentas = cuentas_formset(request.POST, prefix="clientes")
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        id_cuentas = ""
        urljasper = get_url_jasper()
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'
            # Report to process:
            report = get_nombre_reporte('CLIENTE_ESTADO_CUENTA_CLIENTE') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)
            print('el user')
            print(empresa.pass_jasper)

            print('el pass')
            print('+++++++++++')
            print 'la ruta de reopirtr'+str(url)
            print('************************')
            print(empresa.user_jasper)
            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    try:
                        cuenta = PlanCuenta.objects.get(id=informacion.get("cliente"), status=1)
                        id_cuentas += str(cuenta.id) + ","
                    except PlanCuenta.DoesNotExist:
                        pass

            if buscador.getAllClient():
                todos = 1
            else:
                todos = 0
            print str(id_cuentas)+'las cuentas o la cuebte'
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'todos': todos,
                      'cliente': id_cuentas[0:-1]
                      }
            print (str(params['todos']))+'estodos ouno'
            print (str(params['cliente']))
            print('si es todos')
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            print(s)
            r = s.get(url=url+report, auth=auth, params=params)
            print(url+report)
            print(params)
            print('params lm')
            if r.status_code != 200:
                r = None
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")

    #s = requests.Session()
    #r = s.get(url=url+report, auth=auth, params=params)

    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
    nombre = "Estado_cliente_"+str(fecha_ini)+" al "+str(fecha_fin)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    return response

#


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_monto_cliente_por_fecha_pdf(request):
    """
    Reporte de saldos de proveedor por fecha
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    proveed_formset = formset_factory(ReporteSaldosForm)
    clientes = proveed_formset(prefix='clientes')
    lista_saldo_client = []  # Lista donde se guardan las transacciones de los clientes
    lista_id_client = []  # Lista donde se guardan los ids del registro de cada cliente para evitar repeticiones
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            clientes = proveed_formset(request.POST, prefix='clientes')
            # Si escojió la opción de todos los clientes
            listar_saldo_clientes(buscador, lista_id_client, fecha_ini, fecha_final, clientes, lista_saldo_client)

    html = render_to_string('reporte_clientes/reporte_monto_clientes_pdf.html',
                            {'pagesize': 'A4',
                             "lista_saldo_client": lista_saldo_client,
                             "clientes": clientes,
                             "empresa":empresa,
                             'usuario': usuario,
                             'request': request}, context_instance=RequestContext(request))
    nombre = (u'Rpt_est_cta_clientes_' + fecha_ini.strftime("%Y-%m-%d") + '_a_' + fecha_final.strftime("%Y-%m-%d"))
    return generar_pdf_nombre(html, nombre)


def listar_all_clientes(lista, lista_id_client, fecha_ini, fecha_final):
    """
    inserta en una lista todos los movimientos de los clientes en una fecha
    indicada para la hoja de excel
    :param lista:
    :param lista_id_client:
    :param fecha_ini:
    :param fecha_final:
    :return:
    """
    all_client = Cliente.objects.filter(status=1).order_by("razon_social")
    for cliente in all_client:
        list_rep = []
        lista_id_client.append(cliente.id)
        saldo_cliente = get_saldo_client(cliente, fecha_ini)
        saldo_deudor, saldo_acreedor = ord_list_fecha(cliente, fecha_ini, fecha_final, list_rep, saldo_cliente)

        if saldo_cliente > 0 or len(list_rep) > 0:
            lista.append([u"Cliente: " + cliente.razon_social + u"(" + cliente.ruc + u")%/%1"])
            lista.append([u"Fecha%/%2", u"Tipo Doc.%/%2", u"# Documento%/%2", u"Débito%/%2",
                          u"Crédito%/%2", u"Forma de Pago%/%2", u"Saldo Doc.%/%2", u"Saldo Cliente%/%2"])#arreglo en el excel porque la columna decia proveedor siendo cliente
            lista.append([u"", u"", u"", u"", u"", u"", u"", round(saldo_cliente, 2)])
            for obj_list_rep in list_rep:
                if obj_list_rep.tipo == 1:
                    if obj_list_rep.cuentas_x_cobrar.naturaleza == 1:
                        lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                      obj_list_rep.cuentas_x_cobrar.documento.descripcion_documento,
                                      obj_list_rep.cuentas_x_cobrar.num_doc, round(obj_list_rep.cuentas_x_cobrar.monto, 2), "", "",
                                      round(obj_list_rep.cuentas_x_cobrar.monto, 2), round(obj_list_rep.saldo, 2)])
                    else:
                        lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                      obj_list_rep.cuentas_x_cobrar.documento.descripcion_documento,
                                      obj_list_rep.cuentas_x_cobrar.num_doc, "", round(obj_list_rep.cuentas_x_cobrar.monto, 2), "",
                                      round(obj_list_rep.cuentas_x_cobrar.monto, 2), round(obj_list_rep.saldo, 2)])
                else:
                    if obj_list_rep.cuentas_x_cobrar.num_doc != obj_list_rep.cxc_cobro.cobro.num_comp:
                        if obj_list_rep.cxc_cobro.cuentas_x_cobrar.naturaleza == 2:
                            lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"), "",
                                          obj_list_rep.cxc_cobro.cuentas_x_cobrar.num_doc, round(obj_list_rep.cxc_cobro.monto, 2),
                                          "", str(obj_list_rep.cxc_cobro.cobro.forma_pago.descripcion) + " - " +
                                          str(obj_list_rep.cxc_cobro.cobro.num_comp),
                                          round(obj_list_rep.cxc_cobro.get_saldo_doc(), 2), round(obj_list_rep.saldo, 2)])
                        else:
                            lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"), "",
                                          obj_list_rep.cxc_cobro.cuentas_x_cobrar.num_doc, "", round(obj_list_rep.cxc_cobro.monto, 2),
                                          str(obj_list_rep.cxc_cobro.cobro.forma_pago.descripcion) + " - " +
                                          str(obj_list_rep.cxc_cobro.cobro.num_comp),
                                          round(obj_list_rep.cxc_cobro.get_saldo_doc(), 2), round(obj_list_rep.saldo, 2)])

            lista.append(["", "", "", round(saldo_deudor, 2), round(saldo_acreedor, 2)])
            for i in range(0, 2):
                lista.append([u"", u"", u"", u"", u"", u"", u"", u""])


def listar_client_select(obj, lista, lista_id_client, fecha_ini, fecha_final):
    """
    inserta en una lista todos los movimientos de los clientes indicados en el formulario
    en una fecha indicada para la hoja de excel
    :param obj:
    :param lista:
    :param lista_id_client:
    :param fecha_ini:
    :param fecha_final:
    :return:
    """
    informacion = obj.cleaned_data
    try:
        list_rep = []
        cliente = Cliente.objects.get(id=informacion.get("cliente"))
        # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
        if cliente.id not in lista_id_client:
            lista_id_client.append(cliente.id)
            saldo_cliente = get_saldo_client(cliente, fecha_ini)

            saldo_deudor, saldo_acreedor = ord_list_fecha(cliente, fecha_ini, fecha_final, list_rep, saldo_cliente)

            if saldo_cliente > 0 or len(list_rep) > 0:
                lista.append([u"Cliente: " + cliente.razon_social + u"(" + cliente.ruc + u")%/%1"])
                lista.append([u"Fecha%/%2", u"Tipo Doc.%/%2", u"# Documento%/%2", u"Débito%/%2",
                              u"Crédito%/%2", u"Forma de Pago%/%2", u"Saldo Doc.%/%2", u"Saldo Proveedor%/%2"])
                lista.append([u"", u"", u"", u"", u"", u"", u"", round(saldo_cliente, 2)])
                for obj_list_rep in list_rep:
                    if obj_list_rep.tipo == 1:
                        if obj_list_rep.cuentas_x_cobrar.naturaleza == 1:
                            lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                          obj_list_rep.cuentas_x_cobrar.documento.descripcion_documento,
                                          obj_list_rep.cuentas_x_cobrar.num_doc, round(obj_list_rep.cuentas_x_cobrar.monto, 2), "", "",
                                          round(obj_list_rep.cuentas_x_cobrar.monto, 2), round(obj_list_rep.saldo, 2)])
                        else:
                            lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                          obj_list_rep.cuentas_x_cobrar.documento.descripcion_documento,
                                          obj_list_rep.cuentas_x_cobrar.num_doc, "", round(obj_list_rep.cuentas_x_cobrar.monto, 2),
                                          "", round(obj_list_rep.cuentas_x_cobrar.monto, 2), round(obj_list_rep.saldo, 2)])
                    else:
                        if obj_list_rep.cxc_cobro.cuentas_x_cobrar.num_doc != obj_list_rep.cxc_cobro.cobro.num_comp:
                            if obj_list_rep.cxc_cobro.cuentas_x_cobrar.naturaleza == 2:
                                lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"), "",
                                              obj_list_rep.cxc_cobro.cuentas_x_cobrar.num_doc, round(obj_list_rep.cxc_cobro.monto, 2), "",
                                              str(obj_list_rep.cxc_cobro.cobro.forma_pago.descripcion) + " - " +
                                              str(obj_list_rep.cxc_cobro.cobro.num_comp),
                                              round(obj_list_rep.cxc_cobro.get_saldo_doc(), 2), round(obj_list_rep.saldo, 2)])
                            else:
                                lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"), "",
                                              obj_list_rep.cxc_cobro.cuentas_x_cobrar.num_doc, "", round(obj_list_rep.cxc_cobro.monto, 2),
                                              str(obj_list_rep.cxc_cobro.cobro.forma_pago.descripcion) + " - " +
                                              str(obj_list_rep.cxc_cobro.cobro.num_comp),
                                              round(obj_list_rep.cxc_cobro.get_saldo_doc(), 2), round(obj_list_rep.saldo, 2)])

                lista.append(["", "", "", round(saldo_deudor, 2), round(saldo_acreedor, 2)])
                for i in range(0, 2):
                    lista.append([u"", u"", u"", u"", u"", u"", u""])
    except:
        pass


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_clientes(request):
    items = Cliente.objects.filter(status=1).order_by('razon_social')
    datos = {'items': items}
    return render_to_response('reporte_clientes/reporte_clientes.html', datos, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_clientes_excel(request):
    items = Cliente.objects.filter(status=1).order_by('razon_social')
    lista = []

    lista.append(['', '', 'Reportes de Clientes'+TITULO])
    lista.append([u'Fecha de Generación: '+SUBTITULO, str(datetime.datetime.now().strftime('%Y-%m-%d'))+SUBTITULO])
    lista.append([''])
    cabecera = [u'Tipo de Persona'+COLUMNA, u'Tipo de Identificación'+COLUMNA,u'Identificación'+COLUMNA,u'Razón Social'+COLUMNA,u'Actividad'+COLUMNA,u'Dirección'+COLUMNA]
    lista.append(cabecera)

    if items:
        for item in items:
            miactividad = 'No Definida'
            direccion = u'Dirección no definida'

            try:
                if item.actividad.descripcion is not None and item.actividad is not None:
                    miactividad = item.actividad.descripcion
            except:
                miactividad = 'No Definida'

            if item.direccion:
                direccion = item.direccion

            row = [
                item.tipo_cliente.descripcion,
                item.tipo_identificacion.descripcion,
                item.ruc,
                item.razon_social,
                miactividad,
                direccion]
            lista.append(row)

    return ExcelResponse(lista, 'Rpt_clientes_'+str(datetime.datetime.now().strftime('%Y-%m-%d')))


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_clientes_pdf(request):
    """
    Reporte de saldos de proveedor por fecha
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    items = Cliente.objects.filter(status=1).order_by('razon_social')

    html = render_to_string('reporte_clientes/reporte_clientes_pdf.html',
                            {'pagesize': 'A4',
                             "items": items,
                             "empresa":empresa,
                             'usuario': usuario,
                             "request": request}, context_instance=RequestContext(request))
    nombre = u'Rpt_clientes_'+str(datetime.datetime.now().strftime('%Y-%m-%d'))
    return generar_pdf_nombre(html, nombre)


def get_cta_cobrar_pendientes_query(fecha_corte, id_cliente):
    return '''SELECT distinct c.id
                FROM cuentas_x_cobrar as c
                    WHERE (c.status = 1  AND
                        c.fecha_reg <= \'''' + fecha_corte +'''\' AND
                        c.monto >  coalesce((select SUM(cxc_cobro.monto) from cxc_cobro INNER JOIN cobro on (cxc_cobro.cobro_id=cobro.id and c.id = cxc_cobro.cuentas_x_cobrar_id)
                                                where cxc_cobro.status = 1  AND cobro.fecha_reg <= \'''' + fecha_corte +'''\' AND cobro.status=1
                                                and c.id=cxc_cobro.cuentas_x_cobrar_id and c.num_doc != cobro.num_comp)::decimal, 0) AND
                        c.cliente_id = ''' + str(id_cliente) + ''' )
'''


def get_cobrado(fecha_corte, list_cta_cobrar):
    cobrado = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=(x.id for x in list_cta_cobrar),
                                                         status=1,
                                                         cobro__fecha_reg__lte=fecha_corte).filter(~Q(cuentas_x_cobrar__num_doc=F("cobro__num_comp"))).aggregate(Sum('monto',
                                                                                                              field="cxc_cobro.monto"))["monto__sum"])
    return cobrado


def get_cobrado_naturaleza(fecha_corte, list_cta_cobrar, naturaleza):
    cobrado = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=(x.id for x in list_cta_cobrar),
                                                         status=1,
                                                         cobro__fecha_reg__lte=fecha_corte,
                                                         cuentas_x_cobrar__naturaleza=naturaleza).filter(~Q(cuentas_x_cobrar__num_doc=F("cobro__num_comp"))).aggregate(Sum('monto',
                                                                                                              field="cxc_cobro.monto"))["monto__sum"])
    return cobrado


def listar_cuentas_cobrar_pendientes(buscador, clientes, lista_saldo_client, lista_id_client):
    """
    Lista las cuentas por pagar pendientes por fecha en un arreglo
    :param buscador:
    :param proveed_formset:
    :param lista_saldo_client:
    :param lista_id_client:
    :param request:
    :return:
    """
    fecha_ini = buscador.getFechaIni()
    fecha_final = buscador.getFechaFinal()
    total_debito = 0.0
    total_credito = 0.0
    total_cobrado_cruzado = 0.0
    total_cobrar = 0.0

    if fecha_final is not None:
        # Si escojió la opción de todos los clientes
        buscador.is_valid()  # Se llama a la función para usar cleaned_data
        if buscador.getAllClient():
            all_client = Cliente.objects.filter(status=1).order_by("razon_social")

            for cliente in all_client:
                lista_id_client.append(cliente.id)
                datos_cliente = DatosClientSaldos(0)
                datos_cliente.cliente = cliente

                datos_cliente.lista_saldo = Cuentas_por_Cobrar.objects.raw(get_cta_cobrar_pendientes_query(fecha_final.strftime("%Y-%m-%d"), cliente.id))

                total_monto_deudor = cero_if_none(Cuentas_por_Cobrar.objects.filter(id__in=(x.id for x in datos_cliente.lista_saldo)).filter(naturaleza=1).aggregate(Sum('monto',
                                                                                                        field="monto"))["monto__sum"])

                total_monto_acreedor = cero_if_none(Cuentas_por_Cobrar.objects.filter(id__in=(x.id for x in datos_cliente.lista_saldo)).filter(naturaleza=2).aggregate(Sum('monto',
                                                                                                          field="monto"))["monto__sum"])


                total_cobrado = get_cobrado(fecha_final, datos_cliente.lista_saldo)


                total_cobrado_deudor = get_cobrado_naturaleza(fecha_final, datos_cliente.lista_saldo, 1)


                total_cobrado_acreedor = get_cobrado_naturaleza(fecha_final, datos_cliente.lista_saldo, 2)


                datos_cliente.saldo_deudor = total_monto_deudor
                datos_cliente.saldo_acreedor = total_monto_acreedor
                datos_cliente.saldo = total_cobrado
                datos_cliente.total_saldo_pendiente = total_monto_deudor - total_monto_acreedor + \
                                                      total_cobrado_acreedor - total_cobrado_deudor


                # Sumariza el total a cobrar de todos los clientes
                total_debito += total_monto_deudor
                total_credito += total_monto_acreedor
                total_cobrado_cruzado += total_cobrado

                total_cobrar += datos_cliente.total_saldo_pendiente

                if len(list(datos_cliente.lista_saldo)) != 0:
                    lista_saldo_client.append(datos_cliente)

        else:

            if clientes.is_valid():
                for obj in clientes:
                    informacion = obj.cleaned_data
                    try:
                        cliente = Cliente.objects.get(id=informacion.get("cliente"))
                        # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                        if cliente.id not in lista_id_client:
                            lista_id_client.append(cliente.id)
                            datos_cliente = DatosClientSaldos(0)
                            datos_cliente.cliente = cliente
                            datos_cliente.lista_saldo = Cuentas_por_Cobrar.objects.raw(get_cta_cobrar_pendientes_query(fecha_final.strftime("%Y-%m-%d"), cliente.id))

                            total_monto_deudor = cero_if_none(Cuentas_por_Cobrar.objects.filter(id__in=(x.id for x in datos_cliente.lista_saldo)).filter(naturaleza=1).aggregate(Sum('monto',
                                                                                                                    field="monto"))["monto__sum"])

                            total_monto_acreedor = cero_if_none(Cuentas_por_Cobrar.objects.filter(id__in=(x.id for x in datos_cliente.lista_saldo)).filter(naturaleza=2).aggregate(Sum('monto',
                                                                                                                      field="monto"))["monto__sum"])


                            total_cobrado = get_cobrado(fecha_final, datos_cliente.lista_saldo)

                            total_cobrado_deudor = get_cobrado_naturaleza(fecha_final, datos_cliente.lista_saldo, 1)

                            total_cobrado_acreedor = get_cobrado_naturaleza(fecha_final, datos_cliente.lista_saldo, 2)


                            datos_cliente.saldo_deudor = total_monto_deudor
                            datos_cliente.saldo_acreedor = total_monto_acreedor
                            datos_cliente.saldo = total_cobrado
                            datos_cliente.total_saldo_pendiente = total_monto_deudor - total_monto_acreedor + \
                                                                  total_cobrado_acreedor - total_cobrado_deudor


                            # Sumariza el total a cobrar de todos los clientes
                            total_debito += total_monto_deudor
                            total_credito += total_monto_acreedor
                            total_cobrado_cruzado += total_cobrado

                            total_cobrar += datos_cliente.total_saldo_pendiente

                            if len(list(datos_cliente.lista_saldo)) != 0:
                                lista_saldo_client.append(datos_cliente)
                    except Cliente.DoesNotExist:
                        pass

    return total_debito, total_credito, total_cobrado_cruzado, total_cobrar


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuentas_por_cobrar_pendientes_17dicnormal(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    buscador = BuscadorReportes(initial={"fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    cantidad = None
    proveed_formset = formset_factory(ReporteSaldosForm)
    clientes = proveed_formset(prefix='clientes')
    lista_saldo_client = []  # Lista donde se guardan las transacciones de los clientes
    lista_id_client = []  # Lista donde se guardan los ids del registro de cada cliente para evitar repeticiones
    total_debito = 0.0
    total_credito = 0.0
    total_cobrado_cruzado = 0.0
    total_cobrar = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        clientes = proveed_formset(request.POST, prefix='clientes')
        buscador.is_valid()
        total_debito, total_credito, total_cobrado_cruzado, total_cobrar = listar_cuentas_cobrar_pendientes(buscador, clientes, lista_saldo_client, lista_id_client)

        cantidad = len(lista_saldo_client)

    return render_to_response('reporte_clientes/reporte_cuentas_por_cobrar_pendiente.html',
                              {"buscador": buscador,
                               "objetos": lista_saldo_client,
                               "clientes": clientes,
                               "cantidad": cantidad,
                               "total_debito": total_debito,
                               "total_credito": total_credito,
                               "total_cobrado_cruzado": total_cobrado_cruzado,
                               "total_cobrar": total_cobrar,
                               "fecha_ini": buscador.getFechaIni(),
                               "fecha_final": buscador.getFechaFinal()
                               }, context_instance=RequestContext(request))


@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuentas_por_cobrar_pendientes(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    clientes = proveed_formset(prefix='clientes')
    r = None
    cantidad = None
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        clientes = proveed_formset(request.POST, prefix='clientes')
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_fin = buscador.getFechaFinal()
        #cuentas = cuentas_formset(request.POST, prefix="cuentas")
        #clientes = proveed_formset(request.POST, prefix='clientes')
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if fecha_fin is not None:
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('CLIENTES_CXC_PENDIENTES') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if clientes.is_valid():
                for obj in clientes:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Cliente.objects.get(id=informacion.get("cliente"))
                        id_proveedores += str(proveedor.id) + ","
                    except Cliente.DoesNotExist:
                        pass
            else:
                print('no hay clientessssss :o')
            print(str(id_proveedores))
            if buscador.getAllClient():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'cliente': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    return render_to_response('reporte_clientes/reporte_cuentas_por_cobrar_pendiente.html',
                              {"buscador": buscador,
                               "lista": r,#"lista": r,
                               "proveedores": clientes
                               }, context_instance=RequestContext(request))




@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuentas_por_cobrar_pendientes_pdf(request):
    """
    Reporte de cuentas por cobrar pendientes pdf
    :param request:
    :return:
    """

    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes(initial={"fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    proveed_formset = formset_factory(ReporteSaldosForm)
    clientes = proveed_formset(prefix='clientes')
    lista_saldo_client = []  # Lista donde se guardan las transacciones de los clientes
    lista_id_client = []  # Lista donde se guardan los ids del registro de cada cliente para evitar repeticiones
    total_debito = 0.0
    total_credito = 0.0
    total_cobrado_cruzado = 0.0
    total_cobrar = 0.0


    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        clientes = proveed_formset(request.POST, prefix='clientes')
        buscador.is_valid()
        total_debito, total_credito, total_cobrado_cruzado, total_cobrar = listar_cuentas_cobrar_pendientes(buscador, clientes, lista_saldo_client, lista_id_client)

    html = render_to_string('reporte_clientes/reporte_cuentas_por_cobrar_pendiente_pdf.html',
                            {'pagesize': 'A4',
                             "objetos": lista_saldo_client,
                             "clientes": clientes,
                             "desde": buscador.getFechaIni(),
                             "hasta": buscador.getFechaFinal(),
                             "total_debito": total_debito,
                             "total_credito": total_credito,
                             "total_cobrado_cruzado": total_cobrado_cruzado,
                             "total_cobrar": total_cobrar,
                             "fecha_ini": buscador.getFechaIni(),
                             "fecha_final": buscador.getFechaFinal(),
                             "empresa": empresa,
                             'usuario': usuario,
                             'request': request}, context_instance=RequestContext(request))
    nombre = u'Rpt_cta_cob_pend_a_'+str(buscador.getFechaFinal())
    return generar_pdf_nombre(html, nombre)


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuentas_por_cobrar_pendientes_excel(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    buscador = BuscadorReportes(initial={"fecha_ini":
                                             str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    clientes = proveed_formset(prefix='clientes')
    lista_saldo_client = []  # Lista donde se guardan las transacciones de los clientes
    lista_id_client = []  # Lista donde se guardan los ids del registro de cada cliente para evitar repeticiones
    lista = []  # Arreglo para el EXCEL
    total_debito = 0.0
    total_credito = 0.0
    total_cobrado_cruzado = 0.0
    total_cobrar = 0.0
    lista.append([u"", u"", u"Cuentas por cobrar Pendientes"+TITULO])
    lista.append([u""])
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        clientes = proveed_formset(request.POST, prefix='clientes')
        buscador.is_valid()
        total_debito, total_credito, total_cobrado_cruzado, total_cobrar = listar_cuentas_cobrar_pendientes(buscador, clientes, lista_saldo_client, lista_id_client)
        lista.append([u"Hasta: "+SUBTITULO, buscador.getFechaFinal()])
        lista.extend("" for i in range(2))
        for obj in lista_saldo_client:
            lista.append([u"Cliente: "+SUBTITULO, convert_vacio_none(obj.cliente.razon_social),
                          convert_vacio_none(obj.cliente.ruc)])
            lista.append([u"Fecha Registro"+COLUMNA, u"Fecha Emsión"+COLUMNA, u"Tipo Doc."+COLUMNA,
                          u"# Documento"+COLUMNA, u"Débito"+COLUMNA, u"Crédito"+COLUMNA, u"Cobrado/Cruzado"+COLUMNA, u"Saldo Pendiente"+COLUMNA])
            for obj2 in obj.lista_saldo:
                if obj2.naturaleza == 1:
                    lista.append([obj2.fecha_reg, obj2.fecha_emi, obj2.documento.descripcion_documento, obj2.num_doc,
                                  obj2.monto, 0.0, get_monto_cobrado_fecha_func(obj2.id,
                                                                                buscador.getFechaFinal()),
                                  get_monto_saldo_cxc_fecha_func(obj2.id,
                                                                 buscador.getFechaFinal())])
                else:
                    lista.append([obj2.fecha_reg, obj2.fecha_emi, obj2.documento.descripcion_documento, obj2.num_doc,
                                  0.0, obj2.monto, get_monto_cobrado_fecha_func(obj2.id,
                                                                                buscador.getFechaFinal()),
                                  get_monto_saldo_cxc_fecha_func(obj2.id,
                                                                 buscador.getFechaFinal())])

            lista.append([u""])
            lista.append(["", "", "", u"Totales:", str(obj.saldo_deudor)+G_TOTAL, str(obj.saldo_acreedor)+G_TOTAL,
                          str(obj.saldo)+G_TOTAL, str(obj.total_saldo_pendiente)+G_TOTAL])
            lista.append([u""])

        lista.append(["", "", "", u"Totales: ", total_debito, total_credito, total_cobrado_cruzado, total_cobrar])

    return ExcelResponse(lista, u'Rpt_cta_cob_pend_a_'+str(buscador.getFechaFinal()))


class ReporteSaldoCliente():
    """
        Clase que ayuda a agrupar los datos para presentarlos en
        saldos_por_proveedores
    """
    def __init__(self):
        self.cliente = Cliente()
        self.otdoc = 0.0
        self.ant = 0.0
        self.nc = 0.0
        self.nd = 0.0
        self.total = 0.0


def listar_saldo_por_cliente(fecha_final, lista):
    """
    Llena la lista para presentar en el reporte saldo_por_proveedor
    :param fecha_final:
    :param lista:
    :return:
    """
    total_saldo_compras = 0
    total_nd = 0
    total_nc = 0
    total_ant = 0
    total = 0
    for obj in Cliente.objects.filter(status=1).order_by('razon_social'):
        otdoc_debe = convert_cero_none(Cuentas_por_Cobrar.objects.exclude(Q(documento__codigo_documento="AN")|
                                                                          Q(documento__codigo_documento="04")|
                                                                          Q(documento__codigo_documento="05")).
            filter(cliente=obj,
                   fecha_reg__lte=fecha_final,
                   status=1).aggregate(Sum('monto'))["monto__sum"])

        otdoc_haber = convert_cero_none(CXC_cobro.objects.exclude(Q(cuentas_x_cobrar__documento__codigo_documento="AN")|
                                                                  Q(cuentas_x_cobrar__documento__codigo_documento="04")|
                                                                  Q(cuentas_x_cobrar__documento__codigo_documento="05"))
            .filter(cobro__cliente=obj,
                    cobro__fecha_reg__lte=fecha_final,
                    status=1).aggregate(Sum('monto'))["monto__sum"])

        ant_debe = convert_cero_none(Cuentas_por_Cobrar.objects.filter(cliente=obj,
                                                                       documento__codigo_documento="AN",
                                                                       fecha_reg__lte=fecha_final,
                                                                       status=1).aggregate(Sum('monto'))["monto__sum"])

        ant_haber = convert_cero_none(CXC_cobro.objects.filter(cobro__cliente=obj,
                                                               cuentas_x_cobrar__documento__codigo_documento="AN",
                                                               cobro__fecha_reg__lte=fecha_final,
                                                               status=1).filter(~Q(cuentas_x_cobrar__num_doc=F('cobro__num_comp'))).aggregate(Sum('monto'))["monto__sum"])

        n_d_debe = convert_cero_none(Cuentas_por_Cobrar.objects.filter(cliente=obj,
                                                                       documento__codigo_documento="05",
                                                                       fecha_reg__lte=fecha_final,
                                                                       status=1).aggregate(Sum('monto'))["monto__sum"])

        n_d_haber = convert_cero_none(CXC_cobro.objects.filter(cobro__cliente=obj,
                                                               cuentas_x_cobrar__documento__codigo_documento="05",
                                                               cobro__fecha_reg__lte=fecha_final,
                                                               status=1).aggregate(Sum('monto'))["monto__sum"])

        n_c_debe = convert_cero_none(Cuentas_por_Cobrar.objects.filter(cliente=obj,
                                                                       documento__codigo_documento="04",
                                                                       fecha_reg__lte=fecha_final,
                                                                       status=1).aggregate(Sum('monto'))["monto__sum"])

        n_c_haber = convert_cero_none(CXC_cobro.objects.filter(cobro__cliente=obj,
                                                               cuentas_x_cobrar__documento__codigo_documento="04",
                                                               cobro__fecha_reg__lte=fecha_final,
                                                               status=1).aggregate(Sum('monto'))["monto__sum"])

        saldo_cliente = ReporteSaldoCliente()
        saldo_cliente.cliente = obj
        saldo_cliente.otdoc = redondeo(otdoc_debe - otdoc_haber)
        saldo_cliente.ant = redondeo(ant_debe - ant_haber)
        saldo_cliente.nc = redondeo(n_c_debe - n_c_haber)
        saldo_cliente.nd = redondeo(n_d_debe - n_d_haber)
        saldo_cliente.total = redondeo(saldo_cliente.otdoc + saldo_cliente.nd - saldo_cliente.ant
                                         - saldo_cliente.nc)

        # Solo se presentan los proveedores que tengan movimientos

        if saldo_cliente.otdoc + saldo_cliente.ant + saldo_cliente.nc + saldo_cliente.nd != 0:
            lista.append(saldo_cliente)
            total_saldo_compras += saldo_cliente.otdoc
            total_nd += saldo_cliente.nd
            total_nc += saldo_cliente.nc
            total_ant += saldo_cliente.ant
            total += saldo_cliente.total

    return total_saldo_compras, total_nd, total_nc, total_ant, total


@csrf_exempt
@login_required()
def reporte_saldos_por_clientes(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """

    buscador = BuscadorReportes(initial={"fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    lista = []
    total_saldo_ventas = 0
    total_nd = 0
    total_nc = 0
    total_ant = 0
    total = 0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_final = buscador.getFechaFinal()
        if fecha_final is not None:
            total_saldo_ventas, total_nd, total_nc, total_ant, total = listar_saldo_por_cliente(fecha_final, lista)

    return render_to_response('reporte_clientes/reporte_saldos_por_cliente.html',
                              {"buscador": buscador,
                               'lista': lista,
                               'total_saldo_ventas': total_saldo_ventas,
                               'total_nd': total_nd,
                               'total_nc': total_nc,
                               'total_ant': total_ant,
                               'total': total}, context_instance=RequestContext(request))

@csrf_exempt
@login_required()
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldos_por_clientes_pdf(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes()

    lista = []
    total_saldo_ventas = 0
    total_nd = 0
    total_nc = 0
    total_ant = 0
    total = 0
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_final = buscador.getFechaFinal()

        if fecha_final is not None:
            total_saldo_ventas, total_nd, total_nc, total_ant, total = listar_saldo_por_cliente(fecha_final, lista)

            html = render_to_string('reporte_clientes/reporte_saldos_por_clientes_pdf.html',
                                    {'pagesize': 'A4',
                                     'buscador': buscador,
                                     'lista': lista,
                                     "empresa":empresa,
                                     'usuario': usuario,
                                     'total_saldo_ventas': total_saldo_ventas,
                                     'total_nd': total_nd,
                                     'total_nc': total_nc,
                                     'total_ant': total_ant,
                                     'total': total,
                                     'request': request
                                     }, context_instance=RequestContext(request))
            nombre = ('Rpt_Saldos_Cliente_a_'+str(fecha_final))
            return generar_pdf_nombre(html, nombre)
        else:
            errors = buscador._errors.setdefault("fecha_final", ErrorList())
            errors.append(u"La feha final es requerida")

    return render_to_response('reporte_clientes/reporte_saldos_por_cliente.html',
                              {"buscador": buscador,
                               'lista': lista,
                               'total_saldo_compras': total_saldo_ventas,
                               'total_nd': total_nd,
                               'total_nc': total_nc,
                               'total_ant': total_ant,
                               'total': total}, context_instance=RequestContext(request))



@csrf_exempt
@login_required()
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldos_por_clientes_excel(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    lista = []
    lista_excel = []
    total_saldo_ventas = 0
    total_nd = 0
    total_nc = 0
    total_ant = 0
    total = 0
    buscador = BuscadorReportes()

    lista_excel.append([u"", u"", u"Reporte Saldo por Cliente"+TITULO])
    lista_excel.append([u""])
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_final = buscador.getFechaFinal()

        if fecha_final is not None:
            lista_excel.append([u"Hasta: "+SUBTITULO, fecha_final.strftime("%Y-%m-%d")])
            lista_excel.append([u""])

            total_saldo_ventas, total_nd, total_nc, total_ant, total = listar_saldo_por_cliente(fecha_final, lista)

            lista_excel.append([u"Cliente"+COLUMNA, u"Saldo Ventas"+COLUMNA, u"N/D"+COLUMNA, u"N/C"+COLUMNA,
                                u"Anticipos"+COLUMNA, u"Total"+COLUMNA])
            for obj in lista:
                lista_excel.append([obj.cliente.razon_social+" ("+str(obj.cliente.ruc)+")", obj.otdoc, obj.nd, obj.nc,
                                    obj.ant, obj.total])
            lista_excel.append([u""])
            lista_excel.append([u"Totales", total_saldo_ventas, total_nd, total_nc,
                                total_ant, total])

            return ExcelResponse(lista_excel, 'Rpt_Saldos_Proveedor_a_'+str(fecha_final))
        else:
            errors = buscador._errors.setdefault("fecha_final", ErrorList())
            errors.append(u"La feha final es requerida")

    return render_to_response('reporte_clientes/reporte_saldos_por_cliente.html',
                              {"buscador": buscador,
                               'lista': lista,
                               'total_saldo_compras': total_saldo_ventas,
                               'total_nd': total_nd,
                               'total_nc': total_nc,
                               'total_ant': total_ant,
                               'total': total}, context_instance=RequestContext(request))


