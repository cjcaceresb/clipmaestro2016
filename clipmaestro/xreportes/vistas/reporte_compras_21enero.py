#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from librerias.funciones.funciones_vistas import *
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt
from contabilidad.models import *
import json
from contabilidad.formularios.ComprasForm import *
from django.forms.formsets import formset_factory
from django.contrib.auth.decorators import login_required
from contabilidad.formularios.RetencionesCuentaForm import *
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import transaction
from django.forms.util import ErrorList
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from cgi import escape
from django.core.paginator import *
from django import template
from administracion.models import *
from librerias.funciones.permisos import *
from librerias.funciones.funciones_vistas import *
from reportes.formularios.reportes_form import *
import xlwt

from django.db.models import Sum
from librerias.funciones.excel_response_mod import *
import requests

"""
@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha(request):
    query = []
    base0 = 0.0
    baseiva = 0.0
    monto = 0.0
    total = 0.0
    ret_fte = 0.0
    ret_iva = 0.0
    pagar = 0.0

    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)

        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                query = CompraCabecera.objects.exclude(Q(status=0)|Q(status=3)).filter(fecha_reg__range=(fecha_ini, fecha_final)).order_by("fecha_reg", "num_comp")
                base0 = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('base0', field="base0"))["base0__sum"])
                baseiva = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('baseiva', field="baseiva"))["baseiva__sum"])
                monto = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('monto_iva', field="monto_iva"))["monto_iva__sum"])
                total = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('monto_iva', field="base0 + baseiva + monto_iva"))["monto_iva__sum"])
                ret_fte = convert_cero_none(RetencionCompraCabecera.objects.filter(status=1).filter(compra_cabecera__in=CompraCabecera.objects.filter(fecha_reg__range=(fecha_ini, fecha_final))).aggregate(Sum('totalretfte', field="totalretfte"))["totalretfte__sum"])
                ret_iva = convert_cero_none(RetencionCompraCabecera.objects.filter(status=1).filter(compra_cabecera__in=CompraCabecera.objects.filter(fecha_reg__range=(fecha_ini, fecha_final))).aggregate(Sum('totalretiva', field="totalretiva"))["totalretiva__sum"])
                pagar = base0 + baseiva + monto - ret_fte - ret_iva

            else:
                query = []
                base0 = 0.0
                baseiva = 0.0
                monto = 0.0
                total = 0.0
                ret_fte = 0.0
                ret_iva = 0.0
                pagar = 0.0

    return render_to_response('reportes_compras/reporte_compras.html',
                              {"buscador": buscador, "objetos": query,
                               "base0": base0,
                               "baseiva": baseiva,
                               "monto": monto,
                               "total": total,
                               "ret_fte": ret_fte,
                               "ret_iva": ret_iva,
                               "pagar": pagar
                               }, context_instance=RequestContext(request))

"""

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                #report = get_nombre_reporte('CMP_COMPRA') + '.html'
                report = get_nombre_reporte('COMPRA_COMPRAS_X_FECHAS') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reportes_compras/reporte_compras.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))

"""
@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha_excel(request):
    buscador = BuscadorReportes()
    query = []
    base0 = 0.0
    baseiva = 0.0
    monto = 0.0
    total = 0.0
    ret_fte = 0.0
    ret_iva = 0.0
    pagar = 0.0
    lista = []
    lista.append([u"", u"", u"", u"", u"",  u"REPORTE COMPRAS POR FECHA"+TITULO])
    lista.append([u""])

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            query = CompraCabecera.objects.exclude(Q(status=0)|Q(status=3)).filter(fecha_reg__range=(fecha_ini, fecha_final)).order_by("fecha_reg", "num_comp")
            base0 = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('base0', field="base0"))["base0__sum"])
            baseiva = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('baseiva', field="baseiva"))["baseiva__sum"])
            monto = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('monto_iva', field="monto_iva"))["monto_iva__sum"])
            total = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('monto_iva', field="base0 + baseiva + monto_iva"))["monto_iva__sum"])
            ret_fte = convert_cero_none(RetencionCompraCabecera.objects.filter(status=1).filter(compra_cabecera__in=CompraCabecera.objects.filter(fecha_reg__range=(fecha_ini, fecha_final))).aggregate(Sum('totalretfte', field="totalretfte"))["totalretfte__sum"])
            ret_iva = convert_cero_none(RetencionCompraCabecera.objects.filter(status=1).filter(compra_cabecera__in=CompraCabecera.objects.filter(fecha_reg__range=(fecha_ini, fecha_final))).aggregate(Sum('totalretiva', field="totalretiva"))["totalretiva__sum"])
            total_pagar = base0 + baseiva + monto - ret_fte - ret_iva

            lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_final])
            lista.append([u"# Commprobante"+COLUMNA, u"Fecha"+COLUMNA, u"Tipo Doc."+COLUMNA, u"# Documento"+COLUMNA,
                          u"Proveedor"+COLUMNA, u"Base 0"+COLUMNA, u"Base IVA"+COLUMNA, u"Total Iva"+COLUMNA, u"Total Compra"+COLUMNA,
                          u"# Retención"+COLUMNA, u"Ret. Fte."+COLUMNA, u"Ret. IVA"+COLUMNA,
                          u"Pagar"+COLUMNA, u"Estado"+COLUMNA])

            for obj in query:
                #total_compra = obj.base0 + obj.baseiva + obj.monto_ice + obj.monto_iva - obj.get_total_ret()
                total_compra = obj.base0 + obj.baseiva + obj.monto_ice + obj.monto_iva


                if obj.status == 1:
                    if obj.getRetencion() is not None:
                        pagar = obj.base0 + obj.baseiva + obj.monto_iva - convert_cero_none(obj.getRetencion().totalretfte) - convert_cero_none(obj.getRetencion().totalretiva)
                        fila = [obj.num_comp, obj.fecha_reg,  obj.documento.descripcion_documento, obj.num_doc,
                            obj.proveedor.razon_social, obj.base0, obj.baseiva, obj.monto_iva, total_compra, obj.get_num_retencion(), obj.getRetencion().totalretfte,
                            convert_cero_none(obj.getRetencion().totalretiva), pagar, u"Activo"]

                    else:

                        pagar = obj.base0 + obj.baseiva + obj.monto_iva
                        fila = [obj.num_comp, obj.fecha_reg, obj.documento.descripcion_documento, obj.num_doc,
                            obj.proveedor.razon_social, obj.base0, obj.baseiva, obj.monto_iva, total_compra, obj.get_num_retencion(), 0.00,
                            0.00, pagar, u"Activo"]

                else:   # Anulado

                    if obj.getRetencion() is not None:
                        pagar = obj.base0 + obj.baseiva + obj.monto_iva - convert_cero_none(obj.getRetencion().totalretfte) - convert_cero_none(obj.getRetencion().totalretiva)
                        try:#al ponerle este try que no estaba, corre el excel, pero no me escribe los documentos anulados, y me agrega una fila vacia
                            fila = [obj.num_comp, obj.fecha_reg, obj.documento.descripcion_documento, obj.num_doc,
                                obj.proveedor.razon_social, obj.base0, obj.baseiva, obj.monto_iva, total_compra, obj.get_num_retencion(), obj.getRetencion().totalretfte,
                                convert_cero_none(obj.getRetencion().totalretiva), pagar, u"Anulado"]
                        except:
                            fila = [obj.num_comp,obj.fecha_reg,'','','','0.00','0.00','0.00','0.00',obj.get_num_retencion(), '0.00','0.00', '0.00', 'Anulada']


                    else:

                        pagar = obj.base0 + obj.baseiva + obj.monto_iva
                        fila = [obj.num_comp, obj.fecha_reg, obj.documento.descripcion_documento, obj.num_doc,
                                obj.proveedor.razon_social, obj.base0, obj.baseiva, obj.monto_iva, total_compra, obj.get_num_retencion(), 0.00,
                                0.00, pagar, u"Anulado"]


                lista.append(fila)


            lista.append([u""])
            lista.append([u"Totales"+SUBTITULO, u"", u"", u"", u"", str(base0)+G_TOTAL, str(baseiva)+G_TOTAL, str(monto)+G_TOTAL,
                          str(total)+G_TOTAL, u"", str(ret_fte)+G_TOTAL, str(ret_iva)+G_TOTAL, str(total_pagar)+G_TOTAL])

            return ExcelResponse(lista, u'Reporte de Compras por fecha desde '+str(fecha_ini) + u' hasta ' + str(fecha_final))
        else:
            return render_to_response('reportes_compras/reporte_compras.html',
                              {"buscador": buscador, "objetos": query,
                               "base0": base0,
                               "baseiva": baseiva,
                               "monto": monto,
                               "total": total,
                               "ret_fte": ret_fte,
                               "ret_iva": ret_iva,
                               "pagar": pagar
                               }, context_instance=RequestContext(request))
    else:
        return render_to_response('reportes_compras/reporte_compras.html',
                              {"buscador": buscador, "objetos": query,
                               "base0": base0,
                               "baseiva": baseiva,
                               "monto": monto,
                               "total": total,
                               "ret_fte": ret_fte,
                               "ret_iva": ret_iva,
                               "pagar": pagar
                               }, context_instance=RequestContext(request))
"""



@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    buscador = BuscadorReportes_fecha_inicial_final(request.GET)
    urljasper = get_url_jasper()
    if buscador.is_valid():
        if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
            fecha_ini = buscador.getFechaIni()
            fecha_final = buscador.getFechaFinal()
            # Path to resource rest service:
            url = urljasper + empresa.path_jasper + '/'

            #report = get_nombre_reporte('CMP_COMPRA') + '.html'
            report = get_nombre_reporte('COMPRA_COMPRAS_X_FECHAS') + '.xls'
            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_final.strftime("%Y-%m-%d")
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
            nombre = "Compras por fecha del "+str(fecha_ini)+" al "+str(fecha_final)
            response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
            return response

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha_pdf(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    buscador = BuscadorReportes_fecha_inicial_final(request.GET)
    urljasper = get_url_jasper()
    if buscador.is_valid():
        if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
            fecha_ini = buscador.getFechaIni()
            fecha_final = buscador.getFechaFinal()
            # Path to resource rest service:
            url = urljasper + empresa.path_jasper + '/'

            #report = get_nombre_reporte('CMP_COMPRA') + '.html'
            report = get_nombre_reporte('COMPRA_COMPRAS_X_FECHAS') + '.pdf'
            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_final.strftime("%Y-%m-%d")
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            response = HttpResponse(r.content, content_type='application/pdf')
            nombre = "Compras por fecha del "+str(fecha_ini)+" al "+str(fecha_final)
            response['Content-Disposition'] = 'attachment;filename="'+nombre+'".pdf"'
            return response



@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha_excel_23oct(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(request.GET)
    buscador.is_valid()  # Para que funcione el cleaned data
    fecha_ini = buscador.getFechaIni()
    fecha_final = buscador.getFechaFinal()
    urljasper = get_url_jasper()
    if None not in (fecha_ini, fecha_final):
        # Path to resource rest service:
        url = urljasper + empresa.path_jasper + '/'
        # Report to process:
        #get_nombre_reporte('COMPRA_COMPRAS_X_FECHAS')
        #report = get_nombre_reporte('CMP_COMPRA') + '.xls'
        report = get_nombre_reporte('COMPRA_COMPRAS_X_FECHAS') + '.xls'
        # Authorisation credentials:
        auth = (empresa.user_jasper, empresa.pass_jasper)
        # Params
        params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                  'fecha_final': fecha_final.strftime("%Y-%m-%d")
                  }

        # Init session so we have no need to auth again and again:
        s = requests.Session()
        r = s.get(url=url+report, auth=auth, params=params)

        response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
        nombre = "Compras por fecha del "+str(fecha_ini)+" al "+str(fecha_final)
        response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
        return response

#por aqui va a ir el reporte de compra_fecha_proveedor
#en el 18 de junio de 2015
@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha_proveedor(request):
    """
    Reporte de saldos de proveedor por fecha
    :param request:
    :return:
    """
    buscador = BuscadorReportes(initial={"fecha_ini":
                                             str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    ccosto_formset = formset_factory(ReporteSaldosForm)
    centro_costos = ccosto_formset(prefix='centro_costos')
    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    todosloscentroscostos = "no son todos los centros de costo"
    lista_centro_costos = []
    numeroestodoslosproveedores = 0 #bandera del check de los todos los proveedores0 si no es, 1 si lo es
    numeroestodosloscentrosdecosto = 0#bandera de todos los centros de costo,0 si no es, 1 si lo es
    lista_idproveedores = []#para tener los id de los proveedores, probado, si funciona :o
    lista_idcentroscosto = []#para tener los id de los centros de costo, probado, si funciona :o
    lista_saldo_prov = []  # lista de saldo de proveedores
    lista_id_prov = []  # Guarda el id del registro del proveedor, para que no se repitan datos
    print 'mi formulario de adaptacion para que tome proveedores y centro de costo'
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        centro_costos = ccosto_formset(request.POST, prefix="centro_costos")
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        lista_centro_costos = []
        fecha_final = buscador.getFechaFinal()
        por_centro_costo = buscador.por_centro_costo()#esto si es true, se marce todos los centros de costos
        centro_costos.is_valid()
        if centro_costos is not None:
            centro_costos.is_valid()
            for obj_cc in centro_costos:
                info = obj_cc.cleaned_data
                try:
                    if info.get("centro_costo") is not None and info.get("centro_costo") != "":
                        c_costo = Centro_Costo.objects.get(id=info.get("centro_costo"))
                        lista_centro_costos.append(c_costo)
                        lista_idcentroscosto.append(c_costo.id)
                except :
                    pass

        if por_centro_costo:
            todosloscentroscostos = "si son todos los centros de costo"
            numeroestodosloscentrosdecosto = 1#se activa la bandera de todos los centros de costo
            lista_centro_costos = Centro_Costo.objects.filter(status=1).order_by('descripcion')
            for ce in lista_centro_costos:
                lista_idcentroscosto.append(ce.id)
        else:
           todosloscentroscostos = "puede ser uno o ninguno de los centros de costo"

           #validar que al menos seleccione uno de ellos
        if None not in (fecha_ini, fecha_final):
            proveedores = proveed_formset(request.POST, prefix='proveedores')
            # Si escojió la opción de todos los proveedores
            listar_proveedores(buscador, fecha_ini, fecha_final, lista_saldo_prov, proveedores, lista_id_prov,lista_idproveedores,numeroestodoslosproveedores)
        print 'los proveedores miden'+(str(len(lista_saldo_prov)))
    #proveedores trae la lista de los proveedores
    #lista_centro_costos es el centro de costo o todos, dependiendo si estaba marcado el checkbox
    #
    if lista_idcentroscosto:
        print('si hay lista de id de centro costos')
        for cen in lista_idcentroscosto:
            print(str(cen))
    else:
        print('no hay lista de id de centro costos')
    if lista_idproveedores:
        print('si hay lista de id deproveedores')
        for pro in lista_idproveedores:
            print(str(pro))
    else:
        print('no hay lista de id deproveedores')

    if numeroestodosloscentrosdecosto ==1:#si he dicho toods lod centro de costo
        print('si hay lista de id de centro costos por tooodos')
        for cent in lista_idcentroscosto:
            print(str(cent))

    if numeroestodoslosproveedores ==1:#si he dicho toods lod centro de costo
        print('si hay lista de id de centro costos por tooodos')
        for pr in lista_idproveedores:
            print(str(pr))
    #por aqui,validar si las listas tienen cosas, y esas cosas pasarlas de alguna manera al reporte de jasper como listas o cosas independientes

    return render_to_response('reportes_compras/reporte_compras_fecha_proveedor.html',
                              {"buscador": buscador,
                               "lista_saldo_prov": lista_saldo_prov,
                               "centro_costos": centro_costos,
                               "todosloscentroscostos": todosloscentroscostos,
                               "lista_centro_costos": lista_centro_costos,
                               "proveedores": proveedores
                               }, context_instance=RequestContext(request))

#funcion que ayuda en este caso
def listar_proveedores(buscador, fecha_ini, fecha_final, lista_saldo_prov, proveedores, lista_id_prov,lista_idproveedores,numeroestodoslosproveedores):
    """
    Lista cada uno de lops proveedores con las transacciones dependiendo de la fecha
    inicial y final
    :param buscador:
    :param fecha_ini:
    :param fecha_final:
    :param lista_saldo_prov:
    :param proveedores:
    :param lista_id_prov:
    :return:
    """
    buscador.is_valid()  # Se llama a la función para usar cleaned_data
    if buscador.getAllProv():#si es que se marce al check de todos los proveedores
        all_provee = Proveedores.objects.filter(status=1).order_by("razon_social")
        numeroestodoslosproveedores = 1
        for proveedor in all_provee:
            list_rep = []
            lista_saldo_prov.append(proveedor)
            lista_idproveedores.append(proveedor.id)#de igual manera se agrega caaada id d eproveedor del conjunto de tooodos los proveedores  a la lista
    else:
        if proveedores.is_valid():
            for obj in proveedores:
                informacion = obj.cleaned_data
                try:
                    list_rep = []
                    proveedor = Proveedores.objects.get(id=informacion.get("proveedor"))
                    # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                    if proveedor.id not in lista_id_prov:
                        lista_id_prov.append(proveedor.id)
                        lista_saldo_prov.append(proveedor)
                        lista_idproveedores.append(proveedor.id)
                except:
                    pass

"""
@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha_pdf(request):
    query = []
    base0 = 0.0
    baseiva = 0.0
    monto = 0.0
    total = 0.0
    ret_fte = 0.0
    ret_iva = 0.0
    pagar = 0.0

    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni().strftime("%Y-%m-%d")
                fecha_final = buscador.getFechaFinal().strftime("%Y-%m-%d")

                query = CompraCabecera.objects.exclude(Q(status=0)|Q(status=3)).filter(fecha_reg__range=(fecha_ini, fecha_final)).order_by("fecha_reg", "num_comp")
                base0 = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('base0', field="base0"))["base0__sum"])
                baseiva = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('baseiva', field="baseiva"))["baseiva__sum"])
                monto = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('monto_iva', field="monto_iva"))["monto_iva__sum"])
                total = convert_cero_none(CompraCabecera.objects.filter(status=1).exclude(documento__codigo_documento="04").filter(fecha_reg__range=(fecha_ini, fecha_final)).aggregate(Sum('monto_iva', field="base0 + baseiva + monto_iva"))["monto_iva__sum"])
                ret_fte = convert_cero_none(RetencionCompraCabecera.objects.filter(status=1).filter(compra_cabecera__in=CompraCabecera.objects.filter(fecha_reg__range=(fecha_ini, fecha_final))).aggregate(Sum('totalretfte', field="totalretfte"))["totalretfte__sum"])
                ret_iva = convert_cero_none(RetencionCompraCabecera.objects.filter(status=1).filter(compra_cabecera__in=CompraCabecera.objects.filter(fecha_reg__range=(fecha_ini, fecha_final))).aggregate(Sum('totalretiva', field="totalretiva"))["totalretiva__sum"])
                pagar = base0 + baseiva + monto - ret_fte - ret_iva

            else:
                query = []
                base0 = 0.0
                baseiva = 0.0
                monto = 0.0
                total = 0.0
                ret_fte = 0.0
                ret_iva = 0.0
                pagar = 0.0
        else:
            ### error
            print "error form"

    return generar_pdf('reportes_compras/reporte_compras_pdf.html',
                               {'pagesize': 'A4',
                               "empresa":empresa,
                               "usuario":usuario,
                               "buscador": buscador, "objetos": query,
                               "base0": base0,
                               "baseiva": baseiva,
                               "monto": monto,
                               "total": total,
                               "ret_fte": ret_fte,
                               "ret_iva": ret_iva,
                               "pagar": pagar,
                               "request": request})

"""

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_por_fecha_pdf_23oct(request):
    buscador = BuscadorReportes(request.GET)
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    r = None
    urljasper = get_url_jasper()
    if buscador.is_valid():
        if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
            fecha_ini = buscador.getFechaIni()
            fecha_final = buscador.getFechaFinal()
            # Path to resource rest service:
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:
            report = get_nombre_reporte('CMP_COMPRA') + '.pdf'
            #report = 'libro_mayor.pdf'
            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            # Params
            params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_final.strftime("%Y-%m-%d")
                      }

            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)

            response = HttpResponse(r.content, content_type='application/pdf')
            return response
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    return render_to_response('reportes_compras/reporte_compras.html',
                              {"buscador": buscador, "objetos": r,
                               }, context_instance=RequestContext(request))
########################################################################################################################


class RepSaldoCtaXpagar():
    """
    clase que me ayuda a ordenar las cuentas por pagar con
    sus respectivos pagos
    """
    cuenta_x_pagar = Cuentas_por_Pagar()
    lista_pagos = []

    def __init__(self):
        pass


@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldo_ctas_x_pagar(request):
    buscador = BuscadorReportes(request.GET)
    buscador.is_valid()  # Para que funcione el cleaned data
    lista = []
    if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
        fecha_ini = buscador.getFechaIni().strftime("%Y-%m-%d")
        fecha_final = buscador.getFechaFinal().strftime("%Y-%m-%d")
        query = Cuentas_por_Pagar.objects.exclude(status=0).filter(fecha_reg__range=(fecha_ini, fecha_final)).order_by("fecha_reg")
        for obj in query:
            rep_sal_cta_pagar = RepSaldoCtaXpagar()
            rep_sal_cta_pagar.cuenta_x_pagar = obj
            rep_sal_cta_pagar.lista_pagos = obj.get_lista_pagos_x_fecha(fecha_ini, fecha_final)
            lista.append(rep_sal_cta_pagar)
    else:
        lista = []
    return render_to_response('reportes_compras/saldo_cuentas_por_pagar.html',
                              {"buscador": buscador, "objetos": lista,
                               }, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_cartera_compras(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01"
                                         })
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None:
                # tipo_reporte 1 estado 1
                # tipo_reporte 1 estado 2
                # tipo_reporte 2 estado 1
                # tipo_reporte 2 estado 2
                if int(buscador.getTipoReporte()) == 1 and buscador.getEstadoReporte() == 1:
                    print 'resumido vencido'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('COMPRA_RESU_CXP_VENCIDO') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                    #r = s.get(url=url+report, auth=auth, params=params)

                if buscador.getTipoReporte() == 1 and buscador.getEstadoReporte() == 2:
                    print 'resumido por vencer'#estos 2 deben ser los detallados, no los resumidos
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('COMPRA_RESU_CXP_POR_VENCER') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                if buscador.getTipoReporte() == 2 and buscador.getEstadoReporte() == 1:
                    print 'detallado vencido'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('COMPRA_CXP_VENCIDO') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                if buscador.getTipoReporte() == 2 and buscador.getEstadoReporte() == 2:
                    print 'detallado por vencer'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('COMPRA_CXP_POR_VENCER') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }
                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                '''
                fecha_ini = buscador.getFechaIni()
                print (buscador.getEstadoReporte())
                print (buscador.getTipoReporte())

                print(str(fecha_ini))
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'
                print str(url)
                report = get_nombre_reporte('CMP_CARTERA_COMPRA') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)
                print(str(report)),'urlrep'
                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                '''
                r = s.get(url=url+report, auth=auth, params=params)
        else:
            print 'el buscador esta mal'
            errors = buscador._errors.setdefault("estado_reporte", ErrorList())
            errors.append(u"La fecha inicial es requerida")
    else:
        print 'algo pasa y no es post'
    return render_to_response('reportes_compras/reporte_cartera_compras.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cartera_compras_excel_hasta7agosto(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01"
                                         })
    #buscador = BuscadorReportes(request.GET)
    buscador.is_valid()  # Para que funcione el cleaned data
    fecha_ini = buscador.getFechaIni()
    print(str(fecha_ini))
    urljasper = get_url_jasper()
    if None not in (fecha_ini):
        # Path to resource rest service:
        url = urljasper + empresa.path_jasper + '/'
        # Report to process:
        report = get_nombre_reporte('CMP_CARTERA_COMPRA') + '.xls'
        # Authorisation credentials:
        auth = (empresa.user_jasper, empresa.pass_jasper)
        # Params
        params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                  }

        # Init session so we have no need to auth again and again:
        s = requests.Session()
        r = s.get(url=url+report, auth=auth, params=params)

        response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
        nombre = "Cartera_fecha_"+str(fecha_ini)
        response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
        return response


#las 4 cosas pero ahora en excel :o

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_cartera_compras_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01"
                                        })
    print('este reporte uq no anda')
    r = None
    s = None
    post = False  # flag template message
    if request.method == "GET":
        buscador = BuscadorReportes(request.GET)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None:
                # tipo_reporte 1 estado 1
                # tipo_reporte 1 estado 2
                # tipo_reporte 2 estado 1
                # tipo_reporte 2 estado 2
                if int(buscador.getTipoReporte()) == 1 and buscador.getEstadoReporte() == 1:
                    print 'pepinresumido vencido'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('COMPRA_RESU_CXP_VENCIDO') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                    r = s.get(url=url+report, auth=auth, params=params)
                    print('casi termino la funcion')
                    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                    nombre = "CXP_por_plazoresumido vencido "+str(fecha_ini)
                    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
                    return response
                    #r = s.get(url=url+report, auth=auth, params=params)

                if buscador.getTipoReporte() == 1 and buscador.getEstadoReporte() == 2:
                    print 'pepinresumido por vencer'#estos 2 deben ser los detallados, no los resumidos
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('COMPRA_RESU_CXP_POR_VENCER') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                    r = s.get(url=url+report, auth=auth, params=params)
                    print('casi termino la funcion')
                    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                    nombre = "CXP_por_plazoresumido por vencer"+str(fecha_ini)
                    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
                    return response
                if buscador.getTipoReporte() == 2 and buscador.getEstadoReporte() == 1:
                    print 'pepindetallado vencido'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('COMPRA_CXP_VENCIDO') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                    r = s.get(url=url+report, auth=auth, params=params)
                    print('casi termino la funcion')
                    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                    nombre = "CXPVencidaDetallado "+str(fecha_ini)
                    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
                    return response

                if buscador.getTipoReporte() == 2 and buscador.getEstadoReporte() == 2:
                    print 'pepindetallado por vencer'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('COMPRA_CXP_POR_VENCER') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }
                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                    r = s.get(url=url+report, auth=auth, params=params)
                    print('casi termino la funcion')
                    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                    nombre = "CXP_por_plazodetallado por vencer "+str(fecha_ini)
                    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
                    return response
    else:
        print('falla el post')



@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_x_proveedor(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        #cuentas = cuentas_formset(request.POST, prefix="cuentas")
        proveedores = proveed_formset(request.POST, prefix='proveedores')
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('COMPRA_COMPRAS_X_PROVEEDOR') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if proveedores.is_valid():
                for obj in proveedores:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Proveedores.objects.get(id=informacion.get("proveedor"))
                        id_proveedores += str(proveedor.id) + ","
                    except Proveedores.DoesNotExist:
                        pass
            else:
                print('no hay proveedoresss')
            print(str(id_proveedores))
            if buscador.getAllProv():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'proveedor': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    return render_to_response('reportes_compras/reporte_compras_x_proveedor.html',
                              {"buscador": buscador,
                               "lista": r,#"lista": r,
                               "proveedores": proveedores
                               }, context_instance=RequestContext(request))



@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_x_proveedor_excel(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        #cuentas = cuentas_formset(request.POST, prefix="cuentas")
        proveedores = proveed_formset(request.POST, prefix='proveedores')
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('COMPRA_COMPRAS_X_PROVEEDOR') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if proveedores.is_valid():
                for obj in proveedores:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Proveedores.objects.get(id=informacion.get("proveedor"))
                        id_proveedores += str(proveedor.id) + ","
                    except Proveedores.DoesNotExist:
                        pass
            else:
                print('no hay proveedoresss')
            print(str(id_proveedores))
            if buscador.getAllProv():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'proveedor': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
    nombre = "ComprasXproveedor_"+str(fecha_ini)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    return response



@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_x_cuenta(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    cuentas = proveed_formset(prefix='cuenta')
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        #cuentas = cuentas_formset(request.POST, prefix="cuentas")
        cuentas = proveed_formset(request.POST, prefix='cuenta')
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('COMPRA_COMPRAS_X_CUENTA') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    print('esto es informacion')
                    print str(informacion)
                    try:
                        proveedor = PlanCuenta.objects.get(id=informacion.get("cuenta"))
                        id_proveedores += str(proveedor.id) + ","
                    except Proveedores.DoesNotExist:
                        pass
            else:
                print('no hay cuentassss')
            print(str(id_proveedores))
            if buscador.getAllProv():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'cuenta': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    return render_to_response('reportes_compras/reporte_compras_x_cuenta.html',
                              {"buscador": buscador,
                               "lista": r,#"lista": r,
                               "proveedores": cuentas
                               }, context_instance=RequestContext(request))



@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_compras_x_cuenta_excel(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    cuentas = proveed_formset(prefix='cuenta')
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        #cuentas = cuentas_formset(request.POST, prefix="cuentas")
        cuentas = proveed_formset(request.POST, prefix='cuenta')
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('COMPRA_COMPRAS_X_CUENTA') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if cuentas.is_valid():
                for obj in cuentas:
                    informacion = obj.cleaned_data
                    print('esto es informacion')
                    print str(informacion)
                    try:
                        proveedor = PlanCuenta.objects.get(id=informacion.get("cuenta"))
                        id_proveedores += str(proveedor.id) + ","
                    except Proveedores.DoesNotExist:
                        pass
            else:
                print('no hay cuentassss')
            print(str(id_proveedores))
            if buscador.getAllProv():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'cuenta': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    '''
    return render_to_response('reportes_compras/reporte_compras_x_cuenta.html',
                              {"buscador": buscador,
                               "lista": r,#"lista": r,
                               "proveedores": cuentas
                               }, context_instance=RequestContext(request))
    '''
    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
    nombre = "ComprasXCuenta_"+str(fecha_ini)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    return response



@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_estadistica_compras_x_proveedor(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                report = get_nombre_reporte('COMPRA_ESTAD_CP_PROVEEDOR') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reportes_compras/reporte_estadistica_compras_proveedor.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))



#@login_required(login_url='/')
#@csrf_exempt
@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_estadistica_compras_x_proveedor_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    print 'estadisticadecompraxproveedor'
    #post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        #post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                report = get_nombre_reporte('COMPRA_ESTAD_CP_PROVEEDOR') + '.xls'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                print 'algoooo'
                r = s.get(url=url+report, auth=auth, params=params)
                print r.content
        else:
            print('esta cosa se caeeeeee')

    else:
        print('no era post')
    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
    nombre = "Estadisticopor_proveedor_"+str(fecha_ini)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    return response
    '''
    return render_to_response('reportes_compras/reporte_estadistica_compras_proveedor.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))
    '''

