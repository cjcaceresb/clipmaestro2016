#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from librerias.funciones.funciones_vistas import *
from librerias.funciones.excel_response_mod import ExcelResponse
from reportes.formularios.reportes_form import *
import xlwt
from django.db.models import Sum
from django.db import connection
import operator
import requests
from django.db.models import F
from django.forms.formsets import formset_factory
from librerias.funciones.excel_response_mod import *
from librerias.funciones.permisos import *
from reportes.vistas.funciones import *
#cosas del reporte de libro mayor
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import connection
from django.core.paginator import *
from django import forms, template
from django.utils.html import *
from contabilidad.models import *
from librerias.funciones.excel_response_mod import *
import xlwt
from django.db.models import Sum
import operator
from reportes.formularios.reportes_form import *
from django.forms.formsets import formset_factory
from reportes.models import *
from funciones import *
from librerias.funciones.funciones_vistas import *
import calendar
from django.forms.util import ErrorList
from contabilidad.funciones.contabilidad_func import *
from librerias.funciones.paginacion import *
from librerias.funciones.permisos import *
import requests
from django.db import connection



@csrf_exempt
def get_saldo_prov(proveedor, fecha_ini):
    """
    Función que retorna el saldo del proveedor dependiendo de una fecha
    específica
    :param proveedor:
    :param fecha_ini:
    :return: float
    """

    monto_deudor1 = Cuentas_por_Pagar.objects.filter(proveedor=proveedor, naturaleza=1,
                                                     fecha_reg__lt=fecha_ini,
                                                     status=1).aggregate(Sum('monto', field="monto"))["monto__sum"]
    monto_acreedor1 = Cuentas_por_Pagar.objects.filter(proveedor=proveedor, naturaleza=2,
                                                       fecha_reg__lt=fecha_ini,
                                                       status=1).aggregate(Sum('monto', field="monto"))["monto__sum"]

    monto_deudor2 = CXP_pago.objects.exclude(pago__num_comp=F("cuentas_x_pagar__num_doc")).filter(
        pago__proveedor=proveedor,
        cuentas_x_pagar__naturaleza=2,
        pago__fecha_reg__lt=fecha_ini,
        status=1).aggregate(Sum('monto', field="cxp_pago.monto"))["monto__sum"]

    monto_acreedor2 = CXP_pago.objects.exclude(pago__num_comp=F("cuentas_x_pagar__num_doc")).filter(
        pago__proveedor=proveedor, cuentas_x_pagar__naturaleza=1,
        pago__fecha_reg__lt=fecha_ini,
        status=1).aggregate(Sum('monto', field="cxp_pago.monto"))["monto__sum"]

    if monto_deudor1 is None:
        monto_deudor1 = 0.0
    else:
        monto_deudor1 = float(monto_deudor1)

    if monto_deudor2 is None:
        monto_deudor2 = 0.0
    else:
        monto_deudor2 = float(monto_deudor2)

    if monto_acreedor1 is None:
        monto_acreedor1 = 0.0
    else:
        monto_acreedor1 = float(monto_acreedor1)

    if monto_acreedor2 is None:
        monto_acreedor2 = 0.0
    else:
        monto_acreedor2 = float(monto_acreedor2)

    return round(((monto_acreedor1 + monto_acreedor2) - (monto_deudor1 + monto_deudor2)), 2)


class ListRepProv():
    """
    Clase que me ayuda a agrupar las cuentas por pagar
    y los pagos, esto para el reporte de saldo de
    proveedores
    """
    def __init__(self, tipo, fecha_reg, num_doc):
        self.tipo = tipo
        self.fecha_reg = fecha_reg
        self.num_doc = num_doc
        self.cuentas_x_pagar = Cuentas_por_Pagar()
        self.cxp_pago = CXP_pago()
        self.saldo = 0.0


class DatosProveSaldos():
    """
    Clase que me ayuda a agrupar el saldo de cada proveedor
    ademas de los datos del proveedor y la lista de las transacciones
    """
    def __init__(self, saldo):
        self.saldo = saldo
        self.lista_saldo = []
        self.proveedor = Proveedores()
        self.saldo_deudor = 0.0
        self.saldo_acreedor = 0.0
        self.total_saldo_pendiente = 0.0


def ord_list_fecha(proveedor, fecha_ini, fecha_final, list_rep, saldo_ini):
    """
    Función que ordena la clase ListRepProv, por un
    rango de fechas
    :param proveedor:
    :param fecha_ini:
    :param fecha_final:
    :param list:
    :return:
    """
    saldo_deudor = 0.0
    saldo_acreedor = 0.0
    cta_pag = Cuentas_por_Pagar.objects.filter(proveedor=proveedor,
                                               fecha_reg__range=(fecha_ini, fecha_final),
                                               status=1).order_by("-fecha_reg")

    pagos = Pago.objects.filter(proveedor=proveedor,
                                fecha_reg__range=(fecha_ini, fecha_final),
                                status=1).order_by("fecha_reg")
    for obj in cta_pag:
        list_rep_pro = ListRepProv(1, obj.fecha_reg, obj.num_doc)
        list_rep_pro.cuentas_x_pagar = obj
        list_rep.append(list_rep_pro)

    for obj in pagos:
        for obj_cxp_pago in obj.getCxPpago():
            # Se valida que el pago no sea el del anticipo, si no hubiera duplicados
            if obj_cxp_pago.cuentas_x_pagar.num_doc != obj_cxp_pago.pago.num_comp:
                list_rep_pro = ListRepProv(2, obj.fecha_reg, obj_cxp_pago.cuentas_x_pagar.num_doc)
                list_rep_pro.cxp_pago = obj_cxp_pago
                list_rep.append(list_rep_pro)

    list_rep.sort(key=operator.attrgetter('num_doc', 'fecha_reg'))
    saldo = saldo_ini

    for obj_list in list_rep:
        if obj_list.tipo == 1:
            if obj_list.cuentas_x_pagar.naturaleza == 1:
                saldo -= obj_list.cuentas_x_pagar.monto
                saldo_deudor += obj_list.cuentas_x_pagar.monto
            else:
                saldo += obj_list.cuentas_x_pagar.monto
                saldo_acreedor += obj_list.cuentas_x_pagar.monto
        else:
            if obj_list.cxp_pago.cuentas_x_pagar.naturaleza == 1:
                saldo += obj_list.cxp_pago.monto
                saldo_acreedor += obj_list.cxp_pago.monto
            else:
                saldo -= obj_list.cxp_pago.monto
                saldo_deudor += obj_list.cxp_pago.monto
        obj_list.saldo = saldo

    return saldo_deudor, saldo_acreedor


def listar_saldo_proveedor(buscador, fecha_ini, fecha_final, lista_saldo_prov, proveedores, lista_id_prov):
    """
    Lista cada uno de lops proveedores con las transacciones dependiendo de la fecha
    inicial y final
    :param buscador:
    :param fecha_ini:
    :param fecha_final:
    :param lista_saldo_prov:
    :param proveedores:
    :param lista_id_prov:
    :return:
    """
    buscador.is_valid()  # Se llama a la función para usar cleaned_data
    if buscador.getAllProv():
        all_provee = Proveedores.objects.filter(status=1).order_by("razon_social")
        for proveedor in all_provee:

            list_rep = []
            saldo_proveedor = get_saldo_prov(proveedor, fecha_ini)
            saldo_deudor, saldo_acreedor = ord_list_fecha(proveedor, fecha_ini, fecha_final, list_rep, saldo_proveedor)

            if saldo_proveedor != 0 or len(list_rep) != 0:
                datos_prov = DatosProveSaldos(saldo_proveedor)
                datos_prov.proveedor = proveedor
                datos_prov.lista_saldo = list_rep
                datos_prov.saldo_deudor = saldo_deudor
                datos_prov.saldo_acreedor = saldo_acreedor

                lista_saldo_prov.append(datos_prov)
    else:
        if proveedores.is_valid():
            for obj in proveedores:
                informacion = obj.cleaned_data
                try:
                    list_rep = []
                    proveedor = Proveedores.objects.get(id=informacion.get("proveedor"))
                    # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                    if proveedor.id not in lista_id_prov:
                        lista_id_prov.append(proveedor.id)
                        saldo_proveedor = get_saldo_prov(proveedor, fecha_ini)
                        saldo_deudor, saldo_acreedor = ord_list_fecha(proveedor, fecha_ini, fecha_final, list_rep, saldo_proveedor)

                        if saldo_proveedor != 0 or len(list_rep) != 0:
                            datos_prov = DatosProveSaldos(saldo_proveedor)
                            datos_prov.proveedor = proveedor
                            datos_prov.lista_saldo = list_rep
                            datos_prov.saldo_deudor = saldo_deudor
                            datos_prov.saldo_acreedor = saldo_acreedor

                            lista_saldo_prov.append(datos_prov)
                except:
                    pass




@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_monto_proveedor_por_fecha(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        #cuentas = cuentas_formset(request.POST, prefix="cuentas")
        proveedores = proveed_formset(request.POST, prefix='proveedores')
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('PROVEEDOR_ESTADO_CTA_PROVEEDOR') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if proveedores.is_valid():
                for obj in proveedores:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Proveedores.objects.get(id=informacion.get("proveedor"))
                        id_proveedores += str(proveedor.id) + ","
                    except Proveedores.DoesNotExist:
                        pass
            else:
                print('no hay proveedoresss')
            print(str(id_proveedores))
            if buscador.getAllProv():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'proveedor': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    return render_to_response('reporte_proveedores/reporte_saldo_proveedor.html',
                              {"buscador": buscador,
                               "lista": r,#"lista": r,
                               "proveedores": proveedores
                               }, context_instance=RequestContext(request))



@login_required()
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_monto_proveedor_por_fecha(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(now.year)+"-01-01",
                                         "fecha_final": str(now.strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_fin = buscador.getFechaFinal()
        #cuentas = cuentas_formset(request.POST, prefix="cuentas")
        proveedores = proveed_formset(request.POST, prefix='proveedores')
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if None not in(fecha_ini, fecha_fin):
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('PROVEEDOR_ESTADO_CTA_PROVEEDOR') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if proveedores.is_valid():
                for obj in proveedores:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Proveedores.objects.get(id=informacion.get("proveedor"))
                        id_proveedores += str(proveedor.id) + ","
                    except Proveedores.DoesNotExist:
                        pass
            else:
                print('no hay proveedoresss')
            print(str(id_proveedores))
            if buscador.getAllProv():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'proveedor': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerido")
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    return render_to_response('reporte_proveedores/reporte_saldo_proveedor.html',
                              {"buscador": buscador,
                               "lista": r,#"lista": r,
                               "proveedores": proveedores
                               }, context_instance=RequestContext(request))



@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_monto_proveedor_por_fecha_pdf(request):
    """
    Reporte de saldos de proveedor por fecha
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    lista_saldo_prov = []  # lista de saldo de proveedores
    lista_id_prov = []  # Guarda el id del registro del proveedor, para que no se repitan datos

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            proveedores = proveed_formset(request.POST, prefix='proveedores')
            # Si escojió la opción de todos los proveedores
            listar_saldo_proveedor(buscador, fecha_ini, fecha_final, lista_saldo_prov, proveedores, lista_id_prov)

    html = render_to_string('reporte_proveedores/reporte_saldo_proveedor_pdf.html',
                            {'pagesize': 'A4', "buscador": buscador, "lista_saldo_prov": lista_saldo_prov,
                             "proveedores": proveedores, "empresa": empresa, "usuario": usuario, "request": request
                             }, context_instance=RequestContext(request))
    nombre = ( 'Rpt_Estado_cta_Proveedores_' + fecha_ini.strftime("%Y-%m-%d") + '_a_' + fecha_final.strftime("%Y-%m-%d"))
    return generar_pdf_nombre(html, nombre)


@csrf_exempt
def listar_all_proveedores(lista, lista_id_prov, fecha_ini, fecha_final):
    """
    inserta en una lista todos los movimientos de los proveedores en una fecha
    indicada para la hoja de excel
    :param lista:
    :param lista_id_prov:
    :param fecha_ini:
    :param fecha_final:
    :return:
    """
    all_provee = Proveedores.objects.filter(status=1).order_by("razon_social")
    for proveedor in all_provee:
        list_rep = []
        lista_id_prov.append(proveedor.id)
        saldo_proveedor = get_saldo_prov(proveedor, fecha_ini)
        saldo_deudor, saldo_acreedor = ord_list_fecha(proveedor, fecha_ini, fecha_final, list_rep, saldo_proveedor)
        lista.append([u"Proveedor: " + proveedor.razon_social + u"(" + proveedor.ruc + u")%/%1"])
        lista.append([u"Fecha%/%2", u"Tipo Doc.%/%2", u"# Documento%/%2", u"Débito%/%2", u"Crédito%/%2",
                      u"Forma de Pago%/%2", u"Saldo Doc.%/%2", u"Saldo Proveedor%/%2"])
        lista.append([u"", u"", u"", u"", u"", u"", u"", round(saldo_proveedor, 2)])
        for obj_list_rep in list_rep:
            if obj_list_rep.tipo == 1:
                if obj_list_rep.cuentas_x_pagar.naturaleza == 1:
                    lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                  obj_list_rep.cuentas_x_pagar.documento.descripcion_documento,
                                  obj_list_rep.cuentas_x_pagar.num_doc, round(obj_list_rep.cuentas_x_pagar.monto, 2), "", "",
                                  round(obj_list_rep.cuentas_x_pagar.monto, 2), round(obj_list_rep.saldo, 2)])
                else:
                    lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                  obj_list_rep.cuentas_x_pagar.documento.descripcion_documento,
                                  obj_list_rep.cuentas_x_pagar.num_doc, "", round(obj_list_rep.cuentas_x_pagar.monto, 2), "",
                                  round(obj_list_rep.cuentas_x_pagar.monto, 2), round(obj_list_rep.saldo, 2)])
            else:
                if obj_list_rep.cuentas_x_pagar.num_doc != obj_list_rep.cxp_pago.pago.num_comp:
                    if obj_list_rep.cxp_pago.cuentas_x_pagar.naturaleza == 2:
                        lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                      obj_list_rep.cxp_pago.cuentas_x_pagar.documento.descripcion_documento,
                                      obj_list_rep.cxp_pago.cuentas_x_pagar.num_doc, round(obj_list_rep.cxp_pago.monto, 2),
                                      "", obj_list_rep.cxp_pago.pago.get_str_forma_pago() + " - " + obj_list_rep.cxp_pago.pago.num_comp,
                                      round(obj_list_rep.cxp_pago.get_saldo_doc(), 2), round(obj_list_rep.saldo, 2)])
                    else:
                        lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                      obj_list_rep.cxp_pago.cuentas_x_pagar.documento.descripcion_documento,
                                      obj_list_rep.cxp_pago.cuentas_x_pagar.num_doc, "", round(obj_list_rep.cxp_pago.monto, 2),
                                      obj_list_rep.cxp_pago.pago.get_str_forma_pago() + " - " + obj_list_rep.cxp_pago.pago.num_comp,
                                      round(obj_list_rep.cxp_pago.get_saldo_doc(), 2), round(obj_list_rep.saldo, 2)])
        lista.append(["", "", "", str(round(saldo_deudor, 2))+"%/%3", str(round(saldo_acreedor, 2))+"%/%3"])
        for i in range(0, 2):
            lista.append([u"", u"", u"", u"", u"", u"", u"", u""])

@csrf_exempt
def listar_prove_select(obj, lista, lista_id_prov, fecha_ini, fecha_final):
    """
    inserta en una lista todos los movimientos de los proveedores indicados en el formulario
    en una fecha indicada para la hoja de excel
    :param obj:
    :param lista:
    :param lista_id_prov:
    :param fecha_ini:
    :param fecha_final:
    :return:
    """
    informacion = obj.cleaned_data
    try:
        list_rep = []
        proveedor = Proveedores.objects.get(id=informacion.get("proveedor"))
        # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
        if proveedor.id not in lista_id_prov:
            lista_id_prov.append(proveedor.id)
            saldo_proveedor = get_saldo_prov(proveedor, fecha_ini)
            saldo_deudor, saldo_acreedor = ord_list_fecha(proveedor, fecha_ini, fecha_final, list_rep, saldo_proveedor)
            lista.append([u"Proveedor: " + proveedor.razon_social + u"(" + proveedor.ruc + u")%/%1"])
            lista.append([u"Fecha%/%2", u"Tipo Doc.%/%2", u"# Documento%/%2", u"Débito%/%2",
                          u"Crédito%/%2", u"Forma de Pago%/%2", u"Saldo Doc.%/%2", u"Saldo Proveedor%/%2"])
            lista.append([u"", u"", u"", u"", u"", u"", u"", round(saldo_proveedor, 2)])
            for obj_list_rep in list_rep:
                if obj_list_rep.tipo == 1:
                    if obj_list_rep.cuentas_x_pagar.naturaleza == 1:
                        lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                      obj_list_rep.cuentas_x_pagar.documento.descripcion_documento,
                                      obj_list_rep.cuentas_x_pagar.num_doc, round(obj_list_rep.cuentas_x_pagar.monto, 2), "", "",
                                      round(obj_list_rep.cuentas_x_pagar.monto, 2), round(obj_list_rep.saldo, 2)])
                    else:
                        lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                      obj_list_rep.cuentas_x_pagar.documento.descripcion_documento,
                                      obj_list_rep.cuentas_x_pagar.num_doc, "", round(obj_list_rep.cuentas_x_pagar.monto, 2),
                                      "", round(obj_list_rep.cuentas_x_pagar.monto, 2), round(obj_list_rep.saldo, 2)])
                else:
                    if obj_list_rep.cxp_pago.cuentas_x_pagar.num_doc != obj_list_rep.cxp_pago.pago.num_comp:
                        if obj_list_rep.cxp_pago.cuentas_x_pagar.naturaleza == 2:
                            lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                          obj_list_rep.cxp_pago.cuentas_x_pagar.documento.descripcion_documento,
                                          obj_list_rep.cxp_pago.cuentas_x_pagar.num_doc, round(obj_list_rep.cxp_pago.monto, 2),
                                          "", obj_list_rep.cxp_pago.pago.get_str_forma_pago() + " - " + obj_list_rep.cxp_pago.pago.num_comp,
                                          round(obj_list_rep.cxp_pago.get_saldo_doc(), 2), round(obj_list_rep.saldo, 2)])
                        else:
                            lista.append([obj_list_rep.fecha_reg.strftime("%Y-%m-%d"),
                                          obj_list_rep.cxp_pago.cuentas_x_pagar.documento.descripcion_documento,
                                          obj_list_rep.cxp_pago.cuentas_x_pagar.num_doc, "", round(obj_list_rep.cxp_pago.monto, 2),
                                          obj_list_rep.cxp_pago.pago.get_str_forma_pago() + " - " + obj_list_rep.cxp_pago.pago.num_comp,
                                          round(obj_list_rep.cxp_pago.get_saldo_doc(), 2), round(obj_list_rep.saldo, 2)])

            lista.append(["", "", "", str(round(saldo_deudor, 2))+"%/%3", str(round(saldo_acreedor, 2))+"%/%3"])
            for i in range(0, 2):
                lista.append([u"", u"", u"", u"", u"", u"", u""])
    except:
        pass


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_monto_proveedor_por_fecha_excel(request):
    """
    Retorna una hoja de excel con el estado de cuenta del proveedor
    :param request:
    :return: Excel sheet
    """
    lista = [u""]
    lista.append([u"", u"", u"", u"REPORTE ESTADO DE CUENTA PROVEEDORES%/%1"])
    lista.append([u""])
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        proveed_formset = formset_factory(ReporteSaldosForm)
        lista_id_prov = []  # Lista que contiene los id de los proveedores para que no hayan duplicados en el reporte

        if None not in (fecha_ini, fecha_final):
            lista.append([u"Fecha Inicial:%/%2",fecha_ini.strftime("%d-%m-%Y")])
            lista.append([u"Fecha Final:%/%2", fecha_final.strftime("%d-%m-%Y")])
            proveedores = proveed_formset(request.POST, prefix='proveedores')
            # Si escojió la opción de todos los proveedores
            buscador.is_valid()  # Se llama a la función para usar cleaned_data
            if buscador.getAllProv():
                listar_all_proveedores(lista, lista_id_prov, fecha_ini, fecha_final)
            else:
                if proveedores.is_valid():
                    for obj in proveedores:
                        listar_prove_select(obj, lista, lista_id_prov, fecha_ini, fecha_final)

        return ExcelResponse(lista, 'Rpt_Estado_cta_Proveedores_' + fecha_ini.strftime("%Y-%m-%d") + '_a_' + fecha_final.strftime("%Y-%m-%d"))
    else:
        return ExcelResponse(lista)


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_proveedores(request):
    #items = Proveedores.objects.filter(status=1).order_by('razon_social')#solo los activos, pero entonces para que quiere mostrar el estado si todos son activos
    items = Proveedores.objects.all().order_by('razon_social')
    datos = {'items': items}
    return render_to_response('reporte_proveedores/reporte_proveedores.html', datos,
                              context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_proveedores_excel(request):
    items = Proveedores.objects.all().order_by('razon_social')
    lista = []
    lista.append(['','','Reporte de Proveedores'+TITULO])
    lista.append([u'Fecha de Generación: '+SUBTITULO, str(datetime.datetime.now().strftime('%Y-%m-%d'))+SUBTITULO])
    lista.append([''])
    cabecera = [u'Tipo de Persona'+COLUMNA,u'Tipo de Identificación'+COLUMNA,u'Identificación'+COLUMNA,u'Razón Social'+COLUMNA,u'Actividad'+COLUMNA,u'Dirección'+COLUMNA,u'Teléfono'+COLUMNA,u'Correo Electrónico'+COLUMNA,u'Estado'+COLUMNA]
    lista.append(cabecera)

    if items:
        for item in items:
            miactividad = 'No Definida'
            direccion = u'Dirección no definida'
            telefono = 'No Definido'
            correo = 'No Definido'
            estado = 'Inactivo'
            try:
                if item.actividad.descripcion is not None and item.actividad is not None:
                    miactividad = item.actividad.descripcion
            except:
                miactividad = 'No Definida'

            if item.direccion:
                direccion = item.direccion
            if item.telefono:
                telefono = item.telefono
            if item.email:
                correo = item.email

            if item.status == 1:
                estado = 'Activo'

            row = [
                item.tipo_proveedor.descripcion,
                item.tipo_identificacion.descripcion,
                item.ruc,
                item.razon_social,
                miactividad,
                direccion,
                telefono,
                correo,
                estado]
            lista.append(row)

    return ExcelResponse(lista, 'Reporte de Proveedores a la fecha del '+str(datetime.datetime.now().strftime('%Y-%m-%d')))


@csrf_exempt
@login_required(login_url="/")
@permiso_accion(mensaje=mensaje_permiso)
def reporte_proveedores_pdf(request):
    """
    Reporte de saldos de proveedor por fecha
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    items = Proveedores.objects.all().order_by('razon_social')
    html = render_to_string('reporte_proveedores/reporte_proveedores_pdf.html',
                            {'pagesize': 'A4', "items": items, "empresa": empresa, "usuario": usuario,
                             "request": request})

    return generar_pdf_get(html)


def get_cta_pagar_pendientes_query(fecha_corte, id_proveedor):
    return '''
            SELECT distinct c.id
            FROM cuentas_x_pagar as c
                WHERE (c.status = 1  AND
                c.fecha_reg <=\'''' + fecha_corte +'''\' AND
                c.monto >  coalesce((select SUM(cxp_pago.monto) from cxp_pago INNER JOIN pago on (cxp_pago.pago_id=pago.id and c.id = cxp_pago.cuentas_x_pagar_id)
                            where cxp_pago.status = 1  AND pago.fecha_reg <=\'''' + fecha_corte +'''\' AND pago.status=1
                            and c.id=cxp_pago.cuentas_x_pagar_id and c.num_doc != pago.num_comp)::decimal, 0) AND
                c.proveedor_id =''' + str(id_proveedor) + ''')
            '''


def get_pagado(fecha_corte, list_cta_pagar):
    pagado = convert_cero_none(CXP_pago.objects.filter(cuentas_x_pagar__in=(x.id for x in list_cta_pagar),
                                                       status=1,
                                                       pago__fecha_reg__lte=fecha_corte).
        filter(~Q(cuentas_x_pagar__num_doc=F("pago__num_comp"))).aggregate(Sum('monto',
                                                                               field="cxp_pago.monto"))["monto__sum"])
    return pagado


def get_pagado_naturaleza(fecha_corte, list_cta_pagar, naturaleza):
    pagado = convert_cero_none(CXP_pago.objects.filter(cuentas_x_pagar__in=(x.id for x in list_cta_pagar),
                                                       status=1,
                                                       pago__fecha_reg__lte=fecha_corte,
                                                       cuentas_x_pagar__naturaleza=naturaleza).
        filter(~Q(cuentas_x_pagar__num_doc=F("pago__num_comp"))).aggregate(Sum('monto',
                                                                               field="cxp_pago.monto"))["monto__sum"])
    return pagado


def listar_cuentas_pagar_pendientes(buscador, proveedores, lista_saldo_client, lista_id_provee):
    """
    Lista las cuentas por pagar pendientes por fecha en un arreglo
    :param buscador:
    :param proveed_formset:
    :param lista_saldo_client:
    :param lista_id_client:
    :param request:
    :return:
    """
    fecha_final = buscador.getFechaFinal()
    total_debito = 0.0
    total_credito = 0.0
    total_pagado_cruzado = 0.0
    total_pagar = 0.0
    if fecha_final is not None:
        # Si escojió la opción de todos los clientes
        buscador.is_valid()  # Se llama a la función para usar cleaned_data
        if buscador.getAllProv():
            all_provee = Proveedores.objects.filter(status=1).order_by("razon_social")
            for proveedor in all_provee:
                lista_id_provee.append(proveedor.id)
                datos_provee = DatosProveSaldos(0)
                datos_provee.proveedor = proveedor
                datos_provee.lista_saldo = Cuentas_por_Pagar.objects.raw(
                    get_cta_pagar_pendientes_query(fecha_final.strftime("%Y-%m-%d"),
                                                   proveedor.id))

                total_monto_deudor = cero_if_none(Cuentas_por_Pagar.objects.filter(id__in=(x.id for x in datos_provee.lista_saldo)).
                    filter(naturaleza=1).aggregate(Sum('monto', field="monto"))["monto__sum"])

                total_monto_acreedor = cero_if_none(Cuentas_por_Pagar.objects.filter(id__in=(x.id for x in datos_provee.lista_saldo)).
                    filter(naturaleza=2).aggregate(Sum('monto', field="monto"))["monto__sum"])

                total_pagado_deudor = get_pagado_naturaleza(fecha_final, datos_provee.lista_saldo, 1)

                total_pagado_acreedor = get_pagado_naturaleza(fecha_final, datos_provee.lista_saldo, 2)

                total_pagado = get_pagado(fecha_final, datos_provee.lista_saldo)

                datos_provee.saldo_deudor = total_monto_deudor
                datos_provee.saldo_acreedor = total_monto_acreedor
                datos_provee.saldo = total_pagado
                datos_provee.total_saldo_pendiente = ((total_monto_deudor - total_pagado_deudor) -
                                                      (total_monto_acreedor - total_pagado_acreedor))

                # Sumariza el total a pagar a todos los proveedores
                total_debito += total_monto_deudor
                total_credito += total_monto_acreedor
                total_pagado_cruzado += total_pagado

                total_pagar += ((total_monto_deudor - total_pagado_deudor) -
                                (total_monto_acreedor - total_pagado_acreedor))

                if len(list(datos_provee.lista_saldo)) > 0:
                    lista_saldo_client.append(datos_provee)
        else:

            for obj in proveedores:
                #sin esta lines, se cae elñ sistema en la linea de abajo porque dice que el cleaned data no vale aun
                obj.is_valid()
                informacion = obj.cleaned_data
                try:
                    proveedor = Proveedores.objects.get(id=informacion.get("proveedor"))
                    # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                    if proveedor.id not in lista_id_provee:
                        lista_id_provee.append(proveedor.id)
                        datos_provee = DatosProveSaldos(0)
                        datos_provee.proveedor = proveedor

                        datos_provee.lista_saldo = Cuentas_por_Pagar.objects.raw(get_cta_pagar_pendientes_query(fecha_final.strftime("%Y-%m-%d"),
                                                                                                proveedor.id))
                        total_monto_deudor = cero_if_none(Cuentas_por_Pagar.objects.filter(id__in=(x.id for x in datos_provee.lista_saldo)).
                            filter(naturaleza=1).aggregate(Sum('monto', field="monto"))["monto__sum"])

                        total_monto_acreedor = cero_if_none(Cuentas_por_Pagar.objects.filter(id__in=(x.id for x in datos_provee.lista_saldo)).
                            filter(naturaleza=2).aggregate(Sum('monto', field="monto"))["monto__sum"])

                        total_pagado_deudor = get_pagado_naturaleza(fecha_final, datos_provee.lista_saldo, 1)

                        total_pagado_acreedor = get_pagado_naturaleza(fecha_final, datos_provee.lista_saldo, 2)

                        total_pagado = get_pagado(fecha_final, datos_provee.lista_saldo)
                        datos_provee.saldo_deudor = total_monto_deudor
                        datos_provee.saldo_acreedor = total_monto_acreedor
                        datos_provee.saldo = total_pagado
                        datos_provee.total_saldo_pendiente = ((total_monto_deudor - total_pagado_deudor) -
                                                              (total_monto_acreedor - total_pagado_acreedor))

                        # Sumariza el total a pagar a todos los proveedores
                        total_debito += total_monto_deudor
                        total_credito += total_monto_acreedor
                        total_pagado_cruzado += total_pagado
                        total_pagar += ((total_monto_deudor - total_pagado_deudor) -
                                        (total_monto_acreedor - total_pagado_acreedor))

                        if len(list(datos_provee.lista_saldo)) > 0:
                            lista_saldo_client.append(datos_provee)
                except Proveedores.DoesNotExist:
                    pass
    return total_debito, total_credito, total_pagado_cruzado, total_pagar


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuentas_por_pagar_pendientes(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    buscador = BuscadorReportes(initial={"fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    cantidad = None
    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    lista_saldo_provee = []  # Lista donde se guardan las transacciones de los clientes
    lista_id_provee = []  # Lista donde se guardan los ids del registro de cada cliente para evitar repeticiones
    total_debito = 0.0
    total_credito = 0.0
    total_pagado_cruzado = 0.0
    total_pagar = 0.0
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        proveedores = proveed_formset(request.POST, prefix='proveedores')
        buscador.is_valid()
        total_debito, total_credito, total_pagado_cruzado, total_pagar = listar_cuentas_pagar_pendientes(buscador, proveedores, lista_saldo_provee, lista_id_provee)
        cantidad = len(lista_saldo_provee)

    return render_to_response('reporte_proveedores/reporte_cuentas_por_pagar_pendientes.html',
                              {"buscador": buscador,
                               "objetos": lista_saldo_provee,
                               "proveedores": proveedores,
                               "cantidad": cantidad,
                               "total_debito": total_debito,
                               "total_credito": total_credito,
                               "total_pagado_cruzado": total_pagado_cruzado,
                               "total_pagar": total_pagar,
                               "fecha_final": buscador.getFechaFinal()
                               }, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuentas_por_pagar_pendientes_pdf(request):
    """
    Reporte de cuentas por cobrar pendientes pdf
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes(initial={"fecha_ini":
                                             str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    lista_saldo_provee = []  # Lista donde se guardan las transacciones de los clientes
    lista_id_provee = []  # Lista donde se guardan los ids del registro de cada cliente para evitar repeticiones
    total_debito = 0.0
    total_credito = 0.0
    total_pagado_cruzado = 0.0
    total_pagar = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        proveedores = proveed_formset(request.POST, prefix='proveedores')
        buscador.is_valid()
        total_debito, total_credito, total_pagado_cruzado, total_pagar = listar_cuentas_pagar_pendientes(buscador, proveedores, lista_saldo_provee, lista_id_provee)

    html = render_to_string('reporte_proveedores/reporte_cuentas_por_pagar_pendientes_pdf.html',
                            {'pagesize': 'A4',
                             "objetos": lista_saldo_provee,
                             "proveedores": proveedores,
                             "desde": buscador.getFechaIni(),
                             "hasta": buscador.getFechaFinal(),
                             "total_debito": total_debito,
                             "total_credito": total_credito,
                             "total_pagado_cruzado": total_pagado_cruzado,
                             "total_pagar": total_pagar,
                             "empresa":empresa,
                             'usuario': usuario,
                             'request': request
                             }, context_instance=RequestContext(request))
    nombre = ( 'Rpt_Ctas_pag_pend_'+str(buscador.getFechaIni())+'_a_'+str(buscador.getFechaFinal()))
    return generar_pdf_nombre(html, nombre)


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_cuentas_por_pagar_pendientes_excel(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    buscador = BuscadorReportes(initial={"fecha_ini":
                                             str(datetime.datetime.now().year) + "-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    proveed_formset = formset_factory(ReporteSaldosForm)
    proveedores = proveed_formset(prefix='proveedores')
    lista_saldo_provee = []  # Lista donde se guardan las transacciones de los clientes
    lista_id_provee = []  # Lista donde se guardan los ids del registro de cada cliente para evitar repeticiones
    lista = []
    total_debito = 0.0
    total_credito = 0.0
    total_pagado_cruzado = 0.0
    total_pagar = 0.0
    lista.append([u"", u"", u"Cuentas por Pagar Pendientes"+TITULO])
    lista.append([u""])
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        proveedores = proveed_formset(request.POST, prefix='proveedores')
        buscador.is_valid()
        total_debito, total_credito, total_pagado_cruzado, total_pagar = listar_cuentas_pagar_pendientes(buscador, proveedores, lista_saldo_provee, lista_id_provee)

        lista.append([u"Desde: "+SUBTITULO, buscador.getFechaIni()])
        lista.append([u"Hasta: "+SUBTITULO, buscador.getFechaFinal()])
        lista.extend("" for i in range(2))
        fecha_final = buscador.getFechaFinal()
        for obj in lista_saldo_provee:
            lista.append([u"Proveedor: "+SUBTITULO, convert_vacio_none(obj.proveedor.razon_social),
                          convert_vacio_none(obj.proveedor.ruc)])
            lista.append([u"Fecha Registro"+COLUMNA, u"Fecha Emsión"+COLUMNA, u"Tipo Doc."+COLUMNA,
                          u"# Documento"+COLUMNA, u"Débito"+COLUMNA, u"Crédito"+COLUMNA, u"Cobrado/Cruzado"+COLUMNA, u"Saldo Pendiente"+COLUMNA])
            for obj2 in obj.lista_saldo:
                if obj2.naturaleza == 1:
                    lista.append([obj2.fecha_reg, obj2.fecha_emi, obj2.documento.descripcion_documento, obj2.num_doc,
                                  obj2.monto, 0.0, obj2.pagado, (cero_if_none(obj2.pagado)-cero_if_none(obj2.monto))])
                else:
                    lista.append([obj2.fecha_reg, obj2.fecha_emi, obj2.documento.descripcion_documento, obj2.num_doc,
                                  0.0, obj2.monto, get_monto_pagado_fecha_func(obj2.id, fecha_final), get_monto_saldo_cxp_fecha_func(obj2.id, fecha_final)])
            lista.append(["", "", "", "Totales:", obj.saldo_deudor, obj.saldo_acreedor, obj.saldo, obj.total_saldo_pendiente])
            lista.append([u""])
        lista.append(["", "", "", u"Totales:", total_debito, total_credito, total_pagado_cruzado, total_pagar])

    return ExcelResponse(lista, 'Rpt_cta_pag_pend_a_' + str(buscador.getFechaFinal()))


class ReporteSaldoProveedor():
    """
        Clase que ayuda a agrupar los datos para presentarlos en
        saldos_por_proveedores
    """
    def __init__(self):
        self.proveedor = Proveedores()
        self.otdoc = 0.0
        self.ant = 0.0
        self.nc = 0.0
        self.nd = 0.0
        self.total = 0.0


def listar_saldo_por_proveedores(fecha_final, lista):
    """
    Llena la lista para presentar en el reporte saldo_por_proveedor
    :param fecha_final:
    :param lista:
    :return:
    """
    total_saldo_compras = 0
    total_nd = 0
    total_nc = 0
    total_ant = 0
    total = 0
    for obj in Proveedores.objects.filter(status=1).order_by('razon_social'):
        otdoc_debe = convert_cero_none(Cuentas_por_Pagar.objects.exclude(Q(documento__codigo_documento="AN")|
                                                                         Q(documento__codigo_documento="04")|
                                                                         Q(documento__codigo_documento="05")).
            filter(proveedor=obj,
                   fecha_reg__lte=fecha_final,
                   status=1).aggregate(Sum('monto'))["monto__sum"])

        otdoc_haber = convert_cero_none(CXP_pago.objects.exclude(Q(cuentas_x_pagar__documento__codigo_documento="AN")|
                                                                 Q(cuentas_x_pagar__documento__codigo_documento="04")|
                                                                 Q(cuentas_x_pagar__documento__codigo_documento="05"))
            .filter(pago__proveedor=obj,
                    pago__fecha_reg__lte=fecha_final,
                    status=1).aggregate(Sum('monto'))["monto__sum"])

        ant_debe = convert_cero_none(Cuentas_por_Pagar.objects.filter(proveedor=obj,
                                                                      documento__codigo_documento="AN",
                                                                      fecha_reg__lte=fecha_final,
                                                                      status=1).aggregate(Sum('monto'))["monto__sum"])

        ant_haber = convert_cero_none(CXP_pago.objects.filter(pago__proveedor=obj,
                                                              cuentas_x_pagar__documento__codigo_documento="AN",
                                                              pago__fecha_reg__lte=fecha_final,
                                                              status=1).filter(~Q(cuentas_x_pagar__num_doc=F('pago__num_comp'))).aggregate(Sum('monto'))["monto__sum"])

        n_d_debe = convert_cero_none(Cuentas_por_Pagar.objects.filter(proveedor=obj,
                                                                      documento__codigo_documento="05",
                                                                      fecha_reg__lte=fecha_final,
                                                                      status=1).aggregate(Sum('monto'))["monto__sum"])

        n_d_haber = convert_cero_none(CXP_pago.objects.filter(pago__proveedor=obj,
                                                              cuentas_x_pagar__documento__codigo_documento="05",
                                                              pago__fecha_reg__lte=fecha_final,
                                                              status=1).aggregate(Sum('monto'))["monto__sum"])

        n_c_debe = convert_cero_none(Cuentas_por_Pagar.objects.filter(proveedor=obj,
                                                                      documento__codigo_documento="04",
                                                                      fecha_reg__lte=fecha_final,
                                                                      status=1).aggregate(Sum('monto'))["monto__sum"])

        n_c_haber = convert_cero_none(CXP_pago.objects.filter(pago__proveedor=obj,
                                                              cuentas_x_pagar__documento__codigo_documento="04",
                                                              pago__fecha_reg__lte=fecha_final,
                                                              status=1).aggregate(Sum('monto'))["monto__sum"])

        saldo_proveedor = ReporteSaldoProveedor()
        saldo_proveedor.proveedor = obj
        saldo_proveedor.otdoc = redondeo(otdoc_debe - otdoc_haber)
        saldo_proveedor.ant = redondeo(ant_debe - ant_haber)
        saldo_proveedor.nc = redondeo(n_c_debe - n_c_haber)
        saldo_proveedor.nd = redondeo(n_d_debe - n_d_haber)
        saldo_proveedor.total = redondeo(saldo_proveedor.otdoc + saldo_proveedor.nd - saldo_proveedor.ant
                                         - saldo_proveedor.nc)
        # Solo se presentan los proveedores que tengan movimientos
        #if saldo_proveedor.otdoc + saldo_proveedor.ant + saldo_proveedor.nc + saldo_proveedor.nd > 0:

        if saldo_proveedor.otdoc + saldo_proveedor.ant + saldo_proveedor.nc + saldo_proveedor.nd != 0:
            lista.append(saldo_proveedor)
            total_saldo_compras += saldo_proveedor.otdoc
            total_nd += saldo_proveedor.nd
            total_nc += saldo_proveedor.nc
            total_ant += saldo_proveedor.ant
            total += saldo_proveedor.total

    return total_saldo_compras, total_nd, total_nc, total_ant, total

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldos_por_proveedores(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """

    buscador = BuscadorReportes(initial={"fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    lista = []
    total_saldo_compras = 0
    total_nd = 0
    total_nc = 0
    total_ant = 0
    total = 0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_final = buscador.getFechaFinal()
        if fecha_final is not None:
            total_saldo_compras, total_nd, total_nc, total_ant, total = listar_saldo_por_proveedores(fecha_final, lista)

    return render_to_response('reporte_proveedores/reporte_saldos_por_proveedores.html',
                              {"buscador": buscador,
                               'lista': lista,
                               'total_saldo_compras': total_saldo_compras,
                               'total_nd': total_nd,
                               'total_nc': total_nc,
                               'total_ant': total_ant,
                               'total': total}, context_instance=RequestContext(request))


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldos_por_proveedores_pdf(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes(initial={"fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    lista = []
    total_saldo_compras = 0
    total_nd = 0
    total_nc = 0
    total_ant = 0
    total = 0#estas cosas son para pasar los totales finales al sistema en pdf
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_final = buscador.getFechaFinal()

        if fecha_final is not None:
            #listar_saldo_por_proveedores(fecha_final, lista)
            total_saldo_compras, total_nd, total_nc, total_ant, total = listar_saldo_por_proveedores(fecha_final, lista)

    html = render_to_string('reporte_proveedores/reporte_saldos_por_proveedores_pdf.html',
                            {'pagesize': 'A4',
                             'buscador': buscador,
                             'lista': lista,
                             "empresa":empresa,
                             'usuario': usuario,
                             'total_saldo_compras': total_saldo_compras,
                             'total_nd': total_nd,
                             'total_nc': total_nc,
                             'total_ant': total_ant,
                             'total': total,
                             'request': request
                             }, context_instance=RequestContext(request))
    nombre = ('Rpt_Saldos_Proveedor_a_'+str(fecha_final))
    return generar_pdf_nombre(html, nombre)


@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldos_por_proveedores_excel(request):
    """
    Reporte de cuentas por cobrar
    :param request:
    :return:
    """
    lista = []
    lista_excel = []
    total_saldo_compras = 0
    total_nd = 0
    total_nc = 0
    total_ant = 0
    total = 0

    lista_excel.append([u"", u"", u"Reporte Saldo por Proveedor"+TITULO])
    lista_excel.append([u""])
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_final = buscador.getFechaFinal()

        if fecha_final is not None:
            lista_excel.append([u"Hasta: "+SUBTITULO, fecha_final.strftime("%Y-%m-%d")])
            lista_excel.append([u""])

            total_saldo_compras, total_nd, total_nc, total_ant, total = listar_saldo_por_proveedores(fecha_final, lista)

            lista_excel.append([u"Proveedor"+COLUMNA, u"Saldo Compras"+COLUMNA, u"N/D"+COLUMNA, u"N/C"+COLUMNA,
                                u"Anticipos"+COLUMNA, u"Total"+COLUMNA])
        for obj in lista:
            lista_excel.append([obj.proveedor.razon_social+" ("+str(obj.proveedor.ruc)+")", obj.otdoc, obj.nd, obj.nc,
                                obj.ant, obj.total])
        lista_excel.append([u""])
        lista_excel.append([u"Totales", total_saldo_compras, total_nd, total_nc,
                            total_ant, total])
    return ExcelResponse(lista_excel, 'Rpt_Saldos_Proveedor_a_'+str(fecha_final))
