#! /usr/bin/python
# -*- coding: UTF-8-*-
import requests

__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.http import *
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from contabilidad.models import *
import json
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import connection
from django.core.paginator import *
from django import template, forms
from django import forms
from django.utils.html import *
from  contabilidad.models import *
import xlwt
from django.db.models import Sum
import operator
from reportes.formularios.reportes_form import *
from django.forms.formsets import formset_factory
from reportes.models import *
from contabilidad.models import *
from librerias.funciones.funciones_vistas import *
from librerias.funciones.excel_response_mod import *
from librerias.funciones.permisos import *


class SaldoBancosTemp():
    def __init__(self, saldo_ini, debe, haber, saldo):
        self.banco = Cuenta_Banco()
        self.saldo_ini = saldo_ini
        self.debe = debe
        self.haber = haber
        self.saldo = saldo


class ListaBancos():
    """
    Clase que me sirve para guardar la información básica del banco
    con una lista en la cual se contendrán datos dependiendo del
    reporte
    """
    def __init__(self, lista, saldo_ini=0, total_debe=0, total_haber=0, total=0):
        self.banco = Cuenta_Banco()
        self.lista = lista
        self.saldo_ini = saldo_ini
        self.total_debe = total_debe
        self.total_haber = total_haber
        self.total = total


class Cuenta_banco_saldo():
    """
    Clase que me sirve para guardar la información básica del banco
    con una lista y calcular su saldo
    """
    def __init__(self, saldo):
        self.banco = Banco()
        self.saldo = saldo


############################################# REPORTE DE SALDO BANCOS ##################################################
@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldo_bancos_por_fecha(request):
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    lista_saldo = []
    lista_id = []
    total_debitos = 0.0
    total_creditos = 0.0
    total_saldo = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        if None not in (fecha_ini, fecha_final):
            bancos = banco_formset(request.POST, prefix='bancos')
            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data

            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)
                for banco in all_banc:
                    lista_id.append(banco.id)
                    saldos = banco.get_saldos_por_fecha(fecha_ini, fecha_final)
                    total_debitos += saldos[1]
                    total_creditos += saldos[2]
                    total_saldo += saldos[3]
                    saldo_banco = SaldoBancosTemp(saldos[0], saldos[1], saldos[2], saldos[3])
                    saldo_banco.banco = banco
                    lista_saldo.append(saldo_banco)

            else:

                if bancos.is_valid():
                    for obj in bancos:
                        informacion = obj.cleaned_data
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                            if banco.id not in lista_id:
                                lista_id.append(banco.id)
                                saldos = banco.get_saldos_por_fecha(fecha_ini, fecha_final)
                                total_debitos += saldos[1]
                                total_creditos += saldos[2]
                                total_saldo += saldos[3]
                                saldo_banco = SaldoBancosTemp(saldos[0], saldos[1], saldos[2], saldos[3])
                                saldo_banco.banco = banco
                                lista_saldo.append(saldo_banco)
                        except:
                            pass
    return render_to_response('reporte_bancos/reporte_saldo_bancos.html',
                              {"buscador": buscador,
                               "lista_saldo": lista_saldo,
                               "bancos": bancos,
                                "total_haber": total_creditos,
                                "total_debe": total_debitos,
                                "total_saldo": total_saldo}, context_instance=RequestContext(request))

#@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldo_bancos_por_fecha_jasper(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    proveed_formset = formset_factory(ReporteSaldosForm)
    clientes = proveed_formset(prefix='clientes')
    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    r = None
    cantidad = None
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        clientes = proveed_formset(request.POST, prefix='clientes')
        bancos = banco_formset(prefix='bancos')
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_fin = buscador.getFechaFinal()
        fecha_ini = buscador.getFechaIni()
        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if fecha_fin is not None:
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('BCO_BCO_X_FECHAS') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if bancos.is_valid():
                for obj in bancos:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                        id_proveedores += str(proveedor.id) + ","
                    except Cuenta_Banco.DoesNotExist:
                        pass
            else:
                print('no hay clientessssss :o')
            print(str(id_proveedores))
            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'fecha_final': fecha_fin.strftime("%Y-%m-%d"),
                      'banco': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_fin is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    return render_to_response('reporte_clientes/reporte_cuentas_por_cobrar_pendiente.html',
                              {"buscador": buscador,
                               "lista": r,#"lista": r,
                               "clientes": clientes
                               }, context_instance=RequestContext(request))




@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldo_bancos_por_fecha_pdf(request):
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    lista_saldo = []
    lista_id = []
    total_debitos = 0.0
    total_creditos = 0.0
    total_saldo = 0.0

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        if None not in (fecha_ini, fecha_final):
            bancos = banco_formset(request.POST, prefix='bancos')
            total_debitos = 0.0
            total_creditos = 0.0
            total_saldo = 0.0
            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data

            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)

                for banco in all_banc:
                    lista_id.append(banco.id)
                    saldos = banco.get_saldos_por_fecha(fecha_ini, fecha_final)
                    total_debitos += saldos[1]
                    total_creditos += saldos[2]
                    total_saldo += saldos[3]
                    saldo_banco = SaldoBancosTemp(saldos[0], saldos[1], saldos[2], saldos[3])
                    saldo_banco.banco = banco
                    lista_saldo.append(saldo_banco)

            else:

                if bancos.is_valid():
                    for obj in bancos:
                        informacion = obj.cleaned_data
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                            if banco.id not in lista_id:
                                lista_id.append(banco.id)
                                saldos = banco.get_saldos_por_fecha(fecha_ini, fecha_final)
                                total_debitos += saldos[1]
                                total_creditos += saldos[2]
                                total_saldo += saldos[3]
                                saldo_banco = SaldoBancosTemp(saldos[0], saldos[1], saldos[2], saldos[3])
                                saldo_banco.banco = banco
                                lista_saldo.append(saldo_banco)

                        except:
                            pass
    html = render_to_string('reporte_bancos/reporte_saldo_bancos_pdf.html',
                            {'pagesize': 'A4',
                             "lista_saldo": lista_saldo,
                             "bancos": bancos,
                             "total_haber": total_creditos,
                             "total_debe": total_debitos,
                             "total_saldo": total_saldo,
                             'empresa':empresa,
                             'fecha_ini': fecha_ini,
                             'fecha_final': fecha_final,
                             'usuario': usuario,
                             "request": request,
                             }, context_instance=RequestContext(request))
    nombre = 'Rept_saldo_bcos_'+str(fecha_ini)+"_a_"+str(fecha_final)
    return generar_pdf_nombre(html, nombre)

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_saldo_bancos_por_fecha_excel(request):
    banco_formset = formset_factory(ReporteSaldosForm)
    lista = []
    lista_id = []
    lista.append([u"", u"", u"", u"REPORTE SALDO DE BANCOS"+TITULO])
    lista.append([u""])

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        if None not in (fecha_ini, fecha_final):
            bancos = banco_formset(request.POST, prefix='bancos')
            total_debitos = 0.0
            total_creditos = 0.0
            total_saldo = 0.0
            lista.append([u"Fecha inicio:"+SUBTITULO, fecha_ini])
            lista.append([u"Hasta:"+SUBTITULO, fecha_final])
            lista.append([u""])
            lista.append([u"Cta. Banco"+COLUMNA, u"Saldo Inicial"+COLUMNA, u"Débitos"+COLUMNA, u"Créditos"+COLUMNA, u"Saldo Final"+COLUMNA])
            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data
            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)
                for banco in all_banc:
                    saldos = banco.get_saldos_por_fecha(fecha_ini, fecha_final)
                    total_debitos += saldos[1]
                    total_creditos += saldos[2]
                    total_saldo += saldos[3]
                    lista.append([unicode(banco.descripcion) + " - " + str(banco.numero_cuenta),
                                          saldos[0], saldos[1],
                                          saldos[2], saldos[3]])

                lista.append([u"Totales"+SUBTITULO, u"", str(total_debitos)+G_TOTAL, str(total_creditos)+G_TOTAL, str(total_saldo)+G_TOTAL])
                lista.append([u""])
            else:

                if bancos.is_valid():
                    for obj in bancos:
                        informacion = obj.cleaned_data
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                            if banco.id not in lista_id:
                                lista_id.append(banco.id)
                                saldos = banco.get_saldos_por_fecha(fecha_ini, fecha_final)
                                total_debitos += saldos[1]
                                total_creditos += saldos[2]
                                total_saldo += saldos[3]
                                lista.append([unicode(banco.descripcion) + " - " + str(banco.numero_cuenta),
                                                      saldos[0], saldos[1], saldos[2], saldos[3]])
                        except:
                            pass
                    lista.append([u"Totales"+SUBTITULO, u"", str(total_debitos)+G_TOTAL, str(total_creditos)+G_TOTAL, str(total_saldo)+G_TOTAL])
                    lista.append([u""])
    nombre = 'Rept_saldo_bcos_'+str(fecha_ini)+"_a_"+str(fecha_final)
    return ExcelResponse(lista, nombre)

########################################################################################################################

############################################# REPORTE DE MOVIMIENTOS BANCARIOS #########################################
@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def reporte_movimientos_bancarios_20dic(request):
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    lista = []
    lista_id = []

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            bancos = banco_formset(request.POST, prefix='bancos')
            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data
            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)
                for banco in all_banc:
                    lista_id.append(banco.id)
                    lista_trans = []
                    saldo_ini = 0.00
                    try:
                        saldo_ini = Cuenta_Banco.objects.get(plan_cuenta=banco.plan_cuenta).get_saldos_por_fecha(fecha_ini, fecha_final)[0]  # Saldo inicial
                    except:
                       saldo_ini = 0.00
                    saldo = saldo_ini
                    total_debe = 0.0
                    total_haber = 0.0

                    for obj in banco.get_movimientos_por_fecha(fecha_ini, fecha_final):
                        if obj.cuenta_banco.plan_cuenta.naturaleza == "D":
                            if obj.status == 1:
                                if obj.naturaleza == 1:
                                    saldo += obj.valor
                                    total_debe += obj.valor
                                else:
                                    saldo -= obj.valor
                                    total_haber += obj.valor
                            else:
                                total_debe += 0.0
                                total_haber += 0.0

                        else:

                            if obj.status == 1:
                                if obj.naturaleza == 1:
                                    saldo -= obj.valor
                                    total_haber += obj.valor
                                else:
                                    saldo += obj.valor
                                    total_debe += obj.valor
                            else:
                                total_debe += 0.0
                                total_haber += 0.0

                        trans_banco = Cuenta_banco_saldo(saldo)
                        trans_banco.banco = obj
                        lista_trans.append(trans_banco)

                    lista_banco = ListaBancos(lista_trans)
                    lista_banco.banco = banco
                    lista_banco.saldo_ini = saldo_ini
                    lista_banco.total_debe = total_debe
                    lista_banco.total_haber = total_haber
                    if len(lista_trans) > 0:
                        lista.append(lista_banco)
            else:
                if bancos.is_valid():
                    for obj in bancos:
                        informacion = obj.cleaned_data
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            total_debe = 0.0
                            total_haber = 0.0

                            # Para ver si esta dentro de la lista de bancos (banco repetido)
                            if banco.id not in lista_id:
                                lista_trans = []
                                lista_id.append(banco.id)
                                saldo_ini = Cuenta_Banco.objects.get(plan_cuenta=banco.plan_cuenta).get_saldos_por_fecha(fecha_ini, fecha_final)[0]  # Saldo inicial
                                saldo = saldo_ini

                                for obj in banco.get_movimientos_por_fecha(fecha_ini, fecha_final):
                                    if obj.cuenta_banco.plan_cuenta.naturaleza == "D":
                                        if obj.status == 1:
                                            if obj.naturaleza == 1:
                                                saldo += obj.valor
                                                total_debe += obj.valor
                                            else:
                                                saldo -= obj.valor
                                                total_haber += obj.valor
                                        else:
                                            total_debe += 0.0
                                            total_haber += 0.0

                                    else:

                                        if obj.status == 1:
                                            if obj.naturaleza == 1:
                                                saldo -= obj.valor
                                                total_haber += obj.valor
                                            else:
                                                saldo += obj.valor
                                                total_debe += obj.valor
                                        else:
                                            total_debe += 0.0
                                            total_haber += 0.0

                                    trans_banco = Cuenta_banco_saldo(saldo)
                                    trans_banco.banco = obj
                                    lista_trans.append(trans_banco)

                                lista_banco = ListaBancos(lista_trans)
                                lista_banco.banco = banco
                                lista_banco.saldo_ini = saldo_ini
                                lista_banco.total_debe = total_debe
                                lista_banco.total_haber = total_haber
                                if len(lista_trans) > 0:
                                    lista.append(lista_banco)
                        except:
                            pass

    return render_to_response('reporte_bancos/reporte_movimientos_bancarios.html',
                              {"buscador": buscador,
                               "lista": lista,
                               "bancos": bancos,
                               }, context_instance=RequestContext(request))


#@permiso_accion(mensaje=mensaje_permiso)
@csrf_exempt
def reporte_movimientos_bancarios(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    r = None
    cantidad = None
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        banco_formset = formset_factory(ReporteSaldosForm)
        bancos = banco_formset(request.POST, prefix='bancos')
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if fecha_final is not None:
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('BCO_BCO_X_FECHAS') + '.html'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)
            bancos.is_valid()
            if bancos.is_valid():
                print 'es valido el bancosssss'
                for obj in bancos:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                        id_proveedores += str(proveedor.id) + ","
                    except Cuenta_Banco.DoesNotExist:
                        pass
            else:
                print('no hay cuentabancooo :o pero debe ejecutar el todos')
            print(str(id_proveedores))
            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_final': fecha_final.strftime("%Y-%m-%d"),
                      'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'banco': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_final is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    return render_to_response('reporte_bancos/reporte_movimientos_bancarios.html',
                              {"buscador": buscador,
                               "lista": r,
                               "bancos": bancos,
                               }, context_instance=RequestContext(request))


@csrf_exempt
def reporte_movimientos_bancarios_pdf(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    r = None
    cantidad = None
    if request.method == "POST":
        print 'estoy en ekl post cel pdf'
        buscador = BuscadorReportes(request.POST)
        bancos = banco_formset(request.POST,prefix='bancos')
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if fecha_final is not None:
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('BCO_BCO_X_FECHAS') + '.pdf'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if bancos.is_valid():
                for obj in bancos:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                        id_proveedores += str(proveedor.id) + ","
                    except Cuenta_Banco.DoesNotExist:
                        pass
            else:
                print('no hay cuentabancooo :o')
            print(str(id_proveedores))
            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_final': fecha_final.strftime("%Y-%m-%d"),
                      'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'banco': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_final is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    response = HttpResponse(r.content, mimetype='application/pdf')
    nombre = "Rpt_mov_bancos_"+str(fecha_final)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".pdf"'
    return response


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_movimientos_bancarios_pdf_22diciembre(request):
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    lista = []
    lista_id = []

    id_empresa = Empresa.objects.all()[0].empresa_general_id
    empresa = Empresa_Parametro.objects.using("base_central").get(id=id_empresa)

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        if None not in (fecha_ini, fecha_final):
            bancos = banco_formset(request.POST, prefix='bancos')
            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data
            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)
                for banco in all_banc:
                    lista_id.append(banco.id)
                    lista_trans = []
                    saldo_ini = Cuenta_Banco.objects.get(plan_cuenta=banco.plan_cuenta).get_saldos_por_fecha(fecha_ini, fecha_final)[0]  # Saldo inicial
                    saldo = saldo_ini
                    total_debe = 0.0
                    total_haber = 0.0

                    for obj in banco.get_movimientos_por_fecha(fecha_ini, fecha_final):
                        if obj.cuenta_banco.plan_cuenta.naturaleza == "D":
                            if obj.status == 1:
                                if obj.naturaleza == 1:
                                    saldo += obj.valor
                                    total_debe += obj.valor
                                else:
                                    saldo -= obj.valor
                                    total_haber += obj.valor
                            else:
                                total_debe += 0.0
                                total_haber += 0.0
                        else:

                            if obj.status == 1:
                                if obj.naturaleza == 1:
                                    saldo -= obj.valor
                                    total_haber += obj.valor
                                else:
                                    saldo += obj.valor
                                    total_debe += obj.valor
                            else:
                                total_debe += 0.0
                                total_haber += 0.0

                        trans_banco = Cuenta_banco_saldo(saldo)
                        trans_banco.banco = obj
                        lista_trans.append(trans_banco)

                    lista_banco = ListaBancos(lista_trans)
                    lista_banco.banco = banco
                    lista_banco.saldo_ini = saldo_ini
                    lista_banco.total_debe = total_debe
                    lista_banco.total_haber = total_haber
                    lista.append(lista_banco)

            else:

                if bancos.is_valid():
                    for obj in bancos:
                        informacion = obj.cleaned_data
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            total_debe = 0.0
                            total_haber = 0.0
                            # Para ver si esta dentro de la lista de bancos (banco repetido)
                            if banco.id not in lista_id:
                                lista_trans = []
                                lista_id.append(banco.id)
                                saldo_ini = Cuenta_Banco.objects.get(plan_cuenta=banco.plan_cuenta).get_saldos_por_fecha(fecha_ini, fecha_final)[0]  # Saldo inicial
                                saldo = saldo_ini

                                for obj in banco.get_movimientos_por_fecha(fecha_ini, fecha_final):
                                    if obj.cuenta_banco.plan_cuenta.naturaleza == "D":
                                        if obj.status == 1:
                                            if obj.naturaleza == 1:
                                                saldo += obj.valor
                                                total_debe += obj.valor
                                            else:
                                                saldo -= obj.valor
                                                total_haber += obj.valor
                                        else:
                                            total_debe += 0.0
                                            total_haber += 0.0

                                    else:

                                        if obj.status == 1:
                                            if obj.naturaleza == 1:
                                                saldo -= obj.valor
                                                total_haber += obj.valor
                                            else:
                                                saldo += obj.valor
                                                total_debe += obj.valor
                                        else:
                                            total_debe += 0.0
                                            total_haber += 0.0

                                    trans_banco = Cuenta_banco_saldo(saldo)
                                    trans_banco.banco = obj
                                    lista_trans.append(trans_banco)

                                lista_banco = ListaBancos(lista_trans)
                                lista_banco.banco = banco
                                lista_banco.saldo_ini = saldo_ini
                                lista_banco.total_debe = total_debe
                                lista_banco.total_haber = total_haber
                                lista.append(lista_banco)
                        except:
                            pass
    html = render_to_string('reporte_bancos/reporte_movimientos_bancarios_pdf.html',
                            {'pagesize': 'A4',
                             "buscador": buscador,
                             "lista": lista,
                             "bancos": bancos,
                             "empresa": empresa}, context_instance=RequestContext(request))
    return generar_pdf_nombre(html,"Rpt_mov_banc_"+str(fecha_ini)+"_"+str(fecha_final))



@csrf_exempt
def reporte_movimientos_bancarios_excel(request):
    now = datetime.datetime.now()
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    r = None
    cantidad = None
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        bancos = banco_formset(request.POST,prefix='bancos')
        buscador.is_valid()  # Para que funcione el cleaned data
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
        #id_cuentas = ""
        id_proveedores = ""
        urljasper = get_url_jasper()
        print(urljasper)
        if fecha_final is not None:
            url = urljasper + empresa.path_jasper + '/'

            # Report to process:estado_cta_proveedor
            report = get_nombre_reporte('BCO_BCO_X_FECHAS') + '.xls'

            # Authorisation credentials:
            auth = (empresa.user_jasper, empresa.pass_jasper)

            if bancos.is_valid():
                for obj in bancos:
                    informacion = obj.cleaned_data
                    try:
                        proveedor = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                        id_proveedores += str(proveedor.id) + ","
                    except Cuenta_Banco.DoesNotExist:
                        pass
            else:
                print('no hay cuentabancooo :o')
            print(str(id_proveedores))
            if buscador.getSelAll():
                todos = 1
            else:
                todos = 0
            # Params
            params = {'fecha_final': fecha_final.strftime("%Y-%m-%d"),
                      'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                      'banco': id_proveedores[0:-1],
                      'todos': todos
                      }
            # Init session so we have no need to auth again and again:
            s = requests.Session()
            r = s.get(url=url+report, auth=auth, params=params)
            print "STATUS: ", r.status_code
            print r.content[0:1500]
            if r.status_code != 200:
                r = None

        else:
            if fecha_final is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerido")
    #print (r.content)
    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
    nombre = "Rpt_mov_bancos_"+str(fecha_final)
    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
    return response


@csrf_exempt
@login_required(login_url='/')
@permiso_accion(mensaje=mensaje_permiso)
def reporte_movimientos_bancarios_excel_22dic(request):
    banco_formset = formset_factory(ReporteSaldosForm)
    lista = []
    lista.append([u"", u"", u"", u"REPORTE DE MOVIMIENTOS BANCARIOS"+TITULO])
    lista.append([u""])
    lista_id = []

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        if None not in (fecha_ini, fecha_final):
            lista.append([u"Fecha Inicio:"+SUBTITULO, fecha_ini.strftime("%Y-%m-%d")])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_final.strftime("%Y-%m-%d")])
            lista.append([u""])
            bancos = banco_formset(request.POST, prefix='bancos')
            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data
            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)
                for banco in all_banc:
                    lista_id.append(banco.id)
                    total_debe = 0.0
                    total_haber = 0.0
                    saldo = Cuenta_Banco.objects.get(plan_cuenta=banco.plan_cuenta).get_saldos_por_fecha(fecha_ini,
                                                                                                         fecha_final)[0]  # Saldo inicial
                    lista.append([u""])
                    lista.append([u"Banco:"+SUBTITULO, unicode(banco.descripcion + " - " + banco.numero_cuenta)])
                    lista.append([u"", u"", u"", u"", u"", u"", u"Saldo Inicial:"+SUBTITULO, saldo])
                    lista.append([u"#Comprobante"+COLUMNA, u"Fecha Registro"+COLUMNA, u"Referencia"+COLUMNA, u"Recibido/Pagado"+COLUMNA,
                                  u"Concepto"+COLUMNA, u"Débitos"+COLUMNA, u"Créditos"+COLUMNA, u"Saldo"+COLUMNA])

                    for obj in banco.get_movimientos_por_fecha(fecha_ini, fecha_final):
                        if obj.status == 1:
                            if obj.num_cheque:
                                referencia = u"Cheque # " +str(obj.num_cheque)
                            else:
                                referencia = convert_vacio_none(obj.num_referencia)
                            if obj.naturaleza == 1:
                                saldo += obj.valor
                                total_debe += obj.valor
                                lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"), referencia,
                                              unicode(obj.persona), obj.concepto, obj.valor, "",
                                              saldo])
                            else:
                                saldo -= obj.valor
                                total_haber += obj.valor
                                lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"), referencia,
                                              unicode(obj.persona), obj.concepto, "", obj.valor,
                                              saldo])
                        else:

                            if obj.num_cheque:
                                referencia = u"Cheque # " +str(obj.num_cheque)
                            else:
                                referencia = convert_vacio_none(obj.num_referencia)

                            if obj.naturaleza == 1:
                                saldo += 0.00

                                lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"), referencia,
                                              unicode(obj.persona), obj.concepto, 0.00, "",
                                              saldo])
                            else:
                                saldo -= 0.00
                                lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"), referencia,
                                              unicode(obj.persona), obj.concepto, "", 0.00,
                                              ''])

                    lista.append([u"Totales"+SUBTITULO, u"", u"", u"", u"", str(total_debe)+G_TOTAL,
                              str(total_haber)+G_TOTAL])
                    lista.append([u""])


            else:

                if bancos.is_valid():
                    for obj in bancos:
                        total_debe = 0.0
                        total_haber = 0.0
                        informacion = obj.cleaned_data
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            # Para ver si esta dentro de la lista de bancos (banco repetido)
                            if banco.id not in lista_id:
                                lista_id.append(banco.id)
                                saldo = Cuenta_Banco.objects.get(plan_cuenta=banco.plan_cuenta).get_saldos_por_fecha(fecha_ini,
                                                                                                                 fecha_final)[0]  # Saldo inicial

                                lista.append([u""])
                                lista.append([u"Banco:"+SUBTITULO, unicode(banco.descripcion + " - " + banco.numero_cuenta)])
                                lista.append([u"", u"", u"", u"", u"", u"", u"Saldo Inicial:"+SUBTITULO, saldo])
                                lista.append([u"#Comprobante"+COLUMNA, u"Fecha Registro"+COLUMNA, u"Referencia"+COLUMNA, u"Recibido/Pagado"+COLUMNA,
                                              u"Concepto"+COLUMNA, u"Débitos"+COLUMNA, u"Créditos"+COLUMNA, u"Saldo"+COLUMNA])

                                for obj in banco.get_movimientos_por_fecha(fecha_ini, fecha_final):
                                    if obj.status == 1:
                                        if obj.forma_pago_id == 1 and obj.tipo_comprobante_id == 4:
                                            referencia = u"Cheque #"+str(obj.num_cheque)
                                        else:
                                            referencia = obj.num_referencia
                                        if obj.naturaleza == 1:
                                            saldo += obj.valor
                                            total_debe += obj.valor
                                            lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"), referencia,
                                                          unicode(obj.persona), obj.concepto, obj.valor, "",
                                                          saldo])
                                        else:
                                            saldo -= obj.valor
                                            total_haber += obj.valor
                                            lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"), referencia,
                                                          unicode(obj.persona), obj.concepto, "", obj.valor,
                                                          saldo])
                                    else:

                                        if obj.num_cheque not in (None, ""):
                                            referencia = u"Cheque #"+str(obj.num_cheque)
                                        else:
                                            referencia = obj.num_referencia

                                        if obj.naturaleza == 1:
                                            saldo += 0.00

                                            lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"), referencia,
                                                          unicode(obj.persona), obj.concepto, 0.00, "",
                                                          saldo])
                                        else:
                                            saldo -= 0.00
                                            lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"), referencia,
                                                          unicode(obj.persona), obj.concepto, "", 0.00,
                                                          ''])

                                lista.append([u"Totales"+SUBTITULO, u"", u"", u"", u"", str(total_debe)+G_TOTAL, str(total_haber)+G_TOTAL])
                                lista.append([u""])
                        except:
                            pass
    return ExcelResponse(lista, "Rpt_mov_banc_"+str(fecha_ini)+"_"+str(fecha_final))

########################################################################################################################
############################################# REPORTE LISTADO DE CHEQUES GIRADOS #######################################
########################################################################################################################


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_listado_cheques_girados(request):
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    lista = []
    lista_id = []

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final) and fecha_ini < fecha_final:
            bancos = banco_formset(request.POST, prefix='bancos')

            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data
            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)
                for banco in all_banc:
                    lista_id.append(banco.id)
                    lista_cheques = ListaBancos(banco.get_list_cheques_por_fecha(fecha_ini, fecha_final))
                    lista_cheques.banco = banco
                    if len(lista_cheques.lista) > 0:
                        lista.append(lista_cheques)

            else:

                if bancos.is_valid():
                    for obj in bancos:
                        informacion = obj.cleaned_data
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                            if banco.id not in lista_id:
                                lista_id.append(banco.id)
                                lista_cheques = ListaBancos(banco.get_list_cheques_por_fecha(fecha_ini, fecha_final))
                                lista_cheques.banco = banco
                                if len(lista_cheques.lista) > 0:
                                    lista.append(lista_cheques)
                        except:
                            pass
    return render_to_response('reporte_bancos/reporte_lista_cheques_girados.html',
                              {"buscador": buscador,
                               "lista": lista,
                               "bancos": bancos,
                               }, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_listado_cheques_girados_excel(request):
    banco_formset = formset_factory(ReporteSaldosForm)
    lista_id = []
    lista = []
    lista.append([u"", u"", u"", u"REPORTE CHEQUES GIRADOS"+TITULO])
    lista.append([u""])

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini.strftime("%Y-%m-%d")])
        lista.append([u"Fecha Final:"+SUBTITULO, fecha_final.strftime("%Y-%m-%d")])#agregados estos campos en el excel
        if None not in (fecha_ini, fecha_final):
            bancos = banco_formset(request.POST, prefix='bancos')
            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data
            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)
                for banco in all_banc:
                    lista_id.append(banco.id)
                    total = 0.0

                    if len(banco.get_list_cheques_por_fecha(fecha_ini, fecha_final)) > 0:  # Valida que la lista no este vacia

                        lista.append([u""])
                        lista.append([u"Banco:"+SUBTITULO, unicode(banco.descripcion) + " - " + str(banco.numero_cuenta)])
                        lista.append([u"# Comprobante"+COLUMNA, u"Fecha"+COLUMNA, u"#Cheque"+COLUMNA,
                                      u"Beneficiario"+COLUMNA, u"Concepto"+COLUMNA, u"Valor"+COLUMNA])

                        for obj in banco.get_list_cheques_por_fecha(fecha_ini, fecha_final):
                            if obj.num_cheque:
                                total += obj.valor
                                lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"),
                                              obj.num_cheque, convert_vacio_none(obj.persona),
                                              unicode(obj.concepto), obj.valor])

                        lista.append([u"Total:"+SUBTITULO, u"", u"", u"", u"", str(total)+G_TOTAL])
                    lista.append([u""])

            else:

                if bancos.is_valid():
                    for obj in bancos:
                        total = 0.0
                        informacion = obj.cleaned_data
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            # Para ver si esta dentro de la lista de proveedores (proveedor repetido)
                            if banco.id not in lista_id:
                                lista_id.append(banco.id)
                                if len(banco.get_list_cheques_por_fecha(fecha_ini, fecha_final)) > 0:  # Valida que la lista no este vacia
                                    lista.append([u"Banco:"+SUBTITULO, unicode(banco.descripcion) + " - " + str(banco.numero_cuenta)])
                                    lista.append([u"# Comprobante"+COLUMNA, u"Fecha"+COLUMNA, u"#Cheque"+COLUMNA, u"Beneficiario"+COLUMNA, u"Concepto"+COLUMNA, u"Valor"+COLUMNA])
                                    for obj in banco.get_list_cheques_por_fecha(fecha_ini, fecha_final):
                                        total += obj.valor
                                        lista.append([obj.num_comp, obj.fecha_reg.strftime("%Y-%m-%d"),
                                                      obj.num_cheque, unicode(obj.persona),
                                                      unicode(obj.concepto), obj.valor])

                                    lista.append([u"Total:"+SUBTITULO, u"", u"", u"", u"", str(total)+G_TOTAL])
                        except:
                            pass

                        lista.append([u""])
    nombre = 'Rept_cheques_girados_'+str(fecha_ini)+"_a_"+str(fecha_final)
    return ExcelResponse(lista, nombre)

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_listado_cheques_girados_pdf(request):
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    banco_formset = formset_factory(ReporteSaldosForm)
    bancos = banco_formset(prefix='bancos')
    lista = []
    lista_id = []

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            bancos = banco_formset(request.POST, prefix='bancos')
            # Si escojió la opción de todos los bancos
            buscador.is_valid()  # Se llama a la función para usar cleaned_data
            if buscador.getSelAll():
                all_banc = Cuenta_Banco.objects.filter(status=1)
                for banco in all_banc:
                    lista_id.append(banco.id)
                    total = 0.0
                    for obj in banco.get_list_cheques_por_fecha(fecha_ini, fecha_final):
                        total += obj.valor
                    lista_cheques = ListaBancos(banco.get_list_cheques_por_fecha(fecha_ini, fecha_final))
                    lista_cheques.banco = banco
                    lista_cheques.total = total
                    lista.append(lista_cheques)

            else:

                if bancos.is_valid():
                    for obj in bancos:
                        informacion = obj.cleaned_data
                        total = 0.0
                        try:
                            banco = Cuenta_Banco.objects.get(id=informacion.get("banco"))
                            if banco.id not in lista_id:
                                lista_id.append(banco.id)
                                for obj in banco.get_list_cheques_por_fecha(fecha_ini, fecha_final):
                                    total += obj.valor
                                lista_cheques = ListaBancos(banco.get_list_cheques_por_fecha(fecha_ini, fecha_final))
                                lista_cheques.banco = banco
                                lista_cheques.total = total
                                lista.append(lista_cheques)
                        except:
                            pass
    html = render_to_string('reporte_bancos/reporte_lista_cheques_girados_pdf.html',
                            {'pagesize': 'A4',
                             "buscador": buscador,
                             "lista": lista,
                             "bancos": bancos,
                             "fecha_ini": fecha_ini,
                             "fecha_final": fecha_final,
                             "empresa":empresa,
                             "usuario":usuario}, context_instance=RequestContext(request))
    nombre = 'Rept_cheques_girados_'+str(fecha_ini)+"_a_"+str(fecha_final)
    return generar_pdf_nombre(html, nombre)

########################################################################################################################


################################ REPORTE LISTADO TRANSACCIONES POR CONCILIAR ###########################################
class ListaBancosConciliacion():
    """
    Clase que me sirve para guardar la información básica del banco
    con una lista en la cual se contendrán datos dependiendo del
    reporte
    """
    def __init__(self, lista=None, saldo=0.0, total=0.0):
        self.tipo_comp = TipoComprobante()
        self.lista = lista
        self.saldo = saldo
        self.total = total

@login_required(login_url='/')
@csrf_exempt
@transaction.commit_on_success
@permiso_accion(mensaje=mensaje_permiso)
def reporte_transacciones_bancarias_por_conciliar(request):
    '''
    Vista que me genera el reporte de las transacciones bancarias por
    conciliar
    :param request:
    :return:
    '''
    buscador = ReporteSaldosForm(initial={"fecha_corte": str(datetime.datetime.now().date())})
    lista = []
    saldo_ini = 0.0
    saldo_final = 0.0
    saldo_cuenta = 0.0

    if request.method == "POST":
        buscador = ReporteSaldosForm(request.POST)
        if buscador.is_valid():
            if buscador.getBanco() is not None and buscador.getFechaCorte() is not None:
                cuenta_banco = Cuenta_Banco.objects.get(status=1, id=buscador.getBanco().id)
                anio = buscador.getFechaCorte().year
                #  Actualiza la tabla de saldo de cuentas
                cursor = connection.cursor()
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                cursor.close()
                try:
                    saldo_cuenta_model = SaldoCuenta.objects.get(plan_cuenta=cuenta_banco.plan_cuenta, anio=anio)
                    saldo_cuenta_inicial = convert_cero_none(saldo_cuenta_model.saldo_inicial)
                    saldo_cuenta_debe_acum = convert_cero_none(saldo_cuenta_model.get_saldo_debe_acum(buscador.getFechaCorte().month))
                    saldo_cuenta_haber_acum = convert_cero_none(saldo_cuenta_model.get_saldo_haber_acum(buscador.getFechaCorte().month))
                    saldo_cuenta = saldo_cuenta_inicial + saldo_cuenta_debe_acum - saldo_cuenta_haber_acum
                except SaldoCuenta.DoesNotExist:
                    messages.error(request, u"No se encuentra registrado la cuenta en saldo cuenta")
                    saldo_cuenta = 0.0


                if buscador.getSaldoBanco() != "":
                    saldo_ini = float(buscador.getSaldoBanco())
                else:
                    saldo_ini = 0.0

                saldo = saldo_ini

                for tc in TipoComprobante.objects.filter(status=1):
                    lista_bancos = Banco.objects.exclude(conciliado=True).filter(cuenta_banco=cuenta_banco, status=1,
                                                                                 fecha_reg__lte=buscador.getFechaCorte(),
                                                                                 tipo_comprobante=tc).order_by("fecha_reg")
                    lista_conciliacion = ListaBancosConciliacion()
                    lista_conciliacion.tipo_comp = tc
                    lista_conciliacion.lista = lista_bancos

                    for b in lista_bancos:
                        if b.naturaleza == 1:
                            saldo += b.valor
                            total = saldo
                        else:
                            saldo -= b.valor
                            total = saldo

                        lista_conciliacion.saldo = saldo
                        lista_conciliacion.total = total
                        saldo_final = saldo

                    lista.append(lista_conciliacion)
            else:
                transaction.rollback()
                messages.error(request, u"Existió una error al realizar la consulta")

    return render_to_response('reporte_bancos/reporte_conciliacion_bancaria.html',
                              {"buscador": buscador, "saldo_ini": saldo_ini, "saldo_final": saldo_final,
                               "saldo_cuenta": saldo_cuenta, "lista": lista}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_transacciones_bancarias_por_conciliar_pdf(request):
    '''
    Vista que genera el reporte en pdf
    :param request:
    :return:
    '''
    buscador = ReporteSaldosForm(initial={"fecha_corte": str(datetime.datetime.now().date())})
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name
    lista = []
    saldo_cuenta = 0.0
    saldo_ini = 0.0
    saldo_final = 0.0
    textodescriptivo = ''
    fechacortedescriptiva = ''
    if request.method == "POST":
        buscador = ReporteSaldosForm(request.POST)
        if buscador.is_valid():
            if buscador.getBanco() is not None and buscador.getFechaCorte() is not None:
                cuenta_banco = Cuenta_Banco.objects.get(status=1, id=buscador.getBanco().id)
                anio = buscador.getFechaCorte().year
                print unicode(anio)+'-'+unicode(buscador.getFechaCorte().month)+'-'+unicode(buscador.getFechaCorte().day)+"el texto del anio de a fecha de corte"
                textodescriptivo = unicode(buscador.getBanco())
                fechacortedescriptiva = unicode(anio)+'-'+unicode(buscador.getFechaCorte().month)+'-'+unicode(buscador.getFechaCorte().day)
                #  Actualiza la tabla de saldo de cuentas
                cursor = connection.cursor()
                nombrecta = unicode(cuenta_banco.descripcion)
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                cursor.close()

                try:
                    saldo_cuenta_model = SaldoCuenta.objects.get(plan_cuenta=cuenta_banco.plan_cuenta, anio=anio)
                    saldo_cuenta_inicial = convert_cero_none(saldo_cuenta_model.saldo_inicial)
                    saldo_cuenta_debe_acum = convert_cero_none(saldo_cuenta_model.get_saldo_debe_acum(buscador.getFechaCorte().month))
                    saldo_cuenta_haber_acum = convert_cero_none(saldo_cuenta_model.get_saldo_haber_acum(buscador.getFechaCorte().month))
                    saldo_cuenta = saldo_cuenta_inicial + saldo_cuenta_debe_acum - saldo_cuenta_haber_acum
                except SaldoCuenta.DoesNotExist:
                    messages.error(request, u"No se encuentra registrado la cuenta en saldo cuenta")
                    saldo_cuenta = 0.0


                if buscador.getSaldoBanco() != "":
                    saldo_ini = float(buscador.getSaldoBanco())
                else:
                    saldo_ini = 0.0

                saldo = saldo_ini

                for tc in TipoComprobante.objects.filter(status=1):

                    lista_bancos = Banco.objects.exclude(conciliado=True).filter(cuenta_banco=cuenta_banco,
                                                                                 status=1, fecha_reg__lte=buscador.getFechaCorte(),
                                                                                 tipo_comprobante=tc).order_by("fecha_reg")
                    lista_conciliacion = ListaBancosConciliacion()
                    lista_conciliacion.tipo_comp = tc
                    lista_conciliacion.lista = lista_bancos

                    for b in lista_bancos:
                        if b.naturaleza == 1:
                            saldo += b.valor
                            total = saldo
                        else:
                            saldo -= b.valor
                            total = saldo
                        saldo_final = saldo
                        lista_conciliacion.saldo = saldo
                        lista_conciliacion.total = total

                    lista.append(lista_conciliacion)

    html = render_to_string('reporte_bancos/reporte_conciliacion_bancaria_pdf.html',
                            {'pagesize': 'A4',
                             "buscador": buscador,
                             "lista": lista,
                             "empresa": empresa,
                             "nombrecta": textodescriptivo,
                             "fechacortedescriptiva": fechacortedescriptiva,
                             "usuario": usuario,
                             "saldo_ini": saldo_ini,
                             "saldo_final": saldo_final,
                             "saldo_cuenta": saldo_cuenta}, context_instance=RequestContext(request))

    #return generar_pdf(html)
    nombre = 'Rpt_conciliacion_bancaria_'+str(buscador.getFechaCorte())
    return generar_pdf_nombre(html,nombre)


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_transacciones_bancarias_por_conciliar_excel(request):
    '''
    Vista que genera el reporte en excel de conciliacion de transacciones bancarias
    :param request:
    :return:
    '''
    lista = []
    lista_tmp = []
    lista.append([u"", u"", u"", u"REPORTE DE TRANSACCIONES BANCARIAS POR CONCILIAR"+TITULO])
    lista.append([u""])
    saldo_cuenta = 0.0
    saldo_final = 0.0
    textodescriptivo = ''
    fechacortedescriptiva = ''

    if request.method == "POST":
        buscador = ReporteSaldosForm(request.POST)
        if buscador.is_valid():
            if buscador.getBanco() is not None and buscador.getFechaCorte() is not None:
                cuenta_banco = Cuenta_Banco.objects.get(status=1, id=buscador.getBanco().id)
                anio = buscador.getFechaCorte().year
                textodescriptivo = unicode(buscador.getBanco())
                fechacortedescriptiva = unicode(anio)+'-'+unicode(buscador.getFechaCorte().month)+'-'+unicode(buscador.getFechaCorte().day)

                #  Actualiza la tabla de saldo de cuentas
                cursor = connection.cursor()
                cursor.execute('SELECT populate_saldo_cuenta(%s);', [anio])
                cursor.close()

                try:
                    saldo_cuenta_model = SaldoCuenta.objects.get(plan_cuenta=cuenta_banco.plan_cuenta, anio=anio)
                    saldo_cuenta_inicial = convert_cero_none(saldo_cuenta_model.saldo_inicial)
                    saldo_cuenta_debe_acum = convert_cero_none(saldo_cuenta_model.get_saldo_debe_acum(buscador.getFechaCorte().month))
                    saldo_cuenta_haber_acum = convert_cero_none(saldo_cuenta_model.get_saldo_haber_acum(buscador.getFechaCorte().month))
                    saldo_cuenta = saldo_cuenta_inicial + saldo_cuenta_debe_acum - saldo_cuenta_haber_acum
                except SaldoCuenta.DoesNotExist:
                    saldo_cuenta = 0.0


                if buscador.getSaldoBanco() != "":
                    saldo_ini = float(buscador.getSaldoBanco())
                else:
                    saldo_ini = 0.0

                saldo = saldo_ini

                lista.append([u""])
                #lista.append([u"Banco: "+SUBTITULO, unicode(cuenta_banco.descripcion) + " - " + str(cuenta_banco.numero_cuenta)])
                lista.append([u"Banco: "+SUBTITULO, unicode(textodescriptivo)])
                lista.append([u"Fecha de Corte: "+SUBTITULO, unicode(fechacortedescriptiva)])
                lista.append([u"Saldo Inicial: "+SUBTITULO, unicode(saldo_ini)])
                lista.append([u""])


                for tc in TipoComprobante.objects.filter(status=1):
                    if Banco.objects.exclude(conciliado=True).filter(cuenta_banco=cuenta_banco, status=1, fecha_reg__lte=buscador.getFechaCorte(), tipo_comprobante=tc).exists():
                        lista_bancos = Banco.objects.exclude(conciliado=True).filter(cuenta_banco=cuenta_banco, status=1, fecha_reg__lte=buscador.getFechaCorte(), tipo_comprobante=tc).order_by("fecha_reg", "num_comp")
                        lista_conciliacion = ListaBancosConciliacion(lista_bancos)

                        for b in lista_conciliacion.lista:
                            if b.naturaleza == 1:
                                saldo += b.valor
                                total = saldo
                            else:
                                saldo -= b.valor
                                total = saldo

                            lista_conciliacion.tipo_comp = b.tipo_comprobante
                            lista_conciliacion.saldo = saldo
                            lista_conciliacion.total = total
                            saldo_final = saldo

                        lista_tmp.append(lista_conciliacion)

                lista.append([u""])
                for obj_list in lista_tmp:
                    lista.append([u"Tipo Comprobante: "+SUBTITULO, unicode(obj_list.tipo_comp.descripcion)])
                    lista.append([u""])
                    lista.append([u"#Comprobante"+COLUMNA, u"Fecha Registro"+COLUMNA, u"Referencia"+COLUMNA, u"Recibido/Pagado"+COLUMNA,
                                              u"Concepto"+COLUMNA, u""+COLUMNA,  u"Valor"+COLUMNA])

                    for b in obj_list.lista:
                        if b.num_cheque not in (None, ""):
                            referencia = u"Cheque #"+str(b.num_cheque)
                        else:
                            referencia = b.num_referencia

                        if b.concepto not in (None, ""):
                            concepto = unicode(b.concepto)
                        else:
                            concepto = ""

                        if b.persona not in (None, ""):
                            persona = unicode(b.persona)
                        else:
                            persona = ""

                        if b.naturaleza == 1:
                            signo = "                             (+)"
                        else:
                            signo = "                              (-)"

                        lista.append([b.num_comp, b.fecha_reg, referencia, unicode(persona), unicode(concepto), signo, b.valor])

                    lista.append([u"Totales"+SUBTITULO, u"", u"", u"", u"", u"", str(obj_list.total)+G_TOTAL])
                    lista.append([u""])

            lista.append([u""])
            lista.append([u"", u"Cuadre Contable"+SUBTITULO])
            lista.append([u""])
            lista.append([u"", u"Total Libro Banco"+COLUMNA, u"Total Saldo Cuenta"+COLUMNA])
            lista.append([u"", u""+G_TOTAL, u""+G_TOTAL])
            lista.append([u"", str(saldo_final), str(saldo_cuenta)])
            lista.append([u""])
            lista.append([u"", u"Observación"+COLUMNA])
            if saldo_final != saldo_cuenta:
                lista.append([u"", u"Existe un error en el cuadre contable"])
            if saldo_final == saldo_cuenta:
                lista.append([u"", u"El Cuadre Contable es correcto"])

    nombre = 'Rpt_conciliacion_bancaria_'+str(buscador.getFechaCorte())
    return ExcelResponse(lista, nombre)#cambio1


#####################################################################################################################

