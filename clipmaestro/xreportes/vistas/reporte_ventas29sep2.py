#! /usr/bin/python
# -*- coding: UTF-8-*-
__author__ = 'Clip Maestro'
from django.template import RequestContext
from django.shortcuts import render_to_response
from contabilidad.models import *
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.db import connection
import cStringIO as StringIO
import ho.pisa as pisa
from django import forms
from django.utils.html import *
from  contabilidad.models import *
from reportes.formularios.reportes_form import *
from librerias.funciones.funciones_vistas import *
from django.db.models import Sum
from librerias.funciones.excel_response_mod import *
from librerias.funciones.permisos import *
import requests
from django.contrib import messages

############################################ REPORTE VENTAS POR FECHA ##################################################
@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_venta_por_fecha_SINJASPER(request):
    query = []
    base0 = 0.0
    baseiva = 0.0
    monto = 0.0
    total_desc = 0.0
    total_ice = 0.0
    total_venta = 0.0
    total_ret = 0.0
    cobrar = 0.0
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        if buscador.is_valid():
            fecha_ini = buscador.getFechaIni()
            fecha_final = buscador.getFechaFinal()
            if None not in (buscador.getFechaIni(), buscador.getFechaFinal()):
                query = Venta_Cabecera.objects.exclude(Q(status=0)).filter(fecha__range=(fecha_ini, fecha_final)).order_by("fecha", "numero_comprobante")
                # Totales
                base0 = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
                baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
                monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('iva_total', field="monto_iva"))["iva_total__sum"])
                total_desc = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
                total_ice = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
                total_venta = base0 + baseiva + monto + total_ice - total_desc
                total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final), venta_cabecera__status=1),
                                                                       cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])

                cobrar = total_venta - total_ret
            else:
                if fecha_ini is None:
                    errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                    errors.append(u"La fecha inicial es requerida")
                if fecha_final is None:
                    errors = buscador._errors.setdefault("fecha_final", ErrorList())
                    errors.append(u"La fecha final es requerida")

    return render_to_response('reporte_ventas/reporte_venta_fecha.html',
                              {"buscador": buscador,
                               "objetos": query,
                               "base0": base0,
                               "baseiva": baseiva,
                               "monto": monto,
                               "total_ice": total_ice,
                               "total_desc": total_desc,
                               "total_venta": total_venta,
                               "total_ret": total_ret,
                               "cobrar": cobrar
                               }, context_instance=RequestContext(request))

#esto es el nuevo 104 con jasper
@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_venta_por_fecha(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                report = get_nombre_reporte('VENTA_VENTAS_X_FECHAS') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reporte_ventas/reporte_venta_fecha.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))



@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_venta_por_fecha_pdf(request):
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    query = []
    base0 = 0.0
    baseiva = 0.0
    monto = 0.0
    total_desc = 0.0
    total_ice = 0.0
    total_venta = 0.0
    total_ret = 0.0
    cobrar = 0.0
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)

        if buscador.is_valid():
            fecha_ini = buscador.getFechaIni()
            fecha_final = buscador.getFechaFinal()
            if None not in (fecha_ini, fecha_final):
                query = Venta_Cabecera.objects.exclude(Q(status=0)|Q(status=3)).filter(fecha__range=(fecha_ini, fecha_final)).order_by("fecha", "numero_comprobante")
                # Totales
                base0 = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
                baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
                monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('iva_total', field="monto_iva"))["iva_total__sum"])
                total_desc = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
                total_ice = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
                total_venta = base0 + baseiva + monto + total_ice - total_desc
                total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final), venta_cabecera__status=1),
                                                                       cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])

                cobrar = total_venta - total_ret
            else:
                if fecha_ini is None:
                    errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                    errors.append(u"La fecha inicial es requerida")
                if fecha_final is None:
                    errors = buscador._errors.setdefault("fecha_final", ErrorList())
                    errors.append(u"La fecha final es requerida")
                return render_to_response('reporte_ventas/reporte_venta_fecha.html',
                                          {"buscador": buscador,
                                           "objetos": query,
                                           "base0": base0,
                                           "baseiva": baseiva,
                                           "monto": monto,
                                           "total_ice": total_ice,
                                           "total_desc": total_desc,
                                           "total_venta": total_venta,
                                           "total_ret": total_ret,
                                           "cobrar": cobrar
                                           }, context_instance=RequestContext(request))

    html = render_to_string('reporte_ventas/reporte_venta_fecha_pdf.html',
                            {'pagesize': 'A4',
                             "buscador": buscador,
                             "objetos": query,
                             "base0": base0,
                             "baseiva": baseiva,
                             "monto": monto,
                             "total_ice": total_ice,
                             "total_desc": total_desc,
                             "total_venta": total_venta,
                             "total_ret": total_ret,
                             "empresa":empresa,
                             "usuario":usuario,
                             "cobrar": cobrar
                             }, context_instance=RequestContext(request))
    nombre = 'Rpt_vtas_del_'+str(fecha_ini)+'_al_'+str(fecha_final)
    return generar_pdf_nombre(html, nombre)

@csrf_exempt
@login_required()
@permiso_accion(mensaje=mensaje_permiso)
def reporte_venta_por_fecha_excel(request):
    query = []
    base0 = 0.0
    baseiva = 0.0
    monto = 0.0
    total_desc = 0.0
    total_ice = 0.0
    total_venta = 0.0
    total_ret = 0.0
    cobrar = 0.0
    lista = []
    lista.append([u"", u"", u"", u"", u"", u"REPORTE VENTAS POR FECHA"+TITULO])
    lista.append([u""])

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()

        if None not in (fecha_ini, fecha_final):
            query = Venta_Cabecera.objects.exclude(Q(status=0)|Q(status=3)).filter(fecha__range=(fecha_ini, fecha_final)).order_by("fecha", "numero_comprobante")

            ########## Totales #######
            base0 = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
            baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
            monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('iva_total', field="monto_iva"))["iva_total__sum"])
            total_desc = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
            total_ice = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final)).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
            total_venta = base0 + baseiva + monto + total_ice - total_desc
            total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(
                                                                    venta_cabecera__fecha__range=(fecha_ini, fecha_final), venta_cabecera__status=1), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16),
                                                                   status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])

            cobrar = total_venta - total_ret

            lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_final])
            lista.append([ u"# Commprobante"+COLUMNA, u"Fecha"+COLUMNA, u"Tipo Doc."+COLUMNA, u"# Documento"+COLUMNA,
                          u"Cliente"+COLUMNA, u"Base 0"+COLUMNA, u"Base IVA"+COLUMNA, u"Monto Iva"+COLUMNA,
                          u"Monto ICE"+COLUMNA, u"Descuento"+COLUMNA, u"Total Vta"+COLUMNA, u"Total Ret."+COLUMNA,
                          u"Por Cobrar"+COLUMNA, u"Estado"+COLUMNA])


            for obj in query:
                total_venta_in = obj.subtotal_tarifa0 + obj.subtotal_tarifa12 + obj.iva_total + obj.total_ice - \
                              (obj.descuento_tarifa0 + obj.descuento_tarifa12)

                if obj.status == 1:
                    if obj.getRetencionVenta():
                        retencion = convert_cero_none(obj.getRetencionVta().monto)
                    else:
                        retencion = 0.0

                    fila = [obj.numero_comprobante, obj.fecha,  obj.vigencia_doc_empresa.documento_sri.descripcion_documento,
                            str(obj.vigencia_doc_empresa.serie) + "-" + obj.num_documento,
                            unicode(obj.cliente.razon_social), obj.subtotal_tarifa0,
                            obj.subtotal_tarifa12, obj.iva_total, obj.total_ice,
                            obj.descuento_tarifa0 + obj.descuento_tarifa12,
                            total_venta_in, retencion, total_venta - retencion, u"Activo"]
                else:

                       if obj.cliente is not None:
                           cliente = unicode(obj.cliente.razon_social)
                       else:
                           cliente = ""

                       fila = [obj.numero_comprobante, obj.fecha, obj.vigencia_doc_empresa.documento_sri.descripcion_documento,
                            str(obj.vigencia_doc_empresa.serie) + "-" + obj.num_documento,
                            cliente, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, u"Anulado"]

                lista.append(fila)

            lista.append([u""])
            lista.append([u"Totales"+SUBTITULO, u"", u"", u"", u"", str(base0)+G_TOTAL, str(baseiva)+G_TOTAL, str(monto)+G_TOTAL, str(total_ice)+G_TOTAL,
                          str(total_desc)+G_TOTAL, str(total_venta)+G_TOTAL, str(total_ret)+G_TOTAL, str(cobrar)+G_TOTAL])
        else:
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerida")
            if fecha_final is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerida")
            return render_to_response('reporte_ventas/reporte_venta_fecha.html',
                                      {"buscador": buscador,
                                       "objetos": query,
                                       "base0": base0,
                                       "baseiva": baseiva,
                                       "monto": monto,
                                       "total_ice": total_ice,
                                       "total_desc": total_desc,
                                       "total_venta": total_venta,
                                       "total_ret": total_ret,
                                       "cobrar": cobrar
                                       }, context_instance=RequestContext(request))

        nombre = 'Rpt_vtas_del_'+str(fecha_ini)+'_al_'+str(fecha_final)
        return ExcelResponse(lista, unicode(nombre))
    else:
        return ExcelResponse(lista)
########################################################################################################################

class ListaVentas():
    """
    Clase que me sirve para guardar la información completa de una venta
    con una lista en la cual se contendrán datos dependiendo.
    """
    def __init__(self, lista, num_doc="", cliente="", cantidad_pendiente=0):
        self.num_doc = num_doc
        self.cliente = cliente
        self.lista = lista
        self.cantidad_pendiente = cantidad_pendiente

class Items_Venta():
    """
    Clase que me sirve para agrupar las ventas asociadas al item de la consulta
    """
    def __init__(self, lista, item="", cliente=""):
        self.item = item
        self.cliente = cliente
        self.lista = lista

class DatosItemVentas():
    """
    Clase que me ayuda a agrupar la información de las Ventas
    por Item
    """
    def __init__(self):
        self.item = Item()
        self.lista_ventas = []
        self.base0 = 0.0
        self.baseiva = 0.0
        self.monto = 0.0
        self.total_desc = 0.0
        self.total_ice = 0.0
        self.total_venta = 0.0
        self.total_ret = 0.0
        self.cobrar = 0.0

class DatosClienteVentas():
    """
    Clase que me ayuda a agrupar la información de las Ventas
    por Cliente
    """
    def __init__(self):
        self.cliente = Cliente()
        self.lista_ventas = []
        self.base0 = 0.0
        self.baseiva = 0.0
        self.monto = 0.0
        self.total_desc = 0.0
        self.total_ice = 0.0
        self.total_venta = 0.0
        self.total_ret = 0.0
        self.cobrar = 0.0

def listar_venta_por_cliente(buscador, clientes, lista_id_clientes, query):
    '''
    Fución encargada de agrupar la lista de las ventas según el cliente
    :return:
    '''
    fecha_ini = buscador.getFechaIni()
    fecha_final = buscador.getFechaFinal()

    if None not in (fecha_ini, fecha_final):
        buscador.is_valid()                         # Se llama a la función para usar cleaned_data
        if buscador.getAllClient():                 # Si escojió la opción de todos los clientes
            all_client = Cliente.objects.filter(venta_cabecera__in=Venta_Cabecera.objects.filter(status=1, fecha__range=(fecha_ini, fecha_final)).distinct("cliente"))
            for cliente in all_client:
                lista_id_clientes.append(cliente.id)
                datos_cliente_venta = DatosClienteVentas()
                datos_cliente_venta.cliente = cliente
                datos_cliente_venta.lista_ventas = Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).order_by("fecha", "numero_comprobante")
                datos_cliente_venta.base0 = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
                datos_cliente_venta.baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
                datos_cliente_venta.monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('iva_total', field="monto_iva"))["iva_total__sum"])
                datos_cliente_venta.total_desc = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
                datos_cliente_venta.total_ice = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
                datos_cliente_venta.total_venta = datos_cliente_venta.base0 + datos_cliente_venta.baseiva + datos_cliente_venta.monto + datos_cliente_venta.total_ice - datos_cliente_venta.total_desc
                datos_cliente_venta.total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final), cliente=cliente, venta_cabecera__status=1), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])
                datos_cliente_venta.cobrar = datos_cliente_venta.total_venta - datos_cliente_venta.total_ret
                query.append(datos_cliente_venta)

        else:

            if clientes.is_valid():
                for obj in clientes:
                    informacion = obj.cleaned_data
                    try:
                        cliente = Cliente.objects.get(id=informacion.get("cliente"))
                        if cliente.id not in lista_id_clientes:
                            lista_id_clientes.append(cliente.id)
                            datos_cliente_venta = DatosClienteVentas()
                            datos_cliente_venta.cliente = cliente
                            datos_cliente_venta.lista_ventas = Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).order_by("fecha", "numero_comprobante")
                            datos_cliente_venta.base0 = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
                            datos_cliente_venta.baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
                            datos_cliente_venta.monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('iva_total', field="monto_iva"))["iva_total__sum"])
                            datos_cliente_venta.total_desc = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
                            datos_cliente_venta.total_ice = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
                            datos_cliente_venta.total_venta = datos_cliente_venta.base0 + datos_cliente_venta.baseiva + datos_cliente_venta.monto + datos_cliente_venta.total_ice - datos_cliente_venta.total_desc
                            datos_cliente_venta.total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final), cliente=cliente, venta_cabecera__status=1), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])
                            datos_cliente_venta.cobrar = datos_cliente_venta.total_venta - datos_cliente_venta.total_ret
                            query.append(datos_cliente_venta)
                    except Cliente.DoesNotExist:
                        pass

def lista_venta_clientes_all_excel(lista, lista_id_clientes, fecha_ini, fecha_final):
    '''
    Fución que inserta en una lista todos las ventas por el cliente
    indicada para la hoja de excel
    :param lista:
    :param lista_id_clientes:
    :param fecha_ini:
    :param fecha_final:
    :return:
    '''
    all_client = Cliente.objects.filter(venta_cabecera__in=Venta_Cabecera.objects.filter(status=1, fecha__range=(fecha_ini, fecha_final)).distinct("cliente"))
    for cliente in all_client:
        lista_id_clientes.append(cliente.id)
        datos_cliente_venta = DatosClienteVentas()
        datos_cliente_venta.cliente = cliente
        lista.append([u"Cliente: "+SUBTITULO,  datos_cliente_venta.cliente.razon_social + u"(" + datos_cliente_venta.cliente.ruc+ u")"])
        lista.append([u"# Commprobante"+COLUMNA, u"Fecha"+COLUMNA, u"# Documento"+COLUMNA,
                      u"Base 0"+COLUMNA, u"Base IVA"+COLUMNA, u"Descuento"+COLUMNA, u"Monto Iva"+COLUMNA, u"Monto ICE"+COLUMNA,
                       u"Total Vta"+COLUMNA, u"Total Ret."+COLUMNA, u"Por Cobrar"+COLUMNA,
                      u"Estado"+COLUMNA])

        datos_cliente_venta.lista_ventas = Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).order_by("fecha", "numero_comprobante")
        datos_cliente_venta.base0 = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
        datos_cliente_venta.baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
        datos_cliente_venta.monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('iva_total', field="monto_iva"))["iva_total__sum"])
        datos_cliente_venta.total_desc = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
        datos_cliente_venta.total_ice = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
        datos_cliente_venta.total_venta = datos_cliente_venta.base0 + datos_cliente_venta.baseiva + datos_cliente_venta.monto + datos_cliente_venta.total_ice - datos_cliente_venta.total_desc
        datos_cliente_venta.total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final), cliente=cliente, venta_cabecera__status=1), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])
        datos_cliente_venta.cobrar = datos_cliente_venta.total_venta - datos_cliente_venta.total_ret

        for obj in datos_cliente_venta.lista_ventas:
            if obj.getRetencionVenta():
                retencion = convert_cero_none(obj.getRetencionVta().monto)
            else:
                retencion = 0.0

            fila = [obj.numero_comprobante, str(obj.fecha), str(obj.vigencia_doc_empresa.serie) + "-" + obj.num_documento,
                            obj.subtotal_tarifa0, obj.subtotal_tarifa12, obj.descuento_tarifa0 + obj.descuento_tarifa12, obj.iva_total, obj.total_ice,
                            obj.getTotalPagar(), retencion, obj.getTotalPagar() - retencion, str(u"Activo")+COLUMNA]

            lista.append(fila)

        lista.append([u"Totales"+SUBTITULO, u"", u"", str(datos_cliente_venta.base0)+G_TOTAL, str(datos_cliente_venta.baseiva)+G_TOTAL,
                      str(datos_cliente_venta.total_desc)+G_TOTAL, str(datos_cliente_venta.monto)+G_TOTAL, str(datos_cliente_venta.total_ice)+G_TOTAL,
                      str(datos_cliente_venta.total_venta)+G_TOTAL, str(datos_cliente_venta.total_ret)+G_TOTAL, str(datos_cliente_venta.cobrar)+G_TOTAL])

        lista.append([u"", u"", u"", u"", u"", u"", u"", u""])

def lista_ventas_cliente_select_excel(obj, lista, lista_id_clientes, fecha_ini, fecha_final):
    '''
    Fución que inserta en una lista las ventas por el cliente seleccionado
    indicada para la hoja de excel
    :param obj:
    :param lista:
    :param lista_id_clientes:
    :param fecha_ini:
    :param fecha_final:
    :return:
    '''
    informacion = obj.cleaned_data
    try:
        cliente = Cliente.objects.get(id=informacion.get("cliente"))
        if cliente.id not in lista_id_clientes:
            lista_id_clientes.append(cliente.id)
            datos_cliente_venta = DatosClienteVentas()
            datos_cliente_venta.cliente = cliente
            lista.append([u"Cliente: "+SUBTITULO,  datos_cliente_venta.cliente.razon_social + u"(" + datos_cliente_venta.cliente.ruc+ u")"])
            lista.append([u"# Commprobante"+COLUMNA, u"Fecha"+COLUMNA, u"# Documento"+COLUMNA,
                          u"Base 0"+COLUMNA, u"Base IVA"+COLUMNA,   u"Descuento"+COLUMNA, u"Monto Iva"+COLUMNA, u"Monto ICE"+COLUMNA,
                         u"Total Vta"+COLUMNA, u"Total Ret."+COLUMNA, u"Por Cobrar"+COLUMNA,
                          u"Estado"+COLUMNA])

            datos_cliente_venta.lista_ventas = Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).order_by("fecha", "numero_comprobante")
            datos_cliente_venta.base0 = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
            datos_cliente_venta.baseiva = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
            datos_cliente_venta.monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('iva_total', field="monto_iva"))["iva_total__sum"])
            datos_cliente_venta.total_desc = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
            datos_cliente_venta.total_ice = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), cliente=cliente).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
            datos_cliente_venta.total_venta = datos_cliente_venta.base0 + datos_cliente_venta.baseiva + datos_cliente_venta.monto + datos_cliente_venta.total_ice - datos_cliente_venta.total_desc
            datos_cliente_venta.total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final), cliente=cliente, venta_cabecera__status=1), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])
            datos_cliente_venta.cobrar = datos_cliente_venta.total_venta - datos_cliente_venta.total_ret

            for obj in datos_cliente_venta.lista_ventas:
                if obj.getRetencionVenta():
                    retencion = convert_cero_none(obj.getRetencionVta().monto)
                else:
                    retencion = 0.0

                fila = [obj.numero_comprobante, str(obj.fecha), str(obj.vigencia_doc_empresa.serie)+"-"+obj.num_documento,
                                obj.subtotal_tarifa0, obj.subtotal_tarifa12, obj.descuento_tarifa0 + obj.descuento_tarifa12, obj.iva_total, obj.total_ice,
                                obj.getTotalPagar(), retencion, obj.getTotalPagar() - retencion, str(u"Activo")+COLUMNA]
                lista.append(fila)

            lista.append([u"Totales"+SUBTITULO, u"", u"", str(datos_cliente_venta.base0)+G_TOTAL, str(datos_cliente_venta.baseiva)+G_TOTAL,
                           str(datos_cliente_venta.total_desc)+G_TOTAL, str(datos_cliente_venta.monto)+G_TOTAL, str(datos_cliente_venta.total_ice)+G_TOTAL,
                           str(datos_cliente_venta.total_venta)+G_TOTAL, str(datos_cliente_venta.total_ret)+G_TOTAL, str(datos_cliente_venta.cobrar)+G_TOTAL])
        lista.append([u"", u"", u"", u"", u"", u"", u"", u""])

    except:
        pass

def listar_venta_por_item(buscador, items, lista_id_items, query):
    '''
    Fución encargada de agrupar la lista de las ventas según el cliente
    :return:
    '''
    fecha_ini = buscador.getFechaIni()
    fecha_final = buscador.getFechaFinal()

    if None not in (fecha_ini, fecha_final):
        buscador.is_valid()                         # Se llama a la función para usar cleaned_data
        if buscador.getAllItems():                  # Si escojió la opción de todos los items
            print 'he seleccionado todos los item'
            all_items = Item.objects.filter(venta_detalle__in=Venta_Detalle.objects.filter(status=1, venta_cabecera__fecha__range=(fecha_ini, fecha_final)).distinct("item"))
            for item in all_items:
                lista_id_items.append(item.id)
                datos_cliente_venta = DatosItemVentas()
                datos_cliente_venta.item = item
                datos_cliente_venta.lista_ventas = Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).order_by("fecha", "numero_comprobante").distinct()

                datos_cliente_venta.base0 = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
                datos_cliente_venta.baseiva = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
                datos_cliente_venta.monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('iva_total', field="venta_cabecera.monto_iva"))["iva_total__sum"])
                datos_cliente_venta.total_desc = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
                datos_cliente_venta.total_ice = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
                datos_cliente_venta.total_venta = datos_cliente_venta.base0 + datos_cliente_venta.baseiva + datos_cliente_venta.monto + datos_cliente_venta.total_ice - datos_cliente_venta.total_desc
                datos_cliente_venta.total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__in=Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item))), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])

                datos_cliente_venta.cobrar = datos_cliente_venta.total_venta - datos_cliente_venta.total_ret
                query.append(datos_cliente_venta)
        else:
            if items.is_valid():
                for obj in items:
                    informacion = obj.cleaned_data
                    try:
                        print 'el item de cbox'
                        print informacion.get("item")
                        print 'el item de cbox'
                        item = Item.objects.get(id=informacion.get("item"))

                        print item
                        if item.id not in lista_id_items:
                            lista_id_items.append(item.id)
                            datos_cliente_venta = DatosItemVentas()
                            datos_cliente_venta.item = item
                            print 'el item en la lista'
                            print item
                            datos_cliente_venta.lista_ventas = Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).order_by("fecha", "numero_comprobante").distinct()
                            datos_cliente_venta.base0 = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
                            datos_cliente_venta.baseiva = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
                            datos_cliente_venta.monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('iva_total', field="venta_cabecera.monto_iva"))["iva_total__sum"])
                            datos_cliente_venta.total_desc = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
                            datos_cliente_venta.total_ice = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
                            datos_cliente_venta.total_venta = datos_cliente_venta.base0 + datos_cliente_venta.baseiva + datos_cliente_venta.monto + datos_cliente_venta.total_ice - datos_cliente_venta.total_desc
                            print 'agregando3'
                            datos_cliente_venta.total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__in=Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item))), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])
                            datos_cliente_venta.cobrar = datos_cliente_venta.total_venta - datos_cliente_venta.total_ret

                            query.append(datos_cliente_venta)
                            print query
                    except Item.DoesNotExist:
                        pass

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_ventas_cliente(request):
    parametro = 0
    query = []
    lista_id_clientes = []
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",  "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    proveed_formset = formset_factory(ReporteSaldosForm)
    clientes = proveed_formset(prefix='clientes')

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        clientes = proveed_formset(request.POST, prefix='clientes')
        buscador.is_valid()
        listar_venta_por_cliente(buscador, clientes, lista_id_clientes, query)  # Función que realiza la consulta
    print('tamanio lista de los clientessssss')
    print(len(query))
    for nodo in query:
        print nodo.cobrar
    return render_to_response('reporte_ventas/reporte_venta_fecha_cliente.html',
                              {"buscador": buscador, "parametro": parametro,
                               "objetos": query, "clientes": clientes}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_ventas_cliente_pdf(request):
    parametro = 0
    query = []
    lista_id_clientes = []
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",  "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    proveed_formset = formset_factory(ReporteSaldosForm)
    clientes = proveed_formset(prefix='clientes')
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        clientes = proveed_formset(request.POST, prefix='clientes')
        buscador.is_valid()
        listar_venta_por_cliente(buscador, clientes, lista_id_clientes, query)  # Función que realiza la consulta

    html = render_to_string('reporte_ventas/reporte_ventas_fecha_cliente_pdf.html',
                            {'pagesize': 'A4',
                             "buscador": buscador,
                             "parametro": parametro, "objetos": query,
                             "usuario": usuario, "empresa": empresa,
                             "desde": buscador.getFechaIni(),
                             "hasta": buscador.getFechaFinal(),
                             "clientes": clientes
                             }, context_instance=RequestContext(request))
    nombre = 'Rpt_vtas_por_cliente_del_'+str(buscador.getFechaIni())+'_al_'+str(buscador.getFechaFinal())
    return generar_pdf_nombre(html, nombre)


@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_ventas_clientes_excel(request):
    lista = []
    lista.append([u"", u"", u"", u"", u"", u"REPORTE DE VENTAS POR CLIENTES"+TITULO])
    lista.append([u""])

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        cliente_formset = formset_factory(ReporteSaldosForm)
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        lista_id_clientes = []      # Lista que contiene los id de los clientes para que no hayan duplicados en el reporte

        if None not in (fecha_ini, fecha_final):
            lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini.strftime("%d-%m-%Y")])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_final.strftime("%d-%m-%Y")])
            lista.append([u""])
            clientes = cliente_formset(request.POST, prefix='clientes')

            if buscador.getAllClient():                 # Si escojió la opción de todos los clientes
                lista_venta_clientes_all_excel(lista, lista_id_clientes, fecha_ini, fecha_final)
            else:
                if clientes.is_valid():
                    for obj in clientes:
                        lista_ventas_cliente_select_excel(obj, lista, lista_id_clientes, fecha_ini, fecha_final)
            return ExcelResponse(lista, u'Reporte Ventas por Cliente desde '+ fecha_ini.strftime("%Y-%m-%d") + u' hasta: ' + fecha_final.strftime("%Y-%m-%d"))
        else:
            return ExcelResponse(lista)
    else:
        return ExcelResponse(lista)


@login_required(login_url='/')
@csrf_exempt
def reporte_ventas_item(request):
    '''
    Vista del reporte Ventas por Fecha e Items
    :param request:
    :return:
    '''
    parametro = 0
    query = []
    lista_id_items = []
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    item_formset = formset_factory(ReporteSaldosForm)
    items = item_formset(prefix='items')

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        items = item_formset(request.POST, prefix='items')
        buscador.is_valid()
        listar_venta_por_item(buscador, items, lista_id_items, query)  # Función que realiza la consulta

    return render_to_response('reporte_ventas/reporte_ventas_fecha_item.html',
                              {"buscador": buscador,
                               "parametro": parametro,
                               "objetos": query,
                               "items": items}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
def reporte_ventas_item_pdf(request):
    '''
    Vista del reporte Ventas por Fecha e Items
    :param request:
    :return:
    '''
    parametro = 0
    query = []
    lista_id_items = []
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01", "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    item_formset = formset_factory(ReporteSaldosForm)
    items = item_formset(prefix='items')
    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        items = item_formset(request.POST, prefix='items')
        buscador.is_valid()
        listar_venta_por_item(buscador, items, lista_id_items, query)  # Función que realiza la consulta

    html = render_to_string('reporte_ventas/reporte_ventas_fecha_item_pdf.html',
                            {'pagesize': 'A4',
                             "buscador": buscador,
                             "parametro": parametro,
                             "usuario": usuario, "empresa": empresa,
                             "desde": buscador.getFechaIni(),
                             "hasta": buscador.getFechaFinal(),
                             "objetos": query, "items": items
                             }, context_instance=RequestContext(request))
    nombre = ( u'Rpt_Ventas_Item_desde_'+buscador.getFechaIni().strftime("%Y-%m-%d") + u'_a_' +buscador.getFechaFinal().strftime("%Y-%m-%d"))
    return generar_pdf_nombre(html, nombre)


def lista_ventas_items_excel(buscador, items, lista, lista_id_items, fecha_ini, fecha_final):
    '''
    Función que llena la lista con la consulta del reporte ventas por item para exportar
    a excel
    :param buscador:
    :param items:
    :param lista:
    :param lista_id_items:
    :param fecha_ini:
    :param fecha_final:
    :return:
    '''
    if buscador.getAllItems():
        all_items = Item.objects.filter(venta_detalle__in=Venta_Detalle.objects.filter(status=1, venta_cabecera__fecha__range=(fecha_ini, fecha_final)).distinct("item"))
        for item in all_items:
            lista_id_items.append(item.id)
            datos_cliente_venta = DatosItemVentas()
            datos_cliente_venta.item = item
            lista.append([u"Item: "+SUBTITULO,  u"(" + datos_cliente_venta.item.codigo+u")"+" - "+datos_cliente_venta.item.nombre])

            lista.append([u"# Commprobante"+COLUMNA, u"Fecha"+COLUMNA, u"# Documento"+COLUMNA, u"Cliente"+COLUMNA,
                      u"Base 0"+COLUMNA, u"Base IVA"+COLUMNA, u"Descuento"+COLUMNA, u"Monto Iva"+COLUMNA, u"Monto ICE"+COLUMNA,
                       u"Total Vta"+COLUMNA, u"Total Ret."+COLUMNA, u"Por Cobrar"+COLUMNA, u"Estado"+COLUMNA])

            datos_cliente_venta.lista_ventas = Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).order_by("fecha", "numero_comprobante").distinct()
            datos_cliente_venta.base0 = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
            datos_cliente_venta.baseiva = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
            datos_cliente_venta.monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('iva_total', field="venta_cabecera.monto_iva"))["iva_total__sum"])
            datos_cliente_venta.total_desc = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
            datos_cliente_venta.total_ice = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
            datos_cliente_venta.total_venta = datos_cliente_venta.base0 + datos_cliente_venta.baseiva + datos_cliente_venta.monto + datos_cliente_venta.total_ice - datos_cliente_venta.total_desc
            datos_cliente_venta.total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__in=Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item))), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])
            datos_cliente_venta.cobrar = datos_cliente_venta.total_venta - datos_cliente_venta.total_ret

            for obj in datos_cliente_venta.lista_ventas:
                if obj.getRetencionVenta():
                    retencion = convert_cero_none(obj.getRetencionVta().monto)
                else:
                    retencion = 0.0

                fila = [obj.numero_comprobante, str(obj.fecha),
                                str(obj.vigencia_doc_empresa.serie) + "-" + obj.num_documento, obj.cliente.razon_social,
                                obj.subtotal_tarifa0, obj.subtotal_tarifa12, obj.descuento_tarifa0 + obj.descuento_tarifa12,
                                obj.iva_total, obj.total_ice, obj.getTotalPagar(), retencion, obj.getTotalPagar() - retencion, str(u"Activo")+COLUMNA]
                lista.append(fila)

            lista.append([u"", u"", u"", u"", u"", u"", u"", u"",u"", u"", u"", u"", u"", u"", u"", u""])
            lista.append([u"Totales"+SUBTITULO, u"", u"", u"", str(datos_cliente_venta.base0)+G_TOTAL, str(datos_cliente_venta.baseiva)+G_TOTAL,
                          str(datos_cliente_venta.total_desc)+G_TOTAL, str(datos_cliente_venta.monto)+G_TOTAL, str(datos_cliente_venta.total_ice)+G_TOTAL,
                          str(datos_cliente_venta.total_venta)+G_TOTAL, str(datos_cliente_venta.total_ret)+G_TOTAL, str(datos_cliente_venta.cobrar)+G_TOTAL])
            lista.append([u"", u"", u"", u"", u"", u"", u"", u""])
    else:

        if items.is_valid():
            for obj in items:
                informacion = obj.cleaned_data
                try:
                    item = Item.objects.get(id=informacion.get("item"))
                    if item.id not in lista_id_items:
                        lista_id_items.append(item.id)
                        datos_cliente_venta = DatosItemVentas()
                        datos_cliente_venta.item = item

                        lista.append([u"Item: "+SUBTITULO,  u"(" + datos_cliente_venta.item.codigo+u")"+" - "+datos_cliente_venta.item.nombre])
                        lista.append([u"# Commprobante"+COLUMNA, u"Fecha"+COLUMNA, u"# Documento"+COLUMNA, u"Cliente"+COLUMNA,
                           u"Base 0"+COLUMNA, u"Base IVA"+COLUMNA, u"Descuento"+COLUMNA, u"Monto Iva"+COLUMNA, u"Monto ICE"+COLUMNA,
                           u"Total Vta"+COLUMNA, u"Total Ret."+COLUMNA, u"Por Cobrar"+COLUMNA, u"Estado"+COLUMNA])

                        datos_cliente_venta.lista_ventas = Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).order_by("fecha", "numero_comprobante").distinct()
                        datos_cliente_venta.base0 = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('subtotal_tarifa0', field="base0"))["subtotal_tarifa0__sum"])
                        datos_cliente_venta.baseiva = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('subtotal_tarifa12', field="baseiva"))["subtotal_tarifa12__sum"])
                        datos_cliente_venta.monto = convert_cero_none(Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('iva_total', field="venta_cabecera.monto_iva"))["iva_total__sum"])
                        datos_cliente_venta.total_desc = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('descuento_tarifa0', field="descuento0 + descuentoiva"))["descuento_tarifa0__sum"])
                        datos_cliente_venta.total_ice = convert_cero_none(Venta_Cabecera.objects.filter(Q(status=1)).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item)).aggregate(Sum('total_ice', field="total_ice"))["total_ice__sum"])
                        datos_cliente_venta.total_venta = datos_cliente_venta.base0 + datos_cliente_venta.baseiva + datos_cliente_venta.monto + datos_cliente_venta.total_ice - datos_cliente_venta.total_desc
                        datos_cliente_venta.total_ret = convert_cero_none(CXC_cobro.objects.filter(cuentas_x_cobrar__in=Cuentas_por_Cobrar.objects.filter(venta_cabecera__in=Venta_Cabecera.objects.filter(status=1).filter(fecha__range=(fecha_ini, fecha_final), venta_detalle__in=Venta_Detalle.objects.filter(item=item))), cobro__tipo_comprobante=TipoComprobante.objects.get(id=16), status=1).aggregate(Sum('monto', field="cxc_cobro.monto"))["monto__sum"])
                        datos_cliente_venta.cobrar = datos_cliente_venta.total_venta - datos_cliente_venta.total_ret

                        for obj in datos_cliente_venta.lista_ventas:
                            if obj.getRetencionVenta():
                                retencion = convert_cero_none(obj.getRetencionVta().monto)
                            else:
                                retencion = 0.0

                            fila = [obj.numero_comprobante, str(obj.fecha),
                                            str(obj.vigencia_doc_empresa.serie) + "-" + obj.num_documento, obj.cliente.razon_social,
                                            obj.subtotal_tarifa0, obj.subtotal_tarifa12, obj.descuento_tarifa0 + obj.descuento_tarifa12,
                                            obj.iva_total, obj.total_ice, obj.getTotalPagar(), retencion, obj.getTotalPagar() - retencion, str(u"Activo")+COLUMNA]
                            lista.append(fila)

                        lista.append([u"", u"", u"", u"", u"", u"", u"", u"",u"", u"", u"", u"", u"", u"", u"", u""])
                        lista.append([u"Totales"+SUBTITULO, u"", u"", u"", str(datos_cliente_venta.base0)+G_TOTAL, str(datos_cliente_venta.baseiva)+G_TOTAL,
                                      str(datos_cliente_venta.total_desc)+G_TOTAL, str(datos_cliente_venta.monto)+G_TOTAL, str(datos_cliente_venta.total_ice)+G_TOTAL,
                                      str(datos_cliente_venta.total_venta)+G_TOTAL, str(datos_cliente_venta.total_ret)+G_TOTAL, str(datos_cliente_venta.cobrar)+G_TOTAL])

                        lista.append([u"", u"", u"", u"", u"", u"", u"", u""])

                except Item.DoesNotExist:
                    pass

@login_required(login_url='/')
@csrf_exempt
def reporte_ventas_item_excel(request):
    '''
    Vista qiue realiza el reporte de ventas por item excel
    :param request:
    :return:
    '''
    lista = []
    lista.append([u"", u"", u"", u"", u"", u"REPORTE DE VENTAS POR ITEMS"+TITULO])
    lista.append([u""])
    parametro = 0
    query = []
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    item_formset = formset_factory(ReporteSaldosForm)
    items = item_formset(prefix='items')


    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        buscador.is_valid()
        item_formset = formset_factory(ReporteSaldosForm)
        fecha_ini = buscador.getFechaIni()
        fecha_final = buscador.getFechaFinal()
        lista_id_items = []
        if None not in (fecha_ini, fecha_final):
            lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini.strftime("%d-%m-%Y")])
            lista.append([u"Fecha Final:"+SUBTITULO, fecha_final.strftime("%d-%m-%Y")])
            lista.append([u""])
            items = item_formset(request.POST, prefix='items')
            lista_ventas_items_excel(buscador, items, lista, lista_id_items, fecha_ini, fecha_final)

            return ExcelResponse(lista, u'Reporte Ventas por Item desde: '+fecha_ini.strftime("%Y-%m-%d") + u' hasta: ' +fecha_final.strftime("%Y-%m-%d"))

        else:

            messages.error(request, u"Error ingrese los parámetros de búsqueda correctos para continuar con el reporte")
            if fecha_ini is None:
                errors = buscador._errors.setdefault("fecha_ini", ErrorList())
                errors.append(u"La fecha inicial es requerida")
            if fecha_final is None:
                errors = buscador._errors.setdefault("fecha_final", ErrorList())
                errors.append(u"La fecha final es requerida")

            return render_to_response('reporte_ventas/reporte_ventas_fecha_item.html',
                              {"buscador": buscador,
                               "parametro": parametro,
                               "objetos": query,
                               "items": items}, context_instance=RequestContext(request))


    else:
        return ExcelResponse(lista)



########################################## REPORTE VENTAS PENDIENTES POR FECHA #########################################
@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_ventas_pendientes(request):
    lista = []
    lista_cliente = []
    lista_item = []
    lista_item_cliente = []
    lista_detalle = []
    parametro = 0
    item = ""
    cliente = ""
    buscador = BuscadorReportesVentaPendiente(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                                       "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    if request.method == "POST":
        buscador = BuscadorReportesVentaPendiente(request.POST)

        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()

                if buscador.getCliente() is None and buscador.getItem() is None:
                    parametro = 1
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).extra(where=["cant_vendida>cant_entregada"])

                elif buscador.getCliente() is not None and buscador.getItem() is None:
                    parametro = 2
                    cliente = buscador.getCliente()
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).filter(venta_cabecera__cliente=cliente).extra(where=["cant_vendida>cant_entregada"])


                elif buscador.getItem() is not None and buscador.getCliente() is None:
                    parametro = 3
                    item = buscador.getItem()
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).filter(item=item).extra(where=["cant_vendida>cant_entregada"])


                elif buscador.getCliente() is not None and buscador.getItem() is not None:
                    parametro = 4
                    item = buscador.getItem()
                    cliente = buscador.getCliente()
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).\
                        filter(item=item).filter(venta_cabecera__cliente=cliente).extra(where=["cant_vendida>cant_entregada"])

                else:
                    lista_detalle = []
        else:
            pass

    return render_to_response('reporte_ventas/reporte_ventas_pendientes.html',
                              {"buscador": buscador,
                               "lista_ventas": lista,
                               "lista_cliente": "",
                               "lista_item_cliente": lista_item_cliente,
                               "parametro": parametro,
                               "query": lista_detalle,
                               "item": item,
                               "cliente": cliente,
                               "lista_item": lista_item}, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_ventas_pendientes_pdf(request):
    lista_detalle = []
    parametro = 0
    item = ""
    cliente = ""

    empresa = Empresa.objects.filter(status=1)[0]
    usuario = request.user.first_name+' '+request.user.last_name

    if request.method == "POST":
        buscador = BuscadorReportesVentaPendiente(request.POST)

        if buscador.is_valid():
            ############################ GROUP BY ###################
            if buscador.getOrdenamiento() == '1':
                ORDENAMIENTO = "cliente__razon_social"
            elif buscador.getOrdenamiento() == '2':
                ORDENAMIENTO = "item__codigo"
            else:
                ORDENAMIENTO = "num_documento"
            ##########################################################

            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()

                if buscador.getCliente() is None and buscador.getItem() is None:
                    parametro = 1
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).extra(where=["cant_vendida>cant_entregada"])

                elif buscador.getCliente() is not None and buscador.getItem() is None:
                    parametro = 2
                    cliente = buscador.getCliente()
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).filter(venta_cabecera__cliente=cliente).extra(where=["cant_vendida>cant_entregada"])

                elif buscador.getItem() is not None and buscador.getCliente() is None:
                    parametro = 3
                    item = buscador.getItem()
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).filter(item=item).extra(where=["cant_vendida>cant_entregada"])

                elif buscador.getCliente() is not None and buscador.getItem() is not None:
                    parametro = 4
                    item = buscador.getItem()
                    cliente = buscador.getCliente()
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).\
                        filter(item=item).filter(venta_cabecera__cliente=cliente).extra(where=["cant_vendida>cant_entregada"])
                else:
                    lista_detalle = []
        else:
            pass

    html = render_to_string('reporte_ventas/reporte_ventas_pendientes_pdf.html',
                            {'pagesize': 'A4',
                             "parametro": parametro,
                             "query": lista_detalle,
                             "item": item,
                             "empresa":empresa,
                             "usuario":usuario,
                             "cliente": cliente
                             }, context_instance=RequestContext(request))
    nombre = (u'Rpt_vta_pend_de_'+fecha_ini.strftime("%Y-%m-%d") + u'_a_' +fecha_final.strftime("%Y-%m-%d"))
    return generar_pdf_nombre(html, nombre)


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_ventas_pendientes_excel(request):
    lista = []
    lista.append([u"", u"", u"", u"REPORTE DE VENTAS POR ENTREGAR"+TITULO])
    lista.append([u""])

    if request.method == "POST":
        buscador = BuscadorReportesVentaPendiente(request.POST)

        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                lista.append([u"Fecha Inicial:"+SUBTITULO, fecha_ini])
                lista.append([u"Fecha Final:"+SUBTITULO, fecha_final])
                if buscador.getCliente() is None and buscador.getItem() is None:
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).extra(where=["cant_vendida>cant_entregada"])
                    lista.append([u"Fecha Emisión"+COLUMNA, u"# Documento"+COLUMNA, u"Cliente"+COLUMNA,
                                  u"Item"+COLUMNA, u"Cant. Venta"+COLUMNA, u"Cant, Entregada"+COLUMNA, u"Cant. Pendiente"+COLUMNA])

                    for obj in lista_detalle:
                        cantidad_pendiente = float(obj.cantidad) - float(obj.cant_entregada)
                        fila = [obj.venta_cabecera.fecha, obj.venta_cabecera.num_documento, obj.venta_cabecera.cliente.razon_social,
                                str(obj.item.codigo)+"-"+str(obj.item.nombre), obj.cantidad, obj.cant_entregada, cantidad_pendiente]
                        lista.append(fila)

                elif buscador.getCliente() is not None and buscador.getItem() is None:
                    cliente = buscador.getCliente()
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).filter(venta_cabecera__cliente=cliente).extra(where=["cant_vendida>cant_entregada"])
                    lista.append([u""])
                    lista.append([u"Cliente:"+SUBTITULO, cliente.razon_social])
                    lista.append([u""])
                    lista.append([u"Fecha Emisión"+COLUMNA, u"# Documento"+COLUMNA,
                                  u"Item"+COLUMNA, u"Cant. Venta"+COLUMNA, u"Cant, Entregada"+COLUMNA, u"Cant. Pendiente"+COLUMNA])
                    for obj in lista_detalle:
                        cantidad_pendiente = float(obj.cantidad) - float(obj.cant_entregada)
                        fila = [obj.venta_cabecera.fecha, obj.venta_cabecera.num_documento, str(obj.item.codigo)+"-"+str(obj.item.nombre),
                                obj.cantidad, obj.cant_entregada, cantidad_pendiente]
                        lista.append(fila)

                elif buscador.getItem() is not None and buscador.getCliente() is None:
                    item = buscador.getItem()
                    lista.append([u""])
                    lista.append([u"Item:"+SUBTITULO, str(item.codigo)+" - "+str(item.nombre)])
                    lista.append([u""])
                    lista.append([u"Fecha Emisión"+COLUMNA, u"# Documento"+COLUMNA,
                                  u"Cliente"+COLUMNA, u"Cant. Venta"+COLUMNA, u"Cant, Entregada"+COLUMNA, u"Cant. Pendiente"+COLUMNA])
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).filter(item=item).extra(where=["cant_vendida>cant_entregada"])

                    for obj in lista_detalle:
                        cantidad_pendiente = float(obj.cantidad) - float(obj.cant_entregada)
                        fila = [obj.venta_cabecera.fecha, obj.venta_cabecera.num_documento, str(obj.venta_cabecera.cliente.razon_social),
                                obj.cantidad, obj.cant_entregada, cantidad_pendiente]
                        lista.append(fila)

                elif buscador.getCliente() is not None and buscador.getItem() is not None:
                    item = buscador.getItem()
                    cliente = buscador.getCliente()
                    lista.append([u""])
                    lista.append([u"Cliente:"+SUBTITULO, str(cliente.razon_social)])
                    lista.append([u"Item:"+SUBTITULO, str(item.codigo)+" - "+str(item.nombre)])
                    lista.append([u""])
                    lista.append([u"Fecha Emisión"+COLUMNA, u"# Documento"+COLUMNA,
                                 u"Cant. Venta"+COLUMNA, u"Cant, Entregada"+COLUMNA, u"Cant. Pendiente"+COLUMNA])
                    lista_detalle = Venta_Detalle.objects.filter(status=1).exclude(item__categoria_item__tipo_item__id=2).filter(venta_cabecera__fecha__range=(fecha_ini, fecha_final)).\
                        filter(item=item).filter(venta_cabecera__cliente=cliente).extra(where=["cant_vendida>cant_entregada"])

                    for obj in lista_detalle:
                        cantidad_pendiente = float(obj.cantidad) - float(obj.cant_entregada)
                        fila = [obj.venta_cabecera.fecha, obj.venta_cabecera.num_documento,
                                obj.cantidad, obj.cant_entregada, cantidad_pendiente]
                        lista.append(fila)
        nombre = (u'Rpt_vta_pend_de_'+fecha_ini.strftime("%Y-%m-%d") + u'_a_' +fecha_final.strftime("%Y-%m-%d"))
        return ExcelResponse(lista, nombre)
########################################################################################################################

@login_required(login_url='/')
@csrf_exempt
def reporte_ventas_contado_credito_fecha(request):
    '''
    Reporte de Ventas por Contado Crédito / en proceso revisar JasperServer
    :param request:
    :return:
    '''
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})

    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        match = resolve(request.path)
        #permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))

        if buscador.is_valid():
            if None not in (buscador.getFechaIni(), buscador.getFechaFinal()):
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()

                # Path to resource rest service:

                url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports'+ empresa.path_jasper + '/'

                # Report to process:
                report = "ventascontadocreditoporfecha.html"
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)
                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")}

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)
                print r.content[0:150]

    return render_to_response('reporte_ventas/reporte_ventas_contado_credito_fecha.html',
                              {"buscador": buscador, "objetos": r,}, context_instance=RequestContext(request))


@login_required(login_url='/')
@csrf_exempt
def reporte_ventas_contado_credito_fecha_excel(request):


    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        match = resolve(request.path)
        #permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))

        if buscador.is_valid():
            if None not in (buscador.getFechaIni(), buscador.getFechaFinal()):
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()

                # Path to resource rest service:
                print "path jasper: ", empresa.path_jasper
                url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports'+ empresa.path_jasper + '/'

                # Report to process:
                report = "ventascontadocreditoporfecha.xls"
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)
                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")}

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)
                print r.content[0:150]

            response = HttpResponse(r.content, content_type='application/excel')
            response['Content-Disposition'] = 'attachment;filename="ventas_contado_credito_porfecha.xls"'
            return response

@login_required(login_url='/')
@csrf_exempt
def reporte_ventas_contado_credito_fecha_pdf(request):

    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    r = None

    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        match = resolve(request.path)
        #permiso = Permisos.objects.using("base_central").get(etiqueta=str(match.url_name).replace(" ", ""))

        if buscador.is_valid():
            if None not in (buscador.getFechaIni(), buscador.getFechaFinal()):
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()

                # Path to resource rest service:
                print "path jasper: ", empresa.path_jasper

                url = 'http://162.242.232.70:8080/jasperserver/rest_v2/reports'+ empresa.path_jasper + '/'

                # Report to process:
                report = "ventascontadocreditoporfecha.pdf"
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)
                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")}

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)
                print r.content[0:150]



            response = HttpResponse(r.content, content_type='application/excel')
            response['Content-Disposition'] = 'attachment;filename="ventas_contado_credito_porfecha.pdf"'
            return response



@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_cartera_ventas_hasta_7agosto(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01"
                               })
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None:
                fecha_ini = buscador.getFechaIni()
                # Path to resource rest service:
                print('aaaaaaa',urljasper)
                url = urljasper + empresa.path_jasper + '/'
                print str(url)
                report = get_nombre_reporte('VTA_CARTERA_VENTA') + '.html'
                print(str(report)),'urlrep'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)
    else:
        print('no entra al post')
    return render_to_response('reporte_ventas/reporte_cartera_ventas.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_cartera_ventas(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01"
                                         })
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None:
                # tipo_reporte 1 estado 1
                # tipo_reporte 1 estado 2
                # tipo_reporte 2 estado 1
                # tipo_reporte 2 estado 2
                if int(buscador.getTipoReporte()) == 1 and buscador.getEstadoReporte() == 1:
                    print 'resumido vencido'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('CLIENTE_RESU_CXC_VENCIDA') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                    #r = s.get(url=url+report, auth=auth, params=params)

                if buscador.getTipoReporte() == 1 and buscador.getEstadoReporte() == 2:
                    print 'resumido por vencer'#estos 2 deben ser los detallados, no los resumidos
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('CLIENTE_RESU_CXC_POR_VENCER') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                if buscador.getTipoReporte() == 2 and buscador.getEstadoReporte() == 1:
                    print 'detallado vencido'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('CLIENTE_CXC_VENCIDA') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()

                if buscador.getTipoReporte() == 2 and buscador.getEstadoReporte() == 2:
                    print 'detallado por vencer'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('CLIENTE_CXC_POR_VENCER') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }
                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                '''
                fecha_ini = buscador.getFechaIni()
                print (buscador.getEstadoReporte())
                print (buscador.getTipoReporte())

                print(str(fecha_ini))
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'
                print str(url)
                report = get_nombre_reporte('CMP_CARTERA_COMPRA') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)
                print(str(report)),'urlrep'
                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                '''
                r = s.get(url=url+report, auth=auth, params=params)
        else:
            print 'el buscador esta mal'
            errors = buscador._errors.setdefault("estado_reporte", ErrorList())
            errors.append(u"La fecha inicial es requerida")
    else:
        print 'algo pasa y no es post'
    return render_to_response('reporte_ventas/reporte_cartera_ventas.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))




@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_comisiones(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'
                print(url)
                report = get_nombre_reporte('VTA_COMISIONES') + '.html'
                print(report)
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reporte_ventas/reporte_comisiones.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))



@csrf_exempt
@login_required()
def reporte_comisiones_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(request.GET)
    buscador.is_valid()  # Para que funcione el cleaned data
    fecha_ini = buscador.getFechaIni()
    fecha_final = buscador.getFechaFinal()
    urljasper = get_url_jasper()
    if None not in (fecha_ini, fecha_final):
        # Path to resource rest service:
        url = urljasper + empresa.path_jasper + '/'
        # Report to process:
        report = get_nombre_reporte('VTA_COMISIONES') + '.xls'
        # Authorisation credentials:
        auth = (empresa.user_jasper, empresa.pass_jasper)
        # Params
        params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d"),
                  'fecha_final': fecha_final.strftime("%Y-%m-%d")
                  }

        # Init session so we have no need to auth again and again:
        s = requests.Session()
        r = s.get(url=url+report, auth=auth, params=params)

        response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
        nombre = "Comisiones "+str(fecha_ini)+" al "+str(fecha_final)
        response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
        return response



@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_venta_retencion_control(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'
                print(url)
                report = get_nombre_reporte('VENTAS_RETENCION') + '.html'
                print(report)
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)
                print 'la ventaretencioncontrol'
                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reporte_ventas/reporte_venta_retencion_control.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))

#lo mismo de lo de arriba pero en excel
@csrf_exempt
@login_required()
def reporte_venta_retencion_control_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(request.GET)
    buscador.is_valid()  # Para que funcione el cleaned data
    fecha_ini = buscador.getFechaIni()
    fecha_final = buscador.getFechaFinal()
    urljasper = get_url_jasper()
    if None not in (fecha_ini, fecha_final):
        # Path to resource rest service:
        url = urljasper + empresa.path_jasper + '/'
        # Report to process:
        report = get_nombre_reporte('VENTAS_RETENCION') + '.xls'
        # Authorisation credentials:
        auth = (empresa.user_jasper, empresa.pass_jasper)
        # Params
        params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                  'fecha_final': fecha_final.strftime("%Y-%m-%d")
                  }

        # Init session so we have no need to auth again and again:
        s = requests.Session()
        r = s.get(url=url+report, auth=auth, params=params)

        response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
        nombre = "Control_retenciones "+str(fecha_ini)+" al "+str(fecha_final)
        response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
        return response


#las 4 cosas pero ahora en excel :o

@login_required(login_url='/')
@csrf_exempt
#@permiso_accion(mensaje=mensaje_permiso)
def reporte_cartera_ventas_excel(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01"
                                        })
    print('este reporte uq no anda')
    r = None
    s = None
    post = False  # flag template message
    if request.method == "GET":
        buscador = BuscadorReportes(request.GET)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None:
                # tipo_reporte 1 estado 1
                # tipo_reporte 1 estado 2
                # tipo_reporte 2 estado 1
                # tipo_reporte 2 estado 2
                if int(buscador.getTipoReporte()) == 1 and buscador.getEstadoReporte() == 1:
                    print 'resumido vencido'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('CLIENTE_RESU_CXC_VENCIDA') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                    r = s.get(url=url+report, auth=auth, params=params)
                    print('casi termino la funcion')
                    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                    nombre = "CXCobrar_plazoresumido vencido "+str(fecha_ini)
                    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
                    return response
                    #r = s.get(url=url+report, auth=auth, params=params)

                if buscador.getTipoReporte() == 1 and buscador.getEstadoReporte() == 2:
                    print 'resumido por vencer'#estos 2 deben ser los detallados, no los resumidos
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('CLIENTE_RESU_CXC_POR_VENCER') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                    r = s.get(url=url+report, auth=auth, params=params)
                    print('casi termino la funcion')
                    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                    nombre = "CXCobrar_plazoresumido por vencer "+str(fecha_ini)
                    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
                    return response
                if buscador.getTipoReporte() == 2 and buscador.getEstadoReporte() == 1:
                    print 'detallado vencido'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('CLIENTE_CXC_VENCIDA') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }

                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                    r = s.get(url=url+report, auth=auth, params=params)
                    print('casi termino la funcion')
                    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                    nombre = "CXCobrar_plazo_detallado vencido "+str(fecha_ini)
                    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
                    return response

                if buscador.getTipoReporte() == 2 and buscador.getEstadoReporte() == 2:
                    print 'detallado por vencer'
                    fecha_ini = buscador.getFechaIni()
                    print (buscador.getEstadoReporte())
                    print (buscador.getTipoReporte())

                    print(str(fecha_ini))
                    # Path to resource rest service:
                    url = urljasper + empresa.path_jasper + '/'
                    print str(url)
                    report = get_nombre_reporte('CLIENTE_CXC_POR_VENCER') + '.html'
                    # Authorisation credentials:
                    auth = (empresa.user_jasper, empresa.pass_jasper)
                    print(str(report)),'urlrep'
                    # Params
                    params = {'fecha_inicio': fecha_ini.strftime("%Y-%m-%d")
                              }
                    # Init session so we have no need to auth again and again:
                    s = requests.Session()
                    r = s.get(url=url+report, auth=auth, params=params)
                    print('casi termino la funcion')
                    response = HttpResponse(r.content, mimetype='application/vnd.ms-excel')
                    nombre = "CXCobrar_plazodetallado por vencer "+str(fecha_ini)
                    response['Content-Disposition'] = 'attachment;filename="'+nombre+'".xls"'
                    return response
    else:
        print('falla el post')


@login_required(login_url='/')
@csrf_exempt
@permiso_accion(mensaje=mensaje_permiso)
def reporte_estadistica_ventas_x_cliente(request):
    empresa = Empresa_Parametro.objects.using("base_central").get(id=Empresa.objects.all()[0].empresa_general_id)
    buscador = BuscadorReportes_fecha_inicial_final(initial={"fecha_ini": str(datetime.datetime.now().year)+"-01-01",
                                         "fecha_final": str(datetime.datetime.now().strftime("%Y-%m-%d"))})
    r = None
    post = False  # flag template message
    if request.method == "POST":
        buscador = BuscadorReportes_fecha_inicial_final(request.POST)
        post = True
        urljasper = get_url_jasper()
        if buscador.is_valid():
            if buscador.getFechaIni() is not None and buscador.getFechaFinal() is not None:
                fecha_ini = buscador.getFechaIni()
                fecha_final = buscador.getFechaFinal()
                # Path to resource rest service:
                url = urljasper + empresa.path_jasper + '/'

                report = get_nombre_reporte('VENTA_ESTAD_VTA_CLIENTE') + '.html'
                # Authorisation credentials:
                auth = (empresa.user_jasper, empresa.pass_jasper)

                # Params
                params = {'fecha_inicial': fecha_ini.strftime("%Y-%m-%d"),
                          'fecha_final': fecha_final.strftime("%Y-%m-%d")
                          }

                # Init session so we have no need to auth again and again:
                s = requests.Session()
                r = s.get(url=url+report, auth=auth, params=params)

    return render_to_response('reporte_ventas/reporte_estadistica_ventas_cliente.html',
                              {"buscador": buscador, "objetos": r, "post": post
                               }, context_instance=RequestContext(request))


